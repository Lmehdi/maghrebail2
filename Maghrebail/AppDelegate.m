//
//  AppDelegate.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenu.h"
#import "SlideViewController_iphone.h"
#import "JSON.h"


#import "AdvancedPushSDKViewController.h"
#import "ASIHTTPRequest.h"

#import <sys/xattr.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <FacebookSDK/FacebookSDK.h>
#import "PubliciteViewController.h"
#import "Appirater.h"
#import "PubliciteViewController_iphone.h"
#import "PubliciteViewController_ipad.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NewDashBoardVC_iphone.h"
#import "NewDashBoardVC_ipad.h"
#import "SimulationViewController_iphone.h"
#import "AuthentificationViewController_iphone.h"

 #import "Flurry.h"
@implementation AppDelegate
@synthesize isLunched;
@synthesize timer;
@synthesize lastNotif, token, pushSDKView;
@synthesize m_SideMenu;
@synthesize notifstate;

- (void)dealloc
{
    [self.lastNotif release];
    [self.token release];
    [self.pushSDKView release];
    [_window release];
    [timer release];
    [super dealloc];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //FlurrySessionBuilder* builder = [FlurrySessionBuilder new];
    

    
   // [Flurry startSession::@”” withSessionBuilder:builder];
    
    [self createDynamicShortcutItems];
    self.notifstate=NO;
    self.isfirsttimeleasebox=YES;
    self.isfirsttimedash=YES;
    /*
     int cacheSizeMemory = 4*1024*1024; // 4MB
     int cacheSizeDisk = 32*1024*1024; // 32MB
     NSURLCache *sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
     [NSURLCache setSharedURLCache:sharedCache];
     */
    NSData *isFromThreeD = [NSKeyedArchiver archivedDataWithRootObject:@"NO"];
    [DataManager writeDataIntoCachWith: isFromThreeD andKey:@"3DTOUCH"];
    [Fabric with:@[[Crashlytics class]]];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"deconnexionProfil"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    [Appirater setAppId:@"635376099"];
    [Appirater setDaysUntilPrompt:7];
    [Appirater setUsesUntilPrompt:3];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:7];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];

    [[UIApplication sharedApplication] setDelegate:self];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    if (!IS_IPAD)
    {
        PubliciteViewController *publicite = [[PubliciteViewController_iphone alloc]initWithNibName:@"PubliciteViewController_iphone" bundle:nil];
        [publicite loadPub];
    }
    else
    {
        PubliciteViewController *publicite = [[PubliciteViewController_ipad alloc]initWithNibName:@"PubliciteViewController_ipad" bundle:nil];
        [publicite loadPub];
    }
    
    // Register for remote notifs
    
    modeleDevice = [self getModel];
    
    self.token = [[NSString alloc] initWithString:@""];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    // Handle push notifications that may have been received when app was inactive
    
    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (remoteNotif) {
        self.lastNotif = [[NSDictionary alloc] initWithDictionary:remoteNotif];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(insertPushView)
                                                     name:@"lunchApp"
                                                   object:nil];
    }
    
    //start appli
    
    [DataManager sharedManager];
    
   // [Flurry setAppVersion:@"4.0"];
    if (IS_IPAD)
    {
        [Flurry startSession:@"TJ2QTZD89VFQTK4SS334"];
    }
    else
    {
        [Flurry startSession:@"NQB24DNDHDNCQCVZB93Z"];
    }
    
    if (IS_IPAD) {
//        DashboardViewController * dashBoardViewController = nil;
//        !IS_IPAD?(dashBoardViewController = [[DashboardViewController_iphone alloc]initWithNibName:@"DashboardViewController_iphone" bundle:nil]):
//        (dashBoardViewController = [[DashboardViewController_ipad alloc]initWithNibName:@"DashboardViewController_ipad" bundle:nil]);
//        self.mainNavigationController = [[UINavigationController alloc]initWithRootViewController:dashBoardViewController];
    } else {
        NewDashBoardVC_iphone* newDash=[[NewDashBoardVC_iphone alloc] initWithNibName:@"NewDashBoardVC_iphone" bundle:nil];
        [newDash hideAllSeprators:true];
        self.mainNavigationController = [[UINavigationController alloc]initWithRootViewController:newDash];
    }
    
    SplashViewController * splashViewController = nil;
    
    !IS_IPAD? splashViewController = ([[SplashViewController_iphone alloc]initWithNibName:@"SplashViewController_iphone" bundle:nil]):
    (splashViewController = [[SplashViewController_ipad alloc]initWithNibName:@"SplashViewController_ipad" bundle:nil]);
    
    
    self.mainNavigationController.navigationBarHidden = YES;
    
    [self.window setRootViewController:splashViewController];
    [self.window makeKeyAndVisible];
    
    //    DashboardViewController * dashBoardViewController = nil;
    //    !IS_IPAD?(dashBoardViewController = [[DashboardViewController_iphone alloc]initWithNibName:@"DashboardViewController_iphone" bundle:nil]):
    //    (dashBoardViewController = [[DashboardViewController_ipad alloc]initWithNibName:@"DashboardViewController_ipad" bundle:nil]);
    //
    //    SplashViewController * splashViewController = nil;
    //
    //    !IS_IPAD? splashViewController = ([[SplashViewController_iphone alloc]initWithNibName:@"SplashViewController_iphone" bundle:nil]):
    //    (splashViewController = [[SplashViewController alloc]initWithNibName:@"SplashViewController_ipad" bundle:nil]);
    //
    //    UINavigationController * mainNavigationController = [[UINavigationController alloc]initWithRootViewController:dashBoardViewController];
    //    mainNavigationController.navigationBarHidden = YES;
    //
    //    [self.window setRootViewController:mainNavigationController];
    //    //[self.window addSubview: mainNavigationController.view];
    //
    //    [self.window makeKeyAndVisible];
    //    [dashBoardViewController presentModalViewController:splashViewController animated:NO];
    //    dashBoardViewController.window = self.window;
    
    [self performSelector:@selector(loadMentionsLegales) withObject:nil];
    [self performSelector:@selector(loadInscription) withObject:nil];
    [self performSelector:@selector(loadPublications) withObject:nil];
    [self performSelector:@selector(loadSimulateur) withObject:nil];
    [self performSelectorInBackground:@selector(loadProduitsData) withObject:nil];
    [self performSelectorInBackground:@selector(loadInfoSecurite) withObject:nil];
    [self performSelectorInBackground:@selector(loadAppGroupe) withObject:nil];
    
    //[NSThread detachNewThreadSelector:@selector(loadMentionsLegales) toTarget:self withObject:nil];
    //[NSThread detachNewThreadSelector:@selector(loadPublications) toTarget:self withObject:nil];
    //[NSThread detachNewThreadSelector:@selector(loadSimulateur) toTarget:self withObject:nil];
    //[NSThread detachNewThreadSelector:@selector(loadProduitsData) toTarget:self withObject:nil];
    
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager locatedMe];
    isLunched = YES;
    // push notification
    if (launchOptions) {
        NSDictionary *remoteNotifDict = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        [self performSelector:@selector(launchPush:) withObject:remoteNotifDict afterDelay:4];
    }
    self.window.backgroundColor = [UIColor grayColor];
    
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    NSString *resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"appgroupe" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *responseDico = [resultString JSONValue];
    NSArray *dataArray = [responseDico objectForKey:@"response"];
    dataManager.appgroupe = [NSMutableArray arrayWithArray:dataArray];
    
    return YES;
}

-(void) launchPush:(NSDictionary *) remoteNotif{
    
    if ([[remoteNotif objectForKey:@"aps"] objectForKey:@"alert"]) {
        
        UIAlertView* alertShow=[[UIAlertView alloc] initWithTitle:@"MAGHREBAIL" message:[[remoteNotif objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertShow show];
        [alertShow release];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    
    /*
     
     DataManager *dataManager = [DataManager sharedManager];
     
     if (!dataManager.isStayOn)
     {
     dataManager.isAuthentified = NO;
     [DataManager deleteFileFromCache:@"authentificationData"];
     dataManager.authentificationData = nil;
     }
     
     */
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    self.token = [deviceToken description];
    self.token = [self.token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    self.token = [self.token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.token forKey:@"pushToken"];
     NSLog(@"deviceID After: %@", self.token);
    
    [self requestPushForDevice];
}

- (void)requestPushForDevice
{
    
    NSString *feedURLString = [NSString stringWithFormat:@"http://slice.mobiblanc.com/scripte_php/addDevice.php?model=Apple-%@-%@&device=Iphone&token=%@&keu_flury=NQB24DNDHDNCQCVZB93Z", modeleDevice, [[UIDevice currentDevice] systemVersion], self.token];
    
    ASIHTTPRequest *dataRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:feedURLString]];
    [dataRequest setDidFinishSelector:@selector(requestCompleted:)];
    [dataRequest setDidFailSelector:@selector(requestError:)];
    [dataRequest setDelegate:self];
    [dataRequest setTag:1];
    [dataRequest startAsynchronous];
    
}

- (void)requestCompleted:(ASIHTTPRequest *)request
{
    NSLog(@"request: %li and url: %@", (long)request.tag, [request.url description]);
    // Use when fetching text data
    NSString *responseString = [request responseString];
    NSLog(@"responseString: %@", responseString);
    
    // Use when fetching binary data
    NSData *responseData = [request responseData];
    NSString *data = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"respondeData: %@", data);
}

- (void)requestError:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error in request: %@", [error description]);
}

- (NSString *)getModel
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad mini 2G (Cellular)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    NSString *aux = [[platform componentsSeparatedByString:@","] objectAtIndex:0];
    
    //If a newer version exist
    
    if ([aux rangeOfString:@"iPhone"].location!=NSNotFound) {
        int version = [[aux stringByReplacingOccurrencesOfString:@"iPhone" withString:@""] intValue];
        if (version == 3) return @"iPhone4";
        if (version >= 4) return @"iPhone4s";
        
    }
    if ([aux rangeOfString:@"iPod"].location!=NSNotFound) {
        int version = [[aux stringByReplacingOccurrencesOfString:@"iPod" withString:@""] intValue];
        if (version >=4) return @"iPod4thGen";
    }
    if ([aux rangeOfString:@"iPad"].location!=NSNotFound) {
        int version = [[aux stringByReplacingOccurrencesOfString:@"iPad" withString:@""] intValue];
        if (version ==1) return @"iPad3G";
        if (version >=2) return @"iPad2";
    }
    
    //If none was found, send the original string
    return platform;
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error in registration. Error: %@", error);
    
}
-(void)FonctionQuiInstancieLesVues{
    
    // Poster cette NSNotification après que toutes les autres vues aient été chargés (apres splash, etc.)
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lunchApp" object:self];
    
    
    //[FlurryAPI logEvent:@"Lancemant de l'app"];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"Background");
    DataManager *dataManager = [DataManager sharedManager];
    if (dataManager.isDemo)
    {
        dataManager.isDemo = NO;
    }
    
    if (!dataManager.isStayOn)
    {
        dataManager.isAuthentified = NO;
        [DataManager deleteFileFromCache:@"authentificationData"];
        dataManager.authentificationData = nil;
    }
    
    
    else
    {
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:900.0
//                                                      target:self
//                                                    selector:@selector(disconnect:)
//                                                    userInfo:nil
//                                                     repeats:NO];
    }
}

- (void)disconnect:(NSTimer *)aTimer
{
    DataManager *dataManager = [DataManager sharedManager];
    dataManager.isAuthentified = NO;
    [DataManager deleteFileFromCache:@"authentificationData"];
    dataManager.authentificationData = nil;
    [self.timer invalidate];
    timer = nil;
    
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"deconnexionProfil"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"Terminate");
    
    DataManager *dataManager = [DataManager sharedManager];
    if (!dataManager.isStayOn)
    {
        dataManager.isAuthentified = NO;
        [DataManager deleteFileFromCache:@"authentificationData"];
        dataManager.authentificationData = nil;
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [Appirater appEnteredForeground:YES];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (isLunched)
    {
        
        DataManager *dataManager = [DataManager sharedManager];
        dataManager.escapePub = YES;
      //  UINavigationController * mainNavigationContr=nil;
        
        if (IS_IPAD) {
            [self showNewSideMenu];
            return;
//            DashboardViewController * dashBoardViewController = nil;
//            !IS_IPAD?(dashBoardViewController = [[DashboardViewController_iphone alloc]initWithNibName:@"DashboardViewController_iphone" bundle:nil]):
//            (dashBoardViewController = [[DashboardViewController_ipad alloc]initWithNibName:@"DashboardViewController_ipad" bundle:nil]);
//            mainNavigationContr = [[UINavigationController alloc]initWithRootViewController:dashBoardViewController];
        }else{
          //  NewDashBoardVC_iphone* newDash=[[NewDashBoardVC_iphone alloc] initWithNibName:@"NewDashBoardVC_iphone" bundle:nil];
          //  [newDash hideAllSeprators:true];
         //   mainNavigationContr = [[UINavigationController alloc]initWithRootViewController:newDash];
        }
        
      //  mainNavigationContr.navigationBarHidden = YES;
       // [self.window setRootViewController:mainNavigationContr];
        //[self.window addSubview: mainNavigationController.view];
        //[self.window makeKeyAndVisible];
        //dashBoardViewController.window = self.window;
    }
//    if (self.timer.isValid)
//    {
//        [self.timer invalidate];
//        self.timer = nil;
//    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [FBSession setDefaultAppID:@"123703917830824"];
    [FBSettings publishInstall:[FBSession defaultAppID]];
    //Should refresh from sever
}

-(void)loadProduitsData
{
    NSURL * urlTemp = [NSURL URLWithString:kPRODUITS_URL];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 1;
    [request startAsynchronous];
}

-(void)loadInfoSecurite
{
    NSURL * urlTemp = [NSURL URLWithString:kINFO_URL];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 5;
    [request startAsynchronous];
}


- (void)loadInscription
{
    NSURL * urlTemp = [NSURL URLWithString:URL_INSCRIPTION];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 20;
    [request startAsynchronous];
}

-(void)loadMentionsLegales
{
    NSURL * urlTemp = [NSURL URLWithString:URL_MENTIONS_LEGALES];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 2;
    [request startAsynchronous];
}

-(void)loadPublications
{
    NSURL * urlTemp = [NSURL URLWithString:URL_PUBLICATIONS];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 3;
    [request startAsynchronous];
}

-(void)loadSimulateur
{
    NSURL * urlTemp = [NSURL URLWithString:URL_SIMULATEUR];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 4;
    [request startAsynchronous];
}

-(void)loadAppGroupe
{
    NSURL * urlTemp = [NSURL URLWithString:URL_APPGROUPE];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [request setTimeOutSeconds:300];
    request.delegate = self;
    request.tag = 10;
    [request startAsynchronous];
}


-(void)requestFinished:(ASIHTTPRequest *)request
{
    DataManager *dataManager = [DataManager sharedManager];
    NSData *responseData = [request responseData];
    NSString * resultString =  [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *responseDico = [resultString JSONValue];
    NSArray *data = [responseDico objectForKey:@"response"];
    
    switch (request.tag)
    {
        case 1:
            if (data)
            {
                dataManager.produits = [NSMutableArray arrayWithArray:data];
                NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataProduits andKey:@"produits"];
            }
            else
            {
                NSData *dataProduits = [DataManager readDataIntoCachWith:@"produits"];
                if (dataProduits)
                {
                    dataManager.produits = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"produits" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.produits = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        case 2:
            if (data)
            {
                dataManager.mentions = [NSMutableArray arrayWithArray:data];
                NSData *dataMentions = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataMentions andKey:@"mentions"];
            }
            else
            {
                
                NSData *dataMentions = [DataManager readDataIntoCachWith:@"mentions"];
                if (dataMentions)
                {
                    dataManager.mentions = [NSKeyedUnarchiver unarchiveObjectWithData:dataMentions];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mentions" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.mentions = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        case 3:
            if (data)
            {
                dataManager.publications = [NSMutableArray arrayWithArray:data];
                NSData *dataPublications = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataPublications andKey:@"publications"];
            }
            else
            {
                
                NSData *dataPublications = [DataManager readDataIntoCachWith:@"publications"];
                if (dataPublications)
                {
                    dataManager.publications = [NSKeyedUnarchiver unarchiveObjectWithData:dataPublications];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"publications" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.publications = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        case 4:
            if (resultString)
            {
                dataManager.simulateur = resultString;
                NSData *dataPublications = [NSKeyedArchiver archivedDataWithRootObject:resultString];
                [DataManager writeDataIntoCachWith:dataPublications andKey:@"simulateur"];
            }
            else
            {
                
                NSData *dataPublications = [DataManager readDataIntoCachWith:@"simulateur"];
                if (dataPublications)
                {
                    dataManager.simulateur = [NSKeyedUnarchiver unarchiveObjectWithData:dataPublications];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"simulateur" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    dataManager.simulateur = [NSString stringWithFormat:@"%@",resultString];
                }
            }
            break;
        case 5:
            if (data)
            {
                dataManager.infoSecurite = [NSMutableArray arrayWithArray:data];
                NSData *dataPublications = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataPublications andKey:@"infoSecurite"];
            }
            else
            {
                
                NSData *dataPublications = [DataManager readDataIntoCachWith:@"infoSecurite"];
                if (dataPublications)
                {
                    dataManager.infoSecurite = [NSKeyedUnarchiver unarchiveObjectWithData:dataPublications];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"infoSecurite" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.infoSecurite = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        case 10:
            NSLog(@"APP GROUPE FINISHED");
            if (data)
            {
                dataManager.appgroupe = [NSMutableArray arrayWithArray:data];
                NSData *dataPublications = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataPublications andKey:@"appgroupe"];
            }
            else
            {
                
                NSData *dataPublications = [DataManager readDataIntoCachWith:@"appgroupe"];
                if (dataPublications)
                {
                    dataManager.appgroupe = [NSKeyedUnarchiver unarchiveObjectWithData:dataPublications];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"appgroupe" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.appgroupe = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        case 20:
            NSLog(@"INSCRIPTION FINISHED");
            if (data)
            {
                dataManager.inscription = [NSMutableArray arrayWithArray:data];
                NSData *dataPublications = [NSKeyedArchiver archivedDataWithRootObject:data];
                [DataManager writeDataIntoCachWith:dataPublications andKey:@"inscription"];
            }
            else
            {
                
                NSData *dataPublications = [DataManager readDataIntoCachWith:@"inscription"];
                if (dataPublications)
                {
                    dataManager.inscription = [NSKeyedUnarchiver unarchiveObjectWithData:dataPublications];
                }
                else
                {
                    resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"inscription" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                    responseDico = [resultString JSONValue];
                    data = [responseDico objectForKey:@"response"];
                    dataManager.inscription = [NSMutableArray arrayWithArray:data];
                }
            }
            break;
        default:
            break;
    }
    
    
    NSLog(@"Received Header %@",resultString);
    
}
-(void)requestFailed:(ASIHTTPRequest *)request
{
    
    DataManager *dataManager = [DataManager sharedManager];
    NSData *dataArchived;
    NSString *resultString;
    NSDictionary *responseDico;
    NSArray *dataArray;
    switch (request.tag)
    {
        case 1:
            dataArchived = [DataManager readDataIntoCachWith:@"produits"];
            if (dataArchived)
            {
                dataManager.produits = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"produits" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.produits = [NSMutableArray arrayWithArray:dataArray];
            }
            
            break;
        case 2:
            
            dataArchived = [DataManager readDataIntoCachWith:@"mentions"];
            if (dataArchived)
            {
                dataManager.mentions = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mentions" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.mentions = [NSMutableArray arrayWithArray:dataArray];
            }
            break;
        case 3:
            dataArchived = [DataManager readDataIntoCachWith:@"publications"];
            if (dataArchived)
            {
                dataManager.publications = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"publications" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.publications = [NSMutableArray arrayWithArray:dataArray];
            }
            
            break;
        case 4:
            
            dataArchived = [DataManager readDataIntoCachWith:@"simulateur"];
            if (dataArchived)
            {
                dataManager.simulateur = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"simulateur" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                dataManager.simulateur = [NSString stringWithFormat:@"%@",resultString];
            }
            
            break;
            
        case 5:
            dataArchived = [DataManager readDataIntoCachWith:@"infoSecurite"];
            if (dataArchived)
            {
                dataManager.infoSecurite = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"infoSecurite" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.infoSecurite = [NSMutableArray arrayWithArray:dataArray];
            }
            
            break;
        case 10:
            NSLog(@"APP GROUPE FAILED");
            dataArchived = [DataManager readDataIntoCachWith:@"appgroupe"];
            if (dataArchived)
            {
                dataManager.appgroupe = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"appgroupe" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.appgroupe = [NSMutableArray arrayWithArray:dataArray];
            }
            
            break;
        case 20:
            NSLog(@"INSCRIPTION FAILED");
            dataArchived = [DataManager readDataIntoCachWith:@"inscription"];
            if (dataArchived)
            {
                dataManager.inscription = [NSKeyedUnarchiver unarchiveObjectWithData:dataArchived];
            }
            else
            {
                resultString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"inscription" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
                responseDico = [resultString JSONValue];
                dataArray = [responseDico objectForKey:@"response"];
                dataManager.inscription = [NSMutableArray arrayWithArray:dataArray];
            }
            
            break;
        default:
            break;
    }
}

#pragma mark --
#pragma mark -- Slide Menu Function

-(void)showNewSideMenu{
    
    SlideViewController  *leftSideMenuController = nil;
    SlideViewController *rightSideMenuController = nil;
    
    leftSideMenuController = [[SlideViewController_ipad alloc] initWithNibName:@"SlideViewController_ipad" bundle:nil];
    UINavigationController * navController =[[UINavigationController alloc] initWithRootViewController:[self dashNewVC]];
    
    [navController setNavigationBarHidden:YES];
    
    
    self.m_SideMenu= [MFSideMenuContainerViewController
                      containerWithCenterViewController:navController
                      leftMenuViewController:leftSideMenuController
                      rightMenuViewController:rightSideMenuController];
    self.m_SideMenu.panMode=CHMFSideMenuPanModeNone;
    self.window.rootViewController = self.m_SideMenu;
    [self.window makeKeyAndVisible];
}

- (void)createDynamicShortcutItems {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        
        // create several (dynamic) shortcut items
     
        UIApplicationShortcutItem *item3    =   [[UIApplicationShortcutItem alloc] initWithType:@"PRODUIT" localizedTitle:@"Produits" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"3d_produit"] userInfo:nil];
        UIApplicationShortcutItem *item2    =   [[UIApplicationShortcutItem alloc] initWithType:@"LEASEBOX" localizedTitle:@"LEASEBOX" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"3d_leasebox"] userInfo:nil];
        UIApplicationShortcutItem *item1    =   [[UIApplicationShortcutItem alloc] initWithType:@"SIMULATEUR" localizedTitle:@"Simulateur" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"3d_simulateur"] userInfo:nil];
        UIApplicationShortcutItem *item4    =   [[UIApplicationShortcutItem alloc] initWithType:@"AGENCES" localizedTitle:@"Agences" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"3d_agences"] userInfo:nil];
    
        // add all items to an array
        NSArray *items = @[item1, item2, item3, item4];
    
        // add the array to our app
        [UIApplication sharedApplication].shortcutItems     =   items;
    }
}


- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    if ([shortcutItem.type isEqualToString:@"SIMULATEUR"]) {
        if(!IS_IPAD) {
            [self showSimulateur];
        }
        return;
    }
    if ([shortcutItem.type isEqualToString:@"LEASEBOX"]) {
        if(!IS_IPAD) {
            
            NSData *isFromThreeD = [NSKeyedArchiver archivedDataWithRootObject:@"YES"];
            [DataManager writeDataIntoCachWith: isFromThreeD andKey:@"3DTOUCH"];
            [self showLeasebox];
        }
        return;
    }
    
    if ([shortcutItem.type isEqualToString:@"PRODUIT"]) {
        if(!IS_IPAD) {
            [self showProduits];
        }
        return;
    }
    
    if([shortcutItem.type isEqualToString:@"AGENCES"]) {
        if(!IS_IPAD) {
            [self showAgences];
        }
        return;
    }
}

- (void) showSimulateur {
    SlideViewController  *leftSideMenuController    =   nil;
    SlideViewController *rightSideMenuController    =   nil;
    AppDelegate *delegate                           =   [[UIApplication sharedApplication] delegate];
    UINavigationController * navController          =   nil;
    
    SimulationViewController_iphone* simulationViewController   =   [[SimulationViewController_iphone alloc] initWithNibName:@"SimulationViewController_iphone" bundle:nil];
    
    if (!IS_IPAD) {
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        rightSideMenuController.window = delegate.window;
        navController =[[UINavigationController alloc] initWithRootViewController:simulationViewController];
    }
    
    [navController setNavigationBarHidden:YES];
    
    self.sideMenu = [MFSideMenu menuWithNavigationController:navController
                                      leftSideMenuController:leftSideMenuController
                                     rightSideMenuController:rightSideMenuController];
    
    leftSideMenuController.sideMenu = self.sideMenu;
    rightSideMenuController.sideMenu = self.sideMenu;
    //
    self.window.rootViewController =self.sideMenu.navigationController ;
    self.window.rootViewController.view.frame=CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
    [self.window makeKeyAndVisible];
}

- (void) showLeasebox {
    SlideViewController  *leftSideMenuController    =   nil;
    SlideViewController *rightSideMenuController    =   nil;
    AppDelegate *delegate                           =   [[UIApplication sharedApplication] delegate];
    UINavigationController * navController          =   nil;
    
    AuthentificationViewController_iphone* authentificationViewController = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    
    if (!IS_IPAD) {
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        rightSideMenuController.window = delegate.window;
        navController = [[UINavigationController alloc] initWithRootViewController:authentificationViewController];
    }
    
    [navController setNavigationBarHidden:YES];
    
    self.sideMenu = [MFSideMenu menuWithNavigationController:navController
                                      leftSideMenuController:leftSideMenuController
                                     rightSideMenuController:rightSideMenuController];
    
    leftSideMenuController.sideMenu                 =   self.sideMenu;
    rightSideMenuController.sideMenu                =   self.sideMenu;
    //
    self.window.rootViewController                  =   self.sideMenu.navigationController ;
    self.window.rootViewController.view.frame       =   CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
    [self.window makeKeyAndVisible];
}
-(CGFloat )adaptecelllabelldyna:(NSString *)sts :(CGFloat ) wid {
    //    CGSize constraint = CGSizeMake(wid, 20000.0f);
    
    UILabel* label      =   [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, wid, 0)];
    [label setNumberOfLines:0];
    [label setFont:[UIFont fontWithName:@"System" size:17.0f]];
    [label setText:sts];
    [label setTextAlignment:NSTextAlignmentRight];
    [label sizeToFit];
    
    return CGRectGetMaxY(label.frame);
    
}
- (void) showProduits {
    SlideViewController  *leftSideMenuController    =   nil;
    SlideViewController *rightSideMenuController    =   nil;
    AppDelegate *delegate                           =   [[UIApplication sharedApplication] delegate];
    UINavigationController * navController          =   nil;
    
    DetailsProfessionnelsVC_iphone* detailViewController = [[DetailsProfessionnelsVC_iphone alloc] initWithNibName:@"DetailsProfessionnelsVC_iphone" bundle:nil];
    detailViewController.index=1;
    [detailViewController changeScrollPosition];

    if (!IS_IPAD) {
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        rightSideMenuController.window = delegate.window;
        navController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    }
    
    [navController setNavigationBarHidden:YES];
    
    self.sideMenu = [MFSideMenu menuWithNavigationController:navController
                                      leftSideMenuController:leftSideMenuController
                                     rightSideMenuController:rightSideMenuController];
    
    leftSideMenuController.sideMenu = self.sideMenu;
    rightSideMenuController.sideMenu = self.sideMenu;
    //
    self.window.rootViewController =self.sideMenu.navigationController ;
    self.window.rootViewController.view.frame=CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
    [self.window makeKeyAndVisible];
}

- (void) showAgences {
    SlideViewController  *leftSideMenuController    =   nil;
    SlideViewController *rightSideMenuController    =   nil;
    AppDelegate *delegate                           =   [[UIApplication sharedApplication] delegate];
    UINavigationController * navController          =   nil;
    
    AgencesViewController_iphone* agencesViewController = [[AgencesViewController_iphone alloc] initWithNibName:@"AgencesViewController_iphone" bundle:nil];
    
    if (!IS_IPAD) {
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        rightSideMenuController.window = delegate.window;
        navController = [[UINavigationController alloc] initWithRootViewController:agencesViewController];
    }
    
    [navController setNavigationBarHidden:YES];
    
    self.sideMenu = [MFSideMenu menuWithNavigationController:navController
                                      leftSideMenuController:leftSideMenuController
                                     rightSideMenuController:rightSideMenuController];
    
    leftSideMenuController.sideMenu = self.sideMenu;
    rightSideMenuController.sideMenu = self.sideMenu;
    //
    self.window.rootViewController =self.sideMenu.navigationController ;
    self.window.rootViewController.view.frame=CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
    [self.window makeKeyAndVisible];
}

- (void) sideMenue {
    
    SlideViewController  *leftSideMenuController    =   nil;
    SlideViewController *rightSideMenuController    =   nil;
    AppDelegate *delegate                           =   [[UIApplication sharedApplication] delegate];
    UINavigationController * navController          =   nil;
    
    if (!IS_IPAD) {
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
        rightSideMenuController.window = delegate.window;
        navController =[[UINavigationController alloc] initWithRootViewController:[self newDashVC]];
    } else {
//        leftSideMenuController = [[SlideViewController_ipad alloc] initWithNibName:@"SlideViewController_ipad" bundle:nil];
//        rightSideMenuController = [[SlideViewController_ipad alloc] initWithNibName:@"SlideViewController_ipad" bundle:nil];
//        navController =[[UINavigationController alloc] initWithRootViewController:[self dashVC]];
    }
    //  UINavigationController * navController =[[UINavigationController alloc] initWithRootViewController:[self dashVC]];
    
    [navController setNavigationBarHidden:YES];
    
    self.sideMenu = [MFSideMenu menuWithNavigationController:navController
                                      leftSideMenuController:leftSideMenuController
                                     rightSideMenuController:rightSideMenuController];
    
    leftSideMenuController.sideMenu = self.sideMenu;
    rightSideMenuController.sideMenu = self.sideMenu;
    //
    self.window.rootViewController =self.sideMenu.navigationController ;
    self.window.rootViewController.view.frame=CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
    [self.window makeKeyAndVisible];
}
- (NewDashBoardVC_iphone *)newDashVC
{
    NewDashBoardVC_iphone *newDashVC  = [[NewDashBoardVC_iphone alloc] initWithNibName:@"NewDashBoardVC_iphone" bundle:nil ];
    [newDashVC hideAllSeprators:true];
    return newDashVC;
}

//- (DashboardViewController_ipad *)dashVC
//{
//    DashboardViewController_ipad * dashBoardViewController = [[DashboardViewController_ipad alloc]initWithNibName:@"DashboardViewController_ipad" bundle:nil];
//    
//    return dashBoardViewController;
//}
- (NewDashBoardVC_ipad *) dashNewVC
{
    NewDashBoardVC_ipad * dashNewViewcontroller = [[NewDashBoardVC_ipad alloc]initWithNibName:@"NewDashBoardVC_ipad" bundle:nil];
    return dashNewViewcontroller;
}

#pragma mark - Handle Remote Notifications

/*- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    if ([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]) {
        UIAlertView* alertShow=[[UIAlertView alloc] initWithTitle:@"MAGHREBAIL" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertShow show];
        [alertShow release];
    }
}*/

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
       
       /* UILocalNotification* localNotification = [[UILocalNotification lloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60];
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];*/
        
        //Show the view with the content of the push
        NSLog(@"app tapped");
        if ([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]) {
            
             self.popupiphone = [[PopupAlertViewController_iphone alloc]initWithNibName:@"PopupAlertViewController_iphone" bundle:nil];
            CGFloat height=[ self adaptecelllabelldyna:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] :280];
     
           self.popupiphone.textnotification=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
              [self.window.rootViewController.view addSubview:self.popupiphone.view];
            
            
//            if(self.popupiphone)
//            {
//                self.popupiphone.textnotification=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//                
//                [self.window.rootViewController.view addSubview:self.popupiphone.view];
//
//            
//            
//            }
//            else
//            {
//                self.popupiphone = [[PopupAlertViewController_iphone alloc]initWithNibName:@"PopupAlertViewController_iphone" bundle:nil];
//                
//                self.popupiphone.textnotification=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//                
//                [self.window.rootViewController.view addSubview:self.popupiphone.view];
//            
//            }
            
            
            
//         UIAlertView* alertShow=[[UIAlertView alloc] initWithTitle:@"MAGHREBAIL" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [alertShow show];
//             [alertShow release];
            
             
        }
      
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        //Refresh the local model
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        NSLog(@"Active");
        
        if ([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]) {
         
           
            
            
            
            if(self.popupiphone) {
                self.popupiphone.textnotification=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
                
                [self.window.rootViewController.view addSubview:self.popupiphone.view];
                
                
                
            } else {
                self.popupiphone = [[PopupAlertViewController_iphone alloc]initWithNibName:@"PopupAlertViewController_iphone" bundle:nil];
                
                self.popupiphone.textnotification=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
                
                [self.window.rootViewController.view addSubview:self.popupiphone.view];
                
            }
            
        

        }
        
        //Show an in-app banner
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
}


@end
