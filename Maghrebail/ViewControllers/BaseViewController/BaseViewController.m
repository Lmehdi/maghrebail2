//
//  BaseViewController.m
//  
//
//  Created by AHDIDOU on 19/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#import "BaseViewController.h"
#import "ContratsViewController.h"
#import "MaLigneCreditViewController.h"
#import "DetailPDFViewController.h"
#import "ParametresViewController_iphone.h"
#import "ParametresViewController_ipad.h"
#import "BlockAlertView.h"
#import "SlideViewController.h"
#import "DocumentVC.h"
#import "SplashViewController.h"
#import "PubliciteViewController.h"
#import "AppgroupeVC.h"

#define XBEGIN      20      //34
#define XEND        302     //286

@interface BaseViewController ()

@end

@implementation BaseViewController

@synthesize response = _response;
@synthesize messageView = _messageView;
@synthesize stringMessage = _stringMessage;
@synthesize tableView = _tableView;
@synthesize refreshHeaderView = _refreshHeaderView;
@synthesize shouldDisplayDrag;
@synthesize isMenuVisible;
@synthesize isFromButton;
@synthesize isFiltreViewShowed;
@synthesize strMinDate;
@synthesize strMaxDate;
@synthesize strMinMont;
@synthesize strMaxMont;
@synthesize isFromDash;
@synthesize dataunarchive;


#pragma mark --
#pragma mark -- Init Stuff

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    dataManager = [DataManager sharedManager];

    // Portrait
    
    isLandscape = NO; 
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];  
	BOOL deviceOrientation = NO;
 
    switch([UIDevice currentDevice].orientation)
    {
        case UIDeviceOrientationPortrait:
			
            deviceOrientation = NO;
			isLandscape = NO;
           
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
			
			deviceOrientation = NO;
			isLandscape = NO;
           
            break;
            
        case UIDeviceOrientationLandscapeLeft:

            deviceOrientation = YES;
			isLandscape = YES;
            
            break;
            
        case UIDeviceOrientationLandscapeRight:
			
			deviceOrientation = YES;
			isLandscape = YES;
            
			break;
			
		default:
			
			deviceOrientation = NO;
			isLandscape = NO;
            
			break;
	}
	
	dataManager.isLandScape = isLandscape;
	
    if (!IS_IPAD)
    {
        
        if ([DataManager isIphoneX]) {
            NSString *iphoneXNib = [NSString stringWithFormat:@"%@_x_p", nibNameOrNil];
            if (isLandscape)
            {
                iphoneXNib = [NSString stringWithFormat:@"%@_x_l", nibNameOrNil];
                
                if([[NSBundle mainBundle] pathForResource:iphoneXNib ofType:@"nib"] == nil)
                {
                    deviceOrientation = NO;
                    if([[NSBundle mainBundle] pathForResource:iphoneXNib ofType:@"nib"] != nil)
                    {
                        nibNameOrNil = [NSString stringWithFormat:@"%@_x_p", nibNameOrNil];
                    }
                    else
                    {
                        nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                    }
                }
                else
                {
                    nibNameOrNil = [NSString stringWithFormat:@"%@_x_l", nibNameOrNil];
                }
            }
            else
            {
                if([[NSBundle mainBundle] pathForResource:iphoneXNib ofType:@"nib"] != nil)
                {
                    nibNameOrNil = [NSString stringWithFormat:@"%@_x_p", nibNameOrNil];
                }
                else
                {
                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                }
            }
			
			
			
			
            
            
        }
        
        
        
        if ([DataManager isIphone6Plus])
        {
                        NSString *iphone5Nib = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
                        if (isLandscape)
                        {
                            iphone5Nib = [NSString stringWithFormat:@"%@6Plus_l", nibNameOrNil];
            
                            if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] == nil)
                            {
                                deviceOrientation = NO;
                                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
                                }
                                else
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                                }
                            }
                            else
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_l", nibNameOrNil];
                            }
                        }
                        else
                        {
                            if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
                            }
                            else
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                            }
                        }
            }
        
        
                    if ([DataManager isIphone6])
                    {
                        NSString *iphone6Nib = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
                        if (isLandscape)
                        {
                            iphone6Nib = [NSString stringWithFormat:@"%@6_l", nibNameOrNil];
        
                            if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] == nil)
                            {
        
                                deviceOrientation = NO;
                                if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] != nil)
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
                                }
                                else
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                                }
                            }
                            else
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@6_l", nibNameOrNil];
                            }
                        }
                        else
                        {
                            if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] != nil)
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
                            }
                            else
                            {
                                nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                            }
                        }
                    }
        
                        if ([DataManager isIphone5])
                        {
                            // code for 4-inch screen
                            NSString *iphone5Nib = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
                            if (isLandscape)
                            {
                                iphone5Nib = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
        
                                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] == nil)
                                {
        
                                    deviceOrientation = NO;
                                    if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
                                    {
                                        nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
                                    }
                                    else
                                    {
                                        nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                                    }
        
                                }
                                else
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
                                }
                            }
                            else
                            {
                                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
                                }
                                else
                                {
                                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
                                }
                            }
                        }

    
//        if ([DataManager isIphone6Plus])
//        {
//            NSString *iphone5Nib = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
//            if (isLandscape)
//            {
//                iphone5Nib = [NSString stringWithFormat:@"%@6Plus_l", nibNameOrNil];
//                
//                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] == nil)
//                {
//                    deviceOrientation = NO;
//                    if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                    {
//                        nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
//                    }
//                    else
//                    {
//                        nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                    }
//                }
//                else
//                {
//                    nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_l", nibNameOrNil];
//                }
//            }
//            else
//            {
//                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                {
//                    nibNameOrNil = [NSString stringWithFormat:@"%@6Plus_p", nibNameOrNil];
//                }
//                else
//                {
//                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                }
//            }
//            
//        }else{
//            
//            if ([DataManager isIphone6])
//            {
//                NSString *iphone6Nib = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
//                if (isLandscape)
//                {
//                    iphone6Nib = [NSString stringWithFormat:@"%@6_l", nibNameOrNil];
//                    
//                    if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] == nil)
//                    {
//                        
//                        deviceOrientation = NO;
//                        if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] != nil)
//                        {
//                            nibNameOrNil = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
//                        }
//                        else
//                        {
//                            nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                        }
//                    }
//                    else
//                    {
//                        nibNameOrNil = [NSString stringWithFormat:@"%@6_l", nibNameOrNil];
//                    }
//                }
//                else
//                {
//                    if([[NSBundle mainBundle] pathForResource:iphone6Nib ofType:@"nib"] != nil)
//                    {
//                        nibNameOrNil = [NSString stringWithFormat:@"%@6_p", nibNameOrNil];
//                    }
//                    else
//                    {
//                        nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                    }
//                }
//                
//            }else{
//                if ([DataManager isIphone5])
//                {
//                    // code for 4-inch screen
//                    NSString *iphone5Nib = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                    if (isLandscape)
//                    {
//                        iphone5Nib = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
//                        
//                        if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] == nil)
//                        {
//                            
//                            deviceOrientation = NO;
//                            if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                            {
//                                nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                            }
//                            else
//                            {
//                                nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                            }
//                            
//                        }
//                        else
//                        {
//                            nibNameOrNil = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
//                        }
//                    }
//                    else
//                    {
//                        if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                        {
//                            nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                        }
//                        else
//                        {
//                            nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                        }
//                    }
//                }else{
//                    if (!isLandscape)
//                        nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                    else
//                        nibNameOrNil = [NSString stringWithFormat:@"%@_l", nibNameOrNil];
//                }
//                
//            }}
    }
    

//                if (!IS_IPAD)
//                {
//                    if ([DataManager isIphone5])
//                    {
//                        // code for 4-inch screen
//                        NSString *iphone5Nib = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                        if (isLandscape)
//                        {
//                            iphone5Nib = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
//                            
//                            if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] == nil)
//                            {
//                                
//                                deviceOrientation = NO;
//                                if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                                {
//                                    nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                                }
//                                else
//                                {
//                                    nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                                }
//                                
//                            }
//                            else
//                            {
//                                nibNameOrNil = [NSString stringWithFormat:@"%@5_l", nibNameOrNil];
//                            }
//                        }
//                        else
//                        {
//                            if([[NSBundle mainBundle] pathForResource:iphone5Nib ofType:@"nib"] != nil)
//                            {
//                                nibNameOrNil = [NSString stringWithFormat:@"%@5_p", nibNameOrNil];
//                            }
//                            else
//                            {
//                                nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                            }
//                        }
//                    }else{
//                        if (!isLandscape)
//                            nibNameOrNil = [NSString stringWithFormat:@"%@_p", nibNameOrNil];
//                        else
//                            nibNameOrNil = [NSString stringWithFormat:@"%@_l", nibNameOrNil];
//                    }
//                }
      //  }

 //   }
    
    
   /*
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        
        
    } else {
        
        //if ([DataManager isIphone5]) {
            
            nibNameOrNil = [NSString stringWithFormat:@"%@_7", nibNameOrNil];
       // }
        
    }
    */
    

	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        shouldDisplayFooter = YES;
        isMenuVisible = NO;
        shouldDisplayDrag = YES;
        shouldDisplayLoadingView = YES;
		
        shouldAddHeader = YES;
        shouldDisplayLogo = YES;
        shouldDisplayTitre = NO;
		if (deviceOrientation && !IS_IPAD)
		{
			shouldAddHeader = NO;
		}
        shouldAutorate = NO;
		isFromButton = YES;
        self.isFromDash = NO;
        dataManager.shouldMainNavigationAutorate = shouldAutorate;
        [self startOrientationChangeDetection];
    }
    return self;
}

-(void)addHeader
{
    if (shouldAddHeader == YES)
    {
        if (headerView != nil)
        {
            //[headerView removeFromSuperview];
            //[headerView release];
            headerView = nil;
        }
        NSString *headerNibName = @"HeaderView_iphone_p";
        
        if (IS_IPAD) {
            headerNibName = @"HeaderView_iphone_old";
        }
        
        /*if (isLandscape)
        {
            headerNibName = @"HeaderView_iphone_l";
        }*/
        headerView = [[[NSBundle mainBundle]loadNibNamed:headerNibName owner:nil options:nil]objectAtIndex:0];
        headerView.delegate = self;
        
        /*if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
         
         
        */
        if ([DataManager isIphoneX])
            headerView.frame =  CGRectMake(0, 5.0, self.view.frame.size.width, headerView.frame.size.height);
        else
            headerView.frame =  CGRectMake(0, 0, self.view.frame.size.width, headerView.frame.size.height);
        
            
        /*} else {
            
             headerView.frame =  CGRectMake(0,20, self.view.frame.size.width, headerView.frame.size.height);
            
        }*/
        
        headerView.imgHeaderlogo.hidden=!shouldDisplayLogo;
        headerView.labelheaderTitre.hidden=!shouldDisplayTitre;
        headerView.labelheaderTitre.adjustsFontSizeToFitWidth=YES;
        if (self.isFromDash)
        {
            if (headerView) [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
        }
        
        [self.view addSubview:headerView];
    }
}

#pragma mark --
#pragma mark Rotation Handling
	 
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (IS_IPAD)
    {
        return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    return shouldAutorate;
}

-(BOOL)shouldAutorotate
{
    if (IS_IPAD) {
        return YES;
    }
    return shouldAutorate;
}

-(NSUInteger)supportedInterfaceOrientations { 
	//iOS 6 
	 
	 NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
	 
	 if (6 <= [[versionCompatibility objectAtIndex:0] intValue]) {
         if (IS_IPAD) {
             return 16 | 8 ;
            }
         return 2 | 16 | 8 | 4;
	 } else{
         if (IS_IPAD) {
             return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight ;
            }
         return UIInterfaceOrientationPortrait | UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationPortraitUpsideDown;
	 }
	
}

#pragma mark --
#pragma mark Device Orientation

-(void)startOrientationChangeDetection{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
}

-(void)stopOrientationChangeDetection{
    
}


- (void) orientationChanged:(NSNotification *)note
{

    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            
            isLandscape = NO;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            
            isLandscape = NO;
            
            break;
        case UIDeviceOrientationLandscapeLeft:
            
            isLandscape = YES;
            
            break;
        case UIDeviceOrientationLandscapeRight:
            
            isLandscape = YES;
            
            break;
            
        default:
            break;
    };
	
	dataManager.isLandScape = isLandscape;
    
    [self loadNibName];
}

-(void)loadNibName
{
    if (!IS_IPAD) {
        if (shouldAutorate)
        {
            NSString *nibNameOrientaion = self.nibName;
            
           /* if ([nibNameOrientaion hasSuffix:@"_7"]) {
                
                nibNameOrientaion = [nibNameOrientaion substringToIndex:[nibNameOrientaion length]-2];
                
            }*/
            if ([nibNameOrientaion hasSuffix:@"_p"] || [nibNameOrientaion hasSuffix:@"_l"])
            {
                nibNameOrientaion = [nibNameOrientaion substringToIndex:[nibNameOrientaion length]-2];
            }
            if (!isLandscape)
            {   nibNameOrientaion =[NSString stringWithFormat:@"%@_p", nibNameOrientaion];
                
                self.navigationController.sideMenu.recognizer.enabled = YES;
            }
            else
            {
				 nibNameOrientaion =[NSString stringWithFormat:@"%@_l", nibNameOrientaion];
                self.navigationController.sideMenu.recognizer.enabled = NO;
            }
            
          /*  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
                
                
            } else {
               // if ([DataManager isIphone5]) {
                    
                    nibNameOrientaion = [NSString stringWithFormat:@"%@_7", nibNameOrientaion];
                //}
                
            }*/
            
            
            [[NSBundle mainBundle] loadNibNamed:nibNameOrientaion owner:self options:nil];
            if (!isLandscape)
            {
                [self addHeader];
            }
            NSLog(@"NibName %@",nibNameOrientaion);
        }
		
        [self setUpPullToRefreshHeader];
    }
}

-(void)setUpPullToRefreshHeader{
    if (_tableView != nil && self.shouldDisplayDrag)
    {
        if (_refreshHeaderView == nil)
        {
			if (IS_IPAD)
			{
                if ([self isKindOfClass:[AppgroupeVC class]])
                {
                    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, -610, 300, 610)];
                }
                else
                    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, -610, 1024, 610)];
			}
			else
			{
				
				if (isLandscape || IS_IPAD)
				{
					_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.width, _tableView.frame.size.height, _tableView.bounds.size.width)];
				}
				else
					
					_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
				
			}
            _refreshHeaderView.delegate = self;
            [_tableView addSubview:_refreshHeaderView];
        }
        //  update the last update date
        [_refreshHeaderView refreshLastUpdatedDate];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];

   // self.view.backgroundColor = [UIColor colorWithRed:49.0/255 green:89.0/255 blue:138.0/255 alpha:1];
    [self addHeader];
         
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    [self setUpPullToRefreshHeader];
    /* if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        
        // Load resources for iOS 6.1 or earlier
        
    } else {
        
        for (UIView *view in [self.view subviews]) {
            CGRect frame = view.frame;
            
            view.frame = CGRectMake(frame.origin.x, frame.origin.y + 20, frame.size.width, frame.size.height);
        
        }
    }*/
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1 && ![self isKindOfClass:[SplashViewController class]] && ![self isKindOfClass:[PubliciteViewController class]])
        [self setNeedsStatusBarAppearanceUpdate];
}




- (void)viewDidAppear:(BOOL)animated
{
    dataManager.shouldMainNavigationAutorate = shouldAutorate;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   /*
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}*/
	
    
    
    if (self.isFromDash)
    {
        if (headerView)
            [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    }
    
    
	self.navigationController.sideMenu.recognizer.enabled = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)viewDidUnload
{
    
     [super viewDidUnload];
    [self setOverlayView:nil];
    [self setViewContener:nil];
    [self setImgViewTopBar:nil];
    [self setViewFiltre:nil];
    [self setViewTableView:nil];
    [self setBtnMinMontant:nil];
    [self setBtnMaxMontant:nil];
    [self setImgViewScrollMontnat:nil];
    [self setViewMinMontant:nil];
    [self setViewMaxMontant:nil];
    [self setLabelMinMontant:nil];
    [self setLabelMaxMontant:nil];
    [self setLabelMinDate:nil];
    [self setLabelMaxDate:nil];
    [self setViewMinDate:nil];
    [self setViewMaxDate:nil];
    [self setButtonMinDate:nil];
    [self setButtonMaxDate:nil];
    [self setImgViewScrollDate:nil];
    

}



-(void)dealloc
{

    [_refreshHeaderView release];
    [_tableView release];
    [_response release];
    [_viewContener release];
    [_imgViewTopBar release];
    [_overlayView release];
    [_viewFiltre release];
    [_viewTableView release];
    [_btnMinMontant release];
    [_btnMaxMontant release];
    [_imgViewScrollMontnat release];
    [_viewMinMontant release];
    [_viewMaxMontant release];
    [_labelMinMontant release];
    [_labelMaxMontant release];
    [_labelMinDate release];
    [_labelMaxDate release];
    [_viewMinDate release];
    [_viewMaxDate release];
    [_buttonMinDate release];
    [_buttonMaxDate release];
    [_imgViewScrollDate release];
    [super dealloc];
}

#pragma mark --
#pragma mark NetworkManagerDelegate
-(void)showProgressHUD:(BOOL)show{
	if ([self isKindOfClass:[DetailPDFViewController class]]) {
		return;
	}
    if (show) {
        MBProgressHUD * progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Chargement";
        progressHUD.mode = MBProgressHUDModeIndeterminate;
    }else{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

-(void)showProgresspdfHUD:(BOOL)show{
  
    if (show) {
        MBProgressHUD * progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Chargement";
        progressHUD.mode = MBProgressHUDModeIndeterminate;
    }else{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}


-(void)getRemoteContent:(NSString *)url
{
    [self removeMessageView];
    
    if (dataManager.isDemo)
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }

    else
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }
	
 
    
   
    [self showLoadingView:YES];
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
        networkManager.delegate = self;
       [networkManager doGet];
      
        
    }
    
    
}

-(void)getRemoteContentfournisseur:(NSString *)url
{
    [self removeMessageView];
    
  
    
    [self showProgressHUD:YES];
 
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
        networkManager.delegate = self;
        [networkManager doGet];
        
        
    }
    
    
}






-(void)getRemoteContentfournisseur:(NSString *)url andTag:(int)inputTag
{
    
    [self removeMessageView];
    

    [self showProgressHUD:YES];
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
        networkManager.tag=inputTag;
        
        networkManager.delegate = self;
        [networkManager doGet];
        
        
    }
    
}


-(void)getRemoteContent:(NSString *)url andTag:(int)inputTag
{

    [self removeMessageView];
    
    if (dataManager.isDemo)
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }

    else
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }
    
    
    [self showLoadingView:YES];
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
        networkManager.tag=inputTag;

        networkManager.delegate = self;
        [networkManager doGet];
        
        
    }

}


-(void)getRemoteContentAuth:(NSString *)url username:(NSString *)username password:(NSString *)pass  andTag:(int)inputTag
{
    [self removeMessageView];
    

    
    [self showProgressHUD:YES]; 
 //   [self showLoadingView:YES];
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
         networkManager.tag=inputTag;
        networkManager.delegate = self;
        [networkManager doGetAuthentification:username password:pass];
        
        
    }
    
    
}

-(void)getRemoteContentAuth:(NSString *)url username:(NSString *)username password:(NSString *)pass
{
    [self removeMessageView];
    
    
    
    [self showProgressHUD:YES];
    //   [self showLoadingView:YES];
    if ([dataManager shouldRefrech:url]) {
        NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
        networkManager.delegate = self;
        [networkManager doGetAuthentification:username password:pass];
        
        
    }
    
    
}
    
    
    
    






- (void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params sender:(id)sender
{
    [self removeMessageView];
	
	if (dataManager.isDemo)
	{
		if ([url rangeOfString:@"action"].location == NSNotFound) {
			url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
		} else {
			url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
		}
	}
	
    NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
    networkManager.delegate = self;
    [networkManager doPost:params];
}

- (void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params
{
	if (dataManager.isDemo)
	{ 
		if ([url rangeOfString:@"action"].location == NSNotFound) {
			url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
		} else {
			url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
		}
	}
    
    [self removeMessageView];
    [self showProgressHUD:YES];
    NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
    networkManager.delegate = self;
    [networkManager doPost:params];
}
- (void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params andTag:(int) inputTag
{
    if (dataManager.isDemo)
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }
    
    [self removeMessageView];
    [self showProgressHUD:YES];
    NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
    networkManager.tag=inputTag;
    networkManager.delegate = self;
    [networkManager doPost:params];
}
- (void)postDataToServere:(NSString *)url andParam:(NSMutableDictionary *)params andTag:(int) inputTag
{
    if (dataManager.isDemo)
    {
        if ([url rangeOfString:@"action"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        } else {
            url = [NSString stringWithFormat:@"%@&demo=true&token=6b7761d25ded57704372fb7f74e2dc50",url];
        }
    }
    
    [self removeMessageView];
   // [self showProgressHUD:YES];
    NetworkManager * networkManager = [[NetworkManager alloc]initWithURL:url];
    networkManager.tag=inputTag;
    networkManager.delegate = self;
    [networkManager doPost:params];
}



-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark --
#pragma mark NetworkManagerDelegate

-(void)started:(NetworkManager *)networkManager
{
    if (shouldDisplayLoadingView) {
        [self showLoadingView:YES];
        
    }
}

-(void)finished:(NetworkManager *)networkManager result:(NSMutableDictionary *)result{
    [self showProgressHUD:NO];
    
   self.response = [[NSMutableDictionary alloc]initWithDictionary:result];
    
    [self updateView];
    [self updateView:networkManager];
    [self updateViewTag:networkManager.tag];
    
}

-(void)gotError:(NetworkManager *)networkManager
{
      [self showProgressHUD:NO];
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.0];
    [BaseViewController showAlert:nil message:ERROR_LOAGIND delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    [self displayMessageView:ERROR_LOAGIND];
    [self showLoadingView:NO];
}

#pragma mark --
#pragma mark Updating View

-(void)removeMessageView{
    
    if (_messageView !=nil) {
        [_messageView removeFromSuperview];
        [_messageView release];
        _messageView = nil;
    }
}

-(void)displayMessageView:(NSString *)message {
    [self removeMessageView];
    if (_tableView !=nil)
    {
        [self.tableView reloadData];
        _messageView = [[MessageView alloc]initWithFrame:_tableView.frame];
        _messageView.messageLabel.text = message;
        [self.viewContener addSubview:_messageView];
    }
}

-(void)updateViewTag:(int)inputTag {
    
    
    if ([[[self.response objectForKey:@"header"]objectForKey:@"status"] isEqualToString:@"NOK"])
    {
        
        return;
    }
    
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.0];
    [self showLoadingView:NO];
    [self removeMessageView];
    
    
    responseObjectsCount = [[self.response objectForKey:@"response"]count];
    if (responseObjectsCount == 0)
    {
        [self displayMessageView:self.stringMessage];
    }
}

-(void)updateView:(id)sender{
    
    
    if ([[[self.response objectForKey:@"header"]objectForKey:@"status"] isEqualToString:@"NOK"])
    {
        
        return;
    }
   
    
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.0];
    [self showLoadingView:NO];
    [self removeMessageView];
	 

    responseObjectsCount = [[self.response objectForKey:@"response"]count];
    if (responseObjectsCount == 0)
    {
        [self displayMessageView:self.stringMessage];
    }
}
-(void)updateView{
    
	if ([[[self.response objectForKey:@"header"]objectForKey:@"status"] isEqualToString:@"NOK"])
    {
        
        [BaseViewController showAlert:nil message:[[self.response objectForKey:@"header"]objectForKey:@"message"] delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        
        
        return;
    }
	
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.0];
    [self showLoadingView:NO];
    [self removeMessageView];
    responseObjectsCount = [[self.response objectForKey:@"response"]count];
    if (responseObjectsCount == 0)
    {
        [self displayMessageView:self.stringMessage];
    }
    
    //for filtre view
    valuesX = [[NSMutableArray alloc] init];
    int pas = (XEND - XBEGIN)/100;
    for (int i = XBEGIN; i <= XEND; i = i + pas)
    {
        [valuesX addObject:[NSNumber numberWithInt:i]];
    }
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
    shouldDisplayLoadingView = NO;
	_reloading = YES;
    
}

- (void)doneLoadingTableViewData{
	//  model should call this when its done loading
	_reloading = NO;
    shouldDisplayLoadingView = YES;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
    if (_tableView!=nil) {
        
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
    if (_tableView!=nil) {
        
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
	
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	
	
}




- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark --
#pragma mark Other Stuff

-(void)showLoadingView:(BOOL)show{
    if (show)
    {
        self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x , -self.loadingView.frame.size.height, self.loadingView.frame.size.width, self.loadingView.frame.size.height);
        [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelay:0];
        self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x , 0, self.loadingView.frame.size.width, self.loadingView.frame.size.height);
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelay:0];
        self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x , -self.loadingView.frame.size.height, self.loadingView.frame.size.width, self.loadingView.frame.size.height);
        [UIView commitAnimations];
    }
}

+(void)showAlert:(NSString *)title message:(NSString *)message delegate:(id)delegate confirmBtn:(NSString * )confirmBtn cancelBtn:(NSString *)cancelBtn{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:delegate cancelButtonTitle:confirmBtn otherButtonTitles:cancelBtn, nil];
	
    [alert show];
    [alert release];
}

-(IBAction)backToParent:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadImageFromURL:(NSString *)url view:(UIView *)view frame:(CGRect )frame{
    AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:frame];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    // imageView.tag = IMAGE_VIEW_TAG;
    [view addSubview:imageView];
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
    //NSString * stringURL = [NSString stringWithFormat:@"http://94.23.217.222/clients/meditel/bo/web/uploads/images/%@",[[banners objectAtIndex:i]objectForKey:@"image"]];
    NSURL * imageURL = [NSURL URLWithString:url];
    imageView.imageURL =  imageURL;
}

#pragma mark --
#pragma mark Show Params


#pragma mark --
#pragma mark Cache

-(void)readFromCache:(NSString *)url{
    NSURL * urlTemp = [NSURL URLWithString:url];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [[ASIDownloadCache sharedCache] setShouldRespectCacheControlHeaders:NO];
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
    [request setCachePolicy:ASIFallbackToCacheIfLoadFailsCachePolicy];
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    [request setSecondsToCache:60*60*24*30];
    [request setTimeOutSeconds:300];
    NSString *pathCach = [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:request];
    NSString * cachedData = [NSString stringWithContentsOfFile:pathCach encoding:NSUTF8StringEncoding error:nil];
    if (cachedData) {
        DataProcessManager * dataProcessManager = [[DataProcessManager alloc]initWithJson:cachedData];
        dataProcessManager.delegate = self;
        [dataProcessManager processLoginJson];
    }
}
#pragma mark --
#pragma mark DataProcessManager Delegate
-(void)finishedParssing:(DataProcessManager *)dataProcessManager response:(NSDictionary *)response{
    
    self.response = [NSMutableDictionary dictionaryWithDictionary:response];
    
}

#pragma mark --
#pragma mark Toggle Menu

-(IBAction)toggleLeftMenu:(id)sender{
    if (!IS_IPAD) {
     [self.navigationController.sideMenu toggleLeftSideMenu];
    }
   
}

#pragma mark --
#pragma mark HeaderSideMenu Delegate

-(void)showSideMenu
{
    if (self.isFromDash)
    {

        [dataManager.slideMenu shoDashBoard:nil];
        return;
    }
    if (IS_IPAD) {
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    }else{
         [self.navigationController.sideMenu toggleLeftSideMenu];
    }
}



- (void)showSideOptions
{    
	if (!dataManager.authentificationData )
	{
		[Flurry logEvent:@"Paramétres"];
		if (!IS_IPAD) {
			ParametresViewController_iphone *view = [[ParametresViewController_iphone alloc] initWithNibName:@"ParametresViewController_iphone" bundle:nil]; 
			[self.navigationController pushViewController:view animated:YES];
		}
		else
		{ 
			ParametresViewController_ipad *view = [[ParametresViewController_ipad alloc] initWithNibName:@"ParametresViewController_ipad" bundle:nil];
			[self.navigationController pushViewController:view animated:YES];
		}
        
	}
	else
	{
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"deconnexionProfil"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
		[[NSNotificationCenter defaultCenter] postNotificationName:@"showMenu" object:nil];
		[Flurry logEvent:@"Deconnexion"];
		alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes déconnectés"];
		[alert show];
        dataManager.handleauthentificationconnect=NO;
       // [dataManager.tablechangementtiers removeAllObjects];
        dataManager.showdemobutton=NO;
        dataManager.indexselected=-1;
        dataManager.isFournisseur=NO;
        dataManager.isClient=NO;
        dataManager.isClientContent=NO;
        dataManager.isFournisseurContent=NO;
        
		
		[self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
        
	}
}


-(void)dismissAlert
{
	[alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
 
    if (buttonIndex == [actionSheet destructiveButtonIndex]) {
        // destroy something
		dataManager.isAuthentified = NO;
		NSData *dataAuthentification= [NSKeyedArchiver archivedDataWithRootObject:nil];
		[DataManager writeDataIntoCachWith:dataAuthentification andKey:@"authentificationData"];
		dataManager.authentificationData = nil;
		[[NSNotificationCenter defaultCenter] postNotificationName:@"showMenu" object:nil];
		if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];	 
    }
	else
	{
		NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
		if ([choice isEqualToString:@"Déconnexion Automatique"])
		{
			if (standardUserDefaults) {
				[standardUserDefaults setObject:@"OFF" forKey:@"USER-CONNEXION"];
                
				[standardUserDefaults synchronize];
				dataManager.isStayOn = NO;
			}
			
		}
		else if ([choice isEqualToString:@"Rester Connecté"])
		{
			if (standardUserDefaults)
			{
				[standardUserDefaults setObject:@"ON" forKey:@"USER-CONNEXION"];
				[standardUserDefaults synchronize];
				dataManager.isStayOn = YES;
			}
			
		}
	}
}

#pragma mark - Cell
+(UITableViewCell * )getCellWithIdentifier:(NSString *)identifier{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil];
    return  [nib objectAtIndex:0];
}

#pragma mark - Filtre Action

- (IBAction)filtrePushed:(UIButton *)sender
{
    int originView=152;
    
    if (isFiltreViewShowed)
    { 
		self.navigationController.sideMenu.recognizer.enabled = YES;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:@"ScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(filtreFrame.origin.x,filtreFrame.origin.y,mainScreen_width, 0);
        [UIView commitAnimations];
        self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x,originView, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        self.viewFiltre.frame = filtreFrame;
        isFiltreViewShowed = NO;
    } else {
		self.navigationController.sideMenu.recognizer.enabled = NO;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:@"ScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(filtreFrame.origin.x,originView,mainScreen_width, 198);
        [UIView commitAnimations];
        self.viewFiltre.frame = filtreFrame;
        self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, originView+filtreFrame.size.height, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height - self.viewFiltre.frame.size.height);
        isFiltreViewShowed = YES;
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (IBAction)SearchFilterPushed:(UIButton *)sender
{
     int originView=152;
    if (isFiltreViewShowed)
    {
        self.navigationController.sideMenu.recognizer.enabled = YES;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:@"ScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(filtreFrame.origin.x,filtreFrame.origin.y,mainScreen_width, 0);
        [UIView commitAnimations];
        self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x,originView, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        self.viewFiltre.frame = filtreFrame;
        isFiltreViewShowed = NO;
    }
    else
    {
        self.navigationController.sideMenu.recognizer.enabled = NO;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:@"ScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(filtreFrame.origin.x,originView,mainScreen_width, 60);
        [UIView commitAnimations];
        self.viewFiltre.frame = filtreFrame;
        self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, originView+filtreFrame.size.height, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height - self.viewFiltre.frame.size.height);
        isFiltreViewShowed = YES;
    }

    
    
}

- (IBAction)btnback:(UIButton *)sender
{
     int originView=152;
    
    if (isFiltreViewShowed)
    {
        self.navigationController.sideMenu.recognizer.enabled = YES;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:@"ScaleAnimation" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(filtreFrame.origin.x,filtreFrame.origin.y,mainScreen_width, 0);
        [UIView commitAnimations];
        self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x,originView, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        self.viewFiltre.frame = filtreFrame;
        isFiltreViewShowed = NO;
    }
}



- (void)minScrollDragged:(UIButton *)button withEvent:(UIEvent *)event
{
	// get the touch 
	UITouch *touch = [[event touchesForView:button] anyObject];
    
	// get delta
	CGPoint previousLocation = [touch previousLocationInView:button];
	CGPoint location = [touch locationInView:button];
	CGFloat delta_x = location.x - previousLocation.x;
    [self minScrollMoved:button :delta_x];
}

- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event{ 
}

- (void)minScrollMoved:(UIButton *)button :(CGFloat)delta_x
{
    NSArray *values = [valuesMontant copy];
    UILabel *labelMin = self.labelMinMontant;
    UILabel *labelMax = self.labelMaxMontant;
    UIButton *btnMin = self.btnMinMontant;
    UIButton *btnMax = self.btnMaxMontant;
    UIView *viewMin = self.viewMinMontant;
    UIView *viewMax = self.viewMaxMontant;
    UIImageView *imgViewScroll = self.imgViewScrollMontnat;
    if (button.tag == 1)
    {
        values = [valuesDates copy];
        labelMin = self.labelMinDate;
        labelMax = self.labelMaxDate;
        btnMin = self.buttonMinDate;
        btnMax = self.buttonMaxDate;
        viewMin = self.viewMinDate;
        viewMax = self.viewMaxDate;
        imgViewScroll = self.imgViewScrollDate;
    }
    CGFloat newCenter_x = button.center.x + delta_x;
	// move button
    if (newCenter_x > XBEGIN && newCenter_x <= XEND && (newCenter_x < btnMax.center.x - viewMax.frame.size.width/1.3))
    {
        button.center = CGPointMake(newCenter_x, button.center.y);
    }
    float noteValue = (button.center.x * 12)/XBEGIN;
    
	for (int i = 0; i < valuesX.count; i++)
    {
        NSNumber *theX = [valuesX objectAtIndex:i];
        if (button.center.x <= [theX intValue])
        {
            NSString *theD = @"";
            
            if (i >= [values count])
            {
                theD = [values objectAtIndex:[values count]-1];
            }
            else
            {
                theD = [values objectAtIndex:i/*-1*/];
            }
            noteValue = theD.intValue;
            break;
        }
    }
    /*if (noteValue >= 24)
     {
     noteValue = 24;
     }
     if (noteValue <= 12)
     {
     noteValue = 12;
     }*/
    if (button.tag == 0)
    {
        [labelMin setText:[DataManager getFormatedNumero2:[NSString stringWithFormat:@"%.0f",noteValue]]];
    }
    else if (button.tag == 1)
    {
        [labelMin setText:[DataManager timeIntervaleToString:noteValue]];
    }
    CGRect frameScroll = imgViewScroll.frame;
    frameScroll = CGRectMake(button.center.x, frameScroll.origin.y, btnMax.center.x - button.center.x, frameScroll.size.height);
    imgViewScroll.frame = frameScroll;
    viewMin.center = CGPointMake(button.center.x, viewMin.center.y);
    
    [self changeTableViewData:button.tag];
}

- (void)maxScrollDragged:(UIButton *)button withEvent:(UIEvent *)event
{
	// get the touch 
	UITouch *touch = [[event touchesForView:button] anyObject];
    
	// get delta
	CGPoint previousLocation = [touch previousLocationInView:button];
	CGPoint location = [touch locationInView:button];
	CGFloat delta_x = location.x - previousLocation.x;
    [self maxScrollMoved:button :delta_x];
}

- (void)maxScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event{ 
}

- (void)maxScrollMoved:(UIButton *)button :(CGFloat)delta_x
{
    NSArray *values = [valuesMontant copy];
    UILabel *labelMin = self.labelMinMontant;
    UILabel *labelMax = self.labelMaxMontant;
    UIButton *btnMin = self.btnMinMontant;
    UIButton *btnMax = self.btnMaxMontant;
    UIView *viewMin = self.viewMinMontant;
    UIView *viewMax = self.viewMaxMontant;
    UIImageView *imgViewScroll = self.imgViewScrollMontnat;
    if (button.tag == 1)
    {
        values = [valuesDates copy];
        labelMin = self.labelMinDate;
        labelMax = self.labelMaxDate;
        btnMin = self.buttonMinDate;
        btnMax = self.buttonMaxDate;
        viewMin = self.viewMinDate;
        viewMax = self.viewMaxDate;
        imgViewScroll = self.imgViewScrollDate;
    }
    CGFloat newCenter_x = button.center.x + delta_x;
	// move button
    if (newCenter_x > XBEGIN && newCenter_x <= XEND && (newCenter_x > (btnMin.center.x + viewMin.frame.size.width/1.3)))
    {
        button.center = CGPointMake(newCenter_x, button.center.y);
    }
    float noteValue = (button.center.x * 12)/XBEGIN;
    
	for (int i = 0; i < valuesX.count; i++)
    {
        NSNumber *theX = [valuesX objectAtIndex:i];
        if (button.center.x <= [theX intValue])
        {
            NSString *theD = @"";
            
            if (i >= [values count])
            {
                theD = [values objectAtIndex:[values count]-1];
            }
            else
            {
                theD = [values objectAtIndex:i-1];
            }
            noteValue = theD.intValue;
            break;
        }
    }
    /*if (noteValue >= 24)
     {
     noteValue = 24;
     }
     if (noteValue <= 12)
     {
     noteValue = 12;
     }*/
    if (button.tag == 0)
    {
        [labelMax setText:[DataManager getFormatedNumero2:[NSString stringWithFormat:@"%.0f",noteValue]]];
    }
    else if (button.tag == 1)
    {
        [labelMax setText:[DataManager timeIntervaleToString:noteValue]];
    }
    CGRect frameScroll = imgViewScroll.frame;
    frameScroll = CGRectMake(btnMin.center.x, frameScroll.origin.y, button.center.x - btnMin.center.x, frameScroll.size.height);
    imgViewScroll.frame = frameScroll;
    viewMax.center = CGPointMake(button.center.x, viewMax.center.y);
    [self changeTableViewData:button.tag];
}

-(void)changeTableViewData:(int)typeScroll
{
    NSDate *dateEffet =nil;
    NSDate *dateMin =nil;
    NSDate *dateMax =nil;
    UILabel *labelMin = self.labelMinMontant;
    UILabel *labelMax = self.labelMaxMontant;
    UILabel *labelMinDate = self.labelMinDate;
    UILabel *labelMaxDate = self.labelMaxDate;
    NSMutableArray *newListContrat = [NSMutableArray array];
    NSMutableArray *contratList = dataManager.contrats;
    
    if ([self isKindOfClass:[MaLigneCreditViewController class]])
    {
        contratList = dataManager.ligneCredit;
        keyMontant = [NSString stringWithFormat:@"MF"];
        keyDate = [NSString stringWithFormat:@"DATE_DEBUT"];
        
    }
    else if ([self isKindOfClass:[MesReglementsViewController class]])
    {
        contratList = dataManager.reglements;
        
        keyMontant = [NSString stringWithFormat:@"MT_HT"];
        keyDate = [NSString stringWithFormat:@"DATE_MOUVEMENT"];
        
    }
    else if ([self isKindOfClass:[MesFacturesViewController class]])
    {
        contratList = dataManager.factures;
        
        keyMontant = [NSString stringWithFormat:@"MT_HT"];
        keyDate = [NSString stringWithFormat:@"DATE_MOUVEMENT"];
        
    }
    
    
    else if ([self isKindOfClass:[DocumentVC class]])
    {
        contratList = dataManager.mesDocuments;
        keyMontant = [NSString stringWithFormat:@"MFHT"];
        keyDate = [NSString stringWithFormat:@"DATE_EFFET"];
    }
    
    else if ([self isKindOfClass:[MesImmatriculationsViewController class]])
    {
        contratList = dataManager.dataimmatriculation;
        NSLog(@" the first count is %lu",(unsigned long)[dataManager.dataimmatriculation count]);


        keyMontant = [NSString stringWithFormat:@"MT_HT_CMDE"];
        keyDate = [NSString stringWithFormat:@"DATE_COMMANDE"];
    }
    else if ([self isKindOfClass:[MesCommandesViewController class]])
    {
        contratList = dataManager.commandes;
        NSLog(@" the first count is %lu",(unsigned long)[dataManager.commandes count]);
        
        
        keyMontant = [NSString stringWithFormat:@"MT_HT_CMDE"];
        keyDate = [NSString stringWithFormat:@"DATE_COMMANDE"];
    }
    if (contratList.count > 0)
    {
        [self removeMessageView];
        
        for (NSDictionary *contra in contratList)
        {
            
            /*if ([[contra objectForKey:@"TTC"] doubleValue] >= labelMin.text.doubleValue && [[contra objectForKey:@"TTC"] doubleValue] <= labelMax.text.doubleValue && ([[DataManager stringToDtae:[contra objectForKey:@"DATE_EFFET"]] compare:[DataManager stringToDtae:labelMin.text]] == NSOrderedDescending) && ([[DataManager stringToDtae:[contra objectForKey:@"DATE_EFFET"]] compare:[DataManager stringToDtae:labelMax.text]] == NSOrderedAscending))
             {
             [newListContrat addObject:contra];
             }*/
            /*
             if (typeScroll == 0)
             {*/
            
            NSString *strMnHT = [[contra objectForKey:keyMontant] stringByReplacingOccurrencesOfString:@" " withString:@""];
            strMnHT = [strMnHT stringByReplacingOccurrencesOfString:@"," withString:@"."];
            double mnHT = [strMnHT doubleValue];
            NSLog(@"the value of the double is %f",mnHT);
            
            NSString *strMnHTMIN = [labelMin.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            strMnHTMIN = [strMnHTMIN stringByReplacingOccurrencesOfString:@"," withString:@"."];
            double mnMIN = [strMnHTMIN doubleValue];
            
            NSString *strMnHTMAX = [labelMax.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            strMnHTMAX = [strMnHTMAX stringByReplacingOccurrencesOfString:@"," withString:@"."];
            double mnMAX = [strMnHTMAX doubleValue];
            if(dataManager.isFournisseurContent || dataManager.DemoredirectfactureFournisseur)
            {
                dateEffet = [DataManager stringToDtae:[DataManager getDateStringFromPresentableDate:[contra objectForKey:keyDate]]];
                dateMin = [DataManager stringToDtae:labelMinDate.text];
                dateMax = [DataManager stringToDtae:labelMaxDate.text];
            }
            else
            {
                dateEffet = [DataManager stringToDtae:[contra objectForKey:keyDate]];
                dateMin = [DataManager stringToDtae:labelMinDate.text];
                dateMax = [DataManager stringToDtae:labelMaxDate.text];
            
            }
            
            

            
            if (mnHT >= mnMIN && mnHT <= mnMAX)
            {
                
                if ( [dateEffet compare:dateMax] == NSOrderedAscending)
                {
                    
                    if ([dateEffet compare:dateMin]== NSOrderedDescending )
                    {
                        [newListContrat addObject:contra];
                    }
                }
            }
            /* }
             else if (typeScroll == 1)
             {
             if (([[DataManager stringToDtae:[contra objectForKey:@"DATE_EFFET"]] compare:[DataManager stringToDtae:labelMin.text]] == NSOrderedDescending) && ([[DataManager stringToDtae:[contra objectForKey:@"DATE_EFFET"]] compare:[DataManager stringToDtae:labelMax.text]] == NSOrderedAscending))
             {
             [newListContrat addObject:contra];
             }
             }*/
        }
        dataManager.contratFiltred = [NSMutableArray arrayWithArray:newListContrat];
        if ([self isKindOfClass:[MaLigneCreditViewController class]])
        {
            dataManager.ligneCreditFiltred = [NSMutableArray arrayWithArray:newListContrat];
        }
        else if ([self isKindOfClass:[MesImmatriculationsViewController class]])
        {
            dataManager.immatriculationFiltred = [NSMutableArray arrayWithArray:newListContrat];
            NSLog(@" the count is %lu",(unsigned long)[dataManager.immatriculationFiltred count]);
        }
        
        else if ([self isKindOfClass:[MesCommandesViewController class]])
        {
            dataManager.commandeFiltred = [NSMutableArray arrayWithArray:newListContrat];
            NSLog(@" the count is %lu",(unsigned long)[dataManager.commandeFiltred count]);
        }
        else if ([self isKindOfClass:[MesFacturesViewController class]])
        {
            dataManager.FiltredFacture = [NSMutableArray arrayWithArray:newListContrat];
            NSLog(@" the count is %lu",(unsigned long)[dataManager.FiltredFacture count]);
        }
       
        
        else if ([self isKindOfClass:[MesReglementsViewController class]])
        {
            dataManager.reglementsFiltred = [NSMutableArray arrayWithArray:newListContrat];
            NSLog(@" the count is %lu",(unsigned long)[dataManager.reglementsFiltred count]);
        }
        else if ([self isKindOfClass:[DocumentVC class]])
        {
            dataManager.mesDocumentsFiltred = [NSMutableArray arrayWithArray:newListContrat];
        }
        
        
        
        
    }

    else
    {
        [self displayMessageView:kERROR_CINEMA_HORAIRE];
    }
    [_tableView reloadData];

}

#pragma mark --
#pragma mark other Stuff

-(void)adjustLabelHeight:(UILabel * )label string :(NSString *)string{
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
    
    CGSize expectedLabelSize = [string sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    
    //adjust the label the the new height.
    
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height;
    label.frame = newFrame;
    
}
- (float) getCellHeightForTextLandscape:(NSString*) string {
    UILabel* label      =   [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 76, 150)];
    [label setFont:[UIFont boldSystemFontOfSize:11]];
    [label setNumberOfLines:0];
    [label setText:string];
    
    [label sizeToFit];
    
    return label.frame.size.height + 16 ;
    
}


- (float) getCellHeightForTextPortrait:(NSString*) string {
    UILabel* label      =   [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 75, 150)];
    [label setFont:[UIFont boldSystemFontOfSize:11]];
    // [label setTextAlignment:NSTextAlignmentRight];
    [label setNumberOfLines:0];
    [label setText:string];
    
    [label sizeToFit];
   return label.frame.size.height + 16;
}


- (float) getCellHeightForTextNomTiers:(NSString*) string {
    UILabel* label      =   [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 100)];
    [label setFont:[UIFont boldSystemFontOfSize:11]];
    [label setNumberOfLines:0];
     [label setTextAlignment:NSTextAlignmentRight];
    [label setText:string];
    
    [label sizeToFit];
    
    return label.frame.size.height + 24;
}



-(CGFloat )adaptecelllabell:(NSString *)sts :(CGFloat ) wid :(CGFloat) y{
    //    CGSize constraint = CGSizeMake(wid, 20000.0f);
    
    UILabel* label      =   [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, y, wid, 0)];
    [label setNumberOfLines:0];
    [label setFont:[UIFont boldSystemFontOfSize:11]];
    [label setText:sts];
    [label setTextAlignment:NSTextAlignmentRight];
    [label sizeToFit];
    
    return CGRectGetMaxY(label.frame);
    
}





@end
