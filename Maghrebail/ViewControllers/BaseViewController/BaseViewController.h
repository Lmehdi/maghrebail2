//
//  BaseViewController.h
//  
//
//  Created by AHDIDOU on 19/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "NetworkManager.h"
#import "AsyncImageView.h"
#import "DataManager.h"
#import "MessageView.h"
#import "EGORefreshTableHeaderView.h"
#import "ASIDownloadCache.h"
#import "DataProcessManager.h"
#import "MFSideMenu.h"
#import "CHMFSideMenu.h"
#import "MBProgressHUD.h"
#import "HeaderView.h"
#import "BlockAlertView.h"
#import "Flurry.h"


@interface BaseViewController : UIViewController<NetworkManagerDelegate,EGORefreshTableHeaderDelegate,DataProcessManagerDelegate, UITextFieldDelegate, HeaderViewSideMenuDelegate, UIActionSheetDelegate>
{
      
    NSMutableDictionary * _response;
    BOOL shouldDisplayFooter ;
    NSInteger responseObjectsCount;
    BOOL isMenuVisible;
    
    //Exception Handling
    MessageView * _messageView;
    NSString * _stringMessage;
    
    //DragToRefresh
    BOOL shouldDisplayDrag;
    BOOL shouldDisplayLoadingView;
    BOOL isLandscape;
    
    //Header
    BOOL shouldAddHeader;
    BOOL shouldDisplayLogo;
    BOOL shouldDisplayTitre;
    
    HeaderView * headerView;
    
    
    //Filtre
    NSString *keyMontant;
    NSString *keyDate;
    BOOL isFiltreViewShowed;
    NSMutableArray *valuesMontant;
    NSMutableArray *valuesDates;
    NSMutableArray *valuesX;
    int maxMontantValue;
    int minMontantValue;
    int maxDateValue;
    int minDatetValue;
    
    IBOutlet UITableView *_tableView;
    EGORefreshTableHeaderView * _refreshHeaderView;
    BOOL _reloading;
    
    //autorate
    BOOL shouldAutorate;
    
    DataManager *dataManager;
 
	BOOL btnSelectedEncours;
    BOOL btnSelectedRegles;
    BOOL btnSelectedClotures;
	BOOL btnSelectedTerminee;
    
    BOOL shouldDisableOffscreenMenu;
	
	BlockAlertView *alert;
}

@property (retain,strong) NSMutableDictionary *response;  
@property (retain, nonatomic) IBOutlet UIView *viewContener;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewTopBar;
@property (retain, nonatomic) IBOutlet UIView *overlayView;
@property (retain, nonatomic) MessageView * messageView;
@property (retain, nonatomic) NSString  * stringMessage;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) EGORefreshTableHeaderView * refreshHeaderView;
@property (assign) BOOL shouldDisplayDrag;
@property (assign) BOOL isMenuVisible;
@property (retain, nonatomic) IBOutlet UIView *loadingView;
//filtre
@property (assign, nonatomic) BOOL isFiltreViewShowed;
@property (retain, nonatomic) IBOutlet UIView *viewFiltre;
@property (retain, nonatomic) IBOutlet UIView *viewTableView;
@property (retain, nonatomic) IBOutlet UIButton *btnMinMontant;
@property (retain, nonatomic) IBOutlet UIButton *btnMaxMontant;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewScrollMontnat;
@property (retain, nonatomic) IBOutlet UIView *viewMinMontant;
@property (retain, nonatomic) IBOutlet UIView *viewMaxMontant;
@property (retain, nonatomic) IBOutlet UILabel *labelMinMontant;
@property (retain, nonatomic) IBOutlet UILabel *labelMaxMontant;
@property (retain, nonatomic) IBOutlet UILabel *labelMinDate;
@property (retain, nonatomic) IBOutlet UILabel *labelMaxDate;
@property (retain, nonatomic) IBOutlet UIView *viewMinDate;
@property (retain, nonatomic) IBOutlet UIView *viewMaxDate;
@property (retain, nonatomic) IBOutlet UIButton *buttonMinDate;
@property (retain, nonatomic) IBOutlet UIButton *buttonMaxDate;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewScrollDate;
@property (assign, nonatomic) BOOL isFromButton;
@property (assign, nonatomic) BOOL isFromDash;
@property (retain, nonatomic) NSString *strMinDate;
@property (retain, nonatomic) NSString *strMaxDate;
@property (retain, nonatomic) NSString *strMinMont;
@property (retain, nonatomic) NSString *strMaxMont;
@property (retain, nonatomic) NSMutableArray* dataunarchive;
-(void)showLoadingView:(BOOL)show;
-(void)updateView;
-(void)updateView:(id)sender;
-(void)updateViewTag:(int)inputTag;
-(void)getRemoteContent:(NSString *)url;
-(void)getRemoteContentfournisseur:(NSString *)url;
-(void)getRemoteContent:(NSString *)url andTag:(int) inputTag;
-(void)getRemoteContentfournisseur:(NSString *)url andTag:(int)inputTag;
-(void)getRemoteContentAuth:(NSString *)url username:(NSString *)username password:(NSString *)pass;

- (void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params andTag:(int) inputTag;
- (void)postDataToServere:(NSString *)url andParam:(NSMutableDictionary *)params andTag:(int) inputTag;
-(void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params;
-(void)postDataToServer:(NSString *)url andParam:(NSMutableDictionary *)params sender:(id)sender;
-(IBAction)backToParent:(id)sender;
+(void)showAlert:(NSString *)title message:(NSString *)message delegate:(id)delegate confirmBtn:(NSString * )confirmBtn cancelBtn:(NSString *)cancelBtn;

-(void)loadImageFromURL:(NSString *)url view:(UIView *)view frame:(CGRect )frame;
//Message View
-(void)savepdf:(NSString *) pdfname :(NSString *) pdfurl;
-(void)removeMessageView;
-(void)displayMessageView:(NSString *)message;

//DragToRefresh
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
-(IBAction)toggleLeftMenu:(id)sender;
-(void)getRemoteContentAuth:(NSString *)url username:(NSString*)username password:(NSString*)pass andTag:(int)inputTag;


-(void)readFromCache:(NSString *)url;
-(void)addHeader;
-(void)orientationChanged:(NSNotification *)note;

+(UITableViewCell * )getCellWithIdentifier:(NSString *)identifier;
- (float) getCellHeightForTextLandscape:(NSString*) string;
- (float) getCellHeightForTextPortrait:(NSString*) string cellwidth:(float *) widthcell;
- (float) getCellHeightForTextNomTiers:(NSString*) string;
- (float) getCellHeightForTextPortrait:(NSString*) string;
-(CGFloat )adaptecelllabell:(NSString *)sts :(CGFloat ) wid :(CGFloat) y;
-(void)adjustLabelHeight:(UILabel * )label string :(NSString *)string;
-(void)adjustLabelWidth:(UILabel * )label string :(NSString *)string andMax:(float)max;
-(void)setUpPullToRefreshHeader;
-(void)showProgressHUD:(BOOL)show;
-(void)showProgresspdfHUD:(BOOL)show;
- (IBAction)filtrePushed:(UIButton *)sender;
- (IBAction)SearchFilterPushed:(UIButton *)sender;
- (IBAction)btnback:(UIButton *)sender;

- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event;
- (void)maxScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event;
-(void)loadNibName;

@end
