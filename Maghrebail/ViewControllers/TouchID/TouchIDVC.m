//
//  TouchIDVC.m
//  Maghrebail
//
//  Created by Chaouki on 27/11/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "TouchIDVC.h"
#import "EHFAuthenticator.h"
@interface TouchIDVC ()

@end

@implementation TouchIDVC
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[EHFAuthenticator sharedInstance] setReason:@"Enter Password"];
    [[EHFAuthenticator sharedInstance] setFallbackButtonTitle:@"Enter Password"];
    [[EHFAuthenticator sharedInstance] setUseDefaultFallbackTitle:YES];
     NSError * error = nil;
    if (![EHFAuthenticator canAuthenticateWithError:&error]) {
       // [self.authenticationButton setEnabled:NO];
        NSString * authErrorString = kTouchID_CheckSettings;
        
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = kTouchID_No_touchID;
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString =kTouchID_Not_Available;
                break;
            case LAErrorPasscodeNotSet:
                authErrorString =kTouchID_NeedPassCode;
                break;
            default:
                authErrorString = kTouchID_CheckSettings;
                break;
        }
       // [self.authenticationButton setTitle:authErrorString forState:UIControlStateDisabled];
    }

    [self VerfiyTouchID];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)VerfiyTouchID
{
//    LAContext *context = [[LAContext alloc] init];
//    NSError *error = nil;
//    if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]){
//       
//        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
//                localizedReason:@"Veuillez verifier votre emprunt?"
//                          reply:^(BOOL success, NSError *error) {
//                              
//                              [self.btnExit setHidden:false];
//                              [self.pageIndicator setHidden:false];
//                              if(error){
//                                  [self exitView];
//                                  [delegate errorVerification];
//                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
//                                                                                  message:@"Problème de verification de l'identité."
//                                                                                 delegate:nil
//                                                                        cancelButtonTitle:@"Ok"
//                                                                        otherButtonTitles:nil];
//                                  [alert show];
//                                  return;
//                              }
//                              
//                              if(success){
//                                  [self exitView];
//                                   [delegate identityVerified];
//                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
//                                                                                  message:@"Identitée vérifiée !"
//                                                                                 delegate:nil
//                                                                        cancelButtonTitle:@"Ok"
//                                                                        otherButtonTitles:nil];
//                                  [alert show];
//                              } else {
//                                  [self exitView];
//                                  [delegate errorVerification];
//                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
//                                                                                  message:@"Problème de verification de l'identité."
//                                                                                 delegate:nil
//                                                                        cancelButtonTitle:@"Ok"
//                                                                        otherButtonTitles:nil];
//                                  [alert show];
//                              }
//                          }];
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
//                                                        message:@"Vore appareil ne support pas le TouchID!"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^(){
        
        [self presentAlertControllerWithMessage:@"Successfully Authenticated!"];
        [self.view removeFromSuperview];
        
    } andFailure:^(LAError errorCode){
        NSString * authErrorString;
        switch (errorCode) {
            case LAErrorSystemCancel:
                authErrorString = @"System canceled auth request due to app coming to foreground or background.";
                break;
            case LAErrorAuthenticationFailed:
                authErrorString = @"User failed after a few attempts.";
                break;
            case LAErrorUserCancel:
                authErrorString = @"User cancelled.";
                break;
            case LAErrorUserFallback:
                authErrorString = @"Fallback auth method should be implemented here.";
                break;
            case LAErrorTouchIDNotEnrolled:
                authErrorString = @"No Touch ID fingers enrolled.";
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString = @"Touch ID not available on your device.";
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = @"Need a passcode set to use Touch ID.";
                break;
            default:
                authErrorString = @"Check your Touch ID Settings.";
                break;
        }
        [self presentAlertControllerWithMessage:authErrorString];
    }];
}

-(void) presentAlertControllerWithMessage:(NSString *) message{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Touch ID" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void) exitView{
    [self.btnExit setHidden:true];
    [self.pageIndicator setHidden:true];
    [self.view removeFromSuperview];
}
- (void)dealloc {
    [_pageIndicator release];
    [_btnExit release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setPageIndicator:nil];
    [super viewDidUnload];
}
- (IBAction)btnExit:(UIButton *)sender {
}
@end
