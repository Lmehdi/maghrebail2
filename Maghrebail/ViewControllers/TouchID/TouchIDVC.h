//
//  TouchIDVC.h
//  Maghrebail
//
//  Created by Chaouki on 27/11/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@class TouchIDVCDelegate;

@protocol TouchIDVCDelegate <NSObject>

-(void)identityVerified;
-(void)errorVerification;

@end

@interface TouchIDVC : BaseViewController
{
    id<TouchIDVCDelegate>delegate;
    
}
@property(retain)id delegate;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *pageIndicator;
- (IBAction)btnExit:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnExit;
@end
