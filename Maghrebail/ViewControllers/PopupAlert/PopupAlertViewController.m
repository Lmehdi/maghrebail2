//
//  PopupAlertViewController.m
//  Maghrebail
//
//  Created by Belkheir on 18/10/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "PopupAlertViewController.h"

@interface PopupAlertViewController ()

@end

@implementation PopupAlertViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader=NO;
        _textnotification=@"";
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    shouldAddHeader=NO;
    self.iconAlert.layer.cornerRadius = self.iconAlert.frame.size.width / 2;
    self.iconAlert.clipsToBounds = YES;
    
     self.ViewPopUpContainer.layer.cornerRadius = 20;
    self.ViewPopUpContainer.layer.masksToBounds = YES;
    self.Quit.layer.cornerRadius = self.Quit.frame.size.height/2;
    self.Quit.layer.masksToBounds = YES;
    
    
    [[self.Quit layer] setBorderWidth:1.0f];
    [[self.Quit layer] setBorderColor:[UIColor orangeColor].CGColor];
    self.notifdescription.text=_textnotification;
    [self adjustLabelHeight:self.notifdescription string:_textnotification];
    self.Quit.frame=CGRectMake(self.Quit.frame.origin.x, CGRectGetMaxY(self.notifdescription.frame)+7, self.Quit.frame.size.width, self.Quit.frame.size.height);
    self.ViewPopUpContainer.frame=CGRectMake(self.ViewPopUpContainer.frame.origin.x, self.ViewPopUpContainer.frame.origin.y, self.ViewPopUpContainer.frame.size.width, self.notifdescription.frame.size.height+self.Quit.frame.size.height+50);
    
    self.Scroll_view_alert.contentSize = CGSizeMake(self.Scroll_view_alert.frame.size.width, self.notifdescription.frame.size.height);
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Quitter:(id)sender
{
    [self.view removeFromSuperview];
    
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_iconAlert release];
    [_notifdescription release];
    [_Scroll_view_alert release];
    [_Quit release];
    [_ViewPopUpContainer release];
    [super dealloc];
}
- (IBAction)terminer:(id)sender {
}
@end
