//
//  PopupAlertViewController.h
//  Maghrebail
//
//  Created by Belkheir on 18/10/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface PopupAlertViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UIImageView *iconAlert;
@property (retain, nonatomic) IBOutlet UILabel *notifdescription;
@property (retain, nonatomic) IBOutlet UIScrollView *Scroll_view_alert;
@property (retain, nonatomic) IBOutlet UIButton *Quit;

@property (retain, nonatomic) IBOutlet UIView *ViewPopUpContainer;
@property (retain, nonatomic) NSString* textnotification;

- (IBAction)Quitter:(id)sender;


@end
