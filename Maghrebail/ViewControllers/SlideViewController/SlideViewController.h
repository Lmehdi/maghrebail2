//
//  SlideViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "AuthentificationViewController.h"
#import "MonProfilViewController.h"
#import "MaLigneCreditViewController.h"
#import "MesCommandesViewController.h"
#import "MesImmatriculationsViewController.h"
#import "MesReglementsViewController.h"
#import "ContratsViewController_iphone.h"
#import "MesEngagementsViewController.h"
#import "ProduitViewController.h"
#import "MessagerieViewController.h"
#import "ChangerTiersViewController.h"
#import "MesGarantiesViewController.h"
#import "ReclamationsViewController.h"
#import "EcheanvierViewController.h"
#import "FaqViewController.h"
#import "SimulationViewController.h"
#import "NosContactsViewController.h"
#import "MentionsLegalesViewController.h"
#import "AgencesViewController.h"
#import "MesFacturesViewController.h"
#import "MonSoldeViewController.h"
#import "MentionLegaleViewController.h"
#import "PassWordViewController.h"
#import "MesContactsViewController.h"
#import "NosPublicationsViewController.h"
#import "NosPublicationsViewController.h"
#import "InscriptionViewController.h"
#import "DocumentVC.h"
#import "ReseauxSociauxVC.h"
#import "RemoteImageView.h"
#import "DetailsProfessionnelsVC_iphone.h"
#import "DetailsProfessionnelsVC_ipad.h"
#import "AppgroupeVC_iphone.h"
#import "MesRelvesViewController.h"
@interface SlideViewController : UIViewController<CompteMenuDelegate>
{
    AuthentificationViewController *authentificationViewController;
    MonProfilViewController *monProfilViewController;
    MaLigneCreditViewController *maLigneCreditViewController;
    ContratsViewController_iphone *contratsViewController;
    ProduitViewController *produitViewControllerImmobilier;
    ProduitViewController *produitViewControllerEquipement;
    MessagerieViewController * messagerieViewController;
    ChangerTiersViewController *changerTiersViewController;
    FaqViewController *faqViewController;
    SimulationViewController *simulationViewController;
    NosContactsViewController *nosContactsViewController;
    MentionsLegalesViewController *mentionsLegalesViewController;
    AgencesViewController *agencesViewController;
    DetailsProfessionnelsVC_iphone *detailViewController;
    DetailsProfessionnelsVC_ipad *detailPadViewController;
    MesCommandesViewController *mesCommandesViewController;
    MesImmatriculationsViewController *mesImmatriculationsViewController;
    MesEngagementsViewController *mesEngagementsViewController;
    MesReglementsViewController *mesReglementsViewController;
    MesGarantiesViewController * mesGarantiesViewController;
    ReclamationsViewController * reclamationsViewController;
    EcheanvierViewController * echeanvierViewController;
    MesFacturesViewController * mesFacturesViewController;
    MonSoldeViewController * monSoldeViewController;
    MesRelvesViewController  * mesRelevesViewController;
    MentionLegaleViewController * mentionLegaleViewController;
    PassWordViewController * passWordViewControllerPass;
    MesContactsViewController * mesContactsViewController;
	NosPublicationsViewController *nosPublications;
	InscriptionViewController *inscription;
    DocumentVC *mesDocuments;
    ReseauxSociauxVC *facebookVC;
    ReseauxSociauxVC *twitterVC;
    ReseauxSociauxVC *youtubeVC;
    AppgroupeVC_iphone * appGroupeVC;
    
    int affichedViewController;
    BOOL isDecouvrirDisplayed;
    BOOL isInformationDisplayed;
    BOOL isEspaceAbonneDisplayed;
    BOOL isProduitDisplayed;
	
	BOOL isRedo;
	NSMutableArray *aryBtns;
	 	
    DataManager *dataManager;
	BlockAlertView *alert;
}
@property (retain, nonatomic) IBOutlet UIView *viewCanvas;
@property (retain, nonatomic) IBOutlet UIButton *btnCanvas;
@property (retain, nonatomic) IBOutlet RemoteImageView *imgLogoCanvas;
@property (retain, nonatomic) IBOutlet UIImageView *imgLineCanvas;
@property (retain, nonatomic) IBOutlet UIView *viewBMCE;
@property (retain, nonatomic) IBOutlet UIView *viewRMA;
@property (retain, nonatomic) IBOutlet UIView *viewdemo;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *topSpaceLogog;
@property (retain, nonatomic) IBOutlet UIView *topView;

@property (retain, nonatomic) IBOutlet UIView *viewcomptefournisseur;
@property (nonatomic, assign) MFSideMenu *sideMenu;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollSideMenu;
@property (retain, nonatomic) IBOutlet UIView *viewCompte;
@property (retain, nonatomic) IBOutlet UILabel *labelName;
@property (retain, nonatomic) IBOutlet UIView *viewDecouvrir;
@property (retain, nonatomic) IBOutlet UIView *viewInformation;
@property (retain, nonatomic) IBOutlet UIButton *buttonDecouvrir;
@property (retain, nonatomic) IBOutlet UIButton *buttonInformation;
@property (retain, nonatomic) IBOutlet UIButton *buttonEspaceAbonne;
@property (retain, nonatomic) IBOutlet UIView *viewProduit;
@property (retain, nonatomic) IBOutlet UIView *viewUnderProduit;
@property (retain, nonatomic) IBOutlet UIView *viewEspaceAbonneInitial;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFlechProduit;
@property(nonatomic,retain)UIWindow * window;
@property (retain, nonatomic) NSMutableArray *aryBtns;

@property (retain, nonatomic) IBOutlet UIButton *btnDecouvrir;
@property (retain, nonatomic) IBOutlet UIButton *btnEspace1;
@property Boolean iist;
@property Boolean siist;
 @property (retain, nonatomic) IBOutlet UIButton *monprofil;

@property (retain, nonatomic) IBOutlet UIButton *btnEspace2;
@property (retain, nonatomic) IBOutlet UIButton *btnFaq;
@property (retain, nonatomic) IBOutlet UIButton *btnExitDemo;
@property (retain, nonatomic) IBOutlet UIButton *changementtiersclient;

@property (retain, nonatomic) IBOutlet UIButton *btnAppGroupe;
@property (retain, nonatomic) IBOutlet UIButton *changementtiers;

@property (retain, nonatomic) IBOutlet UIView *viewLogoutDemo;
@property (retain, nonatomic) IBOutlet UIView *viewPwd;

- (IBAction)sideButtonPushed:(id)sender;
- (void)showCompteMenu;
- (IBAction)decouvrirPushed:(UIButton *)sender;
- (IBAction)informationPushed:(UIButton *)sender;
- (IBAction)espaceAbonnePushed:(UIButton *)sender;
- (IBAction)produitPushed:(UIButton *)sender;
- (IBAction)retourPushed:(UIButton *)sender;
- (IBAction)shoDashBoard:(id)sender;

//Side Menu Items
-(void)authentification:(BOOL)dash;
-(void)monProfil:(BOOL)dash;
-(void)maLigneCredit:(BOOL)dash;
-(void)contrats:(BOOL)dash;
-(void)produitImmobilier:(BOOL)dash;
-(void)produitEquipement:(BOOL)dash;
-(void)maMessagerie:(BOOL)dash;
-(void)mesGaranties:(BOOL)dash;
-(void)mesReclamations:(BOOL)dash;
-(void)mesEcheanciers:(BOOL)dash;
-(void)faq:(BOOL)dash;
-(void)simulator:(BOOL)dash;
-(void)nosContacts:(BOOL)dash;
-(void)mentionsLegales:(BOOL)dash;
-(void)agences:(BOOL)dash;
-(void)mesContacts:(BOOL)dash;
-(void)showAppGroupe:(BOOL)dash;

-(void)mesFactures:(BOOL)dash;
-(void)monSolde:(BOOL)dash;
-(void)monpaiment:(BOOL)dash;
-(void)mentionLegale:(BOOL)dash;
-(void)mesCommandes:(BOOL)dash;
-(void)mesImmatriculations:(BOOL)dash;
-(void)mesEngagements:(BOOL)dash;
-(void)mesReglements:(BOOL)dash;

-(void)passWord:(BOOL)dash;
-(void)nosPublications:(BOOL)dash;
-(void)mesDocuments:(BOOL)dash;
-(void)ChangerTiers:(BOOL)dash;
-(void)inscription:(BOOL)dash;
-(void)showFacebook:(BOOL)dash;
-(void)showTwitter:(BOOL)dash;
-(void)showYoutube:(BOOL)dash;

-(void)detailView:(BOOL)dash andParallaxe:(BOOL) withParallaxe andIndex:(int) index;


-(void)produitEquipementEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe;

-(void)produitImmobilierEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe;

-(void)produitEquipementProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe;

-(void)produitImmobilierProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe;
@end
