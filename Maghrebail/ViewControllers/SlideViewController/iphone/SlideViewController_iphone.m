 //
//  SlideViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//
#import "SlideViewController_iphone.h"
#import "AuthentificationViewController_iphone.h"
#import "MonProfilViewController_iphone.h"
#import "MaLigneCreditViewController_iphone.h"
#import "ContratsViewController_iphone.h"
#import "ProduitViewController_iphone.h"
#import "FaqViewController_iphone.h"
#import "SimulationViewController_iphone.h"
#import "DashboardViewController_iphone.h"
#import "MesContactsViewController_iphone.h"
#import "ChangerTiersViewController_iphone.h"
#import "MesCommandesViewController_iphone.h"
#import "MesImmatriculationsViewController_iphone.h"
#import "MesReglementsViewController_iphone.h"
#import "MesEngagementsViewController_iphone.h"
#import "NosContactsViewController_iphone.h"
#import "ReclamationsViewController_iphone.h"
#import "NosPublicationsViewController_iphone.h"
#import "MonSoldeViewController_iphone.h"
#import "MentionLegaleViewController_iphone.h"
#import "InscriptionViewController_iphone.h"
#import "PassWordViewController_iphone.h"
#import "DocumentVC_iphone.h"
#import "ReseauxSociauxVC_iphone.h"
#import "NewDashBoardVC_iphone.h"
#import "MesRelvesViewController.h"
#import "MesRelvesViewController_iphone.h"
@implementation SlideViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
		// Do any additional setup after loading the view from its nib.
	dataManager.slideMenu = self;
	
}



-(void)orientationChanged:(NSNotification *)note
{
	[super orientationChanged:note];
	
	NSLog(@"hei %f" , self.topView.frame.size.height) ;
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark -- Action

- (IBAction)decouvrirPushed:(UIButton *)sender
{
    if (!dataManager.authentificationData  && !dataManager.isDemo)
    {
        return;
    }
    
    if (dataManager.authentificationData && self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewCompte.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (!dataManager.authentificationData && self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewEspaceAbonneInitial.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (isDecouvrirDisplayed)
    {
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.buttonDecouvrir.frame.size.height);
    }
    else
    {
        if (isProduitDisplayed)
        {
            self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, 265);
        }
        else
        {
            
            
            self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, 167);
        }
    }
    if(dataManager.isClientContent)
    {
        self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    
    
    }
    
    if(dataManager.isDemo)
    {
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
        
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    
    }
  
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else if(dataManager.isDemo)
    {
    
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
             self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
//        if(IS_IPHONE_4_OR_LESS){
//              self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, 1000, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
//            
//            
//        }else{
//       
//            
//        }
      

    
    
    }
   else if (dataManager.authentificationData && dataManager.isFournisseurContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isDecouvrirDisplayed = !isDecouvrirDisplayed;
	
	
    
}

- (IBAction)informationPushed:(UIButton *)sender
{
    
    if (!dataManager.authentificationData && !dataManager.isDemo)
    {
        return;
    }
    if (dataManager.authentificationData && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height && self.viewCompte.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (!dataManager.authentificationData && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height && self.viewEspaceAbonneInitial.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (isInformationDisplayed)
    {
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewInformation.frame.origin.y, self.viewInformation.frame.size.width, self.buttonInformation.frame.size.height);
    }
    else
    {
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewInformation.frame.origin.y, self.viewInformation.frame.size.width, 355 + (48 * dataManager.appgroupe.count));
    }
    if(dataManager.isClientContent)
    {
    self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
    
    }
    
    if(dataManager.isDemo)
    {
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        
    }
    
    if(dataManager.isFournisseurContent)
    {
    
    self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
    
    
    }
    
    self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
    }
    
    else if (dataManager.isDemo)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
    }
    
    else if(dataManager.isFournisseurContent)
    {
    
    self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
    
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
    }
    isInformationDisplayed = !isInformationDisplayed;
}

- (IBAction)espaceAbonnePushed:(UIButton *)sender
{
    
    if (!dataManager.authentificationData)
    {
        return;
    }
    if (self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height)
        return;
    
    if (isEspaceAbonneDisplayed)
    {
        if(dataManager.isClientContent)
        {
        self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
        }
        if(dataManager.isFournisseurContent)
        {
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
        
        }
        
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y, self.viewEspaceAbonneInitial.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
    }
    else
    {
        if(dataManager.isClientContent)
        {
            if ([[dataManager.authentificationData objectForKey:@"EMAIL"] rangeOfString:@"@maghrebail.ma"].location == NSNotFound)
            {
            self.changementtiersclient.hidden=YES;
            self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, 652);
            
            }
            else
            {
                self.changementtiersclient.hidden=NO;
                self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, 705);
            
            }
        
        
        }
        if(dataManager.isFournisseurContent)
        {
            if ([[dataManager.authentificationData objectForKey:@"EMAIL"] rangeOfString:@"@maghrebail.ma"].location == NSNotFound)
            {
                self.changementtiers.hidden=YES;
             self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, 458);
            }
            else
            {
                self.changementtiers.hidden=NO;
                self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, 509);
            }
            
       
        }
        
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewInformation.frame.size.width, 168);
    }
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
      
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
   else if (dataManager.isDemo)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
   else if (dataManager.authentificationData && dataManager.isFournisseurContent)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isEspaceAbonneDisplayed = !isEspaceAbonneDisplayed;
}

- (IBAction)produitPushed:(UIButton *)sender
{
    if (isProduitDisplayed)
    {
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.viewDecouvrir.frame.size.height - self.viewProduit.frame.size.height);
        self.viewProduit.frame = CGRectMake(self.viewProduit.frame.origin.x, self.viewProduit.frame.origin.y, self.viewProduit.frame.size.width, 0);
        [self.imgViewFlechProduit setImage:[UIImage imageNamed:@"v2_next.png"]];
    }
    else
    {
        self.viewProduit.frame = CGRectMake(self.viewProduit.frame.origin.x, self.viewProduit.frame.origin.y, self.viewProduit.frame.size.width, 98);
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.viewDecouvrir.frame.size.height + self.viewProduit.frame.size.height);
        [self.imgViewFlechProduit setImage:[UIImage imageNamed:@"v2_list.png"]];
    }
    self.viewUnderProduit.frame = CGRectMake(self.viewUnderProduit.frame.origin.x, self.viewProduit.frame.origin.y + self.viewProduit.frame.size.height, self.viewUnderProduit.frame.size.width, self.viewUnderProduit.frame.size.height);
    if(dataManager.isClientContent)
    {
     self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
    
    }
    
    if(dataManager.isDemo)
    {
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
        
    }
    
   
    self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    else if (dataManager.isDemo)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
  
    
    else if(dataManager.isFournisseurContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    
    
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isProduitDisplayed = !isProduitDisplayed;
}


#pragma mark -- side Menu actions


-(void)authentification:(BOOL)dash
{
   
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kAUTHENTIFICATION_PAGE)
    {
        if (!authentificationViewController)
        {
            authentificationViewController = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
         
            authentificationViewController.delegate = self;
            
        }
    }
   
    authentificationViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kAUTHENTIFICATION_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
}

-(void)monProfil:(BOOL)dash
{
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kMON_PROFIL_PAGE)
    {
        if (monProfilViewController)
        {
            monProfilViewController = nil;
            [monProfilViewController release];
            
        }
        monProfilViewController = [[MonProfilViewController_iphone alloc] initWithNibName:@"MonProfilViewController_iphone" bundle:nil];
        monProfilViewController.isFromDash = dash;
    }
    NSArray *controllers = [NSArray arrayWithObject:monProfilViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kMON_PROFIL_PAGE;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
}

-(void)contrats:(BOOL)dash
{
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kCONTRATS_PAGE)
    {
        if (contratsViewController)
        {
            contratsViewController = nil;
            [contratsViewController release];
        }
        
        contratsViewController = [[ContratsViewController_iphone alloc] initWithNibName:@"ContratsViewController_iphone" bundle:nil];
        [contratsViewController setParentType:kPARENT_MENU];
        contratsViewController.isFromButton = YES;
        contratsViewController.isFromDash = dash;
    }
    NSArray *controllers = [NSArray arrayWithObject:contratsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kCONTRATS_PAGE;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}


-(void)mesContacts:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_MES_CONTACTS)
    {
        if (mesContactsViewController)
        {
            mesContactsViewController = nil;
            [mesContactsViewController release];
        }
        mesContactsViewController = [[MesContactsViewController_iphone alloc] initWithNibName:@"MesContactsViewController_iphone" bundle:nil];
        
        mesContactsViewController.isFromButton = YES;
        mesContactsViewController.isFromDash = dash;
    }
    NSArray *controllers = [NSArray arrayWithObject:mesContactsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_MES_CONTACTS;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)maLigneCredit:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kLIGNE_CREDIT_PAGE)
    {
        if (maLigneCreditViewController)
        {
            maLigneCreditViewController = nil;
            [maLigneCreditViewController release];
        }
        
        maLigneCreditViewController = [[MaLigneCreditViewController_iphone alloc] initWithNibName:@"MaLigneCreditViewController_iphone" bundle:nil];
        maLigneCreditViewController.isFromButton = YES;
        maLigneCreditViewController.isFromDash = dash;
    }
    NSArray *controllers = [NSArray arrayWithObject:maLigneCreditViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kLIGNE_CREDIT_PAGE;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

- (void) produitEquipement:(BOOL)dash {
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPRODUITS_EQUIPEMENT) {
        if (!produitViewControllerEquipement) {
            produitViewControllerEquipement = [[ProduitViewController_iphone alloc] initWithNibName:@"ProduitViewController_iphone" bundle:nil];
        }
    }
    produitViewControllerEquipement.isFromDash = dash;
    produitViewControllerEquipement.index = -1;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_EQUIPEMENT;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

- (void) mesImmatriculations:(BOOL)dash {
   
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kIMMATRICULATIONS) {
        if (!mesImmatriculationsViewController) {
            mesImmatriculationsViewController = [[MesImmatriculationsViewController_iphone alloc] initWithNibName:@"MesImmatriculationsViewController_iphone" bundle:nil];
        }
    }
    mesImmatriculationsViewController.isFromDash = dash;
    mesImmatriculationsViewController.index = -1;
    NSArray *controllers = [NSArray arrayWithObject:mesImmatriculationsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kIMMATRICULATIONS;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

- (void) mesEngagements:(BOOL)dash {
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != KMESENGAGEMENTS)
    {
        
        
        if (!mesEngagementsViewController)
        {
            mesEngagementsViewController = [[MesEngagementsViewController_iphone alloc] initWithNibName:@"MesEngagementsViewController_iphone" bundle:nil];
            
        }
    }
    mesEngagementsViewController.isFromDash = dash;
    mesEngagementsViewController.index = -1;
    NSArray *controllers = [NSArray arrayWithObject:mesEngagementsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = KMESENGAGEMENTS;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

- (void) produitImmobilier:(BOOL)dash {
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPRODUITS_IMMOBILIER)
    {
        if (!produitViewControllerImmobilier)
        {
            produitViewControllerImmobilier = [[ProduitViewController_iphone alloc] initWithNibName:@"ProduitViewController_iphone" bundle:nil];
        }
    }
    
    produitViewControllerEquipement.isFromDash = dash;
    produitViewControllerEquipement.index = -1;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerImmobilier];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_IMMOBILIER;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}






-(void)produitEquipementEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe
{
    
    if (dataManager.isLandScape)
        return;
    
    NSString* nibName=@"ProduitViewController_iphone";
    
    if (withParallaxe) {
        nibName=@"ProduitVC_iphone";
    }
    
    produitViewControllerEquipement = [[ProduitViewController_iphone alloc] initWithNibName:nibName bundle:nil];
    produitViewControllerEquipement.index = -1;
    produitViewControllerEquipement.withParallaxe=withParallaxe;
    produitViewControllerEquipement.titreHeader=@"Entreprises";
    produitViewControllerEquipement.imageParallaxe=@"v3-imageEquipementEntreprise.png";
    
    produitViewControllerEquipement.isFromDash = dash;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_EQUIPEMENT;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)produitImmobilierEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe
{
    
    if (dataManager.isLandScape)
        return;
    
    NSString* nibName=@"ProduitViewController_iphone";
    
    if (withParallaxe) {
        nibName=@"ProduitVC_iphone";
    }
    
    produitViewControllerImmobilier = [[ProduitViewController_iphone alloc] initWithNibName:nibName bundle:nil];
    produitViewControllerImmobilier.index = 1;
    produitViewControllerImmobilier.withParallaxe=withParallaxe;
    produitViewControllerImmobilier.titreHeader=@"Entreprises";
    produitViewControllerImmobilier.imageParallaxe=@"v3-imageImmobilierEntreprise.png";
    
    produitViewControllerImmobilier.isFromDash = dash;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerImmobilier];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_IMMOBILIER;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)produitEquipementProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe
{
    
    if (dataManager.isLandScape)
        return;
    NSString* nibName=@"ProduitViewController_iphone";
    
    if (withParallaxe) {
        nibName=@"ProduitVC_iphone";
    }
    
    produitViewControllerEquipement = [[ProduitViewController_iphone alloc] initWithNibName:nibName bundle:nil];
    produitViewControllerEquipement.withParallaxe=withParallaxe;
    produitViewControllerEquipement.index = 2;
    produitViewControllerEquipement.titreHeader=@"Professionnels";
    produitViewControllerEquipement.isFromDash = dash;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_EQUIPEMENT;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
}

-(void)detailView:(BOOL)dash andParallaxe:(BOOL) withParallaxe andIndex:(int) index{
   
    if (!detailViewController)
    {
        detailViewController = [[DetailsProfessionnelsVC_iphone alloc] initWithNibName:@"DetailsProfessionnelsVC_iphone" bundle:nil];
    }
    detailViewController.index=index;
    [detailViewController changeScrollPosition];
    NSArray *controllers = [NSArray arrayWithObject:detailViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
}
-(void)produitImmobilierProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe
{
    if (dataManager.isLandScape) return;
    
    NSString* nibName=@"ProduitViewController_iphone";
    
    if (withParallaxe) {
        nibName=@"ProduitVC_iphone";
    }
    
    produitViewControllerImmobilier = [[ProduitViewController_iphone alloc] initWithNibName:nibName bundle:nil];
    produitViewControllerImmobilier.withParallaxe=withParallaxe;
    produitViewControllerImmobilier.titreHeader=@"Professionnels";
    produitViewControllerImmobilier.imageParallaxe=@"v3-imageImmobilierPro.png";
    produitViewControllerImmobilier.index = 2;
    
    produitViewControllerImmobilier.isFromDash = dash;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerImmobilier];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPRODUITS_IMMOBILIER;
    
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)maMessagerie:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MESSAGERIE_PAGE)
    {
        if (messagerieViewController)
        {
            messagerieViewController = nil;
            [messagerieViewController release];
        }
        
        messagerieViewController = [[MessagerieViewController_iphone alloc] initWithNibName:@"MessagerieViewController_iphone" bundle:nil];
        messagerieViewController.isFromButton = YES;
    }
    messagerieViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:messagerieViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MESSAGERIE_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)mesGaranties:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MES_GARANTIES_PAGE)
    {
        if (mesGarantiesViewController)
        {
            mesGarantiesViewController = nil;
            [mesGarantiesViewController release];
            
        }
        mesGarantiesViewController = [[MesGarantiesViewController_iphone alloc] initWithNibName:@"MesGarantiesViewController_iphone" bundle:nil];
        
        mesGarantiesViewController.isFromButton = YES;
    }
    mesGarantiesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesGarantiesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MES_GARANTIES_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}
-(void)mesDocuments:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_DOCUMENTS)
    {
        if (mesDocuments)
        {
            mesDocuments = nil;
            [mesDocuments release];
            
        }
        mesDocuments = [[DocumentVC_iphone alloc] initWithNibName:@"DocumentVC_iphone" bundle:nil];
        
        mesDocuments.isFromButton = YES;
    }
    mesDocuments.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesDocuments];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_DOCUMENTS;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}


-(void)ChangerTiers:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_CHANGERTIERS)
    {
        if (changerTiersViewController)
        {
            changerTiersViewController = nil;
            [changerTiersViewController release];
            
        }
        changerTiersViewController = [[ChangerTiersViewController_iphone alloc] initWithNibName:@"ChangerTiersViewController_iphone" bundle:nil];
        
        changerTiersViewController.isFromButton = YES;
    }
    changerTiersViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:changerTiersViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_CHANGERTIERS;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}



-(void)mesEcheanciers:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MES_ECHEANCIERS_PAGE)
    {
        if (echeanvierViewController)
        {
            echeanvierViewController = nil;
            [echeanvierViewController release];
            
        }
        echeanvierViewController = [[EcheanvierViewController_iphone alloc] initWithNibName:@"EcheanvierViewController_iphone" bundle:nil];
        
        echeanvierViewController.isFromButton = YES;
    }
    echeanvierViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:echeanvierViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MES_ECHEANCIERS_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)faq:(BOOL)dash
{
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kFAQ_PAGE)
    {
        if (!faqViewController)
        {
            faqViewController = [[FaqViewController_iphone alloc] initWithNibName:@"FaqViewController_iphone" bundle:nil];
        }
    }
    faqViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:faqViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kFAQ_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)simulator:(BOOL)dash {
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kSIMULATEUR_PAGE)
    {
        if (!simulationViewController)
        {
            simulationViewController = [[SimulationViewController_iphone alloc] initWithNibName:@"SimulationViewController_iphone" bundle:nil];
        }
    }
    simulationViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:simulationViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kSIMULATEUR_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)agences:(BOOL)dash {
    if (dataManager.isLandScape)
        return;
    
    if (affichedViewController != kAGENCES_PAGE)
    {
        if (!agencesViewController)
        {
            agencesViewController = [[AgencesViewController_iphone alloc] initWithNibName:@"AgencesViewController_iphone" bundle:nil];
        }
    }
    
    agencesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:agencesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kAGENCES_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)nosPublications:(BOOL)dash
{
    
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_PUBLICATIONS)
    {
        if (!nosPublications)
        {
            nosPublications = [[NosPublicationsViewController_iphone alloc] initWithNibName:@"NosPublicationsViewController_iphone" bundle:nil];
        }
    }
    
    nosPublications.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:nosPublications];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_PUBLICATIONS;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
}

-(void)nosContacts:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kNOS_CONTACTS_PAGE)
    {
        if (!nosContactsViewController)
        {
            nosContactsViewController = [[NosContactsViewController_iphone alloc] initWithNibName:@"NosContactsViewController_iphone" bundle:nil];
        }
    }
    nosContactsViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:nosContactsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kNOS_CONTACTS_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}
-(void)mentionsLegales:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kMENTIONS_LEGALES_PAGE)
    {
        if (!mentionsLegalesViewController)
        {
            mentionsLegalesViewController = [[MentionsLegalesViewController alloc] initWithNibName:@"MentionsLegalesViewController_iphone" bundle:nil];
        }
    }
    mentionsLegalesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mentionsLegalesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kMENTIONS_LEGALES_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)mesReclamations:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MES_RECLAMATIONS_PAGE)
    {
        if (!reclamationsViewController)
        {
            reclamationsViewController = [[ReclamationsViewController_iphone alloc] initWithNibName:@"ReclamationsViewController_iphone" bundle:nil];
        }
    }
    reclamationsViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:reclamationsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MES_RECLAMATIONS_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)mesFactures:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MES_FACTURES_PAGE)
    {
        if (mesFacturesViewController)
        {
            mesFacturesViewController = nil;
            [mesFacturesViewController release];
        }
        mesFacturesViewController = [[MesFacturesViewController_iphone alloc] initWithNibName:@"MesFacturesViewController_iphone" bundle:nil];
        mesFacturesViewController.isFromButton = YES;
    }
    mesFacturesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesFacturesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MES_FACTURES_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}
-(void)monSolde:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MON_SOLDE_PAGE)
    {
        if (mesRelevesViewController)
        {
            mesRelevesViewController = nil;
            [mesRelevesViewController release];
            
        }
        mesRelevesViewController=[[MesRelvesViewController_iphone  alloc] initWithNibName:@"MesRelvesViewController_iphone" bundle:nil ];
        
        monSoldeViewController = [[MonSoldeViewController_iphone alloc] initWithNibName:@"MonSoldeViewController_iphone" bundle:nil];
        
    }
    monSoldeViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:monSoldeViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MON_SOLDE_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}
-(void)monpaiment:(BOOL)dash{
    
    
    [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MON_SOLDE_PAGE)
    {
        if (mesRelevesViewController)
        {
            mesRelevesViewController = nil;
            [mesRelevesViewController release];
            
        }
        mesRelevesViewController=[[MesRelvesViewController_iphone  alloc] initWithNibName:@"MesRelvesViewController_iphone" bundle:nil ];
        
        monSoldeViewController = [[MonSoldeViewController_iphone alloc] initWithNibName:@"MonSoldeViewController_iphone" bundle:nil];
        
    }
    mesRelevesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesRelevesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MON_SOLDE_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
    
}
-(void)mentionLegale:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != MENTION_LEGALE_PAGE)
    {
        if (!mentionLegaleViewController)
        {
            mentionLegaleViewController = [[MentionLegaleViewController_iphone alloc] initWithNibName:@"MentionLegaleViewController_iphone" bundle:nil];
        }
    }
    mentionLegaleViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mentionLegaleViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = MENTION_LEGALE_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)mesCommandes:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_COMMANDES)
    {
        if (!mesCommandesViewController)
        {
            mesCommandesViewController = [[MesCommandesViewController_iphone alloc] initWithNibName:@"MesCommandesViewController_iphone" bundle:nil];
        }
    }
    mesCommandesViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesCommandesViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_COMMANDES;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)mesReglements:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_REGLEMENTS)
    {
        if (!mesReglementsViewController)
        {
            mesReglementsViewController = [[MesReglementsViewController_iphone alloc] initWithNibName:@"MesReglementsViewController_iphone" bundle:nil];
        }
    }
    mesReglementsViewController.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:mesReglementsViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_REGLEMENTS;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}


-(void)passWord:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != PASSWORD_PAGE)
    {
        if (passWordViewControllerPass)
        {
            passWordViewControllerPass = nil;
            [passWordViewControllerPass release];
            
        }
        passWordViewControllerPass = [[PassWordViewController_iphone alloc] initWithNibName:@"PassWordViewController_iphone" bundle:nil];
    }
    passWordViewControllerPass.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:passWordViewControllerPass];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = PASSWORD_PAGE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)inscription:(BOOL)dash
{
    
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_INSCRIPTION)
    {
        if (inscription)
        {
            inscription = nil;
            [inscription release];
        }
        inscription = [[InscriptionViewController_iphone alloc] initWithNibName:@"InscriptionViewController_iphone" bundle:nil];
    }
    inscription.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:inscription];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_INSCRIPTION;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)showFacebook:(BOOL)dash
{
    
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_FACEBOOK)
    {
        if (facebookVC)
        {
            facebookVC = nil;
            [facebookVC release];
        }
        facebookVC = [[ReseauxSociauxVC_iphone alloc] initWithNibName:@"ReseauxSociauxVC_iphone" bundle:nil];
    }
    facebookVC.isFromDash = dash;
    facebookVC.type = 10; //FACEBOOK
    NSArray *controllers = [NSArray arrayWithObject:facebookVC];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_FACEBOOK;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)showTwitter:(BOOL)dash
{
    
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_TWITTER)
    {
        if (twitterVC)
        {
            twitterVC = nil;
            [twitterVC release];
        }
        twitterVC = [[ReseauxSociauxVC_iphone alloc] initWithNibName:@"ReseauxSociauxVC_iphone" bundle:nil];
    }
    
    twitterVC.isFromDash = dash;
    twitterVC.type = 20; //TWITTER
    NSArray *controllers = [NSArray arrayWithObject:twitterVC];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_TWITTER;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

-(void)showYoutube:(BOOL)dash
{
    
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_YOUTUBE)
    {
        if (youtubeVC)
        {
            youtubeVC = nil;
            [youtubeVC release];
        }
        youtubeVC = [[ReseauxSociauxVC_iphone alloc] initWithNibName:@"ReseauxSociauxVC_iphone" bundle:nil];
    }
    
    youtubeVC.isFromDash = dash;
    youtubeVC.type = 30; //Youtube
    NSArray *controllers = [NSArray arrayWithObject:youtubeVC];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_YOUTUBE;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}
-(void)showAppGroupe:(BOOL)dash
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (dataManager.isLandScape)
        return;
    if (affichedViewController != kPAGE_FAppGroupe)
    {
        if (appGroupeVC)
        {
            appGroupeVC = nil;
            [appGroupeVC release];
        }
        appGroupeVC = [[AppgroupeVC_iphone alloc] initWithNibName:@"AppgroupeVC_iphone" bundle:nil];
    }
    
    appGroupeVC.isFromDash = dash;
    NSArray *controllers = [NSArray arrayWithObject:appGroupeVC];
    self.sideMenu.navigationController.viewControllers = controllers;
    affichedViewController = kPAGE_FAppGroupe;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
}

- (IBAction)shoDashBoard:(id)sender
{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    if (dataManager.isLandScape)
        return;
    
    // DashboardViewController * dashBoardViewController =  [[DashboardViewController_iphone alloc] initWithNibName:@"DashboardViewController_iphone" bundle:nil];
    NewDashBoardVC_iphone* newDash=[[NewDashBoardVC_iphone alloc] initWithNibName:@"NewDashBoardVC_iphone" bundle:nil];
    [newDash hideAllSeprators:true];
    UINavigationController * mainNavigationController = [[UINavigationController alloc]initWithRootViewController:newDash];
    mainNavigationController.navigationBarHidden = YES;
    
    [self.window setRootViewController:mainNavigationController];
    //[self.window addSubview: mainNavigationController.view];
    
    [self.window makeKeyAndVisible];
    
    dataManager.escapePub = YES;
    if (dataManager.isDemo)
    {
        dataManager.isDemo = NO;
    }
}
@end
