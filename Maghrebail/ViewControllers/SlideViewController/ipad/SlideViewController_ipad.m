//
//  SlideViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//
#import "SlideViewController_ipad.h"
#import "AuthentificationViewController_ipad.h"
#import "MonProfilViewController_ipad.h"
#import "MaLigneCreditViewController_ipad.h"
#import "ContratsViewController_ipad.h"
#import "ProduitViewController_ipad.h"
#import "FaqViewController_ipad.h"
#import "SimulationViewController_ipad.h"
#import "MesCommandesViewController_ipad.h"
#import "MesContactsViewController_ipad.h"
#import "NosPublicationsViewController_ipad.h"
#import "AgenceViewController_ipad.h"
#import "ReseauxSociauxVC_ipad.h"
#import "DocumentVC_ipad.h"
#import "NewDashBoardVC_ipad.h"
#import "AppDelegate.h"
#import "NosContactsViewController_ipad.h"
#import "MesReglementsViewController_ipad.h"
#import "MesImmatriculationsViewController_ipad.h"
#import "MesEngagementsViewController_ipad.h"
#import "ChangerTiersViewController_ipad.h"
#import "MesRelvesViewController_ipad.h"
@implementation SlideViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    dataManager.slideMenu = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAgence) name:@"showAgence" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMessagerie) name:@"showMessagerie" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSimulateur) name:@"showSimulateur" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesReclamations) name:@"showMesReclamations" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesContact) name:@"showMesContact" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesFactures) name:@"showMesFactures" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesEcheaciers) name:@"showMesEcheaciers" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesImpayes) name:@"showMesImpayes" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesLignesCredits) name:@"showMesLignesCredits" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesContrats) name:@"showMesContrats" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesGaranties) name:@"showMesGaranties" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMesDocuments) name:@"showMesDocuments" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFAQ) name:@"showFAQ" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNosPublication) name:@"showNosPublication" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMentionLegales) name:@"showMentionLegales" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showfacebook) name:@"showFacebook" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showtwitter) name:@"showTwitter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showContacts) name:@"showContacts" object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark -- Action

- (IBAction)decouvrirPushed:(UIButton *)sender
{
    if (!dataManager.authentificationData  && !dataManager.isDemo)
    {
        return;
    }
    
    if (dataManager.authentificationData && self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewCompte.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (!dataManager.authentificationData && self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewEspaceAbonneInitial.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (isDecouvrirDisplayed)
    {
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.buttonDecouvrir.frame.size.height);
    }
    else
    {
        if (isProduitDisplayed)
        {
            self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, 265);
        }
        else
        {
            self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, 167);
        }
    }
    if(dataManager.isClientContent)
    {
        self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
        
        
    }
    
    if(dataManager.isDemo)
    {
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
        
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
        
    }
    
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else if(dataManager.isDemo)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
        
        
        
    }
    else if (dataManager.authentificationData && dataManager.isFournisseurContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isDecouvrirDisplayed = !isDecouvrirDisplayed;
    
}

- (IBAction)informationPushed:(UIButton *)sender
{
    
    if (!dataManager.authentificationData && !dataManager.isDemo)
    {
        return;
    }
    if (dataManager.authentificationData && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height && self.viewCompte.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (!dataManager.authentificationData && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height && self.viewEspaceAbonneInitial.frame.size.height == self.buttonEspaceAbonne.frame.size.height)
        return;
    
    if (isInformationDisplayed)
    {
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewInformation.frame.origin.y, self.viewInformation.frame.size.width, self.buttonInformation.frame.size.height);
    }
    else
    {
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewInformation.frame.origin.y, self.viewInformation.frame.size.width, 355 + (48 * dataManager.appgroupe.count));
    }
    if(dataManager.isClientContent)
    {
        self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
        
    }
    
    if(dataManager.isDemo)
    {
       
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
        
        
    }
    
    self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
    }
    
    else if (dataManager.isDemo)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
    }
    
    else if(dataManager.isFournisseurContent)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
    }
    isInformationDisplayed = !isInformationDisplayed;
}

- (IBAction)espaceAbonnePushed:(UIButton *)sender
{
    
    if (!dataManager.authentificationData)
    {
        return;
    }
    if (self.viewInformation.frame.size.height == self.buttonInformation.frame.size.height && self.viewDecouvrir.frame.size.height == self.buttonDecouvrir.frame.size.height)
        return;
    
    if (isEspaceAbonneDisplayed)
    {
        if(dataManager.isClientContent)
        {
            self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
        }
        if(dataManager.isFournisseurContent)
        {
           
            self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
            
            
        }
        
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y, self.viewEspaceAbonneInitial.frame.size.width, self.buttonEspaceAbonne.frame.size.height);
    }
    else
    {
        if(dataManager.isClientContent)
        {
            if ([[dataManager.authentificationData objectForKey:@"EMAIL"] rangeOfString:@"@maghrebail.ma"].location == NSNotFound)
            {
                self.changementtiersclient.hidden=YES;
                self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, 609);
                
            }
            else
            {
                self.changementtiersclient.hidden=NO;
                self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, 694);
                
            }
            
            
        }
        if(dataManager.isFournisseurContent)
        {
            if ([[dataManager.authentificationData objectForKey:@"EMAIL"] rangeOfString:@"@maghrebail.ma"].location == NSNotFound)
            {
                self.changementtiers.hidden=YES;
                self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, 458);
            }
            else
            {
                self.changementtiers.hidden=NO;
                self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, 509);
            }
            
            
        }
        
        self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewEspaceAbonneInitial.frame.origin.x,self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewInformation.frame.size.width, 168);
    }
    
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    else if (dataManager.isDemo)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
    else if (dataManager.authentificationData && dataManager.isFournisseurContent)
    {
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isEspaceAbonneDisplayed = !isEspaceAbonneDisplayed;
}

- (IBAction)produitPushed:(UIButton *)sender
{
    if (isProduitDisplayed)
    {
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.viewDecouvrir.frame.size.height - self.viewProduit.frame.size.height);
        self.viewProduit.frame = CGRectMake(self.viewProduit.frame.origin.x, self.viewProduit.frame.origin.y, self.viewProduit.frame.size.width, 0);
        [self.imgViewFlechProduit setImage:[UIImage imageNamed:@"v2_next.png"]];
    }
    else
    {
        self.viewProduit.frame = CGRectMake(self.viewProduit.frame.origin.x, self.viewProduit.frame.origin.y, self.viewProduit.frame.size.width, 98);
        self.viewDecouvrir.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y, self.viewDecouvrir.frame.size.width, self.viewDecouvrir.frame.size.height + self.viewProduit.frame.size.height);
        [self.imgViewFlechProduit setImage:[UIImage imageNamed:@"v2_list.png"]];
    }
    self.viewUnderProduit.frame = CGRectMake(self.viewUnderProduit.frame.origin.x, self.viewProduit.frame.origin.y + self.viewProduit.frame.size.height, self.viewUnderProduit.frame.size.width, self.viewUnderProduit.frame.size.height);
    if(dataManager.isClientContent)
    {
        self.viewCompte.frame = CGRectMake(self.viewCompte.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewCompte.frame.size.width, self.viewCompte.frame.size.height);
        
    }
    
    if(dataManager.isDemo)
    {
        self.viewdemo.frame = CGRectMake(self.viewdemo.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewdemo.frame.size.width, self.viewdemo.frame.size.height);
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        self.viewcomptefournisseur.frame = CGRectMake(self.viewcomptefournisseur.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewcomptefournisseur.frame.size.width, self.viewcomptefournisseur.frame.size.height);
        
    }
    
    
    self.viewEspaceAbonneInitial.frame = CGRectMake(self.viewDecouvrir.frame.origin.x, self.viewDecouvrir.frame.origin.y + self.viewDecouvrir.frame.size.height, self.viewEspaceAbonneInitial.frame.size.width, self.viewEspaceAbonneInitial.frame.size.height);
    if (dataManager.authentificationData && dataManager.isClientContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    else if (dataManager.isDemo)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
    
    else if(dataManager.isFournisseurContent)
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
        
        
    }
    else
    {
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width,self.viewDecouvrir.frame.origin.y+ self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    isProduitDisplayed = !isProduitDisplayed;
}



#pragma mark --
#pragma mark -- Notifications System

-(void)showAgence{
    [self agences:YES];
}
-(void)showSimulateur{
    [self simulator:YES];
}
-(void)showMessagerie{
    [self maMessagerie:YES];
}
-(void)showMesContact{
    [self mesContacts:YES];
}
-(void)showMesReclamations{
    [self mesReclamations:YES];
}
-(void)showMesFactures{
    [self mesFactures:YES];
}
-(void)showMesEcheaciers{
    [self mesEcheanciers:YES];
}
-(void)showMesImpayes{
    [self monSolde:YES];
}

-(void)showMesLignesCredits{
    [self maLigneCredit:YES];
}
-(void)showMesContrats{
    [self contrats:YES];
}
-(void)showMesGaranties{
    [self mesGaranties:YES];
}
-(void)showMesDocuments{
    [self mesDocuments:YES];
}

-(void)showFAQ{
    [self faq:YES];
}
-(void)showNosPublication{
    [self nosPublications:YES];
}
-(void)showMentionLegales{
    [self mentionLegale:YES];
}

-(void)showfacebook{
    [self showFacebook:YES];
}
-(void)showtwitter{
    [self showTwitter:YES];
}
-(void)showContacts{
    [self nosContacts:YES];
}


#pragma mark -- side Menu actions

-(void)authentification:(BOOL)dash
{
  
    if (affichedViewController != kAUTHENTIFICATION_PAGE)
    {
        if (!authentificationViewController)
        {
            authentificationViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
            authentificationViewController.delegate = self;
			
        }
	}
    
    authentificationViewController.isFromDash = dash;
    affichedViewController = kAUTHENTIFICATION_PAGE;
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}
-(void)monProfil:(BOOL)dash
{
    if (affichedViewController != kMON_PROFIL_PAGE)
    {
        if (monProfilViewController)
            
        {
			monProfilViewController = nil;
			[monProfilViewController release];
			
        }
		monProfilViewController = [[MonProfilViewController_ipad alloc] initWithNibName:@"MonProfilViewController_ipad" bundle:nil];
        monProfilViewController.isFromDash = dash;
	}
    affichedViewController = kMON_PROFIL_PAGE;
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:monProfilViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)contrats:(BOOL)dash
{
    if (affichedViewController != kCONTRATS_PAGE)
    {
        if (contratsViewController)
        {
			contratsViewController = nil;
			[contratsViewController release];
        }
		
		contratsViewController = [[ContratsViewController_ipad alloc] initWithNibName:@"ContratsViewController_ipad" bundle:nil];
		[contratsViewController setParentType:kPARENT_MENU];
		contratsViewController.isFromButton = YES;
        contratsViewController.isFromDash = dash;
    }

    affichedViewController = kCONTRATS_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:contratsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}


-(void)mesContacts:(BOOL)dash
{
    
    if (affichedViewController != kPAGE_MES_CONTACTS)
    {
        if (mesContactsViewController)
        {
			mesContactsViewController = nil;
			[mesContactsViewController release];
        }
		mesContactsViewController = [[MesContactsViewController_ipad alloc] initWithNibName:@"MesContactsViewController_ipad" bundle:nil];
		
		mesContactsViewController.isFromButton = YES;
        mesContactsViewController.isFromDash = dash;
	}

	affichedViewController = kPAGE_MES_CONTACTS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesContactsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
	

}

-(void)maLigneCredit:(BOOL)dash
{
	
    if (affichedViewController != kLIGNE_CREDIT_PAGE)
    {
		if (maLigneCreditViewController)
		{
			maLigneCreditViewController = nil;
			[maLigneCreditViewController release];
		}
		
		maLigneCreditViewController = [[MaLigneCreditViewController_ipad alloc] initWithNibName:@"MaLigneCreditViewController_ipad" bundle:nil];
 		maLigneCreditViewController.isFromButton = YES;
        maLigneCreditViewController.isFromDash = dash;
	}

    affichedViewController = kLIGNE_CREDIT_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:maLigneCreditViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}
-(void)detailView:(BOOL)dash andParallaxe:(BOOL) withParallaxe andIndex:(int) index{
    
    if (!detailPadViewController)
    {
        detailPadViewController = [[DetailsProfessionnelsVC_ipad alloc] initWithNibName:@"DetailsProfessionnelsVC_ipad" bundle:nil];
    }
    
    detailPadViewController.index=index;
    [detailPadViewController changeScrollPosition];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:detailPadViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)produitEquipement:(BOOL)dash
{
	
    if (affichedViewController != kPRODUITS_EQUIPEMENT)
    {
        if (!produitViewControllerEquipement)
        {
            produitViewControllerEquipement = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
        }
	}
    produitViewControllerEquipement.isFromDash = dash;
    produitViewControllerEquipement.index = -1;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];
    affichedViewController = kPRODUITS_EQUIPEMENT;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)produitImmobilier:(BOOL)dash
{
	
    if (affichedViewController != kPRODUITS_IMMOBILIER)
    {
        if (!produitViewControllerImmobilier)
        {
            produitViewControllerImmobilier = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
        }
	}
    produitViewControllerEquipement.isFromDash = dash;
    produitViewControllerEquipement.index = -1;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];

    affichedViewController = kPRODUITS_IMMOBILIER;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)produitEquipementEntreprise:(BOOL)dash
{
	
    produitViewControllerEquipement = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
    produitViewControllerEquipement.index = 1;
    
    produitViewControllerEquipement.isFromDash = dash;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];

    affichedViewController = kPRODUITS_EQUIPEMENT;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)produitImmobilierEntreprise:(BOOL)dash
{
	
    produitViewControllerImmobilier = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
    produitViewControllerImmobilier.index = 1;
    
    produitViewControllerImmobilier.isFromDash = dash;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];
    affichedViewController = kPRODUITS_IMMOBILIER;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerImmobilier];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)produitEquipementProfessionnel:(BOOL)dash
{
	
    
    produitViewControllerEquipement = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
    produitViewControllerEquipement.index = 2;
    
    produitViewControllerEquipement.isFromDash = dash;
    [produitViewControllerEquipement setTypeProduit:kPRODUITS_EQUIPEMENT];
    affichedViewController = kPRODUITS_EQUIPEMENT;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerEquipement];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)produitImmobilierProfessionnel:(BOOL)dash
{
	
    
    produitViewControllerImmobilier = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
    produitViewControllerImmobilier.index = 2;
    
    produitViewControllerImmobilier.isFromDash = dash;
    [produitViewControllerImmobilier setTypeProduit:kPRODUITS_IMMOBILIER];
    affichedViewController = kPRODUITS_IMMOBILIER;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:produitViewControllerImmobilier];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)maMessagerie:(BOOL)dash
{
    [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
     // [self.monprofil setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    if (affichedViewController != MESSAGERIE_PAGE)
    {
        if (messagerieViewController)
        {
			messagerieViewController = nil;
			[messagerieViewController release];
        }
		
		messagerieViewController = [[MessagerieViewController_ipad alloc] initWithNibName:@"MessagerieViewController_ipad" bundle:nil];
		messagerieViewController.isFromButton = YES;
    }
    messagerieViewController.isFromDash = dash;
	affichedViewController = MESSAGERIE_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:messagerieViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)mesGaranties:(BOOL)dash
{
	
    if (affichedViewController != MES_GARANTIES_PAGE)
    {
        if (mesGarantiesViewController)
        {
			mesGarantiesViewController = nil;
			[mesGarantiesViewController release];
			
        }
		mesGarantiesViewController = [[MesGarantiesViewController_ipad alloc] initWithNibName:@"MesGarantiesViewController_ipad" bundle:nil];
		
		mesGarantiesViewController.isFromButton = YES;
    }
    mesGarantiesViewController.isFromDash = dash;

	affichedViewController = MES_GARANTIES_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesGarantiesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)mesDocuments:(BOOL)dash
{
	
    if (affichedViewController != kPAGE_DOCUMENTS)
    {
        if (mesDocuments)
        {
			mesDocuments = nil;
			[mesDocuments release];
			
        }
		mesDocuments = [[DocumentVC_ipad alloc] initWithNibName:@"DocumentVC_ipad" bundle:nil];
		
		mesDocuments.isFromButton = YES;
    }
    mesDocuments.isFromDash = dash;
	affichedViewController = kPAGE_DOCUMENTS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesDocuments];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)mesEcheanciers:(BOOL)dash
{
	
    if (affichedViewController != MES_ECHEANCIERS_PAGE)
    {
        if (echeanvierViewController)
        {
			echeanvierViewController = nil;
			[echeanvierViewController release];
			
        }
		echeanvierViewController = [[EcheanvierViewController_ipad alloc] initWithNibName:@"EcheanvierViewController_ipad" bundle:nil];
		
		echeanvierViewController.isFromButton = YES;
    }
    echeanvierViewController.isFromDash = dash;
	affichedViewController = MES_ECHEANCIERS_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:echeanvierViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)faq:(BOOL)dash
{
    if (affichedViewController != kFAQ_PAGE)
    {
        if (!faqViewController)
        {
            faqViewController = [[FaqViewController_ipad alloc] initWithNibName:@"FaqViewController_ipad" bundle:nil];
        }
    }
    faqViewController.isFromDash = dash;

    affichedViewController = kFAQ_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:faqViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)simulator:(BOOL)dash
{
    if (affichedViewController != kSIMULATEUR_PAGE)
    {
        if (!simulationViewController)
        {
            simulationViewController = [[SimulationViewController_ipad alloc] initWithNibName:@"SimulationViewController_ipad" bundle:nil];
        }
    }
    simulationViewController.isFromDash = dash;

	affichedViewController = kSIMULATEUR_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:simulationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)agences:(BOOL)dash
{
    
    if (affichedViewController != kAGENCES_PAGE)
    {
        if (!agencesViewController)
        {
            agencesViewController = [[AgenceViewController_ipad alloc] initWithNibName:@"AgenceViewController_ipad" bundle:nil];
        }
    }
    
    agencesViewController.isFromDash = dash;
	affichedViewController = kAGENCES_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:agencesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}


-(void)nosPublications:(BOOL)dash
{
	
    if (affichedViewController != kPAGE_PUBLICATIONS)
    {
        if (!nosPublications)
        {
            nosPublications = [[NosPublicationsViewController_ipad alloc] initWithNibName:@"NosPublicationsViewController_ipad" bundle:nil];
        }
    }
    nosPublications.isFromDash = dash;
	affichedViewController = kPAGE_PUBLICATIONS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:nosPublications];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
}

-(void)nosContacts:(BOOL)dash
{
	 [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != kNOS_CONTACTS_PAGE)
    {
        if (!nosContactsViewController)
        {
            nosContactsViewController = [[NosContactsViewController_ipad alloc] initWithNibName:@"NosContactsViewController_ipad" bundle:nil];
        }
    }
    nosContactsViewController.isFromDash = dash;
	affichedViewController = kNOS_CONTACTS_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:nosContactsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)mentionsLegales:(BOOL)dash
{
	 [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != kMENTIONS_LEGALES_PAGE)
    {
        if (!mentionsLegalesViewController)
        {
            mentionsLegalesViewController = [[MentionsLegalesViewController alloc] initWithNibName:@"MentionsLegalesViewController_ipad" bundle:nil];
        }
    }
    mentionsLegalesViewController.isFromDash = dash;

	affichedViewController = kMENTIONS_LEGALES_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mentionsLegalesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)mesReclamations:(BOOL)dash
{
	 [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != MES_RECLAMATIONS_PAGE)
    {
        if (!reclamationsViewController)
        {
            reclamationsViewController = [[ReclamationsViewController alloc] initWithNibName:@"ReclamationsViewController_ipad" bundle:nil];
        }
    }
    reclamationsViewController.isFromDash = dash;
	affichedViewController = MES_RECLAMATIONS_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:reclamationsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)mesFactures:(BOOL)dash
{
	 [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != MES_FACTURES_PAGE)
    {
        if (mesFacturesViewController)
        {
			mesFacturesViewController = nil;
			[mesFacturesViewController release];
        }
		mesFacturesViewController = [[MesFacturesViewController_ipad alloc] initWithNibName:@"MesFacturesViewController_ipad" bundle:nil];
		mesFacturesViewController.isFromButton = YES;
    }
    mesFacturesViewController.isFromDash = dash;
	affichedViewController = MES_FACTURES_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesFacturesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)monSolde:(BOOL)dash
{
	
    if (affichedViewController != MON_SOLDE_PAGE)
    {
        if (monSoldeViewController)
        {
			monSoldeViewController = nil;
			[monSoldeViewController release];
			
        }
		monSoldeViewController = [[MonSoldeViewController alloc] initWithNibName:@"MonSoldeViewController_ipad" bundle:nil];
		
    }
    monSoldeViewController.isFromDash = dash;
	affichedViewController = MON_SOLDE_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:monSoldeViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)monpaiment:(BOOL)dash{
    
    [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];

    if (affichedViewController != MON_PAIMENT_PAGE)
    {
        if (mesRelevesViewController)
        {
            mesRelevesViewController = nil;
            [mesRelevesViewController release];
            
        }
        mesRelevesViewController=[[MesRelvesViewController_ipad  alloc] initWithNibName:@"MesRelvesViewController_ipad" bundle:nil ];
        
        
        
    }
    mesRelevesViewController.isFromDash = dash;
    affichedViewController = MON_PAIMENT_PAGE;
 
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesRelevesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
}
-(void)mentionLegale:(BOOL)dash
{
	
    if (affichedViewController != MENTION_LEGALE_PAGE)
    {
        if (!mentionLegaleViewController)
        {
            mentionLegaleViewController = [[MentionLegaleViewController alloc] initWithNibName:@"MentionLegaleViewController_ipad" bundle:nil];
        }
    }
    mentionLegaleViewController.isFromDash = dash;
	affichedViewController = MENTION_LEGALE_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mentionLegaleViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
-(void)passWord:(BOOL)dash
{
	
    if (affichedViewController != PASSWORD_PAGE)
    {
        if (passWordViewControllerPass)
        {
			passWordViewControllerPass = nil;
			[passWordViewControllerPass release];
			
        }
		passWordViewControllerPass = [[PassWordViewController alloc] initWithNibName:@"PassWordViewController_ipad" bundle:nil];
    }
    passWordViewControllerPass.isFromDash = dash;
	affichedViewController = PASSWORD_PAGE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:passWordViewControllerPass];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)inscription:(BOOL)dash
{
    if (affichedViewController != kPAGE_INSCRIPTION)
    {
        if (inscription)
        {
			inscription = nil;
			[inscription release];
        }
		inscription = [[InscriptionViewController alloc] initWithNibName:@"InscriptionViewController_ipad" bundle:nil];
    }
    inscription.isFromDash = dash;

	affichedViewController = kPAGE_INSCRIPTION;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:inscription];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)showFacebook:(BOOL)dash
{
    if (affichedViewController != kPAGE_FACEBOOK)
    {
        if (facebookVC)
        {
			facebookVC = nil;
			[facebookVC release];
        }
		facebookVC = [[ReseauxSociauxVC_ipad alloc] initWithNibName:@"ReseauxSociauxVC_ipad" bundle:nil];
    }
    
    facebookVC.isFromDash = dash;
    facebookVC.type = 10; //FACEBOOK

	affichedViewController = kPAGE_FACEBOOK;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:facebookVC];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];

}

-(void)showTwitter:(BOOL)dash
{
    if (affichedViewController != kPAGE_TWITTER)
    {
        if (twitterVC)
        {
			twitterVC = nil;
			[twitterVC release];
        }
		twitterVC = [[ReseauxSociauxVC_ipad alloc] initWithNibName:@"ReseauxSociauxVC_ipad" bundle:nil];
    }
    twitterVC.isFromDash = dash;
    twitterVC.type = 20; //FACEBOOK

	affichedViewController = kPAGE_TWITTER;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:twitterVC];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

- (IBAction)shoDashBoard:(id)sender
{
    
    AppDelegate* delegate=[[UIApplication sharedApplication] delegate];
    [delegate showNewSideMenu];
//    NewDashBoardVC_ipad * dashBoardViewController = [[NewDashBoardVC_ipad alloc]initWithNibName:@"NewDashBoardVC_ipad" bundle:nil];
//    
//    UINavigationController * mainNavigationController = [[UINavigationController alloc]initWithRootViewController:dashBoardViewController];
//    mainNavigationController.navigationBarHidden = YES;
//    
//    [self.window setRootViewController:mainNavigationController];
//    //[self.window addSubview: mainNavigationController.view];
//    
//    [self.window makeKeyAndVisible];
   // dashBoardViewController.window = self.window;
	dataManager.escapePub = YES;
	if (dataManager.isDemo)
	{
		dataManager.isDemo = NO;
	}
}
-(void)mesCommandes:(BOOL)dash
{
    
      [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != kPAGE_COMMANDES)
    {
        if (mesCommandesViewController)
        {
            mesCommandesViewController = nil;
            [mesCommandesViewController release];
        }
                mesCommandesViewController = [[MesCommandesViewController_ipad alloc] initWithNibName:@"MesCommandesViewController_ipad" bundle:nil];
        
        mesCommandesViewController.isFromButton = YES;
        mesCommandesViewController.isFromDash = dash;
    }
  
//    NSArray *controllers = [NSArray arrayWithObject:mesCommandesViewController];
//    self.sideMenu.navigationController.viewControllers = controllers;
//    affichedViewController = kPAGE_COMMANDES;
//    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
    
    affichedViewController = kPAGE_COMMANDES;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesCommandesViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}

-(void)mesReglements:(BOOL)dash{
 
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != kPAGE_REGLEMENTS)
    {
        if (mesReglementsViewController)
        {
            mesReglementsViewController = nil;
            [mesReglementsViewController release];
        }
        mesReglementsViewController = [[MesReglementsViewController_ipad alloc] initWithNibName:@"MesReglementsViewController_ipad" bundle:nil];
        
        mesReglementsViewController.isFromButton = YES;
        mesReglementsViewController.isFromDash = dash;
    }
    
    //    NSArray *controllers = [NSArray arrayWithObject:mesCommandesViewController];
    //    self.sideMenu.navigationController.viewControllers = controllers;
    //    affichedViewController = kPAGE_COMMANDES;
    //    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
    
    affichedViewController = kPAGE_REGLEMENTS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesReglementsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
    
}
-(void)mesImmatriculations:(BOOL)dash{
    
  
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    if (affichedViewController != kIMMATRICULATIONS)
    {
        if (mesImmatriculationsViewController)
        {
            mesImmatriculationsViewController = nil;
            [mesImmatriculationsViewController release];
        }
        mesImmatriculationsViewController = [[MesImmatriculationsViewController_ipad alloc] initWithNibName:@"MesImmatriculationsViewController_ipad" bundle:nil];
        
        mesImmatriculationsViewController.isFromButton = YES;
        mesImmatriculationsViewController.isFromDash = dash;
    }
    
    //    NSArray *controllers = [NSArray arrayWithObject:mesCommandesViewController];
    //    self.sideMenu.navigationController.viewControllers = controllers;
    //    affichedViewController = kPAGE_COMMANDES;
    //    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    
    
    affichedViewController = kIMMATRICULATIONS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesImmatriculationsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
}

-(void)mesEngagements:(BOOL)dash{
    
   
    
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != KMESENGAGEMENTS)
    {
        if (mesEngagementsViewController)
        {
            mesEngagementsViewController = nil;
            [mesEngagementsViewController release];
        }
        mesEngagementsViewController = [[MesEngagementsViewController_ipad alloc] initWithNibName:@"MesEngagementsViewController_ipad" bundle:nil];
        
        mesEngagementsViewController.isFromButton = YES;
        mesEngagementsViewController.isFromDash = dash;
    }
 
    
    
    affichedViewController = KMESENGAGEMENTS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:mesEngagementsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
    
    
}
-(void)ChangerTiers:(BOOL)dash{
     [self.monprofil setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    if (affichedViewController != kPAGE_CHANGERTIERS)
    {
        if (changerTiersViewController)
        {
            changerTiersViewController = nil;
            [changerTiersViewController release];
        }
        changerTiersViewController = [[ChangerTiersViewController_ipad alloc] initWithNibName:@"ChangerTiersViewController_ipad" bundle:nil];
        
        changerTiersViewController.isFromButton = YES;
        changerTiersViewController.isFromDash = dash;
    }
    
    affichedViewController = kPAGE_CHANGERTIERS;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:changerTiersViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
}


-(void)showYoutube:(BOOL)dash{
    
    
    if (affichedViewController != kPAGE_YOUTUBE)
    {
        if (youtubeVC)
        {
            youtubeVC = nil;
            [youtubeVC release];
        }
        youtubeVC = [[ReseauxSociauxVC_ipad alloc] initWithNibName:@"ReseauxSociauxVC_ipad" bundle:nil];
    }
    youtubeVC.isFromDash = dash;
    youtubeVC.type = 30; //FACEBOOK
    
    affichedViewController = kPAGE_YOUTUBE;
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:youtubeVC];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
}
@end
