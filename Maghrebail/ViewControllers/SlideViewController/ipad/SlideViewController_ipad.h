//
//  SlideViewController_ipad.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/6/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SlideViewController.h"
#import "MessagerieViewController_ipad.h"
#import "AuthentificationViewController_ipad.h"
#import "MesGarantiesViewController_ipad.h"
#import "MesFacturesViewController_ipad.h"
#import "EcheanvierViewController_ipad.h"
#import "MonSoldeViewController_ipad.h"
#import "DetailsProfessionnelsVC_ipad.h"

@interface SlideViewController_ipad : SlideViewController

@end
