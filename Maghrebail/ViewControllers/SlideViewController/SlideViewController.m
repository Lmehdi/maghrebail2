//
//  SlideViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SlideViewController.h"
#import "DataManager.h"
#import "BlockAlertView.h"

@interface SlideViewController ()

@end

@implementation SlideViewController 

@synthesize sideMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (!IS_IPAD) {
        
        if ([DataManager isIphoneX])
        {
            // code for 4-inch screen
            nibNameOrNil = [NSString stringWithFormat:@"%@X", nibNameOrNil];
        }
        
        if ([DataManager isIphone5])
        {
            // code for 4-inch screen
            nibNameOrNil = [NSString stringWithFormat:@"%@5", nibNameOrNil];
        }
        if ([DataManager isIphone6])
        {
            // code for 4-inch screen
            nibNameOrNil = [NSString stringWithFormat:@"%@6", nibNameOrNil];
        }
        if ([DataManager isIphone6Plus])
        {
            // code for 4-inch screen
            nibNameOrNil = [NSString stringWithFormat:@"%@6Plus", nibNameOrNil];
        }
//		if ([DataManager isIphone6Plus])
//			{
//				// code for 4-inch screen
//			nibNameOrNil = [NSString stringWithFormat:@"%@6Plus", nibNameOrNil];
//			}
		
    }
  
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
        // Custom initialization
        isDecouvrirDisplayed = YES;
        isInformationDisplayed = YES;
        isEspaceAbonneDisplayed = YES;
        isProduitDisplayed = YES;
		isRedo = NO;
    }
	dataManager = [DataManager sharedManager];
    return self;
}

/*
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (IS_IPAD)
    {
        return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    return NO;
}

-(BOOL)shouldAutorotate
{
    if (IS_IPAD) {
        return YES ;
    }
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
	//iOS 6
    
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    
    if (6 <= [[versionCompatibility objectAtIndex:0] intValue])
    {
        if (IS_IPAD) {
            return 16 | 8 ;
        }
        return 2 | 16 | 8 | 4;
    }else{
        if (IS_IPAD) {
            return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight ;
        }
        return UIInterfaceOrientationPortrait | UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationPortraitUpsideDown;
    }
	
}
*/
- (void)viewDidLoad
{
    [super viewDidLoad];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStatusBarFrameChanged) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];

	self.aryBtns = [NSMutableArray array];
    // Do any additional setup after loading the view from its nib.
    [self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.scrollSideMenu.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height)];
    self.viewdemo.hidden=YES;
    self.viewCompte.hidden = YES;
    self.viewcomptefournisseur.hidden=YES;
    self.viewEspaceAbonneInitial.hidden = NO;
    self.labelName.hidden = YES; 
    if (dataManager.authentificationData)
    {
        [self showCompteMenu];
    }
	else
	{ 
		self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(reset)
												 name:@"showMenu"
											   object:nil];
	 
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showCompte)
												 name:@"showCompte"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showProduit)
												 name:@"showMenuPushed"
											   object:nil];
	
	[self decouvrirPushed:nil];
	[self espaceAbonnePushed:nil];
	[self informationPushed:nil];
	if (!isDecouvrirDisplayed)
    {
        [self decouvrirPushed:nil];
    }
	if (!isEspaceAbonneDisplayed)
    {
        [self espaceAbonnePushed:nil];
    }
    if (!isInformationDisplayed)
    {
        [self informationPushed:nil];
    }
    if (isProduitDisplayed)
    {
        [self produitPushed:nil];
    }
	
	switch (dataManager.typeDash)
	{
		case 1:
			[self.btnDecouvrir setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 2:
			[self.btnFaq setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 3:
			if (dataManager.authentificationData)
			{
				[self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			}
			else
			[self.btnEspace1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		default:
			break;
	}
    
}
/*
- (void) didStatusBarFrameChanged {
    int newY        =   self.view.frame.size.height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
                     }];
}
 */


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (dataManager.appgroupe.count != 0)
    {
        self.viewBMCE.hidden = YES;
        self.viewRMA.hidden = YES;

        [self.btnAppGroupe setTitle:[[NSString stringWithFormat:@"   %@", [[dataManager.appgroupe objectAtIndex:0] objectForKey:@"menu"]] uppercaseString] forState:UIControlStateNormal];
    }

    self.viewCanvas.hidden = YES;
    int yPos = self.viewCanvas.frame.origin.y;

    for (NSInteger i = 0 ; i < dataManager.appgroupe.count; i++)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.viewCanvas.frame.size.width, self.viewCanvas.frame.size.height)];

        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = self.btnCanvas.frame;
        [btn setTitle:[NSString stringWithFormat:@"           %@",[[dataManager.appgroupe objectAtIndex:i] objectForKey:@"titre"]] forState:UIControlStateNormal];

        [btn setTitleColor:[DataManager colorWithHexString:[[dataManager.appgroupe objectAtIndex:i] objectForKey:@"titire_color"]] forState:UIControlStateNormal];
        btn.backgroundColor = self.btnCanvas.backgroundColor;
        btn.tag = i;
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

        [btn addTarget:self action:@selector(btnAppGroupePushed:) forControlEvents:UIControlEventTouchUpInside];

        RemoteImageView *img = [[RemoteImageView alloc] initWithFrame:self.imgLogoCanvas.frame];
        [img loadUsingCache:YES];
        [img loadFromURL:[[dataManager.appgroupe objectAtIndex:i] objectForKey:@"icone"]];

        btn.titleLabel.font = self.btnCanvas.titleLabel.font;

        UIImageView *imgLine = [[UIImageView alloc] initWithFrame:self.imgLineCanvas.frame];
        imgLine.backgroundColor = self.imgLineCanvas.backgroundColor;

        [view addSubview:imgLine];
        [view addSubview:img];
        [view addSubview:btn];
        view.backgroundColor = [UIColor clearColor];
        [self.viewInformation addSubview:view];
        yPos += 48;
    }

    [self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];

    for (NSInteger i = 0 ; i<self.aryBtns.count; i++) {

        UIButton *btn = [self.aryBtns objectAtIndex:i];
        if (btn.tag != EXIT_PAGE)
            [[self.aryBtns objectAtIndex:i ] setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
    }

    [self.btnEspace1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.scrollSideMenu setContentOffset:CGPointMake(0, 0)];

    if (!dataManager.authentificationData)
    {
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, 400 + (48 * dataManager.appgroupe.count));

        self.viewLogoutDemo.hidden = NO;
        self.viewPwd.hidden = YES;
        //[self.btnExitDemo setTitle:@"      Mon Mot De Passe" forState:UIControlStateNormal];
        self.viewdemo.hidden=YES;
        self.viewCompte.hidden = YES;
        self.viewcomptefournisseur.hidden=YES;
        self.viewEspaceAbonneInitial.hidden = NO;
        self.labelName.hidden = YES;



        if (!isDecouvrirDisplayed)
        {
            [self decouvrirPushed:nil];
        }
        if (!isEspaceAbonneDisplayed)
        {
            [self espaceAbonnePushed:nil];
        }
        if (!isInformationDisplayed)
        {
            [self informationPushed:nil];
        }
        if (isProduitDisplayed)
        {
            [self produitPushed:nil];
        }



        [self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.viewInformation.frame.size.height+ self.viewDecouvrir.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height + 20)];

    }
    else
    {

        self.viewLogoutDemo.hidden = YES;
        self.viewPwd.hidden = NO;
        if(dataManager.isClientContent)
        {

            self.viewCompte.hidden = NO;
            self.viewcomptefournisseur.hidden=YES;
            self.viewdemo.hidden=YES;
            self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, 355 + (48 * dataManager.appgroupe.count));

            [self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.viewInformation.frame.size.height+ self.viewDecouvrir.frame.size.height + self.viewCompte.frame.size.height + 20)];

        }

        if(dataManager.isDemo)
        {

            self.viewCompte.hidden = YES;
            self.viewcomptefournisseur.hidden=YES;
            self.viewdemo.hidden=NO;
            self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, 355 + (48 * dataManager.appgroupe.count));

            [self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.viewInformation.frame.size.height+ self.viewDecouvrir.frame.size.height + self.viewdemo.frame.size.height + 20)];

        }



        if(dataManager.isFournisseurContent)
        {
            self.viewdemo.hidden=YES;
            self.viewCompte.hidden = YES;
            self.viewcomptefournisseur.hidden=NO;
            self.viewInformation.hidden=NO;


             self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewcomptefournisseur.frame.origin.y + self.viewcomptefournisseur.frame.size.height, self.viewInformation.frame.size.width, 355 + (48 * dataManager.appgroupe.count));

            [self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.viewInformation.frame.size.height+ self.viewDecouvrir.frame.size.height + self.viewcomptefournisseur.frame.size.height + 20)];



        }

        self.viewEspaceAbonneInitial.hidden = YES;
        self.labelName.hidden = YES;



    }
	
}
- (void)btnAppGroupePushed:(UIButton *)sender
{
    
	if (dataManager.isLandScape && !IS_IPAD)
		return;
	[self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	
	for (NSInteger i = 0 ; i<self.aryBtns.count; i++) {
		
        UIButton *btn = [self.aryBtns objectAtIndex:i];
        if (btn.tag != EXIT_PAGE)
            [[self.aryBtns objectAtIndex:i ] setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1]	forState:UIControlStateNormal];
        
		//[[self.aryBtns objectAtIndex:i ] setBackgroundColor:[UIColor clearColor]];
	}
	
	UIButton *btn = (UIButton*)sender;
    
    [btn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    
    //[btn setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]];
	[self.aryBtns addObject:btn];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/%@?mt=8", [[dataManager.appgroupe objectAtIndex:sender.tag ] objectForKey:@"url_iphone"]]]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  //   [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    [_scrollSideMenu release];
    [_viewCompte release];
    [_labelName release];
    [_viewDecouvrir release];
    [_viewInformation release];
    [_buttonDecouvrir release];
    [_buttonInformation release];
    [_buttonEspaceAbonne release];
    [_viewProduit release];
    [_viewUnderProduit release];
    [_viewEspaceAbonneInitial release];
    [_imgViewFlechProduit release];
	[_btnDecouvrir release];
	[_btnEspace1 release];
	[_btnEspace2 release];
	[_btnFaq release];
	[_btnExitDemo release];
    [_viewLogoutDemo release];
    [_viewPwd release];
    [_viewCanvas release];
    [_btnCanvas release];
    [_imgLogoCanvas release];
    [_imgLineCanvas release];
    [_viewBMCE release];
    [_viewRMA release];
    [_btnAppGroupe release];
    [_viewcomptefournisseur release];
    [_changementtiersclient release];
    [_viewdemo release];
    [_monprofil release];
	[_topSpaceLogog release];
	[_topView release];
	[super dealloc];
}
- (void)viewDidUnload {
    [self setViewCompte:nil];
    [self setLabelName:nil];
    [self setViewDecouvrir:nil];
    [self setViewInformation:nil];
    [self setButtonDecouvrir:nil];
    [self setButtonInformation:nil];
    [self setButtonEspaceAbonne:nil];
    [self setViewProduit:nil];
    [self setViewUnderProduit:nil];
    [self setViewEspaceAbonneInitial:nil];
    [self setImgViewFlechProduit:nil];
	[self setBtnDecouvrir:nil];
	[self setBtnEspace1:nil];
	[self setBtnEspace2:nil];
	[self setBtnFaq:nil];
	[self setBtnExitDemo:nil];
    [self setViewLogoutDemo:nil];
    [self setViewPwd:nil];
    [self setViewCanvas:nil];
    [self setBtnCanvas:nil];
    [self setImgLogoCanvas:nil];
    [self setImgLineCanvas:nil];
    [self setViewBMCE:nil];
    [self setViewRMA:nil];
    [self setBtnAppGroupe:nil];
    [super viewDidUnload];
}

#pragma mark -- Action
- (IBAction)sideButtonPushed:(id)sender {
    
	if (dataManager.isLandScape && !IS_IPAD)
		return;
	[self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	
	for (NSInteger i = 0 ; i<self.aryBtns.count; i++) {
		
        UIButton *btn = [self.aryBtns objectAtIndex:i];
        if (btn.tag != EXIT_PAGE)
            [[self.aryBtns objectAtIndex:i ] setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1]	forState:UIControlStateNormal];
        
		//[[self.aryBtns objectAtIndex:i ] setBackgroundColor:[UIColor clearColor]];
	}
	
	UIButton *btn = (UIButton*)sender;
   
    [btn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
   
    //[btn setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]];
	[self.aryBtns addObject:btn]; 
	
   /* if (isProduitDisplayed && [(UIButton*)sender tag] != kPRODUITS_EQUIPEMENT && [(UIButton*)sender tag] != kPRODUITS_IMMOBILIER)
    {
        [self produitPushed:nil];
    } */
	
    switch ([(UIButton*)sender tag]) {
        case kAUTHENTIFICATION_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/Seconnecter"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Seconnecter"];
            
            dataManager.showdemobutton=NO;
            
            dataManager.isLoad = YES;
         
            [self authentification:NO];
            break;
        case kMON_PROFIL_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/Profil"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/Profil"];
            [self monProfil:NO];
            break;
        case kLIGNE_CREDIT_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/meslignesdecrédits"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meslignesdecrédits"];
            
            
            [self maLigneCredit:NO];
            break;
        case kCONTRATS_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/mescontrats"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mescontrats"];
            [self contrats:NO];
            break;
        case kPRODUITS_EQUIPEMENT:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosproduits/Créditbaildéquipement"];
			else
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosproduits/Créditbaildéquipement"];
          //
           
            if (IS_IPAD) {
                //[self produitEquipement:NO];
                [self detailView:YES andParallaxe:NO andIndex:0];
            }else{
                [self detailView:YES andParallaxe:YES andIndex:0];
            }
            break;
        case kPRODUITS_IMMOBILIER:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosproduits/Créditbaildimmobilier"];
			else
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosproduits/Créditbaildimmobilier"];
           //
            if (IS_IPAD) {
                //[self produitImmobilier:NO];
                  [self detailView:YES andParallaxe:NO andIndex:1];
            }else{
                 [self detailView:YES andParallaxe:YES andIndex:1];
            }
            
            break;
        case MESSAGERIE_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/Messagerie"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/Messagerie"];
            [self maMessagerie:NO];
            break;
            
        case MES_GARANTIES_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/mesgaranties"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesgaranties"];
            [self mesGaranties:NO];
            break;
        case MES_RECLAMATIONS_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/mesréclamations"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesréclamations"];
            [self mesReclamations:NO];
            break;
        case MES_ECHEANCIERS_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers"];
            [self mesEcheanciers:NO];
            break;
        case kFAQ_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/Informations/NotreFAQ"];
			else
				[Flurry logEvent:@"Home/Informations/NotreFAQ"];
            [self faq:NO];
            break;
        case kSIMULATEUR_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Notresimulateur"];
			else
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Notresimulateur"];
            [self simulator:NO];
            break;
        case kNOS_CONTACTS_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/Informations/noscontacts"];
			else
				[Flurry logEvent:@"Home/Informations/noscontacts"];
            [self nosContacts:NO];
			break;
        case MES_FACTURES_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures"];
			else
                dataManager.DemoredirectfactureFournisseur=NO;
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures"];
            [self mesFactures:NO];
            break;
        case kMENTIONS_LEGALES_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/Informations/mentionslégales"];
			else
				[Flurry logEvent:@"Home/Informations/mentionslégales"];
            [self mentionsLegales:NO];
			break;
        case MON_SOLDE_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/mesimpayés"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesimpayés"];
            [self monSolde:NO];
            break;
        case kAGENCES_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosagences"];
			else
				[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosagences"];
            [self agences:NO];
			break;
        case MENTION_LEGALE_PAGE:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/Informations/mentionslégales"];
			else
				[Flurry logEvent:@"Home/Informations/mentionslégales"];
            [self mentionLegale:NO];
            break;
        case MON_PAIMENT_PAGE:
            if (!dataManager.isDemo)
                [Flurry logEvent:@"Home/EspaceAbonnés/mesimpayés"];
            else
                [Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesimpayés"];
            [self monpaiment:NO];
            break;
        case EXIT_PAGE:
        {
				[self reset];
				[Flurry logEvent:@"Home/QuitterDémonstration"];
            dataManager.showdemobutton=NO;
            dataManager.isLoad = YES;
        } break;
        
        case PASSWORD_PAGE:
            
				[self passWord:NO];
				[Flurry logEvent:@"Home/Motdepasse"];
		 
            break;
        case kDEMO:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration"];
            dataManager.isLoad = YES;
            
            [self demo];
            break;
        case kPAGE_MES_CONTACTS:
			if (!dataManager.isDemo)
			{
				[Flurry logEvent:@"Home/EspaceAbonnés/mescontacts"];
				[self mesContacts:NO];
			}
			else
			{
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mescontacts"];
				alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
				[alert show];
			}
			
			[self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
		  
            break;
        case kPAGE_PUBLICATIONS:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/Informations/Nospublications"];
			else
				[Flurry logEvent:@"Home/Informations/Nospublications"];
            [self nosPublications:NO];
            break;
            
        case kPAGE_COMMANDES:
           
            [Flurry logEvent:@"Home/EspaceAbonnés/Commandes"];
            [self mesCommandes:NO];
            break;
            
        case kPAGE_CHANGERTIERS:
            dataManager.isLoad = YES;
            [Flurry logEvent:@"Home/EspaceAbonnés/ChangementTIERS"];
            [self ChangerTiers:NO];
            break;
            
        case kPAGE_REGLEMENTS:
            
            [Flurry logEvent:@"Home/EspaceAbonnés/Reglements"];
            [self mesReglements:NO];
            break;

        case kIMMATRICULATIONS:
            
            [Flurry logEvent:@"Home/EspaceAbonnés/Immatriculations"];
            [self mesImmatriculations:NO];
            break;
            
        case KMESENGAGEMENTS:
            
            [Flurry logEvent:@"Home/EspaceAbonnés/Engagements"];
            [self mesEngagements:NO];
            break;
            
        case kPAGE_INSCRIPTION:
			if (!dataManager.isDemo)
				[Flurry logEvent:@"Home/EspaceAbonnés/Minscrire"];
			else
				[Flurry logEvent:@"Home/EspaceAbonnés/Minscrire"];
            [self inscription:NO];
            break;
        case kPAGE_DOCUMENTS:
            
           // if (!dataManager.isDemo)
		//	{
				[Flurry logEvent:@"Home/EspaceAbonnés/Documents"];
				[self mesDocuments:NO];
		/*	}
			else
			{
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mescontacts"];
				alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
				[alert show];
			}
			
			[self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];*/
            break;
            
            
            
        case kPAGE_RMA:
        {
            [Flurry logEvent:@"Home/ApplicationGroupe/RMA WATANYA"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id579361591?mt=8"]];
        }
            
            break;
        case kPAGE_BMCE:
        {
            [Flurry logEvent:@"Home/ApplicationGroupe/BMCE"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id515211306?mt=8"]];
        }
            
            break;
        case kPAGE_TWITTER:
        {
            [Flurry logEvent:@"Home/ReseauxSociaux/Twitter"];
        }
            [self showTwitter:NO];
            
            break;
        case kPAGE_FACEBOOK:
        {
            [Flurry logEvent:@"Home/ReseauxSociaux/Facebook"];
        }
            [self showFacebook:NO];
            
            break;
        case kPAGE_YOUTUBE:
        {
            [Flurry logEvent:@"Home/ReseauxSociaux/Youtube"];
        }
            [self showYoutube:NO];
        default:
            break;
    }
}

-(void)dismissAlert
{
    if (alert)
    {
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    }
	
}
- (IBAction)decouvrirPushed:(UIButton *)sender
{
}

- (IBAction)informationPushed:(UIButton *)sender {
}

- (IBAction)espaceAbonnePushed:(UIButton *)sender {
}

- (IBAction)produitPushed:(UIButton *)sender
{
    
}

- (IBAction)retourPushed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- side Menu actions 


-(void)authentification:(BOOL)dash {}
-(void)monProfil:(BOOL)dash {}
-(void)maLigneCredit:(BOOL)dash {}
-(void)contrats:(BOOL)dash {}

-(void)produitImmobilier:(BOOL)dash {}
-(void)produitEquipement:(BOOL)dash {}
-(void)maMessagerie:(BOOL)dash {}
-(void)mesGaranties:(BOOL)dash {}
-(void)mesReclamations:(BOOL)dash {}
-(void)mesEcheanciers:(BOOL)dash {

}
-(void)faq:(BOOL)dash {}
-(void)simulator:(BOOL)dash {}
-(void)nosContacts:(BOOL)dash {}
-(void)mentionsLegales:(BOOL)dash {}
-(void)agences:(BOOL)dash {}
-(void)mesContacts:(BOOL)dash {}

-(void)mesFactures:(BOOL)dash {}
-(void)monSolde:(BOOL)dash {}
-(void)monpaiment:(BOOL)dash{}
-(void)mentionLegale:(BOOL)dash {}
-(void)passWord:(BOOL)dash {}
-(void)mesCommandes:(BOOL)dash{}
-(void)mesImmatriculations:(BOOL)dash{}
-(void)mesEngagements:(BOOL)dash{}

-(void)mesReglements:(BOOL)dash{}

-(void)nosPublications:(BOOL)dash {}
-(void)mesDocuments:(BOOL)dash {}
-(void)ChangerTiers:(BOOL)dash {}
-(void)showAppGroupe:(BOOL)dash{}

-(void)produitEquipementEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe{}
-(void)produitImmobilierEntreprise:(BOOL)dash andParallaxe:(BOOL) withParallaxe{}
-(void)produitEquipementProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe {}

-(void)produitImmobilierProfessionnel:(BOOL)dash andParallaxe:(BOOL) withParallaxe{}


- (void)demo
{  
	dataManager.isDemo = YES;
	[self showCompte];
}

#pragma mark -- ShowComteMenu delegate
-(void)showProduit
{
	//[[NSNotificationCenter defaultCenter] removeObserver:self];
    if (!isProduitDisplayed)
    {
        [self produitPushed:nil];
    }
}
-(void)reset
{   
	[self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
 
	
	 for (NSInteger i = 0 ; i<self.aryBtns.count; i++) {
         
         UIButton *btn = [self.aryBtns objectAtIndex:i];
         if (btn.tag != EXIT_PAGE)
             [[self.aryBtns objectAtIndex:i ] setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	}
	
	[self.btnEspace1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
	
	
    
    self.viewLogoutDemo.hidden = YES;
    self.viewPwd.hidden = NO;
	//[self.btnExitDemo setTitle:@"      Mon Mot De Passe" forState:UIControlStateNormal];
    self.viewdemo.hidden=YES;
    self.viewCompte.hidden = YES;
    self.viewcomptefournisseur.hidden=YES;
    self.viewEspaceAbonneInitial.hidden = NO;
    self.labelName.hidden = YES;
	
	[self.scrollSideMenu setContentOffset:CGPointMake(0, 0)];
	
	[self authentification:NO];
	
    
	if (!isDecouvrirDisplayed)
    {
        [self decouvrirPushed:nil];
    }
	if (!isEspaceAbonneDisplayed)
    {
        [self espaceAbonnePushed:nil];
    }
    if (!isInformationDisplayed)
    {
        [self informationPushed:nil];
    }
    if (isProduitDisplayed)
    {
        [self produitPushed:nil];
    } 
		 
    
    
    self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewEspaceAbonneInitial.frame.origin.y + self.viewEspaceAbonneInitial.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    
	[self.scrollSideMenu setContentSize:CGSizeMake(self.scrollSideMenu.frame.size.width, self.viewInformation.frame.size.height+ self.viewDecouvrir.frame.size.height + self.viewEspaceAbonneInitial.frame.size.height + 20)];
    
	dataManager.isDemo = NO;
	
	dataManager.isAuthentified = NO;
	NSData *dataAuthentification= [NSKeyedArchiver archivedDataWithRootObject:nil];
	[DataManager writeDataIntoCachWith:dataAuthentification andKey:@"authentificationData"];
	dataManager.authentificationData = nil;	 

	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (standardUserDefaults)
	{ 
		[standardUserDefaults setObject:@"OFF" forKey:@"USER-CONNEXION"];
		[standardUserDefaults synchronize];
	}
 

	dataManager.isAuthentified = NO;
	dataManager.authentificationData = nil;
	//init
	dataManager.mArrayUrlImage = [NSMutableArray array]; 
	dataManager.contrats = [NSMutableArray array];
	dataManager.contacts = [NSMutableArray array];
	dataManager.contratFiltred = [NSMutableArray array];
	dataManager.ligneCredit = [NSMutableArray array];
	dataManager.ligneCreditFiltred = [NSMutableArray array];
	dataManager.profilData = [NSDictionary dictionary];
	
	
}

-(void)showCompteMenu
{
    
    
    
	if (!dataManager.firstTime)
	{
		[self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
	}
	
	[self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	
	
	switch (dataManager.typeDash)
	{
		case 1:
            [self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			[self.monprofil setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 2:
			[self.btnFaq setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 3:
			if (dataManager.authentificationData)
			{
				[self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			}
			else
				[self.btnEspace1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		default:
			break;
	}
	
	//[self.btnExitDemo setTitle:@"      Mon Mot De Passe" forState:UIControlStateNormal];
    self.viewLogoutDemo.hidden = YES;
    self.viewPwd.hidden = NO;
    
	self.viewEspaceAbonneInitial.hidden = YES;
    
    if(dataManager.isClientContent)
    {
        
        self.viewdemo.hidden=YES;
        self.viewCompte.hidden = NO;
        self.viewcomptefournisseur.hidden=YES;
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewCompte.frame.size.height + self.viewCompte.frame.origin.y);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    if(dataManager.isDemo)
    {
        
        self.viewdemo.hidden=NO;
        self.viewCompte.hidden = YES;
        self.viewcomptefournisseur.hidden=YES;
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewdemo.frame.size.height + self.viewdemo.frame.origin.y);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
    if(dataManager.isFournisseurContent)
    {
        self.viewdemo.hidden=YES;
        self.viewCompte.hidden = YES;
        self.viewcomptefournisseur.hidden=NO;
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewcomptefournisseur.frame.size.height + self.viewcomptefournisseur.frame.origin.y-100);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, CGRectGetMaxY(self.viewcomptefournisseur.frame), self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
        
    }
    
    
   
	
	//[self.scrollSideMenu setContentOffset:CGPointMake(0, 0)];
	
	[self decouvrirPushed:nil];
	[self espaceAbonnePushed:nil];
	[self informationPushed:nil];
	if (!isDecouvrirDisplayed)
    {
        [self decouvrirPushed:nil];
    }
	if (!isEspaceAbonneDisplayed)
    {
        [self espaceAbonnePushed:nil];
    }
    if (!isInformationDisplayed)
    {
        [self informationPushed:nil];
    }
    if (isProduitDisplayed)
    {
        [self produitPushed:nil];
    }

} 

-(void)showCompte
{
    
	if (dataManager.isDemo)
	{
		//[self.btnExitDemo setTitle:@"           Quitter la Démonstration" forState:UIControlStateNormal];
        self.viewLogoutDemo.hidden = NO;
        self.viewPwd.hidden = YES;
	}
	else
    {
        
		//[self.btnExitDemo setTitle:@"           Mon Mot De Passe" forState:UIControlStateNormal];
        self.viewLogoutDemo.hidden = YES;
        self.viewPwd.hidden = NO;
    }
    
    
	if (!dataManager.firstTime)
	{
		[self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
	}
	
	[self.btnDecouvrir setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace1 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnEspace2 setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	[self.btnFaq setTitleColor:[UIColor colorWithRed:115.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1] forState:UIControlStateNormal];
	
	
	switch (dataManager.typeDash)
	{
		case 1:
			[self.btnDecouvrir setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 2:
			[self.btnFaq setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		case 3:
			if (dataManager.authentificationData)
			{
				[self.btnEspace2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			}
			else
				[self.btnEspace1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
			break;
		default:
			break;
	}
	
	self.viewEspaceAbonneInitial.hidden = YES;
    if(dataManager.isClientContent)
    {   self.viewdemo.hidden=YES;
        self.viewCompte.hidden = NO;
        self.viewcomptefournisseur.hidden=YES;
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewCompte.frame.size.height + self.viewCompte.frame.origin.y);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewCompte.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewCompte.frame.origin.y + self.viewCompte.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
    
    if(dataManager.isDemo)
    {   self.viewdemo.hidden=NO;
        self.viewCompte.hidden = YES;
        self.viewcomptefournisseur.hidden=YES;
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewdemo.frame.size.height + self.viewdemo.frame.origin.y);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewdemo.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, self.viewdemo.frame.origin.y + self.viewdemo.frame.size.height, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);
    }
    
    
    
    
    
    if(dataManager.isFournisseurContent)
    {   self.viewdemo.hidden=YES;
        self.viewCompte.hidden = YES;
        self.viewcomptefournisseur.hidden=NO;
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewcomptefournisseur.frame.size.height + self.viewcomptefournisseur.frame.origin.y);
        
        
        self.scrollSideMenu.contentSize = CGSizeMake(self.scrollSideMenu.contentSize.width, self.viewDecouvrir.frame.origin.y+self.viewDecouvrir.frame.size.height + self.viewInformation.frame.size.height + self.viewcomptefournisseur.frame.size.height+20);
        
        self.viewInformation.frame = CGRectMake(self.viewInformation.frame.origin.x, CGRectGetMaxY(self.viewcomptefournisseur.frame)-300, self.viewInformation.frame.size.width, self.viewInformation.frame.size.height);

     }
   
	
	//[self.scrollSideMenu setContentOffset:CGPointMake(0, 0)];
	//lorsque le user clique sur les titres      sur le slide ca se regroupe et ca s'ouvre
	[self decouvrirPushed:nil];
	[self espaceAbonnePushed:nil];
	[self informationPushed:nil];
	if (!isDecouvrirDisplayed)
    {
        [self decouvrirPushed:nil];
    }
	if (!isEspaceAbonneDisplayed)
    {
        [self espaceAbonnePushed:nil];
    }
    if (!isInformationDisplayed)
    {
        [self informationPushed:nil];
    }
    if (isProduitDisplayed)
    {
        [self produitPushed:nil];
    }
	
}
 

@end
