//
//  DashSlideVC.m
//  LaGold
//
//  Created by MAC on 21/06/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DashSlideVC.h"

@interface DashSlideVC ()

@end

@implementation DashSlideVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_imgViewSlider release]; 
    [super dealloc];
}

@end
