//
//  DashSlideVC.h
//  LaGold
//
//  Created by MAC on 21/06/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteImageView.h"
#import "DashboardViewController.h"

@interface DashSlideVC : UIViewController

@property (retain, nonatomic) IBOutlet RemoteImageView *imgViewSlider; 

@end
