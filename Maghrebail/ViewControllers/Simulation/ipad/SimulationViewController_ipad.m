//
//  SimulationViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 12/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SimulationViewController_ipad.h"
#import "AppDelegate.h"

@interface SimulationViewController_ipad ()

@end

@implementation SimulationViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate* delegate=[[UIApplication sharedApplication] delegate];
    delegate.m_SideMenu.panMode=CHMFSideMenuPanModeNone;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
