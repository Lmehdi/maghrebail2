//
//  SimulationViewController.h
//  Maghrebail
//
//  Created by MAC on 26/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

#define kTagApport				10
#define kTagMontant				20
#define kTagDuree				30
#define kTagMensualite			40
#define kTagValResid			50

#define kStepApport				5
#define kStepMontant			20000
#define kStepDuree				12

#define kStepMensualite			5000
#define kStepValResid			5000

@interface SimulationViewController : BaseViewController
{
	double vLastApport;
	double vLastMontant;
}

@property (retain, nonatomic) IBOutlet UIScrollView *scrollContainer;

@property (retain, nonatomic) IBOutlet UITextField *txtFldApport;
@property (retain, nonatomic) IBOutlet UITextField *txtFldMontant;
@property (retain, nonatomic) IBOutlet UITextField *txtFldDuree;
@property (retain, nonatomic) IBOutlet UITextField *txtFldMensualite;
@property (retain, nonatomic) IBOutlet UITextField *txtFldValResid;

@property (retain, nonatomic) IBOutlet UIButton *btnScrollApport;
@property (retain, nonatomic) IBOutlet UIButton *btnScrollMontant;
@property (retain, nonatomic) IBOutlet UIButton *btnScrollDuree;
@property (retain, nonatomic) IBOutlet UIButton *btnScrollMensualite;
@property (retain, nonatomic) IBOutlet UIButton *btnScrollValResid;

@property (retain, nonatomic) IBOutlet UIImageView *imgViewApport;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewMontant;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewDuree;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewMensualite;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewValResid;

@property (retain, nonatomic) IBOutlet UILabel *lblMinApport;
@property (retain, nonatomic) IBOutlet UILabel *lblMaxApport;

@property (retain, nonatomic) IBOutlet UILabel *lblMinMontant;
@property (retain, nonatomic) IBOutlet UILabel *lblMaxMontant;

@property (retain, nonatomic) IBOutlet UILabel *lblMinDuree;
@property (retain, nonatomic) IBOutlet UILabel *lblMaxDuree;

@property (retain, nonatomic) IBOutlet UILabel *lblMinMensualite;
@property (retain, nonatomic) IBOutlet UILabel *lblMaxMensualite;

@property (retain, nonatomic) IBOutlet UILabel *lblMinValResid;
@property (retain, nonatomic) IBOutlet UILabel *lblMaxValResid;

@property (retain, nonatomic) NSArray *arrayValuesApport;
@property (retain, nonatomic) NSArray *arrayValuesMontant;
@property (retain, nonatomic) NSArray *arrayValuesDuree;
@property (retain, nonatomic) NSArray *arrayValuesMensualite;
@property (retain, nonatomic) NSArray *arrayValuesValResid; 

@property (retain, nonatomic) NSArray *arrayCenterXApport;
@property (retain, nonatomic) NSArray *arrayCenterXMontant;
@property (retain, nonatomic) NSArray *arrayCenterXDuree;
@property (retain, nonatomic) NSArray *arrayCenterXMensualite;
@property (retain, nonatomic) NSArray *arrayCenterXValResid;

@property (assign, nonatomic) NSInteger minApport;
@property (assign, nonatomic) NSInteger maxApport;
@property (assign, nonatomic) NSInteger minMontant;
@property (assign, nonatomic) NSInteger maxMontant;
@property (assign, nonatomic) NSInteger minDuree;
@property (assign, nonatomic) NSInteger maxDuree;
@property (assign, nonatomic) NSInteger minMensualite;
@property (assign, nonatomic) NSInteger maxMensualite;
@property (assign, nonatomic) NSInteger minValResid;
@property (assign, nonatomic) NSInteger maxValResid; 
 
@property (assign, nonatomic) double vApport;
@property (assign, nonatomic) double vMontant;
@property (assign, nonatomic) double vDuree;
@property (assign, nonatomic) double vMensualite;
@property (assign, nonatomic) double vValResid;

@property (assign, nonatomic) float maxTaux;

-(void) initHeaderColor;
- (void)addActionTo:(UIButton *)button withTag:(NSInteger)tag;
- (void)setPaddingViewTo:(UITextField *)txtFld;

- (NSString *)getFormatedNumero:(NSString *)numero;
- (NSString *)resetFormatedNumero:(NSString *)numero;
- (void)initiatView;

@end
