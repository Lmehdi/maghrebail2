//
//  SimulationViewController.m
//  Maghrebail
//
//  Created by MAC on 26/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SimulationViewController.h"
#import "JSON.h"

@interface SimulationViewController ()

@end

@implementation SimulationViewController

@synthesize arrayValuesApport;
@synthesize arrayValuesMontant;
@synthesize arrayValuesDuree;
@synthesize arrayValuesMensualite;
@synthesize arrayValuesValResid;

@synthesize arrayCenterXApport;
@synthesize arrayCenterXMontant;
@synthesize arrayCenterXDuree;
@synthesize arrayCenterXMensualite;
@synthesize arrayCenterXValResid;

@synthesize minApport;
@synthesize maxApport;
@synthesize minMontant;
@synthesize maxMontant;
@synthesize minDuree;
@synthesize maxDuree;
@synthesize minMensualite;
@synthesize maxMensualite;
@synthesize minValResid;
@synthesize maxValResid;

@synthesize vApport;
@synthesize vMontant;
@synthesize vDuree;
@synthesize vMensualite;
@synthesize vValResid;

@synthesize maxTaux;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	shouldAutorate = NO;
    [self initiatView];
	 
    [self.scrollContainer setContentSize:CGSizeMake(self.scrollContainer.frame.size.width, 93*5 + 20)];
	[self resetForm];
	
	[self setPaddingViewTo:self.txtFldApport];
	[self setPaddingViewTo:self.txtFldMontant];
	[self setPaddingViewTo:self.txtFldDuree];
	[self setPaddingViewTo:self.txtFldMensualite];
	[self setPaddingViewTo:self.txtFldValResid];
    
	[self addActionTo:self.btnScrollApport withTag:kTagApport];
	[self addActionTo:self.btnScrollMontant withTag:kTagMontant];
	[self addActionTo:self.btnScrollDuree withTag:kTagDuree];
	[self addActionTo:self.btnScrollMensualite withTag:kTagMensualite];
	[self addActionTo:self.btnScrollValResid withTag:kTagValResid];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"NOTRE SIMULATEUR";
    headerView.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    dataManager.typeColor=@"blue";
}
-(void)initiatView
{
    /*[self.scrollContainer setContentSize:CGSizeMake(self.scrollContainer.frame.size.width, 93*5 + 20)];
	[self resetForm];
	
	[self setPaddingViewTo:self.txtFldApport];
	[self setPaddingViewTo:self.txtFldMontant];
	[self setPaddingViewTo:self.txtFldDuree];
	[self setPaddingViewTo:self.txtFldMensualite];
	[self setPaddingViewTo:self.txtFldValResid];
    
	[self addActionTo:self.btnScrollApport withTag:kTagApport];
	[self addActionTo:self.btnScrollMontant withTag:kTagMontant];
	[self addActionTo:self.btnScrollDuree withTag:kTagDuree];
	[self addActionTo:self.btnScrollMensualite withTag:kTagMensualite];
	[self addActionTo:self.btnScrollValResid withTag:kTagValResid];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[_scrollContainer release];
	[_txtFldApport release];
	[_txtFldMontant release];
	[_txtFldDuree release];
	[_txtFldMensualite release];
	[_txtFldValResid release];
	[_btnScrollApport release];
	[_btnScrollMontant release];
	[_btnScrollDuree release];
	[_btnScrollMensualite release];
	[_btnScrollValResid release];
	[_imgViewApport release];
	[_imgViewMontant release];
	[_imgViewDuree release];
	[_imgViewMensualite release];
	[_imgViewValResid release];
	[_lblMinApport release];
	[_lblMaxApport release];
	[_lblMinMontant release];
	[_lblMaxMontant release];
	[_lblMinDuree release];
	[_lblMaxDuree release];
	[_lblMinMensualite release];
	[_lblMaxMensualite release];
	[_lblMinValResid release];
	[_lblMaxValResid release];
	
	[arrayValuesApport release];
	[arrayValuesMontant release];
	[arrayValuesDuree release];
	[arrayValuesMensualite release];
	[arrayValuesValResid release];
	
	[arrayCenterXApport release];
	[arrayCenterXMontant release];
	[arrayCenterXDuree release];
	[arrayCenterXMensualite release];
	[arrayCenterXValResid release];
	
    [super dealloc];
}
- (void)viewDidUnload
{
	[self setScrollContainer:nil];
	[self setTxtFldApport:nil];
	[self setTxtFldMontant:nil];
	[self setTxtFldDuree:nil];
	[self setTxtFldMensualite:nil];
	[self setTxtFldValResid:nil];
	[self setBtnScrollApport:nil];
	[self setBtnScrollMontant:nil];
	[self setBtnScrollDuree:nil];
	[self setBtnScrollMensualite:nil];
	[self setBtnScrollValResid:nil];
	[self setImgViewApport:nil];
	[self setImgViewMontant:nil];
	[self setImgViewDuree:nil];
	[self setImgViewMensualite:nil];
	[self setImgViewValResid:nil];
	[self setLblMinApport:nil];
	[self setLblMaxApport:nil];
	[self setLblMinMontant:nil];
	[self setLblMaxMontant:nil];
	[self setLblMinDuree:nil];
	[self setLblMaxDuree:nil];
	[self setLblMinMensualite:nil];
	[self setLblMaxMensualite:nil];
	[self setLblMinValResid:nil];
	[self setLblMaxValResid:nil];
    [super viewDidUnload];
}


#pragma mark - textField methods

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	NSInteger value = [[textField.text stringByReplacingOccurrencesOfString:@" " withString:@""] integerValue];
	if (textField == self.txtFldMontant)
	{
		if (value <= self.maxMontant && value >= self.minMontant)
			[self moveSlider:kTagMontant withValue:value redo:YES];
		else
		{
			self.txtFldMontant.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMontant]];
			[self moveSlider:kTagMontant withValue:self.minMontant redo:YES];
		}
		
		[self moveSlider:kTagValResid withValue:vMontant / 100 redo:NO];
	}
	else
	{
		if (textField == self.txtFldDuree)
		{
			if (value <= self.maxDuree && value >= self.minDuree)
				[self moveSlider:kTagDuree withValue:value redo:YES];
			else
			{
				self.txtFldDuree.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minDuree]];
				[self moveSlider:kTagDuree withValue:self.minDuree redo:YES];
			}
		}
		else
		{
			if (value <= self.maxApport && value >= self.minApport)
				[self moveSlider:kTagApport withValue:value redo:YES];
			else
			{
				self.txtFldApport.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minApport]];
				[self moveSlider:kTagApport withValue:self.minDuree redo:YES];
			}
		}
	}
	
	[self.scrollContainer setContentOffset:CGPointMake(0, 0 ) animated:YES];
	return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	 
	NSInteger value = [[textField.text stringByReplacingOccurrencesOfString:@" " withString:@""] integerValue];
	if (textField == self.txtFldMontant)
	{
		if (value <= self.maxMontant && value >= self.minMontant)
			[self moveSlider:kTagMontant withValue:value redo:YES];
		else
		{
			self.txtFldMontant.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMontant]];
			[self moveSlider:kTagMontant withValue:self.minMontant redo:YES];
		}
		
		[self moveSlider:kTagValResid withValue:vMontant / 100 redo:NO];
	}
	else
	{
		if (textField == self.txtFldDuree)
		{
			if (value <= self.maxDuree && value >= self.minDuree)
				[self moveSlider:kTagDuree withValue:value redo:YES];
			else
			{
				self.txtFldDuree.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minDuree]];
				[self moveSlider:kTagDuree withValue:self.minDuree redo:YES];
			}
		}
		else
		{
			if (value <= self.maxApport && value >= self.minApport)
				[self moveSlider:kTagApport withValue:value redo:YES];
			else
			{
				self.txtFldApport.text = [DataManager getFormatedNumero:[NSString stringWithFormat:@"%d", self.minApport]];
				[self moveSlider:kTagApport withValue:self.minDuree redo:YES];
			}
		}
	}

	[self.scrollContainer setContentOffset:CGPointMake(0, 0 ) animated:YES];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
	
	for (int i = 0; i < [string length]; i++)
	{
		unichar c = [string characterAtIndex:i];
		if (![myCharSet characterIsMember:c])
		{
			return NO;
		}
	}
	return  YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{ 
	switch (textField.tag)
	{
		case 10:
			[self.scrollContainer setContentOffset:CGPointMake(0, 90 ) animated:YES];
			break;
		case 20:
			[self.scrollContainer setContentOffset:CGPointMake(0, 0 ) animated:YES];
			break;
		case 30:
			[self.scrollContainer setContentOffset:CGPointMake(0, 180 ) animated:YES];
			break;
		case 40:
			[self.scrollContainer setContentOffset:CGPointMake(0, 270 ) animated:YES];
			break;
		case 50:
			[self.scrollContainer setContentOffset:CGPointMake(0, 360 ) animated:YES];
			break; 
			
		default:
			break;
	}
	return YES;
}

- (void)hideKeyBoard
{
	[self.txtFldApport resignFirstResponder];
	[self.txtFldMontant resignFirstResponder];
	[self.txtFldDuree resignFirstResponder];
	[self.txtFldMensualite resignFirstResponder];
	[self.txtFldValResid resignFirstResponder];
}

- (void)setPaddingViewTo:(UITextField *)txtFld
{
	UIView *paddingView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, txtFld.frame.size.height)] autorelease];
	txtFld.leftView = paddingView;
    txtFld.leftViewMode = UITextFieldViewModeAlways;
}

- (void)addActionTo:(UIButton *)button withTag:(NSInteger)tag
{
	button.tag = tag;
    [button addTarget:self action:@selector(minScrollDraggedBegin:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [button addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit | UIControlEventTouchUpInside];
	button.enabled = YES;
}

- (void)setArrays:(NSInteger)tag
{
	int xWidth = 300;
	if (IS_IPAD)
	{
		xWidth = 947;
	}
	double vMin;
	double vMax;
	double vStep;
	
	switch (tag)
	{
		case kTagApport:
			vMin = self.minApport;
			vMax = self.maxApport;
			vStep = kStepApport;
			break;
			
		case kTagMontant:
			vMin = self.minMontant;
			vMax = self.maxMontant;
			vStep = kStepMontant;
			break;
			
		case kTagDuree:
			vMin = self.minDuree;
			vMax = self.maxDuree;
			vStep = kStepDuree;
			break;
			
		case kTagMensualite:
			vMin = self.minMensualite;
			vMax = self.maxMensualite;
			vStep = kStepMensualite;
			break;
			
		case kTagValResid:
			vMin = self.minValResid;
			vMax = self.maxValResid;
			vStep = kStepValResid;
			break;
		
		default:
			break;
	}
	
    NSMutableArray *arrayValues = [[NSMutableArray alloc] init];
	
    for (int i = vMin; i < vMax; i = i + vStep)
        [arrayValues addObject:[NSNumber numberWithInt:i]];

	[arrayValues addObject:[NSNumber numberWithInt:vMax]];
	
    NSMutableArray *arrayCenterX = [[NSMutableArray alloc] init];
    float pas = (float)(xWidth-20)/[arrayValues count];
	
    for (float i = 20; i <= xWidth; i = i + pas)
        [arrayCenterX addObject:[NSNumber numberWithInt:i]];
	
	switch (tag)
	{
		case kTagApport:
			self.arrayValuesApport = [[NSArray alloc] initWithArray:arrayValues];
			self.arrayCenterXApport = [[NSArray alloc] initWithArray:arrayCenterX];
			break;
			
		case kTagMontant:
			self.arrayValuesMontant = [[NSArray alloc] initWithArray:arrayValues];
			self.arrayCenterXMontant = [[NSArray alloc] initWithArray:arrayCenterX];
			break;
			
		case kTagDuree:
			self.arrayValuesDuree = [[NSArray alloc] initWithArray:arrayValues];
			self.arrayCenterXDuree = [[NSArray alloc] initWithArray:arrayCenterX];
			break;

		case kTagMensualite:
			self.arrayValuesMensualite = [[NSArray alloc] initWithArray:arrayValues];
			self.arrayCenterXMensualite = [[NSArray alloc] initWithArray:arrayCenterX];
			break;
			
		case kTagValResid:
			self.arrayValuesValResid = [[NSArray alloc] initWithArray:arrayValues];
			self.arrayCenterXValResid = [[NSArray alloc] initWithArray:arrayCenterX];
			break;
		default:
			break;
	}
}

- (void)resetForm
{ 
	self.minApport = self.minMensualite = self.maxMensualite = 0;
	if (![dataManager.simulateur isEqualToString:@""]) {
		
		NSDictionary *responseDico = [dataManager.simulateur JSONValue];
		self.minApport = [[[responseDico objectForKey:@"response"] objectForKey:@"premier_loyer_min"] doubleValue] ;
		self.maxApport = [[[responseDico objectForKey:@"response"] objectForKey:@"premier_loyer_max"] doubleValue] ;
		self.minMontant = [[[responseDico objectForKey:@"response"] objectForKey:@"montant_financement_min"] doubleValue] ;
		self.maxMontant = [[[responseDico objectForKey:@"response"] objectForKey:@"montant_financement_max"] doubleValue] ;
		self.minDuree = [[[responseDico objectForKey:@"response"] objectForKey:@"duree_min"] doubleValue] ;
		self.maxDuree = [[[responseDico objectForKey:@"response"] objectForKey:@"duree_max"] doubleValue] ; 
		self.maxTaux = [[[responseDico objectForKey:@"response"] objectForKey:@"taux"] doubleValue] ;
	}
	else
	{
		self.maxApport = 50;
		self.minMontant = 75000;
		self.maxMontant = 2000000;
		self.minDuree = 24;
		self.maxDuree = 80;
		self.minValResid = 5000;
		self.maxValResid = 500000;
		self.maxTaux = 10.0;
	}
 
	self.minMensualite = lroundf([self calculMensualite:self.minMontant andMois:self.maxDuree]) - 1;
	self.maxMensualite = lroundf([self calculMensualite:self.maxMontant andMois:self.minDuree]) + 1;
	
	self.txtFldApport.text = [self getFormatedNumero:[NSString stringWithFormat:@"%d", self.minApport]];
	self.txtFldMontant.text = [self getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMontant]];
	self.txtFldDuree.text = [self getFormatedNumero:[NSString stringWithFormat:@"%d", self.minDuree]];
	self.txtFldMensualite.text = [self getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMensualite]];
	self.txtFldValResid.text = [self getFormatedNumero:[NSString stringWithFormat:@"%d", self.minValResid]];
	
	vApport = self.minApport;
	vMontant = self.minMontant;
	vDuree = self.minDuree;
	vMensualite = self.minMensualite;
	vValResid = self.minValResid;
	
	[self setArrays:kTagApport];
	[self setArrays:kTagMontant];
	[self setArrays:kTagDuree];
	[self setArrays:kTagMensualite];
	[self setArrays:kTagValResid];
	 
	self.lblMinApport.text = [NSString stringWithFormat:@"%@ %%", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.minApport]]];
	self.lblMaxApport.text = [NSString stringWithFormat:@"%@ %%", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.maxApport]]];
	
	self.lblMinMontant.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMontant]]];
	self.lblMaxMontant.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.maxMontant]]];
	
	self.lblMinDuree.text = [NSString stringWithFormat:@"%@ Mois", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.minDuree]]];
	self.lblMaxDuree.text = [NSString stringWithFormat:@"%@ Mois", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.maxDuree]]];
	
	self.lblMinMensualite.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.minMensualite]]];
	self.lblMaxMensualite.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.maxMensualite]]];
	
	self.lblMinValResid.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.minValResid]]];
	self.lblMaxValResid.text = [NSString stringWithFormat:@"%@ Dhs", [self  getFormatedNumero:[NSString stringWithFormat:@"%d", self.maxValResid]]];
	
	double value = [self calculMensualite:vMontant andMois:vDuree];
	[self moveSlider:kTagMensualite withValue:value redo:NO];
	[self moveSlider:kTagMontant withValue:self.minMontant redo:NO];
	[self moveSlider:kTagDuree withValue:self.minDuree redo:NO];
	[self moveSlider:kTagValResid withValue:vMontant / 100 redo:NO];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	self.navigationController.sideMenu.recognizer.enabled = NO;
    
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

#pragma mark - scroll methods
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
	[self.txtFldApport resignFirstResponder];
	[self.txtFldDuree resignFirstResponder];
	[self.txtFldMensualite resignFirstResponder];
	[self.txtFldMontant resignFirstResponder]; 
}

- (void)minScrollDraggedBegin:(UIButton *)button withEvent:(UIEvent *)event {
	
	int xWidth = 300;
	if (IS_IPAD)
	{
		xWidth = 947;
	}
    self.navigationController.sideMenu.recognizer.enabled = NO;
	[self hideKeyBoard];
	double vMin;
	double vMax;
	double vStep;
	NSArray *arrayValues;
	NSArray *arrayCenterX;
	
	switch (button.tag)
	{
		case kTagApport:
			vMin = self.minApport;
			vMax = self.maxApport;
			vStep = kStepApport;
			arrayValues = self.arrayValuesApport;
			arrayCenterX = self.arrayCenterXApport;
			if (vMontant == self.minMontant)
			{
				return;
			}
			break;
		case kTagMontant:
			vMin = self.minMontant;
			vMax = self.maxMontant;
			vStep = kStepMontant;
			arrayValues = self.arrayValuesMontant;
			arrayCenterX = self.arrayCenterXMontant;
			break;
		case kTagDuree:
			vMin = self.minDuree;
			vMax = self.maxDuree;
			vStep = kStepDuree;
			arrayValues = self.arrayValuesDuree;
			arrayCenterX = self.arrayCenterXDuree;
			break;
		case kTagMensualite:
			vMin = self.minMensualite;
			vMax = self.maxMensualite;
			vStep = kStepMensualite;
			arrayValues = self.arrayValuesMensualite;
			arrayCenterX = self.arrayCenterXMensualite;
			break;
		case kTagValResid:
			vMin = self.minValResid;
			vMax = self.maxValResid;
			vStep = kStepValResid;
			arrayValues = self.arrayValuesValResid;
			arrayCenterX = self.arrayCenterXValResid;
			break;
			 
		default:
			break;
	}
	
	// get the touch
	UITouch *touch = [[event touchesForView:button] anyObject];
    
	// get delta
	CGPoint previousLocation = [touch previousLocationInView:button];
	CGPoint location = [touch locationInView:button];
	CGFloat delta_x = location.x - previousLocation.x;
    CGFloat newCenter_x = button.center.x + delta_x;
	// move button
    if (newCenter_x > 20 && newCenter_x <= xWidth )
    {
        button.center = CGPointMake(newCenter_x, button.center.y);
    }
    float noteValue = vMax;
	
	for (int i = 0; i < arrayCenterX.count; i++)
    {
        NSNumber *theX = [arrayCenterX objectAtIndex:i];
        if (button.center.x <= [theX intValue])
        {
            NSNumber *theD = nil;
			
            if (i >= [arrayValues count])
            {
                theD = [arrayValues objectAtIndex:[arrayValues count]-1];
            }
            else
            {
				if (i==0)
					theD = [arrayValues objectAtIndex:i];
				else
					theD = [arrayValues objectAtIndex:i-1];
				
            }
            noteValue = [theD intValue];
            break;
        }
    }
	NSLog(@"%f",noteValue);
    if (noteValue >= vMax)
    {
        noteValue = vMax;
    }
    if (noteValue <= vMin)
    {
        noteValue = vMin;
    }
	
	CGRect frame;
	switch (button.tag)
	{
		case kTagApport:
			[self.txtFldApport setText:[NSString stringWithFormat:@"%.f",noteValue]];
			frame = self.imgViewApport.frame;
			frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
			self.imgViewApport.frame = frame;
			self.txtFldApport.text = [self getFormatedNumero:self.txtFldApport.text];
			vApport = noteValue;
			break;
		case kTagMontant:
			[self.txtFldMontant setText:[NSString stringWithFormat:@"%.f",noteValue]];
			frame = self.imgViewMontant.frame;
			frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
			self.imgViewMontant.frame = frame;
			self.txtFldMontant.text = [self getFormatedNumero:self.txtFldMontant.text];
			vMontant = noteValue;
			break;
		case kTagDuree:
			[self.txtFldDuree setText:[NSString stringWithFormat:@"%.f",noteValue]];
			frame = self.imgViewDuree.frame;
			frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
			self.imgViewDuree.frame = frame;
			self.txtFldDuree.text = [self getFormatedNumero:self.txtFldDuree.text];
			vDuree = noteValue;
			break;
		case kTagMensualite:
			[self.txtFldMensualite setText:[NSString stringWithFormat:@"%.f",noteValue]];
			frame = self.imgViewMensualite.frame;
			frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
			self.imgViewMensualite.frame = frame;
			self.txtFldMensualite.text = [self getFormatedNumero:self.txtFldMensualite.text];
			vMensualite = noteValue;
			break;
		case kTagValResid:
			[self.txtFldValResid setText:[NSString stringWithFormat:@"%.f",noteValue]];
			frame = self.imgViewValResid.frame;
			frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
			self.imgViewValResid.frame = frame;
			self.txtFldValResid.text = [self getFormatedNumero:self.txtFldValResid.text];
			vValResid = noteValue;
			break;
		default:
			break;
	}
	
	if (button.tag == kTagMontant)
	{
		double value = [self calculMensualite:vMontant
									  andMois:vDuree];
		[self moveSlider:kTagMensualite withValue:value redo:NO];
		[self moveSlider:kTagValResid withValue:vMontant/100 redo:NO];
  
	}
	else
	{
		if (button.tag == kTagMensualite)
		{
			double value = [self calculNbreMensualite:vMontant andMensualite:vMensualite];
			[self moveSlider:kTagDuree withValue:value redo:NO];
		}
		else
		{
			if (button.tag == kTagDuree)
			{
				double value = [self calculMensualite:vMontant
											  andMois:vDuree];
				[self moveSlider:kTagMensualite withValue:value redo:NO];
			}
		}
	}
}


- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
   // self.navigationController.sideMenu.recognizer.enabled = YES;
	double value;
	switch (button.tag)
	{
		case kTagApport:
			value = (vApport*vMontant)/100;
			if (vApport >= vMontant || value > vMontant)
			{
				[self moveSlider:kTagApport withValue:vLastApport redo:NO];
				[self moveSlider:kTagMontant withValue:vLastMontant redo:NO];
				
				[self moveSlider:kTagValResid withValue:vLastMontant/100 redo:NO];
			}
			else if (vLastApport != vApport)
			{ 
				double valueM = [self calculMensualite:vMontant
											  andMois:vDuree];
				[self moveSlider:kTagMensualite withValue:valueM redo:NO];
				 
			}
			 
			break;
	}
	vLastApport = vApport;
	vLastMontant = vMontant;
}

#pragma mark - format numbers

- (NSString *)getFormatedNumero:(NSString *)numero
{
    NSNumber* number = [NSNumber numberWithDouble:[numero doubleValue]];
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[numberFormatter setGroupingSeparator:@" "];
	NSString* commaString = [numberFormatter stringForObjectValue:number];
	[numberFormatter release];
	return commaString;
}

- (NSString *)resetFormatedNumero:(NSString *)numero
{
    NSNumber* number = [NSNumber numberWithDouble:[numero doubleValue]];
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[numberFormatter setGroupingSeparator:@""];
	NSString* commaString = [numberFormatter stringForObjectValue:number];
	[numberFormatter release];
	return commaString;
}
   

#pragma mark - methods


- (void)moveSlider:(NSInteger)tag withValue:(NSInteger)value redo:(BOOL)redo
{
	NSArray *arrayValues;
	NSArray *arrayCenterX;
	UIButton *button;
	UIImageView *imgView;
	UITextField *txtFld;
	switch (tag)
	{
		case kTagApport:
			arrayValues = self.arrayValuesApport;
			arrayCenterX = self.arrayCenterXApport;
			button = self.btnScrollApport;
			imgView = self.imgViewApport;
			txtFld = self.txtFldApport;
			break;
		case kTagMontant:
			arrayValues = self.arrayValuesMontant;
			arrayCenterX = self.arrayCenterXMontant;
			button = self.btnScrollMontant;
			imgView = self.imgViewMontant;
			txtFld = self.txtFldMontant;
			break;
		case kTagDuree:
			arrayValues = self.arrayValuesDuree;
			arrayCenterX = self.arrayCenterXDuree;
			button = self.btnScrollDuree;
			imgView = self.imgViewDuree;
			txtFld = self.txtFldDuree;
			break;
		case kTagMensualite:
			arrayValues = self.arrayValuesMensualite;
			arrayCenterX = self.arrayCenterXMensualite;
			button = self.btnScrollMensualite;
			imgView = self.imgViewMensualite;
			txtFld = self.txtFldMensualite;
			break;
		case kTagValResid:
			arrayValues = self.arrayValuesValResid;
			arrayCenterX = self.arrayCenterXValResid;
			button = self.btnScrollValResid;
			imgView = self.imgViewValResid;
			txtFld = self.txtFldValResid;
			break;
			
	 		default:
			break;
	}
	
    float xValue = button.center.x;
	
	NSNumber *theV = [arrayValues objectAtIndex:0];
	if (value == [theV intValue])
	{
		xValue = [[arrayCenterX objectAtIndex:0] floatValue];
	}
	else
	{
		theV = [arrayValues objectAtIndex:arrayValues.count - 1];
		if (value == [theV intValue])
		{
			xValue = [[arrayCenterX objectAtIndex:arrayCenterX.count - 1] floatValue];
		}
		else
		{
			for (int i = 0; i < arrayValues.count; i++)
			{
				theV = [arrayValues objectAtIndex:i];
				if (value <= [theV intValue])
				{
					NSNumber *theD = nil;
					
					if (i >= [arrayCenterX count])
					{
						theD = [arrayCenterX objectAtIndex:[arrayCenterX count]-1];
					}
					else
					{
						theD = [arrayCenterX objectAtIndex:i];
					}
					xValue = [theD intValue];
					break;
				}
			}
		}
	}
	CGRect frame = imgView.frame;
	
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	
	button.center = CGPointMake(xValue, button.center.y);
	
	[txtFld setText:[NSString stringWithFormat:@"%.f",(float)value]];
	
	txtFld.text = [self getFormatedNumero:txtFld.text];
	frame.size = CGSizeMake(button.center.x - 20, frame.size.height);
	imgView.frame = frame;
	
    [UIView commitAnimations];
	
	switch (tag)
	{
		case kTagApport:
			vApport = (float)value;
			break;
		case kTagMontant:
			vMontant = (float)value;
			break;
		case kTagDuree:
			vDuree = (float)value;
			break;
		case kTagMensualite:
			vMensualite = (float)value;
			break;
		case kTagValResid:
			vValResid = (float)value;
			break;
			 
			
		default:
			break;
	}
	if (redo)
	{
		if (button.tag == kTagMontant)
		{
			double value = [self calculMensualite:vMontant
										  andMois:vDuree];
			[self moveSlider:kTagMensualite withValue:value redo:NO];
			[self moveSlider:kTagValResid withValue:vMontant/100 redo:NO];
		}
		else
		{
			if (button.tag == kTagMensualite)
			{
				double value = [self calculNbreMensualite:vMontant
											andMensualite:vMensualite];
				[self moveSlider:kTagDuree withValue:value redo:NO];
			}
			else
			{
				double value = [self calculMensualite:vMontant
											  andMois:vDuree];
				[self moveSlider:kTagMensualite withValue:value redo:NO];
			}
		}
	}
}

-(float)calculatPMTWithRatePerPeriod:(double)ratePerPeriod numberOfPayments:(NSInteger)numberOfPayments loanAmount:(double)loanAmount futureValue:(double)futureValue type:(NSInteger)type
{
    
    double q;
    
    q = pow(1 + ratePerPeriod, numberOfPayments);
    
    return (ratePerPeriod * (futureValue + (q * loanAmount))) / ((-1 + q) * (1 + ratePerPeriod * (type)));
    
}

-(double)calculMensualite:(double)montant andMois:(NSInteger)mois
{  
	//REFERENCE EXCEL SIMULATION
	double tx = (self.maxTaux/1200.0);
 	double vr = montant / 100.0;
	float v;
	if (vApport != 0)
	{
		double pl = (montant * vApport) / 100;
		
		double h12 = pl - ((montant - pl)*self.maxTaux/1200);
		vr = montant / 100.0;
		v = [self calculatPMTWithRatePerPeriod:tx numberOfPayments:mois-1 loanAmount:-montant+h12 futureValue:vr type:1];
	}
	else
		v = [self calculatPMTWithRatePerPeriod:tx numberOfPayments:mois loanAmount:-montant futureValue:vr type:1];
	
	if (v < 0)
	{
		v = v*(-1);
	}
	if (self.minMensualite == 0 || self.maxMensualite == 0)
		return v;
	
	if (v > self.minMensualite && v < self.maxMensualite)
		return v;
	else
	{
		if (v < self.minMensualite)
			return self.minMensualite;
		else
		{
			return self.maxMensualite;
		}
	}
}

-(double)calculMontant:(double)mensualite andMois:(NSInteger)mois
{
	double a1 = mensualite;
	double a2 = ((self.maxTaux/100.0)/ 12.0);
	double a4 = mois* -1;
	double a6 = (1 +a2);
	double a3 =  (double)pow(a6,a4);
	double a7 = (1 - a3);
	double a5 = a1*a7 ;
	double v = a5/a2;
	
	if (v > self.minMontant && v < self.maxMontant)
		return v;
	else
	{
		if (v < self.minMontant)
			return  self.minMontant;
		else
			return self.maxMontant;
	}
}

-(double)calculNbreMensualite:(double)montant andMensualite:(double)mensualite
{
	double a1 = mensualite;
	double a2 = ((self.maxTaux/100.0)/ 12.0);
	double a3 = montant* a2;
	double a5 = a3/a1;
	double a4 = (1 +a2);
	double a6 = log(1-a5)*-1;
	double a7 = log(a4);
	double v =  a6/a7;
	
	if (v > self.minDuree && v < self.maxDuree)
	{
		return v;
	}
	else
	{
		if (v < self.minDuree)
		{
			double value = [self calculMontant:vMensualite andMois:self.minDuree];
			[self moveSlider:kTagMontant withValue:value redo:NO];
			[self moveSlider:kTagValResid withValue:value/100 redo:NO];
			
			return  self.minDuree;
		}
		else
		{
			if (vMontant == self.maxMontant)
			{
				double value = [self calculMensualite:vMontant andMois:self.maxDuree];
				[self moveSlider:kTagMensualite withValue:value redo:NO];
			}
			return self.maxDuree;
		}
	}
}


@end
