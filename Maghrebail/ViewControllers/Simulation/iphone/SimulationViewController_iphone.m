//
//  SimulationViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 26/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SimulationViewController_iphone.h"
#import "AppDelegate.h"

@interface SimulationViewController_iphone ()

@end

@implementation SimulationViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initHeaderColor];
}

- (void) initHeaderColor {
    [super initHeaderColor];
    self.navigationController.sideMenu.panMode = MFSideMenuPanModeNone;
    self.navigationController.sideMenu.recognizer.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initHeaderColor];
    
    self.navigationController.sideMenu.panMode = MFSideMenuPanModeNone;
    self.navigationController.sideMenu.recognizer.enabled = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Orientation Change

- (void)orientationChanged:(NSNotification *)note {
    [super orientationChanged:note];
    [self initiatView];
}

@end