//
//  PopUpDashVC.m
//  Maghrebail
//
//  Created by MOBIBLANC on 13/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "PopUpDashVC.h"
#import "MonProfilViewController_iphone.h"
#import "MonProfilViewController_ipad.h"



@implementation PopUpDashVC

@synthesize parent;
@synthesize index;
@synthesize supportTouchID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self authentificationSet];
    
    if ([DataManager isIphoneX]) {
        self.supportFaceID = YES;
    }else {
        self.supportTouchID = NO;
    }
   
    // Do any additional setup after loading the view.
	self.scrollPage.contentSize = CGSizeMake(self.scrollPage.frame.size.width, self.scrollPage.frame.size.height);
    self.viewContainer.layer.cornerRadius = 2.0f;
    self.viewContainer.layer.masksToBounds = YES;
    
    self.viewContainer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.viewContainer.layer.borderWidth = 2.0f;
    
	if ([[NSUserDefaults standardUserDefaults] stringForKey:@"USER-EMAIL"])
	{
		self.textFieldLogin.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER-EMAIL"];
	}
	
	[self.btnInfoSecurite addTarget:self action:@selector(showInfoView:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)AuthentificationPushed:(UIButton *)sender
{
    if (self.supportTouchID) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) {
            [self vertifyIdentity];
            [self.viewTouchID setHidden:false];
        }else{
            [self identityVerified];
        }
        
    }else{
        [self.viewTouchID setHidden:true];
        [self identityVerified];
    }
    
//    if ([self.textFieldLogin.text length] == 0)
//    {
//        [self.textFieldLogin becomeFirstResponder];
//        return;
//    }
//    
//    if ([self.textFieldPassword.text length] == 0)
//    {
//        [self.textFieldPassword becomeFirstResponder];
//        return;
//    }
//    
//    [self dismissKeyboard];
//    //lance authentification
//    self.buttonAuthentification.enabled = NO;
//    [self.activityIndicatorAuthentification startAnimating];
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
//    [paramDic setObject:self.textFieldLogin.text forKey:@"email"];
//    [paramDic setObject:self.textFieldPassword.text forKey:@"password"];
//    [paramDic setObject:@"6" forKey:@"mobileOs"];
//    [paramDic setObject:@"IPHONE" forKey:@"mobileModel"];
//	dataManager.isDemo = NO;
//    
//	//REDEX
//	
//	//[self postDataToServer:[NSString stringWithFormat:@"%@&demo=true", kAUTHENTIFICATION_URL] andParam:paramDic];
//	
//	if (dataManager.isDemo)
//	{
//		paramDic = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
//	}
//	
//	
//	[self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.viewTouchID setHidden:true];
    
    if (self.supportTouchID) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) {
           
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] isEqualToString:@"no"]) {
                [self vertifyIdentity];
                [self.viewTouchID setHidden:false];
                [self.btnConnection setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
            }else{
                [self.btnConnection setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
            }
            
        }else{
             [self.btnConnection setTitle:@"Envoyer" forState:UIControlStateNormal];
        }

    }else{
          [self.btnConnection setTitle:@"Envoyer" forState:UIControlStateNormal];
    }
    

//    if (self.index == 20)
//    {
//        self.btnDemo.enabled = NO;
//        self.btnDemo.alpha = 0.5;
//    }
//    else
//    {
        self.btnDemo.enabled = YES;
        self.btnDemo.alpha = 1.0;
//      }
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
	self.textFieldPassword.text = @"";
	if (![self.switchSave isOn])
		self.textFieldLogin.text = @"";
	
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [_viewContainer release];
    [_btnDemo release];
    [_textFieldPassword release];
    [_textFieldLogin release];
    [delegate release];
    [_activityIndicatorAuthentification release];
    [_buttonAuthentification release];
    [_switchSave release];
    [_scrollPage release];
	[_webView release];
	[_btnInfoSecurite release];
    [_btnConnection release];
    [_viewTouchID release];
    [_imgBgEmail release];
    [_imgBgPassword release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setViewContainer:nil];
    [self setBtnDemo:nil];
    [self setTextFieldPassword:nil];
    [self setTextFieldLogin:nil];
    [self setActivityIndicatorAuthentification:nil];
    [self setButtonAuthentification:nil];
    [self setSwitchSave:nil];
    [self setScrollPage:nil];
	[self setWebView:nil];
	[self setBtnInfoSecurite:nil];
    [super viewDidUnload];
}


- (void)hide
{
	[self toggleLeftMenu:nil];
	dataManager.firstTime = NO;
}


- (IBAction) dismissKeyboard
{
    [self.textFieldLogin resignFirstResponder];
    [self.textFieldPassword resignFirstResponder];
}


#pragma mark - KeyWord

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSLog(@"keyboardWillShow");
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSLog(@"keyboardWillHide");
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn");
    [self dismissKeyboard];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing");
}


-(void)displayMessageView:(NSString *)message
{
    [super displayMessageView:message];
    self.buttonAuthentification.enabled = YES;
    [self.activityIndicatorAuthentification stopAnimating];
}


#pragma mark - updateView

-(void)updateView
{
    [super updateView];
    self.buttonAuthentification.enabled = YES;
    [self.activityIndicatorAuthentification stopAnimating];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        if (self.supportTouchID) {
            [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"deconnexionProfil"];
            [[NSUserDefaults standardUserDefaults]  synchronize];
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"]) {
                UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                alertView.tag=987;
                [alertView show];
                [alertView release];
            }
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"]) {
                UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                alertView.tag=987;
                [alertView show];
                [alertView release];
            }
        }
        if (self.supportFaceID) {
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"FaceIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"FaceIDLinked"] isEqualToString:@"no"]) {
                UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Face ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Face ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                alertView.tag=0001;
                [alertView show];
                [alertView release];
            }
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"FaceIDLinked"]) {
                UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Face ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Face ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                alertView.tag=0001;
                [alertView show];
                [alertView release];
            }
        }

        NSDictionary *response = [[self.response objectForKey:@"response"] objectAtIndex:0];
        dataManager.authentificationData = [NSDictionary dictionaryWithDictionary:response];
        dataManager.isAuthentified = YES;
		
		NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        if ([self.switchSave isOn])
        {
			NSData *dataAuthentification= [NSKeyedArchiver archivedDataWithRootObject:dataManager.authentificationData];
			[DataManager writeDataIntoCachWith:dataAuthentification andKey:@"authentificationData"];
			[[self delegate] showCompteMenu];
			
			if (standardUserDefaults) {
				[standardUserDefaults setObject:self.textFieldLogin.text forKey:@"USER-EMAIL"];
				[standardUserDefaults synchronize];
			}
        }
        else
        {
			if (standardUserDefaults) {
				[standardUserDefaults removeObjectForKey:@"USER-EMAIL" ];
				[standardUserDefaults synchronize];
			}
        }
		[[NSNotificationCenter defaultCenter] postNotificationName:@"showCompte" object:nil];
        [[self delegate] showCompteMenu];
        [self toggleLeftMenu:nil];
        dataManager.isAuthentified = YES;
        //show profil view For ipad or Iphone
        if (IS_IPAD)
        {
            MonProfilViewController_ipad *monProfilViewController_ipad = [[MonProfilViewController_ipad alloc] initWithNibName:@"MonProfilViewController_ipad" bundle:nil];
            [self.navigationController pushViewController:monProfilViewController_ipad animated:YES];
        }
        else
        {
            MonProfilViewController_iphone *monProfilViewController_iphone = [[MonProfilViewController_iphone alloc] initWithNibName:@"MonProfilViewController_iphone" bundle:nil];
            [self.navigationController pushViewController:monProfilViewController_iphone animated:YES];
        }
		
        [self showInterface];
        
    }
	/* else
	 {
	 dataManager.isAuthentified = NO;
	 NSString *message = [header objectForKey:@"message"];
	 [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
	 }*/
}

- (IBAction)demoPushed:(id)sender
{
    dataManager.isDemo = YES;
    [self showInterface];
    
}

- (void)showInterface
{
    
    [self.view removeFromSuperview];
    switch (index)
    {
        case 0:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 1:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 2:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 3:
        {
            [dataManager.slideMenu simulator:YES];
        }
            break;
        case 4:
        {
            [dataManager.slideMenu produitImmobilier:YES];
        }
            break;
        case 5:
        {
            [dataManager.slideMenu agences:YES];
        }
            break;
        case 6:
        {
           //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showFAQ" object:nil];
            }else{
                [dataManager.slideMenu faq:YES];
            }
           
        }
            break;
        case 7: //Publications
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showNosPublication" object:nil];
            }else{
                [dataManager.slideMenu nosPublications:YES];
            }
            
        }
            break;
            
        case 8:
        {
          //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showMentionLegales" object:nil];
            }else{
                 [dataManager.slideMenu mentionLegale:YES];
            }
           
        }
            break;
        case 9: //applications groupe
        {
        }
            break;
            
        case 10:
        {
            [dataManager.slideMenu nosContacts:YES];
        }
            break;
        case 11: //profil
        {
            
            [dataManager.slideMenu monProfil :YES];
        }
            break;
        case 12: //mesimpayes
        {
            
           //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesLignesCredits" object:nil];
            }else{
                 [dataManager.slideMenu maLigneCredit :YES];
            }
            
        }
            break;
        case 13:
        {
            
            //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContrats" object:nil];
            }else{
                [dataManager.slideMenu contrats:YES];
            }
           
        }
            break;
        case 14:
        {
            
           //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesImpayes" object:nil];
            }else{
                 [dataManager.slideMenu monSolde:YES];
            }
            
        }
            break;
        case 15:
        {
            
           //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesGaranties" object:nil];
            }else{
                 [dataManager.slideMenu mesGaranties:YES];
            }
           
        }
            break;
        case 16:
        {
            
           //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesReclamations" object:nil];
            }else{
                [dataManager.slideMenu mesReclamations:YES];
            }
            
        }
            break;
        case 17:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesFactures" object:nil];
            }else{
                [dataManager.slideMenu mesFactures:YES];
            }
            
        }
            break;
        case 18:
        {
            
          //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showMessagerie" object:nil];
            }else{
                 [dataManager.slideMenu maMessagerie:YES];
            }
           
        }
            break;
        case 19://
        {
            
           //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesEcheaciers" object:nil];
            }else{
                [dataManager.slideMenu mesEcheanciers:YES];
            }
            
        }
            break;
        case 20://
        {
            if (dataManager.isDemo)
            {
                alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
                [alert show];
                
                [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
            }
            else
            {
               //
                if(IS_IPAD){
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContact" object:nil];
                }else{
                    [dataManager.slideMenu mesContacts:YES];
                }
               
            }
        }
            break;
        case 21://
        {
            /*if (dataManager.isDemo)
            {
                alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
                [alert show];
                
                [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
            }
            else
            {*/
              //
            if(IS_IPAD){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesDocuments" object:nil];
            }else{
                [dataManager.slideMenu mesDocuments:YES];
            }
           
            //}
            break;
        }
        default:
            break;
    }
}

- (IBAction)removeView:(id)sender {
	[self.view removeFromSuperview];
}

#pragma mark -- 
#pragma mark -- TouchID fonctions

-(void) authentificationSet{
    [[EHFAuthenticator sharedInstance] setReason:kTouchID_check];
    [[EHFAuthenticator sharedInstance] setUseDefaultFallbackTitle:YES];
    NSError * error = nil;
    if (![EHFAuthenticator canAuthenticateWithError:&error]) {
        NSString * authErrorString = kTouchID_CheckSettings;
        self.supportTouchID=NO;
       
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = kTouchID_No_touchID;
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString =kTouchID_Not_Available;
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = kTouchID_NeedPassCode;
                break;
            default:
                authErrorString =kTouchID_CheckSettings;
                break;
        }
    }
}

-(void)vertifyIdentity{
    if ([DataManager isJailbroken]) {
        UIAlertView * alertError=[[UIAlertView alloc] initWithTitle:@"Attention (iOS jailbreak)!" message:kJailbreak delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertError show];
        [alertError release];
    }else{
        [self resignFirstResponder];
        [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^(){
            [self connectExistingCompte];
        } andFailure:^(LAError errorCode){
            NSString * authErrorString;
            switch (errorCode) {
                case LAErrorSystemCancel:
                    authErrorString = @"System canceled auth request due to app coming to foreground or background.";
                    break;
                case LAErrorAuthenticationFailed:
                    authErrorString = @"User failed after a few attempts.";
                    break;
                case LAErrorUserCancel:
                    authErrorString = @"Opération annulée!";
                    break;
                case LAErrorUserFallback:
                    authErrorString = @"Fallback auth method should be implemented here.";
                    break;
                case LAErrorTouchIDNotEnrolled:
                    authErrorString = @"Aucune emprunt n'est disponible pour le Touch ID ";
                    break;
                case LAErrorTouchIDNotAvailable:
                    authErrorString = @"Touch ID n'est pas disponible sur votre appareil.";
                    break;
                case LAErrorPasscodeNotSet:
                    authErrorString = @"Need a passcode set to use Touch ID.";
                    break;
                default:
                    authErrorString = @"Vérifier les paramètres du Touch ID.";
                    break;
            }
            //  [self presentAlertControllerWithMessage:authErrorString];
        }];

    }
}

-(void) presentAlertControllerWithMessage:(NSString *) message{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Touch ID" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)btnTouchID:(UIButton *)sender {
    [self vertifyIdentity];
}
-(void)identityVerified{
    
    if ([self.textFieldLogin.text length] == 0)
    {
        [self.textFieldLogin becomeFirstResponder];
        return;
    }
    
    if ([self.textFieldPassword.text length] == 0)
    {
        [self.textFieldPassword becomeFirstResponder];
        return;
    }
    
    [self dismissKeyboard];
    //lance authentification
    self.buttonAuthentification.enabled = NO;
    [self.activityIndicatorAuthentification startAnimating];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:self.textFieldLogin.text forKey:@"email"];
    [paramDic setObject:self.textFieldPassword.text forKey:@"password"];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.textFieldLogin.text forKey:@"Email"];
     [DataManager createKeychainValue:self.textFieldPassword.text forIdentifier:@"Password"];
    
    [paramDic setObject:@"6" forKey:@"mobileOs"];
    [paramDic setObject:@"IPHONE" forKey:@"mobileModel"];
    dataManager.isDemo = NO;
    
    //REDEX
    
    //[self postDataToServer:[NSString stringWithFormat:@"%@&demo=true", kAUTHENTIFICATION_URL] andParam:paramDic];
    
    if (dataManager.isDemo)
    {
        paramDic = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    
    [self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==987) {
        if (buttonIndex==1) {
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
            [[NSUserDefaults standardUserDefaults] setObject:self.textFieldLogin.text forKey:@"TouchIDLogin"];
            NSData *passData = [DataManager searchKeychainCopyMatching:@"Password"];
            NSString *m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
            [passData release];
            [DataManager createKeychainValue:m_password forIdentifier:@"TouchIDPassword"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"InitTouchID" object:nil];
        }
    }
    if (alertView.tag==0001) {
        if (buttonIndex==1) {
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FaceIDLinked"];
        }
    }
}
-(void) connectExistingCompte{
    [self dismissKeyboard];
    self.buttonAuthentification.enabled = NO;
    dataManager.isDemo = NO;
    [self.activityIndicatorAuthentification startAnimating];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLogin"] forKey:@"email"];
    NSData *passData = [DataManager searchKeychainCopyMatching:@"TouchIDPassword"];
    NSString *m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
    [passData release];
    
    [paramDic setObject:m_password forKey:@"password"];
    [paramDic setObject:@"6" forKey:@"mobileOs"];
    [paramDic setObject:@"IPHONE" forKey:@"mobileModel"];
    if (dataManager.isDemo)
    {
        paramDic = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    [self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic];
}

@end
