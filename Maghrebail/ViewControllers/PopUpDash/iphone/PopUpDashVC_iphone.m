//
//  PopUpDashVC_iphone.m
//  Maghrebail
//
//  Created by MOBIBLANC on 13/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "PopUpDashVC_iphone.h"
#import "InfoViewController_iphone.h"
#import "AppDelegate.h"

@interface PopUpDashVC_iphone ()

@end

@implementation PopUpDashVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
}

-(void) initView{
    
    self.imgBgEmail.layer.masksToBounds=YES;
    self.imgBgEmail.layer.cornerRadius=4;
    self.imgBgEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.imgBgEmail.layer.borderWidth=1;
    self.imgBgPassword.layer.masksToBounds=YES;
    self.imgBgPassword.layer.cornerRadius=4;
    self.imgBgPassword.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.imgBgPassword.layer.borderWidth=1;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showInfoView:(id)sender
{
	InfoViewController_iphone *aboutShowViewController = [[InfoViewController_iphone alloc] initWithNibName:@"InfoViewController_iphone" bundle:[NSBundle mainBundle]];
    aboutShowViewController.FormDash=YES;
    AppDelegate* mainDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    [mainDelegate.window.rootViewController.view addSubview:aboutShowViewController.view];
}

@end
