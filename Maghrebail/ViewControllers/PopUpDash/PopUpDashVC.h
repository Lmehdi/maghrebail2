//
//  PopUpDashVC.h
//  Maghrebail
//
//  Created by MOBIBLANC on 13/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "DashboardViewController.h"

@protocol CompteMenuDelegate <NSObject>

@required

- (void)showCompteMenu;

@end

@interface PopUpDashVC : BaseViewController
{
    id<CompteMenuDelegate>delegate;
}

@property (retain, nonatomic) IBOutlet UIView *viewTouchID;



@property (retain, nonatomic) IBOutlet UIImageView *imgBgEmail;
@property (retain, nonatomic) IBOutlet UIImageView *imgBgPassword;

@property(retain)id delegate;
@property(assign) BOOL supportTouchID;
@property(assign) BOOL supportFaceID;

@property (retain, nonatomic) IBOutlet UIButton *btnInfoSecurite;
@property (retain, nonatomic) IBOutlet UITextField *textFieldLogin;
@property (retain, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorAuthentification;
@property (retain, nonatomic) IBOutlet UIButton *buttonAuthentification;
@property (retain, nonatomic) IBOutlet UISwitch *switchSave;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollPage;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIButton *btnConnection;

@property (retain, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, retain) DashboardViewController *parent;
@property (nonatomic, assign) NSInteger index;
@property (retain, nonatomic) IBOutlet UIButton *btnDemo;

- (IBAction)btnTouchID:(UIButton *)sender;
- (IBAction)AuthentificationPushed:(UIButton *)sender;
- (IBAction)switcheSave:(UISwitch *)sender;
- (IBAction)showInfoView:(id)sender;
- (IBAction)removeView:(id)sender;
- (IBAction)demoPushed:(id)sender;
- (void)showInterface;

@end
