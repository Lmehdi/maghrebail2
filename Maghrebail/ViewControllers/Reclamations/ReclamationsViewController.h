//
//  ReclamationsViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface ReclamationsViewController : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate>{
    BOOL isSuggestion;
    IBOutlet UIScrollView *_scrollView;
     CGPoint svos;
    IBOutlet UIImageView *_messageBG;
}


@property (retain, nonatomic) IBOutlet UIButton *reclamationBTN;
@property (retain, nonatomic) IBOutlet UIButton *suggestionsBTN;
@property (retain, nonatomic) IBOutlet UITextField *sujetTF;
@property (retain, nonatomic) IBOutlet UITextField *messageTF;
@property (retain, nonatomic) IBOutlet UIButton *envoyerBTN;

-(void) initHeaderColor;
-(IBAction)envoyer:(id)sender;
-(IBAction)swich:(id)sender;
@end
