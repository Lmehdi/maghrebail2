//
//  ReclamationsViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ReclamationsViewController.h"

@interface ReclamationsViewController ()

@end

@implementation ReclamationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    isSuggestion = NO;
  

    self.reclamationBTN.selected = YES;
    svos = _scrollView.contentOffset;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height + 50);
    
    UIImage* image = [[UIImage imageNamed:@"champ_txt_iphone.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0];
    _messageBG.image = image;
    
	// Do any additional setup after loading the view.
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"VOS RECLAMATIONS ";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    UIImage* image = [[UIImage imageNamed:@"champ_txt_iphone.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0];
    _messageBG.image = image;
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

-(void)updateView{
    [super updateView];
    if (![[[self.response objectForKey:@"header"]objectForKey:@"status"] isEqualToString:@"NOK"]) 
		[BaseViewController showAlert:nil message:[self.response  objectForKey:@"message" ] delegate:nil confirmBtn:@"OK" cancelBtn:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_reclamationBTN release];
    [_suggestionsBTN release];
    [_sujetTF release];
    [_messageTF release];
    [_envoyerBTN release];
    [_scrollView release];
    [_messageBG release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setReclamationBTN:nil];
    [self setSuggestionsBTN:nil];
    [self setSujetTF:nil];
    [self setMessageTF:nil];
    [self setEnvoyerBTN:nil];
    [_scrollView release];
    _scrollView = nil;
    [_messageBG release];
    _messageBG = nil;
    [super viewDidUnload];
}

#pragma mark --
#pragma mark Actions 
-(void)swich:(id)sender{
    int tag = [(UIButton * )sender tag];
    switch (tag) {
        case 0:
            isSuggestion = NO;
            self.reclamationBTN.selected = YES;
            self.suggestionsBTN.selected = NO;
            break;
        case 1:
            isSuggestion = YES;
            self.reclamationBTN.selected = NO;
            self.suggestionsBTN.selected = YES;
            
            break;
        default:
            break;
    }
}
-(void)envoyer:(id)sender{
	
	if (dataManager.isDemo)
	{
        [BaseViewController showAlert:nil message:@"Vous êtes en mode démo" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
	}
	
    if (self.sujetTF.text == nil || [self.sujetTF.text length] == 0 || self.messageTF.text == nil || [self.messageTF.text length]==0) {
        [BaseViewController showAlert:nil message:@"Tous les champs sont obligatoires" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
    }
    
    NSString * requestType = (isSuggestion?@"suggestion":@"reclamation");
    if (isSuggestion)
	{ 
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/mesréclamations/envoiesuggestions"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/mesréclamations/envoiesuggestions"];
	}
	else
	{
		
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesréclamations/envoieréclamations"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/mesréclamations/envoieréclamations"];
	}
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:requestType,@"type",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",self.sujetTF.text, @"subject" ,self.messageTF.text,@"message",nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_RECLAMATION andParam:params];
}

#pragma mark --
#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        
        [textField resignFirstResponder];
        [_scrollView setContentOffset:svos animated:YES];
    }
    return NO;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
     
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [_scrollView setContentOffset:pt animated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [_scrollView setContentOffset:pt animated:YES];
    
}

@end
