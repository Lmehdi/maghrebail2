//
//  ReclamationsViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ReclamationsViewController_iphone.h"

@interface ReclamationsViewController_iphone ()

@end

@implementation ReclamationsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end