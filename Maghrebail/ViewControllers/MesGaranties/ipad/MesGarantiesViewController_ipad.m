//
//  MesGarantiesViewController_ipad.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/6/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesViewController_ipad.h"

@interface MesGarantiesViewController_ipad ()

@end

@implementation MesGarantiesViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
        static NSString *cellIdentifier_l = @"MesGarantiesCell_ipad";
        MesGarantiesCell *cell = (MesGarantiesCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MesGarantiesCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
	cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"TTC"]];
	cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"MFHT"]];
	cell.statut.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"STATUT_AFFAIRE"]];
	cell.dateFin.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DATE_FIN"]];
	cell.duree.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DURE"];
	cell.miseEnLoyer.text =[DataManager getFormateDate:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DATE_EFFET"]];
	cell.numContrat.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"NUM_AFFAIRE"];
	
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	self.isFromButton = NO;
    MesGarantiesDetailsViewController_ipad * mesGarantiesDetailsViewController_ipad = [[MesGarantiesDetailsViewController_ipad alloc]initWithNibName:@"MesGarantiesDetailsViewController_ipad" bundle:nil];
    
    mesGarantiesDetailsViewController_ipad.garantiesNum = [[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"NUM_AFFAIRE"];
    [self.navigationController pushViewController:mesGarantiesDetailsViewController_ipad animated:YES];
    
}

@end
