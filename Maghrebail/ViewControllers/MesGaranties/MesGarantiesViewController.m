//
//  MesGarantiesViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/26/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesViewController.h"

@interface MesGarantiesViewController ()

@end

@implementation MesGarantiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    

	// Do any additional setup after loading the view.
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES GARANTIES";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (self.isFromButton)
	{ 
		NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
		[self postDataToServer:URL_GARANTIES_LIST andParam:params];
	}
	
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}


-(void)updateView{
    [super updateView];
    [_tableView reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.response objectForKey:@"response"] count];
}
#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_GARANTIES_LIST andParam:params];
}


-(void)orientationChanged:(NSNotification *)note
{  
    [super orientationChanged:note];
	
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
	
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
	
}

@end
