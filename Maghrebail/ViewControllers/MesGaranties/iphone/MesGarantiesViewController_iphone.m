//
//  MesGarantiesViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/26/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesViewController_iphone.h"

@interface MesGarantiesViewController_iphone ()

@end

@implementation MesGarantiesViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
      
    }
    return self;
}

- (void)viewDidLoad
{
   [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
          [self initHeaderColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isLandscape) { 
		static NSString *cellIdentifier_l;
		
		if ([DataManager isIphone5LandScape])
		{
			cellIdentifier_l = @"MesGarantiesCell_iphone5_l";
		}
		else
			cellIdentifier_l = @"MesGarantiesCell_iphone_l";
        MesGarantiesCell *cell = (MesGarantiesCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MesGarantiesCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"TTC"]];
        cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"MFHT"]];
        cell.statut.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"STATUT_AFFAIRE"]];
        cell.dateFin.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DATE_FIN"]];
         cell.duree.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DURE"];
		cell.miseEnLoyer.text =[DataManager getFormateDate:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DATE_EFFET"]];
         cell.numContrat.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"NUM_AFFAIRE"];
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }else{
        static NSString *cellIdentifier_p = @"MesGarantiesCell_iphone_p";
        
        MesGarantiesCell *cell = (MesGarantiesCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (MesGarantiesCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"MFHT"]];
        cell.duree.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DURE"];
		cell.miseEnLoyer.text =[DataManager getFormateDate:[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"DATE_EFFET"]];
        cell.numContrat.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"NUM_AFFAIRE"];
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if (!isLandscape)
	{
		self.isFromButton = NO;
		MesGarantiesDetailsViewController_iphone * mesGarantiesDetailsViewController_iphone = [[MesGarantiesDetailsViewController_iphone alloc]initWithNibName:@"MesGarantiesDetailsViewController_iphone" bundle:nil];
		
		mesGarantiesDetailsViewController_iphone.garantiesNum = [[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"NUM_AFFAIRE"];
		[self.navigationController pushViewController:mesGarantiesDetailsViewController_iphone animated:YES];
	}
    
}
@end
