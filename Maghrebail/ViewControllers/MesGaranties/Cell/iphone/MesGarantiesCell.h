//
//  MesGarantiesCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/26/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MesGarantiesCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *numContrat;
@property (retain, nonatomic) IBOutlet UILabel *miseEnLoyer;
@property (retain, nonatomic) IBOutlet UILabel *duree;
@property (retain, nonatomic) IBOutlet UILabel *montant;
@property (retain, nonatomic) IBOutlet UILabel *dateFin;
@property (retain, nonatomic) IBOutlet UILabel *statut;
@property (retain, nonatomic) IBOutlet UILabel *montantHT;
@end
