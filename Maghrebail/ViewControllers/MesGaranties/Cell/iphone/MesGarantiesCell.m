//
//  MesGarantiesCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/26/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesCell.h"

@implementation MesGarantiesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numContrat release];
    [_miseEnLoyer release];
    [_duree release];
    [_montant release];
    [_dateFin release];
    [_statut release];
    [_montantHT release];
    [super dealloc];
}
@end
