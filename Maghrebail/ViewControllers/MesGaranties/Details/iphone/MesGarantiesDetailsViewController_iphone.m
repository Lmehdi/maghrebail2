//
//  MesGarantiesDetailsViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesDetailsViewController_iphone.h"
#import "GarantieDetailCell_iphone.h"

@interface MesGarantiesDetailsViewController_iphone ()

@end

@implementation MesGarantiesDetailsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
       static NSString *cellIdentifier_p = @"GarantieDetailCell_iphone";
        
        GarantieDetailCell_iphone *cell = (GarantieDetailCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (GarantieDetailCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
	
        cell.labelLibelle.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"LIB_GARANTIE"];
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{ 
    
}

@end
