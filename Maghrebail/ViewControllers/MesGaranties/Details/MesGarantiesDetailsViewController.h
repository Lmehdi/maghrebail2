//
//  MesGarantiesDetailsViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MesGarantiesDetailsViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *description;
@property (retain, nonatomic) NSString * descriptionString;
@property (retain, nonatomic) NSString * titleString;

@property(nonatomic,retain)NSString * garantiesNum;

-(void) initHeaderColor;
@end
