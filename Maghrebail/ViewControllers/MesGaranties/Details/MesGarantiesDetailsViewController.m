//
//  MesGarantiesDetailsViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesDetailsViewController.h"

@interface MesGarantiesDetailsViewController ()

@end

@implementation MesGarantiesDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"DESCRIPTION";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	// Do any additional setup after loading the view.
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.garantiesNum,@"num", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_GARANTIES_DETAILS andParam:params];
	
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	 
}

- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}
-(void)updateView{
    [super updateView];
    
    [_tableView reloadData];
    
}


#pragma mark --
#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.response objectForKey:@"response"] count];
}

#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.garantiesNum,@"num", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_GARANTIES_DETAILS andParam:params];
}
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_titleLabel release];
   // [_description release];
    [_descriptionString release];
    [_titleString release];
    [_garantiesNum release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTitleLabel:nil];
    [self setDescription:nil];
    [super viewDidUnload];
}
@end
