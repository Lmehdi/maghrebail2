//
//  MesGarantiesDetailsViewController_ipad.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesGarantiesDetailsViewController_ipad.h"
#import "GarantieDetailCell_ipad.h"

@interface MesGarantiesDetailsViewController_ipad ()

@end

@implementation MesGarantiesDetailsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
	static NSString *cellIdentifier_p = @"GarantieDetailCell_ipad";
	
	GarantieDetailCell_ipad *cell = (GarantieDetailCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
	if (cell == nil)
	{
		cell = (GarantieDetailCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
	}
	
	cell.labelLibelle.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"LIB_GARANTIE"];
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
	return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
