//
//  GarantieDetailCell.h
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GarantieDetailCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *labelLibelle;

@end
