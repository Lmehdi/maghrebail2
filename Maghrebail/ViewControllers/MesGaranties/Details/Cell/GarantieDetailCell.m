//
//  GarantieDetailCell.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "GarantieDetailCell.h"

@implementation GarantieDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_labelLibelle release];
    [super dealloc];
}
@end
