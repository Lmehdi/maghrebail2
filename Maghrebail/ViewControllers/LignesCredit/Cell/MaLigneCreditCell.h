//
//  MaLigneCreditCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaLigneCreditCell : UITableViewCell


@property (retain, nonatomic) IBOutlet UILabel *numLigne;
@property (retain, nonatomic) IBOutlet UILabel *dateDebut;
@property (retain, nonatomic) IBOutlet UILabel *montantFinancement;
@property (retain, nonatomic) IBOutlet UILabel *resteEngager;
@property (retain, nonatomic) IBOutlet UILabel *objet;
@property (retain, nonatomic) IBOutlet UILabel *dateFin;
@property (retain, nonatomic) IBOutlet UILabel *montantEngage;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (retain, nonatomic) IBOutlet UILabel *labelStatut;

@end
