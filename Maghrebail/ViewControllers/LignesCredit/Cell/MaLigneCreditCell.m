//
//  MaLigneCreditCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MaLigneCreditCell.h"

@implementation MaLigneCreditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numLigne release];
    [_dateDebut release];
    [_montantFinancement release];
    [_resteEngager release];
    [_objet release];
    [_dateFin release];
    [_montantEngage release];
    [_imgViewBackground release];
    [_labelStatut release];
    [super dealloc];
}
@end
