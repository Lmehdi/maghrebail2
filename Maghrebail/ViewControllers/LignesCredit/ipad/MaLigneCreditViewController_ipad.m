//
//  MaLigneCreditViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MaLigneCreditViewController_ipad.h"
#import "ContratsViewController_ipad.h"
#import "AppDelegate.h"

#define XBEGIN      34      //34
#define XEND        286     //286

@interface MaLigneCreditViewController_ipad ()

@end

@implementation MaLigneCreditViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MaLigneCreditCell *cell = nil;
    if (isLandscape)
    {
        static NSString *cellIdentifier_l = @"MaLigneCreditCell_ipad";
        cell = (MaLigneCreditCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MaLigneCreditCell*)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.numLigne.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"];
        cell.objet.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"OBJET"];
        cell.dateDebut.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_DEBUT"]];
        cell.dateFin.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN_VALIDITE"]];
        cell.montantFinancement.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"MF"]];
        cell.montantEngage.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"ME"]];
        cell.resteEngager.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"REST_ENGAGER"]];
        cell.labelStatut.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_LIGNE"];
    }
    else
    {
        static NSString *cellIdentifier_p = @"MaLigneCreditCell_ipad";
        
        cell = (MaLigneCreditCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            
            cell = (MaLigneCreditCell*)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.numLigne.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"];
        cell.objet.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"OBJET"];
        cell.dateDebut.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_DEBUT"]];
        cell.dateFin.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN_VALIDITE"]];
        cell.montantFinancement.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"MF"]];
        cell.montantEngage.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"ME"]];
        cell.resteEngager.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"REST_ENGAGER"]];
        cell.labelStatut.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_LIGNE"];
    }
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	self.isFromButton = NO;
    ContratsViewController_ipad *contratsViewController_ipad = [[ContratsViewController_ipad alloc] initWithNibName:@"ContratsViewController_ipad" bundle:nil];
	contratsViewController_ipad.isBack = YES;
    [contratsViewController_ipad setNumLigneCredit:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"]];
    [contratsViewController_ipad setParentType:kPARENT_LIGNECREDIT];
    [self.navigationController pushViewController:contratsViewController_ipad animated:YES];
    [contratsViewController_ipad release];
} 

- (IBAction)filtrePushed:(UIButton *)sender
{
	//[super filtrePushed:sender];
    AppDelegate* delegate=[[UIApplication sharedApplication] delegate];
     delegate.m_SideMenu.panMode=CHMFSideMenuPanModeNone;
    
	if (isFiltreViewShowed)
    {
		self.navigationController.sideMenu.recognizer.enabled = YES;
        CGRect filtreFrame = self.viewFiltre.frame;
        filtreFrame = CGRectMake(1024,128,500,640);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
		//  self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        
        isFiltreViewShowed = NO;
    }
    else
    {
       
		self.navigationController.sideMenu.recognizer.enabled = NO;
        CGRect filtreFrame = self.viewFiltre.frame;
        filtreFrame = CGRectMake(650,128,500,640);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
        
		// self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y + filtreFrame.size.height, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height - self.viewFiltre.frame.size.height);
        isFiltreViewShowed = YES;
    }
	
	
	if (isFiltreViewShowed)
	{
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;
			/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			 if (dataManager.isDemo)
			 {
			 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			 }
			 [self postDataToServer:kCONTRAT_URL andParam:params];*/
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
				/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
				 if (dataManager.isDemo)
				 {
				 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
				 }
				 [self postDataToServer:kCONTRAT_URL andParam:params];*/
			}
		}
	}
}
 

@end
