//
//  MaLigneCreditViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MaLigneCreditViewController.h"

@interface MaLigneCreditViewController ()

@end

@implementation MaLigneCreditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldDisplayLoadingView = YES;
        shouldAutorate = YES;
		
		keyMontant = @"MF";
		keyDate = @"DATE_DEBUT";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    

    // Do any additional setup after loading the view from its nib.
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES LIGNES DE CREDITS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (self.isFromButton)
	{ 
		btnSelectedEncours = YES;
		btnSelectedTerminee = NO;
		self.buttonEncours.selected = YES;
		self.buttonTermine.selected = NO;
		isFiltreViewShowed = NO;
		//btn action
		
		//lance request
		NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"encours",@"type", nil]; 
		}
		[self postDataToServer:URL_LIGNE_CREDIT andParam:params];
		[self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		
		[self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		
		frameMontImgMinInitial = self.imgViewScrollMontnat.frame;
		frameDateImgMinInitial = self.imgViewScrollDate.frame;
		
		frameMontBtnMinInitial = self.btnMinMontant.frame;
		frameMontLblMinInitial = self.labelMinMontant.frame;
		
		frameMontBtnMaxInitial = self.btnMaxMontant.frame;
		frameMontLblMaxInitial = self.labelMaxMontant.frame;
		
		frameDateBtnMinInitial = self.buttonMinDate.frame;
		frameDateLblMinInitial = self.labelMinDate.frame;
		
		frameDateBtnMaxInitial = self.buttonMaxDate.frame;
		frameDateLblMaxInitial = self.labelMaxDate.frame;
		
		frameViewMontMinInitial = self.viewMinMontant.frame;
		frameViewMontMaxInitial = self.viewMaxMontant.frame;
		frameViewDateMinInitial = self.viewMinDate.frame;
		frameViewDateMaxInitial = self.viewMaxDate.frame;
		
		frameMontImgMin = self.imgViewScrollMontnat.frame;
		frameDateImgMin = self.imgViewScrollDate.frame;
		
		frameMontBtnMin = self.btnMinMontant.frame;
		frameMontLblMin = self.labelMinMontant.frame;
		
		frameMontBtnMax = self.btnMaxMontant.frame;
		frameMontLblMax = self.labelMaxMontant.frame;
		
		frameDateBtnMin = self.buttonMinDate.frame;
		frameDateLblMin = self.labelMinDate.frame;
		
		frameDateBtnMax = self.buttonMaxDate.frame;
		frameDateLblMax = self.labelMaxDate.frame;
		
		frameViewMontMin = self.viewMinMontant.frame;
		frameViewMontMax = self.viewMaxMontant.frame;
		frameViewDateMin = self.viewMinDate.frame;
		frameViewDateMax = self.viewMaxDate.frame;
	}
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateView{
    
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
		NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.ligneCredit removeAllObjects];
        dataManager.ligneCredit = [NSMutableArray arrayWithArray:reponse];
        dataManager.ligneCreditFiltred = (NSMutableArray *)[dataManager.ligneCredit copy];
        
		if (dataManager.ligneCredit.count != 0)
		{
			//remplir valuesMontant
			NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"MF" ascending:YES comparator:^(id obj1, id obj2) {
				obj1 = [obj1 stringByReplacingOccurrencesOfString:@" " withString:@""];
				obj2 = [obj2 stringByReplacingOccurrencesOfString:@" " withString:@""];
				obj1 = [obj1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
				obj2 = [obj2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
				if ([obj1 doubleValue] > [obj2 doubleValue]) {
					return (NSComparisonResult)NSOrderedDescending;
				}
				if ([obj1 doubleValue] < [obj2 doubleValue]) {
					return (NSComparisonResult)NSOrderedAscending;
				}
				return (NSComparisonResult)NSOrderedSame;
			}];
			NSArray *sortedData;
			sortedData = [[dataManager.ligneCredit sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]] copy];
			
			NSString *str1 = [[[sortedData lastObject] objectForKey:@"MF"] stringByReplacingOccurrencesOfString:@" " withString:@""];
			NSString *str2 = [[[sortedData objectAtIndex:0] objectForKey:@"MF"] stringByReplacingOccurrencesOfString:@" " withString:@""];
			str1 = [str1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
			str2 = [str2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
			
			maxMontantValue = [str1 doubleValue];
			minMontantValue = [str2  doubleValue];
			self.labelMinMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", minMontantValue]];
			self.labelMaxMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", maxMontantValue]];
			int diff = (maxMontantValue - minMontantValue)/100;
			valuesMontant = [[NSMutableArray alloc] init];
			for (int i = minMontantValue; i <= maxMontantValue; i = i + diff)
			{
				[valuesMontant addObject:[NSString stringWithFormat:@"%d", i]];
				if (diff == 0)
					break;
			}
			
			//[dataManager.ligneCredit removeAllObjects];
			dataManager.ligneCredit.array = sortedData;
			//remplir valuesDates
			NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"DATE_DEBUT" ascending:YES comparator:^(id obj1, id obj2)
												{
													return (NSComparisonResult)[[DataManager stringToDtae:obj1] compare:[DataManager stringToDtae:obj2]];
												}];
			NSArray *sortedDataWithDate = [[dataManager.ligneCredit sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptorDate,nil]] copy];
			maxDateValue = [[DataManager stringToDtae:[[sortedDataWithDate lastObject] objectForKey:@"DATE_DEBUT"]] timeIntervalSince1970];
			minDatetValue = [[DataManager stringToDtae:[[sortedDataWithDate objectAtIndex:0] objectForKey:@"DATE_DEBUT"]] timeIntervalSince1970];
			self.labelMinDate.text = [DataManager timeIntervaleToString:minDatetValue];
			self.labelMaxDate.text = [DataManager timeIntervaleToString:maxDateValue];
			int diffDate = (maxDateValue - minDatetValue)/100;
			valuesDates = [[NSMutableArray alloc] init];
			for (int i = minDatetValue; i <= maxDateValue; i = i + diffDate)
			{
				[valuesDates addObject:[NSString stringWithFormat:@"%d", i]];
				if (diffDate == 0) {
					break;
				}
			}
			
			if (dataManager.ligneCredit.count == 1)
			{
				[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				
				[self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			}
			else
			{
				[self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				
				[self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			}
			
			self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
			self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
			self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
			self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
		}
		
		
    }
   /* else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
    [_tableView reloadData];
    
}
#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
	
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial;
	
    NSString *type = @"encours";
    if (self.buttonTermine.selected)
    {
        type = @"termine";
    }
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", type,@"type", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_LIGNE_CREDIT andParam:params];
}

#pragma mark --
#pragma mark TableView 
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataManager.ligneCreditFiltred  count];
}

#pragma mark --
#pragma mark Orientation Change
 

-(void)orientationChanged:(NSNotification *)note
{ 
    [super orientationChanged:note];
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
    
	[_tableView reloadData];
	
	if (dataManager.ligneCredit.count == 1)
	{
		[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		
		[self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
	}
	else
	{
		[self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		
		[self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		[self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
		[self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
	}
	
	if (!isLandscape)
	{
		//[self filtrePushed:nil];
	}
	if (!isLandscape)
	{
		//[super filtrePushed:nil];
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO; 
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES; 
			}
		}
		self.labelMinDate.text  = [NSString stringWithFormat:@"%@", self.strMinDate];
		self.labelMinMontant.text = [NSString stringWithFormat:@"%@", self.strMinMont];
		self.labelMaxMontant.text = [NSString stringWithFormat:@"%@", self.strMaxMont];
		self.labelMaxDate.text = [NSString stringWithFormat:@"%@", self.strMaxDate];
	}
 
	self.imgViewScrollMontnat.frame = frameMontImgMin;
	self.imgViewScrollDate.frame=frameDateImgMin;
	
	self.btnMinMontant.frame=frameMontBtnMin;
	self.labelMinMontant.frame=frameMontLblMin;
	self.viewMinMontant.frame=frameViewMontMin;
	
	self.btnMaxMontant.frame=frameMontBtnMax;
	self.labelMaxMontant.frame=frameMontLblMax;
	self.viewMaxMontant.frame=frameViewMontMax;
	
	self.buttonMinDate.frame=frameDateBtnMin;
	self.labelMinDate.frame=frameDateLblMin;
	self.viewMinDate.frame=frameViewDateMin;
	
	self.buttonMaxDate.frame=frameDateBtnMax;
	self.labelMaxDate.frame=frameDateLblMax;
	self.viewMaxDate.frame=frameViewDateMax;
	
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
	
}

- (IBAction)filtrePushed:(UIButton *)sender
{
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meslignesdecrédits/filtre"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/meslignesdecrédits/filtre"];
	if (isFiltreViewShowed)
	{
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;  
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
			}
		}
	} 
}

- (void)dealloc
{
    [_buttonEncours release];
    [_buttonTermine release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [self setButtonEncours:nil];
    [self setButtonTermine:nil];
    [super viewDidUnload];
}

#pragma mark - Action

- (IBAction)encoursPushed:(UIButton *)sender
{
	
	btnSelectedEncours = YES;
	btnSelectedTerminee = NO;
    self.buttonEncours.selected = YES;
    self.buttonTermine.selected = NO;
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"encours",@"type", nil];
	}
    [self postDataToServer:URL_LIGNE_CREDIT andParam:params];
	
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial;
}

- (IBAction)terminePushed:(UIButton *)sender
{
	
	btnSelectedEncours = NO;
	btnSelectedTerminee = YES;
	
    self.buttonEncours.selected = NO;
    self.buttonTermine.selected = YES;
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
	
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"termine",@"type", nil];
	}
    [self postDataToServer:URL_LIGNE_CREDIT andParam:params];
	
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial;
}

- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
	[super minScrollDraggedEnd:button withEvent:event];
	frameMontImgMin = self.imgViewScrollMontnat.frame;
	frameDateImgMin = self.imgViewScrollDate.frame;
	
	frameMontBtnMin = self.btnMinMontant.frame;
	frameMontLblMin = self.labelMinMontant.frame;
	
	frameMontBtnMax = self.btnMaxMontant.frame;
	frameMontLblMax = self.labelMaxMontant.frame;
	
	frameDateBtnMin = self.buttonMinDate.frame;
	frameDateLblMin = self.labelMinDate.frame;
	
	frameDateBtnMax = self.buttonMaxDate.frame;
	frameDateLblMax = self.labelMaxDate.frame;
	
	frameViewMontMin = self.viewMinMontant.frame;
	frameViewMontMax = self.viewMaxMontant.frame;
	frameViewDateMin = self.viewMinDate.frame;
	frameViewDateMax = self.viewMaxDate.frame;
	
	self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
	self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
	self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
	self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}

- (void)maxScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
	[super maxScrollDraggedEnd:button withEvent:event];
	frameMontImgMin = self.imgViewScrollMontnat.frame;
	frameDateImgMin = self.imgViewScrollDate.frame;
	
	frameMontBtnMin = self.btnMinMontant.frame;
	frameMontLblMin = self.labelMinMontant.frame;
	
	frameMontBtnMax = self.btnMaxMontant.frame;
	frameMontLblMax = self.labelMaxMontant.frame;
	
	frameDateBtnMin = self.buttonMinDate.frame;
	frameDateLblMin = self.labelMinDate.frame;
	
	frameDateBtnMax = self.buttonMaxDate.frame;
	frameDateLblMax = self.labelMaxDate.frame;
	
	frameViewMontMin = self.viewMinMontant.frame;
	frameViewMontMax = self.viewMaxMontant.frame;
	frameViewDateMin = self.viewMinDate.frame;
	frameViewDateMax = self.viewMaxDate.frame;
	
	self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
	self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
	self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
	self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}
 
@end
