//
//  MaLigneCreditViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
 
@interface MaLigneCreditViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
{ 
	CGRect frameViewMontMin;
	CGRect frameViewMontMax;
	
	CGRect frameViewDateMin;
	CGRect frameViewDateMax;
	
	CGRect frameMontBtnMin;
	CGRect frameMontImgMin;
	CGRect frameMontLblMin;
	
	CGRect frameDateBtnMin;
	CGRect frameDateImgMin;
	CGRect frameDateLblMin;
	
	
	CGRect frameMontBtnMax;
	CGRect frameMontImgMax;
	CGRect frameMontLblMax;
	
	CGRect frameDateBtnMax;
	CGRect frameDateImgMax;
	CGRect frameDateLblMax;
	
	CGRect frameViewMontMinInitial;
	CGRect frameViewMontMaxInitial;
	
	CGRect frameViewDateMinInitial;
	CGRect frameViewDateMaxInitial;
	
	CGRect frameMontBtnMinInitial;
	CGRect frameMontImgMinInitial;
	CGRect frameMontLblMinInitial;
	
	CGRect frameDateBtnMinInitial;
	CGRect frameDateImgMinInitial;
	CGRect frameDateLblMinInitial;
	
	
	CGRect frameMontBtnMaxInitial;
	CGRect frameMontImgMaxInitial;
	CGRect frameMontLblMaxInitial;
	
	CGRect frameDateBtnMaxInitial;
	CGRect frameDateImgMaxInitial;
	CGRect frameDateLblMaxInitial;
}

@property (retain, nonatomic) IBOutlet UIButton *buttonEncours;
@property (retain, nonatomic) IBOutlet UIButton *buttonTermine;
 
- (IBAction)filtrePushed:(UIButton *)sender;
- (IBAction)encoursPushed:(UIButton *)sender;
- (IBAction)terminePushed:(UIButton *)sender;

-(void) initHeaderColor;
@end
