//
//  MaLigneCreditViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MaLigneCreditViewController_iphone.h"
#import "ContratsViewController_iphone.h"

#define XBEGIN      34      //34
#define XEND        286     //286

@interface MaLigneCreditViewController_iphone ()

@end

@implementation MaLigneCreditViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
      
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
       [self initHeaderColor];

    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MaLigneCreditCell *cell = nil;
    if (isLandscape)
    { 
		static NSString *cellIdentifier_l;
		
		if ([DataManager isIphoneLandScape])
		{
			cellIdentifier_l = @"MaLigneCreditCell_iphone_l";
		}
		else
			cellIdentifier_l = @"MaLigneCreditCell_iphone5_l";
		
        cell = (MaLigneCreditCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MaLigneCreditCell*)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.numLigne.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"];
        cell.objet.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"OBJET"];
        cell.dateDebut.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_DEBUT"]];
        cell.dateFin.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN_VALIDITE"]];
        cell.montantFinancement.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"MF"]];
        cell.montantEngage.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"ME"]];
        cell.resteEngager.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"REST_ENGAGER"]];
        cell.labelStatut.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_LIGNE"];
    }
    else
    {
        static NSString *cellIdentifier_p = @"MaLigneCreditCell_iphone_p";
        
        cell = (MaLigneCreditCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            
            cell = (MaLigneCreditCell*)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.numLigne.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"];
        cell.objet.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"OBJET"];
        cell.dateFin.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_DEBUT"]];
        cell.montantFinancement.text = [DataManager getFormatedNumero:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"MF"]];
        cell.dateDebut.text = [DataManager getFormateDate:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_DEBUT"]];
        cell.resteEngager.text = [[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"REST_ENGAGER"];
    }
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.isFromButton = NO;
    ContratsViewController_iphone *contratsViewController_iphone = [[ContratsViewController_iphone alloc] initWithNibName:@"ContratsViewController_iphone" bundle:nil];
	contratsViewController_iphone.isBack = YES;
    [contratsViewController_iphone setNumLigneCredit:[[dataManager.ligneCreditFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM"]];
    [contratsViewController_iphone setParentType:kPARENT_LIGNECREDIT];
    [self.navigationController pushViewController:contratsViewController_iphone animated:YES];
    [contratsViewController_iphone release];
}

@end
