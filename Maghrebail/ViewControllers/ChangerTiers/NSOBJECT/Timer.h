//
//  Timer.h
//  Maghrebail
//
//  Created by Raoui Mouad on 20/01/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Timer : UIApplication {
    NSDate *start;
    NSDate *end;
}

- (void) startTimer;
- (void) stopTimer;
- (double) timeElapsedInSeconds;
- (double) timeElapsedInMilliseconds;
- (double) timeElapsedInMinutes;

@end
