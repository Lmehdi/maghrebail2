//
//  ChangerTiersViewController.m
//  Maghrebail
//
//  Created by Belkheir on 25/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "ChangerTiersViewController.h"
#import "Timer.h"
@interface ChangerTiersViewController ()

@end

@implementation ChangerTiersViewController

@synthesize  pas;
@synthesize  Count;
@synthesize isfirstrefreche;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
           }
    return self;
}

- (void)viewDidLoad
{
    
    _addData=[[NSMutableArray alloc] init];
    [super viewDidLoad];
    Count=0;
    pas=0;
    
    [self initHeaderColor];
     _tableView.allowsMultipleSelection = NO;
     self.cellSelected = [NSMutableArray array];
    self.selectedIndex=-1;
    
    self.bgsearch.layer.masksToBounds=YES;
    self.bgsearch.layer.cornerRadius=4;
    self.bgsearch.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.bgsearch.layer.borderWidth=1;
    
    
   
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [super textFieldShouldBeginEditing:textField];
    if(textField.tag==3)
    {
        
        self.bgsearch.layer.masksToBounds=YES;
        self.bgsearch.layer.cornerRadius=4;
        self.bgsearch.layer.borderColor=[UIColor orangeColor].CGColor;
        self.bgsearch.layer.borderWidth=1;
        
        
    }
    
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
  
    [super textFieldDidEndEditing:textField];
    
    if(textField.tag==3)
    {
        
        self.bgsearch.layer.masksToBounds=YES;
        self.bgsearch.layer.cornerRadius=4;
        self.bgsearch.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.bgsearch.layer.borderWidth=1;
        
        
    }
    
    
}




- (void)dealloc
{
  
   
    [_textfieldrecherche release];
    [_buttonretour release];
    [_bgsearch release];
    [super dealloc];
}


- (IBAction)SeiarchFilterPushed:(UIButton *)sender
{
    [super SearchFilterPushed:nil];


}

- (IBAction)btnback:(id)sender
{
    [super btnback:nil];
}

-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"CHANGEMENT TIERS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    
    if (self.isFromButton)
    {
        frameViewbtnretour = self.buttonretour.frame;
        frameViewsearchtextbar = self.textfieldrecherche.frame;
      		isFiltreViewShowed = NO;

        if(dataManager.isClientContent)
        {
            
            
      NSData *datasave = [DataManager readDataIntoCachWith:@"datafinds"];
         if(datasave!=nil){
           NSMutableArray *datafound=[[NSMutableArray alloc] init];
           datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
             
           
              dataManager.filteredtablechangetiers = (NSMutableArray *)[datafound copy];
             dataManager.tablechangementtiers=(NSMutableArray *)[datafound copy];
              [_tableView reloadData];
             NSString *formatStringX = [NSString stringWithFormat:@"%d", 0];
             NSString *formatStringY = [NSString stringWithFormat:@"%d", maxvalue-1];
             pas=endValue-1;
             
             NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
             
             [params setObject:@"TEST" forKey:@"mobileOS"];
             [params setObject:@"TEST"forKey:@"mobileModel"];
             [params setObject:formatStringX forKey:@"x"];
             [params setObject:formatStringY forKey:@"y"];
             
             NSLog(@"the end is %@",formatStringY);
             
            // [self postDataToServere:kLIST_TIERS_CLIENT andParam:params andTag:55];
       }else{
                                NSString *formatStringX = [NSString stringWithFormat:@"%d", 0];
                                NSString *formatStringY = [NSString stringWithFormat:@"%d", maxvalue-1];
                                pas=endValue-1;
                                
                                NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
                                
                                [params setObject:@"TEST" forKey:@"mobileOS"];
                                [params setObject:@"TEST"forKey:@"mobileModel"];
                                [params setObject:formatStringX forKey:@"x"];
                                [params setObject:formatStringY forKey:@"y"];
                                
                                NSLog(@"the end is %@",formatStringY);
                                
                                [self postDataToServer:kLIST_TIERS_CLIENT andParam:params andTag:50];
                            }
           
            self.isfirstrefreche=YES;
            
        }
        
        if(dataManager.isFournisseurContent)
            
        {
          [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Tetes_groupes",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"]] andTag:51];
            
        }
        
    }
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
}

-(void)updateViewTag:(int)inputTag{
  //  [super updateView];
    if(inputTag == 0) {
        return;
    }
    
    NSArray *reponse=nil;
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        
        
        switch (inputTag)
        {
            case 50:
                [dataManager.tablechangementtiers removeAllObjects];
                // Premier appel avant de scroller dans Client
                reponse=[[self.response objectForKey:@"response"]objectForKey:@"Tiers"];
                [dataManager.tablechangementtiers removeAllObjects];
                [dataManager.tablechangementtiers addObjectsFromArray:reponse];
                 [_addData addObjectsFromArray:reponse];
                dataManager.filteredtablechangetiers = (NSMutableArray *)[_addData copy];
                     NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:dataManager.filteredtablechangetiers];
                     [DataManager writeDataIntoCachWith: dataProduits andKey:@"datafinds"];
                NSString *countstring=[[[[self.response objectForKey:@"response"]objectForKey:@"Count"]objectAtIndex:0]objectForKey:@"COUNT"];
                Count= [countstring intValue];
                    [_tableView reloadData];
                break;
                
            case 51:{
          //      [dataManager.tablechangementtiers removeAllObjects];
           reponse = [self.response objectForKey:@"response"];
              

                NSMutableArray *myresponse=[NSMutableArray arrayWithArray:reponse];
                dataManager.tablechangementtiers=[[NSMutableArray alloc] init];
                dataManager.filteredtablechangetiers=[[NSMutableArray alloc]init];
                [dataManager.filteredtablechangetiers removeAllObjects];
                [dataManager.tablechangementtiers removeAllObjects];
               // [dataManager.tablechangementtiers removeAllObjects];
                //[dataManager.filteredtablechangetiers addObjectsFromArray:myresponse];
               [dataManager.tablechangementtiers addObjectsFromArray:myresponse];
                 dataManager.filteredtablechangetiers = (NSMutableArray *)[dataManager.tablechangementtiers copy];
                    [_tableView reloadData];
                break;
            }
            case 52:
                // les appels des web services après scroll
                [dataManager.tablechangementtiers removeAllObjects];
                reponse=[[self.response objectForKey:@"response"]objectForKey:@"Tiers"];
                NSLog(@"%lu",(unsigned long)dataManager.tablechangementtiers.count);
           
                
                
                
                
                
                
                
//                NSData *datasave = [DataManager readDataIntoCachWith:@"datafind"];
//                if(datasave!=nil){
//                    NSMutableArray *datafound=[[NSMutableArray alloc] init];
//                    datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
//                     [_addData addObjectsFromArray:datafound];
//                     [dataManager.tablechangementtiers addObjectsFromArray:datafound];
//                }else{
//                    [_addData addObjectsFromArray:reponse];
//                }
//                
                
//                 [_addData addObjectsFromArray:reponse];
//                NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:_addData];
//                [DataManager writeDataIntoCachWith: dataProduits andKey:@"datafind"];
//                  NSLog(@"the new %lu",(unsigned long)_addData.count);
                [dataManager.tablechangementtiers addObjectsFromArray:reponse];
                dataManager.filteredtablechangetiers = (NSMutableArray *)[dataManager.tablechangementtiers copy];
                    NSMutableArray *newlist=[[NSMutableArray alloc]init];
                for (NSDictionary *dict in dataManager.tablechangementtiers) {
                    
                  
                    
                    if(dataManager.isClientContent) {
                        NSData *datasave = [DataManager readDataIntoCachWith:@"text"];
                        NSString *frist=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
                        NSString *secondString = [[dict objectForKey:@"NOM"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        
                        if([secondString hasPrefix:frist] || [secondString hasPrefix:[frist uppercaseString]]) {
                            [newlist addObject:dict];
                        }
                        
                    }
                }
                
                if([newlist count] > 0) {
                    dataManager.filteredtablechangetiers = [NSMutableArray arrayWithArray:newlist];
                    NSLog(@"TABLE HEIGHT %f",_tableView.frame.size.height);
                    
                    
                }else{
                     dataManager.filteredtablechangetiers = [NSMutableArray arrayWithArray:newlist];
                }
                    [_tableView reloadData];
                break;
                
                
            case 55:
            {
              //  [dataManager.tablechangementtiers removeAllObjects];
                // Premier appel avant de scroller dans Client
                reponse=[[self.response objectForKey:@"response"]objectForKey:@"Tiers"];
              //  [dataManager.tablechangementtiers removeAllObjects];
             //   [dataManager.tablechangementtiers addObjectsFromArray:reponse];
                [_addData addObjectsFromArray:reponse];
               // dataManager.filteredtablechangetiers = (NSMutableArray *)[_addData copy];
                NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:_addData];
                [DataManager writeDataIntoCachWith: dataProduits andKey:@"datafinds"];
           
                
                break;
            }
            default:
                break;
  
        }
    
    }
   
 
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView





-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@" thiph pfjzepfj :%@",string);
    NSLog(@"%@",searchStr);
    NSString *firstString;
    
    NSMutableArray *newlist=[[NSMutableArray alloc]init];
 
    NSLog(@"DATATABLE %lu",(unsigned long)dataManager.tablechangementtiers.count);
    
    
    
    for (NSDictionary *dict in dataManager.tablechangementtiers) {
        
        if(dataManager.isFournisseurContent) {
            
            

            
            if([[dict objectForKey:@"NOM_GROUPE"] hasPrefix:searchStr] || [[dict objectForKey:@"NOM_GROUPE"] hasPrefix:[searchStr uppercaseString]]) {
             [newlist addObject:dict];
            }
           
        }

        if(dataManager.isClientContent) {
            firstString = [searchStr stringByReplacingOccurrencesOfString:@" " withString:@""];
     
   
            NSString *secondString = [[dict objectForKey:@"NOM"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            

           if([secondString rangeOfString:firstString].location!=NSNotFound || [secondString rangeOfString:[firstString uppercaseString]].location!=NSNotFound) {
                [newlist addObject:dict];
            }
        
        }
    }
    
    if([newlist count] > 0) {
        dataManager.filteredtablechangetiers = [NSMutableArray arrayWithArray:newlist];
        NSLog(@"TABLE HEIGHT %f",_tableView.frame.size.height);

        
    } else {
    
        if(searchStr.length==0){
            dataManager.filteredtablechangetiers=[NSMutableArray arrayWithArray:dataManager.tablechangementtiers];
            
            
        }else{
            dataManager.filteredtablechangetiers=[NSMutableArray arrayWithArray:newlist];  
            
        }
               // if(_tableView.frame.size.height>newlist.count*44){
//        
//                    if(self.Count>self.pas)
//                    {
////                        if(searchStr.length>1){
////                    NSString *valeurpas = [NSString stringWithFormat:@"%d",self.pas];
////                    int valuepasdebut=self.pas +1;
////        
////                    NSLog(@"la valeur du pas est  %@",valeurpas);
////                    NSString *valeurstart = [NSString stringWithFormat:@"%d",valuepasdebut];
////        
////                    NSLog(@"la valeur de depart est  %@",valeurstart);
////                    int valuepasfin= 2*self.pas +1;
////                    NSString *valeurend = [NSString stringWithFormat:@"%d",valuepasfin];
////                    NSLog(@"la valeur d arret est  %@",valeurend);
////        
////                    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
////        
////                    [params setObject:@"TEST" forKey:@"mobileOS"];
////                    [params setObject:@"TEST" forKey:@"mobileModel"];
////                    [params setObject:valeurstart forKey:@"x"];
////                    [params setObject:valeurend forKey:@"y"];
////        
////        
////                    self.pas=valuepasfin;
////                            NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:firstString];
////                            [DataManager writeDataIntoCachWith: dataProduits andKey:@"text"];
////                    [self postDataToServer:kLIST_TIERS_CLIENT andParam:params andTag:52];
////                        }
//                   }
//                    
//                else{
//                       dataManager.filteredtablechangetiers=[NSMutableArray arrayWithArray:newlist];
//                }
       // dataManager.filteredtablechangetiers = [NSMutableArray arrayWithArray:newlist];
    }
    
    
    [_tableView reloadData];
    
    return YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(dataManager.isFournisseurContent) {
        if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]]) {
            return [[dataManager filteredtablechangetiers] count];
        }
    }
    
    if(dataManager.isClientContent) {
        if ([[self.response objectForKey:@"response"] isKindOfClass:[NSMutableDictionary class]]) {
            return [[dataManager filteredtablechangetiers] count];
        }
    }
    
    return 0;
}


#pragma mark --
#pragma mark Pull to refresh

-(void)reloadTableViewDataSource
{
    {
        [self.mSearchTextField setText:@""];
        [self btnback:nil];
    }
    [super reloadTableViewDataSource];
    
    if(dataManager.isClientContent)
    {
        
        NSString *formatStringX = [NSString stringWithFormat:@"%d",0];
        NSString *formatStringY = [NSString stringWithFormat:@"%d",maxvalue-1];
        pas=maxvalue-1;
        
        NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
        
        [params setObject:@"TEST" forKey:@"mobileOS"];
        [params setObject:@"TEST"forKey:@"mobileModel"];
        [params setObject:formatStringX forKey:@"x"];
        [params setObject:formatStringY forKey:@"y"];
        
        NSLog(@"the end is %@",formatStringY);
        
        [self postDataToServer:kLIST_TIERS_CLIENT andParam:params andTag:50];
        self.isfirstrefreche=YES;
        
    } else if(dataManager.isFournisseurContent) {
        NSString* url       =   [NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Tetes_groupes",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"], [dataManager.authentificationData objectForKey:@"PASSWORD"]];
        
        [self getRemoteContentfournisseur:url andTag:51];
    }
}

- (void) orientationChanged:(NSNotification *)note {
    
    [super orientationChanged:note];
 
    if(dataManager.isFournisseurContent)
    {
        
        if(!isLandscape)
        {
            
            
        }
        else
        {
            
            
        }
        
    }
    
    
    
    
    if (!isLandscape)
    {
        
        
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
        
    }
    
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    [_tableView addSubview:_refreshHeaderView];
    [_refreshHeaderView refreshLastUpdatedDate];
}



- (void)viewDidUnload {
    [super viewDidUnload];
   
}
- (IBAction)btnrecherche:(id)sender {
    
}

@end
