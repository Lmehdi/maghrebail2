//
//  ChangerTiersViewCell.h
//  Maghrebail
//
//  Created by Belkheir on 25/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangerTiersViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *numtiers;
@property (retain, nonatomic) IBOutlet UILabel *nomtiers;
@property (retain, nonatomic) IBOutlet UIImageView *checkmark;

@end
