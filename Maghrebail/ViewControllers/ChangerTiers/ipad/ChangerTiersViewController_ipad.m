//
//  ChangerTiersViewController_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 25/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "ChangerTiersViewController_ipad.h"
#import "ChangerTiersViewCell.h"
#import "MonProfilViewController_ipad.h"
@interface ChangerTiersViewController_ipad ()

@end

@implementation ChangerTiersViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    self.tableView.estimatedRowHeight=58.0;
    self.tableView.rowHeight=UITableViewAutomaticDimension;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataManager.filteredtablechangetiers count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float cellclientheight = 0.0;
    NSString *nameSelected=@"";
    if(dataManager.isClientContent)
    {
        cellclientheight=[self getCellHeightForTextNomTiers:[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM"]];
    }
    if(dataManager.isFournisseurContent)
    {
        cellclientheight=[self getCellHeightForTextNomTiers:[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM_GROUPE"]];
        
    }
    
    
  
        
        static NSString *cellIdentifier_l;
        
        /*      if ([DataManager isIphone5LandScape])
         {
         cellIdentifier_l = @"ChangerTiersViewCell_iphone5_l";
         }
         else
         cellIdentifier_l = @"ChangerTiersViewCell_iphone_l";
         */
        
        cellIdentifier_l = @"ChangerTiersViewCell_ipad";
        
        ChangerTiersViewCell *cell = (ChangerTiersViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ChangerTiersViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        CGRect oldFrame = cell.nomtiers.frame;
        [cell.nomtiers sizeToFit];
        oldFrame.size.height    =   cell.nomtiers.frame.size.height;
        [cell.nomtiers setFrame:oldFrame];
        
        
        if(dataManager.isClientContent)
            
        {
            nameSelected=[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM"];
            cell.nomtiers.text = [[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM"];
            cell.numtiers.text = [[dataManager.filteredtablechangetiers objectAtIndex:indexPath.row]objectForKey:@"TIERS"];
            
        }
        
        
        if(dataManager.isFournisseurContent)
        {
            
            nameSelected=[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM_GROUPE"];
            cell.nomtiers.text = [[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM_GROUPE"];
            
            cell.numtiers.text = [[dataManager.filteredtablechangetiers objectAtIndex:indexPath.row]objectForKey:@"NUM_TETE_GROUPE"];
            
        }
        
        //  cell.numtiers.frame=CGRectMake(cell.numtiers.frame.origin.x, cellclientheight/2 -10, cell.numtiers.frame.size.width, cell.numtiers.frame.size.height);
        
    if(dataManager.indexselected ==-1)
    {
        
        
        NSString  *TIERS=[dataManager.authentificationData objectForKey:@"TIERS"];
        
        
        if([cell.numtiers.text isEqualToString:TIERS])
            
            cell.checkmark.hidden=NO;
        else
            cell.checkmark.hidden=YES;
    }
    
    else if([dataManager.nameIndexselected isEqualToString:nameSelected] && dataManager.indexselected>=0)
    {
        
        cell.checkmark.hidden=NO;
        
        
    }
    else if (![dataManager.nameIndexselected isEqualToString:nameSelected] && dataManager.indexselected >=0)
    {
        cell.checkmark.hidden=YES;
        
        
    }
    
        cell.alpha = 0.5;
    
        switch (indexPath.row % 2)
        {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
   

    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    dataManager.indexselected = indexPath.row;
    
http://abderrahim.marbou%40maghrebail.ma:eac1cdd60eda56af4300224ccff966dd4b75992f@www.maghrebail.ma/leasebox/partenaires/api/rest/changer_tiers/NUM_TIERS/abderrahim.marbou@maghrebail.ma
    if(dataManager.isFournisseurContent) {
        
           NSString* logtest= [[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NUM_TETE_GROUPE"];
        dataManager.nameIndexselected=[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM_GROUPE"];
        NSString* url           =   [NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/changer_tiers/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_TETE_GROUPE"],[dataManager.authentificationData objectForKey:@"EMAIL"]];
        NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:logtest];
        [DataManager writeDataIntoCachWith: dataProduits andKey:@"CHANGERTIERS"];
        [self getRemoteContentfournisseur:url andTag:0];
    }
    
    if(dataManager.isClientContent) {
        dataManager.nameIndexselected=[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row]objectForKey:@"NOM"];
        
        NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
        [params setObject:[dataManager.authentificationData objectForKey:@"EMAIL"] forKey:@"email"];
        [params setObject:[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row] objectForKey:@"TIERS"] forKey:@"tiers"];
        [params setObject:@"TEST" forKey:@"mobileOS"];
        [params setObject:@"TEST" forKey:@"mobileModel"];
          NSString  *TIERS=[[dataManager.filteredtablechangetiers  objectAtIndex:indexPath.row] objectForKey:@"TIERS"] ;
        NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:TIERS];
        [DataManager writeDataIntoCachWith: dataProduits andKey:@"CHANGERTIERSCLIENT"];
        [self postDataToServer:kCHANGEMENT_TIERS andParam:params andTag:0];
    }
}

- (void)updateViewTag:(int)inputTag {
    [super updateViewTag:inputTag];
    
    if(inputTag == 0) {
        MonProfilViewController_ipad *monProfilViewController_iphone = [[MonProfilViewController_ipad alloc] initWithNibName:@"MonProfilViewController_ipad" bundle:nil];
        [self.navigationController pushViewController:monProfilViewController_iphone animated:YES];
    }
}
@end
