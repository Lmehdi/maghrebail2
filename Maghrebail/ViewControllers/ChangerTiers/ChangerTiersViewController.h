//
//  ChangerTiersViewController.h
//  Maghrebail
//
//  Created by Belkheir on 25/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "MonProfilViewController_iphone.h"

@interface ChangerTiersViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIApplicationDelegate>
{
    CGRect frameViewbtnretour;
    CGRect frameViewsearchtextbar;


}

- (IBAction)filtrePushed:(UIButton *)sender;

@property (retain, nonatomic) IBOutlet UITextField *mSearchTextField;

@property (assign, nonatomic) int selectedIndex;
@property (assign, nonatomic)  int index;
@property (assign, nonatomic)  int pas;
@property (assign, nonatomic)  int Count;
@property (assign, nonatomic)   Boolean fromFitre;
@property (assign, nonatomic)   NSString *txtsearch;

@property (assign, nonatomic) BOOL isfirstrefreche;
@property (retain, nonatomic) NSMutableArray *cellSelected;
@property (retain, nonatomic) NSMutableArray *addData;
@property (retain, nonatomic) IBOutlet UITextField *textfieldrecherche;
@property (retain, nonatomic) IBOutlet UIButton *buttonretour;
@property (retain, nonatomic) IBOutlet UIImageView *bgsearch;

- (IBAction)SearchFilterPushed:(UIButton *)sender;
- (IBAction)btnback:(id)sender;

- (void) initHeaderColor;

- (void) pushMyProfilViewController;

@end
