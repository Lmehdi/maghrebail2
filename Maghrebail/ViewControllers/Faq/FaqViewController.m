//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FaqViewController.h"

@interface FaqViewController ()

@end

@implementation FaqViewController
@synthesize aryIndex;
@synthesize faqs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldDisplayDrag = NO;
    }
    return self;
}
-(void) initHeaderTitle{
    headerView.labelheaderTitre.text=@"NOTRE F.A.Q";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderTitle];
    // Do any additional setup after loading the view from its nib.
    //read from plis file
    // Path to the plist (in the application bundle)
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FAQ" ofType:@"plist"];
    self.faqs = [[NSMutableArray alloc] initWithContentsOfFile:path];
	
	if (dataManager.firstTime && !self.isFromDash) {
		[self showSideMenu];
		[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(hide) userInfo:nil repeats:NO];
	}
	self.aryIndex = [NSMutableArray array];
	
    [self getRemoteContent:URL_FAQ];
}

- (void)hide
{
	[self toggleLeftMenu:nil];
	dataManager.firstTime = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[faqs release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.faqs  count];
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderTitle];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

-(void)updateView{
    [super updateView];
    
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [self.faqs removeAllObjects];
        self.faqs = [NSMutableArray arrayWithArray:reponse];
	}
   /* else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
    [_tableView reloadData];
}


@end
