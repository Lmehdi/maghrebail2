//
//  MaLigneCreditCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaqCell : UITableViewCell
{
    
}

@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelText;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFlech;

@end
