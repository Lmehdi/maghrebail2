//
//  MaLigneCreditCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FaqCell.h"

@implementation FaqCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{
    [_imgViewBackground release];
    [_labelTitle release];
    [_labelText release];
    [_imgViewFlech release];
    [super dealloc];
}
@end
