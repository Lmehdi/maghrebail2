//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface FaqViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *faqs;
    BOOL shouldExpandCell;
    int selectedNumber;
	NSMutableArray *aryIndex;
}
@property (nonatomic, retain) NSMutableArray *aryIndex;
@property (nonatomic, retain) NSMutableArray *faqs;
-(void) initHeaderTitle;
@end
