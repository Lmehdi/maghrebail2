//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FaqViewController_iphone.h"
#import "FaqCell_iphone.h"

@interface FaqViewController_iphone ()

@end

@implementation FaqViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderTitle];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderTitle];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
		for (NSInteger i = 0; i<self.aryIndex.count; i++)
		{
			NSIndexPath *obj = (NSIndexPath *)[self.aryIndex objectAtIndex:i];
			if (indexPath.row == obj.row)
			{
                NSString *description = [[self.faqs  objectAtIndex:indexPath.row] objectForKey:@"reponse"];
                description = [description stringByReplacingOccurrencesOfString:@"\r\n>" withString:@"\r\n >"];
				float height = [description sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] constrainedToSize:CGSizeMake(250, 9000) lineBreakMode:self.labelMaxDate.lineBreakMode].height;
				return  49 + height + 30;
			}  
		}
		return 49;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FaqCell_iphone *cell = nil;
    if (isLandscape)
    {
        static NSString *cellIdentifier_l = @"FaqCell_iphone_l";
        cell = (FaqCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (FaqCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
    }
    else
    {
        static NSString *cellIdentifier_p = @"FaqCell_iphone_p";
        
        cell = (FaqCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (FaqCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.labelTitle.text = [[self.faqs  objectAtIndex:indexPath.row] objectForKey:@"question"];
        NSString *description = [[self.faqs  objectAtIndex:indexPath.row] objectForKey:@"reponse"];
        description = [description stringByReplacingOccurrencesOfString:@"\r\n>" withString:@"\r\n >"];
        
        cell.labelText.text = description;
        
		NSLog(@"%@",[[self.faqs  objectAtIndex:indexPath.row] objectForKey:@"question"]);
        
    }
	
	[cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_right_iphone.png"]];
	for (NSInteger i = 0; i<self.aryIndex.count; i++)
	{
		NSIndexPath *obj = (NSIndexPath *)[self.aryIndex objectAtIndex:i];
		if (indexPath.row == obj.row)
		{
			[cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_bas_iphone.png"]];
		}
	}
	
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //shouldExpandCell = !shouldExpandCell;
    /*for (int i = 0; i < faqs.count; i++)
    {
        FaqCell_iphone* cell = (FaqCell_iphone *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_right_iphone.png"]];
	 }*/
	FaqCell_iphone* cell = (FaqCell_iphone *)[tableView cellForRowAtIndexPath:indexPath];
	
	BOOL find = NO;
	for (NSInteger i = 0; i<self.aryIndex.count; i++)
	{
		NSIndexPath *obj = (NSIndexPath *)[self.aryIndex objectAtIndex:i];
		if (indexPath.row == obj.row)
		{
			[cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_right_iphone.png"]];
			[self.aryIndex removeObject:indexPath];
			find = YES;
		}
	}
	if (!find)
	{ 
		[cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_bas_iphone.png"]];
		[self.aryIndex addObject:indexPath];
	}
    
	int row = [indexPath row];
    selectedNumber = row;
	
    [tableView beginUpdates];
    [tableView endUpdates];
	
	//if (indexPath.row == self.faqs.count - 1 || indexPath.row == self.faqs.count - 2)
	//{
		[[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	//}
}


@end
