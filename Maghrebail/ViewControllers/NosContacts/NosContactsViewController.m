//
//  NosContactsViewController.m
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosContactsViewController.h"

@interface NosContactsViewController ()

@end

@implementation NosContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"NOS CONTACTS";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    headerView.labelheaderTitre.text=@"NOS CONTACTS";

	// Do any additional setup after loading the view.
	[self.scrollContainer setContentSize:CGSizeMake(self.scrollContainer.frame.size.width, self.scrollContainer.frame.size.height + 200)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_scrollContainer release];
    [_txtFldNum release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollContainer:nil];
    [self setTxtFldNum:nil];
    [super viewDidUnload];
}

- (void)showMessage:(NSString *)strNum withTag:(NSInteger)tag
{
	UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
													  message:[NSString stringWithFormat:@"Voulez-vous appeler le numéro %@", strNum]
													 delegate:self
											cancelButtonTitle:@"Non, Merci!"
											otherButtonTitles:@"Appeler", nil];
	message.tag = tag;
	[message show];
	[message release];
}

- (IBAction)btnPositionSiege:(UIButton *)sender {

}

- (IBAction)btnTelFstPushed:(id)sender
{
	
	[Flurry logEvent:@"Home/Informations/Noscontacts/Clicktocall"];
	[self showMessage:kNUM_TEL1 withTag:1];
}

- (IBAction)btnTelSdPushed:(id)sender
{
	
	[Flurry logEvent:@"Home/Informations/Noscontacts/Clicktocall2"];
	[self showMessage:kNUM_TEL2 withTag:1];
}

- (IBAction)btnFaxPushed:(id)sender
{
	
	[Flurry logEvent:@"Home/Informations/Noscontacts/Clicktomail"];
	[self showMessage:kNUM_TEL1 withTag:1];
}

- (IBAction)btnEmailPushed:(id)sender
{
	if ([MFMailComposeViewController canSendMail])
	{
		// device is configured to send mail
		MFMailComposeViewController* controller = [[[MFMailComposeViewController alloc] init] retain];
		controller.mailComposeDelegate = self;
		[controller setSubject:@"Application Maghrebail iOS"];
		[controller setMessageBody:@"" isHTML:YES];
		[controller setToRecipients:[NSArray arrayWithObject:@"maghrebail@maghrebail.ma"]];
		NSLog(@"controller MAil %@", controller.description);
		[self presentModalViewController:controller animated:YES];
		[controller release];
	}
	else
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Veuillez configurer votre compte messagerie" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
}

- (IBAction)btnEnvoyerPushed:(id)sender
{
	[self showMessage:kNUM_TEL1 withTag:1];
}

- (void)makeCall:(NSString *)numero
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", numero]]];
}
 

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 1)
	{
		if (buttonIndex == 1)
		{
			[self makeCall:kNUM_TEL1];
		}
	}else if (alertView.tag == 2)
	{
		if (buttonIndex == 1)
		{
			[self makeCall:kNUM_TEL2];
		}
		
	} 
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
    NSString * msgResult= nil ;
    NSString * msgResult2= nil ;
	
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msgResult = @"Envoie annulé";
            msgResult2 = @"Votre avez annulé l'opération et aucun message n'a été mis en file d'attente";
            break;
        case MFMailComposeResultSaved:
            msgResult = @"Message enregistré";
            msgResult2 = @"Vous avez sauvé le message dans les brouillons";
            break;
        case MFMailComposeResultSent:
            msgResult = @"Message envoyé";
            msgResult2 = @"Votre message a été bien envoyé. Merci pour votre commentaire";
            // flurry log event
            break;
        case MFMailComposeResultFailed:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Le message n'a pas été sauvé ou mis en file d'attente, probablement en raison d'une erreur";
            break;
        default:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Message non envoyé";
            break;
    }
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgResult message:msgResult2 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
} 

#pragma mark - textField methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{ 
	[self.scrollContainer setContentOffset:CGPointMake(0, textField.frame.origin.y ) animated:YES];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{ 
	[textField resignFirstResponder];
	[self.scrollContainer setContentOffset:CGPointMake(0, 0 ) animated:YES];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
	unichar c;
	if (![string isEqualToString:@""]) {
		
		c = [string characterAtIndex:0];
	}
	  
	if (c != 48 && [self.txtFldNum.text length] == 0)
	{
		return NO;
	}
	for (int i = 0; i < [string length]; i++)
	{
		unichar c = [string characterAtIndex:i];
		if (![myCharSet characterIsMember:c])
		{
			return NO;
		}
	}
	NSUInteger newLength = [textField.text length] + [string length] - range.length;
	return (newLength > 10) ? NO : YES;
}

#pragma mark - scroll methods

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{ 
	[self.txtFldNum resignFirstResponder];
}

@end
