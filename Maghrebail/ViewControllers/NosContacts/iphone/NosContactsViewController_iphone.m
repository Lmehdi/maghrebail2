//
//  NosContactsViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosContactsViewController_iphone.h"
#import "AgenceProcheViewController_iphone.h"

@interface NosContactsViewController_iphone ()

@end

@implementation NosContactsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

- (IBAction)btnPositionSiege:(UIButton *)sender {
    AgenceProcheViewController_iphone *view = [[AgenceProcheViewController_iphone alloc] initWithNibName:@"AgenceProcheViewController_iphone" bundle:nil];
    view.indAgence = 2;
    [self.navigationController pushViewController:view animated:YES];
    [view release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end