//
//  NosContactsViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 12/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosContactsViewController_ipad.h"
#import "AgenceProcheViewController_ipad.h"
@interface NosContactsViewController_ipad ()

@end

@implementation NosContactsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (IBAction)btnPositionSiege:(UIButton *)sender {
    AgenceProcheViewController_ipad *view = [[AgenceProcheViewController_ipad alloc] initWithNibName:@"AgenceProcheViewController_ipad" bundle:nil];
    view.indAgence = 2;
    [self.navigationController pushViewController:view animated:YES];
    [view release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
