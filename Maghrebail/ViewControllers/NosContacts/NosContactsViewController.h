//
//  NosContactsViewController.h
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h> 

@interface NosContactsViewController : BaseViewController <MFMailComposeViewControllerDelegate>
@property (retain, nonatomic) IBOutlet UIScrollView *scrollContainer;
@property (retain, nonatomic) IBOutlet UITextField *txtFldNum;
- (IBAction)btnPositionSiege:(UIButton *)sender;

- (IBAction)btnTelFstPushed:(id)sender;
- (IBAction)btnTelSdPushed:(id)sender;
- (IBAction)btnFaxPushed:(id)sender;
- (IBAction)btnEmailPushed:(id)sender;
- (IBAction)btnEnvoyerPushed:(id)sender;
- (void)makeCall:(NSString *)numero;
-(void) initHeaderColor;
@end
