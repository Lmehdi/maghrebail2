//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ContratsViewController.h"

@interface ContratsViewController ()

@end

@implementation ContratsViewController

@synthesize parentType;
@synthesize numLigneCredit;
@synthesize isBack;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self initHeaderColor];
	dataManager.contratFiltred = [NSMutableArray array];
	
	if (self.parentType == kPARENT_LIGNECREDIT)
	{
		self.btnFav.hidden = NO;
		if ([[NSUserDefaults standardUserDefaults] stringForKey:self.numLigneCredit])
		{
			self.btnFav.selected = YES;
		}
		else
			self.btnFav.selected = NO;
	}
	else
		self.btnFav.hidden = YES;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES CONTRATS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}

-(void)viewWillAppear:(BOOL)animated
{
	
	[super viewWillAppear:animated];
    [self initHeaderColor];
    [self loadNibName];
	
	
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
	 
	if (isBack)
	{
		[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
		[headerView setDelegate:nil];
		[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
		
		[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	}
	if (self.isFromButton)
	{
		keyMontant = @"MFHT";
		keyDate = @"DATE_EFFET";
		
		btnSelectedEncours = YES;
		btnSelectedTerminee = NO;
		
		self.buttonEncours.selected = YES;
		self.buttonTermine.selected = NO;
		isFiltreViewShowed = NO;
		
		frameMontImgMin = self.imgViewScrollMontnat.frame;
		frameDateImgMin = self.imgViewScrollDate.frame;
		
		frameMontBtnMin = self.btnMinMontant.frame;
		frameMontLblMin = self.labelMinMontant.frame;
		
		frameMontBtnMax = self.btnMaxMontant.frame;
		frameMontLblMax = self.labelMaxMontant.frame;
		
		frameDateBtnMin = self.buttonMinDate.frame;
		frameDateLblMin = self.labelMinDate.frame;
		
		frameDateBtnMax = self.buttonMaxDate.frame;
		frameDateLblMax = self.labelMaxDate.frame;
		
		frameViewMontMin = self.viewMinMontant.frame;
		frameViewMontMax = self.viewMaxMontant.frame;
		frameViewDateMin = self.viewMinDate.frame;
		frameViewDateMax = self.viewMaxDate.frame;
		
		
		//INITIAL
		
		
		frameMontImgMinInitial = self.imgViewScrollMontnat.frame;
		frameDateImgMinInitial = self.imgViewScrollDate.frame;
		
		frameMontBtnMinInitial = self.btnMinMontant.frame;
		frameMontLblMinInitial = self.labelMinMontant.frame;
		
		frameMontBtnMaxInitial = self.btnMaxMontant.frame;
		frameMontLblMaxInitial = self.labelMaxMontant.frame;
		
		frameDateBtnMinInitial = self.buttonMinDate.frame;
		frameDateLblMinInitial = self.labelMinDate.frame;
		
		frameDateBtnMaxInitial = self.buttonMaxDate.frame;
		frameDateLblMaxInitial = self.labelMaxDate.frame;
		
		frameViewMontMinInitial = self.viewMinMontant.frame;
		frameViewMontMaxInitial = self.viewMaxMontant.frame;
		frameViewDateMinInitial = self.viewMinDate.frame;
		frameViewDateMaxInitial = self.viewMaxDate.frame;
		 		 
		[self initiatView];
		
		//lance request
		if (self.parentType == kPARENT_MENU)
		{
			NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"encours",@"type", nil];
			}
			
			[self postDataToServer:kCONTRAT_URL andParam:params];
			
		}
		else if (self.parentType == kPARENT_LIGNECREDIT)
		{
			NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", self.numLigneCredit,@"num", nil];
			
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"encours",@"type", nil];
			}
			
			[self postDataToServer:kLIGNE_CREDIT_DETAIL_URL andParam:params];
		}

	}
	
	if (dataManager.authentificationData)
	{
        if(btnSelectedEncours){
            self.buttonEncours.selected=YES;
            
            NSLog(@"");
        }else{
            self.buttonTermine.selected=YES;
        }
		if (headerView)
			 [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	
	// Do any additional setup after loading the view from its nib.
	
}

- (IBAction)handlFav:(id)sender
{
	if (self.parentType == kPARENT_LIGNECREDIT)
	{
		NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
		if (self.btnFav.selected)
		{
			self.btnFav.selected = NO;
			[standardUserDefaults removeObjectForKey:self.numLigneCredit];
		}
		else
		{
			self.btnFav.selected = YES;
			[standardUserDefaults setObject:@"CONTRAT" forKey:self.numLigneCredit];
		}
		[standardUserDefaults synchronize];
	}
}

- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

-(void)initiatView
{ 
    //btn action
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    
    [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    if (self.parentType == kPARENT_MENU)
    {
        self.buttonRetour.hidden = YES;
    }
    else if (self.parentType == kPARENT_LIGNECREDIT)
    {
        self.buttonRetour.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_buttonEncours release];
    [_buttonTermine release];
    [_buttonRetour release];
	[_btnFav release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setButtonEncours:nil];
    [self setButtonTermine:nil];
    [self setButtonRetour:nil];

	[self setBtnFav:nil];
    [super viewDidUnload];
}

#pragma mark - updateView

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    dataManager.contrats = [[NSMutableArray alloc] init];
    dataManager.contratFiltred = [[NSMutableArray alloc] init];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.contrats removeAllObjects];
        dataManager.contrats = [NSMutableArray arrayWithArray:reponse];
        
		if (dataManager.contrats.count != 0)
		{
			//remplir valuesMontant
			NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:keyMontant ascending:YES comparator:^(id obj1, id obj2) {
				obj1 = [obj1 stringByReplacingOccurrencesOfString:@" " withString:@""];
				obj2 = [obj2 stringByReplacingOccurrencesOfString:@" " withString:@""];
				obj1 = [obj1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
				obj2 = [obj2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
				if ([obj1 doubleValue] > [obj2 doubleValue]) {
					return (NSComparisonResult)NSOrderedDescending;
				}
				if ([obj1 doubleValue] < [obj2 doubleValue]) {
					return (NSComparisonResult)NSOrderedAscending;
				}
				return (NSComparisonResult)NSOrderedSame;
			}];
			NSArray *sortedData = [[dataManager.contrats sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]] copy];
			
			
			NSString *str1 = [[[sortedData lastObject] objectForKey:keyMontant] stringByReplacingOccurrencesOfString:@" " withString:@""];
			NSString *str2 = [[[sortedData objectAtIndex:0] objectForKey:keyMontant] stringByReplacingOccurrencesOfString:@" " withString:@""];
			str1 = [str1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
			str2 = [str2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
			
			maxMontantValue = [str1 doubleValue];
			minMontantValue = [str2  doubleValue];
			self.labelMinMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", minMontantValue]];
			self.labelMaxMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", maxMontantValue]];
			int diff = (maxMontantValue - minMontantValue)/100;
			valuesMontant = [[NSMutableArray alloc] init];
			for (int i = minMontantValue; i <= maxMontantValue; i = i + diff)
			{
				[valuesMontant addObject:[NSString stringWithFormat:@"%d", i]];
				if (diff == 0)
					break;
			}
			
			[dataManager.contrats removeAllObjects];
			dataManager.contrats.array = sortedData;
			//remplir valuesDates
			NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:keyDate ascending:YES comparator:^(id obj1, id obj2)
												{
													return (NSComparisonResult)[[DataManager stringToDtae:obj1] compare:[DataManager stringToDtae:obj2]];
												}];
			NSArray *sortedDataWithDate = [[dataManager.contrats sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptorDate,nil]] copy];
			maxDateValue = [[DataManager stringToDtae:[[sortedDataWithDate lastObject] objectForKey:keyDate]] timeIntervalSince1970];
            NSLog(@" this is the maxdatevalue %d",maxDateValue);
			minDatetValue = [[DataManager stringToDtae:[[sortedDataWithDate objectAtIndex:0] objectForKey:keyDate]] timeIntervalSince1970];
            NSLog(@" this is the maxdatevalue %d",minDatetValue);
			self.labelMinDate.text = [DataManager timeIntervaleToString:minDatetValue];
			self.labelMaxDate.text = [DataManager timeIntervaleToString:maxDateValue];
			int diffDate = (maxDateValue - minDatetValue)/100;
			valuesDates = [[NSMutableArray alloc] init];
			for (int i = minDatetValue; i <= maxDateValue; i = i + diffDate)
			{
				[valuesDates addObject:[NSString stringWithFormat:@"%d", i]];
				if (diffDate == 0) {
					break;
				}
			}
			
			self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
			self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
			self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
			self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
			
			
			
			dataManager.contratFiltred = (NSMutableArray *)[dataManager.contrats copy];
			
			if (dataManager.contrats.count == 1)
			{
				[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				
				[self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			}
			else
			{
				[self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				
				[self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
				[self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
				[self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			}

		}
	}
   /* else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
    [_tableView reloadData];
}

#pragma mark - Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
	
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial; 
	
    NSMutableDictionary * params = nil;
    
    if (self.parentType == kPARENT_MENU)
    {
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        [self postDataToServer:kCONTRAT_URL andParam:params];
    }
    else if (self.parentType == kPARENT_LIGNECREDIT)
    {
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", self.numLigneCredit,@"num", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        [self postDataToServer:kLIGNE_CREDIT_DETAIL_URL andParam:params];
    }
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataManager.contratFiltred  count];
}

#pragma mark - Orientation Change
 
-(void)orientationChanged:(NSNotification *)note
{  
    [super orientationChanged:note];
	
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
	
    [self initiatView];
	if (!isLandscape)
	{
        
        if(IS_IPHONE_5){
        self.viewTableView.frame=CGRectMake(0, 167, 320 , 401);
        }
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
		
		if (isBack)
		{
			[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
			[headerView setDelegate:nil];
			[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
			
			[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
		}
		
		 
		
		[self filtrePushed:nil];
	}
	
    [_tableView reloadData];
	if (!isLandscape)
	{
        
        
		if (dataManager.contrats.count == 1)
		{
			[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			
			[self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			[self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		}
		else
		{
			[self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			[self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			
			[self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
			[self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
			[self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
		}

		[super filtrePushed:nil];
		
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;
			/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
			[self postDataToServer:kCONTRAT_URL andParam:params];*/
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
				/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
				if (dataManager.isDemo)
				{
					params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
				}
				[self postDataToServer:kCONTRAT_URL andParam:params];*/
			}
		}
		
		self.labelMinDate.text  = [NSString stringWithFormat:@"%@", self.strMinDate];
		self.labelMinMontant.text = [NSString stringWithFormat:@"%@", self.strMinMont];
		self.labelMaxMontant.text = [NSString stringWithFormat:@"%@", self.strMaxMont];
		self.labelMaxDate.text = [NSString stringWithFormat:@"%@", self.strMaxDate];

	}
	
	
	self.imgViewScrollMontnat.frame = frameMontImgMin;
	self.imgViewScrollDate.frame=frameDateImgMin;
	
	self.btnMinMontant.frame=frameMontBtnMin;
	self.labelMinMontant.frame=frameMontLblMin;
	self.viewMinMontant.frame=frameViewMontMin;
	
	self.btnMaxMontant.frame=frameMontBtnMax;
	self.labelMaxMontant.frame=frameMontLblMax;
	self.viewMaxMontant.frame=frameViewMontMax;
	
	self.buttonMinDate.frame=frameDateBtnMin;
	self.labelMinDate.frame=frameDateLblMin;
	self.viewMinDate.frame=frameViewDateMin;
	
	self.buttonMaxDate.frame=frameDateBtnMax;
	self.labelMaxDate.frame=frameDateLblMax;
	self.viewMaxDate.frame=frameViewDateMax;
	 
	   
}
 
- (IBAction)filtrePushed:(UIButton *)sender
{
	[super filtrePushed:sender];
    
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mescontrats/filtre"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/mescontrats/filtre"];
	if (isFiltreViewShowed)
	{
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;
			/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
			[self postDataToServer:kCONTRAT_URL andParam:params];*/
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
				/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
				if (dataManager.isDemo)
				{
					params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
				}
				[self postDataToServer:kCONTRAT_URL andParam:params];*/
			}
		} 
	} 
}

#pragma mark - Action

- (IBAction)encoursPushed:(UIButton *)sender
{
	btnSelectedEncours = YES;
	btnSelectedTerminee = NO;
   // if (self.parentType == kPARENT_MENU)
    //{
        self.buttonEncours.selected = YES;
        self.buttonTermine.selected = NO;
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"encours",@"type", nil];
	}
        [self postDataToServer:kCONTRAT_URL andParam:params];
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial;
	
		
    //}
}

- (IBAction)terminePushed:(UIButton *)sender
{
	
	btnSelectedEncours = NO;
	btnSelectedTerminee = YES;
   // if (self.parentType == kPARENT_MENU)
   // {
        self.buttonEncours.selected = NO;
        self.buttonTermine.selected = YES;
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"termine",@"type", nil];
	}
        [self postDataToServer:kCONTRAT_URL andParam:params];
	
	self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
	self.imgViewScrollDate.frame=frameDateImgMinInitial;
	
	self.btnMinMontant.frame=frameMontBtnMinInitial;
	self.labelMinMontant.frame=frameMontLblMinInitial;
	self.viewMinMontant.frame=frameViewMontMinInitial;
	
	self.btnMaxMontant.frame=frameMontBtnMaxInitial;
	self.labelMaxMontant.frame=frameMontLblMaxInitial;
	self.viewMaxMontant.frame=frameViewMontMaxInitial;
	
	self.buttonMinDate.frame=frameDateBtnMinInitial;
	self.labelMinDate.frame=frameDateLblMinInitial;
	self.viewMinDate.frame=frameViewDateMinInitial;
	
	self.buttonMaxDate.frame=frameDateBtnMaxInitial;
	self.labelMaxDate.frame=frameDateLblMaxInitial;
	self.viewMaxDate.frame=frameViewDateMaxInitial;
   // }
}


- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
	[super minScrollDraggedEnd:button withEvent:event];
	frameMontImgMin = self.imgViewScrollMontnat.frame;
	frameDateImgMin = self.imgViewScrollDate.frame;
	
	frameMontBtnMin = self.btnMinMontant.frame;
	frameMontLblMin = self.labelMinMontant.frame;
	
	frameMontBtnMax = self.btnMaxMontant.frame;
	frameMontLblMax = self.labelMaxMontant.frame;
	
	frameDateBtnMin = self.buttonMinDate.frame;
	frameDateLblMin = self.labelMinDate.frame;
	
	frameDateBtnMax = self.buttonMaxDate.frame;
	frameDateLblMax = self.labelMaxDate.frame;
	
	frameViewMontMin = self.viewMinMontant.frame;
	frameViewMontMax = self.viewMaxMontant.frame;
	frameViewDateMin = self.viewMinDate.frame;
	frameViewDateMax = self.viewMaxDate.frame;
	
	self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
	self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
	self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
	self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}

- (void)maxScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
	[super maxScrollDraggedEnd:button withEvent:event];
	frameMontImgMin = self.imgViewScrollMontnat.frame;
	frameDateImgMin = self.imgViewScrollDate.frame;
	
	frameMontBtnMin = self.btnMinMontant.frame;
	frameMontLblMin = self.labelMinMontant.frame;
	
	frameMontBtnMax = self.btnMaxMontant.frame;
	frameMontLblMax = self.labelMaxMontant.frame;
	
	frameDateBtnMin = self.buttonMinDate.frame;
	frameDateLblMin = self.labelMinDate.frame;
	
	frameDateBtnMax = self.buttonMaxDate.frame;
	frameDateLblMax = self.labelMaxDate.frame;
	
	frameViewMontMin = self.viewMinMontant.frame;
	frameViewMontMax = self.viewMaxMontant.frame;
	frameViewDateMin = self.viewMinDate.frame;
	frameViewDateMax = self.viewMaxDate.frame;
	
	self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
	self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
	self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
	self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}
 
@end

