//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ContratsViewController_iphone.h"
#import "ContratCell_iphone.h"
#import "FicheContratViewController_iphone.h"
#import "iCarouselExampleViewController.h"

@interface ContratsViewController_iphone ()

@end
CGRect  frameContrat;
@implementation ContratsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    if(!isLandscape){
        frameContrat=self.viewTableView.frame;
    }
    [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        self.viewTableView.frame=frameContrat;
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContratCell_iphone *cell = nil;
    if (isLandscape)
    {
		static NSString *cellIdentifier_l;
		if ([DataManager isIphoneLandScape])
		{ 
			cellIdentifier_l = @"ContratCell_iphone_l";
		}
		else
		{ 
			cellIdentifier_l = @"ContratCell_iphone5_l";
		}
			
        cell = (ContratCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ContratCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.labelContrat.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDateFin.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN"]];
        cell.labelDure.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"TTC"]];
        cell.labelHt.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
    }
    else
    {
        static NSString *cellIdentifier_p = @"ContratCell_iphone_p";
        
        cell = (ContratCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (ContratCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.labelContrat.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDure.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
    }
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (!isLandscape)
	{
		self.isFromButton = NO;
		
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/descriptionducontrat"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/descriptionducontrat"];
		 
		FicheContratViewController_iphone *ficheContratViewController_iphone = [[FicheContratViewController_iphone alloc] initWithNibName:@"FicheContratViewController_iphone" bundle:nil];
		[ficheContratViewController_iphone setParentType:self.parentType];
		[ficheContratViewController_iphone setNumContrat:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"]];
		[ficheContratViewController_iphone setNumTiers:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"TIERS"]];
		[self.navigationController pushViewController:ficheContratViewController_iphone animated:YES];
	}
}


@end
