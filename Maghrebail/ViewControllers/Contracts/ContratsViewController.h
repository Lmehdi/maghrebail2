//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ContratsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    //is Contrat or ligneCredit
    int parentType;
    NSString *numLigneCredit;
	
	CGRect frameViewMontMin;
	CGRect frameViewMontMax;
	
	CGRect frameViewDateMin;
	CGRect frameViewDateMax;
	
	CGRect frameMontBtnMin;
	CGRect frameMontImgMin;
	CGRect frameMontLblMin;
	
	CGRect frameDateBtnMin;
	CGRect frameDateImgMin;
	CGRect frameDateLblMin;
	
	
	CGRect frameMontBtnMax;
	CGRect frameMontImgMax;
	CGRect frameMontLblMax;
	
	CGRect frameDateBtnMax;
	CGRect frameDateImgMax;
	CGRect frameDateLblMax;
	
	
	CGRect frameViewMontMinInitial;
	CGRect frameViewMontMaxInitial;
	
	CGRect frameViewDateMinInitial;
	CGRect frameViewDateMaxInitial;
	
	CGRect frameMontBtnMinInitial;
	CGRect frameMontImgMinInitial;
	CGRect frameMontLblMinInitial;
	
	CGRect frameDateBtnMinInitial;
	CGRect frameDateImgMinInitial;
	CGRect frameDateLblMinInitial;
	
	
	CGRect frameMontBtnMaxInitial;
	CGRect frameMontImgMaxInitial;
	CGRect frameMontLblMaxInitial;
	
	CGRect frameDateBtnMaxInitial;
	CGRect frameDateImgMaxInitial;
	CGRect frameDateLblMaxInitial;
 
}

@property (retain, nonatomic) IBOutlet UIButton *btnFav;

@property (retain, nonatomic) IBOutlet UIButton *buttonEncours;
@property (retain, nonatomic) IBOutlet UIButton *buttonTermine;
@property (assign) int parentType;
@property (retain, nonatomic) NSString *numLigneCredit;
@property (retain, nonatomic) IBOutlet UIButton *buttonRetour;
@property (assign, nonatomic) BOOL isBack;

-(void) initHeaderColor;
- (IBAction)handlFav:(id)sender;

- (IBAction)filtrePushed:(UIButton *)sender;
- (IBAction)encoursPushed:(UIButton *)sender;
- (IBAction)terminePushed:(UIButton *)sender;
- (void)initiatView;


@end
