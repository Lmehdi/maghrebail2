//
//  iCarouselExampleViewController.m
//  iCarouselExample
//
//  Created by Nick Lockwood on 03/04/2011.
//  Copyright 2011 Charcoal Design. All rights reserved.
//

#import "iCarouselExampleViewController.h"
#import "DetailPDFViewController_iphone.h"
#import "DetailPDFViewController_ipad.h"

@interface iCarouselExampleViewController () <UIActionSheetDelegate>

@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;

@end


@implementation iCarouselExampleViewController
@synthesize numAffaire;
@synthesize imgViewHelp;

- (void)setUp
{
	//set up data
	_wrap = YES;
	self.items = [NSMutableArray array];
	for (int i = 0; i < 1000; i++)
	{
		[_items addObject:[NSNumber numberWithInt:i]];
	}
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	
    self.imgViewHelp.alpha = 0;
    
    self.navigationController.sideMenu.recognizer.enabled = NO;
}

- (void)hideImg
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:1.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    self.imgViewHelp.alpha = 0.0;
    
    [UIView commitAnimations];
}

- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
		shouldAddHeader = YES;
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
       
    }
    return self;
}

- (void)dealloc
{
	//it's a good idea to set these to nil here to avoid
	//sending messages to a deallocated viewcontroller
	_carousel.delegate = nil;
	_carousel.dataSource = nil;
    [imgViewHelp release];
}

#pragma mark -
#pragma mark View lifecycle

- (void)updateSliders
{
    switch (_carousel.type)
    {
        case iCarouselTypeLinear:
        {
            _arcSlider.enabled = NO;
        	_radiusSlider.enabled = NO;
            _tiltSlider.enabled = NO;
            _spacingSlider.enabled = YES;
            break;
        }
        case iCarouselTypeCylinder:
        case iCarouselTypeInvertedCylinder:
        case iCarouselTypeRotary:
        case iCarouselTypeInvertedRotary:
        case iCarouselTypeWheel:
        case iCarouselTypeInvertedWheel:
        {
            _arcSlider.enabled = YES;
        	_radiusSlider.enabled = YES;
            _tiltSlider.enabled = NO;
            _spacingSlider.enabled = YES;
            break;
        }
        default:
        {
            _arcSlider.enabled = NO;
        	_radiusSlider.enabled = NO;
            _tiltSlider.enabled = YES;
            _spacingSlider.enabled = YES;
            break;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    headerView.labelheaderTitre.text=@"MES DOCUMENTS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";

    //configure carousel
    
    _carousel.type = iCarouselTypeTimeMachine;
    _carousel.vertical= YES;
    _navItem.title = @"CoverFlow2";
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type",@"list",@"action",self.numAffaire,@"num", @"ios", @"os",nil];
    
    
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"termine",@"type",@"list",@"action",self.numAffaire,@"num", @"ios", @"os", nil];
    }
    [self postDataToServer:kDOCUMENT_URL andParam:params];
    
    arrayViews = [[NSMutableArray alloc] init];
    arrayWeb = [[NSMutableArray alloc] init];
    
    
}

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.mesPDF removeAllObjects];
        dataManager.mesPDF = [NSMutableArray arrayWithArray:reponse];
        self.imgViewHelp.alpha = 1.0;
        [self performSelector:@selector(hideImg) withObject:nil afterDelay:10.0];
        [_carousel reloadData];
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.carousel = nil;
    self.navItem = nil;
    self.orientationBarItem = nil;
    self.wrapBarItem = nil;
    self.arcSlider = nil;
    self.radiusSlider = nil;
    self.tiltSlider = nil;
    self.spacingSlider = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)switchCarouselType
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Select Carousel Type"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Linear", @"Rotary", @"Inverted Rotary", @"Cylinder", @"Inverted Cylinder", @"Wheel", @"Inverted Wheel", @"CoverFlow", @"CoverFlow2", @"Time Machine", @"Inverted Time Machine", nil];
    [sheet showInView:self.view];
}

- (IBAction)toggleOrientation
{
    //carousel orientation can be animated
    [UIView beginAnimations:nil context:nil];
    _carousel.vertical = !_carousel.vertical;
    [UIView commitAnimations];
    
    //update button
    _orientationBarItem.title = _carousel.vertical? @"Vertical": @"Horizontal";
}

- (IBAction)toggleWrap
{
    _wrap = !_wrap;
    _wrapBarItem.title = _wrap? @"Wrap: ON": @"Wrap: OFF";
    [_carousel reloadData];
}

- (IBAction)insertItem
{
    NSInteger index = MAX(0, _carousel.currentItemIndex);
    [_items insertObject:[NSNumber numberWithInt:_carousel.numberOfItems] atIndex:index];
    [_carousel insertItemAtIndex:index animated:YES];
}

- (IBAction)removeItem
{
    if (_carousel.numberOfItems > 0)
    {
        NSInteger index = _carousel.currentItemIndex;
        [_carousel removeItemAtIndex:index animated:YES];
        [_items removeObjectAtIndex:index];
    }
}

- (IBAction)reloadCarousel
{
    [_carousel reloadData];
}

#pragma mark -
#pragma mark UIActionSheet methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex	>= 0)
    {
        //map button index to carousel type
        iCarouselType type = buttonIndex;
        
        //carousel can smoothly animate between types
        [UIView beginAnimations:nil context:nil];
        _carousel.type = type;
        [self updateSliders];
        [UIView commitAnimations];
        
        //update title
        _navItem.title = [actionSheet buttonTitleAtIndex:buttonIndex];
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [dataManager.mesPDF count];
    //[_items count];
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UIWebView *wv = nil;
    
    NSLog(@"INDEX %d",index);
 
    //create new view if no view is available for recycling
   if (view == nil )
    {
        if (IS_IPAD)
        {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 500.0f, 500)];
        }
        else
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240.0f, 375.0f)];
        
        
        wv = [[UIWebView alloc]initWithFrame:CGRectMake(0,50,view.bounds.size.width,view.bounds.size.height-50)];
        wv.tag = -5;
    
       // NSLog(@"DOC %@", [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"DOC"]);
       // NSLog(@"CHEMIN %@", [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]);
        
        
        NSURL *targetURL = [NSURL URLWithString:[[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [wv loadRequest:request];
        
        
        [wv setUserInteractionEnabled:NO];
        [view setUserInteractionEnabled:NO];
        
        view.contentMode = UIViewContentModeCenter;
        label = [[UILabel alloc] initWithFrame:CGRectMake(0,10,view.bounds.size.width,50)];
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:15];
        label.tag = -1;
        
        /*UIView *vb = [[UIView alloc] initWithFrame:wv.frame];
        vb.backgroundColor = [UIColor lightGrayColor];
        vb.tag = index;
        UIActivityIndicatorView *ai = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(370, 250, 30, 30)];
        [ai startAnimating];
        ai.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
        [vb addSubview:ai];
        [arrayViews addObject:vb];
         
         wv.tag = index;
         wv.delegate = self;
         [view addSubview:vb];
         */
        //[arrayWeb addObject:wv];
        
        [view addSubview:wv];
        [view addSubview:label];
        
        
        
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:-1];
        
        wv = (UIWebView *)[view viewWithTag:-5];
       // [wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        [wv stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];
        
        /*if (index < arrayWeb.count)
        {
            wv = (UIWebView *)[arrayWeb objectAtIndex:index];
        }
        else
        {
            
            wv = (UIWebView *)[view viewWithTag:-5];
            NSURL *targetURL = [NSURL URLWithString:[[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]];
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [wv loadRequest:request];
            
            [arrayWeb addObject:wv];

        }*/
        
        
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel

    NSURL *targetURL = [NSURL URLWithString:[[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [wv loadRequest:request];
    
    label.text = [NSString stringWithFormat:@"%@", [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"DOC"]];
    
    return view;
}
- (BOOL)prefersStatusBarHidden {
    return NO;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    /*UIView *view = [[UIView alloc]initWithFrame:self.view.frame];
    UIWebView *wv = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height )];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(290, 3, 24, 24)];
    [btn setBackgroundImage:[UIImage imageNamed:@"ferme.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
   // NSURL *targetURL = [NSURL URLWithString:@"https://www.maghrebail.ma/leasebox/Documents/0000000060/0353430/Courrier%20lettre%20cheque%20chq%20Facture%20fournisseur%20acquitee%20(CBI)%2031%2010%202013%2017%2013%2049.pdf"];
    NSURL *targetURL = [NSURL URLWithString:[[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [wv loadRequest:request];
    [wv setUserInteractionEnabled:YES];
    [view addSubview:wv];
    [view addSubview:btn];
    
    [self.view addSubview:view];*/
    
    NSLog(@"DOC %@", [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"DOC"]);
    NSLog(@"CHEMIN %@", [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"]);
    
    if (!IS_IPAD)
    {
        DetailPDFViewController_iphone *viewC = [[DetailPDFViewController_iphone alloc] initWithNibName:@"DetailPDFViewController_iphone" bundle:nil];
        viewC.type = @"document";
        viewC.contratNum = [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"DOC"];
        viewC.urlPDF = [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"];
        viewC.contratNumMVT = self.numAffaire;
        [self.navigationController pushViewController:viewC animated:YES];
    }
    else
    {
        DetailPDFViewController_ipad *viewC = [[DetailPDFViewController_ipad alloc] initWithNibName:@"DetailPDFViewController_ipad" bundle:nil];
        viewC.type = @"document";
        viewC.contratNum = [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"DOC"];
        viewC.urlPDF = [[dataManager.mesPDF objectAtIndex:index] objectForKey:@"CHEMIN"];
        viewC.contratNumMVT = self.numAffaire;
        [self.navigationController pushViewController:viewC animated:YES];
    }
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webview tag %d", webView.tag);  
    
    for (NSInteger i = 0; i<arrayViews.count; i++)
    {
        UIView *v = [arrayViews objectAtIndex:i];
        if (v.tag == webView.tag)
        {
            [v removeFromSuperview];
        }
    }
    
    //NSLog(@"\n\n PDF LOADED %d %@", webView.tag, [[dataManager.mesPDF objectAtIndex:webView.tag] objectForKey:@"DOC"]);
}

- (void)webView:(UIWebView *) webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"\n\n PDF ERROR  %d %@ %@", webView.tag,[[dataManager.mesPDF objectAtIndex:webView.tag] objectForKey:@"DOC"], error.description);
}


-(IBAction)back:(id)sender{
    UIButton *btn = (UIButton *) sender;
    [btn.superview removeFromSuperview];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return _wrap;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionArc:
        {
            return 2 * M_PI * _arcSlider.value;
        }
        case iCarouselOptionRadius:
        {
            return value * _radiusSlider.value;
        }
        case iCarouselOptionTilt:
        {
            return _tiltSlider.value;
        }
        case iCarouselOptionSpacing:
        {
            return value * _spacingSlider.value;
        }
        default:
        {
            return value;
        }
    }
}

@end
