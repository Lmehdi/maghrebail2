//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ContratsViewController_ipad.h"
#import "ContratCell_ipad.h"
#import "FicheContratViewController_ipad.h"
#import "AppDelegate.h"
@interface ContratsViewController_ipad ()

@end

@implementation ContratsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContratCell_ipad *cell = nil;
    if (!isLandscape)
    {
        static NSString *cellIdentifier_l = @"ContratCell_ipad";
        cell = (ContratCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ContratCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }		
        cell.labelContrat.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDateFin.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN"]];
        cell.labelDure.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"TTC"]];
        cell.labelHt.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
    }
    else
    {
        static NSString *cellIdentifier_p = @"ContratCell_ipad";
        
        cell = (ContratCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (ContratCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.labelContrat.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDateFin.text = [DataManager getFormateDate:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN"]];
        cell.labelDure.text = [[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"TTC"]];
        cell.labelHt.text = [DataManager getFormatedNumero:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
		
    }
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (dataManager.isDemo) 
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/descriptionducontrat"];
	else 
		[Flurry logEvent:@"Home/EspaceAbonnés/descriptionducontrat"];
	
	self.isFromButton = NO;
    FicheContratViewController_ipad *ficheContratViewController_ipad = [[FicheContratViewController_ipad alloc] initWithNibName:@"FicheContratViewController_ipad" bundle:nil];
    [ficheContratViewController_ipad setParentType:self.parentType];
    [ficheContratViewController_ipad setNumContrat:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"]];
	 [ficheContratViewController_ipad setNumTiers:[[dataManager.contratFiltred  objectAtIndex:indexPath.row] objectForKey:@"TIERS"]];
    [self.navigationController pushViewController:ficheContratViewController_ipad animated:YES];
}


- (IBAction)filtrePushed:(UIButton *)sender
{
	//[super filtrePushed:sender];
	 AppDelegate* delegate=[[UIApplication sharedApplication] delegate];
    delegate.m_SideMenu.panMode=CHMFSideMenuPanModeNone;
	if (self.isFiltreViewShowed)
    {

        CGRect filtreFrame = self.viewFiltre.frame;
        filtreFrame = CGRectMake(1024,128,500,640);
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
      //  self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        
        self.isFiltreViewShowed = NO;
    }
    else
    {
        
        CGRect filtreFrame = self.viewFiltre.frame;
         filtreFrame = CGRectMake(650,128,500,640);
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
        
       // self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y + filtreFrame.size.height, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height - self.viewFiltre.frame.size.height);
        self.isFiltreViewShowed = YES;
    } 
	
	if (self.isFiltreViewShowed)
	{
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;
			/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			 if (dataManager.isDemo)
			 {
			 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			 }
			 [self postDataToServer:kCONTRAT_URL andParam:params];*/
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
				/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
				 if (dataManager.isDemo)
				 {
				 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
				 }
				 [self postDataToServer:kCONTRAT_URL andParam:params];*/
			}
		}
	} 
}

@end
