//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FicheContratViewController_ipad.h"
#import "BienCell_ipad.h"
#import "DetailBienViewController_ipad.h"

@interface FicheContratViewController_ipad ()

@end

@implementation FicheContratViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrolFicheContrat.contentSize = CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height + self.viewBienList.frame.size.height + self.viewInformations.frame.origin.y);
     
	
	/*
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewMontant.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+100)];*/
	
	/*
	[self showView2:nil];
	[self showView3:nil];*/
}

- (void)moveScroll
{
	[self.scrolFicheContrat setContentOffset:CGPointMake(0, 788) animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark Orientation Change

-(void)orientationChanged:(NSNotification *)note{
    [super orientationChanged:note];
    self.scrolFicheContrat.contentSize = CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height + self.viewBienList.frame.size.height + self.viewInformations.frame.origin.y);
}

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BienCell_ipad *cell = nil;

        static NSString *cellIdentifier_p = @"BienCell_ipad";
        
        cell = (BienCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (BienCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        NSDictionary *bien = [[dataManager.detailContrat objectForKey:@"biens"] objectAtIndex:indexPath.row];
        cell.labelDesignation.text = [bien objectForKey:@"DESIGN_BIEN"];
        cell.labelFournisseur.text = [bien objectForKey:@"FOURNISSEUR"];
        cell.labelStatut.text = [bien objectForKey:@"CAT_BIEN"];
        cell.labelMontantHt.text = [DataManager getFormatedNumero:[bien objectForKey:@"MHT"]];
        cell.labelMontantTtc.text = [DataManager getFormatedNumero:[bien objectForKey:@"MTTC"]];
   
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/descriptionducontrat/descriptiondubien"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/descriptionducontrat/descriptiondubien"];
    DetailBienViewController_ipad *detailBienViewController_ipad = [[DetailBienViewController_ipad alloc] initWithNibName:@"DetailBienViewController_ipad" bundle:nil];
    [detailBienViewController_ipad setParentType:self.parentType];
    [detailBienViewController_ipad setNumBien:[[[dataManager.detailContrat objectForKey:@"biens"] objectAtIndex:indexPath.row] objectForKey:@"NUM_BIEN"]];
    [self.navigationController pushViewController:detailBienViewController_ipad animated:YES];
}


- (IBAction)showView1:(id)sender {
	CGRect frame = self.viewInformations.frame;
	CGRect frame2 = self.viewDate.frame;
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewInformations.frame.size.height == 30)
	{
		frame.size.height = 193;
		[self.img1 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame.size.height = 30;
		[self.img1 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	
	frame2.origin.y = frame.origin.y + frame.size.height + 10;
	frame3.origin.y = frame2.origin.y + frame2.size.height + 10;
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	
	
	self.viewInformations.frame = frame;
	self.viewDate.frame = frame2;
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewMontant.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+50)];
}

- (IBAction)showView2:(id)sender {
	
	CGRect frame2 = self.viewDate.frame;
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewDate.frame.size.height == 30)
	{
		frame2.size.height = 228;
		[self.img2 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame2.size.height = 30;
		[self.img2 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	
	frame3.origin.y = frame2.origin.y + frame2.size.height + 10;
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	
	self.viewDate.frame = frame2;
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewMontant.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+50)];
}

- (IBAction)showView3:(id)sender {
	
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewMontant.frame.size.height == 30)
	{
		frame3.size.height = 308;
		[self.img3 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame3.size.height = 30;
		[self.img3 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	
	
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewMontant.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+50)];
	
}

@end
