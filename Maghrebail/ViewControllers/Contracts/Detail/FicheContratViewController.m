//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FicheContratViewController.h"

@interface FicheContratViewController ()

@end

@implementation FicheContratViewController

@synthesize numContrat;
@synthesize numTiers;
@synthesize parentType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        shouldDisplayDrag = NO;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=[self.labelTitleFiche.text uppercaseString];
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    // Do any additional setup after loading the view from its nib.
    if (self.parentType == kPARENT_MENU)
    {
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numContrat, @"num", @"encours", @"type",nil];
		if (dataManager.isDemo)
		{
			param = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        [self postDataToServer:kDETAIL_CONTRAT_URL andParam:param];
    }
    else if (self.parentType == kPARENT_LIGNECREDIT)
    {
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numContrat, @"num", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        [self postDataToServer:kLIGNE_CREDIT_DETAIL_CONTRAT_URL andParam:params];
    }
	if (IS_IPAD)
		self.labelTitleFiche.text = [NSString stringWithFormat:@"%@ %@ / %@",self.labelTitleFiche.text, numTiers, numContrat];
	else
		 		self.labelTitleFiche.text = [NSString stringWithFormat:@"%@ %@",self.labelTitleFiche.text, numContrat];
	
	if ([[NSUserDefaults standardUserDefaults] stringForKey:self.numContrat])
	{
		self.btnFav.selected = YES;
	}
	else
		self.btnFav.selected = NO;
}

- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{ 
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_labelAdresse release];
    [_labelRib release];
    [_labelAdresse release];
    [_LabelGerePar release];
    [_labelCategorie release];
    [_labelLoyer release];
    [_labelDateFin release];
    [_labelDure release];
    [_labelDureReste release];
    [_labelDateEcheance release];
    [_labelMontantHt release];
    [_labelMontantHtEcheance release];
    [_labelTotalPaye release];
    [_labelMontantDebloque release];
    [_labelValeurResid release];
    [_labelTotalPreLoyer release];
    [_labelMontantRestDebloque release];
    [_viewInformations release];
    [_viewDate release];
    [_viewMontant release];
    [_scrolFicheContrat release];
    [_viewBienList release];
	[_labelTitleFiche release];
    [_viewInfo release];
	[_viewDates release];
	[_viewMontants release];
	[_viewTableViews release];
	[_img1 release];
	[_img2 release];
	[_img3 release];
    [_btnFav release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelRib:nil];
    [self setLabelAdresse:nil];
    [self setLabelGerePar:nil];
    [self setLabelCategorie:nil];
    [self setLabelLoyer:nil];
    [self setLabelDateFin:nil];
    [self setLabelDure:nil];
    [self setLabelDureReste:nil];
    [self setLabelDateEcheance:nil];
    [self setLabelMontantHt:nil];
    [self setLabelMontantHtEcheance:nil];
    [self setLabelTotalPaye:nil];
    [self setLabelMontantDebloque:nil];
    [self setLabelValeurResid:nil];
    [self setLabelTotalPreLoyer:nil];
    [self setLabelMontantRestDebloque:nil];
    [self setViewInformations:nil];
    [self setViewDate:nil];
    [self setViewMontant:nil];
    [self setScrolFicheContrat:nil];
    [self setViewBienList:nil];
	[self setLabelTitleFiche:nil];
    [self setViewInfo:nil];
	[self setViewDates:nil];
	[self setViewMontants:nil];
	[self setViewTableViews:nil];
	[self setImg1:nil];
	[self setImg2:nil];
	[self setImg3:nil];
    [self setBtnFav:nil];
    [super viewDidUnload];
}

#pragma mark - updateView

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSDictionary *reponse = [self.response objectForKey:@"response"];
        dataManager.detailContrat = [NSDictionary dictionaryWithDictionary:reponse];
        //remplire fiche viewController & bien viewController
        [self remplirFicheContrat];
    }
    /*else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
}

#pragma mark - remplir fiche Contrat
- (IBAction)handlFav:(id)sender
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (self.btnFav.selected)
	{
		self.btnFav.selected = NO;
		[standardUserDefaults removeObjectForKey:self.numContrat];
	}
	else
	{
		self.btnFav.selected = YES;
		[standardUserDefaults setObject:@"CONTRAT" forKey:self.numContrat];
	}
	[standardUserDefaults synchronize];
}

- (void)remplirFicheContrat
{
    NSDictionary *ficheContrat = [[dataManager.detailContrat objectForKey:@"detail_contrat"] objectAtIndex:0];
    //remplir label
    self.labelRib.text = [ficheContrat objectForKey:@"NRIB"];
    self.labelAdresse.text = [ficheContrat objectForKey:@"ADRESSE_FACT"];
    self.LabelGerePar.text = [ficheContrat objectForKey:@"EXPLOITANT"];
    self.labelCategorie.text = [ficheContrat objectForKey:@"CAT_CONTRAT"];
    self.labelLoyer.text = [DataManager getFormateDate:[ficheContrat objectForKey:@"DATE_MISE_LOYER"]];
    self.labelDateFin.text = [DataManager getFormateDate:[ficheContrat objectForKey:@"DATE_FIN_CONTRAT"]];
    self.labelDure.text = [ficheContrat objectForKey:@"DURE"];
    self.labelDureReste.text = [ficheContrat objectForKey:@"JOUR_QUANTIEME"];
    self.labelDateEcheance.text = [DataManager getFormateDate:[ficheContrat objectForKey:@"DATE_DERNIERE_ECHEANCE"]];
    self.labelMontantHt.text = [ficheContrat objectForKey:@"MHT"];
    self.labelMontantHtEcheance.text = [ficheContrat objectForKey:@"MHT_DERNIERE_ECHEANCE"];
    self.labelTotalPaye.text = [ficheContrat objectForKey:@"TOTAL_LOYE_PAYE"];
    self.labelMontantDebloque.text = [ficheContrat objectForKey:@"MT_DEBLOQUE"];
    self.labelValeurResid.text = [ficheContrat objectForKey:@"VR"];
    self.labelTotalPreLoyer.text = [ficheContrat objectForKey:@"TOTAL_PRELOYER_PAYE"];
    self.labelMontantRestDebloque.text = [ficheContrat objectForKey:@"RESTE_MT_DEBLOQUE"];
    //remplir table
    [_tableView reloadData];
}

- (IBAction)showView1:(id)sender {
	
}

- (IBAction)showView2:(id)sender {
}

- (IBAction)showView3:(id)sender {
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *bienList = [dataManager.detailContrat objectForKey:@"biens"];
    return [bienList  count];
}

#pragma mark - Orientation Change

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    [self remplirFicheContrat];
}

@end
