//
//  MaLigneCreditCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BienCell.h"

@implementation BienCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [_labelDesignation release];
    [_labelFournisseur release];
    [_labelStatut release];
    [_labelMontantHt release];
    [_labelMontantTtc release];
    [_imgViewBackground release];
    [super dealloc];
}
@end
