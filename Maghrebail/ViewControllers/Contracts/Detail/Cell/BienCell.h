//
//  MaLigneCreditCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BienCell : UITableViewCell
{
    
}

@property (retain, nonatomic) IBOutlet UILabel *labelDesignation;
@property (retain, nonatomic) IBOutlet UILabel *labelFournisseur;
@property (retain, nonatomic) IBOutlet UILabel *labelStatut;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantHt;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantTtc;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;

@end
