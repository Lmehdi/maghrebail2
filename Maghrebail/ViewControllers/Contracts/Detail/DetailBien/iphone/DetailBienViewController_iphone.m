//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailBienViewController_iphone.h"

@interface DetailBienViewController_iphone ()

@end

@implementation DetailBienViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    [self.scrollContent setContentSize:CGSizeMake(320, 508)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
