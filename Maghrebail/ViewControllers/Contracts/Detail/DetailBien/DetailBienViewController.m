//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailBienViewController.h"
 
@interface DetailBienViewController ()

@end

@implementation DetailBienViewController

@synthesize parentType;
@synthesize numBien;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		shouldAutorate = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
    // Do any additional setup after loading the view from its nib.
    self.viewBien.alpha = 0;

        if (self.parentType == kPARENT_MENU)
        {
            NSMutableDictionary *param = [NSMutableDictionary dictionaryWithObject:self.numBien forKey:@"num"];
			if (dataManager.isDemo)
			{
				param = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
            [self postDataToServer:kDETAIL_BIEN_URL andParam:param];
        }
        else if (self.parentType == kPARENT_LIGNECREDIT)
        {
            NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numBien, @"num", [dataManager.authentificationData objectForKey:@"EMAIL"], @"email", nil];
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
            [self postDataToServer:kLIGNE_CREDIT_DETAIL_BIEN_URL andParam:params];
        }
	
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"DESCRIPTION DU BIEN";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)goBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_labelDesignation release];
    [_labelCaracterestique release];
    [_labelStatut release];
    [_labelSerie release];
    [_labelLieuLivraison release];
    [_labelDateLivraison release];
    [_labelFournisseur release];
    [_labelMontantHt release];
    [_labelMontantTtc release];
    [_viewBien release];
    [_scrollDetailBien release];
    [_scrollContent release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelDesignation:nil];
    [self setLabelCaracterestique:nil];
    [self setLabelStatut:nil];
    [self setLabelSerie:nil];
    [self setLabelLieuLivraison:nil];
    [self setLabelDateLivraison:nil];
    [self setLabelFournisseur:nil];
    [self setLabelMontantHt:nil];
    [self setLabelMontantTtc:nil];
    [self setViewBien:nil];
    [self setScrollDetailBien:nil];
    [super viewDidUnload];
}

#pragma mark - updateView

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSDictionary *reponse = [[self.response objectForKey:@"response"] objectAtIndex:0];
        dataManager.detailBien = [NSDictionary dictionaryWithDictionary:reponse];
        //remplir Fiche client and Contact
        [self remplirLabelBien];
        
    }
  /*  else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
}

- (void)remplirLabelBien
{
    self.labelDesignation.text = [dataManager.detailBien objectForKey:@"DESIGN_BIEN"];
    self.labelCaracterestique.text = [dataManager.detailBien objectForKey:@"CARACT_BIEN"];
    self.labelStatut.text = [dataManager.detailBien objectForKey:@"CAT_BIEN"];
    self.labelSerie.text = [dataManager.detailBien objectForKey:@"NUM_SERIE"];
    self.labelLieuLivraison.text = [dataManager.detailBien objectForKey:@"LIEU_LIVRAISON"];
    self.labelDateLivraison.text = [DataManager getFormateDate:[dataManager.detailBien objectForKey:@"DATE_LIVRAISON"]];
    self.labelFournisseur.text = [dataManager.detailBien objectForKey:@"FOURNISSEUR"];
    self.labelMontantHt.text = [dataManager.detailBien objectForKey:@"MHT"];
    self.labelMontantTtc.text = [dataManager.detailBien objectForKey:@"MTTC"];
    //fiche animation
    [UIView beginAnimations:@"ScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelay:0];
    self.viewBien.alpha = 1;
    [UIView commitAnimations];
    self.scrollDetailBien.contentSize = CGSizeMake(self.scrollDetailBien.frame.size.width, self.viewBien.frame.size.height + self.viewBien.frame.origin.y);
}


@end
