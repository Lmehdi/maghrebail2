//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DetailBienViewController : BaseViewController
{
    int parentType;
    NSString *numBien;
}

@property (retain, nonatomic) NSString *numBien;
@property (retain, nonatomic) IBOutlet UILabel *labelDesignation;
@property (retain, nonatomic) IBOutlet UILabel *labelCaracterestique;
@property (retain, nonatomic) IBOutlet UILabel *labelStatut;
@property (retain, nonatomic) IBOutlet UILabel *labelSerie;
@property (retain, nonatomic) IBOutlet UILabel *labelLieuLivraison;
@property (retain, nonatomic) IBOutlet UILabel *labelDateLivraison;
@property (retain, nonatomic) IBOutlet UILabel *labelFournisseur;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantHt;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantTtc;
@property (retain, nonatomic) IBOutlet UIView *viewBien;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollDetailBien;
@property (assign) int parentType;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollContent;

- (void)remplirLabelBien;
-(void) initHeaderColor;


@end
