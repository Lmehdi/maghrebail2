//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface FicheContratViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    float ficheClientHeight;
    NSString *numContrat;
    int parentType;
}

@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property (assign) int parentType;
@property (retain, nonatomic) NSString *numContrat;
@property (retain, nonatomic) NSString *numTiers;
//label list
@property (retain, nonatomic) IBOutlet UILabel *labelRib;
@property (retain, nonatomic) IBOutlet UILabel *labelAdresse;
@property (retain, nonatomic) IBOutlet UILabel *LabelGerePar;
@property (retain, nonatomic) IBOutlet UILabel *labelCategorie;
@property (retain, nonatomic) IBOutlet UILabel *labelLoyer;
@property (retain, nonatomic) IBOutlet UILabel *labelDateFin;
@property (retain, nonatomic) IBOutlet UILabel *labelDure;
@property (retain, nonatomic) IBOutlet UILabel *labelDureReste;
@property (retain, nonatomic) IBOutlet UILabel *labelDateEcheance;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantHt;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantHtEcheance;
@property (retain, nonatomic) IBOutlet UILabel *labelTotalPaye;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantDebloque;
@property (retain, nonatomic) IBOutlet UILabel *labelValeurResid;
@property (retain, nonatomic) IBOutlet UILabel *labelTotalPreLoyer;
@property (retain, nonatomic) IBOutlet UILabel *labelMontantRestDebloque;
//view
@property (retain, nonatomic) IBOutlet UIView *viewInformations;
@property (retain, nonatomic) IBOutlet UIView *viewDate;
@property (retain, nonatomic) IBOutlet UIView *viewMontant;
@property (retain, nonatomic) IBOutlet UIScrollView *scrolFicheContrat;
@property (retain, nonatomic) IBOutlet UIView *viewBienList;
@property (retain, nonatomic) IBOutlet UILabel *labelTitleFiche;
@property (retain, nonatomic) IBOutlet UIView *viewInfo; 
@property (retain, nonatomic) IBOutlet UIView *viewDates;
@property (retain, nonatomic) IBOutlet UIView *viewMontants;
@property (retain, nonatomic) IBOutlet UIView *viewTableViews;
@property (retain, nonatomic) IBOutlet UIImageView *img1;
@property (retain, nonatomic) IBOutlet UIImageView *img2;
@property (retain, nonatomic) IBOutlet UIImageView *img3;

-(void) initHeaderColor;
- (IBAction)handlFav:(id)sender;
- (void)remplirFicheContrat;
- (IBAction)showView1:(id)sender;
- (IBAction)showView2:(id)sender;
- (IBAction)showView3:(id)sender;

@end
