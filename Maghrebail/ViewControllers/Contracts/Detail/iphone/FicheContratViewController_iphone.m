//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FicheContratViewController_iphone.h"
#import "BienCell_iphone.h"
#import "DetailBienViewController_iphone.h"

@interface FicheContratViewController_iphone ()

@end

@implementation FicheContratViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
   
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewMontant.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+100)];
	
	
	[self showView2:nil];
	[self showView3:nil];
	
	//[self performSelector:@selector(moveScroll) withObject:nil afterDelay:2.0];
	 
}

- (void)moveScroll
{
	[self showView2:nil];
	[self showView3:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark --
#pragma mark Orientation Change

-(void)orientationChanged:(NSNotification *)note{
    [super orientationChanged:note];
    self.scrolFicheContrat.contentSize = CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height + self.viewBienList.frame.size.height + self.viewInformations.frame.origin.y);
}*/

#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BienCell_iphone *cell = nil;
   /* if (isLandscape)
    {
        static NSString *cellIdentifier_l = @"BienCell_iphone_l";
        cell = (BienCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (BienCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        NSDictionary *bien = [[dataManager.detailContrat objectForKey:@"biens"] objectAtIndex:indexPath.row];
        cell.labelDesignation.text = [bien objectForKey:@"DESIGN_BIEN"];
        cell.labelFournisseur.text = [bien objectForKey:@"FOURNISSEUR"];
        cell.labelStatut.text = [bien objectForKey:@"CAT_BIEN"];
        cell.labelMontantHt.text = [DataManager getFormatedNumero:[bien objectForKey:@"MHT"]];
        cell.labelMontantTtc.text = [DataManager getFormatedNumero:[bien objectForKey:@"MTTC"]];
    }
    else
    {*/
        static NSString *cellIdentifier_p = @"BienCell_iphone_p";
        
        cell = (BienCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (BienCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
    
        NSDictionary *bien = [[dataManager.detailContrat objectForKey:@"biens"] objectAtIndex:indexPath.row];
        cell.labelDesignation.text = [bien objectForKey:@"DESIGN_BIEN"];
        cell.labelFournisseur.text = [bien objectForKey:@"FOURNISSEUR"];
        cell.labelStatut.text = [bien objectForKey:@"CAT_BIEN"];
        cell.labelMontantHt.text = [DataManager getFormatedNumero:[bien objectForKey:@"MHT"]];
        cell.labelMontantTtc.text = [DataManager getFormatedNumero:[bien objectForKey:@"MTTC"]];
  //  }
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/descriptionducontrat/descriptiondubien"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/descriptionducontrat/descriptiondubien"];
    DetailBienViewController_iphone *detailBienViewController_iphone = [[DetailBienViewController_iphone alloc] initWithNibName:@"DetailBienViewController_iphone" bundle:nil];
    [detailBienViewController_iphone setParentType:self.parentType];
    [detailBienViewController_iphone setNumBien:[[[dataManager.detailContrat objectForKey:@"biens"] objectAtIndex:indexPath.row] objectForKey:@"NUM_BIEN"]];
    [self.navigationController pushViewController:detailBienViewController_iphone animated:YES];
}
- (IBAction)showView1:(id)sender {
	CGRect frame = self.viewInformations.frame;
	CGRect frame2 = self.viewDate.frame;
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewInformations.frame.size.height == 30)
	{
		frame.size.height = 193;
		[self.img1 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame.size.height = 30;
		[self.img1 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	
	frame2.origin.y = frame.origin.y + frame.size.height + 10;
	frame3.origin.y = frame2.origin.y + frame2.size.height + 10;
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	 
	
	self.viewInformations.frame = frame;
	self.viewDate.frame = frame2;
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+ self.viewBienList.frame.size.height+50)];
}

- (IBAction)showView2:(id)sender {
	 
	CGRect frame2 = self.viewDate.frame;
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewDate.frame.size.height == 30)
	{
		frame2.size.height = 228;
		[self.img2 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame2.size.height = 30;
		[self.img2 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	 
	frame3.origin.y = frame2.origin.y + frame2.size.height + 10;
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	  
	self.viewDate.frame = frame2;
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+ self.viewBienList.frame.size.height+50)];
}

- (IBAction)showView3:(id)sender {
	 
	CGRect frame3 = self.viewMontant.frame;
	CGRect frame4 = self.viewBienList.frame;
	if (self.viewMontant.frame.size.height == 30)
	{
		frame3.size.height = 308;
		[self.img3 setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{
		frame3.size.height = 30;
		[self.img3 setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	  
	frame4.origin.y = frame3.origin.y + frame3.size.height + 10;
	
	
	self.viewMontant.frame = frame3;
	self.viewBienList.frame = frame4;
	
	[self.scrolFicheContrat setContentSize:CGSizeMake(self.scrolFicheContrat.frame.size.width, self.viewInformations.frame.size.height + self.viewDate.frame.size.height + self.viewMontant.frame.size.height+ self.viewBienList.frame.size.height+50)];
	
}

@end
