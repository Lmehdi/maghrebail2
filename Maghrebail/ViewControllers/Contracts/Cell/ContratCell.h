//
//  MaLigneCreditCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContratCell : UITableViewCell
{
    
}

@property (retain, nonatomic) IBOutlet UILabel *labelContrat;
@property (retain, nonatomic) IBOutlet UILabel *labelLoyer;
@property (retain, nonatomic) IBOutlet UILabel *labelStatut;
@property (retain, nonatomic) IBOutlet UILabel *labelDateFin;
@property (retain, nonatomic) IBOutlet UILabel *labelDure;
@property (retain, nonatomic) IBOutlet UILabel *labelTtc;
@property (retain, nonatomic) IBOutlet UILabel *labelHt;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;


@end
