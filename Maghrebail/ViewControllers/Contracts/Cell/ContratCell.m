//
//  MaLigneCreditCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ContratCell.h"

@implementation ContratCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [_labelContrat release];
    [_labelLoyer release];
    [_labelStatut release];
    [_labelDateFin release];
    [_labelDure release];
    [_labelTtc release];
    [_labelHt release];
    [_imgViewBackground release];
    [super dealloc];
}
@end
