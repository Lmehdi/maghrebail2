//
//  AuthentificationViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "EHFAuthenticator.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "TouchIDVC_iphone.h"
#import "DropDownViewController.h"

@class AuthentificationViewController;
@protocol CompteMenuDelegate <NSObject>

@required

- (void)showCompteMenu;

@end

@interface AuthentificationViewController : BaseViewController<TouchIDVCDelegate,UIAlertViewDelegate,DropDownViewDelegate,UITableViewDataSource,UITableViewDataSource>
{
    id<CompteMenuDelegate>delegate;
    
    UIButton *button;
    
    
   	DropDownViewController *dropDownView;

}
@property (retain, nonatomic) IBOutlet UILabel *typecompte;
@property (retain, nonatomic) IBOutlet UIView *viewtablechoice;
@property (retain, nonatomic) IBOutlet UIButton *togglebuttonuser;

@property (retain, nonatomic) IBOutlet UIView *viewTouchID;
- (IBAction)btnShowTouchIDScaner:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgBgEmail;
@property (retain, nonatomic) IBOutlet UIImageView *imgBgPassword;
@property (retain, nonatomic) IBOutlet NSArray *data;
@property (nonatomic,retain) NSString *selectedchoice;
@property (nonatomic,retain) NSString *typedecompte;
-(IBAction)actionTypeCompte;
@property (retain, nonatomic) IBOutlet UIButton *buttonfilter;

@property(assign) BOOL supportTouchID;
@property(assign) BOOL scanneTouchID;
@property(assign) BOOL toggleIsOnClient;
@property (retain, nonatomic) IBOutlet UITableView *tableviewchoice;
@property (retain, nonatomic) IBOutlet UIButton *toggle;
@property (retain, nonatomic) IBOutlet UILabel *clientlabel;
@property (retain, nonatomic) IBOutlet UILabel *fournisseurlabel;

@property (retain, nonatomic) IBOutlet UIButton *btnInfoSecurite;
@property(retain)id delegate;
@property (retain, nonatomic) IBOutlet UITextField *textFieldLogin;
@property (retain, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorAuthentification;
@property (retain, nonatomic) IBOutlet UIButton *buttonAuthentification;
@property (retain, nonatomic) IBOutlet UISwitch *switchSave;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollPage;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, assign) NSInteger index;
@property (retain, nonatomic) IBOutlet UIButton *demobtn;
- (IBAction)toggle:(id)sender;

- (IBAction)demopush:(id)sender;

-(void) initHeaderColor;
-(void)showtext;

- (IBAction)AuthentificationPushed:(UIButton *)sender;
- (IBAction)switcheSave:(UISwitch *)sender;
- (IBAction)showInfoView:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *viewFaceID;

@end
