//
//  InfoViewController.m
//  Maghrebail
//
//  Created by MAC on 20/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize FormDash;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.FormDash=NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	// Do any additional setup after loading the view.
	
	[Flurry logEvent:@"Home/EspaceAbonnés/Infossécurité"];
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader setHidden:true];
     if (dataManager.infoSecurite.count > 0)
    {
        self.labelTitle.text = [[[dataManager.infoSecurite objectAtIndex:0] objectForKey:@"titre"] uppercaseString];
        self.labelContenu.text = [[dataManager.infoSecurite objectAtIndex:0] objectForKey:@"description"];
    }
    
	CGSize maximumLabelSize = CGSizeMake(self.labelContenu.frame.size.width,9999);
	  
	CGSize expectedLabelSize = [self.labelContenu.text sizeWithFont:self.labelContenu.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelContenu.lineBreakMode];
	
	//adjust the label the the new height.
	
	CGRect newFrame = self.labelContenu.frame;
	newFrame.size.height = expectedLabelSize.height + 10;
	self.labelContenu.frame = newFrame;
	[self.scrollContainer setContentSize:CGSizeMake(self.scrollContainer.frame.size.width, self.labelContenu.frame.size.height+20)];
	if (![DataManager isIphone5] && !IS_IPAD)
	{
		CGRect frameScroll = self.scrollContainer.frame;
		frameScroll.size.height = 370;
		//self.scrollContainer.frame = frameScroll;
	}
}
-(void) initHeaderColor{
    
    headerView.labelheaderTitre.text=@"INFOS SÉCURITÉ";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)goBack
{
    if (self.FormDash) {
        [self.view removeFromSuperview];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClosePushed:(id)sender {
}
- (void)dealloc {
    [_scrollContainer release];
    [_labelTitle release];
    [_labelContenu release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollContainer:nil];
    [self setLabelTitle:nil];
    [self setLabelContenu:nil];
    [super viewDidUnload];
}
@end
