//
//  InfoViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 20/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "InfoViewController_iphone.h"

@interface InfoViewController_iphone ()

@end

@implementation InfoViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

@end
