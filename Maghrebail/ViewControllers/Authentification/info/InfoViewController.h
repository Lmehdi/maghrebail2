//
//  InfoViewController.h
//  Maghrebail
//
//  Created by MAC on 20/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface InfoViewController : BaseViewController

@property (assign) BOOL FormDash;
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelContenu;


@property (retain, nonatomic) IBOutlet UIScrollView *scrollContainer;

-(void) initHeaderColor;
- (IBAction)btnClosePushed:(id)sender;
@end
