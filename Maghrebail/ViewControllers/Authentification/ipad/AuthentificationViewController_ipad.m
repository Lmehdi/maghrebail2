//
//  AuthentificationViewController_ipad.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/6/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AuthentificationViewController_ipad.h"
#import "InfoViewController_ipad.h"

@interface AuthentificationViewController_ipad ()

@end

@implementation AuthentificationViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showInfoView:(id)sender
{
	InfoViewController_ipad *aboutShowViewController = [[InfoViewController_ipad alloc] initWithNibName:@"InfoViewController_ipad" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:aboutShowViewController animated:YES];
    [aboutShowViewController release];
}
@end
