//
//  DropDownViewController.h
//  Maghrebail
//
//  Created by Belkheir on 04/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
@protocol DropDownViewDelegate

@required

-(void)dropDownCellSelected:(NSInteger)returnIndex;

@end


@interface DropDownViewController : UIViewController<UITableViewDataSource,UITableViewDataSource>
{

id<DropDownViewDelegate> delegate;


}
@property (retain, nonatomic) IBOutlet UITableView *tabledropdown;
@property (nonatomic,retain) NSArray *arrayData;
@property (nonatomic,assign) id<DropDownViewDelegate> delegate;

@end
