//
//  AuthentificationViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AuthentificationViewController.h"
#import "MonProfilViewController_iphone.h"
#import "MonProfilViewController_ipad.h"
#import "appdelegate.h"
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonDigest.h>



@interface AuthentificationViewController ()

@end

@implementation AuthentificationViewController
NSString   *datafound;
@synthesize delegate,supportTouchID,scanneTouchID,index,toggleIsOnClient;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		if(![DataManager isIphoneX]) {
			self.supportTouchID=YES;
			self.scanneTouchID=NO;
		}        
    }
    
    return self;
}






- (void)viewDidLoad
{
   
    self.textFieldPassword.text = @"leasebox08.";
    
    NSData *dataTouche=[DataManager readDataIntoCachWith:@"3DTOUCH"];
    
    if(dataTouche!=nil){
      datafound=[NSKeyedUnarchiver unarchiveObjectWithData:dataTouche];
        
        if([datafound isEqualToString:@"YES"]){
            self.demobtn.hidden=YES;
        }else{
                  self.demobtn.hidden=NO;
        }
        
    }
    
 
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];

    [super viewDidLoad];
    [self initHeaderColor];
   
    
    self.selectedchoice=@"CLIENT";
    if(dataManager.isStayOn)
    {
        
        [standardUserDefaults setBool:YES forKey:@"DataAuthentificationClient"];
        [standardUserDefaults setBool:NO forKey:@"DataAuthentificationFournisseur"];
        
    }
    
    
     self.clientlabel.textColor = [UIColor colorWithRed:53.0/255 green:88.0/255 blue:131.0/255 alpha:1];
    if(dataManager.isFournisseur==YES)
    {
       
        
        self.selectedchoice=@"FOURNISSEUR";
        self.fournisseurlabel.textColor = [UIColor colorWithRed:53.0/255 green:88.0/255 blue:131.0/255 alpha:1];
        self.clientlabel.textColor = [UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1];
      
    //    [self.buttonfilter setTitle:@"FOURNISSEUR" forState:UIControlStateNormal];
     //   [self.buttonfilter setEnabled:NO];
      // self.buttonfilter.userInteractionEnabled = NO;
        [self.togglebuttonuser setImage:[UIImage imageNamed:@"fournisseur_toogle.png"] forState:UIControlStateNormal];
        [self.togglebuttonuser setEnabled:NO];
         self.togglebuttonuser.userInteractionEnabled = NO;
        
        dataManager.isFournisseur=NO;
    
        
    }
    if(dataManager.isClient==YES)
    {
      
        self.selectedchoice=@"CLIENT";
         self.clientlabel.textColor = [UIColor colorWithRed:53.0/255 green:88.0/255 blue:131.0/255 alpha:1];
        [self.togglebuttonuser setImage:[UIImage imageNamed:@"client_toogle.png"] forState:UIControlStateNormal];
        [self.togglebuttonuser setEnabled:NO];
        self.togglebuttonuser.userInteractionEnabled = NO;
        
       // [self.buttonfilter setTitle:@"CLIENT" forState:UIControlStateNormal];
        //   [self.buttonfilter setEnabled:NO];
         // self.buttonfilter.userInteractionEnabled = NO;
        dataManager.isClient=NO;
    
    
    }
    
    
    self.typedecompte=@"";
    // Do any additional setup after loading the view from its nib.
    self.scrollPage.contentSize = CGSizeMake(self.scrollPage.frame.size.width, self.scrollPage.frame.size.height);
    self.buttonAuthentification.layer.masksToBounds=YES;
    self.buttonAuthentification.layer.cornerRadius=2;
  
    if (dataManager.firstTime) {
        [self showSideMenu];
  
        [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(hide) userInfo:nil repeats:NO];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"USER-EMAIL"])
    {
        self.textFieldLogin.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER-EMAIL"];
    }
    
    [self.btnInfoSecurite addTarget:self action:@selector(showInfoView:) forControlEvents:UIControlEventTouchUpInside];
    
    /*NSString *html = @"<table width=\"135\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" title=\"Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.\"><tr><td width=\"135\" align=\"center\" valign=\"top\"><script type=\"text/javascript\" src=\"https://seal.verisign.com/getseal?host_name=www.maghrebail.ma&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=fr\"></script><br /><a href=\"http://www.verisign.fr/products-services/security-services/ssl/ssl-information-center/\" target=\"_blank\"  style=\"color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;\">&#192; propos des certificats SSL</a></td></tr></table>";
     
     NSString *html = @"<table width=\"135\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" title=\"Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.\"><tr><td width=\"135\" align=\"center\" valign=\"top\"><script type=\"text/javascript\" src=\"https://seal.verisign.com/getseal?host_name=www.maghrebail.ma&amp;size=M&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=fr\"></script><br /><a href=\"http://www.verisign.fr/products-services/security-services/ssl/ssl-information-center/\" target=\"_blank\"   text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;\">&#192; propos des certificats SSL</a></td></tr></table>";
     // Load it to the webview
     [self.webView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];*/
  //  [viewtablechoice setHidden:YES];
    
    
    
    //[self localAuthtification];
    
}
- (IBAction)didselectFaceId:(UIButton *)sender {
    [self localAuthtification];
}


-(void ) localAuthtification {
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"error";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    // User authenticated successfully, take appropriate action
                                    
                                    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"passwordFace"]) {
                                        AppDelegate* maindelegate = [[UIApplication sharedApplication] delegate];
                                        
                                        [self dismissKeyboard];
                                        [self.activityIndicatorAuthentification startAnimating];
                                        
                                        
                                        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
                                       // [[NSUserDefaults standardUserDefaults] setObject:@"mustapha.dany@maghrebail.ma" forKey:@"email"];
                                        dataManager.isDemo = NO;
                                        
                                        NSString  *email = @"";
                                        NSString  *pass = @"";
                                        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"email"] && [[NSUserDefaults standardUserDefaults] objectForKey:@"passwordFace"]) {
                                            email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
                                            pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"passwordFace"];
                                        }
                                        
                                        if (dataManager.isDemo)
                                        {
                                            paramDic = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
                                        }
                                        if ([self.selectedchoice isEqualToString:@"CLIENT"])
                                        {
                                            
                                            
                                            NSString *mobileModel = [ maindelegate getModel];
                                            float versionOS = [[[UIDevice currentDevice] systemVersion] floatValue];
                                            NSString *VersionOS = [NSString stringWithFormat:@"%f", versionOS];
                                            
                                            // [DataManager createKeychainValue:self.textFieldPassword.text forIdentifier:@"PasswordClient"];//
                                            //   [DataManager createKeychainValue:@"1" forIdentifier:@"casprofil"];
                                            [paramDic setObject:email forKey:@"email"];
                                            [paramDic setObject:pass forKey:@"password"];
                                            [paramDic setObject:VersionOS forKey:@"mobileOs"];
                                            [paramDic setObject:mobileModel forKey:@"mobileModel"];
                                            
                                            
                                            [self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic andTag:7];
                                            
                                            
                                        } else if([self.selectedchoice isEqualToString:@"FOURNISSEUR"]) {
                                            //NSString * password = pass;
                                            NSString *cryptedpassword=[ DataManager hashSHA1:pass salt:SALT];
                                            // [DataManager createKeychainValue:cryptedpassword forIdentifier:@"PasswordFournisseur"];
                                            
                                            
                                            self.typedecompte=@"FOUR";
                                            NSArray *strings = [[NSArray alloc] initWithObjects:kNEWAUTHENTIFICATION_URL,email,cryptedpassword,self.typedecompte,@"1",@"1",nil];
                                            NSString *URLString = [strings componentsJoinedByString:@"/"];
                                            [self.activityIndicatorAuthentification startAnimating];
                                            
                                            [self getRemoteContentAuth:URLString username:email password:cryptedpassword andTag:8];
                                            
                
                                        }
                                    }else {
                                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Face ID"
                                                                                                       message:@"Votre Face ID n'est pas associé."
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                              handler:^(UIAlertAction * action) {}];
                                        
                                        [alert addAction:defaultAction];
                                        [self presentViewController:alert animated:YES completion:nil];
                                    }
                                    
                                    
  
                                } else {
                                    // User did not authenticate successfully, look at error and take appropriate action
                                    
                                    NSLog(@"error");
                                }
                            }];
    } else {
        // Could not evaluate policy; look at authError and present an appropriate message to user
    }
    
    
    
    
}



- (IBAction)toggle:(id)sender
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
   
    
    if(toggleIsOnClient)
    {
        if(dataManager.isStayOn)
        {
            [standardUserDefaults setBool:YES forKey:@"DataAuthentificationClient"];
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationFournisseur"];
            
        }
        self.selectedchoice=@"CLIENT";
        [self.togglebuttonuser setImage:[UIImage imageNamed:@"client_toogle.png"]  forState:UIControlStateNormal];
        self.fournisseurlabel.textColor = [UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1];
        self.clientlabel.textColor = [UIColor colorWithRed:53.0/255 green:88.0/255 blue:131.0/255 alpha:1];
        
        if  (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]))
        {
            
            [self.viewTouchID setHidden:false];
            
            
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] isEqualToString:@"no"])
            {
                
                [self vertifyIdentity];
                // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                
            }
            else
            {
                
                // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
            }
            
        }
        
        
        
        else
        {
            [self.viewTouchID setHidden:true];
            [self initDefaultAction];
        }
        
      
    }
    
    
    
    else
    {
        if(dataManager.isStayOn)
        {
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationClient"];
            [standardUserDefaults setBool:YES forKey:@"DataAuthentificationFournisseur"];
            
        }

        if  (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"yes"]))
        {
            
            [self.viewTouchID setHidden:false];
            
            
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] isEqualToString:@"no"])
            {
                
                [self vertifyIdentity];
                // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                
            }
            else
            {
                
                // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
            }
            
        }
        
        
        
        else
        {
            [self.viewTouchID setHidden:true];
            [self initDefaultAction];
        }

        
        self.selectedchoice=@"FOURNISSEUR";
        [self.togglebuttonuser setImage:[UIImage imageNamed:@"fournisseur_toogle.png"]  forState:UIControlStateNormal];
        self.clientlabel.textColor = [UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1];
        self.fournisseurlabel.textColor = [UIColor colorWithRed:53.0/255 green:88.0/255 blue:131.0/255 alpha:1];
        
       

    }
    toggleIsOnClient = !toggleIsOnClient;
    ;
   
}






-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"ME CONNECTER";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)showInterface1
{
    
  //  [self.view removeFromSuperview];
    switch (index)
    {
        case 0:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 1:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 2:
        {
            [dataManager.slideMenu produitEquipement:YES];
        }
            break;
        case 3:
        {
            [dataManager.slideMenu simulator:YES];
        }
            break;
        case 4:
        {
            [dataManager.slideMenu produitImmobilier:YES];
        }
            break;
        case 5:
        {
            [dataManager.slideMenu agences:YES];
        }
            break;
        case 6:
        {
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showFAQ" object:nil];
            }else{
                [dataManager.slideMenu faq:YES];
            }
            
        }
            break;
        case 7: //Publications
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showNosPublication" object:nil];
            }else{
                [dataManager.slideMenu nosPublications:YES];
            }
            
        }
            break;
            
        case 8:
        {
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMentionLegales" object:nil];
            }else{
                [dataManager.slideMenu mentionLegale:YES];
            }
            
        }
            break;
        case 9: //applications groupe
        {
        }
            break;
            
        case 10:
        {
            [dataManager.slideMenu nosContacts:YES];
        }
            break;
        case 11: //profil
        {
            
            [dataManager.slideMenu monProfil :YES];
        }
            break;
        case 12: //mesimpayes
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesLignesCredits" object:nil];
            }else{
                [dataManager.slideMenu maLigneCredit :YES];
            }
            
        }
            break;
        case 13:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContrats" object:nil];
            }else{
                [dataManager.slideMenu contrats:YES];
            }
            
        }
            break;
        case 14:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesImpayes" object:nil];
            }else{
                [dataManager.slideMenu monSolde:YES];
            }
            
        }
            break;
        case 15:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesGaranties" object:nil];
            }else{
                [dataManager.slideMenu mesGaranties:YES];
            }
            
        }
            break;
        case 16:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesReclamations" object:nil];
            }else{
                [dataManager.slideMenu mesReclamations:YES];
            }
            
        }
            break;
            
            
        case 17:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesFactures" object:nil];
            }else{
                [dataManager.slideMenu mesFactures:YES];
            }
            
        }
            break;
        case 18:
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMessagerie" object:nil];
            }else{
                [dataManager.slideMenu maMessagerie:YES];
            }
            
        }
            break;
        case 19://
        {
            
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesEcheaciers" object:nil];
            }else{
                [dataManager.slideMenu mesEcheanciers:YES];
            }
            
        }
            break;
        case 20://
        {
            if (dataManager.isDemo)
            {
                alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
                [alert show];
                
                [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
            }
            else
            {
                //
                if(IS_IPAD){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContact" object:nil];
                }else{
                    [dataManager.slideMenu mesContacts:YES];
                }
                
            }
        }
            break;
        case 21://
        {
            /*if (dataManager.isDemo)
             {
             alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
             [alert show];
             
             [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
             }
             else
             {*/
            //
            if(IS_IPAD){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesDocuments" object:nil];
            }else{
                [dataManager.slideMenu mesDocuments:YES];
            }
            
            //}
            break;
        }
        case 60://
        {
            /*if (dataManager.isDemo)
             {
             alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes en mode démo"];
             [alert show];
             
             [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
             }
             else
             {*/
            //
          
                [dataManager.slideMenu mesCommandes:YES];
            
            
            //}
            break;
        }
        case 62://
        {
         
            
            [dataManager.slideMenu mesReglements:YES];
            
            
            //}
            break;
        }
        case 64:
        {
            
            [dataManager.slideMenu mesEngagements:YES];
            
            
            break;
        }
        case 65:
        {
            
            [dataManager.slideMenu mesImmatriculations:YES];
            
            
            break;
        }
            
            
        default:
            break;
    }
}


- (IBAction)demopush:(id)sender
{
    dataManager.isDemo = YES;
    [self showInterface1];
    
}
- (void)hide
{
    [self toggleLeftMenu:nil];
    dataManager.firstTime = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];


    if ([self.selectedchoice isEqualToString:@"CLIENT"])
    {
        if(dataManager.isStayOn)
        {
            
            [standardUserDefaults setBool:YES forKey:@"DataAuthentificationClient"];
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationFournisseur"];
            
        }
    
    }
    else if([self.selectedchoice isEqualToString:@"FOURNISSEUR"])
    {
        if(dataManager.isStayOn)
        {
            
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationClient"];
            [standardUserDefaults setBool:YES forKey:@"DataAuthentificationFournisseur"];
       
        }
    }
    
   
    
    if(dataManager.showdemobutton)
    {
    if([datafound isEqualToString:@"YES"]){
       self.demobtn.hidden=true;
    }else{
        self.demobtn.hidden=NO;
    }
       
        
    }
    else
    {
        self.demobtn.hidden=true;
        
        
    }

    
    [self authentificationSet];
    
  
    if (self.supportTouchID)
    {
        if([self.selectedchoice isEqualToString:@"CLIENT"])
        {
            if  (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]))
            {
                
                [self.viewTouchID setHidden:false];
                
                
                
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] isEqualToString:@"no"])
                {
                    
                    [self vertifyIdentity];
                    // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                    [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                    
                }
                else
                {
                    
                    // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                    [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
                }
                
            }
            
            
            
            else
            {
                [self.viewTouchID setHidden:true];
                [self initDefaultAction];
            }

        
        }
        
        
        if([self.selectedchoice isEqualToString:@"FOURNISSEUR"])
        {
            if  (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"yes"]))
            {
                
                [self.viewTouchID setHidden:false];
                
                
                
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"deconnexionProfil"] isEqualToString:@"no"])
                {
                    
                    [self vertifyIdentity];
                    // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                    [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                    
                }
                else
                {
                    
                    // [self.buttonAuthentification setTitle:@"Vérifier votre Touch ID" forState:UIControlStateNormal];
                    [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
                }
                
            }
            
            
            
            else
            {
                [self.viewTouchID setHidden:true];
                [self initDefaultAction];
            }

            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    else
    {
        [self.viewTouchID setHidden:true];
        [self initDefaultAction];
    }
   
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}


-(void) initDefaultAction{
    
   // [self.viewTouchID setHidden:true];
    [self.buttonAuthentification setTitle:@"ENVOYER" forState:UIControlStateNormal];
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"deconnexionProfil"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.textFieldPassword.text = @"";
    if (![self.switchSave isOn])
        self.textFieldLogin.text = @"";
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - updateView

- (void) gotError:(NetworkManager *)networkManager{
    [super gotError:networkManager];
    [self.activityIndicatorAuthentification stopAnimating];
    self.buttonAuthentification.enabled = YES;
}

- (void) updateViewTag:(int)inputTag {
    self.buttonAuthentification.enabled = YES;
    [self.activityIndicatorAuthentification stopAnimating];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"]) {
        
      
        if (self.supportTouchID) {
           
          
            [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"deconnexionProfil"];
            [[NSUserDefaults standardUserDefaults]  synchronize];
            if([self.selectedchoice isEqualToString:@"CLIENT"])
            {
                dataManager.isClientTouchID=YES;

                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"])
                {
                    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                    
                    [[NSUserDefaults standardUserDefaults]  synchronize];
                    
                    alertView.tag=987;
                    [alertView show];
                    [alertView release];
                }
                
                if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"]) {
                    
                    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                    alertView.tag=987;
                    [alertView show];
                    [alertView release];
                }

            }
            if([self.selectedchoice isEqualToString:@"FOURNISSEUR"])
            {
                dataManager.isClientTouchID=NO;

                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"no"])
                {
                    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                    
                    [[NSUserDefaults standardUserDefaults]  synchronize];
                    
                    alertView.tag=987;
                    [alertView show];
                    [alertView release];
                }
                
                if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"]) {
                    
                    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Touch ID \n" message:@"Voulez-vous associer votre compte MAGHREBAIL avec le Touch ID de cet appareil ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
                    alertView.tag=987;
                    [alertView show];
                    [alertView release];
                }
                
            }
            
        
            
            
            
            
           
            
            
            
            
        }
        
        NSDictionary *response = [[self.response objectForKey:@"response"] objectAtIndex:0];
        
        if(inputTag==7)
        {
            for (id key in response) {
                NSLog(@"key: %@, value: %@ \n", key, [response objectForKey:key]);
            }
            dataManager.authentificationData = [NSDictionary dictionaryWithDictionary:response];
            dataManager.isAuthentified = YES;
            dataManager.isClientContent=YES;
            
            
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            if ([self.switchSave isOn])
            {
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSData *dataAuthentification= [NSKeyedArchiver archivedDataWithRootObject:dataManager.authentificationData];
                [DataManager writeDataIntoCachWith:dataAuthentification andKey:@"authentificationData"];
             //   [[self delegate] showCompteMenu];
                
                if (standardUserDefaults) {
                    [standardUserDefaults setObject:self.textFieldLogin.text forKey:@"USER-EMAIL"];
                    [standardUserDefaults synchronize];
                }
                
            }
            else
            {
                if (standardUserDefaults) {
                    [standardUserDefaults removeObjectForKey:@"USER-EMAIL" ];
                    [standardUserDefaults synchronize];
                }
            }
           // [[NSNotificationCenter defaultCenter] postNotificationName:@"showCompte" object:nil];
      
        }
        
        else if(inputTag==8)
        {
           
            dataManager.authentificationData = [NSDictionary dictionaryWithDictionary:response];
            dataManager.isAuthentified = YES;
            dataManager.isFournisseurContent=YES;
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            if ([self.switchSave isOn])
            {
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSData *dataAuthentification= [NSKeyedArchiver archivedDataWithRootObject:dataManager.authentificationData];
                [DataManager writeDataIntoCachWith:dataAuthentification andKey:@"authentificationData"];
             //   [[self delegate] showCompteMenu];
                
                if (standardUserDefaults) {
                    [standardUserDefaults setObject:self.textFieldLogin.text forKey:@"USER-EMAIL"];
                    [standardUserDefaults synchronize];
                }
                
            }
            else
            {
                if (standardUserDefaults) {
                    [standardUserDefaults removeObjectForKey:@"USER-EMAIL" ];
                    [standardUserDefaults synchronize];
                }
            }
          //  [[NSNotificationCenter defaultCenter] postNotificationName:@"showCompte" object:nil];
            
     
        }
        [[self delegate] showCompteMenu];
        [self toggleLeftMenu:nil];
        dataManager.isAuthentified = YES;
        
        if(dataManager.handleauthentificationconnect)
        {
            [self showInterface1];
            
        }
        else
        {
       // dataManager.authentificationData
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:self.textFieldPassword.text forKey:@"PASS"];
             [[NSUserDefaults standardUserDefaults] synchronize];
            if (IS_IPAD)
            {
                MonProfilViewController_ipad *monProfilViewController_ipad = [[MonProfilViewController_ipad alloc] initWithNibName:@"MonProfilViewController_ipad" bundle:nil];
                
                [self.navigationController pushViewController:monProfilViewController_ipad animated:YES];
            }
            else
            {
                MonProfilViewController_iphone *monProfilViewController_iphone = [[MonProfilViewController_iphone alloc] initWithNibName:@"MonProfilViewController_iphone" bundle:nil];
                [self.navigationController pushViewController:monProfilViewController_iphone animated:YES];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showCompte" object:nil];
            [[self delegate] showCompteMenu];
            [self toggleLeftMenu:nil];
        }
    }
}
             //show profil view For ipad or Iphone
        
      
        
  //
        

    
    
    
    /* else
     {
     dataManager.isAuthentified = NO;
     NSString *message = [header objectForKey:@"message"];
     [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
     }*/

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSData *passData=nil;
    NSString *m_password=@"";
    if (alertView.tag==987) {
        if (buttonIndex == 1) {
            if(dataManager.isClientTouchID) {
                [[NSUserDefaults standardUserDefaults] setObject:self.textFieldLogin.text forKey:@"ClientTouchIDLogin"];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
                [[NSUserDefaults standardUserDefaults] setObject:@"Client" forKey:@"TypeAccount"];
                passData = [DataManager searchKeychainCopyMatching:@"PasswordClient"];
                m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
                [DataManager createKeychainValue:m_password forIdentifier:@"TouchIDPasswordClient"];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:self.textFieldLogin.text forKey:@"FournisseurTouchIDLogin"];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinkedFournisseur"];
                [[NSUserDefaults standardUserDefaults] setObject:@"Fournisseur" forKey:@"TypeAccount"];
                passData = [DataManager searchKeychainCopyMatching:@"PasswordFournisseur"];
                m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
                [DataManager createKeychainValue:m_password forIdentifier:@"TouchIDPasswordFournisseur"];
            }
           
            [passData release];

            [[NSNotificationCenter defaultCenter] postNotificationName:@"InitTouchID" object:nil];
        }
    }
}

- (void)displayMessageView:(NSString *)message {
    [super displayMessageView:message];
    self.buttonAuthentification.enabled = YES;
    [self.activityIndicatorAuthentification stopAnimating];
}

#pragma mark - KeyWord

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSLog(@"keyboardWillShow");
}

- (IBAction)demoPushed:(id)sender
{
    dataManager.isDemo = YES;
    [self showInterface1];
    
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    NSLog(@"keyboardWillHide");
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn");
    [self dismissKeyboard];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing");
}


- (IBAction) dismissKeyboard
{
    [self.textFieldLogin resignFirstResponder];
    [self.textFieldPassword resignFirstResponder];
}

#pragma mark - Action

- (IBAction)AuthentificationPushed:(UIButton *)sender
{
    
    
//    if (self.supportTouchID) {
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) {
//            [self vertifyIdentity];
//        }else{
//            [self identityVerified];
//        }
//    }else{
//        
//        [self identityVerified];
//    }
    
     [self identityVerified];
}


#pragma mark - TouchID Delegate
-(void)errorVerification{
    NSLog(@"Error Verification");
}
-(void)identityVerified{
    NSLog(@"Verification OK");
    
    
    AppDelegate* maindelegate=[[UIApplication sharedApplication] delegate];
    if ([self.textFieldLogin.text length] == 0)
    {
        [self.textFieldLogin becomeFirstResponder];
        return;
    }
    
    if ([self.textFieldPassword.text length] == 0)
    {
        [self.textFieldPassword becomeFirstResponder];
        return;
    }
    
    [self dismissKeyboard];
     [self.activityIndicatorAuthentification startAnimating];
 //   self.buttonAuthentification.enabled = NO;

    //lance authentification
   
   
    
    
    
//    [DataManager createKeychainValue:self.textFieldLogin.text forIdentifier:@"email"];
    
    
   
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:self.textFieldLogin.text forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setObject:self.textFieldPassword.text forKey:@"passwordFace"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     dataManager.isDemo = NO;
    
  
    if (dataManager.isDemo)
    {
        paramDic = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    if ([self.selectedchoice isEqualToString:@"CLIENT"])
        
    {
       

        
        NSString *mobileModel = [ maindelegate getModel];
        float versionOS = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSString *VersionOS = [NSString stringWithFormat:@"%f", versionOS];
       
        [DataManager createKeychainValue:self.textFieldPassword.text forIdentifier:@"PasswordClient"];
     //   [DataManager createKeychainValue:@"1" forIdentifier:@"casprofil"];
       [paramDic setObject:self.textFieldLogin.text forKey:@"email"];
        [paramDic setObject:self.textFieldPassword.text forKey:@"password"];
        [paramDic setObject:VersionOS forKey:@"mobileOs"];
        [paramDic setObject:mobileModel forKey:@"mobileModel"];
        
        
        [self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic andTag:7];

       
    } else if([self.selectedchoice isEqualToString:@"FOURNISSEUR"]) {
        NSString * password = self.textFieldPassword.text;
        NSString *cryptedpassword=[ DataManager hashSHA1:password salt:SALT];
        [DataManager createKeychainValue:cryptedpassword forIdentifier:@"PasswordFournisseur"];

        
        self.typedecompte=@"FOUR";
        NSArray *strings = [[NSArray alloc] initWithObjects:kNEWAUTHENTIFICATION_URL,self.textFieldLogin.text,cryptedpassword,self.typedecompte,@"1",@"1",nil];
        NSString *URLString = [strings componentsJoinedByString:@"/"];
        [self.activityIndicatorAuthentification startAnimating];
        
        [self getRemoteContentAuth:URLString username:self.textFieldLogin.text password:cryptedpassword andTag:8];
    }
}





-(void) authentificationSet{
    [[EHFAuthenticator sharedInstance] setReason:kTouchID_check];
    [[EHFAuthenticator sharedInstance] setUseDefaultFallbackTitle:YES];
    
    NSError * error = nil;
    if (![EHFAuthenticator canAuthenticateWithError:&error]) {
        NSString * authErrorString =kTouchID_CheckSettings;
        self.supportTouchID=NO;
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = kTouchID_No_touchID;
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString =kTouchID_Not_Available;
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = kTouchID_NeedPassCode;
                break;
            default:
                authErrorString =kTouchID_CheckSettings;
                break;
        }
    }
}
-(void) connectExistingCompte{
    [self dismissKeyboard];
    self.buttonAuthentification.enabled = NO;
    dataManager.isDemo = NO;
    [self.activityIndicatorAuthentification startAnimating];
   
    
   // NSString *typeprofile=[[NSUserDefaults standardUserDefaults] objectForKey:@"TypeAccount"];
    

  //  if([typeprofile isKindOfClass:[NSNull class]] || [typeprofile isEqualToString:@"Client"]  )
  //  {
    if([self.selectedchoice isEqualToString:@"CLIENT"])
    {
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"ClientTouchIDLogin"] forKey:@"email"];
        
        NSData *passData = [DataManager searchKeychainCopyMatching:@"TouchIDPasswordClient"];
        NSString *m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
        
        [paramDic setObject:m_password forKey:@"password"];
        [paramDic setObject:@"6" forKey:@"mobileOs"];
        [paramDic setObject:@"IPHONE" forKey:@"mobileModel"];
        
        [self postDataToServer:[NSString stringWithFormat:@"%@", kAUTHENTIFICATION_URL] andParam:paramDic andTag:7];
    
    } else if([self.selectedchoice isEqualToString:@"FOURNISSEUR"]) {
        NSString *email=[[NSUserDefaults standardUserDefaults] objectForKey:@"FournisseurTouchIDLogin"];
        NSData *passData = [DataManager searchKeychainCopyMatching:@"TouchIDPasswordFournisseur"];
        
        NSString *m_password = [[NSString alloc] initWithData:passData encoding:NSUTF8StringEncoding];
         //NSString *cryptedpassword=[ DataManager hashSHA1:m_password salt:SALT];
        NSArray *strings = [[NSArray alloc] initWithObjects:kNEWAUTHENTIFICATION_URL,email,m_password,@"FOUR",@"1",@"1",nil];
        NSString *URLString = [strings componentsJoinedByString:@"/"];
       
        
        [self getRemoteContentAuth:URLString username:email password:m_password andTag:8];
               [passData release];
    
    }
   
}

-(void)vertifyIdentity{
    
    if ([DataManager isJailbroken]) {
        UIAlertView * alertError=[[UIAlertView alloc] initWithTitle:@"Attention (iOS jailbreak)!" message:kJailbreak delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertError show];
        [alertError release];
         [self.viewTouchID setHidden:true];
    }else{
        self.buttonAuthentification.enabled = YES;
        [self resignFirstResponder];
        [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^(){
            [self connectExistingCompte];
        } andFailure:^(LAError errorCode){
            NSString * authErrorString;
            switch (errorCode) {
                case LAErrorSystemCancel:
                    authErrorString = @"System canceled auth request due to app coming to foreground or background.";
                    break;
                case LAErrorAuthenticationFailed:
                    authErrorString = @"Empreinte non reconnue!";
                    break;
                case LAErrorUserCancel:
                    authErrorString = @"Opération annulée!";
                   // [self.viewTouchID setHidden:true];
                    break;
                case LAErrorUserFallback:
                    authErrorString = @"Fallback auth method should be implemented here.";
                    break;
                case LAErrorTouchIDNotEnrolled:
                    authErrorString = @"Aucune emprunt n'est disponible pour le Touch ID";
                    break;
                case LAErrorTouchIDNotAvailable:
                    authErrorString = @"Touch ID n'est pas disponible sur votre appareil.";
                    break;
                case LAErrorPasscodeNotSet:
                    authErrorString = @"Need a passcode set to use Touch ID.";
                    break;
                default:
                    authErrorString = @"Vérifier les paramètres du Touch ID.";
                    break;
            }
            // [self presentAlertControllerWithMessage:authErrorString];
        }];

    }
}
-(void) presentAlertControllerWithMessage:(NSString *) message{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Touch ID" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)switcheSave:(UISwitch *)sender
{
    
}

- (IBAction)showInfoView:(id)sender {
}
- (IBAction)btnShowTouchIDScaner:(UIButton *)sender {
   [self vertifyIdentity];
}


- (void)dealloc {
    [_textFieldPassword release];
    [_textFieldLogin release];
    [delegate release];
    [_activityIndicatorAuthentification release];
    [_buttonAuthentification release];
    [_switchSave release];
    [_scrollPage release];
    [_webView release];
    [_btnInfoSecurite release];
    [_viewTouchID release];
    [_imgBgEmail release];
    [_imgBgPassword release];
    
    [_demobtn release];
    [_typecompte release];
    [_buttonfilter release];
    
    
    [_togglebuttonuser release];
    [_toggle release];
    [_clientlabel release];
    [_fournisseurlabel release];
    [_viewFaceID release];
    [super dealloc];
}
- (void)viewDidUnload {
   
    [self setTextFieldPassword:nil];
    [self setTextFieldLogin:nil];
    [self setActivityIndicatorAuthentification:nil];
    [self setButtonAuthentification:nil];
    [self setSwitchSave:nil];
    [self setScrollPage:nil];
    [self setWebView:nil];
    [self setBtnInfoSecurite:nil];
    [super viewDidUnload];
   
}

@end
