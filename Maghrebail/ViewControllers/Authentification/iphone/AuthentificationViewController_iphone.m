//
//  AuthentificationViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AuthentificationViewController_iphone.h"
#import "InfoViewController_iphone.h" 

@interface AuthentificationViewController_iphone ()

@end

@implementation AuthentificationViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [self initHeaderColor];
       
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}

-(void) initView{
    
    self.imgBgEmail.layer.masksToBounds=YES;
    self.imgBgEmail.layer.cornerRadius=4;
    self.imgBgEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.imgBgEmail.layer.borderWidth=1;
    
    self.imgBgPassword.layer.masksToBounds=YES;
    self.imgBgPassword.layer.cornerRadius=4;
    self.imgBgPassword.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.imgBgPassword.layer.borderWidth=1;
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [super textFieldShouldBeginEditing:textField];
   
    switch (textField.tag)
    {
        case 1:
            self.imgBgEmail.layer.masksToBounds=YES;
            self.imgBgEmail.layer.cornerRadius=4;
            self.imgBgEmail.layer.borderColor=[UIColor orangeColor].CGColor;
            self.imgBgEmail.layer.borderWidth=1;
            break;
        case 2:
            self.imgBgPassword.layer.masksToBounds=YES;
            self.imgBgPassword.layer.cornerRadius=4;
            self.imgBgPassword.layer.borderColor=[UIColor orangeColor].CGColor;
            self.imgBgPassword.layer.borderWidth=1;
        
            
        default:
            break;
    }
   
        return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 1:
            self.imgBgEmail.layer.masksToBounds=YES;
            self.imgBgEmail.layer.cornerRadius=4;
            self.imgBgEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
            self.imgBgEmail.layer.borderWidth=1;
            break;
        case 2:
            self.imgBgPassword.layer.masksToBounds=YES;
            self.imgBgPassword.layer.cornerRadius=4;
            self.imgBgPassword.layer.borderColor=[UIColor lightGrayColor].CGColor;
            self.imgBgPassword.layer.borderWidth=1;
            
        default:
            break;
    }
   
  
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showInfoView:(id)sender
{
	InfoViewController_iphone *aboutShowViewController = [[InfoViewController_iphone alloc] initWithNibName:@"InfoViewController_iphone" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:aboutShowViewController animated:YES];
    [aboutShowViewController release];
}

@end
