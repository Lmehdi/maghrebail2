//
//  SplashViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SplashViewController.h"
#import <QuartzCore/QuartzCore.h> 
#import "AppDelegate.h"
@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
	/*
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2.5];
    [UIView setAnimationDelay:0.0];
	[UIView setAnimationDelegate:self];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
	
	CGRect frame1;
	
		frame1 = CGRectMake(182, 104, 49, 31);
	
	self.viewLogo.frame = frame1;
	
	[UIView commitAnimations];
    */
	
	NSArray *animationArray=[NSArray arrayWithObjects:
							 [UIImage imageNamed:@"Untitled-5_00005.png"],
							 [UIImage imageNamed:@"Untitled-5_00006.png"],
							 [UIImage imageNamed:@"Untitled-5_00007.png"],
							 [UIImage imageNamed:@"Untitled-5_00008.png"],
							 [UIImage imageNamed:@"Untitled-5_00009.png"],
							 [UIImage imageNamed:@"Untitled-5_00010.png"],
							 [UIImage imageNamed:@"Untitled-5_00011.png"],
							 [UIImage imageNamed:@"Untitled-5_00012.png"],
							 [UIImage imageNamed:@"Untitled-5_00013.png"],
							 [UIImage imageNamed:@"Untitled-5_00014.png"],
							 [UIImage imageNamed:@"Untitled-5_00015.png"],
							 [UIImage imageNamed:@"Untitled-5_00016.png"],
							 [UIImage imageNamed:@"Untitled-5_00017.png"],
							 [UIImage imageNamed:@"Untitled-5_00018.png"],
							 [UIImage imageNamed:@"Untitled-5_00019.png"],
							 [UIImage imageNamed:@"Untitled-5_00020.png"],
							 [UIImage imageNamed:@"Untitled-5_00021.png"],
							 [UIImage imageNamed:@"Untitled-5_00022.png"],
							 [UIImage imageNamed:@"Untitled-5_00023.png"],
							 [UIImage imageNamed:@"Untitled-5_00024.png"],
							 [UIImage imageNamed:@"Untitled-5_00025.png"],
							 [UIImage imageNamed:@"Untitled-5_00026.png"],
							 [UIImage imageNamed:@"Untitled-5_00027.png"],
							 [UIImage imageNamed:@"Untitled-5_00028.png"],
							 [UIImage imageNamed:@"Untitled-5_00029.png"],
							 [UIImage imageNamed:@"Untitled-5_00030.png"],
							 [UIImage imageNamed:@"Untitled-5_00031.png"],
							 [UIImage imageNamed:@"Untitled-5_00032.png"],
							 [UIImage imageNamed:@"Untitled-5_00033.png"],
							 [UIImage imageNamed:@"Untitled-5_00034.png"],
							 [UIImage imageNamed:@"Untitled-5_00035.png"],
							 [UIImage imageNamed:@"Untitled-5_00036.png"],
							 [UIImage imageNamed:@"Untitled-5_00037.png"],
							 [UIImage imageNamed:@"Untitled-5_00038.png"],
							 [UIImage imageNamed:@"Untitled-5_00039.png"],
							 [UIImage imageNamed:@"Untitled-5_00040.png"],
							 [UIImage imageNamed:@"Untitled-5_00041.png"],
							 [UIImage imageNamed:@"Untitled-5_00042.png"],
							 [UIImage imageNamed:@"Untitled-5_00043.png"],
							 [UIImage imageNamed:@"Untitled-5_00044.png"],
							 [UIImage imageNamed:@"Untitled-5_00045.png"],
							 [UIImage imageNamed:@"Untitled-5_00046.png"],
							 [UIImage imageNamed:@"Untitled-5_00048.png"],
							 [UIImage imageNamed:@"Untitled-5_00049.png"],
							 [UIImage imageNamed:@"Untitled-5_00050.png"],
                             nil];
    
    NSMutableArray *animationLeasingArray=[NSMutableArray array];
    for (int i=0; i<9; i++) {
        NSString* nameImage=[NSString stringWithFormat:@"Comp1_0000%i.png",i];
        UIImage* m_image=[UIImage imageNamed:nameImage];
        [animationLeasingArray addObject:m_image];
        
    }
    for (int i=10; i<75; i++) {
        NSString* nameImage=[NSString stringWithFormat:@"Comp1_000%i.png",i];
        UIImage* m_image=[UIImage imageNamed:nameImage];
        [animationLeasingArray addObject:m_image];
        
    }
    self.imgLeasing.animationImages=[NSArray arrayWithArray:animationLeasingArray];
    self.imgLeasing.animationDuration= 3;
    self.imgLeasing.animationRepeatCount=1;
    
    [self.imgLeasing startAnimating];
    
	self.imgLogoBleu.animationImages=animationArray;
	self.imgLogoBleu.animationDuration= 3;
    self.imgLogoBleu.animationRepeatCount=1;
	
	[self.imgLogoBleu startAnimating];
    
//    if (IS_IPAD) {
//        [self performSelector:@selector(dimiss) withObject:nil afterDelay:1];
//    }else{
        [self performSelector:@selector(dimiss) withObject:nil afterDelay:3.5];
//    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dimiss{
    
}

- (void)dealloc {
	[_imgLogoBleu release];
	[_viewLogo release];
    [_labelMaghrebail release];
    [_imgLeasing release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setImgLogoBleu:nil];
	[self setViewLogo:nil];
	[super viewDidUnload];
}
@end
