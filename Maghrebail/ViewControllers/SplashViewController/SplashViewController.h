//
//  SplashViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface SplashViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UIImageView *imgLogoBleu;
@property (retain, nonatomic) IBOutlet UIView *viewLogo;
@property (retain, nonatomic) IBOutlet UIImageView *labelMaghrebail;
@property (retain, nonatomic) IBOutlet UIImageView *imgLeasing;

-(void)dimiss;
@end
