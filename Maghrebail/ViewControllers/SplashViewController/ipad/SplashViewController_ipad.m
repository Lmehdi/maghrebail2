//
//  SplashViewController_ipadViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/6/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SplashViewController_ipad.h"
#import "AppDelegate.h"
@interface SplashViewController_ipad ()

@end

@implementation SplashViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)dimiss{
    // [self dismissModalViewControllerAnimated:YES];
    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
    [mainDelegate showNewSideMenu];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
