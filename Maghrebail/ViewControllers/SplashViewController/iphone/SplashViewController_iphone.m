//
//  SplashViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "SplashViewController_iphone.h"
#import "AppDelegate.h"
@interface SplashViewController_iphone ()

@end

@implementation SplashViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad]; 
}
-(void)dimiss{
    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];

    if(mainDelegate.window.rootViewController == self) {
        [mainDelegate sideMenue];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
