//
//  AdvancedPushSDKViewController.m
//  Mobiblanc
//
//  Created by Anas Bouzoubaa on 27/12/12.
//
//

#import "AdvancedPushSDKViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLReader.h"

@interface AdvancedPushSDKViewController ()

@end

@implementation AdvancedPushSDKViewController
@synthesize myImage, myMessage, myTitle, myIcon, myButton1, myButton2, activityIndicator, lien, type, posAction, lastNotif, locationManager, token;

#pragma mark - init method
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil notif:(NSDictionary *)lastNotification token:(NSString *)deviceToken
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"[AdvancedPushSDKViewController initWithNibName]");
        
        self.lastNotif = [[NSDictionary alloc] initWithDictionary:lastNotification];
        self.token = deviceToken;
        NSLog(@"Remote notif: %@", [lastNotif description]);
        
        NSString *xmlFile = [lastNotif objectForKey:@"xml"];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:
                                   [NSURL URLWithString:[NSString stringWithFormat:@"http://slice.mobiblanc.com/scripte_php/xmlPush/%@", xmlFile]]];
        NSLog(@"XML File: %@", [request.url description]);
        [request setTimeOutSeconds:25];
        [request setDelegate:self];
        [request setTag:1];
        [request startAsynchronous];
        
    }
    return self;
}


#pragma mark - ASIHTTP Request callbacks
- (void)requestFinished:(ASIHTTPRequest *)request
{
    // Request tags
    // 1- XML File
    // 2- Icon 
    // 3- Image
    // 4- AddBackup
    // ------------
    NSLog(@"REQUEST FINISHED, TAG = %i", request.tag);
    
    // Use when fetching text data
    NSString *responseString = [request responseString];
    NSLog(@"responseString: %@", responseString);
    
    // Use when fetching binary data
    NSData *responseData = [request responseData];
    
    if (request.tag == 1) { // Request for XML File download
        NSLog(@"request for XML download, http status code %i", request.responseStatusCode);
        if (request.responseStatusCode == 404) {
            NSLog(@"Impossible to download XML File !");
            [self dismissModalViewControllerAnimated:YES];
            self.view = nil;
        } else {
            // PARSE
            NSError *parseError = nil;
            XMLDictionary = [XMLReader dictionaryForXMLData:responseData error:&parseError];
            
            // TRIM & PRINT DICTIONARY
            XMLDictionary = [XMLDictionary objectForKey:@"root"];
            NSLog(@"\n\nXMLDictionary after trimming root: \n%@\n", [XMLDictionary description]);
            
            [self setUpContent];
        }
        
    } else if (request.tag == 2) { // ICON
        NSLog(@"request for icon, http status code %i", request.responseStatusCode);
        if (request.responseStatusCode == 404) {
            myIcon.image = [UIImage imageNamed:@"bw@2x.png"];
        } else {
            myIcon.image = [UIImage imageWithData:responseData];
            if (myIcon.image == nil) { // Icon still empty
                myIcon.image = [UIImage imageNamed:@"bw@2x.png"];
            }
        }
        
    } else if (request.tag == 3) { // IMAGE
        NSLog(@"request for image, http status code %i", request.responseStatusCode);
        [activityIndicator stopAnimating];
        [activityIndicator setHidden:YES];
        if (request.responseStatusCode == 404) {
            myImage.image = [UIImage imageNamed:@"image@2x.png"];
        } else {
            myImage.image = [UIImage imageWithData:responseData];
            if (myImage.image == nil) { // Image still empty
                myImage.image = [UIImage imageNamed:@"image@2x.png"];
            }
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"REQUEST FAILED, TAG = %i", request.tag);
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    
    if (request.tag == 1) {
        NSLog(@"Impossible to download XML File !");
        [self dismissModalViewControllerAnimated:YES];
        self.view = nil;
    }
    
    else if (request.tag == 2) {
       myIcon.image = [UIImage imageNamed:@"bw@2x.png"];}
    
    else if (request.tag == 3) {
        [activityIndicator stopAnimating];
        [activityIndicator setHidden:YES];
        myImage.image = [UIImage imageNamed:@"image@2x.png"];}
    
}

#pragma mark - View & Memory Management
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"PushSDK viewDidLoad");
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"View DidAppear");

    // Géoloc
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.lien release];
    [self.type release];
    [self.posAction release];
    [self.myButton1 release];
    [self.myButton2 release];
    [self.lastNotif release];
    [self.locationManager release];
    [self.token release];
    [super dealloc];
}

#pragma mark - Setup Content
- (void)setUpContent
{
    NSLog(@"setUpContent");
    
    // IMAGE
    NSString *lienImage = [[[XMLDictionary objectForKey:@"image"] objectForKey:@"text"] description];
    NSLog(@"lienImage: %@", lienImage);
    ASIHTTPRequest *requestImage = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:lienImage]];
    [requestImage setTag:3]; [activityIndicator setHidden:NO]; [activityIndicator startAnimating];
    [myImage setImage:[UIImage imageNamed:@"image@2x.png"]];
    [requestImage setTimeOutSeconds:30];
    [requestImage setDelegate:self];
    [requestImage startAsynchronous];
    
// ------------------------- BUTTONS
    
    [myButton1 setHidden:YES];
    [myButton2 setHidden:YES];
    NSString *action = [[[[XMLDictionary objectForKey:@"action"] objectForKey:@"libelle"] objectForKey:@"text"] description];
            NSLog(@"action in xml: %@", action);
    NSString *close = [[[[XMLDictionary objectForKey:@"close"] objectForKey:@"libelle"] objectForKey:@"text"] description];
            NSLog(@"close in xml: %@", close);
    [myButton1 setTitle:action forState:UIControlStateNormal];
            NSLog(@"action in button: %@", [[myButton1 titleLabel] text]);
    
    [myButton2 setTitle:close forState:UIControlStateNormal];
            NSLog(@"close in button: %@", [[myButton2 titleLabel] text]);
    
    [myButton1.titleLabel setFont:[UIFont fontWithName:@"helr45w.ttf" size:16.0]];
    [myButton2.titleLabel setFont:[UIFont fontWithName:@"helr45w.ttf" size:16.0]];
    
// ------------------------- BUTTONS
    
    // ICON
    NSString *lienIcon = [[[XMLDictionary objectForKey:@"icon"] objectForKey:@"text"] description];
    NSLog(@"lienIcon: %@", lienIcon);
    ASIHTTPRequest *requestIcon = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:lienIcon]];
    [requestIcon setTag:2];
    [requestIcon setDelegate:self];
    [requestIcon startAsynchronous];
    
    // LIEN
    self.lien = [[[[XMLDictionary objectForKey:@"action"] objectForKey:@"lien"] objectForKey:@"text"] description];
    NSLog(@"lien: %@", self.lien);
    
    // CONTENU TEXT
    myMessage.text = [[[XMLDictionary objectForKey:@"contenu"] objectForKey:@"text"] description];
    [myMessage setFont:[UIFont fontWithName:@"helr45w.ttf" size:25.0]];
    
    // TITRE
    myTitle.text = [[[XMLDictionary objectForKey:@"titre"] objectForKey:@"text"] description];
    [myTitle setFont:[UIFont fontWithName:@"Helvetica_Bold" size:18.0]];
    
    // Avoiding EMPTY parameters
    if (myImage.image == nil) {
        myImage.image = [UIImage imageNamed:@"image@2x.png"];}
    
    if (myIcon.image == nil) {
        myIcon.image = [UIImage imageNamed:@"bw@2x.png"];}
    
    if (myTitle.text == nil) {
        myTitle.text = @"Buzz And Win";}
    
    if ([myMessage.text isEqualToString:@""]) {
        myMessage.text = @"Aucun texte trouvé.";}
    
    self.type = [[[[XMLDictionary objectForKey:@"action"] objectForKey:@"type"] objectForKey:@"text"] description];
    NSLog(@"type of action: %@", self.type);
    

    // Buttons positions
    CGPoint centerLeft = CGPointMake(85.5, 382);
    CGPoint centerRight = CGPointMake(233.5, 382);
    CGPoint center = CGPointMake(centerLeft.x+73, centerLeft.y);
    
    posAction = [[[[XMLDictionary objectForKey:@"action"] objectForKey:@"position"] objectForKey:@"text"] description];
    NSLog(@"posAction: %@", self.posAction);
    
    if ([self.lien isEqualToString:@"NULL"]) {
        NSLog(@"lien is NULL");
        
        [self.myButton2 setBackgroundImage:[UIImage imageNamed:@"accepter@2x"] forState:UIControlStateNormal];
        [self.myButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.myButton2 setCenter:center];
        [self.myButton2 setHidden:NO];
        
    } else if ([self.posAction isEqualToString:@"2"]) {
        NSLog(@"posAction is 2");

        [myButton1 setHidden:NO];
        [myButton2 setHidden:NO];
        
        [myButton1 setCenter:centerRight];
        [myButton2 setCenter:centerLeft];
    }
    
    // Notification center
    [[NSNotificationCenter defaultCenter] postNotificationName:@"popupSetup" object:self];

}

#pragma mark - IB Actions
- (IBAction)closeView:(id)sender
{
    NSLog(@"[AdvancedPushSDKViewController closeView]");
    [self dismissModalViewControllerAnimated:YES];
    self.view = nil;
}

- (IBAction)linkButton:(id)sender
{
    NSLog(@"[AdvancedPushSDKViewController linkButton]");
    /* Handling type of action ------------
      1- URL link: Modal web view
      2- Position: (google/apple) Maps app
    -------------------------------------*/
    
    // URL
    if ([self.type isEqualToString:@"1"]) {
        NSLog(@"linkButton's lien: %@", [self.lien description]);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.lien]];
    }
    // Maps
    else {
        NSLog(@"Opening maps");
        [[UIApplication sharedApplication]
         openURL:[NSURL URLWithString:
                  [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", self.lien]]];
    }
}

#pragma mark - Géolocation
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"locationManager didUpdateToLocation");
    
    [self.locationManager stopUpdatingLocation];
    
    NSString *longitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    NSString *latitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    NSLog(@"Longitude : %@", longitude);
    NSLog(@"Latitude : %@", latitude);
    
    [self addBackup:newLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.locationManager stopUpdatingLocation];
    NSLog(@"Error in location manager: %@", [error description]);
    
    [self addBackup:nil];
}

#pragma mark - Sending push report & geolocation
- (void)addBackup:(CLLocation *)location
{
    NSLog(@"\n\n[AdvancedPushSDK addBackup]");
    NSLog(@"Location: {%f;%f}\nNotif Payload: %@\n\n", location.coordinate.latitude, location.coordinate.longitude, [lastNotif description]);
    
    [locationManager stopUpdatingLocation];
    
    NSLog(@"lastNotif: %@", [self.lastNotif description]);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd-HH:mm:ss"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    NSLog(@"Date: %@", currentDate);
    
    NSString *alertText =   [[self.lastNotif objectForKey:@"alert"] description];
    NSLog(@"alert: %@", alertText);
    
    NSString *appID =       [[self.lastNotif objectForKey:@"idApp"] description];
    NSLog(@"idApp: %@", appID);
    
    NSString *id_envoi =    [[self.lastNotif objectForKey:@"id_envoi"] description];
    NSLog(@"id_envoi: %@", id_envoi);

    NSLog(@"Device ID= %@", self.token);
    
    NSString *longitude = [[NSString alloc] initWithString:@"0"];
    NSString *latitude  = [[NSString alloc] initWithString:@"0"];
    
    if (location != nil) {
        longitude = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    }
    
    NSString *feedURLString = [NSString stringWithFormat:@"http://slice.mobiblanc.com/scripte_php/addbackup.php?id_App=%@&id_envoi=%@&id_device=%@&dateLecture=%@&longitude=%@&latitude=%@", appID, id_envoi, self.token, currentDate, longitude, latitude];
    NSLog(@"Addbackup URL: %@", feedURLString);
    
    ASIHTTPRequest *dataRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:feedURLString]];
    [dataRequest setDelegate:self];
    [dataRequest setTag:4];
    [dataRequest startAsynchronous];
    
}



@end

