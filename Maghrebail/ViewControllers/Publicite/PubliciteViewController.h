//
//  PubliciteViewController.h
//  Maghrebail
//
//  Created by MAC on 02/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteImageView.h"
#import "BaseViewController.h"

@interface PubliciteViewController : BaseViewController

@property (retain, nonatomic) IBOutlet RemoteImageView *imgViewPub;
- (void)loadPub;
-(void)dismiss;
- (IBAction)showAd:(id)sender;
- (IBAction)closeAd:(id)sender;
@end
