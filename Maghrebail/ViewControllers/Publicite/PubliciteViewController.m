//
//  PubliciteViewController.m
//  Maghrebail
//
//  Created by MAC on 02/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "PubliciteViewController.h"

@interface PubliciteViewController ()

@end

@implementation PubliciteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		shouldAutorate = NO;
		shouldAddHeader = NO;
    }
    return self;
}

- (void)loadPub
{
	NetworkManager * networkManager;
	if (IS_IPAD)
	{
		networkManager = [[NetworkManager alloc]initWithURL:[BANNIERE_URL_IPAD stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding ]];
	}
	else
	{
		networkManager = [[NetworkManager alloc]initWithURL:[BANNIERE_URL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding ]];
	}
	 
	networkManager.delegate = self;
	[networkManager doGet];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[Flurry logEvent:@"Bannière"];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if (![self.imgViewPub.imageCache hasImageWithKey:[self.imgViewPub.url lastPathComponent]])
	{
		[self dismiss];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_imgViewPub release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setImgViewPub:nil];
    [super viewDidUnload];
}
 
 
-(void)gotError:(NetworkManager *)networkManager{
    [self dismiss];
}

-(void)updateView{
    
    if (self.response!= nil )
    {
        NSArray *arraySlider = [[[self.response objectForKey:@"Zones"] objectAtIndex:0] objectForKey:@"listBannieres"];
        NSData *dataSlider = [NSKeyedArchiver archivedDataWithRootObject:arraySlider];
        [DataManager writeDataIntoCachWith:dataSlider andKey:@"dataSlider"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSlider" object:self];
    }
}

-(void)dismiss{ 
    [self.view removeFromSuperview];
}

- (IBAction)showAd:(id)sender {
	NSURL *url = [NSURL URLWithString:[[[self.response objectForKey:@"Interstitielle"] objectForKey:@"data" ] objectForKey:@"url"]];
	
	if (![[UIApplication sharedApplication] openURL:url])
		NSLog(@"%@%@",@"Failed to open url:",[url description]);
}


- (IBAction)closeAd:(id)sender
{ 
	[self.view removeFromSuperview];
}
@end
