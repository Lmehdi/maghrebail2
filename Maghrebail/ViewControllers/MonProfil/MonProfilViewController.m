//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MonProfilViewController.h"

@interface MonProfilViewController ()

@end

@implementation MonProfilViewController
@synthesize supportTouchID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.supportTouchID=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MON PROFIL";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    self.btnTouchIDOut.layer.masksToBounds=YES;
    self.btnTouchIDOut.layer.cornerRadius=3;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initBtnAssocier) name:@"InitTouchID" object:nil];
    if(dataManager.isFournisseurContent) {
        self.titledomicilation.text = @"PATENTE";
        self.titleclientdepuis.text = @"ICE";
        self.ficheclientlabel.text  = @"FICHE FOURNISSEUR";
    }
    /*
     if (dataManager.profilData)
     {
     //remplir Fiche client and Contact
     [self remplirScrollContact:[dataManager.profilData objectForKey:@"contacts"]];
     [self remplirLabelProfil:[dataManager.profilData objectForKey:@"profil"]];
     }
     else
     {
     */
    
    // Touch ID
    
    [self initBtnAssocier];
    [self authentificationSet];
    //
    self.scrollProfil.alpha = 0;
    NSMutableDictionary *email ;
    
    if (dataManager.isDemo) {
        [self.btnTouchIDOut setHidden:true];
        [self.labelTouchID setHidden:true];
        email = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"email"];
    } else {
        [self.btnTouchIDOut setHidden:false];
        [self.labelTouchID setHidden:false];
        email = [NSMutableDictionary dictionaryWithObject:[dataManager.authentificationData objectForKey:@"EMAIL"] forKey:@"email"];
    }
    
    [self.btnTouchIDOut setHidden:true];
    [self.labelTouchID setHidden:true];
    
    if(dataManager.isClientContent || dataManager.isDemo) {
        [self postDataToServer:kPROFIL_URL andParam:email];

    }
    if(dataManager.isFournisseurContent) {
        
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
         NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_profil/%@",emailencoded,password,[dataManager.authentificationData objectForKey:@"EMAIL"]];
        
        [self getRemoteContentfournisseur:url];
    
    }
    
    if (dataManager.firstTime && self.isFromDash) {
        [self showSideMenu];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(hide) userInfo:nil repeats:NO];
        
    }
    
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            if(IS_IPHONE_5){
                headerView.btnRightHeader.frame=CGRectMake(280, headerView.btnRightHeader.frame.origin.y, headerView.btnRightHeader.frame.size.width, headerView.btnRightHeader.frame.size.height);
                
            }
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            if(IS_IPHONE_5){
                headerView.btnRightHeader.frame=CGRectMake(280, headerView.btnRightHeader.frame.origin.y, headerView.btnRightHeader.frame.size.width, headerView.btnRightHeader.frame.size.height);
                
            }
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
}

-(void) authentificationSet{
    [[EHFAuthenticator sharedInstance] setReason:@"Vérifier votre Touch ID"];
    [[EHFAuthenticator sharedInstance] setUseDefaultFallbackTitle:YES];
    NSError * error = nil;
    if (![EHFAuthenticator canAuthenticateWithError:&error]) {
        NSString * authErrorString = @"Check your Touch ID Settings.";
        self.supportTouchID=NO;
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = kTouchID_No_touchID;
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString =kTouchID_Not_Available;
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = kTouchID_NeedPassCode;
                break;
            default:
                authErrorString =kTouchID_CheckSettings;
                break;
        }    }
}
- (void)hide
{
    [self toggleLeftMenu:nil];
    dataManager.firstTime =NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_labelNature release];
    [_labelRc release];
    [_labelForme release];
    [_labelAdresse release];
    [_labelCapital release];
    [_labelDomiciliation release];
    [_labelRib release];
    [_labelDepuis release];
    [_viewFicheClient release];
    [_scrollProfil release];
    [_scrollContact release];
    [_pageControlContacts release];
    [_labelTitle release];
    [_labelTouchID release];
    [_btnTouchIDOut release];
    [_titledomicilation release];
    [_titleclientdepuis release];
    [_ficheclientlabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelNature:nil];
    [self setLabelRc:nil];
    [self setLabelForme:nil];
    [self setLabelAdresse:nil];
    [self setLabelCapital:nil];
    [self setLabelDomiciliation:nil];
    [self setLabelRib:nil];
    [self setLabelDepuis:nil];
    [self setViewFicheClient:nil];
    [self setScrollProfil:nil];
    [self setScrollContact:nil];
    [self setPageControlContacts:nil];
    [self setLabelTitle:nil];
    [super viewDidUnload];
}

#pragma mark - updateView

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSDictionary *reponse = [self.response objectForKey:@"response"];
        if(dataManager.isClientContent || dataManager.isDemo)
        {
        
            dataManager.profilData = [NSDictionary dictionaryWithDictionary:reponse];
            NSDictionary *profil = [reponse objectForKey:@"profil"];
            NSArray *contactArray = [reponse objectForKey:@"contacts"];
            //remplir Fiche client and Contact
            [self remplirScrollContact:contactArray];
            [self remplirLabelProfil:profil];
            
            self.labelTitle.text = [NSString stringWithFormat:@"%@ ",[profil objectForKey:@"RS"]];
        
        }
        if(dataManager.isFournisseurContent)
        {
        
            dataManager.profilData = [NSDictionary dictionaryWithDictionary:reponse];
            NSArray *profil = [reponse objectForKey:@"profil"];
            NSArray *contactArray = [reponse objectForKey:@"contacts"];
            //remplir Fiche client and Contact
            [self remplirScrollContact:contactArray];
            [self remplirLabelProfilFournisseur:profil];
            
            self.labelTitle.text = [NSString stringWithFormat:@"%@ ",[[profil objectAtIndex:0] objectForKey:@"RS"]];

            
            
        
        
        }
      
        
    }
    /* else
     {
     NSString *message = [header objectForKey:@"message"];
     [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
     }*/
}

- (void)remplirLabelProfilFournisseur:(NSArray *)ficheFournisseur
{
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"NATURE"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"NATURE"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelNature.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"NATURE"];
        
    }
    else
    {
        
        self.labelNature.text = @"";
    
    }
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"RC"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"RC"] isKindOfClass:[NSNull class]]))
    {
        
      self.labelRc.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"RC"];
        
    }
    else
    {
        
        self.labelRc.text = @"";
        
    }
    
    if([[ficheFournisseur objectAtIndex:0] objectForKey:@"FORM_JUR"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"FORM_JUR"] isKindOfClass:[NSNull class]])
    {
        
        self.labelForme.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"FORM_JUR"];
        
    }
    else
    {
        
        self.labelForme.text = @"";
        
    }
    
    
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"ADRESSE"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"ADRESSE"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelAdresse.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"ADRESSE"];
        
    }
    else
    {
        
        self.labelAdresse.text = @"";
        
    }
    
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"CAPITAL"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"CAPITAL"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelCapital.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"CAPITAL"];
        
    }
    else
    {
        
        self.labelCapital.text = @"";
        
    }
    
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"RIB"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"RIB"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelRib.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"RIB"];
        
    }
    else
    {
        
        self.labelRib.text =@"";
        
    }
    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"PATENTE"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"PATENTE"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelDomiciliation.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"PATENTE"];
    }
    else
    {
        
        self.labelDomiciliation.text = @"";
    }

    if(!([[ficheFournisseur objectAtIndex:0] objectForKey:@"ICE"]==nil  || [[[ficheFournisseur objectAtIndex:0] objectForKey:@"ICE"] isKindOfClass:[NSNull class]]))
    {
        
        self.labelDepuis.text = [[ficheFournisseur objectAtIndex:0] objectForKey:@"ICE"];
    }
    else
    {
        
        self.labelDepuis.text = @"";
    }

    
    
    
    
    
    





    [UIView beginAnimations:@"ScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelay:0];
    self.scrollProfil.alpha = 1;
    [UIView commitAnimations];
    self.scrollProfil.contentSize = CGSizeMake(self.scrollContact.frame.size.width,  self.viewFicheClient.frame.size.height + self.scrollContact.frame.size.height + 100);

}



- (void)remplirLabelProfil:(NSDictionary *)ficheClient
{
  
        self.labelNature.text = [ficheClient objectForKey:@"NATURE"];
        self.labelRc.text = [ficheClient objectForKey:@"RC"];
        self.labelForme.text = [ficheClient objectForKey:@"FORM_JUR"];
        self.labelAdresse.text = [ficheClient objectForKey:@"ADRESSE"];
        self.labelCapital.text = [ficheClient objectForKey:@"CAPITAL"];
        self.labelDomiciliation.text = [ficheClient objectForKey:@"BANCAIRE"];
        self.labelRib.text = [ficheClient objectForKey:@"RIB"];
        self.labelDepuis.text = [ficheClient objectForKey:@"DEPUIS"];
    

    
    //fiche animation
    [UIView beginAnimations:@"ScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelay:0];
    self.scrollProfil.alpha = 1;
    [UIView commitAnimations];
    self.scrollProfil.contentSize = CGSizeMake(self.scrollContact.frame.size.width,  self.viewFicheClient.frame.size.height + self.scrollContact.frame.size.height + 100);
}






-(void)remplirScrollContact:(NSArray *)contactArray
{
}

-(void) initBtnAssocier{
    
    // Associé le Touch ID
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"]) {
        self.labelTouchID.text=@"Associer votre Touch ID";
        return;
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"]) {
        self.labelTouchID.text=@"Associer votre Touch ID";
        return;
    }
    // Dissocié le Touch ID
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) {
        self.labelTouchID.text=@"Dissocier votre Touch ID";
        return;
    }
    
}
- (IBAction)btnDessocie:(UIButton *)sender {
    
    // Associé le Touch ID
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
        self.labelTouchID.text=@"Dissocier votre Touch ID";
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"" message:kCompteAssocie delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return;
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"]) {
        self.labelTouchID.text=@"Dissocier votre Touch ID";
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:kCompteAssocie delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return;
    }
    
    // Dissocié le Touch ID
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) {
        self.labelTouchID.text=@"Associer votre Touch ID";
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"TouchIDLinked"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDLogin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDPassword"];
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:kCompteDissocie delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return;
    }
}

@end
