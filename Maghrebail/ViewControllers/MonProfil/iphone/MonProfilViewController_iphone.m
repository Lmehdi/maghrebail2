//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MonProfilViewController_iphone.h"
#import "Contact_iphone.h"

@interface MonProfilViewController_iphone ()

@end

@implementation MonProfilViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - updateView

-(void)remplirScrollContact:(NSArray *)contactArray
{
    if(!contactArray || ![contactArray isKindOfClass:[NSArray class]])
        contactArray        =   [NSMutableArray array];
    
    self.pageControlContacts.numberOfPages = [contactArray count];
    self.pageControlContacts.currentPage = 0;
    int i = 0;
    
    for (NSDictionary *contactDic in contactArray)
    {
        NSString *viewNibName = @"Contact_iphone_p";

        if (isLandscape)
        {
            viewNibName = @"Contact_iphone_l";
        }
        
        Contact_iphone * contact_iphone =   [[[NSBundle mainBundle] loadNibNamed:viewNibName owner:self options:nil] objectAtIndex:0];
        
        if([contactDic objectForKey:@"TELEPHONE"]==nil  || [[contactDic objectForKey:@"TELEPHONE"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelTelephone.text = @"";
            
        }
        else
        {
            contact_iphone.labelTelephone.text = [contactDic objectForKey:@"TELEPHONE"];
        }
        
        
        
        if([contactDic objectForKey:@"REPRESENTANT"]==nil  || [[contactDic objectForKey:@"REPRESENTANT"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelNom.text = @"";

        }
        else
        {
            contact_iphone.labelNom.text = [contactDic objectForKey:@"REPRESENTANT"];
        }
        
        
        
        if([contactDic objectForKey:@"FAX"]==nil  || [[contactDic objectForKey:@"FAX"] isKindOfClass:[NSNull class]])
        {
        
         contact_iphone.labelFax.text = @"";
        
        }
        else
        {
              contact_iphone.labelFax.text = [contactDic objectForKey:@"FAX"];
        }
        
        if([contactDic objectForKey:@"EMAIL"]==nil  || [[contactDic objectForKey:@"EMAIL"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelEmail.text = @"";
            
        }
        else
        {
            contact_iphone.labelEmail.text = [contactDic objectForKey:@"EMAIL"];
        }
      
        [self.scrollContact addSubview:contact_iphone];
        contact_iphone.frame = CGRectMake(self.scrollContact.frame.size.width * i, 0, self.scrollContact.frame.size.width, self.scrollContact.frame.size.height);
        i++;
    }
    self.scrollContact.contentSize = CGSizeMake(self.scrollContact.frame.size.width * i, self.scrollContact.frame.size.height);
}


#pragma mark --
#pragma mark -- Scroll Delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControlContacts.currentPage = page;
}

@end
