//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "EHFAuthenticator.h"

@interface MonProfilViewController : BaseViewController<UIScrollViewDelegate>
{
    
}
@property(assign) BOOL supportTouchID;
- (IBAction)btnDessocie:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnTouchIDOut;

@property (retain, nonatomic) IBOutlet UILabel *labelTouchID;
@property (retain, nonatomic) IBOutlet UILabel *ficheclientlabel;

@property (retain, nonatomic) IBOutlet UILabel *labelNature;
@property (retain, nonatomic) IBOutlet UILabel *labelRc;
@property (retain, nonatomic) IBOutlet UILabel *labelForme;
@property (retain, nonatomic) IBOutlet UILabel *labelAdresse;
@property (retain, nonatomic) IBOutlet UILabel *labelCapital;
@property (retain, nonatomic) IBOutlet UILabel *labelDomiciliation;
@property (retain, nonatomic) IBOutlet UILabel *labelRib;
@property (retain, nonatomic) IBOutlet UILabel *labelDepuis;
@property (retain, nonatomic) IBOutlet UIView *viewFicheClient;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollProfil;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollContact;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControlContacts;
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *titledomicilation;
@property (retain, nonatomic) IBOutlet UILabel *titleclientdepuis;

-(void) initHeaderColor;
- (void)remplirLabelProfil:(NSDictionary *)ficheClient;
- (void)remplirScrollContact:(NSArray *)contactArray;
- (void)remplirLabelProfilFournisseur:(NSArray *)ficheFournisseur;

@end
