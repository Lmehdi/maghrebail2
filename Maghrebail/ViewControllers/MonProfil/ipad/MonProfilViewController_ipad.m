//
//  MonProfilViewController.m
//  Maghrebail NQB24DNDHDNCQCVZB93Z iPAD // TJ2QTZD89VFQTK4SS334
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MonProfilViewController_ipad.h"
#import "Contact_ipad.h"

@interface MonProfilViewController_ipad ()

@end

@implementation MonProfilViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - updateView

-(void)remplirScrollContact:(NSArray *)contactArray
{
//    self.pageControlContacts.numberOfPages = [contactArray count];
//    self.pageControlContacts.currentPage = 0;
//    int i = 0;
//    for (NSDictionary *contactDic in contactArray)
//    {
//        NSString *viewNibName = @"Contact_ipad";
//        if (isLandscape)
//        {
//            viewNibName = @"Contact_ipad";
//        }
//        Contact_iphone * contact_iphone =   [[[NSBundle mainBundle] loadNibNamed:viewNibName owner:self options:nil] objectAtIndex:0];
//        contact_iphone.labelNom.text = [contactDic objectForKey:@"REPRESENTANT"];
//        contact_iphone.labelTelephone.text = [contactDic objectForKey:@"TELEPHONE"];
//        contact_iphone.labelFax.text = [contactDic objectForKey:@"FAX"];
//        contact_iphone.labelEmail.text = [contactDic objectForKey:@"EMAIL"];
//        [self.scrollContact addSubview:contact_iphone];
//        contact_iphone.frame = CGRectMake(contact_iphone.frame.size.width * i, 0, contact_iphone.frame.size.width, contact_iphone.frame.size.height);
//        i++;
//    }
//    self.scrollContact.contentSize = CGSizeMake(self.scrollContact.frame.size.width * i, self.scrollContact.frame.size.height);
    if(!contactArray || ![contactArray isKindOfClass:[NSArray class]])
        contactArray        =   [NSMutableArray array];
    
    self.pageControlContacts.numberOfPages = [contactArray count];
    self.pageControlContacts.currentPage = 0;
    int i = 0;
    
    for (NSDictionary *contactDic in contactArray)
    {
        NSString *viewNibName = @"Contact_ipad";
        
//        if (isLandscape)
//        {
//            viewNibName = @"Contact_ipad";
//        }
        
        Contact * contact_iphone =   [[[NSBundle mainBundle] loadNibNamed:viewNibName owner:self options:nil] objectAtIndex:0];
        
        if([contactDic objectForKey:@"TELEPHONE"]==nil  || [[contactDic objectForKey:@"TELEPHONE"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelTelephone.text = @"";
            
        }
        else
        {
            contact_iphone.labelTelephone.text = [contactDic objectForKey:@"TELEPHONE"];
        }
        
        
        
        if([contactDic objectForKey:@"REPRESENTANT"]==nil  || [[contactDic objectForKey:@"REPRESENTANT"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelNom.text = @"";
            
        }
        else
        {
            contact_iphone.labelNom.text = [contactDic objectForKey:@"REPRESENTANT"];
        }
        
        
        
        if([contactDic objectForKey:@"FAX"]==nil  || [[contactDic objectForKey:@"FAX"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelFax.text = @"";
            
        }
        else
        {
            contact_iphone.labelFax.text = [contactDic objectForKey:@"FAX"];
        }
        
        if([contactDic objectForKey:@"EMAIL"]==nil  || [[contactDic objectForKey:@"EMAIL"] isKindOfClass:[NSNull class]])
        {
            
            contact_iphone.labelEmail.text = @"";
            
        }
        else
        {
            contact_iphone.labelEmail.text = [contactDic objectForKey:@"EMAIL"];
        }
        
        [self.scrollContact addSubview:contact_iphone];
        contact_iphone.frame = CGRectMake(self.scrollContact.frame.size.width * i, 0, self.scrollContact.frame.size.width, self.scrollContact.frame.size.height);
        i++;
    }
    self.scrollContact.contentSize = CGSizeMake(self.scrollContact.frame.size.width * i, self.scrollContact.frame.size.height);
}

#pragma mark - Scroll Delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControlContacts.currentPage = page;
}

@end
