//
//  Popup.h
//  
//
//  Created by Bendnaiba on 27/11/12.
//  Copyright (c) 2012 Bendnaiba. All rights reserved.
//
#ifndef Contact_h
#define Contact_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Contact : UIView
{
    
}

@property (retain, nonatomic) IBOutlet UILabel *labelNom;
@property (retain, nonatomic) IBOutlet UILabel *labelTelephone;
@property (retain, nonatomic) IBOutlet UILabel *labelFax;
@property (retain, nonatomic) IBOutlet UILabel *labelEmail;


@end

#endif
