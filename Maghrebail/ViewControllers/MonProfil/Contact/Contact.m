//
//  Menu.m
//  
//
//  Created by AHDIDOU on 27/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#import "Contact.h"
#import "DataManager.h"

@implementation Contact

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)dealloc
{
    [_labelNom release];
    [_labelTelephone release];
    [_labelFax release];
    [_labelEmail release];
    [super dealloc];
}


@end
