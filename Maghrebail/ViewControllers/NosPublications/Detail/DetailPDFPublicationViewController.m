//
//  DetailPDFPublicationViewController.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailPDFPublicationViewController.h"
#import "Reachability.h"

@interface DetailPDFPublicationViewController ()

@end

@implementation DetailPDFPublicationViewController
@synthesize lien, title;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization 
    }
    return self;
    
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=self.labelTitle.text;
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
    
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	[reachability startNotifier];
	
	NetworkStatus status = [reachability currentReachabilityStatus];
	
	self.loadingView.hidden = NO;
	self.labelProb.hidden = YES;
	self.loadingIndicator.hidden = YES;
	if(status == NotReachable)
	{
		//No internet
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Aucune Connexion Internet Trouvée" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		alertView.tag = 1;
		[alertView show];
		[alertView release];
		
	}
	else if (status == ReachableViaWiFi)
	{
		//WiFi 
		self.loadingIndicator.hidden = NO;
		NSURL *url = [NSURL URLWithString:self.lien];
		//URL Requst Object
		NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
		//Load the request in the UIWebView.
		[self.labelWebView loadRequest:requestObj];
		
	}
	else if (status == ReachableViaWWAN)
	{
		//3G
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Voulez-vous télécharger le PDF via votre connexion 3G?" delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Oui",nil];
		alertView.tag = 2;
		[alertView show];
		[alertView release];
	}
	
	// Do any additional setup after loading the view.
	
	self.labelTitle.text = self.title;
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
}

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 1)
	{
		[self goBack];
	}
	else if (alertView.tag == 2)
	{
		if (buttonIndex == 1)
		{
			self.loadingIndicator.hidden = NO;
			NSURL *url = [NSURL URLWithString:self.lien];
			//URL Requst Object
			NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
			//Load the request in the UIWebView.
			[self.labelWebView loadRequest:requestObj];
		}
		else
			[self goBack];
	}
	
	
}

- (void)goBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	self.viewChargement.hidden = YES;
}

- (void)webView:(UIWebView *) webView didFailLoadWithError:(NSError *)error
{
	self.loadingIndicator.hidden = YES;
	
	self.labelProb.hidden = NO;

}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
	[_labelTitle release];
	[_labelWebView release];
	[title release];
	[lien release];
	[_viewChargement release];
	[_loadingIndicator release];
	[_labelProb release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setLabelTitle:nil];
	[self setLabelWebView:nil];
	[self setViewChargement:nil];
	[self setLoadingIndicator:nil];
	[self setLabelProb:nil];
	[super viewDidUnload];
}
@end
