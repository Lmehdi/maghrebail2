//
//  DetailPDFPublicationViewController.h
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailPDFPublicationViewController : BaseViewController
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UIWebView *labelWebView;

@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) NSString *lien;
@property (retain, nonatomic) IBOutlet UIView *viewChargement;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (retain, nonatomic) IBOutlet UILabel *labelProb;
-(void) initHeaderColor;
@end
