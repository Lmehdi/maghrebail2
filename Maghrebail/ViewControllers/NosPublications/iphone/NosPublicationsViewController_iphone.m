//
//  NosPublicationsViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosPublicationsViewController_iphone.h"
#import "PublicationCell_iphone.h"
#import "DetailPDFPublicationsViewController_iphone.h";

@interface NosPublicationsViewController_iphone ()

@end

@implementation NosPublicationsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 49;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PublicationCell *cell = nil;
	static NSString *cellIdentifier_p = @"PublicationCell_iphone";
        
        cell = (PublicationCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (PublicationCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
		
		cell.labelTitle.text = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"titre"];
        cell.labelTaille.text = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"taille"];
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    } 
	
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
	DetailPDFPublicationsViewController_iphone *view  = [[DetailPDFPublicationsViewController_iphone alloc] initWithNibName:@"DetailPDFPublicationsViewController_iphone" bundle:nil];
	view.title = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"titre"];;
	view.lien = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"lien_pdf"];;
	[self.navigationController pushViewController:view animated:YES];
	[view release];
}

@end
