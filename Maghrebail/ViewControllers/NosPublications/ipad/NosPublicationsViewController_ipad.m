//
//  NosPublicationsViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosPublicationsViewController_ipad.h"
#import "PublicationCell_ipad.h"
#import "DetailPDFPublicationsViewController_ipad.h";

@interface NosPublicationsViewController_ipad ()

@end

@implementation NosPublicationsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 49;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PublicationCell *cell = nil;
	static NSString *cellIdentifier_p = @"PublicationCell_ipad";
        
        cell = (PublicationCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (PublicationCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
		
		cell.labelTitle.text = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"titre"];
        cell.labelTaille.text = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"taille"];
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    } 
	
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
	DetailPDFPublicationsViewController_ipad *view  = [[DetailPDFPublicationsViewController_ipad alloc] initWithNibName:@"DetailPDFPublicationsViewController_ipad" bundle:nil];
	view.title = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"titre"];;
	view.lien = [[dataManager.publications  objectAtIndex:indexPath.row] objectForKey:@"lien_pdf"];;
	[self.navigationController pushViewController:view animated:YES];
	[view release];
}

@end
