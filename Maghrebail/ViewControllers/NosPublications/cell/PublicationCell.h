//
//  PublicationCell.h
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelTaille;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;

@end
