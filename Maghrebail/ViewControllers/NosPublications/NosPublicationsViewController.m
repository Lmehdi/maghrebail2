//
//  NosPublicationsViewController.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "NosPublicationsViewController.h"
#import "JSON.h"
@interface NosPublicationsViewController ()

@end

@implementation NosPublicationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		shouldDisplayDrag = NO;
    }
    return self;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"NOS PUBLICATIONS";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    //[self performSelectorInBackground:@selector(loadPublications) withObject:nil];
	if (dataManager.publications.count == 0)
	{
		
        NSData *dataProduits = [DataManager readDataIntoCachWith:@"publications"];
        if (dataProduits)
        {
            dataManager.publications = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
        }
		else
		{
			NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"publications" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
			NSDictionary *responseDico = [response JSONValue];
			NSArray *data = [responseDico objectForKey:@"response"];
			dataManager.publications = [NSMutableArray arrayWithArray:data];
		}
	}
	// Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
     [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}
 


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataManager.publications.count;
}


@end
