//
//  MesImmatriculationsViewController_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesImmatriculationsViewController_ipad.h"
#import "MesImmatriculationsViewCell.h"
#import "ListCommandesViewController_ipad.h"
@interface MesImmatriculationsViewController_ipad ()

@end

@implementation MesImmatriculationsViewController_ipad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    static NSString *cellIdentifier_l;
    /*
     if ([DataManager isIphone5LandScape])
     {
     cellIdentifier_l = @"MesImmatriculationsViewCell_iphone5_l";
     }
     
     else
     cellIdentifier_l = @"MesImmatriculationsViewCell_iphone_l";
     
     */
    
    cellIdentifier_l = @"MesImmatriculationsViewCell_ipad";
    
    MesImmatriculationsViewCell *cell = (MesImmatriculationsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
    if (cell == nil)
    {
        cell = (MesImmatriculationsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
    }
    
    cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
    
    cell.datecom.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
    cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
    cell.MTTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];
    
    cell.montantht.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC_CMDE"]];
    
    cell.alpha = 0.5;
    [cell layoutIfNeeded];
    switch (indexPath.row % 2) {
        case 0:
            cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
            
            break;
        case 1:
            cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    self.isFromButton = NO;
    dataManager.isdataimmatriculation=YES;
    dataManager.isdatacommande=NO;
    dataManager.isdataengagement=NO;
    dataManager.isdetailscontact=NO;
    ListCommandesViewController_ipad * listCommandesViewController_iphone = [[ListCommandesViewController_ipad alloc]initWithNibName:@"ListCommandesViewController_ipad" bundle:nil];
    
    /*     if (!self.showFav)
     listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
     else
     listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
    listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
    [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
}

@end
