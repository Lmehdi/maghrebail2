//
//  MesImmatriculationsViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesImmatriculationsViewController_iphone.h"

#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@interface MesImmatriculationsViewController_iphone ()

@end
CGRect  oldFrame;
@implementation MesImmatriculationsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    if(!isLandscape){
        oldFrame=self.viewTableView.frame;
    }
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
        
    {
        self.viewTableView.frame=oldFrame;
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    if (isLandscape) {
        
        
        static NSString *cellIdentifier_l;
     /*
        if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"MesImmatriculationsViewCell_iphone5_l";
        }
        
        else
            cellIdentifier_l = @"MesImmatriculationsViewCell_iphone_l";
        
      */
        
        cellIdentifier_l = @"MesImmatriculationsViewCell_iphone_l";
        
        MesImmatriculationsViewCell *cell = (MesImmatriculationsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MesImmatriculationsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
       cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
        
        cell.datecom.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
        cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
        cell.MTTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];
        
        cell.montantht.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC_CMDE"]];

        cell.alpha = 0.5;
        [cell layoutIfNeeded];
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }else
    {
        
       
        static NSString *cellIdentifier_p = @"MesImmatriculationsViewCell_iphone_p";
       
        
        MesImmatriculationsViewCell *cell = (MesImmatriculationsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell = (MesImmatriculationsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.alpha = 0.5;
        
        
        cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
        cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
        
        //date
        cell.datecom.text = [DataManager getDateStringFromPresentableDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
        cell.montantht.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isLandscape)
    {
        /*   if (dataManager.isDemo)
         
         else
         [Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];*/
        self.isFromButton = NO;
        dataManager.isdataimmatriculation=YES;
        dataManager.isdatacommande=NO;
        dataManager.isdataengagement=NO;
        dataManager.isdetailscontact=NO;
        ListCommandesViewController_iphone * listCommandesViewController_iphone = [[ListCommandesViewController_iphone alloc]initWithNibName:@"ListCommandesViewController_iphone" bundle:nil];
        
        /*     if (!self.showFav)
         listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
         else
         listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
        listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
        [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
    }
    
    
    
}
@end
