//
//  MesImmatriculationsViewCell.h
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MesImmatriculationsViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *numcommande;
@property (retain, nonatomic) IBOutlet UILabel *datecom;
@property (retain, nonatomic) IBOutlet UILabel *client;
@property (retain, nonatomic) IBOutlet UILabel *interlocuteur;
@property (retain, nonatomic) IBOutlet UILabel *montantht;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;
@property (retain, nonatomic) IBOutlet UILabel *MTTTC;


@end
