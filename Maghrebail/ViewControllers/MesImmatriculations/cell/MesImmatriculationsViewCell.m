//
//  MesImmatriculationsViewCell.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesImmatriculationsViewCell.h"

@implementation MesImmatriculationsViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numcommande release];
    [_datecom release];
    [_client release];
    [_interlocuteur release];
    [_montantht release];
    [_flecheimage release];
    [_MTTTC release];
    [super dealloc];
}
@end
