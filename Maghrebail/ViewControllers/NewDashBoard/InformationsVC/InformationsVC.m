//
//  InformationsVC.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "InformationsVC.h"

@interface InformationsVC ()

@end

@implementation InformationsVC
@synthesize  strechy;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    int indexOrigine=323;
    NSString* nameImage=@"v3-informations.png";
    if([DataManager isIphone4]){
        nameImage=@"information-iphone4.png";
    }
    
    UIImage *image = [ UIImage imageNamed:nameImage];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;
    
    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width,indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    imgParallaxe.image=[UIImage imageNamed:nameImage];
    
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scrollViewInfos];
    self.scrollViewInfos.scrollEnabled=NO;
    [self.scrollViewInfos setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,332)];
    [self.mainView addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_mainView release];
    [_scrollViewInfos release];
    [super dealloc];
}

@end
