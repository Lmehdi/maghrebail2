//
//  InformationsVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "StrechyParallaxScrollView.h"
@interface InformationsVC : BaseViewController
@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
@property (retain, nonatomic) IBOutlet UIView *mainView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollViewInfos;

@end
