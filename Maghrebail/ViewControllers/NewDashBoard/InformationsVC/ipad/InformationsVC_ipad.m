//
//  InformationsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "InformationsVC_ipad.h"
#import "AppgroupeVC_ipad.h"
#import "SlideViewController.h"
#import "AppDelegate.h"


@interface InformationsVC_ipad ()

@end

@implementation InformationsVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)btnActionInfo:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showFAQ" object:nil];
            break;
        case 1:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showNosPublication" object:nil];
            break;
        case 2:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMentionLegales" object:nil];
            break;
        default:
            break;
    }
}

- (IBAction)btnActionContacter:(UIButton *)sender {
    AppgroupeVC_ipad *appgoupeVC =nil;

    AppDelegate* mainDelegate=nil;
    
    switch (sender.tag) {
        case 3:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showFacebook" object:nil];
            //[dataManager.slideMenu showFacebook:YES];
            break;
            
        case 4:
            // [dataManager.slideMenu showTwitter:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showTwitter" object:nil];
            break;
            
        case 5:
             mainDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
             appgoupeVC = [[AppgroupeVC_ipad alloc] initWithNibName:@"AppgroupeVC_ipad" bundle:nil];
             [mainDelegate.window.rootViewController.view addSubview:appgoupeVC.view];
            break;
            
        case 6:
          //  [dataManager.slideMenu nosContacts:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showContacts" object:nil];
            break;
            
        default:
            break;
    }
}
@end
