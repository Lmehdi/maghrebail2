//
//  InformationsVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "InformationsVC.h"

@interface InformationsVC_ipad: InformationsVC

- (IBAction)btnActionInfo:(UIButton *)sender;
- (IBAction)btnActionContacter:(UIButton *)sender;
@end
