//
//  InformationsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "InformationsVC_iphone.h"
#import "AppgroupeVC_iphone.h"
#import "SlideViewController.h"
#import "AppDelegate.h"
@interface InformationsVC_iphone ()

@end

@implementation InformationsVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)btnActionInfo:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
             [dataManager.slideMenu faq:YES];
            break;
        case 1:
              [dataManager.slideMenu nosPublications:YES];
            break;
        case 2:
            [dataManager.slideMenu mentionLegale:YES];
            break;
        default:
            break;
    }
}

- (IBAction)btnActionContacter:(UIButton *)sender {
    AppgroupeVC_iphone *appgoupeVC =nil;
    AppDelegate* mainDelegate=nil;
    
    switch (sender.tag) {
        case 3:
            [dataManager.slideMenu showFacebook:YES];
            break;
            
        case 4:
             [dataManager.slideMenu showTwitter:YES];
            break;
            
        case 5:
             mainDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
             appgoupeVC = [[AppgroupeVC_iphone alloc] initWithNibName:@"AppgroupeVC_iphone" bundle:nil];
             [mainDelegate.window.rootViewController.view addSubview:appgoupeVC.view];
           // [self.view addSubview:appgoupeVC.view];
           // [self.navigationController pushViewController:vappgoupeVC animated:YES];
           // [dataManager.slideMenu showAppGroupe:YES];
            break;
            
        case 6:
            [dataManager.slideMenu nosContacts:YES];
            break;
            
        default:
            break;
    }
}
@end
