//
//  NewDashBoardVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "NewDashBoardVC.h"

@class ProfessionnelsVC_ipad;
@class EntreprisesVC_ipad;
@class MonProfileVC_ipad;
@class MaSituationVC_ipad;
@class MesContratsVC_ipad;
@class InformationsVC_ipad;
@class fournisseurVC_ipad;
@interface NewDashBoardVC_ipad : NewDashBoardVC<UIScrollViewDelegate>
{
    int lastPosition;
}
@property(nonatomic,retain) ProfessionnelsVC_ipad* ProfesionalsVC;
@property(nonatomic,retain) EntreprisesVC_ipad* EntrepriseVC;
@property(nonatomic,retain) MonProfileVC_ipad* ProfilVC;
@property(nonatomic,retain) MaSituationVC_ipad* SituationVC;
@property(nonatomic,retain) MesContratsVC_ipad* ContractVC;
@property(nonatomic,retain) InformationsVC_ipad* InformationsVC;
@property(nonatomic,retain) fournisseurVC_ipad * fournisseurVC;

@end
