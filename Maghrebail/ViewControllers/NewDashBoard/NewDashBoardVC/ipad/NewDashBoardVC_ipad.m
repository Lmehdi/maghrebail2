//
//  NewDashBoardVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "NewDashBoardVC_ipad.h"
#import "ProfessionnelsVC_ipad.h"
#import "EntreprisesVC_ipad.h"
#import "MonProfileVC_ipad.h"
#import "MaSituationVC_ipad.h"
#import "MesContratsVC_ipad.h"
#import "InformationsVC_ipad.h"
#import "fournisseurVC.h"
#import "fournisseurVC_ipad.h"
#define screen_height 768
#define screen_width 938
#define numberOfPage 7

@interface NewDashBoardVC_ipad ()

@end

@implementation NewDashBoardVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        lastPosition=0;
    }
    return self;
}

-(void) constructViewControllers{
    
    CGFloat offSet =0;
    if(!dataManager.isClientContent && !dataManager.isFournisseurContent)
    {
    if (!self.ProfesionalsVC) {
        self.ProfesionalsVC=[[ProfessionnelsVC_ipad alloc] initWithNibName:@"ProfessionnelsVC_ipad" bundle:nil];
    }
    
    [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, screen_width,screen_height)];
    offSet+=screen_width;
   
    if (!self.EntrepriseVC) {
        self.EntrepriseVC=[[EntreprisesVC_ipad alloc] initWithNibName:@"EntreprisesVC_ipad" bundle:nil];
    }
    [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;
    
    if (!self.ProfilVC) {
        self.ProfilVC=[[MonProfileVC_ipad alloc] initWithNibName:@"MonProfileVC_ipad" bundle:nil];
    }
    [self.ProfilVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;
    
    if (!self.SituationVC) {
        self.SituationVC=[[MaSituationVC_ipad alloc] initWithNibName:@"MaSituationVC_ipad" bundle:nil];
    }
    [self.SituationVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;

    
    if (!self.ContractVC) {
        self.ContractVC=[[MesContratsVC_ipad alloc] initWithNibName:@"MesContratsVC_ipad" bundle:nil];
    }
    [self.ContractVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;
  
//    if (!self.InformationsVC) {
//        self.InformationsVC=[[InformationsVC_ipad alloc] initWithNibName:@"InformationsVC_ipad" bundle:nil];
//    }
//    
//    [self.InformationsVC.view setFrame:CGRectMake(offSet,0, 1024, screen_height)];
//    offSet+=screen_width;
    
    // Organize subViews in scroll View
    
    //DS
    
    if (!self.fournisseurVC) {
        self.fournisseurVC=[[fournisseurVC_ipad alloc] initWithNibName:@"fournisseurVC_ipad" bundle:nil];
    }
    [self.fournisseurVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;
        
        
 //   [self.scrollMain addSubview:self.fournisseurVC.view];
    
    
    
    if (!self.InformationsVC) {
        self.InformationsVC=[[InformationsVC_ipad alloc] initWithNibName:@"InformationsVC_ipad" bundle:nil];
    }
    
    [self.InformationsVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
    offSet+=screen_width;
    //[self.scrollMain addSubview:self.InformationsVC.view];
    
    self.scrollControll.numberOfPages=numberOfPage;
    [self.scrollMain setContentSize:CGSizeMake(screen_width*numberOfPage, screen_height)];
}



    ///
    if(dataManager.isClientContent)
    {
        
        if (!self.ProfesionalsVC) {
            self.ProfesionalsVC=[[ProfessionnelsVC_ipad alloc] initWithNibName:@"ProfessionnelsVC_ipad" bundle:nil];
        }
        [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        [self.scrollMain addSubview:self.ProfesionalsVC.view];
        
        
        
        if (!self.EntrepriseVC) {
            self.EntrepriseVC=[[EntreprisesVC_ipad alloc] initWithNibName:@"EntreprisesVC_ipad" bundle:nil];
        }
        [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        [self.scrollMain addSubview:self.EntrepriseVC.view];
        
        
        
        
        
        if (!self.ProfilVC) {
            self.ProfilVC=[[MonProfileVC_ipad alloc] initWithNibName:@"MonProfileVC_ipad" bundle:nil];
        }
        [self.ProfilVC.view setFrame:CGRectMake(offSet,0,screen_width, screen_height)];
        offSet+=screen_width;
        [self.scrollMain addSubview:self.ProfilVC.view];
        
        
        
        if (!self.SituationVC) {
            self.SituationVC=[[MaSituationVC_ipad alloc] initWithNibName:@"MaSituationVC_ipad" bundle:nil];
        }
        [self.SituationVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        [self.scrollMain addSubview:self.SituationVC.view];
        
        
        if (!self.ContractVC) {
            self.ContractVC=[[MesContratsVC_ipad alloc] initWithNibName:@"MesContratsVC_ipad" bundle:nil];
        }
        [self.ContractVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        [self.scrollMain addSubview:self.ContractVC.view];
        
        
        
        
        if (!self.InformationsVC) {
            self.InformationsVC=[[InformationsVC_ipad alloc] initWithNibName:@"InformationsVC_ipad" bundle:nil];
        }
        
        [self.InformationsVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
      //  [self.scrollMain addSubview:self.InformationsVC.view];
        
        self.scrollControll.numberOfPages=numberOfPage;
        [self.scrollMain setContentSize:CGSizeMake(screen_width*numberOfPage, screen_height)];
    }
    
    if(dataManager.isFournisseurContent)
    {
        if (!self.ProfesionalsVC) {
            self.ProfesionalsVC=[[ProfessionnelsVC_ipad alloc] initWithNibName:@"ProfessionnelsVC_ipad" bundle:nil];
        }
        [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
      //  [self.scrollMain addSubview:self.ProfesionalsVC.view];
        
        if (!self.EntrepriseVC) {
            self.EntrepriseVC=[[EntreprisesVC_ipad alloc] initWithNibName:@"EntreprisesVC_ipad" bundle:nil];
        }
        [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
    //    [self.scrollMain addSubview:self.EntrepriseVC.view];
        
        if (!self.ProfilVC) {
            self.ProfilVC=[[MonProfileVC_ipad alloc] initWithNibName:@"MonProfileVC_ipad" bundle:nil];
        }
        [self.ProfilVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
      //  [self.scrollMain addSubview:self.ProfilVC.view];
        
        if (!self.SituationVC) {
            self.SituationVC=[[MaSituationVC_ipad alloc] initWithNibName:@"MaSituationVC_ipad" bundle:nil];
        }
        [self.SituationVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        
        
        if (!self.ContractVC) {
            self.ContractVC=[[MesContratsVC_ipad alloc] initWithNibName:@"MesContratsVC_ipad" bundle:nil];
        }
        [self.ContractVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
        /*
         if (!self.SituationVC) {
         self.SituationVC=[[MaSituationVC_iphone alloc] initWithNibName:@"MaSituationVC_iphone" bundle:nil];
         }
         [self.SituationVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
         offSet+=mainScreen_width;
         [self.scrollMain addSubview:self.SituationVC.view];
         
         
         if (!self.ContractVC) {
         self.ContractVC=[[MesContratsVC_iphone alloc] initWithNibName:@"MesContratsVC_iphone" bundle:nil];
         }
         [self.ContractVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
         offSet+=mainScreen_width;
         [self.scrollMain addSubview:self.ContractVC.view];
         */
        
        if (!self.fournisseurVC) {
            self.fournisseurVC=[[fournisseurVC_ipad alloc] initWithNibName:@"fournisseurVC_ipad" bundle:nil];
        }
        [self.fournisseurVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
       
        
        if (!self.InformationsVC) {
            self.InformationsVC=[[InformationsVC_ipad alloc] initWithNibName:@"InformationsVC_ipad" bundle:nil];
        }
        
        [self.InformationsVC.view setFrame:CGRectMake(offSet,0, screen_width, screen_height)];
        offSet+=screen_width;
      //  [self.scrollMain addSubview:self.InformationsVC.view];
        
        self.scrollControll.numberOfPages=numberOfPage;
        [self.scrollMain setContentSize:CGSizeMake(screen_width*numberOfPage, screen_height)];
        
        
        
    }



    [self.scrollMain addSubview:self.InformationsVC.view];
     [self.scrollMain addSubview:self.fournisseurVC.view];
    [self.scrollMain addSubview:self.ContractVC.view];
    [self.scrollMain addSubview:self.SituationVC.view];
    [self.scrollMain addSubview:self.ProfilVC.view];
    [self.scrollMain addSubview:self.EntrepriseVC.view];
    [self.scrollMain addSubview:self.ProfesionalsVC.view];
    
    // init ScrollView
    
     self.scrollControll.numberOfPages=numberOfPage;
    [self.scrollMain setContentSize:CGSizeMake(screen_width*numberOfPage, screen_height)];
    [self.scrollHeader setContentSize:CGSizeMake(2560, 74)];
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    // Sep animation
    
    float valueX=0;
    float coefficent=(float)938/512;
    valueX=scrollView.contentOffset.x*coefficent-lastPosition;
    lastPosition=scrollView.contentOffset.x*coefficent;
 
    CGPoint offset = self.scrollHeader.contentOffset;
    offset.x = self.scrollMain.contentOffset.x/coefficent;
  
    [self.btnMenu setFrame:CGRectMake(-7-self.scrollHeader.contentOffset.x, self.btnMenu.frame.origin.y,self.btnMenu.frame.size.width, self.btnMenu.frame.size.height)];
    
    [self.scrollHeader setContentOffset:offset];
    dataManager.indexNewDash=(int)(self.scrollMain.contentOffset.x/screen_width);
    self.scrollControll.currentPage=dataManager.indexNewDash;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initializePrincipaleScrollView];
}
-(void) initializePrincipaleScrollView{
    [self.scrollMain setContentOffset:CGPointMake(dataManager.indexNewDash*screen_width, self.scrollMain.frame.origin.y)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
