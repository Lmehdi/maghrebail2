//
//  NewDashBoardVC.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "NewDashBoardVC.h"
#import "AppDelegate.h"

@interface NewDashBoardVC ()

@end

@implementation NewDashBoardVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.automaticallyAdjustsScrollViewInsets=NO;
    [self constructViewControllers];
    
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self hideAllSeprators:true];
}
-(void) hideAllSeprators:(BOOL) hiden{
    [self.viewSep2 setHidden:hiden];
    [self.viewSep3 setHidden:hiden];
    [self.viewSep4 setHidden:hiden];
    [self.viewSep5 setHidden:hiden];
    [self.viewSep6 setHidden:hiden];
    [self.view_fournisseur setHidden:hiden];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void) constructViewControllers{
    
}


- (void)dealloc {
    [_scrollMain release];
    [_scrollHeader release];
    [_btnMenu release];
    [_viewSep2 release];
    [_viewSep3 release];
    [_viewSep4 release];
    [_viewSep5 release];
    [_viewSep6 release];
    [_scrollControll release];
    [_labelPro release];
    [_labelEtr release];
    [_labelLesbox1 release];
    [_labelLesbox2 release];
    [_labelLesbox2 release];
    [_labelLesbox3 release];
    [_labelInfos release];
    [_view_fournisseur release];
    [_labelLesbox_fournisseur release];
    [super dealloc];
}
- (IBAction)btnMenu:(UIButton *)sender {
    [self showSideMenu];
}

@end
