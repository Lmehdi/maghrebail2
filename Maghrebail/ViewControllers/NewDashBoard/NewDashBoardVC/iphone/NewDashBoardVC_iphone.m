//
//  NewDashBoardVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "NewDashBoardVC_iphone.h"
#import "ProfessionnelsVC_iphone.h"
#import "EntreprisesVC_iphone.h"
#import "MonProfileVC_iphone.h"
#import "MaSituationVC_iphone.h"
#import "InformationsVC_iphone.h"
#import "fournisseurVC_iphone.h"
#import "MesContratsVC_iphone.h"
#import "SlideViewController_iphone.h"
#import "ProfessionnelsVC_ipad.h"
#import "AppDelegate.h"

#define numberOfPage 7
#define numberofPageClient 6
#define numberofPageFournisseur 5

@interface NewDashBoardVC_iphone ()

@end

@implementation NewDashBoardVC_iphone
@synthesize ProfesionalsVC,EntrepriseVC,ProfilVC,SituationVC,InformationsVC,ContractVC,fournisseurVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
        lastPosition=0;
    }
    return self;
}




- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.sideMenu = [self sideMenu];
    [self initLabelHeaderPositions];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStatusBarFrameChanged) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}
-(void) initLabelHeaderPositions{
    CGRect fram=CGRectMake(mainScreen_width/4,self.labelEtr.frame.origin.y, mainScreen_width/2, self.labelEtr.frame.size.height);
    
    [self.labelPro setFrame:fram];
    
    fram.origin.x+=mainScreen_width/2;
    [self.labelEtr setFrame:fram];
    
    
    
    fram.origin.x+=mainScreen_width/2;
    [self.labelLesbox1 setFrame:fram];
    
    fram.origin.x+=mainScreen_width/2;
    [self.labelLesbox3 setFrame:fram];
    
    fram.origin.x+=mainScreen_width/2;
    [self.labelLesbox2 setFrame:fram];
   
    fram.origin.x+=mainScreen_width/2;
    [self.labelLesbox_fournisseur setFrame:fram];
    
    fram.origin.x+=mainScreen_width/2;
    [self.labelInfos setFrame:fram];
    
}
- (MFSideMenu *)sideMenu
{
    
    SlideViewController  *leftSideMenuController = nil;
    SlideViewController *rightSideMenuController = nil;
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    if (!IS_IPAD)
    {
	 	leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
		leftSideMenuController.window = delegate.window;
		rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
		rightSideMenuController.window = delegate.window;
	
    }
 
    
    UINavigationController *navigationController = [self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:leftSideMenuController
                                            rightSideMenuController:rightSideMenuController];
    leftSideMenuController.sideMenu = sideMenu;
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}


-(void) constructViewControllers{
    
    CGFloat offSet =0;
    if(!dataManager.isClientContent && !dataManager.isFournisseurContent)
    {
        if (!self.ProfesionalsVC) {
            self.ProfesionalsVC=[[ProfessionnelsVC_iphone alloc] initWithNibName:@"ProfessionnelsVC_iphone" bundle:nil];
        }
        [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width,mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfesionalsVC.view];
        
        
        
        if (!self.EntrepriseVC) {
            self.EntrepriseVC=[[EntreprisesVC_iphone alloc] initWithNibName:@"EntreprisesVC_iphone" bundle:nil];
        }
        [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.EntrepriseVC.view];
        
        
        
        
        
        if (!self.ProfilVC) {
            self.ProfilVC=[[MonProfileVC_iphone alloc] initWithNibName:@"MonProfileVC_iphone" bundle:nil];
        }
        [self.ProfilVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfilVC.view];
        
        
        
        if (!self.SituationVC) {
            self.SituationVC=[[MaSituationVC_iphone alloc] initWithNibName:@"MaSituationVC_iphone" bundle:nil];
        }
        [self.SituationVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.SituationVC.view];
        
        
        if (!self.ContractVC) {
            self.ContractVC=[[MesContratsVC_iphone alloc] initWithNibName:@"MesContratsVC_iphone" bundle:nil];
        }
        [self.ContractVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ContractVC.view];
        
        if (!self.fournisseurVC) {
            self.fournisseurVC=[[fournisseurVC_iphone alloc] initWithNibName:@"fournisseurVC_iphone" bundle:nil];
        }
        [self.fournisseurVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        
        
        
        [self.scrollMain addSubview:self.fournisseurVC.view];
        
        
        
        if (!self.InformationsVC) {
            self.InformationsVC=[[InformationsVC_iphone alloc] initWithNibName:@"InformationsVC_iphone" bundle:nil];
        }
        
        [self.InformationsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.InformationsVC.view];
    
        self.scrollControll.numberOfPages=numberOfPage;
        [self.scrollMain setContentSize:CGSizeMake(mainScreen_width*numberOfPage, mainScreen_height)];
            }
    if(dataManager.isClientContent)
    {

        if (!self.ProfesionalsVC) {
            self.ProfesionalsVC=[[ProfessionnelsVC_iphone alloc] initWithNibName:@"ProfessionnelsVC_iphone" bundle:nil];
        }
        [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width,mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfesionalsVC.view];
        
        
        
        if (!self.EntrepriseVC) {
            self.EntrepriseVC=[[EntreprisesVC_iphone alloc] initWithNibName:@"EntreprisesVC_iphone" bundle:nil];
        }
        [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.EntrepriseVC.view];
        
        
        
        
        
        if (!self.ProfilVC) {
            self.ProfilVC=[[MonProfileVC_iphone alloc] initWithNibName:@"MonProfileVC_iphone" bundle:nil];
        }
        [self.ProfilVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfilVC.view];
        
        
        
        if (!self.SituationVC) {
            self.SituationVC=[[MaSituationVC_iphone alloc] initWithNibName:@"MaSituationVC_iphone" bundle:nil];
        }
        [self.SituationVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.SituationVC.view];
        
        
        if (!self.ContractVC) {
            self.ContractVC=[[MesContratsVC_iphone alloc] initWithNibName:@"MesContratsVC_iphone" bundle:nil];
        }
        [self.ContractVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ContractVC.view];
        
        
        
        
        if (!self.InformationsVC) {
            self.InformationsVC=[[InformationsVC_iphone alloc] initWithNibName:@"InformationsVC_iphone" bundle:nil];
        }
        
        [self.InformationsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.InformationsVC.view];
    
        self.scrollControll.numberOfPages=numberofPageClient;
        [self.scrollMain setContentSize:CGSizeMake(mainScreen_width*numberofPageClient, mainScreen_height)];
    }
    
    if(dataManager.isFournisseurContent)
    {
        if (!self.ProfesionalsVC) {
            self.ProfesionalsVC=[[ProfessionnelsVC_iphone alloc] initWithNibName:@"ProfessionnelsVC_iphone" bundle:nil];
        }
        [self.ProfesionalsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width,mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfesionalsVC.view];
        
        if (!self.EntrepriseVC) {
            self.EntrepriseVC=[[EntreprisesVC_iphone alloc] initWithNibName:@"EntreprisesVC_iphone" bundle:nil];
        }
        [self.EntrepriseVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.EntrepriseVC.view];
        
     if (!self.ProfilVC) {
            self.ProfilVC=[[MonProfileVC_iphone alloc] initWithNibName:@"MonProfileVC_iphone" bundle:nil];
        }
        [self.ProfilVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ProfilVC.view];
        
        
     /*
        if (!self.SituationVC) {
            self.SituationVC=[[MaSituationVC_iphone alloc] initWithNibName:@"MaSituationVC_iphone" bundle:nil];
        }
        [self.SituationVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.SituationVC.view];
        
      
        if (!self.ContractVC) {
            self.ContractVC=[[MesContratsVC_iphone alloc] initWithNibName:@"MesContratsVC_iphone" bundle:nil];
        }
        [self.ContractVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.ContractVC.view];
        */
        
        if (!self.fournisseurVC) {
            self.fournisseurVC=[[fournisseurVC_iphone alloc] initWithNibName:@"fournisseurVC_iphone" bundle:nil];
        }
        [self.fournisseurVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.fournisseurVC.view];
        
        if (!self.InformationsVC) {
            self.InformationsVC=[[InformationsVC_iphone alloc] initWithNibName:@"InformationsVC_iphone" bundle:nil];
        }
        
        [self.InformationsVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, mainScreen_height)];
        offSet+=mainScreen_width;
        [self.scrollMain addSubview:self.InformationsVC.view];
        
        self.scrollControll.numberOfPages=numberofPageFournisseur;
        [self.scrollMain setContentSize:CGSizeMake(mainScreen_width*numberofPageFournisseur, mainScreen_height)];
        
        

   }

    [self.scrollHeader setContentSize:CGSizeMake(1020, 64)];

    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    // Sep animation
    
    float valueX=0;
    valueX=scrollView.contentOffset.x*2-lastPosition;
    lastPosition=scrollView.contentOffset.x*2;
    
    [self.viewSep2 setFrame:CGRectMake(self.viewSep2.frame.origin.x-valueX, self.viewSep2.frame.origin.y, self.viewSep2.frame.size.width, self.viewSep2.frame.size.height)];
    [self.viewSep3 setFrame:CGRectMake(self.viewSep3.frame.origin.x-valueX, self.viewSep3.frame.origin.y, self.viewSep3.frame.size.width, self.viewSep3.frame.size.height)];
    [self.viewSep4 setFrame:CGRectMake(self.viewSep4.frame.origin.x-valueX, self.viewSep4.frame.origin.y, self.viewSep4.frame.size.width, self.viewSep4.frame.size.height)];
    [self.viewSep5 setFrame:CGRectMake(self.viewSep5.frame.origin.x-valueX, self.viewSep5.frame.origin.y, self.viewSep5.frame.size.width, self.viewSep5.frame.size.height)];
    [self.view_fournisseur setFrame:CGRectMake(self.view_fournisseur.frame.origin.x-valueX, self.view_fournisseur.frame.origin.y, self.view_fournisseur.frame.size.width, self.view_fournisseur.frame.size.height)];
    [self.viewSep6 setFrame:CGRectMake(self.viewSep6.frame.origin.x-valueX, self.viewSep6.frame.origin.y, self.viewSep6.frame.size.width, self.viewSep6.frame.size.height)];
    
    int valueOfOffSet=scrollView.contentOffset.x;
    if (valueOfOffSet % 320 == 0) {
        
        [self hideAllSeprators:true];
        
    }else{
        
        [self hideAllSeprators:true];
    }
    
    CGPoint offset = self.scrollHeader.contentOffset;
    offset.x = self.scrollMain.contentOffset.x/2;
    [self.btnMenu setFrame:CGRectMake(-7-self.scrollHeader.contentOffset.x, self.btnMenu.frame.origin.y,self.btnMenu.frame.size.width, self.btnMenu.frame.size.height)];
    
    [self.scrollHeader setContentOffset:offset];
    dataManager.indexNewDash=(int)(self.scrollMain.contentOffset.x/mainScreen_width);
    
    self.scrollControll.currentPage=dataManager.indexNewDash;
    
}
-(void) initializePrincipaleScrollView{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if(dataManager.isFournisseurContent && appDelegate.isfirsttimeleasebox)
    {
        NSLog(@"%d",dataManager.indexNewDash);
        if(dataManager.indexNewDash==0)
        {
        
        [self.scrollMain setContentOffset:CGPointMake((dataManager.indexNewDash)*mainScreen_width, self.scrollMain.frame.origin.y)];
        }
        else
        {
               [self.scrollMain setContentOffset:CGPointMake((dataManager.indexNewDash-2)*mainScreen_width, self.scrollMain.frame.origin.y)];
        }
     
        appDelegate.isfirsttimeleasebox=NO;
    
    }
    else
    {
        [self.scrollMain setContentOffset:CGPointMake((dataManager.indexNewDash)*mainScreen_width, self.scrollMain.frame.origin.y)];

    }
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     [self hideAllSeprators:true];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self hideAllSeprators:true];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self initializePrincipaleScrollView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) didStatusBarFrameChanged
{
    
    [UIView animateWithDuration:0.5
                     animations:^{
                     
                         int absY= self.view.frame.size.height - self.scrollMain.frame.origin.y;
                         //self.scrollHeader.frame=CGRectMake(self.scrollHeader.frame.origin.x, , self.scrollHeader.frame.size.width, self.scrollHeader.frame.size.height);
                         
//
                         
                       //  self.view.frame=CGRectMake(self.view.frame.origin.x, 20, self.view.frame.size.width, self.view.frame.size.height+20);
                        self.scrollMain.frame=CGRectMake(self.scrollMain.frame.origin.x, self.scrollMain.frame.origin.y, self.scrollMain.frame.size.width, absY);
//                         
                //      [self.scrollMain setContentSize:CGSizeMake(self.scrollMain.frame.size.width,20+self.scrollMain.frame.size.height)];
                         
                         
                     }];
    
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];

    [super dealloc];




}

@end
