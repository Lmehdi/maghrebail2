//
//  NewDashBoardVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "NewDashBoardVC.h"

@class ProfessionnelsVC_iphone;
@class EntreprisesVC_iphone;
@class MonProfileVC_iphone;
@class MaSituationVC_iphone;
@class MesContratsVC_iphone;
@class InformationsVC_iphone;
@class fournisseurVC_iphone;
@interface NewDashBoardVC_iphone : NewDashBoardVC<UIScrollViewDelegate>
{
    int lastPosition;
}
//
@property(nonatomic,retain) ProfessionnelsVC_iphone* ProfesionalsVC;
@property(nonatomic,retain) EntreprisesVC_iphone* EntrepriseVC;
@property(nonatomic,retain) MonProfileVC_iphone* ProfilVC;
@property(nonatomic,retain) MaSituationVC_iphone* SituationVC;
@property(nonatomic,retain) InformationsVC_iphone* InformationsVC;
@property(nonatomic,retain) MesContratsVC_iphone* ContractVC;
@property(nonatomic,retain) fournisseurVC_iphone* fournisseurVC;

@end
