//
//  NewDashBoardVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface NewDashBoardVC : BaseViewController

//Header control

@property (retain, nonatomic) IBOutlet UIPageControl *scrollControll;

// Menu
@property (retain, nonatomic) IBOutlet UIView *viewSep2;
@property (retain, nonatomic) IBOutlet UIView *viewSep3;
@property (retain, nonatomic) IBOutlet UIView *viewSep4;
@property (retain, nonatomic) IBOutlet UIView *viewSep5;
@property (retain, nonatomic) IBOutlet UIView *viewSep6;
@property (retain, nonatomic) IBOutlet UIView *view_fournisseur;
@property (retain, nonatomic) IBOutlet UILabel *labelLesbox_fournisseur;

// Labels
@property (retain, nonatomic) IBOutlet UILabel *labelPro;
@property (retain, nonatomic) IBOutlet UILabel *labelEtr;
@property (retain, nonatomic) IBOutlet UILabel *labelLesbox1;
@property (retain, nonatomic) IBOutlet UILabel *labelLesbox2;
@property (retain, nonatomic) IBOutlet UILabel *labelLesbox3;
@property (retain, nonatomic) IBOutlet UILabel *labelInfos;

@property (retain, nonatomic) IBOutlet UIButton *btnMenu;
// Scroll
@property (retain, nonatomic) IBOutlet UIScrollView *scrollMain;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollHeader;



- (IBAction)btnMenu:(UIButton *)sender;
- (void) hideAllSeprators:(BOOL) hiden;
- (void) constructViewControllers;
@end
