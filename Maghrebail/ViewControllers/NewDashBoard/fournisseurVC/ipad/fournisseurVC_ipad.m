//
//  fournisseurVC_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 24/10/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "fournisseurVC_ipad.h"
#import "AppDelegate.h"
#import "AuthentificationViewController_ipad.h"
@interface fournisseurVC_ipad ()

@end

@implementation fournisseurVC_ipad

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    int indexOrigine=323;
    NSString* nameImage=@"bg_fournisseur.png";
    
  
    
    UIImage *image = [ UIImage imageNamed:nameImage];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;
    
    if([DataManager isIphone4]){
        indexOrigine=290;
    }
    
    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width,indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    
    imgParallaxe.image=[UIImage imageNamed:nameImage];
    
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:self.view.frame andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scrollViewListe];
    self.scrollViewListe.scrollEnabled=NO;
    [self.scrollViewListe setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,284)];
    [self.viewMain addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}


- (void)showPopUp:(NSInteger )index
{/*
  if (!popVC)
  {
  popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
  }
  popVC.index = index;
  [self.view addSubview:popVC.view];*/
    dataManager.handleauthentificationconnect=YES;
    
   AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    
//    if(!authentificationviewcontroller_iphone)
//    {
//        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
//        
//        
//    }
//    authentificationviewcontroller_iphone.index=index;
//    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];
    
    // [mainDelegate.window.rootViewController presentViewController:authentificationviewcontroller_iphone animated:YES completion:<#^(void)completion#>];
    
    
    
  
        if (!authentificationViewController)
        {
            
            if (authentificationViewController)
            {
                authentificationViewController = nil;
                [authentificationViewController release];
            }
            authentificationViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
        // authentificationViewController.delegate = self;
            
        }
 
    
     authentificationViewController.isFromDash = NO;
    authentificationViewController.index=index;
   // mainDelegate.m_SideMenu
    UINavigationController *navigationController = mainDelegate.m_SideMenu.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
   // [mainDelegate.window setRootViewController:authentificationViewController];
 // [mainDelegate.w.navigationController pushViewController:infoController animated:YES];
    
}
- (IBAction)btnActionfournisseur:(UIButton *)sender
{
    dataManager.isFournisseur=YES;
    dataManager.showdemobutton=YES;
    dataManager.DemoredirectfactureFournisseur=YES;
    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    switch (sender.tag) {
        case 17:
            [dataManager.slideMenu mesFactures:YES];
            break;
        case 60:
            [dataManager.slideMenu mesCommandes:YES];
            break;
            
        case 62:
            [dataManager.slideMenu mesReglements:YES];
            break;
        case 64:
            [dataManager.slideMenu mesEngagements:YES];
            break;
        case 65:
            [dataManager.slideMenu mesImmatriculations:YES];
            break;
            
        default:
            break;
    }
    
    
    
    
}

@end
