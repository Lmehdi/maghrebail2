//
//  fournisseurVC.h
//  Maghrebail
//
//  Created by Belkheir on 24/10/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "StrechyParallaxScrollView.h"
@interface fournisseurVC : BaseViewController
@property (retain, nonatomic) IBOutlet UIScrollView *scrollViewListe;

@property (retain, nonatomic) IBOutlet UIView *viewMain;
@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
- (IBAction)btnActionfournisseur:(UIButton *)sender;
@end
