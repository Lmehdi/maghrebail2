//
//  fournisseurVC_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 24/10/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "fournisseurVC_iphone.h"
#import "AppDelegate.h"
@interface fournisseurVC_iphone ()

@end

@implementation fournisseurVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    int indexOrigine=323;
    NSString* nameImage=@"bg_fournisseur.png";
    
   
    if([DataManager isIphone4]){
        nameImage=@"bg_fournisseur.png";
    }
    
    UIImage *image = [ UIImage imageNamed:nameImage];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;
    
    if([DataManager isIphone4]){
        indexOrigine=290;
    }
    
    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width,indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    
    imgParallaxe.image=[UIImage imageNamed:nameImage];
    
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scrollViewListe];
    self.scrollViewListe.scrollEnabled=NO;
    [self.scrollViewListe setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,284)];
    [self.viewMain addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];

    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showPopUp:(NSInteger )index
{/*
  if (!popVC)
  {
  popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
  }
  popVC.index = index;
  [self.view addSubview:popVC.view];*/
    dataManager.handleauthentificationconnect=YES;

    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(!authentificationviewcontroller_iphone)
    {
        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
        
        
    }
    authentificationviewcontroller_iphone.index=index;
    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];
    
   // [mainDelegate.window.rootViewController presentViewController:authentificationviewcontroller_iphone animated:YES completion:<#^(void)completion#>];
    
}
- (IBAction)btnActionfournisseur:(UIButton *)sender
{
     dataManager.isFournisseur=YES;
    dataManager.showdemobutton=YES;
    dataManager.DemoredirectfactureFournisseur=YES;
    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    switch (sender.tag) {
        case 17:
            [dataManager.slideMenu mesFactures:YES];
            break;
        case 60:
            [dataManager.slideMenu mesCommandes:YES];
            break;

        case 62:
            [dataManager.slideMenu mesReglements:YES];
            break;
        case 64:
            [dataManager.slideMenu mesEngagements:YES];
            break;
        case 65:
            [dataManager.slideMenu mesImmatriculations:YES];
            break;
            
        default:
            break;
    }
  



}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
