//
//  MaSituationVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "StrechyParallaxScrollView.h"
@interface MaSituationVC : BaseViewController


@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollViewListe;

@property (retain, nonatomic) IBOutlet UIView *mainView;
- (IBAction)btnActionContact:(UIButton *)sender;

@end
