//
//  MaSituationVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MaSituationVC_iphone.h"
#import "AppDelegate.h"
@interface MaSituationVC_iphone ()

@end

@implementation MaSituationVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    int indexOrigine=323;
    
    UIImage *image = [ UIImage imageNamed: @"v3-MaSituation.png"];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;

    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width, indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    imgParallaxe.image=[UIImage imageNamed:@"v3-MaSituation.png"];
    
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scrollViewListe];
     self.scrollViewListe.scrollEnabled=NO;
    [self.scrollViewListe setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,232)];
    [self.mainView addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];
}
- (void)showPopUp:(NSInteger )index
{
   /* if (!popVC)
    {
        popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
    }

    popVC.index = index;
    [self.view addSubview:popVC.view];*/
    
    dataManager.handleauthentificationconnect=YES;
    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(!authentificationviewcontroller_iphone)
    {
        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
        
        
    }
 
        authentificationviewcontroller_iphone.index=index;
    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];


    }
- (IBAction)btnActionContact:(UIButton *)sender {
    dataManager.DemoredirectfactureFournisseur=NO;
    dataManager.showdemobutton=YES;

    if (!dataManager.authentificationData )
    {
        if(sender.tag==19 || sender.tag==14)
        {
            dataManager.isClient=YES;
        }
        [self showPopUp:sender.tag];
        return;
    }
    
    switch (sender.tag) {
        case 17:
            [dataManager.slideMenu mesFactures:YES];
            break;
        case 19:
            [dataManager.slideMenu mesEcheanciers:YES];
            break;
        case 14:
            [dataManager.slideMenu monSolde:YES];
            break;
            
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
