//
//  MaSituationVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MaSituationVC_ipad.h"
#import "AppDelegate.h"
@interface MaSituationVC_ipad ()

@end

@implementation MaSituationVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (IBAction)btnActionContact:(UIButton *)sender {
    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    
    switch (sender.tag) {
        case 17:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesFactures" object:nil];
            break;
        case 19:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesEcheaciers" object:nil];
            break;
        case 14:
           [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesImpayes" object:nil];
            break;
            
        default:
            break;
    }
}
- (void)showPopUp:(NSInteger )index
{
 /*   if (!popVC)
    {
        popVC = [[PopUpDashVC_ipad alloc] initWithNibName:@"PopUpDashVC_ipad" bundle:nil];
    }
    
    popVC.index = index;
    [self.view addSubview:popVC.view];
    */
    
    dataManager.handleauthentificationconnect=YES;
    
    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //
    //
    //    if(!authentificationviewcontroller_iphone)
    //    {
    //        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    //
    //
    //    }
    //    authentificationviewcontroller_iphone.index=index;
    //    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];
    
    // [mainDelegate.window.rootViewController presentViewController:authentificationviewcontroller_iphone animated:YES completion:<#^(void)completion#>];
    
    
    
    
    if (!authentificationViewController)
    {
        
        if (authentificationViewController)
        {
            authentificationViewController = nil;
            [authentificationViewController release];
        }
        authentificationViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
        // authentificationViewController.delegate = self;
        
    }
    
    dataManager.isClient=YES;
    authentificationViewController.isFromDash = NO;
    authentificationViewController.index=index;
    // mainDelegate.m_SideMenu
    UINavigationController *navigationController = mainDelegate.m_SideMenu.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
