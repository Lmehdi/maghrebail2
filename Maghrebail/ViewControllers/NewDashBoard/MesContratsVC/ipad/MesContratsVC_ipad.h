//
//  MesContratsVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MesContratsVC.h"
#import "PopUpDashVC_ipad.h"
#import "AuthentificationViewController_ipad.h"

@interface MesContratsVC_ipad : MesContratsVC
{
    PopUpDashVC_ipad *popVC;
    AuthentificationViewController_ipad *authentificationviewcontroller_ipad;
    AuthentificationViewController *authentificationViewController;
}

@end
