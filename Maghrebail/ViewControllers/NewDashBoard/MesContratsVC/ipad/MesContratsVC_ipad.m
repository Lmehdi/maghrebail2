//
//  MesContratsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MesContratsVC_ipad.h"
#import "AppDelegate.h"

@interface MesContratsVC_ipad ()

@end

@implementation MesContratsVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)showPopUp:(NSInteger )index
{
/*    if (!popVC)
    {
        popVC = [[PopUpDashVC_ipad alloc] initWithNibName:@"PopUpDashVC_ipad" bundle:nil];
    }
    popVC.index = index;
    [self.view addSubview:popVC.view];*/
    //-----------------
   // AppDelegate *mainDelegate  =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
  //  authentificationviewcontroller_ipad = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
  //  [ mainDelegate.window setRootViewController:authentificationviewcontroller_ipad];
//----------------------------
    
    dataManager.handleauthentificationconnect=YES;
    
    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //
    //
    //    if(!authentificationviewcontroller_iphone)
    //    {
    //        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    //
    //
    //    }
    //    authentificationviewcontroller_iphone.index=index;
    //    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];
    
    // [mainDelegate.window.rootViewController presentViewController:authentificationviewcontroller_iphone animated:YES completion:<#^(void)completion#>];
    
    
    
    
    if (!authentificationViewController)
    {
        
        if (authentificationViewController)
        {
            authentificationViewController = nil;
            [authentificationViewController release];
        }
        authentificationViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
        // authentificationViewController.delegate = self;
        
    }
    
    dataManager.isClient=YES;
    authentificationViewController.isFromDash = NO;
    authentificationViewController.index=index;
    // mainDelegate.m_SideMenu
    UINavigationController *navigationController = mainDelegate.m_SideMenu.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
    
    
}

- (IBAction)btnActionDoc:(UIButton *)sender {
    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    switch (sender.tag) {
        case 13:
          [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContrats" object:nil];
            break;
        case 12:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesLignesCredits" object:nil];
            break;
        case 21:
             [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesDocuments" object:nil];
            break;
        case 15:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesGaranties" object:nil];
            break;
            
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
