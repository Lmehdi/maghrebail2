//
//  MesContratsVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "StrechyParallaxScrollView.h"

@interface MesContratsVC : BaseViewController

@property (retain, nonatomic) IBOutlet UIScrollView *scollMainContrat;

@property (retain, nonatomic) IBOutlet UIView *viewMain;
@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
- (IBAction)btnActionDoc:(UIButton *)sender;
@end
