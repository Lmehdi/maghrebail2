//
//  MesContratsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MesContratsVC_iphone.h"
#import "AppDelegate.h"

@interface MesContratsVC_iphone ()

@end
CGRect  oldFrameContrat;
@implementation MesContratsVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    int indexOrigine=323;
    NSString* nameImage=@"v3-MesContratsBg.png";
 
    if([DataManager isIphone4]){
        nameImage=@"leasbox-iphone4.png";
    }

    UIImage *image = [ UIImage imageNamed:nameImage];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;
   
    if([DataManager isIphone4]){
        indexOrigine=290;
    }

    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width, indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    
    imgParallaxe.image=[UIImage imageNamed:nameImage];
    
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scollMainContrat];
    self.scollMainContrat.scrollEnabled=NO;
    [self.scollMainContrat setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,284)];
    [self.viewMain addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];
  
    
}

- (void)showPopUp:(NSInteger )index
{/*
    if (!popVC)
    {
        popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
    }
    popVC.index = index;
    [self.view addSubview:popVC.view];*/
    dataManager.handleauthentificationconnect=YES;
    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
    if(!authentificationviewcontroller_iphone)
    {
        authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
        
        
    }
    authentificationviewcontroller_iphone.index=index;
    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];


    
}
- (IBAction)btnActionDoc:(UIButton *)sender {
    dataManager.isClient=YES;
    dataManager.showdemobutton=YES;

    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    switch (sender.tag) {
        case 13:
            [dataManager.slideMenu contrats:YES];
            break;
        case 12:
            [dataManager.slideMenu maLigneCredit:YES];
            break;
        case 21:
            [dataManager.slideMenu mesDocuments:YES];
            break;
        case 15:
            [dataManager.slideMenu mesGaranties:YES];
            break;
            
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
