//
//  EntreprisesVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "EntreprisesVC_ipad.h"
#import "DetailEntrVC.h"
#import "SlideViewController.h"

@interface EntreprisesVC_ipad ()

@end

@implementation EntreprisesVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollContent setContentSize:CGSizeMake(320, 508)];
}

- (IBAction)btnAction:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            
            [self contructviewController:-1 andType:kPRODUITS_EQUIPEMENT];
            
            break;
        case 1:
            
            [self contructviewController:1 andType:kPRODUITS_IMMOBILIER];
            
            break;
        case 2:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showAgence" object:nil];
            break;
        case 3:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showSimulateur" object:nil];
            
        default:
            break;
    }
}

-(void) contructviewController:(int) index andType:(int) type{
    DetailEntrVC* detail=[[DetailEntrVC alloc]  initWithNibName:@"DetailEntrVC" bundle:nil];
    detail.indexPage=index;
    detail.type=type;
    [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.4];
    [self.viewContent addSubview:detail.view];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
