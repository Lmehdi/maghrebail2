//
//  EntreprisesVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "EntreprisesVC_iphone.h"
#import "SlideViewController.h"

@interface EntreprisesVC_iphone ()

@end

@implementation EntreprisesVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}
- (IBAction)btnAction:(UIButton *)sender {
    
    [dataManager.slideMenu detailView:YES andParallaxe:YES andIndex:(int)sender.tag];
    
    switch (sender.tag) {
        case 0:
            [dataManager.slideMenu detailView:YES andParallaxe:YES andIndex:3];
            break;
            
        case 1:
            [dataManager.slideMenu detailView:YES andParallaxe:YES andIndex:2];
            break;
            
        case 2:
            [dataManager.slideMenu agences:YES];
            break;
        case 3:
            [dataManager.slideMenu simulator:YES];
            
        default:
            break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollContent setContentSize:CGSizeMake(320, 508)];
     self.viewHeader.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
  // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStatusBarFrameChanged) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];

    
}
/*
- (void) didStatusBarFrameChanged {
   
    CGRect statusBar = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"height = %f", statusBar.size.height);

    int newY   =   self.view.frame.size.height -self.view_agence.frame.size.height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.scrollContent setFrame:CGRectMake(self.scrollContent.frame.origin.x, self.scrollContent.frame.origin.y + statusBar.size.height, self.scrollContent.frame.size.width, newY)];
                         
                         
                     }];
}
*/
/*

-(void)dealloc
{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];



}
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
