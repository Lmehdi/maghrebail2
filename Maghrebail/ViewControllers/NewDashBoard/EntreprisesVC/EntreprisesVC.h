//
//  EntreprisesVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface EntreprisesVC : BaseViewController

- (IBAction)btnAction:(UIButton *)sender;

@property (retain, nonatomic) IBOutlet UIView *viewContent;
@property (retain, nonatomic) IBOutlet UIView *view_agence;
@property (retain, nonatomic) IBOutlet UIView *view_simulator;

@property (retain, nonatomic) IBOutlet UIView *viewHeader;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollContent;
@end
