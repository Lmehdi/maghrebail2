//
//  DetailEntrVC.m
//  Maghrebail
//
//  Created by Chaouki on 12/03/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailEntrVC.h"
#import "ProduitViewController_iphone.h"

@interface DetailEntrVC ()

@end

@implementation DetailEntrVC
@synthesize indexPage,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.indexPage=self.type=0;
        shouldAddHeader=NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self contructView];
}
-(void) contructView{
    
    
    NSString* nibName=@"ProduitVC_iphone5_p";
    
    ProduitViewController_iphone* produitViewControllerEquipement = [[ProduitViewController_iphone alloc] initWithNibName:nibName bundle:nil];
    produitViewControllerEquipement.index =self.indexPage;
    produitViewControllerEquipement.showBtnBack=true;
    produitViewControllerEquipement.withParallaxe=NO;
    produitViewControllerEquipement.titreHeader=@"";
    produitViewControllerEquipement.imageParallaxe=@"v3-imageEquipementEntreprise.png";
    
    produitViewControllerEquipement.isFromDash = YES;
    [produitViewControllerEquipement setTypeProduit:self.type];
    [self.viewContent addSubview:produitViewControllerEquipement.view];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_viewContent release];
    [super dealloc];
}
- (IBAction)btnExit:(UIButton *)sender {
    [self.view removeFromSuperview];
}
@end
