//
//  DetailEntrVC.h
//  Maghrebail
//
//  Created by Chaouki on 12/03/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailEntrVC : BaseViewController

@property (assign) int indexPage;
@property (assign) int type;

@property (retain, nonatomic) IBOutlet UIView *viewContent;
- (IBAction)btnExit:(UIButton *)sender;
@end
