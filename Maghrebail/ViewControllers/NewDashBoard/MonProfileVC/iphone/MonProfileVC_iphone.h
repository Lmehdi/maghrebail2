//
//  MonProfileVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MonProfileVC.h"
#import "PopUpDashVC_iphone.h"
#import "AuthentificationViewController_iphone.h"
#import "SlideViewController_iphone.h"
@interface MonProfileVC_iphone : MonProfileVC
{
    PopUpDashVC_iphone *popVC;
    AuthentificationViewController_iphone *authentificationviewcontroller_iphone;
    SlideViewController_iphone *slideview;
}


@end
