//
//  MonProfileVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MonProfileVC_iphone.h"
#import "AppDelegate.h"
#import "SlideViewController_iphone.h"

@interface MonProfileVC_iphone ()

@end

@implementation MonProfileVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(dataManager.isFournisseurContent)
    {
        self.viewreclamation.hidden=YES;
    }
    int indexOrigine=323;
    
    UIImage *image = [ UIImage imageNamed: @"v3-mon_profil.png" ];
    indexOrigine=(mainScreen_width*image.size.height)/image.size.width;
    
    if([DataManager isIphone4]){
       indexOrigine=290;
    }
    
    
    
    UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, indexOrigine)];
    imgParallaxe.backgroundColor=[UIColor clearColor];
    imgParallaxe.image=[UIImage imageNamed:@"v3-mon_profil.png"];
  
    if (!self.strechy) {
        self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
    }
    
    [self.strechy addSubview:self.scrollAccess];
     self.scrollAccess.scrollEnabled=NO;
    [self.scrollAccess setFrame:CGRectMake(0,indexOrigine-100,mainScreen_width,232)];
    [self.viewPrincipale addSubview:self.strechy];
    [self.strechy setContentSize:CGSizeMake(mainScreen_width,mainScreen_height+50)];
    
}
- (IBAction)btnActionProfile:(UIButton *)sender {
    dataManager.showdemobutton=YES;

    if (!dataManager.authentificationData )
    {
        if(sender.tag==16)
        {
            dataManager.isClient=YES;
        }
        
        [self showPopUp:sender.tag];
        return;
    }
    
    switch (sender.tag) {
        case 18:
            [dataManager.slideMenu maMessagerie:YES];
            break;
        case 20:
            [dataManager.slideMenu mesContacts:YES];
            break;
        case 16:
            [dataManager.slideMenu mesReclamations:YES];
            break;
            
        default:
            break;
    }
}
- (void)showPopUp:(NSInteger )index
{/*
    if (!popVC)
    {
        popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
    }
    
    //popVC.parent = self;
    popVC.index = index;
    [self.view addSubview:popVC.view];
  
  */
 //   AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
  
    
   // authentificationviewcontroller_iphone.index=index;
    
 //   [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];
/*    if(!slideview)
    {
    
     slideview = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
    
    }
    authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    authentificationviewcontroller_iphone.index=index;
     authentificationviewcontroller_iphone.delegate = slideview;
    NSArray *controllers = [NSArray arrayWithObject:authentificationviewcontroller_iphone];
    slideview.sideMenu.navigationController.viewControllers = controllers;
    [slideview.sideMenu setMenuState:MFSideMenuStateClosed];

*/
    dataManager.handleauthentificationconnect=YES;

    
    AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
    authentificationviewcontroller_iphone = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    
    authentificationviewcontroller_iphone.index=index;
    [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_iphone animated:YES];

    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
