//
//  MonProfileVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MonProfileVC_ipad.h"
#import "AppDelegate.h"
@interface MonProfileVC_ipad ()

@end

@implementation MonProfileVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (IBAction)btnActionProfile:(UIButton *)sender {
    dataManager.showdemobutton=YES;
    if (!dataManager.authentificationData )
    {
        [self showPopUp:sender.tag];
        return;
    }
    switch (sender.tag) {
        case 18:
             [[NSNotificationCenter defaultCenter] postNotificationName:@"showMessagerie" object:nil];
            break;
        case 20:
              [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesContact" object:nil];
            break;
        case 16:
           [[NSNotificationCenter defaultCenter] postNotificationName:@"showMesReclamations" object:nil];
            break;
            
        default:
            break;
    }
}
- (void)showPopUp:(NSInteger )index
{
  /*  if (!popVC)
    {
        popVC = [[PopUpDashVC_ipad alloc] initWithNibName:@"PopUpDashVC_ipad" bundle:nil];
    }
    
    popVC.index = index;
    
    [self.view addSubview:popVC.view];*/
    
 AppDelegate *mainDelegate       =   (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // authentificationviewcontroller_ipad = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
  //  [mainDelegate.window.rootViewController pushViewController:authentificationviewcontroller_ipad animated:YES];
    
    
    if (!authentificationViewController)
    {
        
        if (authentificationViewController)
        {
            authentificationViewController = nil;
            [authentificationViewController release];
        }
        authentificationViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
        // authentificationViewController.delegate = self;
        
    }
    
    
    authentificationViewController.isFromDash = NO;
    authentificationViewController.index=index;
    // mainDelegate.m_SideMenu
    UINavigationController *navigationController = mainDelegate.m_SideMenu.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:authentificationViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:CHMFSideMenuStateClosed];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
