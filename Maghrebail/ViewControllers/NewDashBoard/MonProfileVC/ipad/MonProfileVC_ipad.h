//
//  MonProfileVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MonProfileVC.h"
#import "PopUpDashVC_ipad.h"
#import "AuthentificationViewController_ipad.h"

@interface MonProfileVC_ipad: MonProfileVC
{
    PopUpDashVC_ipad *popVC;
    AuthentificationViewController_ipad *authentificationviewcontroller_ipad;
    AuthentificationViewController *authentificationViewController;
}


@end
