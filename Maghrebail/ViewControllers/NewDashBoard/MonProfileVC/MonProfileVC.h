//
//  MonProfileVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "StrechyParallaxScrollView.h"


@interface MonProfileVC : BaseViewController

@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
@property (retain, nonatomic) IBOutlet UIView *viewPrincipale;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollAccess;
- (IBAction)btnActionProfile:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIView *viewreclamation;


@end
