//
//  ProfessionnelsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "ProfessionnelsVC_iphone.h"
#import "SlideViewController.h"

@interface ProfessionnelsVC_iphone ()

@end

@implementation ProfessionnelsVC_iphone

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollContent setContentSize:CGSizeMake(320, 508)];
     self.vewHeader.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
}
- (IBAction)btnActionBtns:(UIButton *)sender {
    
    switch (sender.tag) {
        case 0:
            [dataManager.slideMenu detailView:YES andParallaxe:YES andIndex:0];
            break;
        case 1:
            [dataManager.slideMenu detailView:YES andParallaxe:YES andIndex:1];
            break;
            
        case 2:
            [dataManager.slideMenu agences:YES];
            break;
        case 3:
            [dataManager.slideMenu simulator:YES];
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
