//
//  ProfessionnelsVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface ProfessionnelsVC : BaseViewController

- (IBAction)btnActionBtns:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIView *vewHeader;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollContent;
@property (retain, nonatomic) IBOutlet UIView *viewContent;

@end
