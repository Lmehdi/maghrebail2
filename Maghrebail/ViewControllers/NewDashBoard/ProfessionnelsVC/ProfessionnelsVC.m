//
//  ProfessionnelsVC.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "ProfessionnelsVC.h"

#import "DetailsProfessionnelsVC_iphone.h"

@interface ProfessionnelsVC ()

@end

@implementation ProfessionnelsVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldAddHeader = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)btnActionBtns:(UIButton *)sender {
    
}
- (void)dealloc {
    [_vewHeader release];
    [_scrollContent release];
    [_viewContent release];
    [super dealloc];
}

@end
