//
//  DetailProfVC.h
//  Maghrebail
//
//  Created by Chaouki on 06/03/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailProfVC : BaseViewController

@property (assign) int indexPage;
@property (assign) int type;


@property (retain, nonatomic) IBOutlet UIView *viewContent;

- (IBAction)btnExit:(UIButton *)sender;
@end
