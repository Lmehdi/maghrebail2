//
//  DetailProfVC.m
//  Maghrebail
//
//  Created by Chaouki on 06/03/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailProfVC.h"
#import "DetailsProfessionnelsVC_iphone.h"
@interface DetailProfVC ()

@end

@implementation DetailProfVC
@synthesize indexPage,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.indexPage=self.type=0;
        shouldAddHeader=NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self contructView];
}
-(void) contructView{

    ProduitViewController_iphone *ProEquipVC=[[ProduitViewController_iphone alloc] initWithNibName:@"ProduitVC_iphone5_p" bundle:nil];
    ProEquipVC.showBtnBack=true;
    ProEquipVC.withParallaxe=NO;
    ProEquipVC.index = self.indexPage;
    ProEquipVC.titreHeader=@"";
    ProEquipVC.isFromDash = YES;
    [ProEquipVC setTypeProduit:self.type];
    [self.viewContent addSubview:ProEquipVC.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnExit:(UIButton *)sender{
    [self.view removeFromSuperview];
}


@end
