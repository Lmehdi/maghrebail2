//
//  ProfessionnelsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 06/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "ProfessionnelsVC_ipad.h"
#import "DetailProfVC.h"
#import "SlideViewController_ipad.h"
#import "AgenceViewController_ipad.h"
#import "AppDelegate.h"

@interface ProfessionnelsVC_ipad ()

@end

@implementation ProfessionnelsVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)btnActionBtns:(UIButton *)sender {
    
    switch (sender.tag) {
        case 0:
            
            [self contructviewController:2 andType:kPRODUITS_EQUIPEMENT];
            break;
        case 1:
            
            [self contructviewController:2 andType:kPRODUITS_IMMOBILIER];
            break;
            
        case 2:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showAgence" object:nil];
            break;
        case 3:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showSimulateur" object:nil];
            break;
            
        default:
            break;
    }
}

-(void) contructviewController:(int) index andType:(int) type{
    DetailProfVC* detail=[[DetailProfVC alloc]  initWithNibName:@"DetailProfVC" bundle:nil];
    detail.indexPage=index;
    detail.type=type;
    [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.4];
    [self.viewContent addSubview:detail.view];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
