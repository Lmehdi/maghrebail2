//
//  DetailsProfessionnelsVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailsProfessionnelsVC.h"

@interface DetailsProfessionnelsVC_iphone : DetailsProfessionnelsVC

@property(nonatomic,retain) ProduitViewController_iphone* ProImmoVC;
@property(nonatomic,retain) ProduitViewController_iphone* ProEquipVC;
@property(nonatomic,retain) ProduitViewController_iphone* EtrImmoVC;
@property(nonatomic,retain) ProduitViewController_iphone* EtrEquipVC;

-(void) changeScrollPosition;
@end
