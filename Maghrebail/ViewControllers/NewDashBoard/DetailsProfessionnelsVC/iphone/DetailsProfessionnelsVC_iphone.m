//
//  DetailsProfessionnelsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailsProfessionnelsVC_iphone.h"

@interface DetailsProfessionnelsVC_iphone ()

@end

@implementation DetailsProfessionnelsVC_iphone
@synthesize ProEquipVC,ProImmoVC,EtrEquipVC,EtrImmoVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self constructViewControllers];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self.scrollPrincipale setContentOffset:CGPointMake(self.index*mainScreen_width,0)];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}
-(void) changeScrollPosition{
     [self.scrollPrincipale setContentOffset:CGPointMake(self.index*mainScreen_width,0)];
}
-(void) constructViewControllers{
    
    CGFloat offSet =0;
    
    if (!self.ProEquipVC) {
        self.ProEquipVC=[[ProduitViewController_iphone alloc] initWithNibName:@"ProduitVC_iphone" bundle:nil];
    }
    self.ProEquipVC.showBtnBack=true;
    self.ProEquipVC.withParallaxe=YES;
    self.ProEquipVC.index = 2;
    self.ProEquipVC.titreHeader=@"Professionnels";
    self.ProEquipVC.isFromDash = YES;
    [self.ProEquipVC setTypeProduit:kPRODUITS_EQUIPEMENT];
    
    [self.ProEquipVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, 568)];
    offSet+=mainScreen_width;
    [self.scrollPrincipale addSubview:self.ProEquipVC.view];
    
    if (!self.ProImmoVC) {
        self.ProImmoVC=[[ProduitViewController_iphone alloc] initWithNibName:@"ProduitVC_iphone" bundle:nil];
    }
    
     self.ProImmoVC.showBtnBack=true;
    self.ProImmoVC.withParallaxe=YES;
    self.ProImmoVC.titreHeader=@"Professionnels";
    self.ProImmoVC.imageParallaxe=@"v3-imageImmobilierPro.png";
    self.ProImmoVC.index = 2;
    self.ProImmoVC.isFromDash = YES;
    [self.ProImmoVC setTypeProduit:kPRODUITS_IMMOBILIER];
    [self.ProImmoVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, 568)];
    offSet+=mainScreen_width;
    [self.scrollPrincipale addSubview:self.ProImmoVC.view];
    
    if (!self.EtrImmoVC) {
        self.EtrImmoVC=[[ProduitViewController_iphone alloc] initWithNibName:@"ProduitVC_iphone" bundle:nil];
    }
    
    self.EtrImmoVC.showBtnBack=true;
    self.EtrImmoVC.index = 1;
    self.EtrImmoVC.withParallaxe=YES;
    self.EtrImmoVC.titreHeader=@"Entreprises";
    self.EtrImmoVC.imageParallaxe=@"v3-imageImmobilierEntreprise.png";
    
    self.EtrImmoVC.isFromDash = YES;
    [self.EtrImmoVC setTypeProduit:kPRODUITS_IMMOBILIER];
    
    [self.EtrImmoVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, 568)];
    offSet+=mainScreen_width;
    [self.scrollPrincipale addSubview:self.EtrImmoVC.view];
    
    if (!self.EtrEquipVC) {
        self.EtrEquipVC=[[ProduitViewController_iphone alloc] initWithNibName:@"ProduitVC_iphone" bundle:nil];
    }
    self.EtrEquipVC.showBtnBack=true;
    self.EtrEquipVC.index = -1;
    self.EtrEquipVC.withParallaxe=YES;
    self.EtrEquipVC.titreHeader=@"Entreprises";
    self.EtrEquipVC.imageParallaxe=@"v3-imageEquipementEntreprise.png";
    
     self.EtrEquipVC.isFromDash = YES;
    [self.EtrEquipVC setTypeProduit:kPRODUITS_EQUIPEMENT];
    [self.EtrEquipVC.view setFrame:CGRectMake(offSet,0, mainScreen_width, 568)];
     offSet+=mainScreen_width;
    [self.scrollPrincipale addSubview:self.EtrEquipVC.view];
    
    [self.scrollPrincipale setContentSize:CGSizeMake(mainScreen_width*4, 400)];
}

#pragma mark --
#pragma mark -- Scroll Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGFloat pageWidth = 320;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth)+1;
    self.pageIndicator.currentPage = page;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
