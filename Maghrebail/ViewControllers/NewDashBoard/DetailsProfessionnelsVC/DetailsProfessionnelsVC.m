//
//  DetailsProfessionnelsVC.m
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailsProfessionnelsVC.h"
#import "ProduitViewController_iphone.h"

@interface DetailsProfessionnelsVC ()

@end

@implementation DetailsProfessionnelsVC

@synthesize index;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.index=0;
        shouldAddHeader = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_scrollPrincipale release];
    [_pageIndicator release];
    [super dealloc];
}
- (IBAction)btnBack:(UIButton *)sender {
      [self showSideMenu];
}
@end
