//
//  DetailsProfessionnelsVC_iphone.m
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailsProfessionnelsVC_ipad.h"
#define screen_width 1024

@interface DetailsProfessionnelsVC_ipad ()

@end

@implementation DetailsProfessionnelsVC_ipad

- (void)viewDidLoad {
    [super viewDidLoad];
    [self constructViewControllers];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self.scrollPrincipale setContentOffset:CGPointMake(self.index*screen_width,0)];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

-(void) changeScrollPosition{
     [self.scrollPrincipale setContentOffset:CGPointMake(self.index*screen_width,0)];
}

-(void) constructViewControllers{
    
    CGFloat offSet =0;
    
    if (!self.ProEquipVC) {
        self.ProEquipVC=[[ProduitViewController_ipad alloc] initWithNibName:@"ProduitVC_ipad" bundle:nil];
    }
    self.ProEquipVC.withParallaxe=YES;
    self.ProEquipVC.index = 2;
    self.ProEquipVC.titreHeader=@"Professionnels-Equipements";
    self.ProEquipVC.nameImage= @"ipad-v3-image1.jpg";
    self.ProEquipVC.isFromDash = YES;
    [self.ProEquipVC setTypeProduit:kPRODUITS_EQUIPEMENT];

    [self.ProEquipVC.view setFrame:CGRectMake(offSet,0, screen_width, 568)];
    offSet+=screen_width;
    
    if (!self.ProImmoVC) {
        self.ProImmoVC=[[ProduitViewController_ipad alloc] initWithNibName:@"ProduitVC_ipad" bundle:nil];
    }
    
    self.ProImmoVC.withParallaxe=YES;
    self.ProImmoVC.titreHeader=@"Professionnels-Immobilier";
    self.ProImmoVC.nameImage= @"ipad-v3-image2.jpg";
    self.ProImmoVC.index = 2;
    self.ProImmoVC.isFromDash = YES;
    [self.ProImmoVC setTypeProduit:kPRODUITS_IMMOBILIER];
    [self.ProImmoVC.view setFrame:CGRectMake(offSet,0, screen_width, 568)];
    offSet+=screen_width;
    
    
    if (!self.EtrEquipVC) {
        self.EtrEquipVC=[[ProduitViewController_ipad alloc] initWithNibName:@"ProduitVC_ipad" bundle:nil];
    }
    
    self.EtrEquipVC.index = -1;
    self.EtrEquipVC.withParallaxe=YES;
    self.EtrEquipVC.titreHeader=@"Entreprises-Equipements";
    self.EtrEquipVC.nameImage= @"ipad-v3-image3.jpg";
    
    self.EtrEquipVC.isFromDash = YES;
    [self.EtrEquipVC setTypeProduit:kPRODUITS_EQUIPEMENT];
    [self.EtrEquipVC.view setFrame:CGRectMake(offSet,0, screen_width, 568)];
    offSet+=screen_width;

    if (!self.EtrImmoVC) {
        self.EtrImmoVC=[[ProduitViewController_ipad alloc] initWithNibName:@"ProduitVC_ipad" bundle:nil];
    }
    
    self.EtrImmoVC.index = 1;
    self.EtrImmoVC.withParallaxe=YES;
    self.EtrImmoVC.titreHeader=@"Entreprises-Immobilier";
    self.EtrImmoVC.nameImage= @"ipad-v3-image4.png";
    
    self.EtrImmoVC.isFromDash = YES;
    [self.EtrImmoVC setTypeProduit:kPRODUITS_IMMOBILIER];
    
    [self.EtrImmoVC.view setFrame:CGRectMake(offSet,0, screen_width, 568)];
    offSet+=screen_width;
   
    [self.scrollPrincipale addSubview:self.EtrImmoVC.view];
    [self.scrollPrincipale addSubview:self.EtrEquipVC.view];
    [self.scrollPrincipale addSubview:self.ProImmoVC.view];
    [self.scrollPrincipale addSubview:self.ProEquipVC.view];
    
    [self.scrollPrincipale setContentSize:CGSizeMake(screen_width*4, 400)];
}

#pragma mark --
#pragma mark -- Scroll Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGFloat pageWidth = screen_width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth)+1;
    self.pageIndicator.currentPage = page;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
