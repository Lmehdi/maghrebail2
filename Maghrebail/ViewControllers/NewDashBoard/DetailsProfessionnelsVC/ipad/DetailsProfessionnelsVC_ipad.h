//
//  DetailsProfessionnelsVC_iphone.h
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "DetailsProfessionnelsVC.h"

@interface DetailsProfessionnelsVC_ipad : DetailsProfessionnelsVC

@property(nonatomic,retain) ProduitViewController_ipad* ProImmoVC;
@property(nonatomic,retain) ProduitViewController_ipad* ProEquipVC;
@property(nonatomic,retain) ProduitViewController_ipad* EtrImmoVC;
@property(nonatomic,retain) ProduitViewController_ipad* EtrEquipVC;

-(void) changeScrollPosition;
@end
