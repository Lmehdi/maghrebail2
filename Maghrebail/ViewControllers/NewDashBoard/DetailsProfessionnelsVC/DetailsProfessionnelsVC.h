//
//  DetailsProfessionnelsVC.h
//  Maghrebail
//
//  Created by Chaouki on 07/01/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "ProduitViewController_iphone.h"
#import "ProduitViewController_ipad.h"

@interface DetailsProfessionnelsVC : BaseViewController<UIScrollViewDelegate>

- (IBAction)btnBack:(UIButton *)sender;

@property(assign) int index;



@property (retain, nonatomic) IBOutlet UIPageControl *pageIndicator;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollPrincipale;

@end
