//
//  MesEngagementsViewCell.h
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MesEngagementsViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *numcommande;
@property (retain, nonatomic) IBOutlet UILabel *client;
@property (retain, nonatomic) IBOutlet UILabel *sujet;
@property (retain, nonatomic) IBOutlet UILabel *datecommande;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;

@end
