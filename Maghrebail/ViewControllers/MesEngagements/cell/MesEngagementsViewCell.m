//
//  MesEngagementsViewCell.m
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesEngagementsViewCell.h"

@implementation MesEngagementsViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_flecheimage release];
    [super dealloc];
}
@end
