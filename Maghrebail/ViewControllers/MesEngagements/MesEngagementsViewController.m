//
//  MesEngagementsViewController.m
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesEngagementsViewController.h"

@interface MesEngagementsViewController ()

@end

@implementation MesEngagementsViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	

	
    [Flurry logEvent:@"MesEngagements"];
    [self initHeaderColor];
}

//-(void) readFromJsonFile{
//	NSDictionary *dict = [DataManager JSONFromFile];
//
//	NSArray *colours = [dict objectForKey:@"colors"];
//
//	for (NSDictionary *colour in colours) {
//		NSString *name = [colour objectForKey:@"name"];
//		NSLog(@"Colour name: %@", name);
//
//		if ([name isEqualToString:@"green"]) {
//			NSArray *pictures = [colour objectForKey:@"pictures"];
//			for (NSDictionary *picture in pictures) {
//				NSString *pictureName = [picture objectForKey:@"name"];
//				NSLog(@"Picture name: %@", pictureName);
//			}
//		}
//	}
//}


-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES ENGAGEMENTS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    


    
    if (self.isFromButton || dataManager.isLoad)
    {
        dataManager.isLoad = NO;
	
        
        [self.response removeAllObjects];
        [_tableView reloadData];
        
        //  NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
        /* if (dataManager.isDemo)
         {
         params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
         }
         */
        if(dataManager.isDemo)
        {
            [self getRemoteContentAuth:ENGAGEMENTs_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];

        }
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/engagements/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
        }
    
        
    
        
    }
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
}

-(void)updateViewTag:(int)inputTag{
  //  [super updateView];
    self.index=inputTag;
	
	

    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    
    
    
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.dataengagement removeAllObjects];
        dataManager.dataengagement = [NSMutableArray arrayWithArray:reponse];
        // dataManager.commandeFiltred = (NSMutableArray *)[dataManager.commandes copy];
        
       [_tableView reloadData];
    }
    
    
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        return [[self.response objectForKey:@"response"] count];
        
    }
    else
    {
        return 0;
    }
}

#pragma mark --
#pragma mark Pull to refresh

-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    if(dataManager.isDemo)
    {
        //[self getRemoteContentAuth:ENGAGEMENTs_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
        [self getRemoteContentAuth:ENGAGEMENTs_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
    }
    else
    {
    [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/engagements/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
    }
    
    
    
    
    
    
    
}


-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    
    
    if (!isLandscape)
    {
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
    }
    
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    [_tableView addSubview:_refreshHeaderView];
    [_refreshHeaderView refreshLastUpdatedDate];
}




@end
