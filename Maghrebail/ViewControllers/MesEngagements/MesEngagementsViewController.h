//
//  MesEngagementsViewController.h
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MesEngagementsViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (assign, nonatomic)  int index;
@property(assign,nonatomic) NSDictionary *mMesEngagementDemoDic;
-(void) initHeaderColor;
@end
