//
//  MesEngagementsViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesEngagementsViewController_iphone.h"
#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@interface MesEngagementsViewController_iphone ()

@end

@implementation MesEngagementsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    if (isLandscape) {
        
        static NSString *cellIdentifier_l;
      /*
        if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"MesEngagementsViewCell_iphone5_l";
        }
        else
            cellIdentifier_l = @"MesEngagementsViewCell_iphone_l";
        */
        
        cellIdentifier_l = @"MesEngagementsViewCell_iphone_l";

        MesEngagementsViewCell *cell = (MesEngagementsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MesEngagementsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.client.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"RS"]];
    cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"COMMANDE"];
        cell.datecommande.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_CREA"]];
        cell.sujet.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"] ;
        
        
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }else{
        
      // float cellclientheight= [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"RS"]];
        
        static NSString *cellIdentifier_p = @"MesEngagementsViewCell_iphone_p";
        
        MesEngagementsViewCell *cell = (MesEngagementsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell = (MesEngagementsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
       
        cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"COMMANDE"];
        cell.client.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"RS"]];
        cell.datecommande.text = [DataManager getDateStringFromPresentableDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_CREA"]];

        cell.alpha = 0.5;
        
       
    
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isLandscape)
    {
	
	if([dataManager isDemo]) {
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
																	   message:@"Vous n'avez aucun détail dans cette rubrique."
																preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
															  handler:^(UIAlertAction * action) {}];
		
		[alert addAction:defaultAction];
		[self presentViewController:alert animated:YES completion:nil];
	}
	else {
		self.isFromButton = NO;
		dataManager.isdataimmatriculation=NO;
		dataManager.isdatacommande=NO;
		dataManager.isdataengagement=YES;
		dataManager.isdetailscontact=NO;
		dataManager.isdatafacture=NO;
		
		ListCommandesViewController_iphone * listCommandesViewController_iphone = [[ListCommandesViewController_iphone alloc]initWithNibName:@"ListCommandesViewController_iphone" bundle:nil];
		
		
		listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"COMMANDE"];
		[self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
	}

	
    }
    

    
}
@end
