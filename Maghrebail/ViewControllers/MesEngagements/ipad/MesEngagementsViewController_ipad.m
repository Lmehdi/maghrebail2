//
//  MesEngagementsViewController_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 21/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesEngagementsViewController_ipad.h"
#import "MesEngagementsViewCell.h"
#import "ListCommandesViewController_ipad.h"
@interface MesEngagementsViewController_ipad ()

@end

@implementation MesEngagementsViewController_ipad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    static NSString *cellIdentifier_l;
    /*
     if ([DataManager isIphone5LandScape])
     {
     cellIdentifier_l = @"MesEngagementsViewCell_iphone5_l";
     }
     else
     cellIdentifier_l = @"MesEngagementsViewCell_iphone_l";
     */
    
    cellIdentifier_l = @"MesEngagementsViewCell_ipad";
    
    MesEngagementsViewCell *cell = (MesEngagementsViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
    if (cell == nil)
    {
        cell = (MesEngagementsViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
    }
    
    cell.client.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"RS"]];
    cell.numcommande.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"COMMANDE"];
    cell.datecommande.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_CREA"]];
    cell.sujet.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"] ;
    
    
    cell.alpha = 0.5;
    
    switch (indexPath.row % 2) {
        case 0:
            
            
            cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
            
            break;
        case 1:
            cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    self.isFromButton = NO;
    dataManager.isdataimmatriculation=NO;
    dataManager.isdatacommande=NO;
    dataManager.isdataengagement=YES;
    dataManager.isdetailscontact=NO;
    dataManager.isdatafacture=NO;
    
    ListCommandesViewController_ipad * listCommandesViewController_iphone = [[ListCommandesViewController_ipad alloc]initWithNibName:@"ListCommandesViewController_ipad" bundle:nil];
    
    
    listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"COMMANDE"];
    [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
    
}
@end
