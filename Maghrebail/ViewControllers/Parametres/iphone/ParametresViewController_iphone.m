//
//  ParametresViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 03/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ParametresViewController_iphone.h"

@interface ParametresViewController_iphone ()

@end

@implementation ParametresViewController_iphone
@synthesize supportTouchID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    headerView.labelheaderTitre.text=@"PARAMETRES";
	
	if ([DataManager isIphoneX]) {
		self.faceTouchIdLabel.text = @"Associer votre Face ID";
	}
	else {
		self.faceTouchIdLabel.text = @"Associer votre Touch ID";
	}
	
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self intialization];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_faceTouchIdLabel release];
    [super dealloc];
}
@end
