//
//  ParametresViewController.h
//  Maghrebail
//
//  Created by MAC on 03/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "EHFAuthenticator.h"

@interface ParametresViewController : BaseViewController

@property(assign) BOOL supportTouchID;

@property (retain, nonatomic) NSString* typeColor;
@property (retain, nonatomic) IBOutlet UISwitch *btnSwitch;
@property (retain, nonatomic) IBOutlet UIView *viewTouchID;
@property (retain, nonatomic) IBOutlet UISwitch *btnSwitchTouchID;

- (void) intialization;
- (IBAction)switchChanged:(id)sender;
- (IBAction)switchTouchID:(UISwitch *)sender;

@end
