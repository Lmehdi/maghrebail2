//
//  ParametresViewController.m
//  Maghrebail
//
//  Created by MAC on 03/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ParametresViewController.h"

@interface ParametresViewController ()

@end

@implementation ParametresViewController
@synthesize typeColor;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.typeColor=@"";
        self.supportTouchID=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeHeaderColor];
}
-(void) initializeHeaderColor{
    
    headerView.backgroundColor=[DataManager getColorFromType:dataManager.typeColor];
    self.view.backgroundColor=[DataManager getColorFromType:dataManager.typeColor];
    
}
-(void) intialization{
    
    if (!self.supportTouchID) {
        [self.viewTouchID setHidden:true];
		if ([DataManager isIphoneX]) {
			[self.viewTouchID setHidden:false];
		}
    }else{
        [self.viewTouchID setHidden:false];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self intialization];
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	
	if (headerView)
		[headerView.btnRightHeader setHidden:YES];
	 
	if (dataManager.isStayOn)
	{ 
		[self.btnSwitch setOn:NO];
	}
	else
	{
		[self.btnSwitch setOn:YES];
	}
    
    [self authentificationSet];
    [self initBtnAssocier];
    [self intialization];

    
}
-(void) initBtnAssocier{
    
    // Associé le Touch ID
    if ((![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"])|| (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"]) ) {
        [self.btnSwitchTouchID setOn:NO];
        return;
    }
    
    
    
    if (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"]) || ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"no"])) {

        [self.btnSwitchTouchID setOn:NO];
        return;
    }
    // Dissocié le Touch ID
    if (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) ||  ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"yes"])  ) {
        [self.btnSwitchTouchID setOn:YES];
        return;
    }
}
- (void)goBack
{
    
	[self.navigationController popViewControllerAnimated:YES];
    
}
  
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
	[_btnSwitch release];
    [_viewTouchID release];
    [_btnSwitchTouchID release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setBtnSwitch:nil];
	[super viewDidUnload];
}


- (IBAction)switchChanged:(id)sender
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if ([self.btnSwitch isOn])
	{
		if (standardUserDefaults)
		{
			[standardUserDefaults setObject:@"OFF" forKey:@"USER-CONNEXION"];
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationClient"];
            [standardUserDefaults setBool:NO forKey:@"DataAuthentificationFournisseur"];
            [standardUserDefaults synchronize];
			dataManager.isStayOn = NO;
		} 
	}
	else  
	{
		if (standardUserDefaults) {
			[standardUserDefaults setObject:@"ON" forKey:@"USER-CONNEXION"];
			[standardUserDefaults synchronize];
			dataManager.isStayOn = YES;
		}
	}

}
- (IBAction)switchTouchID:(UISwitch *)sender {
	
	NSString* messageAlert = @"" ;
		
		if (![self.btnSwitchTouchID isOn])
			{
			
			if (([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"yes"]) || ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"yes"])) {
				
				[[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"TouchIDLinked"];
				[[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"TouchIDLinkedFournisseur"];
				
				[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDLogin"];
				[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDPassword"];
				if ([DataManager isIphoneX]) {
					messageAlert = kCompteFaceDissocie ;
				}
				else {
					messageAlert = kCompteDissocie;
				}
				UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:messageAlert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alertView show];
				[alertView release];
				return;
			}
			
			
			
			}else{
				
				if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"])
					{
					
					[[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
					
					if ([DataManager isIphoneX]) {
						messageAlert = kCompteFAceAssocie ;
					}
					else {
						messageAlert = kCompteAssocie ;
					}
					UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"" message:messageAlert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alertView show];
					[alertView release];
					return;
					}
				if (![[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"])
					{
					
					[[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinkedFournisseur"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
					if ([DataManager isIphoneX]) {
						messageAlert = kCompteFAceAssocie ;
					}
					else {
						messageAlert = kCompteAssocie ;
					}
					UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"" message:messageAlert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alertView show];
					[alertView release];
					return;
					}
				
				if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinked"] isEqualToString:@"no"]) {
					
					[[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinked"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
					if ([DataManager isIphoneX]) {
						messageAlert = kCompteFAceAssocie ;
					}
					else {
						messageAlert = kCompteAssocie ;
					}
					UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:messageAlert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alertView show];
					[alertView release];
					return;
				}
				if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIDLinkedFournisseur"] isEqualToString:@"no"]) {
					
					[[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"TouchIDLinkedFournisseur"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] forKey:@"TouchIDLogin"];
					[[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] forKey:@"TouchIDPassword"];
					if ([DataManager isIphoneX]) {
						messageAlert = kCompteFAceAssocie ;
					}
					else {
						messageAlert = kCompteAssocie ;
					}
					UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:messageAlert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alertView show];
					[alertView release];
					return;
				}
				
				
			}
	
}

-(void) authentificationSet{
    [[EHFAuthenticator sharedInstance] setReason:@"Vérifier votre Touch ID"];
    [[EHFAuthenticator sharedInstance] setUseDefaultFallbackTitle:YES];
    NSError * error = nil;
    if (![EHFAuthenticator canAuthenticateWithError:&error]) {
        NSString * authErrorString = @"Check your Touch ID Settings.";
        self.supportTouchID=NO;
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = @"No Touch ID fingers enrolled.";
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString = @"Touch ID not available on your device.";
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = @"Need a passcode set to use Touch ID.";
                break;
            default:
                authErrorString = @"Vérifier les paramètres du Touch ID.";
                break;
        }
    }
}
@end
