//
//  AdvancedPushSDKViewController.h
//  Mobiblanc
//
//  Created by Anas Bouzoubaa on 27/12/12.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AdvancedPushSDKViewController : UIViewController <CLLocationManagerDelegate>
{
    NSString *posAction;
    NSString *lien;
    NSString *type;

    // IB
    IBOutlet UIImageView *myImage;
    IBOutlet UITextView *myMessage;
    IBOutlet UILabel *myTitle;
    IBOutlet UIImageView *myIcon;
    IBOutlet UIButton *myButton1;
    IBOutlet UIButton *myButton2;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSDictionary *XMLDictionary;
    NSDictionary *lastNotif;
    CLLocationManager *locationManager;
    NSString *token;

}

@property (assign, nonatomic) IBOutlet UIImageView *myImage;
@property (assign, nonatomic) IBOutlet UITextView *myMessage;
@property (assign, nonatomic) IBOutlet UILabel *myTitle;
@property (assign, nonatomic) IBOutlet UIImageView *myIcon;
@property (assign, nonatomic) IBOutlet UIButton *myButton1;
@property (assign, nonatomic) IBOutlet UIButton *myButton2;
@property (assign, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (retain, nonatomic) NSString *lien;
@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *posAction;
@property (nonatomic, retain) NSDictionary *lastNotif;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) NSString *token;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil notif:(NSDictionary *)lastNotification token:(NSString *)deviceToken;

-(IBAction)closeView:(id)sender;
-(IBAction)linkButton:(id)sender;

@end
