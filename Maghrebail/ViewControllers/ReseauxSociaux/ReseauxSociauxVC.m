
//
//  ReseauxSociauxVC.m
//  Maghrebail
//
//  Created by Monassir on 22/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "ReseauxSociauxVC.h"

@interface ReseauxSociauxVC ()

@end

@implementation ReseauxSociauxVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAddHeader = YES;
        shouldAutorate = NO;
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"RÉSEAUX SOCIAUX";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    NSURL *url;
    
    if (self.type == 10)
        url = [NSURL URLWithString:@"https://www.facebook.com/Maghrebail?fref=ts"];
    else if(self.type == 20)
        url = [NSURL URLWithString:@"https://twitter.com/Maghrebail"];
    else
        url = [NSURL URLWithString:@"https://www.youtube.com/user/Maghrebail"];
        
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [self.webView loadRequest:requestObj];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	self.viewChargment.hidden = YES;
}

- (void)webView:(UIWebView *) webView didFailLoadWithError:(NSError *)error
{ 
    
}


- (IBAction)removeView:(id)sender {
    
}
- (void)dealloc {
    [_webView release];
    [_viewChargment release];
    [_indicatorWebView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setWebView:nil];
    [self setViewChargment:nil];
    [super viewDidUnload];
}
@end
