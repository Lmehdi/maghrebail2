//
//  ReseauxSociauxVC_iphone.m
//  Maghrebail
//
//  Created by Monassir on 22/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "ReseauxSociauxVC_iphone.h"

@interface ReseauxSociauxVC_iphone ()

@end

@implementation ReseauxSociauxVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
