//
//  ReseauxSociauxVC_ipad.m
//  Maghrebail
//
//  Created by Monassir on 23/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "ReseauxSociauxVC_ipad.h"

@interface ReseauxSociauxVC_ipad ()

@end

@implementation ReseauxSociauxVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
