//
//  ReseauxSociauxVC.h
//  Maghrebail
//
//  Created by Monassir on 22/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface ReseauxSociauxVC : BaseViewController

@property (retain, nonatomic) IBOutlet UIView *viewChargment;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (assign, nonatomic) NSInteger type;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicatorWebView;
-(void) initHeaderColor;
- (IBAction)removeView:(id)sender;
@end
