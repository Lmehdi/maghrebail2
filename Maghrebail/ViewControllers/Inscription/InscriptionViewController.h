//
//  InscriptionViewController.h
//  Maghrebail
//
//  Created by MAC on 03/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface InscriptionViewController : BaseViewController < MFMailComposeViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
-(IBAction)emailPushed:(id)sender;
-(IBAction)browserPushed:(id)sender;
-(void) initHeaderColor;
@end
