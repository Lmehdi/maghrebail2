//
//  InscriptionViewController.m
//  Maghrebail
//
//  Created by MAC on 03/04/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "InscriptionViewController.h"

@interface InscriptionViewController ()

@end

@implementation InscriptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"INSCRIPTION";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self initHeaderColor];
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    if (dataManager.inscription.count)
    {
        NSDictionary *dic = [dataManager.inscription objectAtIndex:0];
        //<font size="7" face="Georgia, Arial" color="maroon">C</font>
        NSString *html = [NSString stringWithFormat:@"<body style=\"font-family:Arial\">%@</body>",[dic objectForKey:@"description"]];
        //NSString *html = @"Nous vous invitons à télécharger notre formulaire d'inscription à l'ESPACE ABONNES sur notre site web :<a href=\"https://www.maghrebail.ma\">www.maghrebail.ma</a>, le cacheter et le retourner à MAGHREBAIL soit par e-mail à l'adresse <a href=\"mailto:services@maghrebail.ma\">services@maghrebail.ma</a>,soit par courrier avec accusé de reception.Nous restons à votre service,MAGHREBAIL";
      
        //@"Nous vouss invitonss à télécharger notre formulaire d'inscription à l'ESPACE ABONNES sur notre site web : <a href=\"http://www.maghrebail.ma\">Click me!</a> sds sd sds ds ds ds sds ds sds ds <a href=\"mailto://contact@maghrebail.ma\">Click me!</a>";
        
        self.lblTitle.text = [dic objectForKey:@"titre"];
        
        [self.webView loadHTMLString:html baseURL:nil];
    }
    
	// Do any additional setup after loading the view.
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}


#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
    NSString * msgResult= nil ;
    NSString * msgResult2= nil ;
	
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msgResult = @"Envoie annulé";
            msgResult2 = @"Votre avez annulé l'opération et aucun message n'a été mis en file d'attente";
            break;
        case MFMailComposeResultSaved:
            msgResult = @"Message enregistré";
            msgResult2 = @"Vous avez sauvé le message dans les brouillons";
            break;
        case MFMailComposeResultSent:
            msgResult = @"Message envoyé";
            msgResult2 = @"Votre message a été bien envoyé. Merci pour votre commentaire";
            // flurry log event
            break;
        case MFMailComposeResultFailed:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Le message n'a pas été sauvé ou mis en file d'attente, probablement en raison d'une erreur";
            break;
        default:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Message non envoyé";
            break;
    }
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgResult message:msgResult2 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}



-(IBAction)emailPushed:(id)sender
{
	if ([MFMailComposeViewController canSendMail])
	{
		// device is configured to send mail
		MFMailComposeViewController* controller = [[[MFMailComposeViewController alloc] init] retain];
		controller.mailComposeDelegate = self;
		[controller setSubject:@"Application Maghrebail iOS"];
		[controller setMessageBody:@"" isHTML:YES];
		[controller setToRecipients:[NSArray arrayWithObject:@"services@maghrebail.ma"]];
		NSLog(@"controller MAil %@", controller.description);
		[self presentModalViewController:controller animated:YES];
		[controller release];
	}
	else
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Veuillez configurer votre compte messagerie" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
}


-(IBAction)browserPushed:(id)sender
{ 
	NSURL *url = [NSURL URLWithString:@"https:www.maghrebail.ma"];
	
	if (![[UIApplication sharedApplication] openURL:url])
		NSLog(@"%@%@",@"Failed to open url:",[url description]);
}


- (void)dealloc {
    [_webView release];
    [_lblTitle release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setWebView:nil];
    [self setLblTitle:nil];
    [super viewDidUnload];
}
@end
