//
//  PassWordViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface PassWordViewController : BaseViewController<UIScrollViewDelegate, UITextFieldDelegate>{
    CGPoint svos;
}

@property (retain, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (retain, nonatomic) IBOutlet UIButton *envoyerBtn;
@property (retain, nonatomic) IBOutlet UITextField *oldPassword;
@property (retain, nonatomic) IBOutlet UITextField *newPassword;
@property (retain, nonatomic) IBOutlet UITextField *confirmPassword;
-(void) initHeaderColor;
- (IBAction)envoyer:(id)sender;
@end
