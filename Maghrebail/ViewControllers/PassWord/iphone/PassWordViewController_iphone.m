//
//  PassWordViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "PassWordViewController_iphone.h"

@interface PassWordViewController_iphone ()

@end

@implementation PassWordViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
