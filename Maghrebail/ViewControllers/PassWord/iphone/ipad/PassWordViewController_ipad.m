//
//  PassWordViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "PassWordViewController_ipad.h"

@interface PassWordViewController_ipad ()

@end

@implementation PassWordViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
