//
//  PassWordViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/5/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "PassWordViewController.h"

@interface PassWordViewController ()

@end

@implementation PassWordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    svos = self._scrollView.contentOffset;
    self._scrollView.contentSize = CGSizeMake(self._scrollView.frame.size.width, self._scrollView.frame.size.height + 20);
	// Do any additional setup after loading the view.
}

-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MON MOT DE PASSE";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
-(void)updateView{
    [super updateView];
	if (![[[self.response objectForKey:@"header"]objectForKey:@"status"] isEqualToString:@"NOK"]) 
    [BaseViewController showAlert:nil message:[[self.response objectForKey:@"header"] objectForKey:@"message"] delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"TouchIDLinked"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"TouchIDLinkedFournisseur"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDLogin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"TouchIDPassword"];
    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"Attention!" message:kCompteDissocie delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self._scrollView release];
    [_envoyerBtn release];
    [_oldPassword release];
    [_newPassword release];
    [_confirmPassword release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self set_scrollView:nil];
    [self setEnvoyerBtn:nil];
    [self setOldPassword:nil];
    [self setNewPassword:nil];
    [self setConfirmPassword:nil];
    [super viewDidUnload];
}
#pragma mark --
#pragma mark TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        
        [textField resignFirstResponder];
        [self._scrollView setContentOffset:svos animated:YES];
    }
    return NO;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self._scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self._scrollView setContentOffset:pt animated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:self._scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self._scrollView setContentOffset:pt animated:YES];
    
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
           [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}
#pragma mark -- 
#pragma mark Actions 

- (IBAction)envoyer:(id)sender {
	
	if (dataManager.isDemo)
	{ 
        [BaseViewController showAlert:nil message:@"Vous êtes en mode démo" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
	}
	
    if (self.oldPassword.text == nil || [self.oldPassword.text length] == 0 || self.newPassword.text == nil || [self.newPassword.text length]==0) {
        [BaseViewController showAlert:nil message:@"Tous les champs sont obligatoires" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
    }
	
    if (self.newPassword.text == nil || [self.newPassword.text length] < 6)
	{
        [BaseViewController showAlert:nil message:@"Minimum 6 caractères" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
    }
	else
	{
		if (![self.newPassword.text isEqualToString:self.confirmPassword.text])
		{
			[BaseViewController showAlert:nil message:@"Les deux mots de passe ne sont pas identiques" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
			return;
		}
	}
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.oldPassword.text,@"lastPwd",self.newPassword.text, @"newPwd",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_CHANGE_PASSWORD andParam:params];
}
@end
