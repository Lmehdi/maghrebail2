//
//  AgencesViewController_iphone.h
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgencesViewController.h"

@interface AgencesViewController_iphone : AgencesViewController 
@property (retain, nonatomic) IBOutlet UIScrollView *mMainScrollView;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;

@end
