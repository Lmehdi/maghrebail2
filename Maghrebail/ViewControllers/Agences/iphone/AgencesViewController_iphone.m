//
//  AgencesViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgencesViewController_iphone.h"
#import "AgenceCell_iphone.h"
#import "AgenceProcheViewController_iphone.h"

@interface AgencesViewController_iphone ()

@end

@implementation AgencesViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _tableView.frame = CGRectMake(self.btnMap.frame.origin.x, self.btnMap.frame.origin.y + self.btnMap.frame.size.height + 5, self.mMainScrollView.frame.size.width, 200);
    
     [self.mMainScrollView setContentSize:CGSizeMake(self.mMainScrollView.frame.size.width, 500)];
    
    
    // Do any additional setup after loading the view.
	self.btnMap.enabled = NO;
	[self getRemoteContent:URL_AGENCES];
	
    selectedNumber          =   -1;
	_tableView.delegate = self;
	_tableView.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --
#pragma mark -- TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (shouldExpandCell) {
        if ([indexPath row] == selectedNumber) {
            return  356;
        } else {
            return 49;
        }
    } else {
        return 49;
    }
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	
    AgenceCell_iphone *cell = nil;
	
    if (isLandscape) {
        static NSString *cellIdentifier_l = @"AgenceCell_iphone_l";
        cell = (AgenceCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil) {
            cell = (AgenceCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
    } else {
        static NSString *cellIdentifier_p = @"AgenceCell_iphone_p";
        
        cell = (AgenceCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (AgenceCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
		cell.labelTitle.textColor = [UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1];
        cell.labelTitle.text = [NSString stringWithFormat:@"%@",[[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"nom"]];
        cell.labelAdresse.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"adresse"];
        cell.labelTel.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"telephone"];
        cell.labelFax.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"fax"];
		
        cell.labelNom.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"nom"];
        cell.labelContact.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"contact"];
        cell.labelEmail.text = [[aryAgences  objectAtIndex:indexPath.row] objectForKey:@"email"];
    }
	
	
	
	
    [cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_right_iphone.png"]];
    if (indexPath.row % 2) {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
    } else {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
	
	
	[cell.imgViewExtend setBackgroundColor:[UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1]];
	
	
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	indAgence = indexPath.row;
	for (int i = 0; i < aryAgences.count; i++) {
        AgenceCell_iphone* cell = (AgenceCell_iphone *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
		cell.labelTitle.textColor = [UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1];
		if (i % 2)
		{
			[cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
		}
		else
		{
			[cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
		}
    }

	
	AgenceCell_iphone* cell = (AgenceCell_iphone *)[tableView cellForRowAtIndexPath:indexPath];
	[cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1]];
	cell.labelTitle.textColor = [UIColor whiteColor];
	
	self.agenceProcheTitle.text = [NSString stringWithFormat:@"%@",[[aryAgences  objectAtIndex:indAgence] objectForKey:@"nom"]];
	self.agenceProcheAdresse.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"adresse"];
	self.agenceProcheTel.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"telephone"];
	self.agenceProcheFax.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"fax"];
	self.agenceEmail.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"email"];
	self.agenceContact.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"contact"];
	self.labelAgencePLusProche.hidden = YES;

    shouldExpandCell = !shouldExpandCell;
	
    for (int i = 0; i < aryAgences.count; i++)
    {
        AgenceCell_iphone* cell = (AgenceCell_iphone *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_right_iphone.png"]];
		cell.labelTitle.textColor = [UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1];
		if (i % 2)
		{
			[cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
		}
		else
		{
			[cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
		}
    }
    if (shouldExpandCell)
    {
        AgenceCell_iphone* cell = (AgenceCell_iphone *)[tableView cellForRowAtIndexPath:indexPath];
        [cell.imgViewFlech setImage:[UIImage imageNamed:@"fleche_orange_cell.png"]];
		[cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:54.0f/255.0f green:90.0f/255.0f blue:134.0f/255.0f alpha:1]];
		cell.labelTitle.textColor = [UIColor whiteColor];
    }
    int row = [indexPath row];
    selectedNumber = row;
    [tableView beginUpdates];
    //[tableView endUpdates];
	
	if (indexPath.row == aryAgences.count - 1 || indexPath.row == aryAgences.count - 2)
	{
		[[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
}
#pragma mark - updateView

-(void)updateView
{
    [super updateView];
	_tableView.hidden = NO;
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
		[dataManager.agences removeAllObjects];
		dataManager.agences.array = reponse;
		self.aryAgences.array = reponse;
	    [self.tableView reloadData];
		self.btnMap.enabled = YES;
		indAgence = 0;
		CLLocationDistance distanceMin = 5000.0;
		
		for (NSInteger i = 0 ; i < aryAgences.count; i++)
		{ 
			CLLocation *before = [[CLLocation alloc] initWithLatitude:[[[aryAgences  objectAtIndex:i] objectForKey:@"latitude"] doubleValue] longitude:[[[aryAgences  objectAtIndex:i] objectForKey:@"longitude"] doubleValue]];
			CLLocation *now = [[CLLocation alloc] initWithLatitude:dataManager.locationMe.coordinate.latitude longitude:dataManager.locationMe.coordinate.longitude]; 
			CLLocationDistance distance = ([before distanceFromLocation:now]) * 0.000621371192;
			if (distance < distanceMin)
			{
				distanceMin = distance;
				indAgence = i;
			}
			[before release];
			[now release];
            if (dataManager.locationMe.coordinate.latitude==0.0 && dataManager.locationMe.coordinate.longitude==0.0) {
                indAgence =2;
                break;
            }
		}
		
		self.agenceProcheTitle.text = [NSString stringWithFormat:@"%@",[[aryAgences  objectAtIndex:indAgence] objectForKey:@"nom"]];
		self.agenceProcheAdresse.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"adresse"];
        self.agenceProcheTel.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"telephone"];
        self.agenceProcheFax.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"fax"];
        self.agenceEmail.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"email"];
        self.agenceContact.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"contact"];
		
	}
    /*else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
	
    [_tableView reloadData];
}

- (IBAction)showAgenceProche:(id)sender
{
	[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosagences/consulterlaposition"];
	AgenceProcheViewController_iphone *view = [[AgenceProcheViewController_iphone alloc] initWithNibName:@"AgenceProcheViewController_iphone" bundle:nil];
	view.indAgence = indAgence;
	[self.navigationController pushViewController:view animated:YES];
	[view release];
}


-(IBAction)emailPushed:(id)sender
{
	if ([MFMailComposeViewController canSendMail])
	{
		// device is configured to send mail
		MFMailComposeViewController* controller = [[[MFMailComposeViewController alloc] init] retain];
		controller.mailComposeDelegate = self;
		[controller setSubject:@"Application Maghrebail iOS"];
		[controller setMessageBody:@"" isHTML:YES];
		[controller setToRecipients:[NSArray arrayWithObject:self.agenceEmail.text]];
		NSLog(@"controller MAil %@", controller.description);
		[self presentModalViewController:controller animated:YES];
		[controller release];
	}
	else
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Veuillez configurer votre compte messagerie" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
	
}

- (void)dealloc {
    [_mMainScrollView release];
	[_tableView release];
	[super dealloc];
}
@end
