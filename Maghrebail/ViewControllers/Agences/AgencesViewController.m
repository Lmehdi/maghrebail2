//
//  AgencesViewController.m
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgencesViewController.h"
#import "JSON.h"

@interface AgencesViewController ()

@end

@implementation AgencesViewController

@synthesize aryAgences;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self initHeaderColor];
    [dataManager locatedMe];
	if (dataManager.authentificationData) {
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	} else {
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (void) initHeaderColor {
    headerView.labelheaderTitre.text=@"NOS AGENCES";
    headerView.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    dataManager.typeColor=@"blue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initHeaderColor];
	self.aryAgences = [[NSMutableArray alloc] init];
	NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"agences" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
	NSDictionary *responseDico = [response JSONValue];
	NSArray *data = [responseDico objectForKey:@"response"];
	self.aryAgences = [NSMutableArray arrayWithArray:data];	
	_tableView.hidden = YES;
	self.labelAgencePLusProche.hidden = NO;
	
    
	if (self.aryAgences.count)
	{
		_tableView.hidden = NO;
		self.btnMap.enabled = YES;
		
		
		indAgence = 0;
		CLLocationDistance distanceMin = 5000.0;
		
		for (NSInteger i = 0 ; i < aryAgences.count; i++)
		{
			CLLocation *before = [[CLLocation alloc] initWithLatitude:[[[aryAgences  objectAtIndex:i] objectForKey:@"latitude"] doubleValue] longitude:[[[aryAgences  objectAtIndex:i] objectForKey:@"longitude"] doubleValue]];
			CLLocation *now = [[CLLocation alloc] initWithLatitude:dataManager.locationMe.coordinate.latitude longitude:dataManager.locationMe.coordinate.longitude];
            CLLocationDistance distance = ([before distanceFromLocation:now]) * 0.000621371192;
            if (distance < distanceMin)
            {
                distanceMin = distance;
                indAgence = i;
            }
            [before release];
            [now release];
            
            if (dataManager.locationMe.coordinate.latitude==0.0 && dataManager.locationMe.coordinate.longitude==0.0) {
                indAgence =2;
                break;
            }
			
		}
		
		self.agenceProcheTitle.text = [NSString stringWithFormat:@"%@",[[aryAgences  objectAtIndex:indAgence] objectForKey:@"nom"]];
        NSLog(@"nom : %@",self.agenceProcheTitle.text);
		self.agenceProcheAdresse.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"adresse"];
        self.agenceProcheTel.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"telephone"];
        self.agenceProcheFax.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"fax"];
        self.agenceEmail.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"email"];
        self.agenceContact.text = [[aryAgences  objectAtIndex:indAgence] objectForKey:@"contact"];
		
		[_tableView reloadData];
	}
    [dataManager locatedMe];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.aryAgences.count;
}


- (void)dealloc
{
	[aryAgences release];
	[_agenceProcheTitle release];
	[_agenceProcheAdresse release];
	[_agenceProcheTel release];
	[_agenceProcheFax release];
    [_btnMap release];
    [_agenceContact release];
    [_agenceEmail release];
    [_labelAgencePLusProche release];
	[super dealloc];
}

- (IBAction)showAgenceProche:(id)sender {
}
 

-(IBAction)emailPushed:(id)sender
{
 
}

- (IBAction)tel1Pushed:(id)sender
{
	NSString *strTel = self.agenceProcheTel.text;
	
	if([self.agenceProcheTel.text rangeOfString:@"/"].location != NSNotFound)
	{
		//found it
		NSArray *chunks = [self.agenceProcheTel.text componentsSeparatedByString:@"/"];
		strTel = [NSString stringWithFormat:@"%@",[chunks objectAtIndex:0]];
	}
	UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
													  message:[NSString stringWithFormat:@"Voulez-vous appeler le numéro \n%@", strTel]
													 delegate:self
											cancelButtonTitle:@"Non, Merci!"
											otherButtonTitles:@"Appeler", nil];
	message.tag = 1;
	[message show];
	[message release];
	NSLog(@"|%@|",strTel);
}

- (IBAction)tel2Pushed:(id)sender
{
	NSString *strTel = self.agenceProcheTel.text;
	if([self.agenceProcheTel.text rangeOfString:@"/"].location != NSNotFound)
	{
		//found it 
		NSArray *chunks = [self.agenceProcheTel.text componentsSeparatedByString:@"/"];
		strTel = [NSString stringWithFormat:@"%@",[chunks objectAtIndex:1]];
	} 
	 
	UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
													  message:[NSString stringWithFormat:@"Voulez-vous appeler le numéro \n%@", strTel]
													 delegate:self
											cancelButtonTitle:@"Non, Merci!"
											otherButtonTitles:@"Appeler", nil];
	message.tag = 2;
	[message show];
	[message release];
	NSLog(@"|%@|",strTel);
}

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *strTel = self.agenceProcheTel.text;
	if (alertView.tag == 1)
	{
		if([self.agenceProcheTel.text rangeOfString:@"/"].location != NSNotFound)
		{
			//found it
			NSArray *chunks = [self.agenceProcheTel.text componentsSeparatedByString:@"/"];
			strTel = [NSString stringWithFormat:@"%@",[chunks objectAtIndex:0]];
		}
		if (buttonIndex == 1)
		{
			strTel = [strTel stringByReplacingOccurrencesOfString:@" " withString:@""];
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strTel]]];
			NSLog(@"|%@|",strTel);
		}
	}
	if (alertView.tag == 2)
	{
		if([self.agenceProcheTel.text rangeOfString:@"/"].location != NSNotFound)
		{
			//found it
			NSArray *chunks = [self.agenceProcheTel.text componentsSeparatedByString:@"/"];
			strTel = [NSString stringWithFormat:@"%@",[chunks objectAtIndex:1]];
		}
		if (buttonIndex == 1)
		{ 
			strTel = [strTel stringByReplacingOccurrencesOfString:@" " withString:@""];
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strTel]]];
			NSLog(@"|%@|",strTel);
		}
	}

}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
    NSString * msgResult= nil ;
    NSString * msgResult2= nil ;
	
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msgResult = @"Envoie annulé";
            msgResult2 = @"Votre avez annulé l'opération et aucun message n'a été mis en file d'attente";
            break;
        case MFMailComposeResultSaved:
            msgResult = @"Message enregistré";
            msgResult2 = @"Vous avez sauvé le message dans les brouillons";
            break;
        case MFMailComposeResultSent:
            msgResult = @"Message envoyé";
            msgResult2 = @"Votre message a été bien envoyé. Merci pour votre commentaire";
            // flurry log event
            break;
        case MFMailComposeResultFailed:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Le message n'a pas été sauvé ou mis en file d'attente, probablement en raison d'une erreur";
            break;
        default:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Message non envoyé";
            break;
    }
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgResult message:msgResult2 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}

- (void)viewDidUnload {
	[self setAgenceProcheTitle:nil];
	[self setAgenceProcheAdresse:nil];
	[self setAgenceProcheTel:nil];
	[self setAgenceProcheFax:nil];
    [self setBtnMap:nil];
    [self setAgenceContact:nil];
    [self setAgenceEmail:nil];
    [self setLabelAgencePLusProche:nil];
	[super viewDidUnload];
}
@end
