//
//  AgenceCell.h
//  Maghrebail
//
//  Created by MAC on 04/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgenceCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelText;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFlech;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewExtend;
@property (retain, nonatomic) IBOutlet UILabel *labelAdresse;
@property (retain, nonatomic) IBOutlet UILabel *labelTel;
@property (retain, nonatomic) IBOutlet UILabel *labelFax;
@property (retain, nonatomic) IBOutlet UILabel *labelNom;
@property (retain, nonatomic) IBOutlet UILabel *labelContact;
@property (retain, nonatomic) IBOutlet UILabel *labelEmail;

@end
