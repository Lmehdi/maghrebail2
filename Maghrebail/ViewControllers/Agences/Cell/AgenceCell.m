//
//  AgenceCell.m
//  Maghrebail
//
//  Created by MAC on 04/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgenceCell.h"

@implementation AgenceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [_imgViewBackground release];
    [_labelTitle release];
    [_labelText release];
    [_imgViewFlech release];
    [_imgViewExtend release];
    [_labelAdresse release];
    [_labelTel release];
    [_labelFax release];
    [_labelNom release];
    [_labelContact release];
    [_labelEmail release];
    [super dealloc];
}

@end
