//
//  AgenceProcheViewController.m
//  Maghrebail
//
//  Created by MAC on 08/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgenceProcheViewController.h"
#import "Place.h"
#import "PlaceMark.h"
#import "MKMapView+ZoomLevel.h"
#import "BlockAlertView.h"
#import "JSON.h"
@interface AgenceProcheViewController ()

@end

@implementation AgenceProcheViewController
@synthesize mapView;
@synthesize lineColor;
@synthesize overlay;
@synthesize indAgence;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"POSITION DE L'AGENCE";
    headerView.backgroundColor=[DataManager getColorFromType:dataManager.typeColor];
    self.view.backgroundColor=[DataManager getColorFromType:dataManager.typeColor];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
	// Do any additional setup after loading the view.
	[self.mapView removeAnnotations:self.mapView.annotations];
	
	NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
	
	if (6 == [[versionCompatibility objectAtIndex:0] intValue])
	{
		overlay = [[TileOverlay alloc] initOverlay];
		[self.mapView addOverlay:overlay];
		MKMapRect visibleRect = [self.mapView mapRectThatFits:overlay.boundingMapRect];
		visibleRect.size.width /= 2;
		visibleRect.size.height /= 2;
		visibleRect.origin.x += visibleRect.size.width / 2;
		visibleRect.origin.y += visibleRect.size.height / 2;
		self.mapView.visibleMapRect = visibleRect;
		// END OSM
	}
	
	routeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
	routeView.userInteractionEnabled = NO;
	[mapView addSubview:routeView];
	self.lineColor = [UIColor colorWithRed:0.2823 green:0.2392 blue:0.5450 alpha:0.7];
 	
	Place* agence = [[[Place alloc] init] autorelease];
    if ([dataManager.agences count]==0) {
        NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"agences" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
        NSDictionary *responseDico = [response JSONValue];
        NSArray *data = [responseDico objectForKey:@"response"];
        dataManager.agences = [NSMutableArray arrayWithArray:data];
    }
	agence.name = [NSString stringWithFormat:@"Agence %@",[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"ville"]];
	agence.description = [NSString stringWithFormat:@"%@",[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"nom"]];
	agence.latitude = [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"latitude"] doubleValue];
	agence.longitude = [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"longitude"] doubleValue];
	PlaceMark* place = [[[PlaceMark alloc] initWithPlace:agence] autorelease];
	  
	if ([[[dataManager.agences  objectAtIndex:0] objectForKey:@"latitude"] doubleValue] > 20.0 && [[[dataManager.agences  objectAtIndex:0] objectForKey:@"latitude"] doubleValue] < 36.0 && [[[dataManager.agences  objectAtIndex:0] objectForKey:@"longitude"] doubleValue] > -17.0 && [[[dataManager.agences  objectAtIndex:0] objectForKey:@"longitude"] doubleValue] < 0.0 && [[[dataManager.agences  objectAtIndex:0] objectForKey:@"latitude"] doubleValue] != 0.0 && [[[dataManager.agences  objectAtIndex:0] objectForKey:@"longitude"] doubleValue] != 0.0)
		[self.mapView addAnnotation:place];

	
	MKMapRect zoomRect = MKMapRectNull;
	for (id <MKAnnotation> annotation in self.mapView.annotations)
	{
		MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
		MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
		if (MKMapRectIsNull(zoomRect))
		{
			zoomRect = pointRect;
		}
		else
		{
			zoomRect = MKMapRectUnion(zoomRect, pointRect);
		}
	}
	[self.mapView setVisibleMapRect:zoomRect animated:YES];

	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	
	[headerView.btnRightHeader setImage:[UIImage imageNamed:@"pin.png"] forState:UIControlStateNormal];
	[headerView.btnRightHeader addTarget:self action:@selector(showRoute) forControlEvents:UIControlEventTouchUpInside];
	
	[self.mapView setCenterCoordinate:self.mapView.centerCoordinate
							zoomLevel:15
							 animated:NO];
}


#pragma mark - MapDelegate Methods
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)ovl
{
    TileOverlayView *view = [[TileOverlayView alloc] initWithOverlay:ovl];
    view.tileAlpha = 1.0; // e.g. 0.6 alpha for semi-transparent overlay
    return view;
}

- (void)goBack
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)showRoute
{
	[Flurry logEvent:@"Home/DécouvrirMaghrebail/Nosagences/consulterlaposition/itinéraire"];
	// set the center of the map region to the now updated map view center 
	double lat = dataManager.locationMe.coordinate.latitude;
	double lng = dataManager.locationMe.coordinate.longitude;
	
	CLLocation *before = [[CLLocation alloc] initWithLatitude:[[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"latitude"] doubleValue] longitude:[[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"longitude"] doubleValue]];
	
	CLLocation *now = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
	
	CLLocationDistance distance = ([before distanceFromLocation:now]) * 0.000621371192;
	[before release];
	[now release];
	
	NSLog(@"Scrolled distance: %@", [NSString stringWithFormat:@"%.02f", distance]);
	if (distance > 19 || lat == 0.0 || lng == 0.0 )
	{ 
		
		alert = [BlockAlertView alertWithTitle:@"" message:@"L'itinéraire ne peut pas être affiché"];
		 
		[alert show];
		
		[self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];
		return;
	}

	Place* agence = [[[Place alloc] init] autorelease];
	agence.name = [NSString stringWithFormat:@"Agence %@",[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"ville"]];
	agence.description = [NSString stringWithFormat:@"%@",[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"nom"]];
	agence.latitude = [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"latitude"] doubleValue];
	agence.longitude = [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"longitude"] doubleValue];
	PlaceMark* place = [[[PlaceMark alloc] initWithPlace:agence] autorelease];
	Place* pos = [[[Place alloc] init] autorelease];
	pos.name = @"Ma position";
	pos.description = @"";
	pos.latitude = dataManager.locationMe.coordinate.latitude;
	pos.longitude = dataManager.locationMe.coordinate.longitude;
	PlaceMark* position = [[[PlaceMark alloc] initWithPlace:pos] autorelease];
	position.isUserLocation = YES;
	if (pos.latitude > 20.0 && pos.latitude < 36.0 && pos.longitude > -17.0 && pos.longitude < 0.0 && pos.latitude != 0.0 && pos.longitude != 0.0)
		[self.mapView addAnnotation:position];
	 
    [self drawItineraryFrom:position.coordinate to:place.coordinate];
    
//	routes = [[self calculateRoutesFrom:position.coordinate to:place.coordinate] retain];
//	drawLine = YES;
//	//[self updateRouteView];
	[self centerMap];
}

- (void)viewWillDisappear:(BOOL)animated
{
	 
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

 

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [mapView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setMapView:nil];
    [super viewDidUnload];
}
 

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
	NSLog(@"- annotation %@", annotation.description);
    if ([annotation isKindOfClass:[PlaceMark class]])
    {
		MKAnnotationView *annView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"HEBERJAHIZ"];
		if (annView == nil)
        {
            //NSLog(@"creating MyAnnotation from scratch");
            annView=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"HEBERJAHIZ"];
        }
		
        //No Pin
        PlaceMark *placeMark = (PlaceMark *)annotation;
		[annView setCanShowCallout:YES];
		[annView setSelected:YES];
		[annView setUserInteractionEnabled:YES];
		
		UIImage *logo;
        if (!placeMark.isUserLocation )
        { 
			logo = [UIImage imageNamed:@"AgencePin.png"];
        }
		else
		{
            logo = [UIImage imageNamed:@"green-marker.png"];
		}
		annView.image = logo;
		
		return annView;
	}
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
	
}

//-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
//	NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
//	NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
//	
//	NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
//	NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
//	NSLog(@"api url: %@", apiUrl);
//	NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl];
//	NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
//	
//	return [self decodePolyLine:[encodedPoints mutableCopy]];
//}


- (void) drawItineraryFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t  {
    [self showProgressHUD:YES];
    __block NSMutableArray * routs         =   [NSMutableArray array];
    
    [routs retain];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        MKDirectionsRequest *directionsRequest      =   [[MKDirectionsRequest alloc] init];
        MKPlacemark* placeMark1                     =   [[MKPlacemark alloc] initWithCoordinate:f addressDictionary:nil];
        MKPlacemark* placeMark2                     =   [[MKPlacemark alloc] initWithCoordinate:t addressDictionary:nil];
        
        [directionsRequest setTransportType:MKDirectionsTransportTypeAny];
        [directionsRequest setRequestsAlternateRoutes:NO];
        [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:placeMark1]];
        [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:placeMark2]];
        
        MKDirections * directions = [[MKDirections alloc] initWithRequest:directionsRequest];
        
        [directions calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error) {
            [self showProgressHUD:NO];

            if (error) {
                [self showErrorDialogWithMessage:@"L'itineraire ne peut être chargé pour le moment, veuillez réessayer plus tard."];

                return;
            }
            MKRoute *route = [response.routes firstObject];
            [routs addObject:route];
            dispatch_async(dispatch_get_main_queue(), ^{    // For each element, dispatch to the main queue to draw route and annotation corresponding to that location
                MKRoute *route = [routs firstObject];
                [self.mapView addOverlay:route.polyline];
            });
        }];
    });
}

- (MKOverlayRenderer*) mapView:(nonnull MKMapView *)mapView rendererForOverlay:(nonnull id<MKOverlay>)overlay {
    MKPolylineRenderer * routeLineRenderer  =   [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    routeLineRenderer.strokeColor           =   [UIColor colorWithRed:0.2823 green:0.2392 blue:0.5450 alpha:0.7];
    routeLineRenderer.lineWidth             =   6;
    return routeLineRenderer;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&mode=driving", f.latitude, f.longitude, t.latitude, t.longitude]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLResponse *response = nil;
    
    NSError *error = nil;
    
    
    
    CLLocationCoordinate2D endCoordinate;
    
    NSData *responseData =  [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!error) {
        
        
        
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"%@", responseDict);
        
        if ([[responseDict valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
            
            /*[[[UIAlertView alloc] initWithTitle:@"Error"
             
             message:@"Could not route path from your current location"
             
             delegate:nil
             
             cancelButtonTitle:@"Close"
             
             otherButtonTitles:nil, nil] show];*/
            
            return nil;
            
        }
        
        int points_count = 0;
        
        if ([[responseDict objectForKey:@"routes"] count])
            
            points_count = (int)[[[[[[responseDict objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"] count];
        
        
        
        
        
        if (!points_count) {
            
            /*[[[UIAlertView alloc] initWithTitle:@"Error"
             
             message:@"Could not route path from your current location"
             
             delegate:nil
             
             cancelButtonTitle:@"Close"
             
             otherButtonTitles:nil, nil] show];*/
            
            return nil;
            
        }
        
        CLLocationCoordinate2D points[points_count];
        
        NSLog(@"routes %@", [[[[responseDict objectForKey:@"routes"] objectAtIndex:0]objectForKey:@"overview_polyline"] objectForKey:@"points"]
              
              );
        
        int j = 0;
        
        NSArray *steps = nil;
        
        if (points_count && [[[[responseDict objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"legs"] count])
            steps = [[[[[responseDict objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
        
        NSMutableArray *routesTemp = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < points_count; i++) {
            double st_lat = [[[[steps objectAtIndex:i] objectForKey:@"start_location"] valueForKey:@"lat"] doubleValue];
            double st_lon = [[[[steps objectAtIndex:i] objectForKey:@"start_location"] valueForKey:@"lng"] doubleValue];
            
            if (st_lat > 0.0f && st_lon > 0.0f) {
                points[j] = CLLocationCoordinate2DMake(st_lat, st_lon);
                j++;
            }
            
            double end_lat = [[[[steps objectAtIndex:i] objectForKey:@"end_location"] valueForKey:@"lat"] doubleValue];
            double end_lon = [[[[steps objectAtIndex:i] objectForKey:@"end_location"] valueForKey:@"lng"] doubleValue];
            
            points[j] = CLLocationCoordinate2DMake(end_lat, end_lon);
            endCoordinate = CLLocationCoordinate2DMake(end_lat, end_lon);
            
            j++;
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:endCoordinate.latitude longitude:endCoordinate.longitude];
            [routesTemp addObject:location];
        }
        
        NSLog(@"points Count %f",endCoordinate.latitude);
        NSLog(@"points Count %f",endCoordinate.longitude);
        
        return routesTemp;
        
    }
    
    return nil;
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
	[encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
								options:NSLiteralSearch
								  range:NSMakeRange(0, [encoded length])];
	NSInteger len = [encoded length];
	NSInteger index = 0;
	NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
	NSInteger lat=0;
	NSInteger lng=0;
	while (index < len) {
		NSInteger b;
		NSInteger shift = 0;
		NSInteger result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lat += dlat;
		shift = 0;
		result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lng += dlng;
		NSNumber *latitude = [[[NSNumber alloc] initWithFloat:lat * 1e-5] autorelease];
		NSNumber *longitude = [[[NSNumber alloc] initWithFloat:lng * 1e-5] autorelease];
		printf("[%f,", [latitude doubleValue]);
		printf("%f]", [longitude doubleValue]);
		CLLocation *loc = [[[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]] autorelease];
		[array addObject:loc];
	}
	
	return array;
}

- (void)updateRouteView
{
	CGContextRef context = 	CGBitmapContextCreate(nil,
												  routeView.frame.size.width,
												  routeView.frame.size.height,
												  8,
												  4 * routeView.frame.size.width,
												  CGColorSpaceCreateDeviceRGB(),
												  kCGImageAlphaPremultipliedLast);
	
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
	CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
	CGContextSetLineWidth(context, 3.0);
	
	for(int i = 0; i < routes.count; i++)
	{
		CLLocation* location = [routes objectAtIndex:i];
		CGPoint point = [mapView convertCoordinate:location.coordinate toPointToView:routeView];
		
		if(i == 0)
		{
			CGContextMoveToPoint(context, point.x, routeView.frame.size.height - point.y);
		}
		else
		{
			CGContextAddLineToPoint(context, point.x, routeView.frame.size.height - point.y);
		}
	}
	
	CGContextStrokePath(context);
	
	CGImageRef image = CGBitmapContextCreateImage(context);
	UIImage* img = [UIImage imageWithCGImage:image];
	
	routeView.image = img;
	CGContextRelease(context);
}

- (void) centerMap
{
    if (routes.count > 0)
    {
        MKCoordinateRegion region;
        
        CLLocationDegrees maxLat = -90;
        CLLocationDegrees maxLon = -180;
        CLLocationDegrees minLat = 90;
        CLLocationDegrees minLon = 180;
        for(int idx = 0; idx < routes.count; idx++)
        {
            CLLocation* currentLocation = [routes objectAtIndex:idx];
            if(currentLocation.coordinate.latitude > maxLat)
                maxLat = currentLocation.coordinate.latitude;
            if(currentLocation.coordinate.latitude < minLat)
                minLat = currentLocation.coordinate.latitude;
            if(currentLocation.coordinate.longitude > maxLon)
                maxLon = currentLocation.coordinate.longitude;
            if(currentLocation.coordinate.longitude < minLon)
                minLon = currentLocation.coordinate.longitude;
        }
		region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat;
        region.span.longitudeDelta = maxLon - minLon;
        
        [mapView setRegion:region animated:YES];
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	routeView.hidden = YES;
	
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	[self updateRouteView];
	
	if (drawLine)
		routeView.hidden = NO;
	
	[routeView setNeedsDisplay];
	
	NSLog(@"Zoom Level: %@", [NSString stringWithFormat:@"%u", [self.mapView zoomLevel]]);
	if( [self.mapView zoomLevel] < 10 && [self.mapView zoomLevel] !=1)
	{
		[self.mapView setCenterCoordinate:self.mapView.centerCoordinate
								zoomLevel:15
								 animated:NO];
		
		MKMapRect zoomRect = MKMapRectNull;
		for (id <MKAnnotation> annotation in self.mapView.annotations)
		{
			MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
			MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 50, 50);
			if (MKMapRectIsNull(zoomRect))
			{
				zoomRect = pointRect;
			}
			else
			{
				zoomRect = MKMapRectUnion(zoomRect, pointRect);
			}
		}
		[self.mapView setVisibleMapRect:zoomRect animated:YES];
	}
	
	if([self.mapView zoomLevel] !=1)
	{
		MKCoordinateRegion mapRegion;
		// set the center of the map region to the now updated map view center
		mapRegion.center = self.mapView.centerCoordinate;
		double lat = mapRegion.center.latitude;
		double lng = mapRegion.center.longitude;
		
		CLLocation *before = [[CLLocation alloc] initWithLatitude:[[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"latitude"] doubleValue] longitude:[[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"longitude"] doubleValue]];
		
		CLLocation *now = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
		
		CLLocationDistance distance = ([before distanceFromLocation:now]) * 0.000621371192;
		[before release];
		[now release];
		
		NSLog(@"Scrolled distance: %@", [NSString stringWithFormat:@"%.02f", distance]);
		
		if( distance > 20 && [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"latitude"] doubleValue] != 0.0 && [[[dataManager.agences  objectAtIndex:self.indAgence] objectForKey:@"longitude"] doubleValue] != 0.0)
		{
			// do something awesome
			NSLog(@"##################");
			[self.mapView setCenterCoordinate:self.mapView.centerCoordinate
									zoomLevel:10
									 animated:NO];
			
			MKMapRect zoomRect = MKMapRectNull;
			for (id <MKAnnotation> annotation in self.mapView.annotations)
			{
				MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
				MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 50, 50);
				if (MKMapRectIsNull(zoomRect))
				{
					zoomRect = pointRect;
				}
				else
				{
					zoomRect = MKMapRectUnion(zoomRect, pointRect);
				}
			}
			[self.mapView setVisibleMapRect:zoomRect animated:YES];
		}
	}
}

- (void) showErrorDialogWithMessage:(NSString*) message {
    UIAlertController* errorDialog = [UIAlertController alertControllerWithTitle:nil
                                                                         message:message
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [errorDialog addAction:defaultAction];
    [self presentViewController:errorDialog animated:YES completion:nil];
}

@end