//
//  AgenceProcheViewController.h
//  Maghrebail
//
//  Created by MAC on 08/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BaseViewController.h"
#import "TileOverlay.h"
#import "TileOverlayView.h"  
#import "TileOverlay.h"
#import "TileOverlayView.h" 
#import "PlaceMark.h" 
#import "MKMapView+ZoomLevel.h"

@interface AgenceProcheViewController : BaseViewController
{ 
	UIImageView* routeView;
	
	NSArray* routes;
	
	UIColor* lineColor;
	BOOL drawLine;
}
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (assign, nonatomic) NSInteger indAgence;
@property (nonatomic, retain) TileOverlay *overlay;

@property (nonatomic, retain) UIColor* lineColor;

-(void) initHeaderColor;
@end
