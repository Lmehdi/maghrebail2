//
//  AgenceProcheViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 12/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "AgenceProcheViewController_ipad.h"

@interface AgenceProcheViewController_ipad ()

@end

@implementation AgenceProcheViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
