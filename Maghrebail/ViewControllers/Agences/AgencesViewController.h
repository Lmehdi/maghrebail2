//
//  AgencesViewController.h
//  Maghrebail
//
//  Created by MAC on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h" 
#import <MessageUI/MessageUI.h>

@interface AgencesViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>
{
    NSMutableArray *aryAgences;
    BOOL shouldExpandCell;
    int selectedNumber;
	NSInteger indAgence;
}
@property (retain, nonatomic) IBOutlet UILabel *labelAgencePLusProche;
@property (retain, nonatomic) IBOutlet UIButton *btnMap;
@property (retain, nonatomic) IBOutlet UILabel *agenceProcheTitle;
@property (retain, nonatomic) IBOutlet UILabel *agenceProcheAdresse;
@property (retain, nonatomic) IBOutlet UILabel *agenceProcheTel;
@property (retain, nonatomic) IBOutlet UILabel *agenceProcheFax;
@property (nonatomic, retain) NSMutableArray *aryAgences;
@property (retain, nonatomic) IBOutlet UILabel *agenceContact;
@property (retain, nonatomic) IBOutlet UILabel *agenceEmail;
- (IBAction)showAgenceProche:(id)sender;
- (IBAction)emailPushed:(id)sender;
- (IBAction)tel1Pushed:(id)sender;
- (IBAction)tel2Pushed:(id)sender;
-(void)initHeaderColor;
@end
