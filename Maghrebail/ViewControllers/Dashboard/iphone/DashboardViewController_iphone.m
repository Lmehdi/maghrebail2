//
//  DashboardViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DashboardViewController_iphone.h"
#import "PopUpDashVC_iphone.h"
#import "DashSlideVC_iPhone.h"
#import "AppgroupeVC_iphone.h"
#import "PubliciteViewController_iphone.h"
#import "MonProfilViewController_iphone.h"

@interface DashboardViewController_iphone ()

@end

@implementation DashboardViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
        // Custom initialization
        shouldAddHeader = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSlide) name:@"dataSlider" object:nil];
    
    // Do any additional setup after loading the view from its nib.
    if ([DataManager isIphone6] || [DataManager isIphone6Plus]) {
        [self.scrollMenu setContentSize:CGSizeMake(750, self.scrollMenu.frame.size.height)];
        [self.dashboardLeasebox setContentSize:CGSizeMake(375, self.dashboardLeasebox.frame.size.height)];
        [self.dashboardInfo setContentSize:CGSizeMake(375, self.dashboardInfo.frame.size.height)];
    }else{
        [self.scrollMenu setContentSize:CGSizeMake(1280, self.scrollMenu.frame.size.height)];
        [self.dashboardLeasebox setContentSize:CGSizeMake(640, self.dashboardLeasebox.frame.size.height)];
        [self.dashboardInfo setContentSize:CGSizeMake(427, self.dashboardInfo.frame.size.height)];
    }
	
	[self.dashboardMaghrebail setContentSize:CGSizeMake(321, self.dashboardMaghrebail.frame.size.height)];
	
	
	self.navigationController.sideMenu = [self sideMenu];
    self.mArraySlider = [NSMutableArray array];
   
    [self updateSlide];
    
    NSTimer *timer;
	timer = [NSTimer scheduledTimerWithTimeInterval: 5
											 target: self
										   selector: @selector(handleTimer)
										   userInfo: nil
											repeats: YES];
}

- (void)updateSlide
{
    self.mArraySlider = [NSMutableArray array];
    NSData *dataSlider = [DataManager readDataIntoCachWith:@"dataSlider"];
    
    if (dataSlider)
    {
        self.mArraySlider = [NSKeyedUnarchiver unarchiveObjectWithData:dataSlider];
    }
    if (self.mArraySlider.count > 0)
    {
        self.imgDefault.hidden = YES;
        [self paintSlide:self.mArraySlider];
    }
}

- (void)handleTimer
{   
	int page = self.scrollSlider.contentOffset.x / self.scrollSlider.frame.size.width;
	
	if ( page + 1 < self.pageControlSlide.numberOfPages )
	{
		page++;
		self.pageControlSlide.currentPage = page++;
	}
	else
	{
		page = 0;
		self.pageControlSlide.currentPage = page;
	}
	[self changePage];
}

- (void)changePage
{
	int page = self.pageControlSlide.currentPage;
    
	[self.scrollSlider setContentOffset:CGPointMake(self.scrollSlider.frame.size.width * page, self.scrollSlider.contentOffset.y) animated:YES];
}


 
- (void)paintSlide:(NSArray *)ary
{
	int i = 0;
	for (NSDictionary *dico in ary)
	{
        if ([DataManager isIphone6Plus])
        {
            sliderVC = [[DashSlideVC_iPhone alloc] initWithNibName:@"DashSlideVC_iPhone6Plus" bundle:nil];
        }else{
            if ([DataManager isIphone6])
            {
                sliderVC = [[DashSlideVC_iPhone alloc] initWithNibName:@"DashSlideVC_iPhone6" bundle:nil];
            }else{
                if ([DataManager isIphone5])
                {
                    sliderVC = [[DashSlideVC_iPhone alloc] initWithNibName:@"DashSlideVC_iPhone5" bundle:nil];
                }
                else
                {
                    sliderVC = [[DashSlideVC_iPhone alloc] initWithNibName:@"DashSlideVC_iPhone" bundle:nil];
                }
            }

        }
        
		CGRect frameSlider = sliderVC.view.frame;
        
		frameSlider.origin.y = 0;
		frameSlider.origin.x = frameSlider.origin.x + (i*frameSlider.size.width);
		sliderVC.view.frame = frameSlider;
		[sliderVC.imgViewSlider loadUsingCache:YES];
		[sliderVC.imgViewSlider loadFromURL:[dico objectForKey:@"banniere"]];
		
		[self.scrollSlider addSubview:sliderVC.view];
		i++;
	}
	self.pageControlSlide.numberOfPages = ary.count;
	self.pageControlSlide.currentPage = 0;
	
	[self.scrollSlider setContentSize:CGSizeMake(self.scrollSlider.frame.size.width * i , self.scrollSlider.frame.size.height)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark Actions 

-(void)showEspaceAbonne:(id)sender
{
	dataManager.typeDash = 3;
    //[self.view addSubview: [self sideMenu].navigationController.view];
    self.window.rootViewController =[self sideMenu].navigationController;
} 

-(void)showDecouvrir:(id)sender
{
	dataManager.typeDash = 1;
   // [self.view addSubview: [self sideMenu].navigationController.view];
    self.window.rootViewController =[self sideMenu].navigationController;
}

-(void)showInformations:(id)sender
{
	dataManager.typeDash = 2;
    //[self.view addSubview: [self sideMenu].navigationController.view];
    self.window.rootViewController = [self sideMenu].navigationController;
}

-(void)displayBanner
{
	publicite = [[PubliciteViewController_iphone alloc]initWithNibName:@"PubliciteViewController_iphone" bundle:nil];
	[publicite loadPub];
	[self performSelector:@selector(addBanner) withObject:nil afterDelay:3];
}

- (void)addBanner
{
	[self.view addSubview:publicite.view]; 
    [publicite performSelector:@selector(dismiss) withObject:nil afterDelay:5];
}

- (IBAction)btnMenuPushed:(UIButton *)sender
{
	for (NSInteger i = 0 ; i < self.aryButtons.count; i++)
	{
		UIButton *btn = (UIButton *)[self.aryButtons objectAtIndex:i];
		btn.selected = NO;
		
		//UILabel *lbl = (UILabel *)[self.aryLabels objectAtIndex:i];
		//lbl.textColor = [UIColor colorWithRed:52.0/255.0 green:91.0/255.0 blue:140.0/255.0 alpha:1];
	}
	sender.selected = YES;
	/*for (NSInteger i = 0 ; i < self.aryLabels.count; i++)
	{
		UILabel *lbl = (UILabel *)[self.aryLabels objectAtIndex:i];
		if (lbl.tag == sender.tag)
		{
			lbl.textColor = [UIColor colorWithRed:205.0/255.0 green:117.0/255.0 blue:36.0/255.0 alpha:1];
		}
	}*/
	
    switch (sender.tag)
    {
        case 0:
        {
            [Flurry logEvent:@"Home/Nosproduits/produitEquipementEntreprise"];
            [dataManager.slideMenu produitEquipementEntreprise:YES];
        }
            break;
        case 1:
        {
            [Flurry logEvent:@"Home/Nosproduits/produitImmobilierProfessionnel"];
            [dataManager.slideMenu produitImmobilierProfessionnel:YES andParallaxe:NO];
        }
            break;
        case 2:
        {
            [Flurry logEvent:@"Home/Nosproduits/produitEquipementProfessionnel"];
            [dataManager.slideMenu produitEquipementProfessionnel:YES andParallaxe:NO];
        }
            break;
        case 3:
        {
            [Flurry logEvent:@"Home/Notresimulateur"];
            [dataManager.slideMenu simulator:YES];
        }
            break;
        case 4:
        {
            [Flurry logEvent:@"Home/Nosproduits/produitImmobilierEntreprise"];
            [dataManager.slideMenu produitImmobilierEntreprise:YES];
        }
            break;
        case 5:
        {
            [Flurry logEvent:@"Home/Nosagences"];
            [dataManager.slideMenu agences:YES];
        }
            break;
        case 6:
        {
            [Flurry logEvent:@"Home/Informations/NotreFAQ"];
            [dataManager.slideMenu faq:YES];
        }
            break;
        case 7: //Publications
        {
            [Flurry logEvent:@"Home/Informations/NotreFAQ"];
            [dataManager.slideMenu nosPublications:YES];
        }
            break;
            
        case 8:
        {
            [Flurry logEvent:@"Home/Informations/mentionslégales"];
            [dataManager.slideMenu mentionLegale:YES];
        }
            break;
        case 9: //applications groupe
        {
            [Flurry logEvent:@"Home/Informations/appGroupe"];
            AppgroupeVC_iphone *viewC = [[AppgroupeVC_iphone alloc] initWithNibName:@"AppgroupeVC_iphone" bundle:nil];
            [self.view addSubview:viewC.view];
        }
            break;
            
        case 10:
        {
            [Flurry logEvent:@"Home/Informations/noscontacts"];
            [dataManager.slideMenu nosContacts:YES];
        }
            break;
        case 11: //profil
        {
            if (!dataManager.authentificationData )
            {
            //    [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Profil"];
            [dataManager.slideMenu monProfil:YES];
        }
            break;
        case 12: //mesimpayes
        {
            if (!dataManager.authentificationData )
            {
              //  [self showPopUp:sender.tag];
             MonProfilViewController_iphone   *monProfilViewController = [[MonProfilViewController_iphone alloc] initWithNibName:@"MonProfilViewController_iphone" bundle:nil];
                [self.navigationController pushViewController:monProfilViewController animated:YES];
                return;
            }
            [Flurry logEvent:@"Home/meslignesdecrédits"];
            [dataManager.slideMenu maLigneCredit:YES];
        }
            break;
        case 13:
        {
            if (!dataManager.authentificationData )
            {
            //    [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Démonstration/mescontrats"];
            [dataManager.slideMenu contrats:YES];
        }
            break;
        case 14:
        {
            if (!dataManager.authentificationData )
            {
            //    [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/mesimpayés"];
            [dataManager.slideMenu monSolde:YES];
        }
            break;
        case 15:
        {
            if (!dataManager.authentificationData )
            {
           //     [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Démonstration/mesgaranties"];
            [dataManager.slideMenu mesGaranties:YES];
        }
            break;
        case 16:
        {
            if (!dataManager.authentificationData )
            {
            //    [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Démonstration/mesréclamations"];
            [dataManager.slideMenu mesReclamations:YES];
        }
            break;
        case 17:
        {
            if (!dataManager.authentificationData )
            {
            //    [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/mesfactures"];
            [dataManager.slideMenu mesFactures:YES];
        }
            break;
        case 18:
        {
            if (!dataManager.authentificationData )
            {
           //     [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Messagerie"];
            [dataManager.slideMenu maMessagerie:YES];
        }
            break;
        case 19:
        {
            if (!dataManager.authentificationData )
            {
          //      [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/meséchéanciers"];
            [dataManager.slideMenu mesEcheanciers:YES];
        }
            break;
        case 20://
        {
            if (!dataManager.authentificationData)
            {
        //        [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/mescontacts"];
            [dataManager.slideMenu mesContacts:YES];
        }
            break;
        case 21://
        {
            if (!dataManager.authentificationData)
            {
           //     [self showPopUp:sender.tag];
                return;
            }
            [Flurry logEvent:@"Home/Documents"];
            [dataManager.slideMenu mesDocuments:YES];
        }
            break;
        case 30://
        {
            [Flurry logEvent:@"Home/Facebook"];
            [dataManager.slideMenu showFacebook:YES];
        }
            break;
        case 31://
        {
            [Flurry logEvent:@"Home/Twitter"];
            [dataManager.slideMenu showTwitter:YES];
        }
            break;
              default:
            break;
    }

}


- (IBAction)btnMenuTopPushed:(UIButton *)sender
{
    if (sender == selectedBtn && sender.selected)
    {
        self.pageControlMenu.numberOfPages = 2;
        sender.selected = NO;
        self.scrollMenu.hidden = NO;
        
        scroll = self.scrollMenu;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(hide)];
        
        self.scrollMenu.alpha = 1;
        self.dashboardInfo.alpha = 0;
        self.dashboardLeasebox.alpha = 0;
        self.dashboardMaghrebail.alpha = 0;
      
        [UIView commitAnimations];

        for (NSInteger i = 0 ; i < self.aryLblTop.count; i++)
        {
            UILabel *lbl = (UILabel *)[self.aryLblTop objectAtIndex:i];
            
            lbl.textColor = [UIColor colorWithRed:52.0/255.0 green:91.0/255.0 blue:140.0/255.0 alpha:1];
        }
        return;
    }
    
    for (NSInteger i = 0 ; i < self.aryBtnTop.count; i++)
	{
		UIButton *btn = (UIButton *)[self.aryBtnTop objectAtIndex:i];
		btn.selected = NO;
		
		UILabel *lbl = (UILabel *)[self.aryLblTop objectAtIndex:i];
		lbl.textColor = [UIColor colorWithRed:52.0/255.0 green:91.0/255.0 blue:140.0/255.0 alpha:1];
	}
	sender.selected = YES;
   for (NSInteger i = 0 ; i < self.aryLblTop.count; i++)
	{
		UILabel *lbl = (UILabel *)[self.aryLblTop objectAtIndex:i];
		if (lbl.tag == sender.tag)
		{
			lbl.textColor = [UIColor colorWithRed:205.0/255.0 green:117.0/255.0 blue:36.0/255.0 alpha:1];
		}
	}
    
    for (NSInteger i = 0 ; i < self.aryImgTriangle.count; i++)
	{
        UIImageView *img = (UIImageView *)[self.aryImgTriangle objectAtIndex:i];
        if (img.tag == sender.tag)
            img.hidden = NO;
        else
            img.hidden = YES;
	}
    switch (sender.tag)
    {
        case 0:
            self.pageControlMenu.numberOfPages = 1;
            [self setFocusOn:self.dashboardMaghrebail];
            break;
        case 1:
            self.pageControlMenu.numberOfPages = 1;
            [self setFocusOn:self.dashboardLeasebox];
            break;
        case 2:
            self.pageControlMenu.numberOfPages = 1;
            [self setFocusOn:self.dashboardInfo];
            break;
            
        default:
            break;
    }
    
    
    selectedBtn = sender;
}

- (void)showPopUp:(NSInteger )index
{
    if (!popVC)
    {
        popVC = [[PopUpDashVC_iphone alloc] initWithNibName:@"PopUpDashVC_iphone" bundle:nil];
    }
    popVC.parent = self;
    popVC.index = index;
    [self.view addSubview:popVC.view];
 
}

- (IBAction)aryBtnTop:(UIButton *)sender
{
}

@end
