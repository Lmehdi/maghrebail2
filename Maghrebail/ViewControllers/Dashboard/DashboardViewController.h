//
//  DashboardViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "MFSideMenu.h"
#import "SlideViewController_iphone.h"
#import "ProduitViewController_iphone.h"
#import "ProduitViewController_ipad.h"
#import "ProduitViewController.h"
#import "SplashViewController.h"
#import "SplashViewController_iphone.h"
#import "SlideViewController_ipad.h"
#import "SplashViewController_ipad.h"
#import "DashSlideVC.h"
#import "PubliciteViewController.h"
@class DashSlideVC;
@interface DashboardViewController : BaseViewController <RemoteImageViewDelegate>
{
    
    PubliciteViewController *publicite;
    DashSlideVC *sliderVC;
    UIButton *selectedBtn;
    NSInteger cmptImg;
    
    UIScrollView *scroll;
}

@property (retain, nonatomic) IBOutlet UIImageView *imgDefault;
@property (retain, nonatomic) IBOutlet UIImageView *img1;
@property (retain, nonatomic) IBOutlet UIImageView *img2;
@property (retain, nonatomic) IBOutlet UIImageView *img0;

@property (retain, nonatomic) IBOutletCollection(UIImageView) NSArray *aryImgTriangle;
@property (retain, nonatomic) IBOutlet UIScrollView *dashboardInfo;
@property (retain, nonatomic) IBOutlet UIScrollView *dashboardLeasebox;
@property (retain, nonatomic) IBOutlet UIScrollView *dashboardMaghrebail;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollSlider;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControlSlide;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControlMenu;
@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray *aryBtnTop;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray *aryLblTop;
@property(nonatomic,retain)UIWindow * window;
@property(nonatomic, retain) IBOutlet UIScrollView *scrollMenu;
@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray *aryButtons;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray *aryLabels;

@property (retain, nonatomic) NSMutableArray *mArraySlider;

- (IBAction)btnMenuPushed:(UIButton *)sender;
- (IBAction)btnMenuTopPushed:(UIButton *)sender;

-(IBAction)showDecouvrir:(id)sender;
-(IBAction)showEspaceAbonne:(id)sender;
-(IBAction)showInformations:(id)sender;
- (MFSideMenu *)sideMenu;
- (void) setupNavigationControllerApp ;
-(void)displayBanner;
- (void)setFocusOn:(UIScrollView *)scrollView;

@end
