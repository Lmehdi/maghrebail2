//
//  DashboardViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/27/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DashboardViewController.h"
#import "MonProfilViewController.h"
#import "FaqViewController_iphone.h"
#import "FaqViewController_ipad.h"
#import "AuthentificationViewController.h"
#import "AuthentificationViewController_iphone.h"
#import "PubliciteViewController_iphone.h"
#import "PubliciteViewController_ipad.h"
#import "AppDelegate.h"
@interface DashboardViewController ()

@end

@implementation DashboardViewController
@synthesize window;
@synthesize scrollMenu;
@synthesize mArraySlider;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = NO;
        cmptImg = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[Flurry logEvent:@"Home"];
	if (!dataManager.escapePub)
	{
		//[self performSelector:@selector(displayBanner) withObject:nil afterDelay:0];
	}
	
    self.scrollMenu.hidden = NO;
    
	// Do any additional setup after loading the view.
	
}

- (void)setFocusOn:(UIScrollView *)scrollView
{
    scroll = scrollView;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hide)];
    
    self.scrollMenu.alpha = 0;
    self.dashboardInfo.alpha = 0;
    self.dashboardLeasebox.alpha = 0;
    self.dashboardMaghrebail.alpha = 0;
    if (scrollView != nil)
    {
        scrollView.alpha = 1;
        scrollView.hidden = NO;
    }
    
    [UIView commitAnimations];
}

- (void)hideScroll
{
    self.scrollMenu.hidden = YES;
    self.dashboardInfo.hidden = YES;
    self.dashboardLeasebox.hidden = YES;
    self.dashboardMaghrebail.hidden = YES;
    
    if (scroll != nil)
    {
        scroll.alpha = 1;
        scroll.hidden = NO;
    }
}


-(void)imageLoaded:(RemoteImageView *)image fromCache:(BOOL)cache
{
    cmptImg++;
    
    if (cmptImg == self.mArraySlider.count)
    {
        sliderVC.view.hidden = NO;
        [self.imgDefault removeFromSuperview];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (void)viewDidUnload
{
    [self setAryButtons:nil];
    [self setAryLabels:nil];
    [self setAryLblTop:nil];
    [self setAryBtnTop:nil];
    [self setPageControlSlide:nil];
    [self setPageControlMenu:nil];
    [self setScrollSlider:nil];
    [self setDashboardMaghrebail:nil];
    [self setDashboardLeasebox:nil];
    [self setDashboardInfo:nil];
    [self setAryImgTriangle:nil];
    [self setImg0:nil];
    [self setImg1:nil];
    [self setImg2:nil];
    [self setImgDefault:nil];
	[super viewDidUnload];
	self.scrollMenu = nil;
}

- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[scrollMenu release];
    [_aryButtons release];
    [_aryLabels release];
    [_aryLblTop release];
    [_aryBtnTop release];
    [_pageControlSlide release];
    [_pageControlMenu release];
    [_scrollSlider release];
    [_dashboardMaghrebail release];
    [_dashboardLeasebox release];
    [_dashboardInfo release];
    [_aryImgTriangle release];
    [mArraySlider release];
    [_img0 release];
    [_img1 release];
    [_img2 release];
    [_imgDefault release];
	[super dealloc];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (UINavigationController *)navigationController {
	
	switch (dataManager.typeDash)
	{
		case 1:
			
			[Flurry logEvent:@"Home/DécouvrirMaghrebail"];
			return [[UINavigationController alloc]
					initWithRootViewController:[self produitViewController]];
			break; 
		case 2:
			
			[Flurry logEvent:@"Home/Informations"];
			return [[UINavigationController alloc]
					initWithRootViewController:[self faqViewController]];
			break;
		case 3:
			
			[Flurry logEvent:@"Home/EspaceAbonnés"];
			if (dataManager.authentificationData)
			{
				return [[UINavigationController alloc]
						initWithRootViewController:[self profilViewController]];
			}
			
			return [[UINavigationController alloc]
					initWithRootViewController:[self seConnecter]];
			break; 
		default:
			break;
	}
	return [[UINavigationController alloc]
			initWithRootViewController:[self produitViewController]];
}*/

-(PubliciteViewController *)publiciteViewController
{
	PubliciteViewController *publiciteViewController  = nil;
    if (!IS_IPAD)
	{
        
        publiciteViewController = [[PubliciteViewController_iphone alloc] initWithNibName:@"PubliciteViewController_iphone" bundle:nil];
    }else{
        publiciteViewController = [[PubliciteViewController_ipad alloc] initWithNibName:@"PubliciteViewController_ipad" bundle:nil];
    } 
    return publiciteViewController;

}

- (ProduitViewController *)produitViewController
{
    ProduitViewController *produitViewController  = nil;
    if (!IS_IPAD) {
        
        produitViewController = [[ProduitViewController_iphone alloc] initWithNibName:@"ProduitViewController_iphone" bundle:nil];
    }else{
        produitViewController = [[ProduitViewController_ipad alloc] initWithNibName:@"ProduitViewController_ipad" bundle:nil];
    }
    [produitViewController setTypeProduit:kPRODUITS_EQUIPEMENT];
    return produitViewController;
}


- (FaqViewController *)faqViewController
{
    FaqViewController *produitViewController  = nil;
    if (!IS_IPAD) {
        
        produitViewController = [[FaqViewController_iphone alloc] initWithNibName:@"FaqViewController_iphone" bundle:nil];
    }else{
        produitViewController = [[FaqViewController_ipad alloc] initWithNibName:@"FaqViewController_ipad" bundle:nil];
    } 
    return produitViewController;
}


- (MonProfilViewController *)profilViewController
{
    MonProfilViewController *produitViewController  = nil;
	
    if (!IS_IPAD) {
        
        produitViewController = [[MonProfilViewController alloc] initWithNibName:@"MonProfilViewController_iphone" bundle:nil];
    }else{
        produitViewController = [[MonProfilViewController alloc] initWithNibName:@"MonProfilViewController_ipad" bundle:nil];
    } 
    return produitViewController;
}


- (AuthentificationViewController *)seConnecter
{
    AuthentificationViewController *produitViewController  = nil;
	
    if (!IS_IPAD)
	{
        produitViewController = [[AuthentificationViewController_iphone alloc] initWithNibName:@"AuthentificationViewController_iphone" bundle:nil];
    }
	else
	{
        produitViewController = [[AuthentificationViewController_ipad alloc] initWithNibName:@"AuthentificationViewController_ipad" bundle:nil];
    }
    return produitViewController;
}

- (DashboardViewController *)dash
{
    return self;
}

- (MFSideMenu *)sideMenu
{
    SlideViewController  *leftSideMenuController = nil;
    SlideViewController *rightSideMenuController = nil;
	AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
    if (!IS_IPAD)
	{
        leftSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
		leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_iphone alloc] initWithNibName:@"SlideViewController_iphone" bundle:nil];
		rightSideMenuController.window = delegate.window;
    }
	else
	{
        leftSideMenuController = [[SlideViewController_ipad alloc] initWithNibName:@"SlideViewController_ipad" bundle:nil];
		leftSideMenuController.window = delegate.window;
        rightSideMenuController = [[SlideViewController_ipad alloc] initWithNibName:@"SlideViewController_ipad" bundle:nil];
		rightSideMenuController.window = delegate.window;
    }
    
    UINavigationController *navigationController = [self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:leftSideMenuController
                                            rightSideMenuController:rightSideMenuController];
    leftSideMenuController.sideMenu = sideMenu;
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}

- (void) setupNavigationControllerApp
{
    SplashViewController * splashViewController = nil;
    
    !IS_IPAD? splashViewController = ([[SplashViewController_iphone alloc]initWithNibName:@"SplashViewController_iphone" bundle:nil]):
    (splashViewController = [[SplashViewController alloc]initWithNibName:@"SplashViewController_ipad" bundle:nil]);
    
    // self.window.rootViewController = [self sideMenu].navigationController;
    
    // [self.window makeKeyAndVisible];
    // [self.window.rootViewController presentModalViewController:splashViewController animated:NO];
}


- (IBAction)btnMenuPushed:(UIButton *)sender {}

- (IBAction)btnMenuTopPushed:(id)sender {}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (scrollView == self.scrollMenu || scrollView == self.dashboardMaghrebail  || scrollView == self.dashboardInfo  || scrollView == self.dashboardLeasebox)
    {
        if (scrollView == self.scrollMenu)
        {
            for (NSInteger i = 0 ; i < self.aryImgTriangle.count; i++)
            {
                UIImageView *img = (UIImageView *)[self.aryImgTriangle objectAtIndex:i];
                img.hidden = YES;
            }
            if (!IS_IPAD)
            {
                switch (page)
                {
                    case 0:
                        self.img0.hidden = YES;
                        self.img1.hidden = NO;
                        self.img2.hidden = YES;
                        break;
                    case 1:
                        self.img0.hidden = YES;
                        self.img1.hidden = NO;
                        self.img2.hidden = YES;
                        
                        
                        break;
                    case 2:
                        self.img0.hidden = NO;
                        self.img1.hidden = YES;
                        self.img2.hidden = YES;
                        
                        break;
                    case 3:
                        self.img0.hidden = YES;
                        self.img1.hidden = YES;
                        self.img2.hidden = NO;
                        
                        break;
                        
                    default:
                        break;
                }
            }
        }
        
        if (scrollView == self.dashboardInfo && scrollView.contentOffset.x > 0)
            self.pageControlMenu.currentPage = page+1;
        else
            self.pageControlMenu.currentPage = page;
    }
    else
    {
        
        self.pageControlSlide.currentPage = page ;
        
        
    }
    
}


@end
