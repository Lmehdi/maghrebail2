//
//  ListFacturesViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ListFacturesViewController.h"

@interface ListFacturesViewController ()

@end

@implementation ListFacturesViewController
@synthesize contratNumMVT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    self.labelTitle.text=@"FACTURES DU CONTRAT N°";
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.contratNum,@"num", nil];
	self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.contratNum];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_MES_FACTURES_CONTRACT_LIST andParam:params];
    
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
  //  if(IS_IPHONE){
	if ([[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum])
	{
		if ([[[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum] isEqualToString:@"FACTURE"])
			self.btnFav.selected = YES;
		else
			self.btnFav.selected = NO;
	}
	else 
		self.btnFav.selected = NO;
        
   // }
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=self.labelTitle.text;
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
}
 
- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
        
        self.btnFav.hidden=NO;
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateView{
    [super updateView];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.response objectForKey:@"response"] count];
}

#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.contratNum,@"num", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_MES_FACTURES_CONTRACT_LIST andParam:params];
}


-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
	 
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
		
		[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
		[headerView setDelegate:nil];
		[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
		[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
		
		self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.contratNum];
	}
	
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
}

- (void)dealloc {
	[_labelTitle release];
	[_btnFav release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setLabelTitle:nil];
	[self setBtnFav:nil];
	[super viewDidUnload];
}
- (IBAction)handlFav:(id)sender {
	
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (self.btnFav.selected)
	{
		self.btnFav.selected = NO;
		[standardUserDefaults removeObjectForKey:self.contratNum];
	}
	else
	{ 
		self.btnFav.selected = YES;
		[standardUserDefaults setObject:@"FACTURE" forKey:self.contratNum];
	}
	[standardUserDefaults synchronize];
	
}
@end
