//
//  ListFacturesViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ListFacturesViewController_iphone.h"
#import "DetailPDFViewController_iphone.h"

@interface ListFacturesViewController_iphone ()

@end

@implementation ListFacturesViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isLandscape) {
  
		static NSString *cellIdentifier_l;
		
		if ([DataManager isIphone5])
		{
			cellIdentifier_l = @"ListBiensCell_iphone5_l";
		}
		else
			cellIdentifier_l = @"ListBiensCell_iphone_l";
		
        ListBiensCell *cell = (ListBiensCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ListBiensCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
     
        cell.numFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_MVT"] ;
        cell.dateFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FACTURE"] ;
        cell.dateEcheance.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_ECHEANCE"] ;
        cell.statutBien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TYPE_FACTURE"];  
        cell.montantTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
        
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }else{
        static NSString *cellIdentifier_p = @"ListBiensCell_iphone_p";
        
        ListBiensCell *cell = (ListBiensCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (ListBiensCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.alpha = 0.5;
        
        cell.numFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_MVT"] ;
        
        cell.dateFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FACTURE"] ;
        cell.dateEcheance.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_ECHEANCE"] ;
        cell.statutBien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_ECHEANCE"];
        cell.montantTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]]; 
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (!isLandscape)
	{
		
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/listefactures/PDF"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures/PDF"];
		DetailPDFViewController_iphone *detailPDF = [[DetailPDFViewController_iphone alloc] initWithNibName:@"DetailPDFViewController_iphone" bundle:nil];
		detailPDF.contratNum = self.contratNum;
		detailPDF.contratNumMVT = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_MVT"];
		detailPDF.email = self.email;
		detailPDF.type = @"facture";
		[self.navigationController pushViewController:detailPDF animated:YES];
		[detailPDF release];
	} 
}
@end
