//
//  ListBiensCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ListBiensCell.h"

@implementation ListBiensCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_designation release];
    [_fournisseur release];
    [_statutBien release];
    [_montantTTC release];
    [_dateEcheance release];
    [_dateFacture release];
    [_numFacture release];
    [super dealloc];
}
@end
