//
//  ListBiensCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListBiensCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *designation;
@property (retain, nonatomic) IBOutlet UILabel *fournisseur;
@property (retain, nonatomic) IBOutlet UILabel *statutBien;
@property (retain, nonatomic) IBOutlet UILabel *montantTTC;
@property (retain, nonatomic) IBOutlet UILabel *dateEcheance;
@property (retain, nonatomic) IBOutlet UILabel *dateFacture;
@property (retain, nonatomic) IBOutlet UILabel *numFacture;
@end
