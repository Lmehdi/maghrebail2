//
//  ListFacturesViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface ListFacturesViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property(nonatomic, retain)NSString * contratNum;
@property(nonatomic, retain)NSString * contratNumMVT;
@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property(nonatomic, retain)NSString * email;

-(void) initHeaderColor;
- (IBAction)handlFav:(id)sender;

@end
