//
//  ListFacturesViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ListFacturesViewController_ipad.h"
#import "DetailPDFViewController_ipad.h"

@interface ListFacturesViewController_ipad ()

@end

@implementation ListFacturesViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
        static NSString *cellIdentifier_l = @"ListBiensCell_ipad";
        ListBiensCell *cell = (ListBiensCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ListBiensCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
	
	cell.numFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_MVT"] ;
	cell.dateFacture.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FACTURE"] ;
	cell.dateEcheance.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_ECHEANCE"] ;
	cell.statutBien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TYPE_FACTURE"];
	cell.montantTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
	
    
        cell.alpha = 0.5;
    
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
	
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/listefactures/PDF"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures/PDF"];
	DetailPDFViewController_ipad *detailPDF = [[DetailPDFViewController_ipad alloc] initWithNibName:@"DetailPDFViewController_ipad" bundle:nil];
    detailPDF.contratNum = self.contratNum;
    detailPDF.contratNumMVT = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_MVT"];
    detailPDF.email = self.email;
    detailPDF.type = @"facture";
    [self.navigationController pushViewController:detailPDF animated:YES];
    [detailPDF release];
}
@end
