//
//  DetailPDFViewController.h
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface DetailPDFViewController : BaseViewController
{
	BOOL isSend;
}

@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UIWebView *webViewPDF;

@property(nonatomic, retain)NSString * contratNum;
@property(nonatomic, retain)NSString * contratNumMVT;
@property(nonatomic, retain)NSString * email;
@property(nonatomic, retain)NSString *type;
@property (nonatomic, retain) NSString *urlPDF;
@property (retain, nonatomic) IBOutlet UIView *viewLoading;


-(void) initHeaderColor;
@end
