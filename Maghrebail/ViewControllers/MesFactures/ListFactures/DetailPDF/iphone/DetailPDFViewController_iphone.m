//
//  DetailPDFViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailPDFViewController_iphone.h"

@implementation DetailPDFViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    
}
@end
