//
//  DetailPDFViewController.m
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailPDFViewController.h"
#import "AppDelegate.h"
#import "ADNActivityCollection.h"

@interface DetailPDFViewController ()

@end

@implementation DetailPDFViewController   

@synthesize urlPDF;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		isSend = NO; 
    }
    return self;
}

-(void) initHeaderColor{
    
    headerView.labelheaderTitre.text=@"MES FACTURES";
    headerView.labelheaderTitre.text=[self.labelTitle.text uppercaseString];
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewDidLoad
{
	
	[self showProgressHUD:NO];
    [super viewDidLoad];
    [self initHeaderColor];
    
	NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.contratNumMVT,@"num",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"token",@"6b7761d25ded57704372fb7f74e2dc50" , nil];
    if ([self.type isEqualToString:@"facture"])
	{
		
		self.labelTitle.text = [NSString stringWithFormat:@"Facture %@ %@",self.labelTitle.text, self.contratNumMVT];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
		[self postDataToServer:URL_FACTURES_PDF andParam:params];
	}
	else
	{
        if ([self.type isEqualToString:@"echeancier"])
        {
            self.labelTitle.text = [NSString stringWithFormat:@"Échéancier du Contrat %@ %@",self.labelTitle.text, self.contratNumMVT];
            if (dataManager.isDemo)
            {
                params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
            }
            [self postDataToServer:URL_ECHEANCIER_PDF andParam:params];
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"%@", self.contratNum];
            //Create a URL object.
            NSURL *url = [NSURL URLWithString:self.urlPDF];
            //URL Requst Object
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            //Load the request in the UIWebView.
            [self.webViewPDF loadRequest:requestObj];
        }

	}
		
	// Do any additional setup after loading the view.
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    headerView.btnRightHeader.hidden=YES;
//	[headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
//	[headerView.btnRightHeader addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];



}

- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}


- (void)sendMail
{
    	[self showProgresspdfHUD:YES];
      [self sharecontentview];
    
    
}
-(void)sharecontentview
{
   
   

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       NSData *urlData=nil;
                        NSString*  filePath_;
                       if ([self.type isEqualToString:@"facture"] || [self.type isEqualToString:@"echeancier"]  )
                       {
                           
                           
                           
                           NSString *urlAddress = [[[self.response objectForKey:@"response"]objectAtIndex:0] objectForKey:@"pdf_url"];
                           
                           NSString *pdfname = [[urlAddress lastPathComponent] stringByDeletingPathExtension];//le nom du fichier sans extension
                           
                           NSFileManager *fileManager = [NSFileManager defaultManager];
                           
                           NSString *pdf_=[NSString stringWithFormat:@"%@/%@.%@",@"Library/Caches",pdfname,@"pdf"];
                           
                            filePath_= [NSHomeDirectory() stringByAppendingPathComponent:pdf_ ];
                           
                           if ([fileManager fileExistsAtPath:filePath_])
                           {
                               //  NSURL  *url = [NSURL URLWithString:filePath_];
                               urlData = [NSData dataWithContentsOfFile:filePath_];
                               
                               
                           }
                           else
                           {
                               NSURL  *url = [NSURL URLWithString:urlAddress];
                               urlData = [NSData dataWithContentsOfURL:url];
                               // [self savepdf:pdfname :urlAddress];
                               [urlData writeToFile:filePath_ atomically:YES];
                               
                               
                           }
                       }
                       else
                       {
                           
                           
                           
                           NSString *pdfname = [[self.urlPDF lastPathComponent] stringByDeletingPathExtension];//le nom du fichier sans extension
                           
                           NSFileManager *fileManager = [NSFileManager defaultManager];
                           
                           NSString *pdf_=[NSString stringWithFormat:@"%@/%@.%@",@"Library/Caches",pdfname,@"pdf"];
                           
                           filePath_= [NSHomeDirectory() stringByAppendingPathComponent:pdf_ ];
                           
                           if ([fileManager fileExistsAtPath:filePath_])
                           {
                               //  NSURL  *url = [NSURL URLWithString:filePath_];
                               urlData = [NSData dataWithContentsOfFile:filePath_];
                               
                               
                           }
                           else
                           {
                               NSURL  *url = [NSURL URLWithString:self.urlPDF];
                               urlData = [NSData dataWithContentsOfURL:url];
                               // [self savepdf:pdfname :urlAddress];
                               [urlData writeToFile:filePath_ atomically:YES];
                               
                               
                           }
                           
                           
                       }
 
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
            
                           
                           
                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                           NSString *documentsDirectory = [paths objectAtIndex:0];
                           NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", @"FileName"]];
                           NSURL *URL = [NSURL fileURLWithPath:filePath_];
                           NSArray *activityItems = [NSArray arrayWithObjects:URL, nil];
                           UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                           
                           
                           AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
                           
                           
                           NSArray *allActivities = [ADNActivityCollection allActivities];
                        //   NSArray *activityItems = @[urlData,@"FD"];
                           
                           NSMutableArray *activities = [NSMutableArray array];
                           [activities addObjectsFromArray:allActivities];
                           
                           UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                                      applicationActivities:activities];
                           
                           // Exclude default activity types for demo.
                  //         NSArray * excludeActivities = @[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeMessage];
                           NSData *pdfData = [NSData dataWithContentsOfFile:filePath_];
//                           UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[pdfData] applicationActivities:nil];
                           
                           [mainDelegate.window.rootViewController presentViewController: activityViewController animated:YES completion:nil ];
                           
                           	[self showProgresspdfHUD:NO];
        });
        
    });
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
/*
    NSArray *reponse = [self.response objectForKey:@"response"];
    //urlpdf
    NSString *urlAddress = [[reponse objectAtIndex:0] objectForKey:@"pdf_url"];
    
    NSString *theFileName = [urlAddress lastPathComponent];
   
   NSString* documentsPath = [DataManager savePath];
    NSString *pdfdirectory=[documentsPath stringByAppendingPathComponent:@"pdfpartage"];
    
 //   NSString* pathfile = [pdfdirectory stringByAppendingPathComponent:theFileName];
    
 //   BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathfile];
 //   NSLog(@" this is the url pdf %@" ,self.urlPDF);
    
        NSURL  *url = [NSURL URLWithString:urlAddress];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
      
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", pdfdirectory,theFileName];
            [urlData writeToFile:filePath atomically:YES];*/

    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
    
    
    NSString *urlAddress = [[[self.response objectForKey:@"response"]objectAtIndex:0] objectForKey:@"pdf_url"];
    
    
    NSString *pdfname = [[urlAddress lastPathComponent] stringByDeletingPathExtension];//le nom du fichier sans extension
    
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *pdf_=[NSString stringWithFormat:@"%@/%@.%@",@"Library/Caches",pdfname,@"pdf"];
    
    NSString*  filePath_= [NSHomeDirectory() stringByAppendingPathComponent:pdf_ ];

  
    

    
            if(buttonIndex==0)
            {
                
                
               
                
            }
    
            if(buttonIndex==1)
            {/*
              [Flurry logEvent:FLURRY_ACTION_PARTAGE_TWITTER];
              NSDictionary *dicoTwitter = [dicoPartage objectForKey:@"Twitter"];
              if ( 5 <= [[versionCompatibility objectAtIndex:0] intValue] )
              {
              /// iOS 5 et plus est installer
              TWTweetComposeViewController *twitterViewController = [[TWTweetComposeViewController alloc] init];
              [twitterViewController setInitialText:[NSString stringWithFormat:@"%@, %@", [dicoTwitter objectForKey:@"message"], [dicoTwitter objectForKey:@"url"]]];
              [mainDelegate.window.rootViewController presentModalViewController:twitterViewController animated:YES];
              }
              */
                
                
            }
            else if(buttonIndex==2)
                
                
            {
                NSData *urlData=nil;
                
                if ([fileManager fileExistsAtPath:filePath_])
                {
                    //  NSURL  *url = [NSURL URLWithString:filePath_];
                 urlData = [NSData dataWithContentsOfFile:filePath_];
                    
                }
                else
                {
                    NSURL  *url = [NSURL URLWithString:urlAddress];
                    urlData = [NSData dataWithContentsOfURL:url];
                    // [self savepdf:pdfname :urlAddress];
                    [urlData writeToFile:filePath_ atomically:YES];
                }
                
                
                if ([MFMailComposeViewController canSendMail])
                {
                    MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
                    
                    viewController.mailComposeDelegate = self;
                    [viewController setSubject:@"TESTS"];
                    [viewController addAttachmentData:urlData mimeType:@"application/pdf" fileName:[urlAddress lastPathComponent]];

                    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
                    [mainDelegate.window.rootViewController presentModalViewController:viewController animated:YES];
                    [viewController release];
                }
                else
                {
                    UIAlertView * alert =[[UIAlertView alloc]initWithTitle:nil message:@"error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                }
            }
            }

            
            
            

            
            
            
            

    
    
    
    

/*


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
    [mainDelegate.window.rootViewController dismissModalViewControllerAnimated:YES];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
    [mainDelegate.window.rootViewController dismissModalViewControllerAnimated:YES];
}


*/

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
	
}

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{ 
	if (buttonIndex == 1)
	{
		isSend = YES;
		NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.contratNumMVT,@"num",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"token",@"6b7761d25ded57704372fb7f74e2dc50" , nil];
		if ([self.type isEqualToString:@"facture"])
		{
			if (dataManager.isDemo)
			{
				
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/description/PDF/Partage"];
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
			else 
				[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/description/PDF/Partage"];
			
			[self postDataToServer:URL_FACTURES_MAIL andParam:params];
		}
		else  if ([self.type isEqualToString:@"echeancier"])
		{
			if (dataManager.isDemo)
			{
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/description/PDF/Partage"];
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
			else 				
				[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers/description/PDF/Partage"];
			[self postDataToServer:URL_ECHEANCIER_MAIL andParam:params];
		}
        else if ([self.type isEqualToString:@"document"])
        {
            
			if (dataManager.isDemo)
			{
				[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/description/PDF/Partage"];
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
			else
            {
                
                [Flurry logEvent:@"Home/EspaceAbonnés/mesDocuments/description/PDF/Partage"];
                [params setObject:self.contratNum forKey:@"nom"];
            }
            [self postDataToServer:URL_DOCUMENT_MAIL andParam:params];
        }
		
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[self showProgressHUD:NO]; 
	[super viewWillAppear:animated];
	[self initHeaderColor];
	[headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
    if(IS_IPHONE_5){
        headerView.btnRightHeader.frame=CGRectMake(280, headerView.btnLeftHeader.frame.origin.y, headerView.btnRightHeader.frame.size.width, headerView.btnRightHeader.frame.size.height);
    }
    
    
	
}

- (void)viewWillDisappear:(BOOL)animated
{
/*	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
	[headerView setDelegate:self];
	[super viewWillDisappear:animated];
 */
 
   [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
    //[headerView setDelegate:nil];
    [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateView{
    [super updateView];
	NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
      
		if (!isSend)
		{ 
			NSString *urlAddress = [[reponse objectAtIndex:0] objectForKey:@"pdf_url"];
			//Create a URL object.
			NSURL *url = [NSURL URLWithString:urlAddress];
			
			//URL Requst Object
			NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
			
			//Load the request in the UIWebView.
			[self.webViewPDF loadRequest:requestObj];
		}
		else
		{
			
			UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
															  message:[header objectForKey:@"message"]
															 delegate:nil
													cancelButtonTitle:@"Ok"
													otherButtonTitles:nil];
			
			[message show];
			[message release];
		}
	}
    /*else
    {
        NSString *message = [header objectForKey:@"message"];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		[alert release];
    }*/

}

- (void)dealloc {
    [_labelTitle release];
    [_webViewPDF release];
	[_viewLoading release];
    [urlPDF release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelTitle:nil];
    [self setWebViewPDF:nil];
	[self setViewLoading:nil];
    [super viewDidUnload];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	self.viewLoading.hidden = YES;
    headerView.btnRightHeader.hidden=NO;
    [headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
    [headerView.btnRightHeader addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
}

- (void)webView:(UIWebView *) webView didFailLoadWithError:(NSError *)error
{
	self.viewLoading.hidden = YES;
	[BaseViewController showAlert:@"Info" message:@"Problème affichage PDF" delegate:nil confirmBtn:@"OK" cancelBtn:@""];
}
@end
