//
//  MesFacturesViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesFacturesViewController_ipad.h"

@interface MesFacturesViewController_ipad ()

@end

@implementation MesFacturesViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(dataManager.isFournisseurContent || dataManager.isDemo){
        
        self.btnFav.hidden=YES;
        self.labelFavoris.hidden=YES;
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *cellIdentifier_l = @"FactureContractCell_ipad";
        FactureContractCell *cell = (FactureContractCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (FactureContractCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
    if(dataManager.isDemo)
    {
        if(dataManager.DemoredirectfactureFournisseur)
        {
            
            btnSelectedEncours = YES;
            btnSelectedRegles = NO;
            btnSelectedClotures=NO;
            self.buttonEncours.selected = YES;
            self.buttonRegles.selected = NO;
            self.buttonClotures.selected=NO;
            
            cell.numContract.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
            cell.dateMise.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];
            
            cell.datefin.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
            cell.duree.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_HT"];
            cell.montant.text= [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TAXE"];
            
            cell.statut.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"] ;
        }
        else{
            
            if (self.showFav)
            {
                
                cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.dateMise.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                
                cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.montant.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"];
                cell.statut.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
            }
            else
            {
                
                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.dateMise.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                
                cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.montant.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"];
                cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
            }
        }
    }
    
    else if(dataManager.isFournisseurContent)
    {
        
        
//        btnSelectedEncours = YES;
//        btnSelectedRegles = NO;
//        btnSelectedClotures=NO;
//        
//        
//        self.buttonEncours.selected = YES;
//        self.buttonRegles.selected = NO;
//        self.buttonClotures.selected=NO;
        cell.numContract.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
        cell.dateMise.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];
        
        cell.datefin.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
        cell.duree.text = [DataManager getFormatedNumero:[[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
        cell.montant.text= [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TAXE"];
        
        cell.statut.text = [DataManager getFormatedNumero:[[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]] ;
        
        
    }
    else if(dataManager.isClientContent)
    {
        
        self.first.text=@"N°CONTRAT";
        self.second.text=@"DATE DE MISE EN LOYER";
        self.third.text=@"DATE FIN";
        self.fourth.text=@"DURÉE";
        self.last.text=@"STATUT";
        if (self.showFav)
        {
            
            cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
            cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
            cell.dateMise.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
            
            cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
            cell.montant.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
            cell.statut.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
        }
        else
        {
            
            
            cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
            cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
            cell.dateMise.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
            
            cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
            cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
            cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
            
            
        }
        
        
    }
    

	
        cell.alpha = 0.5;
    
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
//	self.isFromButton = NO;
//	if (dataManager.isDemo)
//		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/listefactures"];
//	else
//		[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];
//    ListFacturesViewController_ipad * listFacturesViewController_ipad = [[ListFacturesViewController_ipad alloc]initWithNibName:@"ListFacturesViewController_ipad" bundle:nil];
//    
//    listFacturesViewController_ipad.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
//    [self.navigationController pushViewController:listFacturesViewController_ipad animated:YES];
    
    
    
    if(dataManager.isClientContent || (dataManager.isDemo && !dataManager.DemoredirectfactureFournisseur))
    {
        if (dataManager.isDemo)
            [Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/listefactures"];
        else
            [Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];
        self.isFromButton = NO;
       ListFacturesViewController_ipad * listFacturesViewController_ipad = [[ListFacturesViewController_ipad alloc]initWithNibName:@"ListFacturesViewController_ipad" bundle:nil];
        
        if (!self.showFav)
            listFacturesViewController_ipad.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
        else
            listFacturesViewController_ipad.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
        [self.navigationController pushViewController:listFacturesViewController_ipad animated:YES];
    }
    
    if(dataManager.isFournisseurContent || (dataManager.isDemo && dataManager.DemoredirectfactureFournisseur))
    {
        self.isFromButton = NO;
        dataManager.isdatafacture=YES;
        dataManager.isdetailsengagement=NO;
        dataManager.isdetailscommande=NO;
        dataManager.isdetailsreglement=NO;
        dataManager.isdetailsimmatriculation=NO;
        dataManager.isdetailscontact=NO;
        
        
        
        /*
         DetailsFactureViewController_iphone * detailsfactureviewcontroller = [[DetailsFactureViewController_iphone alloc]initWithNibName:@"DetailsFactureViewController_iphone" bundle:nil];*/
        DetailsBienCommandeViewController_ipadViewController *detailsfactureviewcontroller=[[DetailsBienCommandeViewController_ipadViewController alloc]initWithNibName:@"DetailsBienCommandeViewController_ipadViewController" bundle:nil];
        
        detailsfactureviewcontroller.idmvt =  [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MVT"];
        
        [self.navigationController pushViewController:detailsfactureviewcontroller animated:YES];
        
    }
    
    
}
@end
