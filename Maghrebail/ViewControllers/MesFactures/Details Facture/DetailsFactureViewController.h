//
//  DetailsFactureViewController.h
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import  "QuartzCore/QuartzCore.h"

@interface DetailsFactureViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property(nonatomic, retain)NSString * idmvt;
-(void) initHeaderColor;

@end
