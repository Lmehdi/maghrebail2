//
//  DetailsFactureViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "DetailsFactureViewController_iphone.h"


@interface DetailsFactureViewController_iphone ()

@end

@implementation DetailsFactureViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    if (isLandscape)
    {
        static NSString *cellIdentifier_l;
        
        if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"DetailsFactureCell_iphone5_l";
        }
        else
            cellIdentifier_l = @"DetailsFactureCell_iphone_l";
        
        DetailsFactureCell *cell = (DetailsFactureCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (DetailsFactureCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        
        //   cell.textLabel.text = [[[self.response objectForKey:@"response"]objectAtIndex:0]objectForKey:@"DESIGN_BIEN"];
        
        cell.alpha = 0.5;
        NSMutableDictionary* item  = [[[self.response objectForKey:@"response"]objectAtIndex:0] mutableCopy];
        NSString *key=[[item allKeys]objectAtIndex:indexPath.row];
        NSString *value= [[item allValues]objectAtIndex:indexPath.row];
        if(key == nil ||  [key isKindOfClass:[NSNull class]])
        {
            cell.keyfacture.text   = @""  ;
            
        }
        else
        {
            cell.keyfacture.text=key;
        }
        if(value == nil ||  [value isKindOfClass:[NSNull class]])
        {
            cell.valuefacture.text   = @""  ;
            
        }
        else
        {
            cell.valuefacture.text=value;
        }
        
        
        
        return cell;
    }else
    {
        
        static NSString *cellIdentifier_p = @"DetailsFactureCell_iphone_p";
        
        DetailsFactureCell *cell = (DetailsFactureCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell = (DetailsFactureCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.alpha = 0.5;
        
        NSMutableDictionary* item  = [[[self.response objectForKey:@"response"]objectAtIndex:0] mutableCopy];
        NSString *key=[[item allKeys]objectAtIndex:indexPath.row];
        NSString *value= [[item allValues]objectAtIndex:indexPath.row];
        if(key == nil ||  [key isKindOfClass:[NSNull class]])
        {
            cell.keyfacture.text   = @""  ;
            
        }
        else
        {
            cell.keyfacture.text=key;
        }
        if(value == nil ||  [value isKindOfClass:[NSNull class]])
        {
            cell.valuefacture.text   = @""  ;
            
        }
        else
        {
            cell.valuefacture.text=value;
        }
        
        
        return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
