//
//  DetailsFactureCell.m
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "DetailsFactureCell.h"

@implementation DetailsFactureCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
   
 
    [_keyfacture release];
    [_valuefacture release];
    [super dealloc];
}
@end
