//
//  DetailsFactureCell.h
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsFactureCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *keyfacture;
@property (retain, nonatomic) IBOutlet UILabel *valuefacture;


@end
