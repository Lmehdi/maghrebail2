//
//  MesFacturesViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MesFacturesViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
	BOOL showFav;
    CGRect frameViewMontMin;
    CGRect frameViewMontMax;
    
    CGRect frameViewDateMin;
    CGRect frameViewDateMax;
    
    CGRect frameMontBtnMin;
    CGRect frameMontImgMin;
    CGRect frameMontLblMin;
    
    CGRect frameDateBtnMin;
    CGRect frameDateImgMin;
    CGRect frameDateLblMin;
    
    
    CGRect frameMontBtnMax;
    CGRect frameMontImgMax;
    CGRect frameMontLblMax;
    
    CGRect frameDateBtnMax;
    CGRect frameDateImgMax;
    CGRect frameDateLblMax;
    
    CGRect frameViewMontMinInitial;
    CGRect frameViewMontMaxInitial;
    
    CGRect frameViewDateMinInitial;
    CGRect frameViewDateMaxInitial;
    
    CGRect frameMontBtnMinInitial;
    CGRect frameMontImgMinInitial;
    CGRect frameMontLblMinInitial;
    
    CGRect frameDateBtnMinInitial;
    CGRect frameDateImgMinInitial;
    CGRect frameDateLblMinInitial;
    
    
    CGRect frameMontBtnMaxInitial;
    CGRect frameMontImgMaxInitial;
    CGRect frameMontLblMaxInitial;
    
    CGRect frameDateBtnMaxInitial;
    CGRect frameDateImgMaxInitial;
    CGRect frameDateLblMaxInitial;
	
}
@property (retain, nonatomic) IBOutlet UILabel *numcontrat;

@property (retain, nonatomic) IBOutlet UIButton *btnaffaireencours;
@property (retain, nonatomic) IBOutlet UIButton *btnaffairesemises;
@property (retain, nonatomic) IBOutlet UIButton *btnaffairesvalidees;
@property (retain, nonatomic) IBOutlet UIButton *buttonEncours;
@property (retain, nonatomic) IBOutlet UIButton *buttonRegles;
@property (retain, nonatomic) IBOutlet UIButton *buttonClotures;
@property (retain, nonatomic) IBOutlet UILabel *textfieldfilteresult;
@property (retain, nonatomic) IBOutlet UIButton *buttonpush;


- (IBAction)filteraction:(id)sender;
- (IBAction)encoursPushed:(UIButton *)sender;
- (IBAction)reglesPushed:(UIButton *)sender;
- (IBAction)ValiderPushed:(UIButton *)sender;


@property (retain, nonatomic) NSMutableArray *arrayFav;


@property (assign, nonatomic) BOOL showFav;




@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property (retain, nonatomic) IBOutlet UILabel *labelFavoris;
@property (assign, nonatomic)  int index;
//@property (retain, nonatomic) IBOutlet UILabel *Numcontrat;
@property (retain, nonatomic) IBOutlet UILabel *datefin;
@property (retain, nonatomic) IBOutlet UILabel *duree;
@property (retain, nonatomic) IBOutlet UIButton *Rencours;
@property (retain, nonatomic) IBOutlet UIButton *Rvalides;
@property (retain, nonatomic) IBOutlet UIButton *Rregles;
@property (retain, nonatomic) IBOutlet UILabel *Ncontrat;
@property (retain, nonatomic) IBOutlet UILabel *statut;

@property (retain, nonatomic) IBOutlet UILabel *first;
@property (retain, nonatomic) IBOutlet UILabel *second;
@property (retain, nonatomic) IBOutlet UILabel *third;
@property (retain, nonatomic) IBOutlet UILabel *fourth;
@property (retain, nonatomic) IBOutlet UILabel *last;

@property (retain, nonatomic) IBOutlet UILabel *datemiseenloyer;
-(void) initHeaderColor;
- (IBAction)showFav:(id)sender;
@end
