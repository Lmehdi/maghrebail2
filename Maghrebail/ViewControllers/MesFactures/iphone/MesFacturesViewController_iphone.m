//
//  MesFacturesViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesFacturesViewController_iphone.h"
#import "ListCommandesViewController_iphone.h"
#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)

@interface MesFacturesViewController_iphone ()

@end
CGRect  oldFrameFacture;
@implementation MesFacturesViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
  
    if(!isLandscape){
        oldFrameFacture=self.viewTableView.frame;
    }
    
    [super viewDidLoad];
    [self initHeaderColor];
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
	
	btnSelectedEncours = YES;
	btnSelectedRegles = NO;
	btnSelectedClotures=NO;
	self.buttonEncours.selected = YES;
	self.buttonRegles.selected = NO;
	self.buttonClotures.selected=NO;
	
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
   
   
    
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
   
    [super orientationChanged:note];
    
      if (!isLandscape)
    {
        
         self.viewTableView.frame=oldFrameFacture;
        [self initHeaderColor];
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView //commenter

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(dataManager.isFournisseurContent || dataManager.isFournisseur)
//    {
//        return 44.0;
//
//
//    }
//    else if(dataManager.isClientContent || dataManager.isClient)
//    {
//        return 44.0;
//
//
//    }
//    else {
//         return 44.0;
//    }

//    else
//    {
//
//
//        if(isLandscape)
//        {
//            return [self getCellHeightForTextLandscape:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
//        }
//        else
//
//    return [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
//
//    }
    
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;
    
    
}
 

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (isLandscape) {
        
        static NSString *cellIdentifier_l;
       
        /*if ([DataManager isIphone5LandScape])
            {
                cellIdentifier_l = @"FactureContractCell_iphone5_l";
            }
            
            
            
            else
            {
                cellIdentifier_l = @"FactureContractCell_iphone_l";
            }
        
        */
      
       cellIdentifier_l = @"FactureContractCell_iphone_l";
        
        
        FactureContractCell *cell = (FactureContractCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (FactureContractCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        if(dataManager.isDemo)
        {
            if(dataManager.DemoredirectfactureFournisseur)
            {
                btnSelectedEncours = YES;
                btnSelectedRegles = NO;
                btnSelectedClotures=NO;
                self.buttonEncours.selected = YES;
                self.buttonRegles.selected = NO;
                self.buttonClotures.selected=NO;
            
                cell.numContract.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
                cell.dateMise.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];
                
                cell.datefin.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
                cell.duree.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_HT"];
                cell.montant.text= [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TAXE"];
                
                cell.statut.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"] ;
            
            }
            
            else
            {
            
                
                if (self.showFav)
                {
                    
                    cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                    cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                    cell.dateMise.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                    
                    cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                    cell.montant.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"];
                    cell.statut.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
                }
                else
                {
                    
                    
                    cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                    cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                    cell.dateMise.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                    
                    cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                    cell.montant.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"];
                    cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
                    
                    
                }
            
            
            }
        
        
        }
        
       else if(dataManager.isFournisseurContent)//do
        {

		
            if (self.buttonEncours.selected) {

//                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
//                cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];
//                cell.datefin.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
//                 cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"];
//                cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"];
				
				
				cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
				cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
				cell.datefin.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"];
				cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC_CMDE"];
				cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_COMMANDE"];
				
            }else {

                cell.numContract.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row] objectForKey:@"NO_MOUVEMENT"];
                cell.dateMise.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];//
                
                cell.datefin.text = [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
                cell.duree.text = [DataManager getFormatedNumero:[[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
                cell.montant.text= [[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TAXE"];
                
                cell.statut.text = [DataManager getFormatedNumero:[[[dataManager FiltredFacture] objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]] ;
                
            }

            
            

            
         
        }
 
        else if(dataManager.isClientContent)
        {
            
        
            
            if (self.showFav)
            {
                
                cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.dateMise.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                
                cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.montant.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
                cell.statut.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
            }
            else//do
            {
                
                
                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.dateMise.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
                
                cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
                cell.statut.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
                
                
            }
          

        }
        
        
            
            
     
        
        
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    
    {
        
  //   float cellclientheight=  [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
        
        static NSString *cellIdentifier_p = @"FactureContractCell_iphone_p";
        
        FactureContractCell *cell = (FactureContractCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (FactureContractCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.alpha = 0.5;
        
        if(dataManager.isDemo)
        {
            if(dataManager.DemoredirectfactureFournisseur)
            {
                cell.datefin.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
                
                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
                cell.duree.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]] ;
                
                cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"];
            }
            else
            {
                
                if (self.showFav)
                {
                    cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                    cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                    cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                    cell.dateMise.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"];
                }
                else
                {
                    
                    
                    
                    cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                    cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                    cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                    cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"];
                    
                }
            
            }
        
        
        
        }
        else if(dataManager.isFournisseurContent)//do
        {

          
            if (self.buttonEncours.selected) {
                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
                cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
                cell.datefin.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"];
                cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STATUT_COMMANDE"];
            }else {
                
                  cell.datefin.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
                
                 cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
                
                  cell.duree.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]] ;//  MT_HT_CMDE
                
                  cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];//LIBELLE
                
            }

          
            
        }
        else if(dataManager.isClientContent)
        {
        
            if (self.showFav)
            {
                cell.numContract.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.dateMise.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"];
            }
            else//do
            {
                
                cell.numContract.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                cell.datefin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
                cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"] ;
                cell.dateMise.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"];
                
            }
            
        
        }
        
        
        
       
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        
    
    if (!isLandscape)
    {
        if(dataManager.isClientContent || (dataManager.isDemo && !dataManager.DemoredirectfactureFournisseur))
        {		
                if (dataManager.isDemo)
                    [Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/listefactures"];
                else
                    [Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];
                self.isFromButton = NO;
                ListFacturesViewController_iphone * listFacturesViewController_iphone = [[ListFacturesViewController_iphone alloc]initWithNibName:@"ListFacturesViewController_iphone" bundle:nil];
                
                if (!self.showFav)
                    listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                else
                    listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                [self.navigationController pushViewController:listFacturesViewController_iphone animated:YES];
            
        }

        if(dataManager.isFournisseurContent || (dataManager.isDemo && dataManager.DemoredirectfactureFournisseur))
        {
            
            
            if (self.buttonEncours.selected) {
                
                self.isFromButton = NO;
                dataManager.isdataimmatriculation=NO;
                dataManager.isdatacommande=YES;
                dataManager.isdataengagement=NO;
                
                ListCommandesViewController_iphone * listCommandesViewController_iphone = [[ListCommandesViewController_iphone alloc]initWithNibName:@"ListCommandesViewController_iphone" bundle:nil];
                
                /*     if (!self.showFav)
                 listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
                 else
                 listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
                listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"] objectAtIndex:indexPath.row] objectForKey:@"NO_COMMANDE"];
                [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
                
            }else
            {
                self.isFromButton = NO;
                dataManager.isdatafacture=YES;
                dataManager.isdetailsengagement=NO;
                dataManager.isdetailscommande=NO;
                dataManager.isdetailsreglement=NO;
                dataManager.isdetailsimmatriculation=NO;
                dataManager.isdetailscontact=NO;
                
                
                
                /*
                 DetailsFactureViewController_iphone * detailsfactureviewcontroller = [[DetailsFactureViewController_iphone alloc]initWithNibName:@"DetailsFactureViewController_iphone" bundle:nil];*/
                DetailsBienCommandeViewController_iphone *detailsfactureviewcontroller=[[DetailsBienCommandeViewController_iphone alloc]initWithNibName:@"DetailsBienCommandeViewController_iphone" bundle:nil];
                
                detailsfactureviewcontroller.idmvt =  [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MVT"];
                
                [self.navigationController pushViewController:detailsfactureviewcontroller animated:YES];
                
            }
            
            


        }


    }
    
}
@end
