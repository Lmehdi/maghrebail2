//
//  MesFacturesViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesFacturesViewController.h"

@interface MesFacturesViewController ()

@end
NSString *switchtab;
@implementation MesFacturesViewController
@synthesize arrayFav, showFav;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
		showFav = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
  
    //[self.buttonEncours setTitle:@"Dossier incomplet" forState:UIControlStateNormal];
  
	self.arrayFav = [NSMutableArray array];
    
    [self initHeaderColor];
    
    
    if(dataManager.isFournisseurContent)
    {
        self.buttonEncours.hidden=NO;
         self.buttonEncours.selected = YES;
        self.numcontrat.text=@"N Commande";
        self.datemiseenloyer.text=@"Client";
        self.duree.text=@"Statut Affaire";
        self.datefin.text=@"Montant HT";
        self.statut.text=@"Montant TTC";
        [Flurry logEvent:@"MesFacturesFournisseur"];
        
    }
    if(dataManager.isDemo)
    {
        if(dataManager.DemoredirectfactureFournisseur)
        {
            self.numcontrat.text=@"Référence Maghrebail";
            self.datemiseenloyer.text=@"N°Facture fournisseur";
            self.duree.text=@"Montant TTC";
            self.datefin.text=@"Client";
            self.statut.text=@"Montant HT";
        }
        else
        {
            self.buttonEncours.hidden=YES;
            self.buttonRegles.hidden=YES;
            self.buttonClotures.hidden=YES;
            self.numcontrat.text=@"N° CONTRAT";
            self.datemiseenloyer.text=@"DATE DE MISE EN LOYER";
            self.duree.text=@"DURÉE";
            self.datefin.text=@"DATE FIN";
            self.statut.text=@"STATUT";
        
        }
    
    
    }
    if(dataManager.isClientContent) //do
    {
        self.numcontrat.text=@"N° CONTRAT";
        self.datemiseenloyer.text=@"DATE DE MISE EN LOYER";
        self.duree.text=@"DURÉE";
        self.datefin.text=@"DATE FIN";
        self.statut.text=@"STATUT";
         [Flurry logEvent:@"MesFacturesClient"];
        
    }
    
    
    
}

-(void) visibilityHeaderTable {
    
    if(dataManager.isFournisseurContent)
    {
        if (self.buttonEncours.selected) {
            self.numcontrat.text=@"N Commande";
            self.datemiseenloyer.text=@"Client";
            self.duree.text=@"Statut Affaire";
            self.datefin.text=@"Montant HT";
            self.statut.text=@"Montant TTC";
            
        }else {
            self.numcontrat.text=@"Référence Maghrebail";
            self.datemiseenloyer.text=@"N°Facture fournisseur";
            self.duree.text=@"Montant TTC";
            self.datefin.text=@"Client";
            self.statut.text=@"Montant HT";
        }
    }
    
}


- (void)dealloc
{
	[arrayFav release];
	[_btnFav release];
    [_labelFavoris release];
    [_numcontrat release];
    [_datemiseenloyer release];
    [_datefin release];
    [_duree release];
    [_Rencours release];
    [_Rregles release];
    [_Rvalides release];
    [_Ncontrat release];
    [_datemiseenloyer release];
    [_datefin release];
    [_duree release];
    [_statut release];
    [_textfieldfilteresult release];
    [_buttonpush release];
    [_first release];
    [_second release];
    [_third release];
    [_fourth release];
    [_last release];
	[super dealloc];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES FACTURES";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
    self.Rencours.hidden=YES;
    self.Rregles.hidden=YES;
    self.viewFiltre.hidden=YES;
    self.Rvalides.hidden=YES;
    
    //self.labelFavoris.hidden=YES;
    //self.btnFav.hidden=YES;
    self.textfieldfilteresult.hidden=YES;
    self.buttonpush.hidden=YES;
    NSMutableDictionary * params=nil;
    if (self.isFromButton)
    {
    btnSelectedEncours = YES;
    btnSelectedRegles = NO;
    btnSelectedClotures=NO;
    self.buttonEncours.selected = YES;
    self.buttonRegles.selected = NO;
    self.buttonClotures.selected=NO;
    switchtab=@"Encour";
    
   
	 if (dataManager.isDemo) {
         self.buttonEncours.hidden=YES;
         self.buttonRegles.hidden=YES;
         self.buttonClotures.hidden=YES;
        if(dataManager.DemoredirectfactureFournisseur) {
            self.viewFiltre.hidden=NO;
            self.textfieldfilteresult.hidden=NO;
            self.buttonpush.hidden=NO;
            isFiltreViewShowed = NO;
            
            /*    self.Rencours.hidden=NO;
                self.Rregles.hidden=NO;
                self.Rvalides.hidden=NO;
             */
           
              
            [self getRemoteContentAuth:FACTURE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:16];
        } else {
            self.labelFavoris.hidden=NO;
            self.btnFav.hidden=NO;
            
            params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
            [self postDataToServer:URL_MES_FACTURES_LIST andParam:params];
            
        }
    } else if(dataManager.isClientContent) {
        self.labelFavoris.hidden=NO;
        self.btnFav.hidden=NO;
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"], @"email", nil];
        
        self.buttonEncours.hidden=YES;
        self.buttonRegles.hidden=YES;
        self.buttonClotures.hidden=YES;
       
        [self postDataToServer:URL_MES_FACTURES_LIST andParam:params];
    } else if(dataManager.isFournisseurContent) {
        self.viewFiltre.hidden=NO;
        self.textfieldfilteresult.hidden=NO;
        self.buttonpush.hidden=NO;
        self.btnFav.hidden=YES;
        
        self.buttonEncours.selected = YES;
        self.buttonEncours.hidden=NO;
        self.buttonRegles.hidden =NO;
        self.buttonClotures.hidden=NO;

        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        
        NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_encours/%@", emailencoded, password, email];
        
        [self getRemoteContentfournisseur:url];
    }
		
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    
    [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
	if (dataManager.authentificationData) {
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	} else {
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (IBAction)encoursPushed:(UIButton *)sender {
    
    btnSelectedEncours = YES;
    btnSelectedRegles = NO;
    btnSelectedClotures=NO;
    self.buttonEncours.selected = YES;
    self.buttonRegles.selected = NO;
    self.buttonClotures.selected=NO;
      switchtab=@"Encour";
    [self visibilityHeaderTable];
  
    if(dataManager.DemoredirectfactureFournisseur) {
        [self getRemoteContentAuth:FACTURE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
    } else if(dataManager.isFournisseurContent) {
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        
        NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_encours/%@",emailencoded,password,email];
        
        [self getRemoteContentfournisseur:url];
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;
}

- (IBAction)ValiderPushed:(UIButton *)sender
{
    
    
    
    btnSelectedEncours = NO;
    btnSelectedRegles = NO;
    btnSelectedClotures=YES;
    self.buttonEncours.selected = NO;
    self.buttonRegles.selected = NO;
    self.buttonClotures.selected=YES;
      switchtab=@"valider";
    [self visibilityHeaderTable];
    
    if(dataManager.isDemo) {
        [self getRemoteContentAuth:FACTURE_CLOTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" ];
        
    } else if(dataManager.isFournisseurContent) {
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        

       NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_validees/%@",emailencoded,password,email];
        [self getRemoteContentfournisseur:url ];
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;
    
    
}

- (IBAction)reglesPushed:(UIButton *)sender
{
    
    btnSelectedEncours = NO;
    btnSelectedRegles = YES;
    btnSelectedClotures=NO;
	
	
    self.buttonEncours.selected = NO;
    self.buttonRegles.selected = YES;
    self.buttonClotures.selected=NO;
	
     [self visibilityHeaderTable];
      switchtab=@"reglee";
    if(dataManager.isDemo) {
        [self getRemoteContentAuth:FACTURE_REGLE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
    } else if(dataManager.isFournisseurContent) {
        NSString *email         =   [dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password      =   [dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded =   [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        NSString *url           =   [NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_reglees/%@", emailencoded, password, email];
        [self getRemoteContentfournisseur:url];
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame    = frameDateImgMinInitial;
    
    self.btnMinMontant.frame        = frameMontBtnMinInitial;
    self.labelMinMontant.frame      = frameMontLblMinInitial;
    self.viewMinMontant.frame       = frameViewMontMinInitial;
    
    self.btnMaxMontant.frame        = frameMontBtnMaxInitial;
    self.labelMaxMontant.frame      = frameMontLblMaxInitial;
    self.viewMaxMontant.frame       = frameViewMontMaxInitial;
    
    self.buttonMinDate.frame        = frameDateBtnMinInitial;
    self.labelMinDate.frame         = frameDateLblMinInitial;
    self.viewMinDate.frame          = frameViewDateMinInitial;
    
    self.buttonMaxDate.frame        = frameDateBtnMaxInitial;
    self.labelMaxDate.frame         = frameDateLblMaxInitial;
    self.viewMaxDate.frame          = frameViewDateMaxInitial;
}


-(void)updateView{
    
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.factures removeAllObjects];
        dataManager.factures = [NSMutableArray arrayWithArray:reponse];
        dataManager.FiltredFacture = (NSMutableArray *)[dataManager.factures copy];
        
        if (dataManager.factures.count != 0)
        {
            //remplir valuesMontant
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"MT_HT_CMDE" ascending:YES comparator:^(id obj1, id obj2) {
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                if ([obj1 doubleValue] > [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1 doubleValue] < [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];
            NSArray *sortedData;
            sortedData = [[dataManager.factures sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]] copy];
            
            NSString *str1 = [[[sortedData lastObject] objectForKey:@"MT_HT_CMDE"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *str2 = [[[sortedData objectAtIndex:0] objectForKey:@"MT_HT_CMDE"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            str1 = [str1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            str2 = [str2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            
            maxMontantValue = [str1 doubleValue];
            minMontantValue = [str2  doubleValue];
            self.labelMinMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", minMontantValue]];
            self.labelMaxMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", maxMontantValue]];
            int diff = (maxMontantValue - minMontantValue)/100;
            valuesMontant = [[NSMutableArray alloc] init];
            for (int i = minMontantValue; i <= maxMontantValue; i = i + diff)
            {
                [valuesMontant addObject:[NSString stringWithFormat:@"%d", i]];
                if (diff == 0)
                    break;
            }
            
            //[dataManager.ligneCredit removeAllObjects];
            dataManager.factures.array = sortedData;
            //remplir valuesDates
            NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"DATE_MOUVEMENT" ascending:YES comparator:^(id obj1, id obj2)
                                                {
                                                    return (NSComparisonResult)[[DataManager stringToDtae:obj1] compare:[DataManager stringToDtae:obj2]];
                                                }];
            NSArray *sortedDataWithDate = [[dataManager.factures sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptorDate,nil]] copy];
            maxDateValue = [[DataManager stringToDtae:[[sortedDataWithDate lastObject] objectForKey:@"DATE_MOUVEMENT"]] timeIntervalSince1970];
            minDatetValue = [[DataManager stringToDtae:[[sortedDataWithDate objectAtIndex:0] objectForKey:@"DATE_MOUVEMENT"]] timeIntervalSince1970];
            self.labelMinDate.text = [DataManager timeIntervaleToString:minDatetValue];
            self.labelMaxDate.text = [DataManager timeIntervaleToString:maxDateValue];
            int diffDate = (maxDateValue - minDatetValue)/100;
            valuesDates = [[NSMutableArray alloc] init];
            for (int i = minDatetValue; i <= maxDateValue; i = i + diffDate)
            {
                [valuesDates addObject:[NSString stringWithFormat:@"%d", i]];
                if (diffDate == 0) {
                    break;
                }
            }
            
            if (dataManager.factures.count == 1)
            {
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            else
            {
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            
            self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
            self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
            self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
            self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
        }
        
        
    }
    /* else
     {
     NSString *message = [header objectForKey:@"message"];
     [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
     }*/
   if (dataManager.isDemo) {
       self.buttonEncours.hidden=YES;
       self.buttonRegles.hidden=YES;
       self.buttonClotures.hidden=YES;
   }
    [_tableView reloadData];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (self.showFav)
	{
		return self.arrayFav.count;
	}
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        if(dataManager.isFournisseurContent|| dataManager.DemoredirectfactureFournisseur)
        {
            return [dataManager.FiltredFacture count];

        }
        else
        {
            return [[self.response objectForKey:@"response"] count];

        }
    
    }
    else
    {
        return 0;
    }
    
}

#pragma mark --
#pragma mark Pull to refresh

-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
    NSString *url=@"";
    NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
    NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
    NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
	
    if(dataManager.isClientContent || (dataManager.isDemo && !dataManager.DemoredirectfactureFournisseur))
    {
        if(dataManager.isDemo)
        {
            params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
            [self postDataToServer:URL_MES_FACTURES_LIST andParam:params];
        }
        else
        {
            [self postDataToServer:URL_MES_FACTURES_LIST andParam:params];

        }
    }
    

    
     if(dataManager.isFournisseurContent || dataManager.DemoredirectfactureFournisseur)
    {
        if(self.buttonEncours.selected==YES)
        {
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
                
            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_encours/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url ];
            }
            
            
            
        }
        if(self.buttonRegles.selected==YES)
        {
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_REGLE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
                
            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_reglees/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url];
                
            }
            
            
            
        }
        if(self.buttonClotures.selected==YES)
        {
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_CLOTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
                
            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_validees/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url];
            }
            
            
            
        }
        
        
    }
    
  
   

        
        
        
        
	}
    


/*
- (IBAction)filteraction:(id)sender
{
    NSString *url=@"";
    NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
    NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
    NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];

    
    UIButton *button=(UIButton *)sender;
    switch([button tag])
    {
        case 10:
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:16];

            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_encours/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url andTag:16];
            }
           
            
            break;
            
        case 11:
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_CLOTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:17];

            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_validees/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url andTag:17];
            }
    
           
            break;
        case 12:
            if(dataManager.isDemo)
            {
                [self getRemoteContentAuth:FACTURE_REGLE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:18];

            }
            else
            {
                url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_reglees/%@",emailencoded,password,email];
                [self getRemoteContentfournisseur:url andTag:18];
            }
         
            
        default:
            break;
            
    }
    
    
    
    
    
}

*/

-(void)orientationChanged:(NSNotification *)note
{
    
    [super orientationChanged:note];
    
   
    
        if(dataManager.DemoredirectfactureFournisseur || dataManager.isFournisseurContent)
        {
            self.viewFiltre.hidden=NO;
            self.textfieldfilteresult.hidden=NO;
            self.buttonpush.hidden=NO;
           if(isLandscape)
            {
            
                if (!self.buttonEncours.selected) {
                    self.numcontrat.text=@"Référence Maghrebail";
                    self.datemiseenloyer.text=@"N°Facture fournisseur";
                    self.duree.text=@"Montant TTC";
                    self.datefin.text=@"Client";
                    self.statut.text=@"Montant HT";
                }else {
                    self.numcontrat.text=@"N Commande";
                    self.datemiseenloyer.text=@"Client";
                    self.duree.text=@"Statut Affaire";
                    self.datefin.text=@"Montant HT";
                    self.statut.text=@"Montant TTC";
                    self.buttonEncours.selected = YES;
                }
               
            }
            else
            {
                if ([switchtab isEqualToString:@"Encour"]) {
                    
                    self.numcontrat.text=@"N° Commande";
                    self.datemiseenloyer.text=@"Client";
                    self.duree.text=@"Statut Affaire";
                    self.datefin.text=@"Montant HT";
                    self.statut.text=@"Montant TTC";
                
                }
                else {
                    
                    self.numcontrat.text=@"Référence Maghrebail";
                    self.datemiseenloyer.text=@"N°Facture fournisseur";
                    self.duree.text=@"Montant HT";
                    self.datefin.text=@"Client";
                    self.statut.text=@"Montant TTC";
                

                }
                
                
            }

        }
        else
        {
         
            
            if(isLandscape)
            {
                self.numcontrat.text=@"N° CONTRAT";
                self.datemiseenloyer.text=@"DATE DE MISE EN LOYER";
                //COM
                self.duree.text=@"STATUT";
                self.datefin.text=@"DATE FIN";
                self.statut.text=@"DURÉE";
                
                
            }
            else
            {
                self.buttonEncours.hidden=YES;
                self.buttonRegles.hidden=YES;
                self.buttonClotures.hidden=YES;
                self.numcontrat.text=@"N° CONTRAT";
                self.datemiseenloyer.text=@"DATE DE MISE EN LOYER";
                self.duree.text=@"DURÉE";
                self.datefin.text=@"DATE FIN";
                self.statut.text=@"STATUT";
            }

        
        }
    
  

    
	if (!isLandscape)
	{
        
        if(IS_IPHONE_5){
            
            
            self.viewTableView.frame=CGRectMake(0, 74, 375, 353);
        }
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
       
	}
    [_tableView reloadData];
    if (dataManager.factures.count == 1)
    {
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    else
    {
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    if (!isLandscape)
    {
        [self filtrePushed:nil];
    }
    if (!isLandscape)
    {
        [super filtrePushed:nil];
        if ([switchtab isEqualToString:@"Encour"])
        {
            self.buttonEncours.selected = YES;
            self.buttonRegles.selected = NO;
            self.buttonClotures.selected=NO;
        }
        else if ([switchtab isEqualToString:@"reglee"])
        {
            self.buttonEncours.selected = NO;
            self.buttonRegles.selected = YES;
            self.buttonClotures.selected=NO;
        }
        else
        {
            if([switchtab isEqualToString:@"valider"])
            {
                self.buttonEncours.selected = NO;
                self.buttonRegles.selected = NO;
                self.buttonClotures.selected=YES;
                
                
            }
            
            
        }
        self.labelMinDate.text  = [NSString stringWithFormat:@"%@", self.strMinDate];
        self.labelMinMontant.text = [NSString stringWithFormat:@"%@", self.strMinMont];
        self.labelMaxMontant.text = [NSString stringWithFormat:@"%@", self.strMaxMont];
        self.labelMaxDate.text = [NSString stringWithFormat:@"%@", self.strMaxDate];
    }
    
    
    self.imgViewScrollMontnat.frame = frameMontImgMin;
    self.imgViewScrollDate.frame=frameDateImgMin;
    
    self.btnMinMontant.frame=frameMontBtnMin;
    self.labelMinMontant.frame=frameMontLblMin;
    self.viewMinMontant.frame=frameViewMontMin;
    
    self.btnMaxMontant.frame=frameMontBtnMax;
    self.labelMaxMontant.frame=frameMontLblMax;
    self.viewMaxMontant.frame=frameViewMontMax;
    
    self.buttonMinDate.frame=frameDateBtnMin;
    self.labelMinDate.frame=frameDateLblMin;
    self.viewMinDate.frame=frameViewDateMin;
    
    self.buttonMaxDate.frame=frameDateBtnMax;
    self.labelMaxDate.frame=frameDateLblMax;
    self.viewMaxDate.frame=frameViewDateMax;
    
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (IBAction)filtrePushed:(UIButton *)sender
{
    [super filtrePushed:sender];
    
    if (isFiltreViewShowed)
    {
        if (btnSelectedEncours)
        {
            self.buttonEncours.selected = YES;
            self.buttonRegles.selected = NO;
            self.buttonClotures.selected=NO;
            
        }
        if (btnSelectedRegles)
        {
            self.buttonEncours.selected = NO;
            self.buttonRegles.selected = YES;
            self.buttonClotures.selected=NO;
            
        }
        else
        {
            if (btnSelectedClotures)
            {
                self.buttonEncours.selected = NO;
                self.buttonRegles.selected = NO;
                self.buttonClotures.selected = YES;
                
            }
        }
    } 
}

- (IBAction)showFav:(id)sender {
	
	[arrayFav removeAllObjects];
	if (!showFav)
	{
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/mesfactures/Favoris"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/Favoris"];
		showFav = YES;
		self.btnFav.selected = YES;
        self.labelFavoris.text=@"LISTE";
		for (NSInteger i = 0 ; i < [[self.response objectForKey:@"response"] count]; i++)
		{
			NSString *str = [[[self.response objectForKey:@"response"] objectAtIndex:i] objectForKey:@"NUM_AFFAIRE"];
			
			if ([[NSUserDefaults standardUserDefaults] stringForKey:str])
			{
				if ([[[NSUserDefaults standardUserDefaults] stringForKey:str] isEqualToString:@"FACTURE"])
					[self.arrayFav addObject:[[self.response objectForKey:@"response"] objectAtIndex:i]];
			}
		}
	}
	else
	{
		showFav = NO;
		self.btnFav.selected = NO;
        self.labelFavoris.text=@"FAVORIS";
	}
	
	[_tableView reloadData];
}

- (void)viewDidUnload {
	[self setBtnFav:nil];
	[super viewDidUnload];
}
@end
