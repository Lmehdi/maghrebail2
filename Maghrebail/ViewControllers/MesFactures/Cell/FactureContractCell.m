//
//  FactureContractCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "FactureContractCell.h"

@implementation FactureContractCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numContract release];
    [_datefin release];
    [_duree release];
    [_montant release];
    [_dateMise release];
    [_statut release];
    [_flecheimage release];
    [super dealloc];
}
@end
