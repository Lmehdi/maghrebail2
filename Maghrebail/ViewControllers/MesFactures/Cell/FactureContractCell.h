//
//  FactureContractCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FactureContractCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;
@property (retain, nonatomic) IBOutlet UILabel *numContract;
@property (retain, nonatomic) IBOutlet UILabel *datefin;
@property (retain, nonatomic) IBOutlet UILabel *duree;
@property (retain, nonatomic) IBOutlet UILabel *montant;
@property (retain, nonatomic) IBOutlet UILabel *dateMise;
@property (retain, nonatomic) IBOutlet UILabel *statut;
@end
