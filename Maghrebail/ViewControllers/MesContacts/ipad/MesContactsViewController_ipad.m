//
//  MesContactsViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 13/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesContactsViewController_ipad.h"
#import "ContactCell_ipad.h"

@interface MesContactsViewController_ipad ()

@end

@implementation MesContactsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *cellIdentifier_p = @"ContactCell_ipad";
	
	ContactCell_ipad *cell = (ContactCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
	if (cell == nil)
	{
		cell = (ContactCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
	}
	cell.alpha = 0.5;
	
	cell.navigationController2nd = self.navigationController;
//	cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ville"];
//	cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"nom"];
//	cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"email"] ;
//	cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"telephone"];
    if(dataManager.isClientContent)
    {
        cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ville"];
        cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"nom"];
        cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"email"] ;
        cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"telephone"];
    }
    if(dataManager.isFournisseurContent)
    {
        NSString *direction=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
        NSString *contactname=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
        NSString *emailcontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
           NSString *prenom=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"PRENOM_CONTACT"] ;
        NSString *mobilecontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
        
        
        if(direction == nil ||  [direction isKindOfClass:[NSNull class]])
        {
            cell.labelVille.text   = @""  ;
            
        }
        else
        {
            cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
            
        }
        
        if(contactname == nil ||  [contactname isKindOfClass:[NSNull class]])
        {
            cell.labelNom.text   = @""  ;
            
        }
        else
        {
            cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
        }
        
        
        if(emailcontact == nil ||  [emailcontact isKindOfClass:[NSNull class]])
        {
            cell.labelEmail.text   = @""  ;
            
        }
        else
        {
            cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
        }
        
        
        if(prenom == nil ||  [prenom isKindOfClass:[NSNull class]])
        {
            cell.prenom.text   = @""  ;
            
        }
        else
        {
            cell.prenom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"PRENOM_CONTACT"] ;
        }
        
        
        
        if(mobilecontact == nil ||  [mobilecontact isKindOfClass:[NSNull class]])
        {
            NSString *directContact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECT_CONTACT"];
            
            if(directContact == nil ||  [directContact isKindOfClass:[NSNull class]])
                
                
            {
                NSString *standarContact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STANDARD_AUTO"];
                
                
                if(standarContact == nil ||  [standarContact isKindOfClass:[NSNull class]])
                    
                    
                {
                    cell.labelTel.text   = @""  ;
                }else{
                    cell.labelTel.text   = standarContact ;
                }
                
            }else{
                cell.labelTel.text   = directContact ;
            }
            
        }
        else
        {
            cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
            
        }
        
        
        
        
    }

	
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
	return cell;
    
}



@end
