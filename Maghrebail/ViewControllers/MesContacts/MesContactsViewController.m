//
//  MesContactsViewController.m
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesContactsViewController.h"

@interface MesContactsViewController ()

@end

@implementation MesContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [self initHeaderColor];
	  
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES CONTACTS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self initHeaderColor];
    
	if( self.isFromButton)
	{
	//	[self showProgressHUD:YES];
        if(dataManager.isClientContent)
        {
        
        [self getRemoteContent:URL_MES_CONTACTS];
        
        }
        else if(dataManager.isFournisseurContent)
        {
            self.villelabel.text=@"Département";
            self.nom_contact.text=@"Nom";
            self.email.text=@"E-mail";
            self.telephone.text=@"Portable";
            NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
            NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
            NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
            
            NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/contacts",emailencoded,password];
            
            [self getRemoteContentfournisseur:url];
            
            
        }
       
        
        
		
	}
	if (!isLandscape)
	{ 
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateView{
    [super updateView];
    
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.contacts removeAllObjects];
        dataManager.contacts = [NSMutableArray arrayWithArray:reponse]; 
	
	}
    /*else
    {
        NSString *message = [header objectForKey:@"message"];
        [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
    }*/
    [_tableView reloadData];
}
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        return [[self.response objectForKey:@"response"] count];
        
    }
    else
    {
        return 0;
    }
    
    
}
 


-(void)emailPop:(NSString *)contactID
{
	
}

NSString *strTel;
-(void)telPop:(NSString *)contactID
{ 
}

#pragma mark - Orientation Change
 

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    if(dataManager.isFournisseurContent)
    {
        if(!isLandscape)
        {
            self.villelabel.text=@"Département";
            self.nom_contact.text=@"Nom";
            self.email.text=@"E-mail";
            self.telephone.text=@"Portable";
        }
        else
        {
            self.villelabel.text=@"Département";
            self.nom_contact.text=@"Nom";
            self.email.text=@"E-mail";
            self.telephone.text=@"Portable";
        }
        
    }
    
    
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
	
    [_tableView reloadData];
	
	if (!IS_IPAD)
	{ 
		_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
		
		_refreshHeaderView.delegate = self;
		[_tableView addSubview:_refreshHeaderView];
		[_refreshHeaderView refreshLastUpdatedDate];
	}
}

#pragma mark - alertView methods
#pragma mark - Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    
    if(dataManager.isClientContent)
    {
        [self getRemoteContent:URL_MES_CONTACTS];

    
    }
    if(dataManager.isFournisseurContent)
    {
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        
        NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/contacts",emailencoded,password];
        
        [self getRemoteContentfournisseur:url];
    
    }
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
    NSString * msgResult= nil ;
    NSString * msgResult2= nil ;
	
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msgResult = @"Envoie annulé";
            msgResult2 = @"Votre avez annulé l'opération et aucun message n'a été mis en file d'attente";
            break;
        case MFMailComposeResultSaved:
            msgResult = @"Message enregistré";
            msgResult2 = @"Vous avez sauvé le message dans les brouillons";
            break;
        case MFMailComposeResultSent:
            msgResult = @"Message envoyé";
            msgResult2 = @"Votre message a été bien envoyé. Merci pour votre commentaire";
            // flurry log event
            break;
        case MFMailComposeResultFailed:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Le message n'a pas été sauvé ou mis en file d'attente, probablement en raison d'une erreur";
            break;
        default:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Message non envoyé";
            break;
    }
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgResult message:msgResult2 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}
 
- (void)dealloc {
    [_villelabel release];
    [_nom_contact release];
    [_email release];
    [_telephone release];
    [super dealloc];
}
@end
