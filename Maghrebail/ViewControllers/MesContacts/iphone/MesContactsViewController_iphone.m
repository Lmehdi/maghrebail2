//
//  MesContactsViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MesContactsViewController_iphone.h"
#import "ContactCell_iphone.h"
#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@interface MesContactsViewController_iphone ()

@end

@implementation MesContactsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];

    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark --
#pragma mark TableView


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;
}
 

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	ContactCell_iphone *cell = nil;
	if (isLandscape)
    {
		static NSString *cellIdentifier_l;
		
	/*	if ([DataManager isIphone5LandScape])
		{
			cellIdentifier_l = @"ContactCell_iphone5_l";
		}
		else
        {
            cellIdentifier_l = @"ContactCell_iphone_l";

			
        }
     */
        cellIdentifier_l = @"ContactCell_iphone_l";

		
		cell = (ContactCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
		if (cell == nil)
		{
			cell = (ContactCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
		}
		cell.alpha = 0.5;
        if(dataManager.isClientContent)
        {
            cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ville"];
            cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"nom"];
            cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"email"] ;
            cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"telephone"];
        }
        if(dataManager.isFournisseurContent)
        {
            NSString *direction=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
            NSString *contactname=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
            NSString *emailcontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
            NSString *mobilecontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
            
            
            if(direction == nil ||  [direction isKindOfClass:[NSNull class]])
            {
                cell.labelVille.text   = @""  ;
                
            }
            else
            {
                cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
                
            }
            
            if(contactname == nil ||  [contactname isKindOfClass:[NSNull class]])
            {
                cell.labelNom.text   = @""  ;
                
            }
            else
            {
                cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
            }
            
            
            if(emailcontact == nil ||  [emailcontact isKindOfClass:[NSNull class]])
            {
                cell.labelEmail.text   = @""  ;
                
            }
            else
            {
                cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
            }
            
            
            if(mobilecontact == nil ||  [mobilecontact isKindOfClass:[NSNull class]])
            {
                cell.labelTel.text   = @""  ;
                
            }
            else
            {
                cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
                
            }

            
            
        
        }
		
		
		cell.parrent = self;
		cell.navigationController2nd = self.navigationController;
		switch (indexPath.row % 2) {
			case 0:
				cell.contentView.backgroundColor = [UIColor whiteColor];
				break;
			case 1:
				cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
				break;
			default:
				break;
		}

	}
	else
	{
		static NSString *cellIdentifier_p = @"ContactCell_iphone_p";
        
     /*   if([DataManager isIphone5])
        {
        cellIdentifier_p=@"ContactCell_iphone5_p";
        
        }
      */
        
		cell = (ContactCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
		if (cell == nil)
		{
			cell = (ContactCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
		}
		cell.alpha = 0.5;
        
        if(dataManager.isClientContent)
        {
            cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ville"];
            cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"nom"];
            cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"email"] ;
            cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"telephone"];
         //   cell.flecheimage.frame=CGRectMake(cell.flecheimage.frame.origin.x,23, cell.flecheimage.frame.size.width, cell.flecheimage.frame.size.height);
        }
        if(dataManager.isFournisseurContent)
        {
        //    NSString *direction=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
         //   NSString *contactname=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
          //  NSString *emailcontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
            
            
            NSString *mobilecontact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
          //  cell.flecheimage.frame=CGRectMake(cell.flecheimage.frame.origin.x,23, cell.flecheimage.frame.size.width, cell.flecheimage.frame.size.height);

            
                cell.labelVille.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECTION"];
                cell.labelNom.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NOM_CONTACT"];
            

            
                cell.labelEmail.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"EMAIL_CONTACT"] ;
           
            
            if(mobilecontact == nil ||  [mobilecontact isKindOfClass:[NSNull class]])
                
                
            {
                
                 NSString *directContact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DIRECT_CONTACT"];
                
                if(directContact == nil ||  [directContact isKindOfClass:[NSNull class]])
                    
                    
                {
                     NSString *standarContact=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"STANDARD_AUTO"];
                    
                    
                    if(standarContact == nil ||  [standarContact isKindOfClass:[NSNull class]])
                        
                        
                    {
                           cell.labelTel.text   = @""  ;
                    }else{
                       cell.labelTel.text   = standarContact ;
                    }
                    
                }else{
                      cell.labelTel.text   = directContact ;
                }
             
                
            }
            else
            {
                cell.labelTel.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MOBILE_CONTACT"];
                
            }

            
            
        }
        
		
		cell.navigationController2nd = self.navigationController;
		cell.parrent = self;
		
		switch (indexPath.row % 2) {
			case 0:
				cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
				break;
			case 1:
				cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
				break;
			default:
				break;
		}

	} 
		return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isLandscape)
    {
        /*   if (dataManager.isDemo)
         
         else
         [Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];*/
//        self.isFromButton = NO;
//        DetailsBienCommandeViewController_iphone *  detailsBienCommandeViewController_iphone  = [[DetailsBienCommandeViewController_iphone alloc]initWithNibName:@"DetailsBienCommandeViewController_iphone" bundle:nil];
//        dataManager.isdetailscontact=YES;
//        dataManager.isdetailsengagement=NO;
//        dataManager.isdetailscommande=NO;
//        dataManager.isdetailsreglement=NO;
//        dataManager.isdetailsimmatriculation=NO;
//        dataManager.isdatafacture=NO;
//
//
//
//        detailsBienCommandeViewController_iphone.dictionnary=[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row];
        
        /*     if (!self.showFav)
         listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
         else
         listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
        
      //  [self.navigationController pushViewController:detailsBienCommandeViewController_iphone animated:YES];
    }
    
    
}







@end
