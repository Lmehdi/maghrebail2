//
//  MesContactsViewController.h
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface MesContactsViewController : BaseViewController <MFMailComposeViewControllerDelegate>
@property (retain, nonatomic) IBOutlet UILabel *nom_contact;
@property (retain, nonatomic) IBOutlet UILabel *villelabel;
@property (retain, nonatomic) IBOutlet UILabel *email;
@property (retain, nonatomic) IBOutlet UILabel *telephone;
-(void)emailPop:(NSString *)contactID;
-(void)telPop:(NSString *)contactID;
-(void) initHeaderColor;
@end
