//
//  ContactCell.h
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h> 

#import <MessageUI/MessageUI.h> 
#import "MesContactsViewController.h"

@class MesContactsViewController;
@class ContactCell;
@protocol ContactCellDelegate <NSObject>

@required
-(void)contactCell:(ContactCell *)messagerieCelle emailDelegatePushed:(int)contactId;

@required
-(void)contactCell:(ContactCell *)messagerieCelle telDelegatePushed:(int)contactId;

@end

@interface ContactCell : UITableViewCell<MFMailComposeViewControllerDelegate>
{
	
    id<ContactCellDelegate>delegate;
}

@property (retain, nonatomic) UINavigationController *navigationController2nd;
@property (retain, nonatomic) IBOutlet UILabel *labelVille;
@property (retain, nonatomic) IBOutlet UILabel *labelNom;
@property (retain, nonatomic) IBOutlet UILabel *labelEmail;
@property (retain, nonatomic) IBOutlet UILabel *labelTel;
@property (retain, nonatomic) IBOutlet UILabel *prenom;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;
@property (retain, nonatomic) MesContactsViewController *parrent;

@property (assign) int contactID;

@property(retain)id delegate;


-(IBAction)emailPushed:(id)sender;
-(IBAction)telPushed:(id)sender;

@end
