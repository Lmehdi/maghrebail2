//
//  ContactCell.m
//  Maghrebail
//
//  Created by MAC on 11/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

@synthesize delegate,contactID;
@synthesize parrent;
@synthesize navigationController2nd;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
	[navigationController2nd release];
    [_labelVille release];
    [_labelNom release];
    [_labelEmail release];
    [_labelTel release];
	[parrent release];
    [_flecheimage release];
    [_flecheimage release];
    [super dealloc];
}


-(IBAction)emailPushed:(id)sender
{
	[Flurry logEvent:@"Home/EspaceAbonnés/mescontacts/mail"];
	if ([MFMailComposeViewController canSendMail])
	{
		// device is configured to send mail
		MFMailComposeViewController* controller = [[[MFMailComposeViewController alloc] init] retain];
		controller.mailComposeDelegate = self;
		[controller setSubject:@"Application Maghrebail iOS"];
		[controller setMessageBody:@"" isHTML:YES];
		[controller setToRecipients:[NSArray arrayWithObject:_labelEmail.text]];
		NSLog(@"controller MAil %@", controller.description);
		[self.navigationController2nd presentModalViewController:controller animated:YES];
		
	}
	else
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Veuillez configurer votre compte messagerie" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
 
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [controller dismissModalViewControllerAnimated:YES];
    
    NSString * msgResult= nil ;
    NSString * msgResult2= nil ;
	
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msgResult = @"Envoie annulé";
            msgResult2 = @"Votre avez annulé l'opération et aucun message n'a été mis en file d'attente";
            break;
        case MFMailComposeResultSaved:
            msgResult = @"Message enregistré";
            msgResult2 = @"Vous avez sauvé le message dans les brouillons";
            break;
        case MFMailComposeResultSent:
            msgResult = @"Message envoyé";
            msgResult2 = @"Votre message a été bien envoyé. Merci pour votre commentaire";
            // flurry log event
            break;
        case MFMailComposeResultFailed:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Le message n'a pas été sauvé ou mis en file d'attente, probablement en raison d'une erreur";
            break;
        default:
            msgResult = @"Envoie échoué";
            msgResult2 = @"Message non envoyé";
            break;
    }
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgResult message:msgResult2 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}

-(IBAction)telPushed:(id)sender
{
	[Flurry logEvent:@"Home/EspaceAbonnés/mescontacts/call"];
	[self.delegate contactCell:self telDelegatePushed:self.contactID];
	
	UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
													  message:[NSString stringWithFormat:@"Voulez-vous appeler le numéro %@", _labelTel.text]
													 delegate:self
											cancelButtonTitle:@"Non, Merci!"
											otherButtonTitles:@"Appeler", nil];
	
	[message show];
	[message release];
}

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	
	if (buttonIndex == 1)
	{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", _labelTel.text]]];
	}
	
}

@end
