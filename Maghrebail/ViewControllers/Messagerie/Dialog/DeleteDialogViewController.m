//
//  DeleteDialogViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DeleteDialogViewController.h"

@interface DeleteDialogViewController ()

@end

@implementation DeleteDialogViewController
@synthesize delegate;
@synthesize messageID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma mark --
#pragma mark show hide
-(void)showDialog{
    
    self.view.frame = CGRectMake(self.view.frame.origin.x , -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelay:0];
    self.view.frame = CGRectMake(self.view.frame.origin.x , 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

}
-(void)hideDialog{
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.2];
    [UIView beginAnimations:@"MoveAndScaleAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelay:0];
    self.view.frame = CGRectMake(self.view.frame.origin.x ,  -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)confirmer:(id)sender {
    [[self delegate] confirmeDeleteMessage:self confirm:YES messageID:self.messageID ];
    
}
- (IBAction)annuler:(id)sender {
    [[self delegate] confirmeDeleteMessage:self confirm:NO messageID:self.messageID];
}

-(void)dealloc{
    
    [delegate release];
    [super dealloc];
}
@end
