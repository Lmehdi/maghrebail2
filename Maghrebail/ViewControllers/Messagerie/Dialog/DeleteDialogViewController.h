//
//  DeleteDialogViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/1/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DeleteDialogViewController;
@protocol DeleteMessageDialogDelegate <NSObject>

@required
-(void)confirmeDeleteMessage:(DeleteDialogViewController * )dialog confirm:(BOOL)confirme messageID:(NSString *)messageID;

@end

@interface DeleteDialogViewController : UIViewController{
    id<DeleteMessageDialogDelegate>delegate;
}
@property(retain)id delegate;
@property(nonatomic, retain)NSString * messageID;

- (IBAction)confirmer:(id)sender;
- (IBAction)annuler:(id)sender;

-(void)showDialog;
-(void)hideDialog;
@end
