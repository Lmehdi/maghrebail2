//
//  MessagerieDetailsViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/22/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieDetailsViewController_iphone.h"

@interface MessagerieDetailsViewController_iphone ()

@end

@implementation MessagerieDetailsViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
   
    
    
    [self initHeaderColor];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
} 

-(void)orientationChanged:(NSNotification *)note
{
	if (isLandscape)
	{
		shouldAddHeader = YES;
        [self initHeaderColor];
	}
	else
		shouldAddHeader = NO;
    [super orientationChanged:note]; 
}


@end
