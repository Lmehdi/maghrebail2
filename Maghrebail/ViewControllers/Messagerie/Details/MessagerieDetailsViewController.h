//
//  MessagerieDetailsViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/22/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MessagerieDetailsViewController : BaseViewController


@property (retain, nonatomic) NSString *dateText;
@property (retain, nonatomic) NSString *sujetText;
@property (retain, nonatomic) NSString *contenueText;
@property (retain, nonatomic) NSString *idmessage;
@property (retain, nonatomic,setter=setdictionnary:) NSMutableDictionary *dictionnary;
@property (retain, nonatomic) IBOutlet UILabel *date;
@property (retain, nonatomic) IBOutlet UILabel *sujet;
@property (retain, nonatomic) IBOutlet UILabel *contenue;
-(void) initHeaderColor;

@end
