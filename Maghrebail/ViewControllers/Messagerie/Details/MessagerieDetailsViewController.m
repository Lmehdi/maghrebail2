//
//  MessagerieDetailsViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/22/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieDetailsViewController.h"

@interface MessagerieDetailsViewController ()

@end

@implementation MessagerieDetailsViewController
@synthesize dateText;
@synthesize sujetText;
@synthesize contenueText;
 
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
        // Custom initialization
        shouldAutorate = YES;
    } 
    return self;
}
-(void) initHeaderColor{
	
		headerView.labelheaderTitre.text=@"MA MESSAGERIE";
		headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
		self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
		dataManager.typeColor=@"orange";
	
}


//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//	if (!isLandscape)
//	{
//		[self initHeaderColor];
//		
//	}
//	
//}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	if (!isLandscape)
		{
		[self initHeaderColor];
		
		}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self initHeaderColor];
    if(dataManager.isClientContent || dataManager.isDemo)
    {
       [self showProgressHUD:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(showContenu:)
                                                     name:@"showContenu"
                                                   object:nil];
        
        _date.text = [DataManager getFormateDate:self.dateText];
        _sujet.text = self.sujetText;
        //_contenue.text = self.contenueText;
        
        [self adjustLabelHeight:_contenue string:_contenue.text];

    
    }
    
    if(dataManager.isFournisseurContent)
    {
    
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        
        NSString *url=[NSString stringWithFormat:@"http://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/details_message/%@",emailencoded,password,self.idmessage];
        
        [self getRemoteContentfournisseur:url];

    
    
    }
		// Do any additional setup after loading the view.
	
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}




- (void)showContenu:(NSNotification *)notification
{
	if ([notification.name isEqualToString:@"showContenu"])
	{
		[self showProgressHUD:NO]; 
		if ( [[[notification.userInfo objectForKey:@"response"] objectAtIndex:0] objectForKey:@"TEXT"])
		{
			self.contenueText = [[[notification.userInfo objectForKey:@"response"] objectAtIndex:0] objectForKey:@"TEXT"];
			self.contenue.text = [[[notification.userInfo objectForKey:@"response"] objectAtIndex:0] objectForKey:@"TEXT"];
			CGSize maximumLabelSize = CGSizeMake(self.contenue.frame.size.width,9999);
			
			CGSize expectedLabelSize = [self.contenue.text sizeWithFont:self.contenue.font constrainedToSize:maximumLabelSize lineBreakMode:self.contenue.lineBreakMode];
			
			//adjust the label the the new height.
			
			CGRect newFrame = self.contenue.frame;
			newFrame.size.height = expectedLabelSize.height;
			self.contenue.frame = newFrame;
		}
	}
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
	
	if (!isLandscape)
	{ 
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
	
	_date.text = [DataManager getFormateDate:self.dateText];
    _sujet.text = self.sujetText;
    _contenue.text = self.contenueText;
    
    [self adjustLabelHeight:_contenue string:_contenue.text];
	
 [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
 [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
} 



-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        

        _date.text = [DataManager getFormateDate:self.dateText];
        _sujet.text = self.sujetText;
        self.sujet = [[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"OBJET"];
        self.contenueText = [[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"TEXT"];
        self.contenue.text = [[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"TEXT"];
        CGSize maximumLabelSize = CGSizeMake(self.contenue.frame.size.width,9999);
        
        CGSize expectedLabelSize = [self.contenue.text sizeWithFont:self.contenue.font constrainedToSize:maximumLabelSize lineBreakMode:self.contenue.lineBreakMode];
        
        //adjust the label the the new height.
        
        CGRect newFrame = self.contenue.frame;
        newFrame.size.height = expectedLabelSize.height;
        self.contenue.frame = newFrame;

        
    }
     else
     {
     NSString *message = [header objectForKey:@"message"];
         [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
     }

}

     
- (void)goBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
	//[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v2_icone_menu.png"] forState:UIControlStateNormal];
	//[headerView setDelegate:self];
	[super viewWillDisappear:animated];
	[self showProgressHUD:NO];
	[[NSNotificationCenter defaultCenter] removeObserver:self];


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_date release];
    [_sujet release];
    [_contenue release];
    
    [dateText release];
    [sujetText release];
    [contenueText release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setDate:nil];
    [self setSujet:nil];
    [self setContenue:nil];
    [super viewDidUnload];
}
@end
