//
//  MessagerieCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieCell.h"

@implementation MessagerieCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)deleteMessageAction:(id)sender{
    [self.delegate messagerieCell:self deleteMessage:_messageID];
}
-(void)readMessageAction:(id)sender{
    [self.delegate messagerieCell:self markMessageAsRead:_messageID];
}

- (void)dealloc {
    [_sujet release];
    [_dateReception release];
    [_luBtn release];
    [_deleteBtn release];
    [delegate release];
    [super dealloc];
}
@end
