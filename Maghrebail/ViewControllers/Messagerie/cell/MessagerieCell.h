//
//  MessagerieCell.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessagerieCell;
@protocol MessagerieCellDeleteDelegate <NSObject>

@required
-(void)messagerieCell:(MessagerieCell *)messagerieCelle deleteMessage:(int)messageID;

@required
-(void)messagerieCell:(MessagerieCell *)messagerieCelle markMessageAsRead:(int)messageID;

@end

@interface MessagerieCell : UITableViewCell{
    
    id<MessagerieCellDeleteDelegate>delegate;
}

@property (retain, nonatomic) IBOutlet UILabel *sujet;
@property (retain, nonatomic) IBOutlet UILabel *dateReception;
@property (retain, nonatomic) IBOutlet UIButton *luBtn;
@property (retain, nonatomic) IBOutlet UIButton *deleteBtn;
@property (assign) int messageID;

@property(retain)id delegate;

-(IBAction)deleteMessageAction:(id)sender;
-(IBAction)readMessageAction:(id)sender;

@end
