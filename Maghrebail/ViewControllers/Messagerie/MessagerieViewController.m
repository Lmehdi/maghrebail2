//
//  MessagerieViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieViewController.h"

@interface MessagerieViewController ()

@end

@implementation MessagerieViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}
-(void) initHeaderColor{
   
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
- (void)viewWillAppear:(BOOL)animated
{ 
    [super viewWillAppear:animated];
    [self initHeaderColor];
	///if (self.isFromButton) {
        NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERSCLIENT"];
        
        
        NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
        if(datafound==nil){
            datafound=[dataManager.authentificationData objectForKey:@"TIERS"];
        }
		NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:datafound,@"tiers", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        if(dataManager.isClientContent || dataManager.isDemo)
        {
          
            [self postDataToServer:URL_MESSAGERIE andParam:params];

        }
        
        
        if(dataManager.isFournisseurContent)
        {
            NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERS"];
            
            
            NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
            NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
            NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
            NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
            if(datafound==nil){
                datafound=[dataManager.authentificationData objectForKey:@"TIERS"];
            }
            NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/messages/%@",emailencoded,password,datafound];
            
            [self getRemoteContentfournisseur:url];
            
        }
    
   // else
    //    [self showProgressHUD:NO];
     
////        if(dataManager.isFournisseurContent)
////        {
//            NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERS"];
//            
//            
//            NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
//            
//            NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
//            NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
//            NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
//            
//            if(dataManager.isFournisseurContent){
//                NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/messages/%@",emailencoded,password,datafound];
//                if(datafound==nil){
//                    datafound=[dataManager.authentificationData objectForKey:@"NUM_TETE_GROUPE"];
//                }
//                [self getRemoteContentfournisseur:url];
//              
//                
//            }else{
//                NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERSCLIENT"];
//                
//                
//                NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
//                if(datafound==nil){
//                    datafound=[dataManager.authentificationData objectForKey:@"TIERS"];
//                }
//                NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/messages/%@",emailencoded,password,datafound];
//                
//                [self getRemoteContentfournisseur:url];
//            }
//         
//        
//        }
////	}
//	else
//		[self showProgressHUD:NO];
    if(IS_IPHONE_5){
        headerView.btnRightHeader.frame=CGRectMake(280, headerView.btnRightHeader.frame.origin.y, headerView.btnRightHeader.frame.size.width, headerView.btnRightHeader.frame.size.height);
        
    }
	if (dataManager.authentificationData)
	{
        
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}

}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note]; 
	
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
		
		_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
}
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark UpdateView
-(void)updateView{
    [self showProgressHUD:NO];
    [super updateView];
    NSMutableDictionary * params;
    switch (mode) {
        case MESSAGERIE_MODE_NORMAL:
            [_tableView reloadData];
            break;
        case MESSAGERIE_MODE_DELETE:
            //[BaseViewController showAlert:nil message:[[[self.response objectForKey:@"response"]objectAtIndex:0]objectForKey:@"message"] delegate:nil confirmBtn:@"OK" cancelBtn:nil];
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"TIERS"],@"tiers", nil];
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
            
            [self postDataToServer:URL_MESSAGERIE andParam:params];
            break;
        case MESSAGERIE_MODE_READ:
            //[BaseViewController showAlert:nil message:[[[self.response objectForKey:@"response"]objectAtIndex:0]objectForKey:@"message"] delegate:nil confirmBtn:@"OK" cancelBtn:nil];
			 
			
			[[NSNotificationCenter defaultCenter] postNotificationName:@"showContenu" object:nil userInfo:self.response];
			
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"TIERS"],@"tiers", nil];
			
			if (dataManager.isDemo)
			{
				params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			}
            
            if(dataManager.isClientContent){
                
               [self postDataToServer:URL_MESSAGERIE andParam:params]; 
            }
            
            break;
        default:
            break;
    }
    mode = MESSAGERIE_MODE_NORMAL;
}

#pragma mark --
#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.response objectForKey:@"response"] count];
}
#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
//    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"TIERS"],@"tiers", nil];
//	if (dataManager.isDemo)
//	{
//		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
//	}
//    if(dataManager.isClientContent || dataManager.isDemo)
//    {
//        [self postDataToServer:URL_MESSAGERIE andParam:params];
//
//    }
//    if(dataManager.isFournisseurContent)
//    {
//        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
//        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
//        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
//        
//        NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/messages/%@",emailencoded,password,[dataManager.authentificationData objectForKey:@"TIERS"]];
//        
//        [self getRemoteContentfournisseur:url];
//    
//    }
    
    NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERSCLIENT"];
    
    
    NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
    if(datafound==nil){
        datafound=[dataManager.authentificationData objectForKey:@"TIERS"];
    }
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:datafound,@"tiers", nil];
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    if(dataManager.isClientContent || dataManager.isDemo)
    {
        
        [self postDataToServer:URL_MESSAGERIE andParam:params];
        
    }
    
    
    if(dataManager.isFournisseurContent)
    {
        NSData *datasave = [DataManager readDataIntoCachWith:@"CHANGERTIERS"];
        
        
        NSString *  datafound=[NSKeyedUnarchiver unarchiveObjectWithData:datasave];
        NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
        NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
        NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
        if(datafound==nil){
            datafound=[dataManager.authentificationData objectForKey:@"TIERS"];
        }
        NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/messages/%@",emailencoded,password,datafound];
        
        [self getRemoteContentfournisseur:url];
        
    }
}
#pragma mark --
#pragma mark Delete Message

-(void)deleteMessage:(NSString *)messageID{
    
    DeleteDialogViewController * confirmeDialog = [[DeleteDialogViewController alloc]initWithNibName:@"DeleteDialogViewController_iphone_p" bundle:nil];
    confirmeDialog.view.frame = CGRectMake(0, -confirmeDialog.view.frame.size.height, confirmeDialog.view.frame.size.width, confirmeDialog.view.frame.size.height);
    confirmeDialog.delegate = self;
    confirmeDialog.messageID = messageID;
    [self.view addSubview:confirmeDialog.view];
    [confirmeDialog showDialog];
   
}

-(void)markMessageRead:(NSString *)messageID{
    
    mode = MESSAGERIE_MODE_READ;
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:messageID,@"id", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_MESSAGERIE_READ andParam:params];
    
}

#pragma mark --
#pragma mark Delete Message Delegate
-(void)messagerieCell:(MessagerieCell *)messagerieCelle deleteMessage:(int)messageID{
	
    if (dataManager.isDemo)
	{
        [BaseViewController showAlert:nil message:@"Vous êtes en mode démo" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
	}
	
	[Flurry logEvent:@"Home/DécouvrirMaghrebail/Messagerie/Suppresionmessage"];
    [self deleteMessage:[NSString stringWithFormat:@"%d",messageID ]];
}

-(void)messagerieCell:(MessagerieCell *)messagerieCelle markMessageAsRead:(int)messageID{
	if (dataManager.isDemo)
	{
        [BaseViewController showAlert:nil message:@"Vous êtes en mode démo" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
	}
     [self markMessageRead:[NSString stringWithFormat:@"%d",messageID ]];
}

#pragma mark --
#pragma mark Delete Message Dialog Delegate
-(void)confirmeDeleteMessage:(DeleteDialogViewController * )dialog confirm:(BOOL)confirme messageID:(NSString *)messageID {
   
    if (confirme) {
        mode = MESSAGERIE_MODE_DELETE;
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:messageID,@"id", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
        [self postDataToServer:URL_MESSAGERIE_DELETE andParam:params];
        [self showLoadingView:YES];
    }
    [dialog hideDialog];
}
@end
