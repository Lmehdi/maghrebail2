//
//  MessagerieViewController_ipad.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/6/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieViewController_ipad.h"
#import "MessagerieCell_ipad.h"
#import "MessagerieDetailsViewController_ipad.h"

@interface MessagerieViewController_ipad ()

@end

@implementation MessagerieViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
	static NSString *cellIdentifier_p = @"MessagerieCell_ipad";
	
	MessagerieCell_ipad *cell = (MessagerieCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
	if (cell == nil)
	{
		cell = (MessagerieCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
	}
	cell.alpha = 0.5;
	cell.sujet.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
	cell.dateReception.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"]];
	cell.messageID = [(NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"] intValue];
	cell.delegate = self;
	if ([[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"VUE"] isEqualToString:@"1"]) {
		[cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagelu_iphone.png"] forState:UIControlStateNormal];
	}else{
		[cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagenonlu_iphone.png"] forState:UIControlStateNormal];
	} 
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
	return cell;
 
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//	if (dataManager.isDemo)
//		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/Messagerie/Corpsdemessage"];
//	else
//		[Flurry logEvent:@"Home/EspaceAbonnés/Messagerie/Corpsdemessage"];
//	self.isFromButton = NO;
//	mode = MESSAGERIE_MODE_READ;
//	NSString *str = (NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"];
//	NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:str,@"id", nil];
//	if (dataManager.isDemo)
//	{
//		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
//	}
//	[self postDataToServer:URL_MESSAGERIE_READ andParam:params];
//	ind = indexPath.row;
//	
//	MessagerieDetailsViewController_ipad * messagerieDetailsViewController_ipad = [[MessagerieDetailsViewController_ipad alloc]initWithNibName:@"MessagerieDetailsViewController_ipad" bundle:nil];
//	
//	messagerieDetailsViewController_ipad.dateText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"];
//	messagerieDetailsViewController_ipad.contenueText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
//	messagerieDetailsViewController_ipad.sujetText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
//	[self.navigationController pushViewController:messagerieDetailsViewController_ipad animated:YES];
    
    
    
    
    if (dataManager.isDemo)
        [Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/Messagerie/Corpsdemessage"];
    else
        [Flurry logEvent:@"Home/EspaceAbonnés/Messagerie/Corpsdemessage"];
    self.isFromButton = NO;
    MessagerieDetailsViewController_ipad * messagerieDetailsViewController_iphone = [[MessagerieDetailsViewController_ipad alloc]initWithNibName:@"MessagerieDetailsViewController_ipad" bundle:nil];
    mode = MESSAGERIE_MODE_READ;
    if(dataManager.isClientContent || dataManager.isDemo)
    {
        
        NSString *str = (NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"];
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:str,@"id", nil];
        if (dataManager.isDemo)
        {
            params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
        }
        [self postDataToServer:URL_MESSAGERIE_READ andParam:params];
        ind =(int) indexPath.row;
        
        
        
        messagerieDetailsViewController_iphone.dateText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"];
        messagerieDetailsViewController_iphone.contenueText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
        messagerieDetailsViewController_iphone.sujetText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
        
    }
    
    if(dataManager.isFournisseurContent)
    {
        
        
        messagerieDetailsViewController_iphone.dateText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"];
        messagerieDetailsViewController_iphone.sujetText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
        
        messagerieDetailsViewController_iphone.idmessage = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"];
    }
http://abderrahim.marbou%40maghrebail.ma:eac1cdd60eda56af4300224ccff966dd4b75992f@www.maghrebail.ma/leasebox/partenaires/api/rest/details_message/15
    
    [self.navigationController pushViewController:messagerieDetailsViewController_iphone animated:YES];
    
    
    
    
    
    
}

-(void)deleteMessage:(NSString *)messageID{
    
    DeleteDialogViewController * confirmeDialog = [[DeleteDialogViewController alloc]initWithNibName:@"DeleteDialogViewController_ipad" bundle:nil];
    confirmeDialog.view.frame = CGRectMake(0, -confirmeDialog.view.frame.size.height, confirmeDialog.view.frame.size.width, confirmeDialog.view.frame.size.height);
    confirmeDialog.delegate = self;
    confirmeDialog.messageID = messageID;
    [self.view addSubview:confirmeDialog.view];
    [confirmeDialog showDialog];
   
}

@end
