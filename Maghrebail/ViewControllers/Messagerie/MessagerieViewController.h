//
//  MessagerieViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MessagerieCell_iphone_l.h"
#import "MessagerieCell_iphone_p.h"
#import "DeleteDialogViewController.h"


@interface MessagerieViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate,MessagerieCellDeleteDelegate,DeleteMessageDialogDelegate>{
    
    int  mode;
	int ind;
    }


-(void)deleteMessage:(NSString *)messageID;
-(void)markMessageRead:(NSString *)messageID;
-(void) initHeaderColor;
@end
