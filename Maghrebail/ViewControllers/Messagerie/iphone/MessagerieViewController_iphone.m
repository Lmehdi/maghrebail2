//
//  MessagerieViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MessagerieViewController_iphone.h"

@interface MessagerieViewController_iphone ()

@end

@implementation MessagerieViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initTitre];
}
-(void) initTitre{
    headerView.labelheaderTitre.text=@"MA MESSAGERIE";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initTitre];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initTitre];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];

    if (!isLandscape)
    {
         [self initTitre];
         [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark --
#pragma mark -- TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (isLandscape)
		return 54.0;
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isLandscape) {
		static NSString *cellIdentifier_l;
	
       /* if ([DataManager isIphone5])
		{
			
			cellIdentifier_l = @"MessagerieCell_iphone5_l";
		}
		else
			cellIdentifier_l = @"MessagerieCell_iphone_l";
        */
        cellIdentifier_l = @"MessagerieCell_iphone_l";

		
        MessagerieCell_iphone_l *cell = (MessagerieCell_iphone_l *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MessagerieCell_iphone_l *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.sujet.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
            cell.dateReception.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"]];
            cell.messageID = [(NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"] intValue];
            cell.delegate = self;
            cell.alpha = 0.5;
            if ([[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"VUE"] isEqualToString:@"1"])
            {
                [cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagelu_iphone.png"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagenonlu_iphone.png"] forState:UIControlStateNormal];
            }

       
        
                switch (indexPath.row % 2)
        {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        static NSString *cellIdentifier_p = @"MessagerieCell_iphone_p";
        
        MessagerieCell_iphone_p *cell = (MessagerieCell_iphone_p *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (MessagerieCell_iphone_p *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.alpha = 0.5;
        
        
            cell.sujet.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
            cell.dateReception.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"]];
            cell.messageID = [(NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"] intValue];
            cell.delegate = self;
            if ([[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"VUE"] isEqualToString:@"1"])
            {
                [cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagelu_iphone.png"] forState:UIControlStateNormal];
            }else{
                [cell.luBtn setImage:[UIImage imageNamed:@"messagerie_messagenonlu_iphone.png"] forState:UIControlStateNormal];
            }
            
      
        
        
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
    
}
 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
				
	//if (!isLandscape)
	//{
	
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/Messagerie/Corpsdemessage"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/Messagerie/Corpsdemessage"];
		self.isFromButton = NO;
    MessagerieDetailsViewController_iphone * messagerieDetailsViewController_iphone = [[MessagerieDetailsViewController_iphone alloc]initWithNibName:@"MessagerieDetailsViewController_iphone" bundle:nil];
    mode = MESSAGERIE_MODE_READ;
    if(dataManager.isClientContent || dataManager.isDemo)
    {
        
        NSString *str = (NSString *)[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"];
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:str,@"id", nil];
        if (dataManager.isDemo)
        {
            params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
        }
        [self postDataToServer:URL_MESSAGERIE_READ andParam:params];
        ind =(int) indexPath.row;
        
        
        
        messagerieDetailsViewController_iphone.dateText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"];
        messagerieDetailsViewController_iphone.contenueText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
        messagerieDetailsViewController_iphone.sujetText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
  
    }
    
    if(dataManager.isFournisseurContent)
    {
        
       
        messagerieDetailsViewController_iphone.dateText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_RECU"];
        messagerieDetailsViewController_iphone.sujetText = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"SUJET"];
      
        messagerieDetailsViewController_iphone.idmessage = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MSG"];
    }
    http://abderrahim.marbou%40maghrebail.ma:eac1cdd60eda56af4300224ccff966dd4b75992f@www.maghrebail.ma/leasebox/partenaires/api/rest/details_message/15

    [self.navigationController pushViewController:messagerieDetailsViewController_iphone animated:YES];
    

	
	 
}

@end
