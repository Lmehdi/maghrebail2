//
//  MesReglementsAffaireCell.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesReglementsAffaireCell.h"

@implementation MesReglementsAffaireCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numReference release];
    [_numContrat release];
    [_client release];
    [_mtht release];
    [_libelle release];
    [_mttva release];
    [_mtttc release];
    [_flecheimage release];
    [super dealloc];
}
@end
