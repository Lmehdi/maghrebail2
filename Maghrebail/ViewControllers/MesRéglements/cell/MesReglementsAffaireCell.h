//
//  MesReglementsAffaireCell.h
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MesReglementsAffaireCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *numReference;
@property (retain, nonatomic) IBOutlet UILabel *numContrat;
@property (retain, nonatomic) IBOutlet UILabel *client;
@property (retain, nonatomic) IBOutlet UILabel *mtht;
@property (retain, nonatomic) IBOutlet UILabel *libelle;
@property (retain, nonatomic) IBOutlet UILabel *mttva;
@property (retain, nonatomic) IBOutlet UILabel *mtttc;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;

@end
