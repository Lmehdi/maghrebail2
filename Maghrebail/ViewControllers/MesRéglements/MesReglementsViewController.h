//
//  MesReglementsViewController.h
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MesReglementsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{


    CGRect frameViewMontMin;
    CGRect frameViewMontMax;
    
    CGRect frameViewDateMin;
    CGRect frameViewDateMax;
    
    CGRect frameMontBtnMin;
    CGRect frameMontImgMin;
    CGRect frameMontLblMin;
    
    CGRect frameDateBtnMin;
    CGRect frameDateImgMin;
    CGRect frameDateLblMin;
    
    
    CGRect frameMontBtnMax;
    CGRect frameMontImgMax;
    CGRect frameMontLblMax;
    
    CGRect frameDateBtnMax;
    CGRect frameDateImgMax;
    CGRect frameDateLblMax;
    
    
    CGRect frameViewMontMinInitial;
    CGRect frameViewMontMaxInitial;
    
    CGRect frameViewDateMinInitial;
    CGRect frameViewDateMaxInitial;
    
    CGRect frameMontBtnMinInitial;
    CGRect frameMontImgMinInitial;
    CGRect frameMontLblMinInitial;
    
    CGRect frameDateBtnMinInitial;
    CGRect frameDateImgMinInitial;
    CGRect frameDateLblMinInitial;
    
    
    CGRect frameMontBtnMaxInitial;
    CGRect frameMontImgMaxInitial;
    CGRect frameMontLblMaxInitial;
    
    CGRect frameDateBtnMaxInitial;
    CGRect frameDateImgMaxInitial;
    CGRect frameDateLblMaxInitial;



}



- (IBAction)filtrePushed:(UIButton *)sender;
- (void)initiatView;
-(void) initHeaderColor;
@end
