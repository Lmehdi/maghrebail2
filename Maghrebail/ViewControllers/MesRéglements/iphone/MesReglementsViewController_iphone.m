//
//  MesReglementsViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesReglementsViewController_iphone.h"

#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@interface MesReglementsViewController_iphone ()

@end

@implementation MesReglementsViewController_iphone
@synthesize valeurclient;
@synthesize valeurlibelle;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];

    
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  /*  if(isLandscape)
    {
        return [self getCellHeightForTextLandscape:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
    }
    else
   
        valeurlibelle=[self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
        valeurclient=[self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"]];
        long maximumvalue=MAX(valeurlibelle,valeurclient);
   
    return maximumvalue;
       // return [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];*/
    
    
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    if (isLandscape) {
        
        static NSString *cellIdentifier_l;
    /*
        if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"MesReglementsAffaireCell_iphone5_l";
        }
  
        else
            cellIdentifier_l = @"MesReglementsAffaireCell_iphone_l";
        */
        cellIdentifier_l = @"MesReglementsAffaireCell_iphone_l";

        MesReglementsAffaireCell *cell = (MesReglementsAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (MesReglementsAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        cell.numReference.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
        
        cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
        
        
        cell.libelle.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"] ;
        
        cell.mtttc.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
        
        cell.mtht.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]] ;

        
      
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        

        static NSString *cellIdentifier_p = @"MesReglementsAffaireCell_iphone_p";
        
     
    if([DataManager isIphone5])
        {
        cellIdentifier_p=@"MesReglementsAffaireCell_iphone5_p";
        }
        
        MesReglementsAffaireCell *cell = (MesReglementsAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell = (MesReglementsAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
       
        cell.alpha = 0.5;
        
        cell.numReference.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
        cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
        cell.libelle.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"] ;
        cell.mtht.text =[DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isLandscape)
    {
     
        self.isFromButton = NO;
        dataManager.isdetailsreglement=YES;
       dataManager.isdetailscommande=NO;
        dataManager.isdetailsimmatriculation=NO;
        dataManager.isdetailsengagement=NO;
        dataManager.isdetailscontact=NO;
        dataManager.isdatafacture=NO;

        DetailsBienCommandeViewController_iphone *  detailsBienCommandeViewController_iphone  = [[DetailsBienCommandeViewController_iphone alloc]initWithNibName:@"DetailsBienCommandeViewController_iphone" bundle:nil];
        
        /*     if (!self.showFav)
         listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
         else
         listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
        detailsBienCommandeViewController_iphone.idmvt=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MVT"];
        [self.navigationController pushViewController:detailsBienCommandeViewController_iphone animated:YES];
    }

    

}

@end
