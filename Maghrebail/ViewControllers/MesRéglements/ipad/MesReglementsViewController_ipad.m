//
//  MesReglementsViewController_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesReglementsViewController_ipad.h"
#import "MesReglementsAffaireCell.h"
#import "DetailsBienCommandeViewController_ipadViewController.h"
@implementation MesReglementsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellIdentifier_l;
    /*
     if ([DataManager isIphone5LandScape])
     {
     cellIdentifier_l = @"MesReglementsAffaireCell_iphone5_l";
     }
     
     else
     cellIdentifier_l = @"MesReglementsAffaireCell_iphone_l";
     */
    cellIdentifier_l = @"MesReglementsAffaireCell_ipad";
    
    MesReglementsAffaireCell *cell = (MesReglementsAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
    if (cell == nil)
    {
        cell = (MesReglementsAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
    }
    
    cell.numReference.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_MOUVEMENT"];
    
    cell.client.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
    
    
    cell.libelle.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"] ;
    
    cell.mtttc.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
    
    cell.mtht.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]] ;
    
    
    
    cell.alpha = 0.5;
    
    switch (indexPath.row % 2) {
        case 0:
            cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
            
            break;
        case 1:
            cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    
    return cell;
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.isFromButton = NO;
    dataManager.isdetailsreglement=YES;
    dataManager.isdetailscommande=NO;
    dataManager.isdetailsimmatriculation=NO;
    dataManager.isdetailsengagement=NO;
    dataManager.isdetailscontact=NO;
    dataManager.isdatafacture=NO;
    
    DetailsBienCommandeViewController_ipadViewController *  detailsBienCommandeViewController_iphone  = [[DetailsBienCommandeViewController_ipadViewController alloc]initWithNibName:@"DetailsBienCommandeViewController_ipadViewController" bundle:nil];
    
    /*     if (!self.showFav)
     listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
     else
     listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
    detailsBienCommandeViewController_iphone.idmvt=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"ID_MVT"];
    [self.navigationController pushViewController:detailsBienCommandeViewController_iphone animated:YES];
    
    
    
}




@end
