//
//  MesReglementsViewController.m
//  Maghrebail
//
//  Created by Belkheir on 17/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesReglementsViewController.h"

@interface MesReglementsViewController ()

@end

@implementation MesReglementsViewController
//@synthesize maximumvalue;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
  
    
    [self initHeaderColor];
     [Flurry logEvent:@"MesRegelementsFournisseur"];
}


- (void)dealloc
{
  
    [super dealloc];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES REGLEMENTS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{   keyMontant = @"MT_HT";
    keyDate = @"DATE_MOUVEMENT";
    
    [super viewWillAppear:animated];
    [self initHeaderColor];
    

    
    
    if (self.isFromButton || dataManager.isLoad)
    {
        isFiltreViewShowed = NO;
        dataManager.isLoad = NO;
        
        [self.response removeAllObjects];
        [_tableView reloadData];
        
        if(dataManager.isDemo)
        {
            
          
             [self getRemoteContentAuth:REGLEMENT_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];

        }
        else
        {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_reglees/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
     
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        frameMontImgMinInitial = self.imgViewScrollMontnat.frame;
        frameDateImgMinInitial = self.imgViewScrollDate.frame;
        
        frameMontBtnMinInitial = self.btnMinMontant.frame;
        frameMontLblMinInitial = self.labelMinMontant.frame;
        
        frameMontBtnMaxInitial = self.btnMaxMontant.frame;
        frameMontLblMaxInitial = self.labelMaxMontant.frame;
        
        frameDateBtnMinInitial = self.buttonMinDate.frame;
        frameDateLblMinInitial = self.labelMinDate.frame;
        
        frameDateBtnMaxInitial = self.buttonMaxDate.frame;
        frameDateLblMaxInitial = self.labelMaxDate.frame;
        
        frameViewMontMinInitial = self.viewMinMontant.frame;
        frameViewMontMaxInitial = self.viewMaxMontant.frame;
        frameViewDateMinInitial = self.viewMinDate.frame;
        frameViewDateMaxInitial = self.viewMaxDate.frame;
        
        frameMontImgMin = self.imgViewScrollMontnat.frame;
        frameDateImgMin = self.imgViewScrollDate.frame;
        
        frameMontBtnMin = self.btnMinMontant.frame;
        frameMontLblMin = self.labelMinMontant.frame;
        
        frameMontBtnMax = self.btnMaxMontant.frame;
        frameMontLblMax = self.labelMaxMontant.frame;
        
        frameDateBtnMin = self.buttonMinDate.frame;
        frameDateLblMin = self.labelMinDate.frame;
        
        frameDateBtnMax = self.buttonMaxDate.frame;
        frameDateLblMax = self.labelMaxDate.frame;
        
        frameViewMontMin = self.viewMinMontant.frame;
        frameViewMontMax = self.viewMaxMontant.frame;
        frameViewDateMin = self.viewMinDate.frame;
        frameViewDateMax = self.viewMaxDate.frame;
    }
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
}


-(void)initiatView
{
    //btn action
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    
    [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
}


-(void)updateView{
	
	
	
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];

    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.reglements removeAllObjects];
        dataManager.reglements = [NSMutableArray arrayWithArray:reponse];
        dataManager.reglementsFiltred = (NSMutableArray *)[dataManager.reglements copy];
        
        if (dataManager.reglements.count != 0)
        {
            //remplir valuesMontant
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:keyMontant ascending:YES comparator:^(id obj1, id obj2) {
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                if ([obj1 doubleValue] > [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1 doubleValue] < [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];
            NSArray *sortedData;
            sortedData = [[dataManager.reglements sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]] copy];
            
            NSString *str1 = [[[sortedData lastObject] objectForKey:keyMontant] stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *str2 = [[[sortedData objectAtIndex:0] objectForKey:keyMontant] stringByReplacingOccurrencesOfString:@" " withString:@""];
            str1 = [str1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            str2 = [str2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            
            maxMontantValue = [str1 doubleValue];
            minMontantValue = [str2  doubleValue];
            self.labelMinMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", minMontantValue]];
            self.labelMaxMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", maxMontantValue]];
            int diff = (maxMontantValue - minMontantValue)/100;
            valuesMontant = [[NSMutableArray alloc] init];
            for (int i = minMontantValue; i <= maxMontantValue; i = i + diff)
            {
                [valuesMontant addObject:[NSString stringWithFormat:@"%d", i]];
                if (diff == 0)
                    break;
            }
            
            //[dataManager.ligneCredit removeAllObjects];
            dataManager.reglements.array = sortedData;
            //remplir valuesDates
            NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:keyDate ascending:YES comparator:^(id obj1, id obj2)
                                                {
                                                    return (NSComparisonResult)[[DataManager stringToDtae:[DataManager getDateStringFromPresentableDate:obj1]] compare:[DataManager stringToDtae:[DataManager getDateStringFromPresentableDate:obj2]]];
                                                }];
            
            NSArray *sortedDataWithDate = [[dataManager.reglements sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptorDate,nil]] copy];
            
            maxDateValue = [[DataManager stringToDtae:[DataManager getDateStringFromPresentableDate:[[sortedDataWithDate lastObject] objectForKey:keyDate]]] timeIntervalSince1970];
            minDatetValue = [[DataManager stringToDtae:[DataManager getDateStringFromPresentableDate:[[sortedDataWithDate objectAtIndex:0] objectForKey:keyDate]]] timeIntervalSince1970];
            
            self.labelMinDate.text = [DataManager timeIntervaleToString:minDatetValue];
            self.labelMaxDate.text = [DataManager timeIntervaleToString:maxDateValue];
            int diffDate = (maxDateValue - minDatetValue)/100;
            valuesDates = [[NSMutableArray alloc] init];
            for (int i = minDatetValue; i <= maxDateValue; i = i + diffDate)
            {
                [valuesDates addObject:[NSString stringWithFormat:@"%d", i]];
                if (diffDate == 0) {
                    break;
                }
            }
            
            if (dataManager.reglements.count == 1)
            {
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            else
            {
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            
            self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
            self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
            self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
            self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
        }
        
        
    }
    
    
    
    
    
    [_tableView reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        return [dataManager.reglementsFiltred  count];
        
    }
    else
    {
        return 0;
    }
}

#pragma mark --
#pragma mark Pull to refresh

-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;

    if(dataManager.isDemo)
    {
        [self getRemoteContentAuth:REGLEMENT_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
        
    }
    else
    {
     [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Reglements_reglees/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
    
    }
   


}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    
    
    if (!isLandscape)
    {
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
        
        
    }
    
    [_tableView reloadData];
    
    if (dataManager.reglements.count == 1)
    {
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    else
    {
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMin;
    self.imgViewScrollDate.frame=frameDateImgMin;
    
    self.btnMinMontant.frame=frameMontBtnMin;
    self.labelMinMontant.frame=frameMontLblMin;
    self.viewMinMontant.frame=frameViewMontMin;
    
    self.btnMaxMontant.frame=frameMontBtnMax;
    self.labelMaxMontant.frame=frameMontLblMax;
    self.viewMaxMontant.frame=frameViewMontMax;
    
    self.buttonMinDate.frame=frameDateBtnMin;
    self.labelMinDate.frame=frameDateLblMin;
    self.viewMinDate.frame=frameViewDateMin;
    
    self.buttonMaxDate.frame=frameDateBtnMax;
    self.labelMaxDate.frame=frameDateLblMax;
    self.viewMaxDate.frame=frameViewDateMax;
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    [_tableView addSubview:_refreshHeaderView];
    [_refreshHeaderView refreshLastUpdatedDate];
    [self initiatView];
    
}

- (void)minScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
    [super minScrollDraggedEnd:button withEvent:event];
    frameMontImgMin = self.imgViewScrollMontnat.frame;
    frameDateImgMin = self.imgViewScrollDate.frame;
    
    frameMontBtnMin = self.btnMinMontant.frame;
    frameMontLblMin = self.labelMinMontant.frame;
    
    frameMontBtnMax = self.btnMaxMontant.frame;
    frameMontLblMax = self.labelMaxMontant.frame;
    
    frameDateBtnMin = self.buttonMinDate.frame;
    frameDateLblMin = self.labelMinDate.frame;
    
    frameDateBtnMax = self.buttonMaxDate.frame;
    frameDateLblMax = self.labelMaxDate.frame;
    
    frameViewMontMin = self.viewMinMontant.frame;
    frameViewMontMax = self.viewMaxMontant.frame;
    frameViewDateMin = self.viewMinDate.frame;
    frameViewDateMax = self.viewMaxDate.frame;
    
    self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
    self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
    self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
    self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}

- (void)maxScrollDraggedEnd:(UIButton *)button withEvent:(UIEvent *)event
{
    [super maxScrollDraggedEnd:button withEvent:event];
    frameMontImgMin = self.imgViewScrollMontnat.frame;
    frameDateImgMin = self.imgViewScrollDate.frame;
    
    frameMontBtnMin = self.btnMinMontant.frame;
    frameMontLblMin = self.labelMinMontant.frame;
    
    frameMontBtnMax = self.btnMaxMontant.frame;
    frameMontLblMax = self.labelMaxMontant.frame;
    
    frameDateBtnMin = self.buttonMinDate.frame;
    frameDateLblMin = self.labelMinDate.frame;
    
    frameDateBtnMax = self.buttonMaxDate.frame;
    frameDateLblMax = self.labelMaxDate.frame;
    
    frameViewMontMin = self.viewMinMontant.frame;
    frameViewMontMax = self.viewMaxMontant.frame;
    frameViewDateMin = self.viewMinDate.frame;
    frameViewDateMax = self.viewMaxDate.frame;
    
    self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
    self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
    self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
    self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
}

- (IBAction)filtrePushed:(UIButton *)sender
{
    [super filtrePushed:sender];
    
}

- (void)viewDidUnload {
 
    [super viewDidUnload];
}

@end
