//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ProduitViewController : BaseViewController
{
    int typeProduit;
}

@property (retain, nonatomic) IBOutlet UIImageView *imgBackground;

@property (retain, nonatomic) NSString* titreHeader;
@property (retain, nonatomic) NSString* imageParallaxe;
@property (assign) BOOL withParallaxe;
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UIButton *buttonEntreprise;
@property (retain, nonatomic) IBOutlet UIButton *buttonProfessionnel;
@property (retain, nonatomic) IBOutlet UIWebView *webViewProduit;
@property (assign) int typeProduit;
@property (retain, nonatomic) IBOutlet UILabel *labelExmpl;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (assign, nonatomic) NSInteger index;
@property (retain, nonatomic) IBOutlet UIButton *btnBackOut;


- (IBAction)entreprisePushed:(UIButton *)sender;
- (IBAction)professionnelPushed:(UIButton *)sender;

@end
