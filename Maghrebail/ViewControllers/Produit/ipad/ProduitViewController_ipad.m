//
//  MonProfilViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ProduitViewController_ipad.h"

@interface ProduitViewController_ipad ()

@end

@implementation ProduitViewController_ipad
@synthesize nameImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.nameImage=@"ipad-v3-image1.jpg";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.labelTitle.text=self.titreHeader;
    [self.btnBackOut setHidden:YES];
    [self.imgBackground setImage:[UIImage imageNamed:self.nameImage]];
    [self initHeaderColor];
}
-(void)initHeaderColor{
    headerView.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    dataManager.typeColor=@"blue";
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
