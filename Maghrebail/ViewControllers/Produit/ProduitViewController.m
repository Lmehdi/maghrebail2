	//
	//  MonProfilViewController.m
	//  Maghrebail
	//
	//  Created by AHDIDOU on 18/02/13.
	//  Copyright (c) 2013 Mobiblanc. All rights reserved.
	//

#import "ProduitViewController.h"
#import "JSON.h"
@interface ProduitViewController ()

@end

@implementation ProduitViewController

@synthesize typeProduit;
@synthesize index;
@synthesize withParallaxe;
@synthesize imageParallaxe;
@synthesize titreHeader;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
			// Custom initialization
		self.titreHeader=@"";
		shouldAutorate = NO;
		self.withParallaxe=NO;
		self.imageParallaxe=@"v3-imageEquipementPro.png";
		self.index = -1;
		shouldAddHeader=NO;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
		//[self performSelectorInBackground:@selector(loadProduitsData) withObject:nil];
	
	if (dataManager.produits.count == 0)
		{
		NSData *dataProduits = [DataManager readDataIntoCachWith:@"produits"];
		if (dataProduits)
			{
			dataManager.produits = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
			}
		else
			{
				NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"produits" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
				NSDictionary *responseDico = [response JSONValue];
				NSArray *data = [responseDico objectForKey:@"response"];
				dataManager.produits = [NSMutableArray arrayWithArray:data];
			}
		}
	
		// Do any additional setup after loading the view from its nib.
	[self.buttonEntreprise setSelected:YES];
	[self.buttonProfessionnel setSelected:NO];
	if (self.typeProduit == kPRODUITS_EQUIPEMENT)
		{
		self.labelTitle.text = @"EQUIPEMENTS";
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"desctiption"] baseURL:nil];
		
		}
	else if (self.typeProduit == kPRODUITS_IMMOBILIER)
		{
		self.labelTitle.text = @"IMMOBILIER";
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"description"] baseURL:nil];
		}
	if (self.index == -1)
		{
		[self entreprisePushed:nil];
		}
	else
		{
		if (self.index == 1)
			[self entreprisePushed:nil];
		else
			[self professionnelPushed:nil];
		}
	
	if (dataManager.firstTime && !self.isFromDash)
		{
		[self showSideMenu];
		[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(hide) userInfo:nil repeats:NO];
		}
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if (dataManager.authentificationData)
		{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
	else
		{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
}

-(void)loadProduitsData
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"produits" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
	NSDictionary *responseDico = [response JSONValue];
	NSArray *data = [responseDico objectForKey:@"response"];
	dataManager.produits = [NSMutableArray arrayWithArray:data];
	
	NSURL *url = [NSURL URLWithString:[kPRODUITS_URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	response = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
	response = [DataManager stringByStrippingHTML:response];
	responseDico = [response JSONValue];
	data = [responseDico objectForKey:@"response"];
	
	if (data)
		{
		dataManager.produits = [NSMutableArray arrayWithArray:data];
		NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:data];
		[DataManager writeDataIntoCachWith:dataProduits andKey:@"produits"];
		}
	else
		{
		
		NSData *dataProduits = [DataManager readDataIntoCachWith:@"produits"];
		if (dataProduits)
			{
			dataManager.produits = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
			}
		else
			{
			response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"produits" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
			responseDico = [response JSONValue];
			data = [responseDico objectForKey:@"response"];
			dataManager.produits = [NSMutableArray arrayWithArray:data];
			}
		}
	[pool release];
}

- (void)hide
{
	[self toggleLeftMenu:nil];
	dataManager.firstTime = NO;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
		// Dispose of any resources that can be recreated.
}

- (void)dealloc {
	[_labelTitle release];
	[_buttonEntreprise release];
	[_buttonProfessionnel release];
	[_webViewProduit release];
	[_labelExmpl release];
	[_scrollView release];
	[_btnBackOut release];
	[_imgBackground release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setLabelTitle:nil];
	[self setButtonEntreprise:nil];
	[self setButtonProfessionnel:nil];
	[self setWebViewProduit:nil];
	[self setLabelExmpl:nil];
	[self setScrollView:nil];
	[super viewDidUnload];
}

#pragma mark - Action

- (IBAction)entreprisePushed:(UIButton *)sender
{
	self.buttonEntreprise.selected = YES;
	self.buttonProfessionnel.selected = NO;
	[self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
	
	if (self.typeProduit == kPRODUITS_EQUIPEMENT)
		{
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"description"] baseURL:nil];
		int yOrigin = 10;
		for (NSInteger i = 0 ; i < dataManager.produits.count ; i++)
			{
			NSDictionary *dic = [dataManager.produits objectAtIndex:i];
			if ([[dic objectForKey:@"category"] isEqualToString:@"ENTREPRISES"] && [[dic objectForKey:@"type"] isEqualToString:@"equipement"])
				{
				NSArray *arydescription = [dic objectForKey:@"description"];
				
				for (NSInteger j = 0 ; j < arydescription.count ; j++)
					{
					UILabel *label = [[UILabel alloc] init];
					label.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
					label.backgroundColor = [UIColor clearColor];
					label.numberOfLines = 0;
					label.font = [UIFont systemFontOfSize:17];
					CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
					
					label.text = [[arydescription objectAtIndex:j] objectForKey:@"text"];
					
					CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelTitle.lineBreakMode];
					
						//adjust the label the the new height.
					
					CGRect newFrame = label.frame;
					newFrame.size.height = expectedLabelSize.height;
					label.frame = newFrame;
					
					
					yOrigin += expectedLabelSize.height+10;
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"black"])
						{
						label.textColor =  [UIColor colorWithRed:105.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"orange"])
						{
						label.textColor = [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:44.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"blue"])
						{
						label.textColor = [UIColor colorWithRed:86.0f/255.0f green:129.0f/255.0f blue:181.0f/255.0f alpha:1];
						}
					[self.scrollView addSubview:label];
					[label release];
					}
				}
			}
		[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, yOrigin)];
		
		}
	else if (self.typeProduit == kPRODUITS_IMMOBILIER)
		{
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"description"] baseURL:nil];
		int yOrigin = 10;
		for (NSInteger i = 0 ; i < dataManager.produits.count ; i++)
			{
			NSDictionary *dic = [dataManager.produits objectAtIndex:i];
			if ([[dic objectForKey:@"category"] isEqualToString:@"ENTREPRISES"] && [[dic objectForKey:@"type"] isEqualToString:@"immobilier"])
				{
				NSArray *arydescription = [dic objectForKey:@"description"];
				
				for (NSInteger j = 0 ; j < arydescription.count ; j++)
					{
					UILabel *label = [[UILabel alloc] init];
					label.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
					label.backgroundColor = [UIColor clearColor];
					label.numberOfLines = 0;
					label.font = [UIFont systemFontOfSize:17];
					CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
					
					
					label.text = [[arydescription objectAtIndex:j] objectForKey:@"text"];
					
					CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelTitle.lineBreakMode];
					
						//adjust the label the the new height.
					
					CGRect newFrame = label.frame;
					newFrame.size.height = expectedLabelSize.height;
					label.frame = newFrame;
					
					yOrigin += expectedLabelSize.height+10;
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"black"])
						{
						label.textColor = [UIColor colorWithRed:105.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"orange"])
						{
						label.textColor = [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:44.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"blue"])
						{
						label.textColor = [UIColor colorWithRed:86.0f/255.0f green:129.0f/255.0f blue:181.0f/255.0f alpha:1];
						}
					[self.scrollView addSubview:label];
					[label release];
					}
				}
			}
		[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, yOrigin)];
		}
}

- (IBAction)professionnelPushed:(UIButton *)sender
{
	self.buttonEntreprise.selected = NO;
	self.buttonProfessionnel.selected = YES;
	[self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
	if (self.typeProduit == kPRODUITS_EQUIPEMENT)
		{
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"description"] baseURL:nil];
		int yOrigin = 10;
		for (NSInteger i = 0 ; i < dataManager.produits.count ; i++)
			{
			NSDictionary *dic = [dataManager.produits objectAtIndex:i];
			if ([[dic objectForKey:@"category"] isEqualToString:@"PROFESSIONNELS"] && [[dic objectForKey:@"type"] isEqualToString:@"equipement"])
				{
				NSArray *arydescription = [dic objectForKey:@"description"];
				
				for (NSInteger j = 0 ; j < arydescription.count ; j++)
					{
					UILabel *label = [[UILabel alloc] init];
					label.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
					label.backgroundColor = [UIColor clearColor];
					label.numberOfLines = 0;
					label.font = [UIFont systemFontOfSize:17];
					CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
					
					label.text = [[arydescription objectAtIndex:j] objectForKey:@"text"];
					
					CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelTitle.lineBreakMode];
						//adjust the label the the new height.
					
					CGRect newFrame = label.frame;
					newFrame.size.height = expectedLabelSize.height;
					label.frame = newFrame;
					
					yOrigin += expectedLabelSize.height+10;
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"black"])
						{
						label.textColor = [UIColor colorWithRed:105.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"orange"])
						{
						label.textColor = [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:44.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"blue"])
						{
						label.textColor = [UIColor colorWithRed:86.0f/255.0f green:129.0f/255.0f blue:181.0f/255.0f alpha:1];
						}
					[self.scrollView addSubview:label];
					[label release];
					}
				}
			}
		[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, yOrigin)];
		}
	else if (self.typeProduit == kPRODUITS_IMMOBILIER)
		{
			//[self.webViewProduit loadHTMLString:[[dataManager.produits objectAtIndex:0] objectForKey:@"description"] baseURL:nil];
		int yOrigin = 10;
		for (NSInteger i = 0 ; i < dataManager.produits.count ; i++)
			{
			NSDictionary *dic = [dataManager.produits objectAtIndex:i];
			if ([[dic objectForKey:@"category"] isEqualToString:@"PROFESSIONNELS"] && [[dic objectForKey:@"type"] isEqualToString:@"immobilier"])
				{
				NSArray *arydescription = [dic objectForKey:@"description"];
				
				for (NSInteger j = 0 ; j < arydescription.count ; j++)
					{
					UILabel *label = [[UILabel alloc] init];
					label.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
					label.backgroundColor = [UIColor clearColor];
					label.numberOfLines = 0;
					label.font = [UIFont systemFontOfSize:17];
					CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
					
					label.text = [[arydescription objectAtIndex:j] objectForKey:@"text"];
					
					CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelTitle.lineBreakMode];
					
						//adjust the label the the new height.
					
					CGRect newFrame = label.frame;
					newFrame.size.height = expectedLabelSize.height;
					label.frame = newFrame;
					
					yOrigin += expectedLabelSize.height+10;
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"black"])
						{
						label.textColor = [UIColor colorWithRed:105.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"orange"])
						{
						label.textColor = [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:44.0f/255.0f alpha:1];
						}
					
					if ([[[arydescription objectAtIndex:j] objectForKey:@"style"] isEqualToString:@"blue"])
						{
						label.textColor = [UIColor colorWithRed:86.0f/255.0f green:129.0f/255.0f blue:181.0f/255.0f alpha:1];
						}
					[self.scrollView addSubview:label];
					[label release];
					}
				}
			}
		[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, yOrigin)];
		
		}
}
@end

