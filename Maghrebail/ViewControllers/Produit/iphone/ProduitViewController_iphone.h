//
//  MonProfilViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "ProduitViewController.h"
#import "StrechyParallaxScrollView.h"

@interface ProduitViewController_iphone : ProduitViewController
{
    
}

- (IBAction)btnBack:(UIButton *)sender;

@property (assign) BOOL showBtnBack;

@property (retain, nonatomic) IBOutlet UILabel *labelTitreHeader;
@property (retain, nonatomic) StrechyParallaxScrollView *strechy;
@property (retain, nonatomic) IBOutlet UIView *viewPrincipale;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollMain;

@end
