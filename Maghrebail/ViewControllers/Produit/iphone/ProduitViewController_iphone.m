	//
	//  MonProfilViewController.m
	//  Maghrebail
	//
	//  Created by AHDIDOU on 18/02/13.
	//  Copyright (c) 2013 Mobiblanc. All rights reserved.
	//

#import "ProduitViewController_iphone.h"

@interface ProduitViewController_iphone ()

@end

@implementation ProduitViewController_iphone
@synthesize  strechy;
@synthesize showBtnBack;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		shouldAddHeader=NO;
		self.showBtnBack=NO;
	}
	return self;
}

- (void)viewDidLoad
{
	
		// Construct Parallaxe
	
	[self.btnBackOut setHidden:self.showBtnBack];
	self.labelTitreHeader.text=self.titreHeader;
	
	[self.scrollMain setContentSize:CGSizeMake(mainScreen_width,950)];
	
	if (self.withParallaxe) {
		
		shouldAddHeader=NO;
		UIImageView* imgParallaxe= [[UIImageView alloc] initWithFrame:CGRectMake(0,0,mainScreen_width,250)];
		imgParallaxe.backgroundColor=[UIColor clearColor];
		imgParallaxe.image=[UIImage imageNamed:self.imageParallaxe];
		
		if (!self.strechy) {
			self.strechy = [[StrechyParallaxScrollView alloc] initWithFrame:CGRectMake(0, 0, mainScreen_width, mainScreen_height) andTopView:imgParallaxe];
		}
		
		[self.strechy addSubview:self.scrollMain];
		self.scrollMain.scrollEnabled=NO;
		self.scrollView.scrollEnabled=NO;
		[self.scrollMain setFrame:CGRectMake(0,150,mainScreen_width,800)];
		[self.viewPrincipale addSubview:self.strechy];
		[self.strechy setContentSize:CGSizeMake(mainScreen_width,950)];
		
	}else{
			//  shouldAddHeader=YES;
	}
	
	[super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (void)dealloc {
	
	[_viewPrincipale release];
	[_scrollMain release];
	[_labelTitreHeader release];
	
	[super dealloc];
}

- (IBAction)btnBack:(UIButton *)sender {
	[self showSideMenu];
}
@end

