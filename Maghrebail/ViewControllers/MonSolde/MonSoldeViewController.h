//
//  MonSoldeViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MonSoldeViewController : BaseViewController
@property (assign) BOOL canShareContent;
@property (retain, nonatomic) IBOutlet UILabel *solde;
@property (retain, nonatomic) IBOutlet UILabel *labelInfo;
@property (retain, nonatomic) IBOutlet UIButton *btnPDFOut;

@property (retain, nonatomic) IBOutlet UIWebView *webViewPDF;

-(void) initHeaderColor;
- (IBAction)btnPDF:(UIButton *)sender;

@end
