//
//  MesRelvesViewCell.m
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "MesRelvesViewCell.h"


@implementation MesRelvesViewCell


// MARK: - Initialization
- (void)awakeFromNib
{
    [super awakeFromNib];
}


// MARK: - Methods
- (void)dealloc
{
    
    [_mSixstLabel release];
    [_mLabelDateEchange release];
    [super dealloc];
}


// MARK: - Actions
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    //
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


@end
