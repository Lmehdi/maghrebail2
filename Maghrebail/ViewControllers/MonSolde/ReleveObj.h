//
//  ReleveObj.h
//  Maghrebail
//
//  Created by Raoui M on 15/09/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReleveObj : NSObject<NSCoding>


@property (retain, nonatomic) NSString  *tiers;
@property (retain, nonatomic) NSString  *rstiers;
@property (retain, nonatomic) NSString  *naffaire;
@property (retain, nonatomic) NSString  *reference;
@property (retain, nonatomic) NSString  *dateechange;
@property (retain, nonatomic) NSString  *montantdebit;
@property (retain, nonatomic) NSString  *montantcredit;
@property (retain, nonatomic) NSString  *loyercompement;
@property (retain, nonatomic) NSString  *devise;
@property (retain, nonatomic) NSString  *apayer;
@property (retain, nonatomic) NSString  *libelle;
@property (retain, nonatomic) NSString  *idekip;
@property Boolean etat;

@end
