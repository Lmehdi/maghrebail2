//
//  MesRelvesViewController.h
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import "MtcViewController.h"

@interface MesRelvesViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>


// MARK: - Properties
@property (retain,nonatomic) NSMutableArray *mImpayes;
@property (retain,nonatomic) NSMutableArray *mArraySelectedFactures;
@property (retain,nonatomic) NSMutableArray *mDataArray;
@property (retain,nonatomic) NSMutableArray *mrefArray;
@property (nonatomic, assign) double montant;
@property (nonatomic, assign) double tableViewHeight;

// MARK: - Outlets
@property (retain, nonatomic) IBOutlet UITableView *mTableView;
@property (retain, nonatomic) IBOutlet UILabel *mLabelMsg;
@property (retain, nonatomic) IBOutlet UIView *mViewMsg;
@property (retain, nonatomic) IBOutlet UIView *ViewWebView;
@property (retain, nonatomic) IBOutlet UIButton *payerbutton;
@property (retain, nonatomic) IBOutlet UIView *headerInfosView;
@property (retain, nonatomic) IBOutlet UILabel *lblTiers;
@property (retain, nonatomic) IBOutlet UILabel *lblCaseNumber;
@property (retain, nonatomic) IBOutlet UIWebView *mWebView;
@property (retain, nonatomic) IBOutlet UILabel *lblMontant;
@property (retain, nonatomic) IBOutlet UIButton *btnValider;
@property (retain, nonatomic) IBOutlet UIWebView *conditionsWebView;
@property (retain, nonatomic) IBOutlet UIView *viewValider;
@property (retain, nonatomic) IBOutlet UILabel *lblConditions;
@property (retain, nonatomic) IBOutlet UIButton *btnCheck;
@property Boolean isChecked;
@property (retain, nonatomic) IBOutlet UIButton *btnOpenConditions;
@property (retain, nonatomic) IBOutlet UIButton *btnpayer2;
@property (retain, nonatomic) IBOutlet UILabel *lblLibelleFrais;
@property (retain, nonatomic) IBOutlet UILabel *lblFraisMontant;
@property (retain, nonatomic) IBOutlet UIView *viewFrais;
@property (retain, nonatomic) IBOutlet UIButton *btnAnnuler;
@property (retain, nonatomic) IBOutlet UILabel *lblPaye;
@property (nonatomic) double  montantApayer;
@property float mHeightable;
@property double somme;
@property double sommeFrais;
// MARK: - Methods
-(void) initHeaderColor;
@property (retain, nonatomic) IBOutlet UIView *viewpayer;
@property (retain, nonatomic) IBOutlet UILabel *lblmaghrebailtxt;


// MARK: - Actions
- (IBAction)didSelectedPayer:(id)sender;
- (IBAction)didselectedclosebutton:(id)sender;
- (IBAction)didSelectValider:(id)sender;
- (IBAction)didSelectCheckbox:(id)sender;
- (IBAction)didSelectPayer2:(id)sender;
-(IBAction)actionShowConditions:(id)sender;
- (IBAction)actionAnnuler:(id)sender ;

@end
