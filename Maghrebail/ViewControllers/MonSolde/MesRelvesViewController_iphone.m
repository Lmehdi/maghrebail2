        //
//  MesRelvesViewController_iphone.m
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "MesRelvesViewController_iphone.h"
#import "MesRelvesViewCell.h"
#import "ReleveObj.h"

#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@implementation MesRelvesViewController_iphone


// MARK: - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    
    return self;
}


// MARK: - View states
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //self.tx
  //  self.mImpayes=[[NSMutableArray alloc]init];
    self.isChecked=false;
    NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
    
 //self.mImpayes=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
    
    [self initHeaderColor];
    
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    self.lblMontant.text=[NSString stringWithFormat:@"%.02f",self.montantApayer ];
    self.mHeightable=self.tableView.frame.size.height;
    if(!self.isChecked){
        self.btnpayer2.hidden=YES;
        self.viewFrais.hidden=YES;
        self.btnOpenConditions.hidden=NO;
        
        
    }
    else{
        self.lblConditions.hidden = true;
        self.btnCheck.hidden = true;
        self.btnOpenConditions.hidden = true;
        CGRect frameRect = self.viewFrais.frame;
        self.lblFraisMontant.text=[NSString stringWithFormat:@"%f",self.sommeFrais];
        frameRect.origin.y = (self.tableView.frame.origin.y) + self.mArraySelectedFactures.count*70;
        self.viewFrais.frame = frameRect;
        self.btnAnnuler.hidden=NO;
        self.payerbutton.hidden=YES;
        self.btnpayer2.hidden=NO;
    }
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
    [_tableView reloadData];
}


// MARK: - Tableview data source & delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.isChecked){
        
        return self.mArraySelectedFactures.count;
    }else{
//        NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
//        
//        NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
        return self.mImpayes.count;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;
   
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReleveObj *mdic                               =   [[ReleveObj alloc] init];
    
    
    
//    NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
//    
//    NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
    if(self.isChecked){
        mdic                                                    =   [self.mArraySelectedFactures objectAtIndex:indexPath.row];
    }else{
       mdic                                                    =   [self.mImpayes objectAtIndex:indexPath.row];
    }
    
    
    if (isLandscape)
    {
        static NSString *cellIdentifier_l                   =   @"";
        
        if(IS_IPHONE_4_OR_LESS){
            cellIdentifier_l=@"MesRelvesViewCell_iphone_l";
        }else if(IS_IPHONE_5){
            cellIdentifier_l=@"MesRelvesViewCell_iphone5_l";
        }else if(IS_IPHONE_6){
            cellIdentifier_l=@"MesRelvesViewCell_iphone6_l";
            
        }else{
            cellIdentifier_l=@"MesRelvesViewCell_iphone6plus_l";
        }
        MesRelvesViewCell *cell                             =   (MesRelvesViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        double montantApayer = 0;

        if (cell == nil)
        {
            cell                                            =   (MesRelvesViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }

        double montantdebit=[mdic.montantdebit doubleValue];
        double loyercomplmnet=[mdic.loyercompement doubleValue];
        double montantcredit=[mdic.montantcredit doubleValue];
        montantApayer+=(montantdebit+loyercomplmnet)-montantcredit;
        NSNumber *montantD = [NSNumber numberWithDouble:montantApayer];
        cell.alpha                                          =   0.5;
        cell.selectionStyle                                 =   UITableViewCellSelectionStyleNone;
        cell.mButtonCheck.tag                               =   indexPath.row;
        cell.mfirstlabel.text                               =   mdic.reference;
        cell.mSecondLabel.text                              =   mdic.libelle;
        cell.mFourLabel.text                              =    [NSString stringWithFormat:@"%.02f", [montantD floatValue] ];
        cell.mThiredLabel.text=mdic.dateechange;
        //        cell.mfifthlabel.text                                =  mdic.montantcredit;
        //        cell.mLastLabel.text                                =   mdic.loyercompement;
        
        if(self.isChecked){
            cell.mButtonCheck.hidden=YES;
            self.lblPaye.hidden = YES;
        }else{
            cell.mButtonCheck.hidden=NO;
            self.lblPaye.hidden = NO;
        }
        NSString *aPayer                                    =   mdic.apayer;
        NSString *mdevise                                   =  mdic.devise;

        
        if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"1"])
        {
            if(mdic.etat){
                
                 cell.mButtonCheck.selected                      =   YES;
            }else{
                
              cell.mButtonCheck.selected                      =   NO;
            }
            
            [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"2"])
        {
            cell.mButtonCheck.selected                      =   YES;
            [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([aPayer isEqualToString:@"0"])
        {
            cell.mButtonCheck.hidden                        =   YES;
        }

        cell.alpha                                          =   0.5;
        
        switch (indexPath.row % 2)
        {
            case 0:     cell.contentView.backgroundColor    =   [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];     break;
            case 1:     cell.contentView.backgroundColor    =   [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];     break;
            default:    break;
        }
        	cell.selectionStyle                             =   UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else
    {
        //   float cellclientheight=  [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];
        static NSString *cellIdentifier_p                   =   @"MesRelvesViewCell";
        double montantApayer = 0;
        MesRelvesViewCell *cell                             =   (MesRelvesViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell                                            =   (MesRelvesViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        double montantdebit=[mdic.montantdebit doubleValue];
        double loyercomplmnet=[mdic.loyercompement doubleValue];
        double montantcredit=[mdic.montantcredit doubleValue];
        montantApayer+=(montantdebit+loyercomplmnet)-montantcredit;
        NSNumber *montantD = [NSNumber numberWithDouble:montantApayer];
        cell.alpha                                          =   0.5;
        cell.selectionStyle                                 =   UITableViewCellSelectionStyleNone;
        cell.mButtonCheck.tag                               =   indexPath.row;
        cell.mfirstlabel.text                               =   mdic.reference;
        cell.mSecondLabel.text                              =   mdic.libelle;
        cell.mThiredLabel.text                              =   [NSString stringWithFormat:@"%.02f", [montantD floatValue] ];
        //cell.mFourLabel.text                                =   mdic.loyercompement;
         if(self.isChecked){
             
             cell.mButtonCheck.hidden=YES;
             self.lblPaye.hidden = YES;
             
         }else{
            cell.mButtonCheck.hidden=NO;
             self.lblPaye.hidden = NO;
         }
  
        
        NSString *aPayer                                    =   mdic.apayer;
        NSString *mdevise                                   =  mdic.devise;
        
        if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"1"])
        {
           //
            if(mdic.etat){
                
                cell.mButtonCheck.selected                      =   YES;
            }else{
                
               cell.mButtonCheck.selected                      =   NO;
            }
            [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"2"])
        {
            //cell.mButtonCheck.selected                      =   YES;
            [cell.mButtonCheck setImage:[UIImage imageNamed:@"check_gris"] forState:UIControlStateNormal];
        
            [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([aPayer isEqualToString:@"0"])
        {
            cell.mButtonCheck.hidden                        =   YES;
        }
        
        switch (indexPath.row % 2)
        {
            case 0:     cell.contentView.backgroundColor    =   [UIColor whiteColor];                                                               break;
            case 1:     cell.contentView.backgroundColor    =   [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];    break;
            default:    break;
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
    
    NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
    
    NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
    long somme=0;
    for(int i=0;i<self.mArraySelectedFactures.count;i++){
        
        
        ReleveObj *obj=[[ReleveObj alloc]init];
        
        obj=[self.mArraySelectedFactures objectAtIndex:i];
       
        
        if(obj.etat){
            long montantdebit=[obj.montantdebit longLongValue];
            long loyercomplmnet=[obj.loyercompement longLongValue];
            long montantcredit=[obj.montantcredit longLongValue];
            somme+=(montantdebit+loyercomplmnet)-montantcredit;
            self.montant = somme;
     }
        
        
    }
//    if(somme<0){
//        self.payerbutton.enabled=NO;
//        [self.payerbutton setBackgroundColor:[UIColor grayColor]];
//    }else{
//         [self.payerbutton setBackgroundColor:[UIColor blueColor]];
//        
//    }
    
    NSLog(@"diSel");
    
}


// MARK: - Actions
-(void)didSelectedCheckButton:(UIButton *)btn
{
//    NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
//    
//    NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
    ReleveObj *dic                               =   [[ReleveObj alloc] init];
        dic                        =   [self.mImpayes objectAtIndex:btn.tag];
    NSString *aPayer                                =  dic.apayer;
    NSString *mdevise                               =   dic.devise;

    if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"1"])
    {
        UIButton* selectedButton                    =   ((UIButton*) btn);
        [selectedButton setSelected                 :   !((UIButton*) btn).isSelected];
        
        if (selectedButton.selected)
        {
            dic.etat=true;
           [self.mArraySelectedFactures addObject   :   dic];
        }
        else
        {
            dic.etat=false;
            for (int j=0;j<self.mArraySelectedFactures.count;j++)
            {
                ReleveObj *dicarray       =   [self.mArraySelectedFactures objectAtIndex:j];
                
                if ([dicarray.reference isEqualToString:dic.reference])
                {
                    
                   [self.mArraySelectedFactures removeObjectAtIndex:j];
                }
            }
        }
    }
    
    self.somme=0;
    for(int i=0;i<self.mArraySelectedFactures.count;i++){
        
        
        ReleveObj *obj=[[ReleveObj alloc]init];
        
        obj=[self.mArraySelectedFactures objectAtIndex:i];
        
        
        if(obj.etat){
            double montantdebit=[obj.montantdebit doubleValue];
            double loyercomplmnet=[obj.loyercompement doubleValue];
            double montantcredit=[obj.montantcredit doubleValue];
            self.somme+=(montantdebit+loyercomplmnet)-montantcredit;
           
        }
        
        else{
            self.lblMontant.text = @"0.0";
        }
        
        
        
    }
    self.montantApayer=self.somme;
  
    self.lblMontant.text = [NSString stringWithFormat:@"%.02f", self.somme];
    if(self.mArraySelectedFactures.count==0){
          self.lblMontant.text = @"0.0";
    }
    
    if(self.somme <= 0 ){
       // self.payerbutton.enabled=NO;
        [self.payerbutton setBackgroundColor:[UIColor grayColor]];
    }else if(self.somme > 0 ){
        // self.payerbutton.enabled=YES;
        [self.payerbutton setBackgroundColor:[UIColor blueColor]];
        
    }
}




@end
