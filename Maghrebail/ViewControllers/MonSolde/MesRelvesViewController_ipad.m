//
//  MesRelvesViewController_ipad.m
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "MesRelvesViewController_ipad.h"
#import "MesRelvesViewCell.h"
#import "ReleveObj.h"

@implementation MesRelvesViewController_ipad


// MARK: - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self                                                =   [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        shouldDisplayLogo                               =   YES;
        shouldDisplayTitre                              =   YES;
        shouldAutorate = YES;
    }
    
    return self;
}


// MARK: - View states
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
  
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.tableView.estimatedRowHeight                   =   58.0;
    self.tableView.rowHeight                            =   UITableViewAutomaticDimension;
}
- (void) initHeaderColor
{
    NSLog(@"- entered initHeaderColor()");
    
    headerView.labelheaderTitre.text                    =   @"Mes Paiements";
    headerView.backgroundColor                          =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor                           =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor                               =   @"orange";
}

// MARK: - Tableview data source & delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.isChecked){
        
        return self.mArraySelectedFactures.count;
    }else{
        //        NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
        //
        //        NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
        return self.mImpayes.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReleveObj *mdic                               =   [[ReleveObj alloc] init];
    
    
    
    //    NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
    //
    //    NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
    if(self.isChecked){
        mdic                                                    =   [self.mArraySelectedFactures objectAtIndex:indexPath.row];
    }else{
        mdic                                                    =   [self.mImpayes objectAtIndex:indexPath.row];
    }
    //float cellclientheight =  [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"LIBELLE"]];

    static NSString *cellIdentifier_p                   =   @"MesRelvesViewCell_ipad";
    MesRelvesViewCell *cell                             =   (MesRelvesViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
          double montantApayer = 0;
    if (cell == nil)
    {
        cell            =   (MesRelvesViewCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
    }
    
    double montantdebit=[mdic.montantdebit doubleValue];
    double loyercomplmnet=[mdic.loyercompement doubleValue];
    double montantcredit=[mdic.montantcredit doubleValue];
    montantApayer+=(montantdebit+loyercomplmnet)-montantcredit;
    NSNumber *montantD = [NSNumber numberWithDouble:montantApayer];
    cell.alpha                                          =   0.5;
    cell.selectionStyle                                 =   UITableViewCellSelectionStyleNone;
    cell.mButtonCheck.tag                               =   indexPath.row;
    cell.mfirstlabel.text                               =   mdic.reference;
    cell.mSecondLabel.text                              =   mdic.dateechange;
    cell.mLabelDateEchange.text                         =   mdic.libelle;
    cell.mLastLabel.text                                =    [NSString stringWithFormat:@"%.02f", [montantD floatValue] ];;
    //cell.mFourLabel.text                                =   mdic.loyercompement;
    if(self.isChecked){
        
        cell.mButtonCheck.hidden=YES;
        self.lblPaye.hidden = YES;
        
    }else{
        cell.mButtonCheck.hidden=NO;
        self.lblPaye.hidden = NO;
    }
    
    
    NSString *aPayer                                    =   mdic.apayer;
    NSString *mdevise                                   =  mdic.devise;
    
    if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"1"])
    {
        //
        if(mdic.etat){
            
            cell.mButtonCheck.selected                      =   YES;
        }else{
            
            cell.mButtonCheck.selected                      =   NO;
        }
        [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"2"])
    {
        //cell.mButtonCheck.selected                      =   YES;
        [cell.mButtonCheck setImage:[UIImage imageNamed:@"check_gris"] forState:UIControlStateNormal];
        
        [cell.mButtonCheck addTarget                    :   self action:@selector(didSelectedCheckButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([aPayer isEqualToString:@"0"])
    {
        cell.mButtonCheck.hidden                        =   YES;
    }
    

    self.viewFrais.frame=CGRectMake(self.viewFrais.frame.origin.x, CGRectGetMaxY(self.tableView.frame), self.viewFrais.frame.size.width, self.viewFrais.frame.size.height);
    
    switch (indexPath.row % 2)
    {
        case 0:     cell.contentView.backgroundColor    =   [UIColor whiteColor];                                                               break;
        case 1:     cell.contentView.backgroundColor    =   [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];    break;
        default:    break;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}


// MARK: - Actions
-(void)didSelectedCheckButton:(UIButton *)btn
{
    ReleveObj *dic                               =   [[ReleveObj alloc] init];
    dic                        =   [self.mImpayes objectAtIndex:btn.tag];
    NSString *aPayer                                =  dic.apayer;
    NSString *mdevise                               =   dic.devise;
    
    if ([mdevise isEqualToString:@"MAD"] && [aPayer isEqualToString:@"1"])
    {
        UIButton* selectedButton                    =   ((UIButton*) btn);
        [selectedButton setSelected                 :   !((UIButton*) btn).isSelected];
        
        if (selectedButton.selected)
        {
            dic.etat=true;
            [self.mArraySelectedFactures addObject   :   dic];
        }
        else
        {
            dic.etat=false;
            for (int j=0;j<self.mArraySelectedFactures.count;j++)
            {
                ReleveObj *dicarray       =   [self.mArraySelectedFactures objectAtIndex:j];
                
                if ([dicarray.reference isEqualToString:dic.reference])
                {
                    
                    [self.mArraySelectedFactures removeObjectAtIndex:j];
                }
            }
        }
    }
    
    self.somme=0;
    for(int i=0;i<self.mArraySelectedFactures.count;i++){
        
        
        ReleveObj *obj=[[ReleveObj alloc]init];
        
        obj=[self.mArraySelectedFactures objectAtIndex:i];
        
        
        if(obj.etat){
            double montantdebit=[obj.montantdebit doubleValue];
            double loyercomplmnet=[obj.loyercompement doubleValue];
            double montantcredit=[obj.montantcredit doubleValue];
            self.somme+=(montantdebit+loyercomplmnet)-montantcredit;
            
        }
        
        else{
            self.lblMontant.text = @"0.0";
        }
        
        
        
    }
    self.montantApayer=self.somme;
    
    self.lblMontant.text = [NSString stringWithFormat:@"%.02f", self.somme];
    if(self.mArraySelectedFactures.count==0){
        self.lblMontant.text = @"0.0";
    }
    
    if(self.somme <= 0 ){
        // self.payerbutton.enabled=NO;
        [self.payerbutton setBackgroundColor:[UIColor grayColor]];
    }else if(self.somme > 0 ){
        // self.payerbutton.enabled=YES;
        [self.payerbutton setBackgroundColor:[UIColor blueColor]];
        
    }
    
    
    
}


@end
