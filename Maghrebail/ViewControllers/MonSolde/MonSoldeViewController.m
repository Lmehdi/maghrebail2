//
//  MonSoldeViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MonSoldeViewController.h"
#import "DetailPDFViewController_iphone.h"
#import "AppDelegate.h"
#import "ADNActivityCollection.h"

@interface MonSoldeViewController ()

@end

@implementation MonSoldeViewController
@synthesize canShareContent;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.canShareContent=NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES RELEVÉS";
    
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    [headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
    if(IS_IPHONE_5){
        headerView.btnRightHeader.frame=CGRectMake(280, headerView.btnLeftHeader.frame.origin.y, headerView.btnRightHeader.frame.size.width, headerView.btnRightHeader.frame.size.height);
    }
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    [self.btnPDFOut setHidden:true];
    [self.tableView setHidden:true];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
    
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    
    [self postDataToServer:URL_IMPAYE andParam:params andTag:0];
    
    self.labelInfo.hidden = YES;
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView) [headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
    }
    
}
- (void)showSideOptions{
    
    if (dataManager.authentificationData)
    {
        if (self.canShareContent) {
           [self shareContentView];
        }else{
            
//            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"deconnexionProfil"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"showMenu" object:nil];
//             alert = [BlockAlertView alertWithTitle:@"" message:@"Vous êtes déconnectés"];
//            [alert show];
//            [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:2.0];

            [super showSideOptions];
        }
        
    }else
    {
        if (self.canShareContent) {
            [self shareContentView];
        }
    }
  
}

-(void)shareContentView
{
    [self showProgresspdfHUD:YES];

    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       
                           NSString*  filePath_;
                       NSData *urlData=nil;
                       
                           
                           NSString *urlAddress = [[[self.response objectForKey:@"response"]objectAtIndex:0] objectForKey:@"pdf_url"];
                           
                           NSString *pdfname = [[urlAddress lastPathComponent] stringByDeletingPathExtension];//le nom du fichier sans extension
                           
                           NSFileManager *fileManager = [NSFileManager defaultManager];
                           
                           NSString *pdf_=[NSString stringWithFormat:@"%@/%@.%@",@"Library/Caches",pdfname,@"pdf"];
                           
                            filePath_= [NSHomeDirectory() stringByAppendingPathComponent:pdf_ ];
                           
                           if ([fileManager fileExistsAtPath:filePath_])
                           {
                               //  NSURL  *url = [NSURL URLWithString:filePath_];
                               urlData = [NSData dataWithContentsOfFile:filePath_];
                               
                               
                           }
                           else
                           {
                               NSURL  *url = [NSURL URLWithString:urlAddress];
                               urlData = [NSData dataWithContentsOfURL:url];
                               // [self savepdf:pdfname :urlAddress];
                               [urlData writeToFile:filePath_ atomically:YES];
                               
                               
                           }
                       
                       
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          
                                          NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                          NSString *documentsDirectory = [paths objectAtIndex:0];
                                
                                          NSURL *URL = [NSURL fileURLWithPath:filePath_];
                                          NSArray *activityItems = [NSArray arrayWithObjects:URL, nil];
                                          UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                                          
                                          
                                          AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
                                          
                                          
//                                          NSArray *allActivities = [ADNActivityCollection allActivities];
//                                          NSArray *activityItems = @[@"DSZ",urlData];
//                                          
//                                          NSMutableArray *activities = [NSMutableArray array];
//                                          [activities addObjectsFromArray:allActivities];
//                                          
//                                          UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems
//                                                                                                                     applicationActivities:activities];
                                          
                                          // Exclude default activity types for demo.
                                          //         NSArray * excludeActivities = @[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeMessage];
                                          
                                          [mainDelegate.window.rootViewController presentViewController: activityViewController animated:YES completion:nil ];
                                          
                                          [self showProgresspdfHUD:NO];
                                      });
                       
                   });
    
    
}

/*
-(void)shareContent
{
    AppDelegate *mainDelegate = [[UIApplication sharedApplication] delegate];
    //read data from cach
    NSData *dataPartage = [DataManager readDataIntoCachWith:@"dataPartage"];
    NSDictionary *dicoPartage = [NSKeyedUnarchiver unarchiveObjectWithData:dataPartage];
    
    //Partage
    NSArray *activityItems = [NSArray array];
    
    //For mail
    NSDictionary *dicoEmail = [dicoPartage objectForKey:@"Email"];
    
    //For texting : face book and twitter and ...
    NSDictionary *dicoFacebook = [dicoPartage objectForKey:@"Facebook"];
    
    //For everything else
    activityItems = @[[dicoFacebook objectForKey:@"message"],[NSURL URLWithString:[dicoFacebook objectForKey:@"url"]]];
    
    NSArray *allActivities = [ADNActivityCollection allActivities];
    
    NSMutableArray *activities = [NSMutableArray array];
    [activities addObjectsFromArray:allActivities];
    
    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                               applicationActivities:activities];
    
    // Exclude default activity types for demo.
    activityView.excludedActivityTypes = @[
                                           UIActivityTypePostToWeibo,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           ];
    
    [mainDelegate.window.rootViewController presentViewController:activityView animated:YES completion:NULL];





}
 */

#pragma mark - alertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"6b7761d25ded57704372fb7f74e2dc50",@"token" ,nil];
        
        if (dataManager.isDemo)
        {
            params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
        }
        //partage document via webservice
        
        [self postDataToServer:URL_SHARE_IMPAYES andParam:params andTag:2];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)updateViewTag:(int)inputTag{
    [super updateViewTag:inputTag];
    switch (inputTag) {
        case 0:
            if ([[[self.response objectForKey:@"header"] objectForKey:@"status"] isEqualToString:@"OK"]) {
                
                if ([[[[self.response objectForKey:@"response"] objectForKey:@"Type_Resultat"] objectForKey:@"Type_Resultat"] isEqualToString:@"D"]) {
                    [self btnPDF:self.btnPDFOut];
                    self.canShareContent=YES;
                    if (headerView) [headerView.btnRightHeader setImage:[UIImage imageNamed:@"icone-envoyer.png"] forState:UIControlStateNormal];
                }else{
                    
                    self.canShareContent=NO;
                    if (dataManager.authentificationData)
                    {
                        if (headerView)[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
                    }
                    
                    self.solde.text = [NSString stringWithFormat:@"%@",[[[self.response objectForKey:@"response"] objectForKey:@"Impaye_message"] objectForKey:@"Impaye_message"]];
                    self.solde.adjustsFontSizeToFitWidth=YES;
                    self.solde.frame=CGRectMake(0, 300, self.solde.frame.size.width, self.solde.frame.size.height);
                }
            }else{
                
            }
            break;
        case 1:
            
            if ([[[self.response objectForKey:@"header"] objectForKey:@"status"] isEqualToString:@"OK"]) {
                //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"pdf_url"]]];
                [self openPDF:[[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"pdf_url"]];
                
                
                
            }
            break;
        case 2:
            
            if ([[[self.response objectForKey:@"header"] objectForKey:@"status"] isEqualToString:@"OK"]) {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@" "
                                                                  message:[[self.response objectForKey:@"header"] objectForKey:@"message"]
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles:nil];
                
                [message show];
                [message release];
            }
            break;
        default:
            break;
    }
}

-(void) openPDF:(NSString*) pdfUrl{
    [self showProgressHUD:YES];
    NSURL *url = [NSURL URLWithString:pdfUrl];
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    //Load the request in the UIWebView.
    [self.webViewPDF loadRequest:requestObj];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self showProgressHUD:NO];
}

- (void)webView:(UIWebView *) webView didFailLoadWithError:(NSError *)error
{
    [self showProgressHUD:NO];
    [BaseViewController showAlert:@"Info" message:@"Problème affichage PDF" delegate:nil confirmBtn:@"OK" cancelBtn:@""];
}
- (void)dealloc
{
    
    [_solde release];
    [_labelInfo release];
    [_btnPDFOut release];
    [_webViewPDF release];
    [super dealloc];
}

-(void)viewDidUnload{
    
    [self setSolde:nil];
    [self setLabelInfo:nil];
    [super viewDidUnload];
}
- (IBAction)btnPDF:(UIButton *)sender {
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    [self postDataToServer:URL_IMPAYE_PDF andParam:params andTag:1];
    
}
@end
