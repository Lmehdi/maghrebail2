//
//  MesRelvesViewController.m
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "MesRelvesViewController.h"
#import "MesRelvesViewCell.h"
#import "ReleveObj.h"
#import "MtcViewController.h"
#import "MtcViewController_iphone.h"


@interface MesRelvesViewController ()

@end
MtcViewController *mtcViewController;
double somme=0;
@implementation MesRelvesViewController





// MARK: - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        shouldAutorate = YES;
    }
    
    return self;
}


// MARK: - View states
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"- entered viewDidLoad()");
    
    
    
//    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.lblmaghrebailtxt.text];
//    [attributeString addAttribute:NSUnderlineStyleAttributeName
//                            value:[NSNumber numberWithInt:1]
//
//                            range:(NSRange){0,[attributeString length]}];
//    NSMutableAttributedString *attributeStringg = [[NSMutableAttributedString alloc] initWithString:self.lblConditions.text];
    
    ////
//    [attributeStringg addAttribute:NSUnderlineStyleAttributeName
//                            value:[NSNumber numberWithInt:1]
//                            range:(NSRange){0,[attributeString length]}];
 //   self.lblmaghrebailtxt.attributedText=attributeString;
    
    //self.lblConditions.attributedText=@"attributeStringg";
    
    self.lblConditions.text=@"- J'ai lu et j'accepte les conditions générales d'utilisation.";
    
    self.tableViewHeight = self.tableView.frame.size.height;
    
    //self.lblMontant.text = @"0.0";
    self.mArraySelectedFactures                         =   [[NSMutableArray alloc]init];
    self.mrefArray                                      =   [[NSMutableArray alloc]init];
    
    self.payerbutton.layer.cornerRadius                 =   3;
    self.payerbutton.clipsToBounds                      =   YES;
    //self.payerbutton.enabled=NO;
    self.isChecked = false;
    self.btnCheck.selected = false;
    self.btnpayer2.hidden = YES;
    //self.btnpayer2.enabled = NO;
    self.btnAnnuler.hidden = true;
    //self.btnAnnuler.enabled = false;
    
    [self.payerbutton setBackgroundColor:[UIColor grayColor]];
    
    self.viewFrais.hidden = YES;
    self.lblLibelleFrais.hidden = YES;
    self.lblFraisMontant.hidden = YES;
    
    NSString *email                                     =   [[NSUserDefaults standardUserDefaults] objectForKey:@"USER-EMAIL"];
    
    NSString* baseUrl                                   =   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php";
    NSString *url                                       =   [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50&action=payement&email=%@",baseUrl, email];
    
    [self getRemoteContent                              :   url andTag:0];
    //[self showLoadingView                               :   YES];
    
    [self showProgressHUD:YES];
}



-(void)reloadTableViewDataSource{
    
    [super reloadTableViewDataSource];
    
    NSString *email                                     =   [[NSUserDefaults standardUserDefaults] objectForKey:@"USER-EMAIL"];
    
    NSString* baseUrl                                   =   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php";
    NSString *url                                       =   [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50&action=payement&email=%@",baseUrl, email];
    
    [self getRemoteContent                              :   url andTag:0];
    
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        if (dataManager.authentificationData)
        {
            if (headerView)
            {
                [headerView.btnRightHeader setImage     :   [UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            if (headerView)
            {
                [headerView.btnRightHeader setImage     :   [UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
            }
        }
    }
    
    [_tableView reloadData];
}


// MARK: - Methods
-(void)updateViewTag:(int)inputTag
{
    NSLog(@"- entered updateViewTag()");

    [self showLoadingView:NO];
    [self showProgressHUD:NO];
    NSLog(@"RESPONSe %@",self.response);
    
    self.mImpayes                                       =   [[NSMutableArray alloc]init];
    
    if(inputTag==0){
        NSMutableDictionary *mResponseData                  =   [self.response objectForKey:@"response"];
        NSMutableDictionary *typeResult                     =   [mResponseData objectForKey:@"Type_Resultat"];
        
        NSString *msg                                       =   [typeResult objectForKey:@"Type_Resultat"];
        
        if ([msg isEqualToString:@"G"])
        {
            self.mViewMsg.hidden                            =   NO;
            self.mLabelMsg.text                             =   [[mResponseData objectForKey:@"Impaye_message"] objectForKey:@"Impaye_message"];
        }
        else
        {
            self.mImpayes=[[NSMutableArray alloc] init];
            NSMutableArray *arrayData=[[NSMutableArray alloc]init];
            
            arrayData=[mResponseData objectForKey:@"Impayes"];
            [DataManager deleteFileFromCache:@"IMPS"];
            // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"IMPS"];
            for(int i=0;i<arrayData.count;i++){
                
                NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
                ReleveObj *relve=[[ReleveObj alloc]init];
                dic=[arrayData objectAtIndex:i];
                self.lblTiers.text=[dic objectForKey:@"TIERS"];
                self.lblCaseNumber.text=[dic objectForKey:@"N_AFFAIRE"];
                relve.tiers=[dic objectForKey:@"TIERS"];
                relve.rstiers=[dic objectForKey:@"RS_TIERS"];
                relve.naffaire=[dic objectForKey:@"N_AFFAIRE"];
                relve.reference=[dic objectForKey:@"REFERENCE"];
                relve.dateechange=[dic objectForKey:@"DATE_ECHEANCE"];
                relve.libelle=[dic objectForKey:@"LIBELLE"];
                relve.montantdebit=[dic objectForKey:@"MONTANT_DEBIT"];
                relve.montantcredit=[dic objectForKey:@"MONTANT_CREDIT"];
                relve.loyercompement=[dic objectForKey:@"LOYERS_COMPLEMENTAIRES"];
                relve.devise=[dic objectForKey:@"DEVISE"];
                relve.apayer=[dic objectForKey:@"A_PAYER"];
                relve.idekip =[dic objectForKey:@"IDEKIP"];
                [self.mImpayes addObject:relve];
                
                
            }
            
//            NSData *dataImp=[NSKeyedArchiver archivedDataWithRootObject:self.mImpayes];
//            //  [[NSUserDefaults standardUserDefaults] setObject:dataImp forKey:@"IMPS"];
//            [DataManager writeDataIntoCachWith:dataImp andKey:@"IMPS"];
            
            
            
        }
        
        [self.tableView reloadData];
    }else if(inputTag==3){
            NSString *mfrais=[[[self.response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"Frais"];
        
            self.lblFraisMontant.text=mfrais;
        self.sommeFrais=[mfrais doubleValue];
        self.lblMontant.text =[NSString stringWithFormat:@"%.02f", somme+self.sommeFrais];
        
    }else{

        NSString *header=[[self.response objectForKey:@"header"] objectForKey:@"status"];
        if([header isEqualToString:@"OK"]){
            
            NSString *murl=[[[self.response objectForKey:@"response"]objectAtIndex:0] objectForKey:@"url"];
            NSLog(@"urltest : %@" ,murl);
            
          //  if(IS_IPHONE){
                
                mtcViewController = [[MtcViewController_iphone alloc] initWithNibName:@"MtcViewController_iphone_p" bundle:nil];
                mtcViewController.mUrl = murl;
                
         
            
            NSArray *controllers = [NSArray arrayWithObject:mtcViewController];
            
            self.navigationController.viewControllers = controllers;
            
//            NSURL *nsUrl = [NSURL URLWithString:murl];
//            NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl];
//            NSLog(@"request : %@" ,request);
//
//            [_mWebView loadRequest:request];
            
            
        }
        
    }
}

- (void) dealloc
{
    [_viewpayer release];
    NSLog(@"- entered dealloc()");
    [_lblConditions release];
    [_lblmaghrebailtxt release];
    [super dealloc];
    [_mTableView release];
    [_mLabelMsg release];
    [_mViewMsg release];
    [_ViewWebView release];
    [_payerbutton release];
    [_lblTiers release];
    [_lblCaseNumber release];
    [_headerInfosView release];
    [_mWebView release];
    [_lblMontant release];
    [_btnValider release];
    [_conditionsWebView release];
    [_viewValider release];
    [_lblConditions release];
    [_btnCheck release];
    [_btnOpenConditions release];
    [_btnpayer2 release];
    [_lblLibelleFrais release];
    [_lblFraisMontant release];
    [_viewFrais release];
    [_btnAnnuler release];
    [_lblPaye release];
}

- (void) initHeaderColor
{
    NSLog(@"- entered initHeaderColor()");
    
    headerView.labelheaderTitre.text                    =   @"Mes Paiements";
    headerView.backgroundColor                          =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor                           =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor                               =   @"orange";
}


// MARK: - Actions

- (IBAction)didSelectValider:(id)sender {
    NSLog(@"- entered didSelectValider()");

    self.viewValider.frame                          =   CGRectMake(0, SCREEN_HEIGHT, self.viewValider.frame.size.width, self.viewValider.frame.size.height);
    self.lblMontant.alpha = 1;
}

- (IBAction)didSelectCheckbox:(id)sender {
    NSLog(@"- entered didSelectCheckbox()");

    if(self.btnCheck.selected)
        [self.btnCheck setSelected:NO];
    else
        [self.btnCheck setSelected:YES];
}

- (IBAction)didSelectPayer2:(id)sender {
    NSLog(@"- entered didSelectPayer2()");

    [UIView animateWithDuration:0.4 delay:0.1 options:UIViewAnimationOptionCurveLinear  animations:^
     {
         //self.ViewWebView.frame                          =   CGRectMake(0, 60, self.ViewWebView.frame.size.width, self.ViewWebView.frame.size.height);
     }
                     completion:^(BOOL finished)
     {
         double somme=0;
         NSString* sommepayer = @"";
         self.mrefArray=[[NSMutableArray alloc]init];
         for(int i=0;i<self.mArraySelectedFactures.count;i++){
             
             
             ReleveObj *obj=[[ReleveObj alloc]init];
             
             obj=[self.mArraySelectedFactures objectAtIndex:i];
             
             if(obj.etat){
                 double montantdebit=[obj.montantdebit doubleValue];
                 double loyercomplmnet=[obj.loyercompement doubleValue];
                 double montantcredit=[obj.montantcredit doubleValue];
                 
                 somme+=(montantdebit+loyercomplmnet)-montantcredit;
                 
                 [_mrefArray addObject:obj.idekip];
                 
                 NSLog(@"_mrefArray : (%@)", _mrefArray);
                 
             }
         }
         
         sommepayer = [NSString stringWithFormat:@"%.02f", somme+self.sommeFrais];
         self.lblMontant.text =sommepayer;
         NSLog(@"sommepayer : (%@)", sommepayer);
         //ReleveObj *mdic                               =   [[ReleveObj alloc] init];
         //NSData *mdata=[DataManager readDataIntoCachWith:@"IMPS"];
         //NSMutableArray *arrayIms=[ NSKeyedUnarchiver unarchiveObjectWithData:mdata];
         
         NSString *email                                     =   [[NSUserDefaults standardUserDefaults] objectForKey:@"USER-EMAIL"];
         NSLog(@"email : (%@)", email);
         
         NSString* baseUrl                                   =   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/cmi.php";
         NSLog(@"baseUrl : (%@)", baseUrl);
         
         NSString* references = [self.mrefArray componentsJoinedByString:@","];
         NSLog(@"references : (%@)", references);
         
         NSString *url                                       =   [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50&action=paiement&email=%@&montant=%@&data=%@",baseUrl, email,sommepayer,references/*"\" \""*/];
         NSLog(@"url : (%@)", url);
         
         [self getRemoteContent                              :   url andTag:1];
         [self showLoadingView:YES];

     }];
    
}

- (IBAction)actionShowConditions:(id)sender {
    NSLog(@"- entered actionShowConditions()");

    [UIView animateWithDuration:0.4 delay:0.1 options:UIViewAnimationOptionCurveLinear  animations:^
     {
         self.viewValider.frame                          =   CGRectMake(0, 47, self.viewValider.frame.size.width, self.viewValider.frame.size.height);
     }
     
                     completion:^(BOOL finished)
     {
         
         
         NSString *murl=@"https://www.maghrebail.ma/crm/mobile/dynamique/pdf/CGU-MAGHREBAIL.pdf";
         NSURL *nsUrl = [NSURL URLWithString:murl];
         NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl];
         
         [self.conditionsWebView loadRequest:request];
         
         
     }];
}


- (IBAction)didSelectedPayer:(id)sender
{
    NSLog(@"- entered didSelectedPayer()");
    
   if(self.somme > 0 ){
    if(self.btnCheck.selected){
    self.btnAnnuler.hidden = false;
    [UIView animateWithDuration:0.4 delay:0.1 options:UIViewAnimationOptionCurveLinear  animations:^
     {
         //self.ViewWebView.frame                          =   CGRectMake(0, 47, self.ViewWebView.frame.size.width, self.ViewWebView.frame.size.height);
     }
                     completion:^(BOOL finished)
     {
        
         NSString* sommepayer = @"";
         for(int i=0;i<self.mArraySelectedFactures.count;i++){
             
             
             ReleveObj *obj=[[ReleveObj alloc]init];
             
             obj=[self.mArraySelectedFactures objectAtIndex:i];
             
             if(obj.etat){
                 double montantdebit=[obj.montantdebit doubleValue];
                 double loyercomplmnet=[obj.loyercompement doubleValue];
                 double montantcredit=[obj.montantcredit doubleValue];
                 
                 somme+=(montantdebit+loyercomplmnet)-montantcredit;
                 
                 [_mrefArray addObject:obj.idekip];
                 
                 NSLog(@"_mrefArray : (%@)", _mrefArray);
                
             }
         }
         sommepayer = [NSString stringWithFormat:@"%.02f", somme];
     
         NSLog(@"sommepayer : (%@)", sommepayer);
         
         NSString *email                                     =   [[NSUserDefaults standardUserDefaults] objectForKey:@"USER-EMAIL"];
         NSLog(@"email : (%@)", email);
         
         NSString* baseUrl                                   =   @"https:www.maghrebail.ma/crm/mobile/dynamique/ws/cmi.php";
         NSLog(@"baseUrl : (%@)", baseUrl);
         
         NSString* references = [self.mrefArray componentsJoinedByString:@","];
         NSLog(@"references : (%@)", references);
     
         NSString *url                                       =   [NSString stringWithFormat:@"%@?token=6b7761d25ded57704372fb7f74e2dc50&action=paiement&email=%@&montant=%@&data=%@",baseUrl, email,sommepayer,references/*"\" \""*/];
         NSLog(@"url : (%@)", url);
         
       //  [self getRemoteContent                              :   url andTag:1];
         self.isChecked=true;
         
         //self.btnOpenConditions.enabled = NO;
         self.payerbutton.hidden = YES;
         self.lblConditions.hidden=YES;
         self.btnCheck.hidden = YES;
         self.btnOpenConditions.hidden = YES;
         //self.payerbutton.enabled = NO;
         self.btnpayer2.hidden = NO;
         //self.btnpayer2.enabled = YES;
         
         //self.btnAnnuler.enabled = true;
         
         [self.tableView reloadData];
         self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.mArraySelectedFactures.count*60);

         self.viewFrais.hidden = NO;
         self.lblLibelleFrais.hidden = NO;
         self.lblFraisMontant.hidden = NO;
         CGRect frameRect = self.viewFrais.frame;
         frameRect.origin.y = (self.tableView.frame.origin.y) + (self.tableView.frame.size.height);
         self.viewFrais.frame = frameRect;
         NSString *mFraisUrl=[NSString stringWithFormat:@"https://www.maghrebail.ma/crm/mobile/dynamique/ws/frais.php?token=6b7761d25ded57704372fb7f74e2dc50&action=frais&montant=%@",sommepayer];
         
          [self getRemoteContent                              :   mFraisUrl andTag:3];
           [self showProgressHUD:YES];
         
     }];
    }else{
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Conditions générale"
                                                          message:@"Veuillez accepter les conditions générale"
                                                         delegate:self
                                                cancelButtonTitle:@"ok"
                                                otherButtonTitles:nil];
        message.tag = 1;
        [message show];
        [message release];
        
    }
   }
}

- (IBAction)actionAnnuler:(id)sender {
    NSLog(@"- entered actionAnnuler()");
    self.lblMontant.text=@"0.0";
    self.isChecked=false;
    self.somme=0;
    self.btnCheck.hidden = NO ;
    self.lblConditions.hidden = NO;
    self.btnAnnuler.hidden = false;
    self.btnOpenConditions.enabled = YES;
     self.btnOpenConditions.hidden = NO;
    self.payerbutton.hidden = NO;
    //self.payerbutton.enabled = YES;
    self.btnpayer2.hidden = YES;
    somme=0;
    //self.btnpayer2.enabled = NO;
    self.viewFrais.hidden = YES;
    self.lblLibelleFrais.hidden = YES;
    self.lblFraisMontant.hidden = YES;
    self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableViewHeight);
    [self.tableView reloadData];
    //self.btnAnnuler.enabled = false;
    [self viewDidLoad];
}


- (IBAction)conditionbtnPressed:(id)sender
{
    NSLog(@"- entered conditionbtnPressed()");

    [UIView animateWithDuration:0.4 delay:0.1 options:nil  animations:^
     {
         self.lblConditions.alpha = 1.0;
     }
                     completion:^(BOOL finished)
     {
         //
     }];
}

- (IBAction)conditionbtnReleased:(id)sÒender
{
    NSLog(@"- entered conditionbtnReleased()");

    [UIView animateWithDuration:0.4 delay:0.1 options:nil  animations:^
     {
         self.lblConditions.alpha = 0.5;
     }
                     completion:^(BOOL finished)
     {
         //
     }];
}

- (IBAction)didselectedclosebutton:(id)sender
{
    
    NSURL *nsUrl = [NSURL URLWithString:@""];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl];
    NSLog(@"request : %@" ,request);
    
    [_mWebView loadRequest:request];
    NSLog(@"- entered didselectedclosebutton()");

    [UIView animateWithDuration:0.4 delay:0.1 options:UIViewAnimationOptionCurveLinear  animations:^
     {
         self.ViewWebView.frame                          =   CGRectMake(0, SCREEN_HEIGHT, self.ViewWebView.frame.size.width, self.ViewWebView.frame.size.height);
         self.viewValider.frame                          =   CGRectMake(0, SCREEN_HEIGHT, self.viewValider.frame.size.width, self.viewValider.frame.size.height);
         self.isChecked=false;
         NSLog(@"%ld count", self.mImpayes.count);
         self.btnCheck.hidden = NO;
         self.lblConditions.hidden = NO;
         //self.btnOpenConditions.enabled = YES;
         self.payerbutton.hidden = NO;
       //  self.payerbutton.enabled = YES;
         self.btnpayer2.hidden = YES;
         //self.btnpayer2.enabled = NO;
         self.viewFrais.hidden = YES;
         self.btnAnnuler.hidden = true;
         self.lblMontant.text=@"0.0";
         //self.btnAnnuler.enabled = false;
         self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableViewHeight);
         [self.tableView reloadData];
         [self viewDidLoad];

         //[self viewWillAppear:YES];
     }
                     completion:^(BOOL finished)
     {
         //
     }];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self showProgressHUD:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self showProgressHUD:NO];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
     [self showProgressHUD:NO];
}

@end
