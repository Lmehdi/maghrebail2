//
//  MtcViewController.m
//  Maghrebail
//
//  Created by Raoui M on 23/11/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "MtcViewController.h"
#import "MesRelvesViewController_iphone.h"
#import "MesRelvesViewController.h"
#import "MesRelvesViewController_ipad.h"
@interface MtcViewController ()

@end
MesRelvesViewController *mtcViewController;
@implementation MtcViewController

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    
//    if (self)
//    {
//        shouldAutorate = YES;
//    }
//    
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *nsUrl = [NSURL URLWithString:self.mUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl];
    NSLog(@"request : %@" ,request);
    
    [_mWebView loadRequest:request];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void) initHeaderColor
//{
//    NSLog(@"- entered initHeaderColor()");
//    
//    headerView.labelheaderTitre.text                    =   @"Mes Paiements";
//    headerView.backgroundColor                          =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
//    self.view.backgroundColor                           =   [UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
//    dataManager.typeColor                               =   @"orange";
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_mWebView release];
    [super dealloc];
}
- (IBAction)didSelectedClose:(id)sender {
    NSURL *nsUrl = [NSURL URLWithString:@""];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl];
    NSLog(@"request : %@" ,request);
    
    [_mWebView loadRequest:request];
    
    if(IS_IPHONE)
        mtcViewController = [[MesRelvesViewController_iphone alloc] initWithNibName:@"MesRelvesViewController_iphone" bundle:nil];
    else
     mtcViewController = [[MesRelvesViewController_ipad alloc] initWithNibName:@"MesRelvesViewController_ipad" bundle:nil];
    NSArray *controllers = [NSArray arrayWithObject:mtcViewController];

    self.navigationController.viewControllers = controllers;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    MBProgressHUD * progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Chargement";
    progressHUD.mode = MBProgressHUDModeIndeterminate;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
  [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
   [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
