//
//  MesRelvesViewCell.h
//  Maghrebail
//
//  Created by Raoui M on 07/08/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MesRelvesViewCell : UITableViewCell


// MARK: - Properties


// MARK: - Outlets
@property (retain, nonatomic) IBOutlet UIButton *mButtonCheck;
@property (retain, nonatomic) IBOutlet UILabel *mfirstlabel;
@property (retain, nonatomic) IBOutlet UILabel *mSecondLabel;
@property (retain, nonatomic) IBOutlet UILabel *mThiredLabel;
@property (retain, nonatomic) IBOutlet UILabel *mFourLabel;
@property (retain, nonatomic) IBOutlet UILabel *mfifthlabel;
@property (retain, nonatomic) IBOutlet UILabel *mSixstLabel;
@property (retain, nonatomic) IBOutlet UILabel *mLastLabel;

@property (retain, nonatomic) IBOutlet UILabel *mLabelDateEchange;

// MARK: - Methods


@end
