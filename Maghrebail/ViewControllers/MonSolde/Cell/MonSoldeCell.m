//
//  MonSoldeCell.m
//  Maghrebail
//
//  Created by Chaouki on 05/02/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import "MonSoldeCell.h"

@implementation MonSoldeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_labelNAffaire release];
    [_labelDate release];
    [_labelMTDebit release];
    [_labelMTCredit release];
    [super dealloc];
}
@end
