//
//  MonSoldeCell.h
//  Maghrebail
//
//  Created by Chaouki on 05/02/2015.
//  Copyright (c) 2015 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonSoldeCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *labelNAffaire;
@property (retain, nonatomic) IBOutlet UILabel *labelDate;
@property (retain, nonatomic) IBOutlet UILabel *labelMTDebit;
@property (retain, nonatomic) IBOutlet UILabel *labelMTCredit;

@end
