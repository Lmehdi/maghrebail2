//
//  MtcViewController.h
//  Maghrebail
//
//  Created by Raoui M on 23/11/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MesRelvesViewController.h"

@interface MtcViewController : UIViewController <UIWebViewDelegate>
@property (retain, nonatomic) IBOutlet UIWebView *mWebView;

@property(retain ,nonatomic ,setter=setUrl: ) NSString *mUrl;
- (IBAction)didSelectedClose:(id)sender;

@end
