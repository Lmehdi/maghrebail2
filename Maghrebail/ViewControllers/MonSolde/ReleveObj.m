//
//  ReleveObj.m
//  Maghrebail
//
//  Created by Raoui M on 15/09/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "ReleveObj.h"

@implementation ReleveObj

-(void)encodeWithCoder:(NSCoder *)aCoder{
  [aCoder encodeObject:self.tiers forKey:@"TIERS"];
  [aCoder encodeObject:self.rstiers forKey:@"rstiers"];
   [aCoder encodeObject:self.naffaire forKey:@"naffaire"];
     [aCoder encodeObject:self.reference forKey:@"reference"];
     [aCoder encodeObject:self.dateechange forKey:@"dateechange"];
     [aCoder encodeObject:self.montantdebit forKey:@"montantdebit"];
     [aCoder encodeObject:self.montantcredit forKey:@"montantcredit"];
     [aCoder encodeObject:self.loyercompement forKey:@"loyercompement"];
     [aCoder encodeObject:self.devise forKey:@"devise"];
     [aCoder encodeObject:self.apayer forKey:@"apayer"];
     [aCoder encodeObject:self.libelle forKey:@"libelle"];
    [aCoder encodeObject:self.etat forKey:@"etat"];
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    
    if(self = [super init]){
        self.tiers = [aDecoder decodeObjectForKey:@"TIERS"];
         self.rstiers = [aDecoder decodeObjectForKey:@"rstiers"];
         self.naffaire = [aDecoder decodeObjectForKey:@"naffaire"];
         self.reference = [aDecoder decodeObjectForKey:@"reference"];
         self.dateechange = [aDecoder decodeObjectForKey:@"dateechange"];
        self.montantdebit = [aDecoder decodeObjectForKey:@"montantdebit"];
         self.montantcredit = [aDecoder decodeObjectForKey:@"montantcredit"];
         self.loyercompement = [aDecoder decodeObjectForKey:@"loyercompement"];
         self.devise = [aDecoder decodeObjectForKey:@"devise"];
         self.apayer = [aDecoder decodeObjectForKey:@"apayer"];
         self.libelle = [aDecoder decodeObjectForKey:@"libelle"];
         self.etat = [aDecoder decodeObjectForKey:@"etat"];
    
    }
    return self;
}
@end
