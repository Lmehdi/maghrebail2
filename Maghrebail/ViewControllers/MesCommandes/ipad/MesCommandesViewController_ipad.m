//
//  MesCommandesViewController_ipad.m
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesCommandesViewController_ipad.h"
#import "ListCommandesViewController_ipad.h"
@interface MesCommandesViewController_ipad ()

@end

@implementation MesCommandesViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier_l;
    /*
     if ([DataManager isIphone5LandScape])
     {
     cellIdentifier_l = @"CommandeAffaireCell_iphone5_l";
     }
     else if  ([DataManager isIphone6LandScape])
     {
     cellIdentifier_l = @"CommandeAffaireCell_iphone6_l";
     }
     
     else
     
     cellIdentifier_l = @"CommandeAffaireCell_iphone_l";
     */
    cellIdentifier_l = @"CommandeAffaireCell_ipad";
    
    
    CommandeAffaireCell *cell = (CommandeAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
    if (cell == nil)
    {
        cell = (CommandeAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
    }
    
    
    NSMutableDictionary* currentItem   =   [[self.response objectForKey:@"response"]objectAtIndex:indexPath.row];
    
    
    /*
     CGRect oldFrame = cell.nomClient.frame;
     [cell.nomClient sizeToFit];
     oldFrame.size.height    =   cell.nomClient.frame.size.height;
     [cell.nomClient setFrame:oldFrame];
     */
    
    
    // cell.nocommande.frame=CGRectMake(cell.nocommande.frame.origin.x,cellclientheight/2 - 13, cell.nocommande.frame.size.width, cell.nocommande.frame.size.height);
    cell.nomClient.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
    cell.nocommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey :@"NO_COMMANDE"];
    if(btnSelectedEncours){
        cell.datecommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
    }else{
        cell.datecommande.text =  [DataManager getDateStringFromPresentableDate:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
    }
    cell.mt_ttc.text = [DataManager getFormatedNumero:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];
    cell.mt_ht_cmde.text =  [DataManager getFormatedNumero:[[dataManager.commandeFiltred  objectAtIndex:indexPath.row] objectForKey:@"MT_TTC_CMDE"]];
    
    cell.interlocuteur.text=[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"INTERLOCUTEUR"];
    
    
    //  cell.datecommande.frame=CGRectMake(cell.datecommande.frame.origin.x, cellclientheight/2 -13, cell.datecommande.frame.size.width, cell.datecommande.frame.size.height);
    
    // cell.mt_ht_cmde.frame=CGRectMake(cell.mt_ht_cmde.frame.origin.x, cellclientheight/2 -13 , cell.mt_ht_cmde.frame.size.width, cell.mt_ht_cmde.frame.size.height);
    
    //    cell.mt_ttc.frame=CGRectMake(cell.mt_ttc.frame.origin.x,cellclientheight/2 -13 , cell.mt_ttc.frame.size.width, cell.mt_ttc.frame.size.height);
    
    //   cell.interlocuteur.frame=CGRectMake(cell.interlocuteur.frame.origin.x,cellclientheight/2 - 13 , cell.interlocuteur.frame.size.width, cell.interlocuteur.frame.size.height);
    
    cell.alpha = 0.5;
    
    switch (indexPath.row % 2) {
        case 0:
            cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
            
            break;
        case 1:
            cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    
    return cell;

    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.isFromButton = NO;
    dataManager.isdataimmatriculation=NO;
    dataManager.isdatacommande=YES;
    dataManager.isdataengagement=NO;
    
    ListCommandesViewController_ipad * listCommandesViewController_iphone = [[ListCommandesViewController_ipad alloc]initWithNibName:@"ListCommandesViewController_ipad" bundle:nil];
    
    /*     if (!self.showFav)
     listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
     else
     listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
    listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
    [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
    
}
@end
