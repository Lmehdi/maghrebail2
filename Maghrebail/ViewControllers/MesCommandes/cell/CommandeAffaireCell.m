//
//  CommandeAffaireCell.m
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "CommandeAffaireCell.h"

@implementation CommandeAffaireCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)dealloc {
    [_idaffaire release];
    [_nocommande release];
    [_datecommande release];
    [_mt_ht_cmde release];
    [_mt_tva_cmde release];
    [_code_devise release];
    [_flecheimage release];
    [_interlocuteur release];
    [_mt_ttc release];
    [super dealloc];
}
-(void)didMoveToSuperview {
    [self layoutIfNeeded];
}

@end
