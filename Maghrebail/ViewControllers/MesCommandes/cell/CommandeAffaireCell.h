//
//  CommandeAffaireCell.h
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommandeAffaireCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *nomClient;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;
@property (retain, nonatomic) IBOutlet UILabel *interlocuteur;
@property (retain, nonatomic) IBOutlet UILabel *mt_ttc;

@property (retain, nonatomic) IBOutlet UILabel *idaffaire;
@property (retain, nonatomic) IBOutlet UILabel *nocommande;
@property (retain, nonatomic) IBOutlet UILabel *datecommande;
@property (retain, nonatomic) IBOutlet UILabel *mt_ht_cmde;
@property (retain, nonatomic) IBOutlet UILabel *mt_tva_cmde;
@property (retain, nonatomic) IBOutlet UILabel *code_devise;
@end
