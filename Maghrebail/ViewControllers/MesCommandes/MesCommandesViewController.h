//
//  MesCommandesViewController.h
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface MesCommandesViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    CGRect frameViewMontMin;
    CGRect frameViewMontMax;
    
    CGRect frameViewDateMin;
    CGRect frameViewDateMax;
    
    CGRect frameMontBtnMin;
    CGRect frameMontImgMin;
    CGRect frameMontLblMin;
    
    CGRect frameDateBtnMin;
    CGRect frameDateImgMin;
    CGRect frameDateLblMin;
    
    
    CGRect frameMontBtnMax;
    CGRect frameMontImgMax;
    CGRect frameMontLblMax;
    
    CGRect frameDateBtnMax;
    CGRect frameDateImgMax;
    CGRect frameDateLblMax;
    
    CGRect frameViewMontMinInitial;
    CGRect frameViewMontMaxInitial;
    
    CGRect frameViewDateMinInitial;
    CGRect frameViewDateMaxInitial;
    
    CGRect frameMontBtnMinInitial;
    CGRect frameMontImgMinInitial;
    CGRect frameMontLblMinInitial;
    
    CGRect frameDateBtnMinInitial;
    CGRect frameDateImgMinInitial;
    CGRect frameDateLblMinInitial;
    
    
    CGRect frameMontBtnMaxInitial;
    CGRect frameMontImgMaxInitial;
    CGRect frameMontLblMaxInitial;
    
    CGRect frameDateBtnMaxInitial;
    CGRect frameDateImgMaxInitial;
    CGRect frameDateLblMaxInitial;


}

/*{
    BOOL showFav;
    
}
 */
@property (retain, nonatomic) IBOutlet UILabel *interlocuteur;
@property (retain, nonatomic) IBOutlet UILabel *Id_affaire;
@property (retain, nonatomic) NSMutableArray *arrayFav;
//@property (assign, nonatomic) BOOL showFav;
//@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property (retain, nonatomic) IBOutlet UILabel *No_commande;
@property (assign, nonatomic)  int index;
@property (retain, nonatomic) IBOutlet UILabel *Date_commande;
@property (retain, nonatomic) IBOutlet UILabel *Mt_ht_commande;
@property (retain, nonatomic) IBOutlet UILabel *mt_tva_commande;
@property (retain, nonatomic) IBOutlet UIButton *btnaffaireencours;
@property (retain, nonatomic) IBOutlet UIButton *btnaffairesemises;
@property (retain, nonatomic) IBOutlet UIButton *btnaffairesvalidees;
@property (retain, nonatomic) IBOutlet UIButton *buttonEncours;
@property (retain, nonatomic) IBOutlet UIButton *buttonRegles;
@property (retain, nonatomic) IBOutlet UIButton *buttonClotures;
@property (retain, nonatomic) IBOutlet UILabel *statusaffaire;

@property (retain, nonatomic) IBOutlet UILabel *codedevise;

- (IBAction)filteraction:(id)sender;
- (IBAction)encoursPushed:(UIButton *)sender;
- (IBAction)reglesPushed:(UIButton *)sender;
- (IBAction)CloturesPushed:(UIButton *)sender;
-(void) initHeaderColor;

@end
