//
//  DetailsBienCell.m
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "DetailsBienCell.h"

@implementation DetailsBienCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [_thekey release];
  
    [_thevalue release];
    [super dealloc];
}
@end
