//
//  DetailsBienCell.h
//  Maghrebail
//
//  Created by Belkheir on 16/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsBienCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *thekey;
@property (retain, nonatomic) IBOutlet UILabel *thevalue;

@end
