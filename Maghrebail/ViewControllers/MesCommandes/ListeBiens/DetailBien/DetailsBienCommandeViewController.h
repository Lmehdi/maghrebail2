//
//  DetailsBienCommandeViewController.h
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"
#import  "QuartzCore/QuartzCore.h"
@interface DetailsBienCommandeViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property(nonatomic, retain)NSString * immobilisation;
@property(nonatomic, retain)NSString * idmvt;
@property(nonatomic, retain)NSMutableDictionary * dictionnary;

@property (retain, nonatomic) IBOutlet UILabel *titreheadertable;

-(void) initHeaderColor;
@end
