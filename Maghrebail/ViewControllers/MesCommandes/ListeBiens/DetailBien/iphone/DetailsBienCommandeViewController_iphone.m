//
//  DetailsBienCommandeViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "DetailsBienCommandeViewController_iphone.h"

@interface DetailsBienCommandeViewController_iphone ()

@end

@implementation DetailsBienCommandeViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}



- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self initHeaderColor];
    if(dataManager.isdetailsreglement){
          [Flurry logEvent:@"DetailsMesReglementsFournisseur"];
    }else{
          [Flurry logEvent:@"DetailsMesFacturesFournisseur"];
    }
   
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* item =nil;
    if(dataManager.isDemo) {
        item  = [self.response objectForKey:@"response"];
    } else {
        item  = [[self.response objectForKey:@"response"] objectAtIndex:0];
    }
    
    if (isLandscape) {
        static NSString *cellIdentifier_l;
        
     /*   if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"DetailsBienCell_iphone5_l";
        }
        else
            cellIdentifier_l = @"DetailsBienCell_iphone_l";
      */
        
        cellIdentifier_l = @"DetailsBienCell_iphone_l";

        DetailsBienCell *cell = (DetailsBienCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (DetailsBienCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }

     //   UIImageView *aLine = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y , cell.frame.size.width,0)];
      //  [aLine setImage:[UIImage imageNamed:@"1px.png"]];
       // [cell.contentView addSubview:aLine];
     //   cell.textLabel.text = [[[self.response objectForKey:@"response"]objectAtIndex:0]objectForKey:@"DESIGN_BIEN"];
        
        cell.alpha = 0.5;
       
     
            
        if(!dataManager.isdetailscontact)
        {
            switch (indexPath.row)
            {
                case 0:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Désignation";
                        
                        
                        if([item objectForKey:@"DESIGN_BIEN"] == nil ||  [[item objectForKey:@"DESIGN_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"DESIGN_BIEN"];
                        }
                        
                    }
                    else if (dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Immobilisation";
                        
                        
                        if([item objectForKey:@"IMMOBILISATION"] == nil ||  [[item objectForKey:@"IMMOBILISATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"IMMOBILISATION"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Libellé";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Bénéficiaire";
                        
                        
                        if([item objectForKey:@"BENEFICIAIRE"] == nil ||  [[item objectForKey:@"BENEFICIAIRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"BENEFICIAIRE"];
                        }
                        
                        
                    }
                    
                    
                    break;
                case 1:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Caractéristiques du Bien";
                        
                        if([item objectForKey:@"CARACT_BIEN"] == nil ||  [[item objectForKey:@"CARACT_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CARACT_BIEN"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Désignation";
                        
                        if([item objectForKey:@"DÉSIGNATION_BIEN"] == nil ||  [[item objectForKey:@"DÉSIGNATION_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DÉSIGNATION_BIEN"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant HT";
                        
                        
                        if([item objectForKey:@"MT_HT"] == nil ||  [[item objectForKey:@"MT_HT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_HT"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Tiers Client";
                        
                        
                        if([item objectForKey:@"TIERS_CLIENT"] == nil ||  [[item objectForKey:@"TIERS_CLIENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"TIERS_CLIENT"];
                        }
                        
                        
                    }
                    break;
                case 2:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Statut du bien";
                        
                        if([item objectForKey:@"CAT_BIEN"] == nil ||  [[item objectForKey:@"CAT_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CAT_BIEN"];
                        }
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Date Immatriculation";
                        
                        if([item objectForKey:@"DATE_IMMAT"] == nil ||  [[item objectForKey:@"DATE_IMMAT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_IMMAT"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant TVA";
                        
                        
                        if([item objectForKey:@"MT_TAXE"] == nil ||  [[item objectForKey:@"MT_TAXE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TAXE"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"N° Facture MAGHREBAIL";
                        
                        
                        if([item objectForKey:@"NO_MOUVEMENT"] == nil ||  [[item objectForKey:@"NO_MOUVEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"NO_MOUVEMENT"];
                        }
                        
                        
                        
                        
                    }
                    break;
                case 3:
                    
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"N°Serie ou Titre Foncier";
                        
                        if([item objectForKey:@"NUM_SERIE"] == nil ||  [[item objectForKey:@"NUM_SERIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"NUM_SERIE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Immatriculation";
                        
                        if([item objectForKey:@"IMMATRICULATION"] == nil ||  [[item objectForKey:@"IMMATRICULATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"IMMATRICULATION"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Libellé";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        
                        cell.thekey.text=@"Libellé Facture Fournisseu";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }
                        
                        
                    }
                    
                    break;
                case 4:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Lieu de Livraison";
                        
                        if([item objectForKey:@"LIEU_LIVRAISON"] == nil ||  [[item objectForKey:@"LIEU_LIVRAISON"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"LIEU_LIVRAISON"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Puissance";
                        
                        if([item objectForKey:@"PUISSANCE"] == nil ||  [[item objectForKey:@"PUISSANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"PUISSANCE"];
                        }
                        
                        
                    }
                    
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant TTC";
                        
                        
                        if([item objectForKey:@"MT_TTC"] == nil ||  [[item objectForKey:@"MT_TTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TTC"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Date Facture";
                        
                        
                        if([item objectForKey:@"DATE_MOUVEMENT"] == nil ||  [[item objectForKey:@"DATE_MOUVEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                           // cell.thevalue.text = [item objectForKey:@"DATE_MOUVEMENT"];
                            cell.thevalue.text = [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_MOUVEMENT"]];
                        }
                        
                        
                    }
                    break;
                case 5:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Date Réglement";
                        
                        if([item objectForKey:@"DATE_REGLEMENT"] == nil ||  [[item objectForKey:@"DATE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_REGLEMENT"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Date Echéance";
                        
                        if([item objectForKey:@"DATE_ECHEANCE"] == nil ||  [[item objectForKey:@"DATE_ECHEANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                           // cell.thevalue.text=[item objectForKey:@"DATE_ECHEANCE"];
                            cell.thevalue.text =[DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_ECHEANCE"]];
                        }
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Mode réglement";
                        
                        
                        if([item objectForKey:@"MODE_REGLEMENT"] == nil ||  [[item objectForKey:@"MODE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"MODE_REGLEMENT"];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        
                        
                        cell.thekey.text=@"Date Echéance";
                        
                        
                        if([item objectForKey:@"DATE_ECHEANCE"] == nil ||  [[item objectForKey:@"DATE_ECHEANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            //cell.thevalue.text=[item objectForKey:@"DATE_ECHEANCE"];
                            cell.thevalue.text= [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_ECHEANCE"]];
                        }
                        
                        
                        
                    }
                    break;
                case 6:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Montant d'Acquisition HT";
                        
                        if([item objectForKey:@"MHT"] == nil ||  [[item objectForKey:@"MHT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MHT"]] ;
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Genre";
                        
                        if([item objectForKey:@"GENRE"] == nil ||  [[item objectForKey:@"GENRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"GENRE"] ;
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Num chéque";
                        
                        
                        if([item objectForKey:@"NUM_CHEQUE"] == nil ||  [[item objectForKey:@"NUM_CHEQUE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"NUM_CHEQUE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Mode Réglement";
                        
                        
                        if([item objectForKey:@"MODE_REGLEMENT"] == nil ||  [[item objectForKey:@"MODE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"MODE_REGLEMENT"];
                        }
                        
                        
                    }
                    break;
                case 7:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Marque";
                        
                        if([item objectForKey:@"MARQUE"] == nil ||  [[item objectForKey:@"MARQUE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"MARQUE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Energie";
                        
                        if([item objectForKey:@"ENERGIE"] == nil ||  [[item objectForKey:@"ENERGIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"ENERGIE"];
                        }
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        
                        cell.thekey.text=@"Montant HT";
                        
                        if([item objectForKey:@"MT_HT"] == nil ||  [[item objectForKey:@"MT_HT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_HT"]];
                        }
                        
                        
                    }
                    break;
                case 8:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Date de livraison";
                        
                        if([item objectForKey:@"DATE_LIVRAISON"] == nil ||  [[item objectForKey:@"DATE_LIVRAISON"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_LIVRAISON"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Carrosserie";
                        
                        if([item objectForKey:@"CARROSSERIE"] == nil ||  [[item objectForKey:@"CARROSSERIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CARROSSERIE"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Montant TVA";
                        
                        if([item objectForKey:@"MT_TAXE"] == nil ||  [[item objectForKey:@"MT_TAXE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TAXE"]];
                        }
                        
                        
                    }
                    break;
                    
                case 9:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Montant d'Acquisition TTC";
                        
                        if([item objectForKey:@"MTTC"] == nil ||  [[item objectForKey:@"MTTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MTTC"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Date Prem Immat";
                        
                        if([item objectForKey:@"DATE_PREM_IMMAT"] == nil ||  [[item objectForKey:@"DATE_PREM_IMMAT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_PREM_IMMAT"];
                        }
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        
                        cell.thekey.text=@"Montant TTC";
                        
                        if([item objectForKey:@"MT_TTC"] == nil ||  [[item objectForKey:@"MT_TTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TTC"]];
                        }
                        
                        
                        
                        
                    }
                    
                    break;
                    
                case 10:
                    if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Date Ancienne Certif";
                        
                        if([item objectForKey:@"DATE_ANCIEN_CERTIF"] == nil ||  [[item objectForKey:@"DATE_ANCIEN_CERTIF"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_ANCIEN_CERTIF"];
                        }
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Numéro d'affaire";
                        
                        if([item objectForKey:@"IE_AFFAIRE"] == nil ||  [[item objectForKey:@"IE_AFFAIRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"IE_AFFAIRE"];
                        }
                        
                        
                    }
                    
                    break;
                case 11:
                    if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Date Mise en Circulation";
                        
                        if([item objectForKey:@"DATE_MISE_CIRCULATION"] == nil ||  [[item objectForKey:@"DATE_MISE_CIRCULATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_MISE_CIRCULATION"];
                        }
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"N° Commande";
                        
                        if([item objectForKey:@"NO_COMMANDE"] == nil ||  [[item objectForKey:@"NO_COMMANDE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"NO_COMMANDE"];
                        }
                        
                        
                    }
                    
                    break;
                    
                case 12:
                    if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Devise";
                        
                        if([item objectForKey:@"CODE_DEVISE"] == nil ||  [[item objectForKey:@"CODE_DEVISE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CODE_DEVISE"];
                        }
                        
                        
                        
                    }
                    
                    break;
                case 13:
                    if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Bon à Payer";
                        
                        if([item objectForKey:@"BON_A_PAYER"] == nil ||  [[item objectForKey:@"BON_A_PAYER"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"BON_A_PAYER"];
                        }
                        
                        
                        
                    }
                    
                    break;
                    
                default:
                    break;
            }
            
            
            
        }
        
        
        
        
        
        
        
        else
        {
            NSMutableDictionary* item  = [self.dictionnary mutableCopy];
            NSString *key=[[item allKeys]objectAtIndex:indexPath.row];
            NSString *value= [[item allValues]objectAtIndex:indexPath.row];
            if(key == nil ||  [key isKindOfClass:[NSNull class]])
            {
                cell.thekey.text   = @""  ;
                
            }
            else
            {
                cell.thekey.text=key;
            }
            if(value == nil ||  [value isKindOfClass:[NSNull class]])
            {
                cell.thevalue.text   = @""  ;
                
            }
            else
            {
                cell.thevalue.text=value;
            }
            
            
            
            
        }
        
        
        
        
     
        
        
        
        
        
       
        return cell;
    }
    else
    {
        
        

        static NSString *cellIdentifier_p = @"DetailsBienCell_iphone_p";
        
        DetailsBienCell *cell = (DetailsBienCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        
        if (cell == nil)
        {
            cell = (DetailsBienCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
       
    
        cell.alpha = 0.5;
        if(!dataManager.isdetailscontact)
        {
            switch (indexPath.row)
            {
                case 0:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Désignation";
                        
                        
                        if([item objectForKey:@"DESIGN_BIEN"] == nil ||  [[item objectForKey:@"DESIGN_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"DESIGN_BIEN"];
                        }
                        
                    }
                    else if (dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Immobilisation";
                        
                        
                        if([item objectForKey:@"IMMOBILISATION"] == nil ||  [[item objectForKey:@"IMMOBILISATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"IMMOBILISATION"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                    
                        cell.thekey.text=@"Libellé";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }

                    
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Bénéficiaire";
                        
                        
                        if([item objectForKey:@"BENEFICIAIRE"] == nil ||  [[item objectForKey:@"BENEFICIAIRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"BENEFICIAIRE"];
                        }

                    
                    }
                    
                    
                    break;
                case 1:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Caractéristiques du Bien";
                        
                        if([item objectForKey:@"CARACT_BIEN"] == nil ||  [[item objectForKey:@"CARACT_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CARACT_BIEN"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Désignation";
                        
                        if([item objectForKey:@"DÉSIGNATION_BIEN"] == nil ||  [[item objectForKey:@"DÉSIGNATION_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DÉSIGNATION_BIEN"];
                        }
                       
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant HT";
                        
                        
                        if([item objectForKey:@"MT_HT"] == nil ||  [[item objectForKey:@"MT_HT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
            cell.thevalue.text=[DataManager getFormatedNumero:[DataManager getFormatedNumero:[item objectForKey:@"MT_HT"]]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Tiers Client";
                        
                        
                        if([item objectForKey:@"TIERS_CLIENT"] == nil ||  [[item objectForKey:@"TIERS_CLIENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"TIERS_CLIENT"];
                        }

                        
                    }
                    break;
                case 2:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Statut du bien";
                        
                        if([item objectForKey:@"CAT_BIEN"] == nil ||  [[item objectForKey:@"CAT_BIEN"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CAT_BIEN"];
                        }
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Date Immatriculation";
                        
                        if([item objectForKey:@"DATE_IMMAT"] == nil ||  [[item objectForKey:@"DATE_IMMAT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        { //date
                           // cell.thevalue.text = [item objectForKey:@"DATE_IMMAT"];
                            cell.thevalue.text = [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_IMMAT"]];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant TVA";
                        
                        
                        if([item objectForKey:@"MT_TAXE"] == nil ||  [[item objectForKey:@"MT_TAXE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TAXE"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"N° Facture MAGHREBAIL";
                        
                        
                        if([item objectForKey:@"NO_MOUVEMENT"] == nil ||  [[item objectForKey:@"NO_MOUVEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"NO_MOUVEMENT"];
                        }
                        

                        
                        
                    }
                    break;
                case 3:
                    
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"N°Serie ou Titre Foncier";
                        
                        if([item objectForKey:@"NUM_SERIE"] == nil ||  [[item objectForKey:@"NUM_SERIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"NUM_SERIE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Immatriculation";
                        
                        if([item objectForKey:@"IMMATRICULATION"] == nil ||  [[item objectForKey:@"IMMATRICULATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"IMMATRICULATION"];
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Libellé";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        
                        cell.thekey.text=@"Libellé Facture Fournisseu";
                        
                        
                        if([item objectForKey:@"LIBELLE"] == nil ||  [[item objectForKey:@"LIBELLE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"LIBELLE"];
                        }

                        
                    }
                    
                    break;
                case 4:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Lieu de Livraison";
                        
                        if([item objectForKey:@"LIEU_LIVRAISON"] == nil ||  [[item objectForKey:@"LIEU_LIVRAISON"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"LIEU_LIVRAISON"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Puissance";
                        
                        if([item objectForKey:@"PUISSANCE"] == nil ||  [[item objectForKey:@"PUISSANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"PUISSANCE"];
                        }
                        
                        
                    }
                    
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Montant TTC";
                        
                        
                        if([item objectForKey:@"MT_TTC"] == nil ||  [[item objectForKey:@"MT_TTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TTC"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Date Facture";
                        
                        
                        if([item objectForKey:@"DATE_MOUVEMENT"] == nil ||  [[item objectForKey:@"DATE_MOUVEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            //cell.thevalue.text=[item objectForKey:@"DATE_MOUVEMENT"];
                            cell.thevalue.text = [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_MOUVEMENT"]];
                        }
 
                        
                    }
                    break;
                case 5:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Date Réglement";
                        
                        if([item objectForKey:@"DATE_REGLEMENT"] == nil ||  [[item objectForKey:@"DATE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_REGLEMENT"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Date Echéance";
                        
                        if([item objectForKey:@"DATE_ECHEANCE"] == nil ||  [[item objectForKey:@"DATE_ECHEANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {  //date
                            cell.thevalue.text=[item objectForKey:@"DATE_ECHEANCE"];
                        }
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Mode réglement";
                        
                        
                        if([item objectForKey:@"MODE_REGLEMENT"] == nil ||  [[item objectForKey:@"MODE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"MODE_REGLEMENT"];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                     
                        
                        cell.thekey.text=@"Date Echéance";
                        
                        
                        if([item objectForKey:@"DATE_ECHEANCE"] == nil ||  [[item objectForKey:@"DATE_ECHEANCE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                           // cell.thevalue.text=[item objectForKey:@"DATE_ECHEANCE"];
                            cell.thevalue.text= [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_ECHEANCE"]];
                        }

                        
                        
                    }
                    break;
                case 6:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Montant d'Acquisition HT";
                        
                        if([item objectForKey:@"MHT"] == nil ||  [[item objectForKey:@"MHT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MHT"]] ;
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Genre";
                        
                        if([item objectForKey:@"GENRE"] == nil ||  [[item objectForKey:@"GENRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"GENRE"] ;
                        }
                        
                        
                    }
                    else if (dataManager.isdetailsreglement)
                    {
                        
                        cell.thekey.text=@"Num chéque";
                        
                        
                        if([item objectForKey:@"NUM_CHEQUE"] == nil ||  [[item objectForKey:@"NUM_CHEQUE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"NUM_CHEQUE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Mode Réglement";
                        
                        
                        if([item objectForKey:@"MODE_REGLEMENT"] == nil ||  [[item objectForKey:@"MODE_REGLEMENT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            
                            
                            cell.thevalue.text=[item objectForKey:@"MODE_REGLEMENT"];
                        }

                        
                    }
                    break;
                case 7:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Marque";
                        
                        if([item objectForKey:@"MARQUE"] == nil ||  [[item objectForKey:@"MARQUE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"MARQUE"];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Energie";
                        
                        if([item objectForKey:@"ENERGIE"] == nil ||  [[item objectForKey:@"ENERGIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"ENERGIE"];
                        }
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                    
                        cell.thekey.text=@"Montant HT";
                        
                        if([item objectForKey:@"MT_HT"] == nil ||  [[item objectForKey:@"MT_HT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_HT"]];
                        }
                    
                    
                    }
                    break;
                case 8:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        
                        cell.thekey.text=@"Date de livraison";
                        
                        if([item objectForKey:@"DATE_LIVRAISON"] == nil ||  [[item objectForKey:@"DATE_LIVRAISON"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_LIVRAISON"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Carrosserie";
                        
                        if([item objectForKey:@"CARROSSERIE"] == nil ||  [[item objectForKey:@"CARROSSERIE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CARROSSERIE"];
                        }
                        
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Montant TVA";
                        
                        if([item objectForKey:@"MT_TAXE"] == nil ||  [[item objectForKey:@"MT_TAXE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TAXE"]];
                        }
                        
                        
                    }
                    break;
                    
                case 9:
                    if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
                    {
                        cell.thekey.text=@"Montant d'Acquisition TTC";
                        
                        if([item objectForKey:@"MTTC"] == nil ||  [[item objectForKey:@"MTTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MTTC"]];
                        }
                        
                        
                    }
                    else if(dataManager.isdetailsimmatriculation)
                    {
                        cell.thekey.text=@"Date Prem Immat";
                        
                        if([item objectForKey:@"DATE_PREM_IMMAT"] == nil ||  [[item objectForKey:@"DATE_PREM_IMMAT"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            //date
                            //cell.thevalue.text= [item objectForKey:@"DATE_PREM_IMMAT"];
                            
                             cell.thevalue.text=  [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_PREM_IMMAT"]];
                            
                        }
                        
                    }
                    else if(dataManager.isdatafacture)
                    {
                    
                        cell.thekey.text=@"Montant TTC";
                        
                        if([item objectForKey:@"MT_TTC"] == nil ||  [[item objectForKey:@"MT_TTC"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[DataManager getFormatedNumero:[item objectForKey:@"MT_TTC"]];
                        }

                    
                    
                    
                    }
                    
                    break;
                    
                case 10:
                    if(dataManager.isdetailsimmatriculation)
                    {
                        
                  cell.thekey.text=@"Date Ancienne Certif";
                        
                        if([item objectForKey:@"DATE_ANCIEN_CERTIF"] == nil ||  [[item objectForKey:@"DATE_ANCIEN_CERTIF"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"DATE_ANCIEN_CERTIF"];
                        }
                    }
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Numéro d'affaire";
                        
                        if([item objectForKey:@"IE_AFFAIRE"] == nil ||  [[item objectForKey:@"IE_AFFAIRE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"IE_AFFAIRE"];
                        }

                        
                    }
                    
                    break;
                case 11:
                    if(dataManager.isdetailsimmatriculation)
                    {
                        
                        cell.thekey.text=@"Date Mise en Circulation";
                        
                        if([item objectForKey:@"DATE_MISE_CIRCULATION"] == nil ||  [[item objectForKey:@"DATE_MISE_CIRCULATION"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            //date
                            //cell.thevalue.text=[item objectForKey:@"DATE_MISE_CIRCULATION"];
                            cell.thevalue.text = [DataManager getDateStringFromPresentableDate: [item objectForKey:@"DATE_MISE_CIRCULATION"]];
                        }
                        
                    }
                    
                    else if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"N° Commande";
                        
                        if([item objectForKey:@"NO_COMMANDE"] == nil ||  [[item objectForKey:@"NO_COMMANDE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"NO_COMMANDE"];
                        }
  
                        
                    }
                    
                    break;
                    
                 case 12:
                    if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Devise";
                        
                        if([item objectForKey:@"CODE_DEVISE"] == nil ||  [[item objectForKey:@"CODE_DEVISE"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"CODE_DEVISE"];
                        }
                        

                    
                    }
                    
                    break;
                case 13:
                    if(dataManager.isdatafacture)
                    {
                        cell.thekey.text=@"Bon à Payer";
                        
                        if([item objectForKey:@"BON_A_PAYER"] == nil ||  [[item objectForKey:@"BON_A_PAYER"] isKindOfClass:[NSNull class]])
                        {
                            cell.thevalue.text   = @""  ;
                            
                        }
                        else
                        {
                            cell.thevalue.text=[item objectForKey:@"BON_A_PAYER"];
                        }
                        
  
                        
                    }
                    
                    break;
                    
                default:
                    break;
            }
            
            
            
        }
        
        
        
        
        
        
        
        else
        {
            NSMutableDictionary* item  = [self.dictionnary mutableCopy];
            NSString *key=[[item allKeys]objectAtIndex:indexPath.row];
            NSString *value= [[item allValues]objectAtIndex:indexPath.row];
            if(key == nil ||  [key isKindOfClass:[NSNull class]])
            {
                cell.thekey.text   = @""  ;
                
            }
            else
            {
                cell.thekey.text=key;
            }
            if(value == nil ||  [value isKindOfClass:[NSNull class]])
            {
                cell.thevalue.text   = @""  ;
                
            }
            else
            {
                cell.thevalue.text=value;
            }
            
            
            
            
        }
        
        
      
        
    
               return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (void)dealloc {
   
    [super dealloc];
}
@end
