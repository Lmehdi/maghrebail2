//
//  DetailsBienCommandeViewController.m
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "DetailsBienCommandeViewController.h"

@interface DetailsBienCommandeViewController ()

@end

@implementation DetailsBienCommandeViewController
@synthesize dictionnary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
   // [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.tableView.layer.borderWidth = 2.0;
  //  self.tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1px"]];

  //  NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numcommande,@"num", nil];
        /*
     if (dataManager.isDemo)
     {
     params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
     }
     */
    [self.response removeAllObjects];
    [_tableView reloadData];
    
    if(dataManager.isdetailscommande)
    {
          self.titreheadertable.text=@"DETAILS BIEN";
        if(dataManager.isDemo)
        {
		

              [self getRemoteContentAuth:FICHE_COMMANDE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
            
            
    
        }
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/fichebien/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation]];
          
        }


     
    }
    
    if(dataManager.isdatafacture)
    {
        if(dataManager.isDemo)
        {
        
           // [self getRemoteContentAuth:DETAILS_FACTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
             [self getRemoteContentAuth:DETAILS_FACTURE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];

        }
        else
        {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_facture/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.idmvt,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
    
    
    
    }
    if(dataManager.isdetailscontact)
    {
    
    self.titreheadertable.text=@"DETAILS CONTACT";
    
    }
    if(dataManager.isdetailsreglement)
    {
        self.titreheadertable.text=@"DETAILS REGLEMENT";

        if(dataManager.isDemo)
        {
		//[self getRemoteContent:@"https://www.maghrebail.ma"];
            
         [self getRemoteContentAuth:DETAILS_REGLEMENT_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        
        }
        else
        {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_reglement/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.idmvt,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
    
        

    }
    if(dataManager.isdetailsimmatriculation)
    {
        
        self.titreheadertable.text=@"DETAILS IMMATRICULATIONS";
        if(dataManager.isDemo)
        {
		
        
            // [self getRemoteContentAuth:REGLEMENT_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
             [self getRemoteContentAuth:FICHE_IMMATRICULATION_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
            
            
        }
        else
        {
             [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_immatriculation/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
        }
        
      
        
       
    
     
        

    }
    if(dataManager.isdetailsengagement)
    {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/fichebien/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation]];
        self.titreheadertable.text=@"DETAILS BIEN";
    

    
    }
    
    
    
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView setDelegate:nil];
    [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
    /*
     if ([[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum])
     {
     if ([[[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum] isEqualToString:@"FACTURE"])
     self.btnFav.selected = YES;
     else
     self.btnFav.selected = NO;
     }
     else
     self.btnFav.selected = NO;
     */
}
-(void) initHeaderColor{
    
    
    if(dataManager.isdetailscommande)
    {
      //  self.titreheadertable.text=@"DETAILS BIEN";
       headerView.labelheaderTitre.text = [NSString stringWithFormat:@"Détails BIEN N° %@", self.immobilisation];

        
        
    
    }
    
    if(dataManager.isdatafacture)
    {
        headerView.labelheaderTitre.text = [NSString stringWithFormat:@"Détails FACTURE N° %@", self.idmvt];
     
    }
    
    if(dataManager.isdetailsreglement)
    {
    //    self.titreheadertable.text=@"DETAILS REGLEMENT";
       headerView.labelheaderTitre.text = [NSString stringWithFormat:@"Détails REGLEMENT N° %@", self.idmvt];


    }
    if(dataManager.isdetailsimmatriculation)
    {
    
    
        headerView.labelheaderTitre.text = [NSString stringWithFormat:@"Détails IMMATRICULATION N° %@", self.immobilisation];

    
    }
    if(dataManager.isdetailsengagement)
    {
    
        headerView.labelheaderTitre.text = [NSString stringWithFormat:@"Détails ENGAGEMENT N° %@", self.immobilisation];

    
    
    }
    if(dataManager.isdetailscontact)
    {
    
        headerView.labelheaderTitre.text = @"Détails contact";

    
    }
    
    
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView setDelegate:nil];
    [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
}

- (void)goBack
{
    if (!isLandscape || IS_IPAD)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
    [headerView setDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateView{
    [super updateView];
	
//    if(dataManager.isdetailsimmatriculation) {
//        NSString *path = path = [[NSBundle mainBundle] pathForResource:@"DetailBienImmatriculationDemo" ofType:@"json"];
//        NSData *data = [NSData dataWithContentsOfFile:path];
//        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//        self.response = dict;
//    }
//    else if(dataManager.isdetailscommande) {
//        NSString *path = path = [[NSBundle mainBundle] pathForResource:@"FicheBienCommandeDemo" ofType:@"json"];
//        NSData *data = [NSData dataWithContentsOfFile:path];
//        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//        self.response = dict;
//    }
//    else {
//        NSString *path = path = [[NSBundle mainBundle] pathForResource:@"DetailsReglement" ofType:@"json"];
//        NSData *data = [NSData dataWithContentsOfFile:path];
//        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//        self.response = dict;
//    }
	
    [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(!dataManager.isdetailscontact)
    {
       
            
        if(dataManager.isdetailscommande || dataManager.isdetailsengagement)
        {
            if([[self.response objectForKey:@"response"] isKindOfClass:[NSDictionary class]])
            {
                return 10;
            }
            
            
          else if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
            {
                return 10;
                
            }
            
            else
            {
                return 0;
            }
            
            
        
        }
    
        else if(dataManager.isdetailsimmatriculation)
        {
            if([[self.response objectForKey:@"response"] isKindOfClass:[NSDictionary class]])

            {
            return 12;
            
            }
           else if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
            {
                return 12;
    
            }
            else
            {
                return 0;
            }
            

            
        }
    
        else if(dataManager.isdatafacture)
        {
            if([[self.response objectForKey:@"response"] isKindOfClass:[NSDictionary class]])
                
            {
                return 14;
                
            }
            if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
            {
                return 14;
                
            }
            else
            {
                return 0;
            }
        
        
        
        }
            
        else
        {
            if([[self.response objectForKey:@"response"] isKindOfClass:[NSDictionary class]])
            {
            
            return [[[self.response objectForKey:@"response"] allKeys] count];
            }
            
           else if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
            {
                return [[[[self.response objectForKey:@"response"]objectAtIndex:0] allKeys] count]-2;
                
                
            }
            else
            {
                return 0;
            }
            
            
            
        }
    }
            

   
   else
    {
    return [[self.dictionnary allKeys] count];
    
    }
    
    
    
    

}

#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
   
    if(!dataManager.isdetailscontact){
     [super reloadTableViewDataSource];
 
    
       /* if (dataManager.isDemo)
     {
     params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
     }
     */
    if(dataManager.isdetailscommande)
    {
        if(dataManager.isDemo)
            
        {
            //[self getRemoteContent:@"https://www.maghrebail.ma"];
            
             [self getRemoteContentAuth:FICHE_COMMANDE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
            
        }
        else
        {
               [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/fichebien/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation]];
        }
    
    }
    if(dataManager.isdetailsreglement)
    {
        if(dataManager.isDemo)
        {
		
		   // [self getRemoteContent:@"https://www.maghrebail.ma"];
            
         [self getRemoteContentAuth:DETAILS_REGLEMENT_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        }
        else
        {
           [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_reglement/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.idmvt,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
        
      
    
    
    }
    
    if(dataManager.isdatafacture)
    {
        if(dataManager.isDemo)
        {
            
            //[self getRemoteContentAuth:DETAILS_FACTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
            [self getRemoteContentAuth:DETAILS_FACTURE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        }
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_facture/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.idmvt,[dataManager.authentificationData objectForKey:@"EMAIL"]]];

        }
        
        
        
    }
    

    
    if(dataManager.isdetailsimmatriculation)
    {
        if(dataManager.isDemo)
        {
		
            //[self getRemoteContent:@"http://www.maghrebail.ma"] ;
             [self getRemoteContentAuth:FICHE_IMMATRICULATION_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        }
        else
        {
               [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_immatriculation/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
        }
        

       
    
    
    }
    
    if(dataManager.isdetailsengagement)
    {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/fichebien/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.immobilisation]];
      
    }
   
    } else
    {
       	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.0];

       
    
    }
    
 
}


-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
        
        [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
        [headerView setDelegate:nil];
        [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
        
       
    }
    if(dataManager.isdetailscommande)
    {
        if(isLandscape)
        {

            self.titreheadertable.text=@"DETAILS BIEN";
            
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"Détails BIEN N° %@", self.immobilisation];
            self.titreheadertable.text=@"DETAILS BIEN";
            
        }
        
        
    }
    
    if(dataManager.isdatafacture)
    {
        if(isLandscape)
        {
            
            self.titreheadertable.text=@"DETAILS FACTURE";
            
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"Détails FACTURE  N° %@", self.immobilisation];
            self.titreheadertable.text=@"DETAILS FACTURE";
            
        }
        
        
    }
    
    
    
    if(dataManager.isdetailscontact)
    {
        if(isLandscape)
        {
            
            self.titreheadertable.text=@"DETAILS CONTACT";
            
        }
        else
        {
            self.labelTitle.text = @"Détails Contact";
            self.titreheadertable.text=@"DETAILS CONTACT";
            
        }
        
    
    
    
    }
    
    if(dataManager.isdetailsimmatriculation)
    {
        if(isLandscape)
        {
            
            self.titreheadertable.text=@"DETAILS IMMATRICULATION";
            
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"Détails IMMATRICULATION N° %@", self.immobilisation];
            self.titreheadertable.text=@"DETAILS IMMATRICULATION";
            
        }
        
        
    }
    
    
    if(dataManager.isdetailsreglement)
    {
        if(isLandscape)
        {
            
            self.titreheadertable.text=@"DETAILS REGLEMENT";
            
        }
        else
        {
            
            self.labelTitle.text = [NSString stringWithFormat:@"Détails REGLEMENT N° %@", self.idmvt];

            self.titreheadertable.text=@"DETAILS REGLEMENT";
     }
        
    
    }
    if(dataManager.isdetailsengagement)
    {
        if(isLandscape)
        {
            
            self.titreheadertable.text=@"DETAILS ENGAGEMENT";
            
        }
        else
        {
            
            self.labelTitle.text = [NSString stringWithFormat:@"Détails ENGAGEMENT N° %@", self.idmvt];
            
            self.titreheadertable.text=@"DETAILS ENGAGEMENT";
            
            
            
        }
        
        
    }
  
   _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
    
    
    
    
}

- (void)dealloc {
    [_labelTitle release];

       [_titreheadertable release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelTitle:nil];

    [super viewDidUnload];
}
/*
 - (IBAction)handlFav:(id)sender {
 
 NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
 if (self.btnFav.selected)
 {
 self.btnFav.selected = NO;
 [standardUserDefaults removeObjectForKey:self.contratNum];
 }
 else
 {
 self.btnFav.selected = YES;
 [standardUserDefaults setObject:@"FACTURE" forKey:self.contratNum];
 }
 [standardUserDefaults synchronize];
 
 }
 */




@end
