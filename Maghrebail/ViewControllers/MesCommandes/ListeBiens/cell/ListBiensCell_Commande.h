//
//  ListBiensCell_Commande.h
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListBiensCell_Commande : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *numbien;
@property (retain, nonatomic) IBOutlet UILabel *designation;
@property (retain, nonatomic) IBOutlet UILabel *typeligne;
@property (retain, nonatomic) IBOutlet UILabel *montantTTC;
@property (retain, nonatomic) IBOutlet UILabel *montantHT;
@property (retain, nonatomic) IBOutlet UIImageView *flecheimage;
@property (retain, nonatomic) IBOutlet UILabel *tva;

@end
