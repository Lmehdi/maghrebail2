//
//  ListBiensCell_Commande.m
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "ListBiensCell_Commande.h"

@implementation ListBiensCell_Commande


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)dealloc {
    [_numbien release];
    [_typeligne release];
    [_designation release];
    [_montantTTC release];
    [_montantHT release];
    [_flecheimage release];
    [_tva release];
   [super dealloc];
}


@end
