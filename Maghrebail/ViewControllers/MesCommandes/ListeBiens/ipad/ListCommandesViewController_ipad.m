//
//  ListCommandesViewController_ipad.m
//  Maghrebail
//
//  Created by Raoui Mouad on 16/02/2017.
//  Copyright © 2017 Mobiblanc. All rights reserved.
//

#import "ListCommandesViewController_ipad.h"
#import "ListBiensCell_Commande.h"
#import "DetailsBienCommandeViewController_ipadViewController.h"
@interface ListCommandesViewController_ipad ()

@end

@implementation ListCommandesViewController_ipad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     static NSString *cellIdentifier_l;
    cellIdentifier_l = @"ListBiensCell_Commande_ipad";
    
    ListBiensCell_Commande *cell = ( ListBiensCell_Commande *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
    if (cell == nil)
    {
        cell = ( ListBiensCell_Commande *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
    }
    cell.numbien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"IMMOBILISATION"] ;
    
    cell.designation.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"] ;
    
    
    
    cell.typeligne.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TYPE_LIGNE"];
    
    
    cell.montantTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
    cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
    cell.tva.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TVA"];
    
    
    cell.alpha = 0.5;
    
    switch (indexPath.row % 2) {
        case 0:
            cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
            
            break;
        case 1:
            cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    
    return cell;
    
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.isFromButton = NO;
    DetailsBienCommandeViewController_ipadViewController *  detailsBienCommandeViewController_iphone  = [[DetailsBienCommandeViewController_ipadViewController alloc]initWithNibName:@"DetailsBienCommandeViewController_ipadViewController" bundle:nil];
    if(dataManager.isdatacommande)
        
    {   dataManager.isdetailscommande=YES;
        dataManager.isdetailsreglement=NO;
        dataManager.isdetailsimmatriculation=NO;
        dataManager.isdetailsengagement=NO;
        dataManager.isdetailscontact=NO;
        dataManager.isdatafacture=NO;
        
    }
    
    if(dataManager.isdataimmatriculation)
    {
        dataManager.isdetailsimmatriculation=YES;
        dataManager.isdetailscommande=NO;
        dataManager.isdetailsreglement=NO;
        dataManager.isdetailsengagement=NO;
        dataManager.isdetailscontact=NO;
        dataManager.isdatafacture=NO;
        
        
    }
    if(dataManager.isdataengagement)
    {
        dataManager.isdetailsengagement=YES;
        dataManager.isdetailscommande=NO;
        dataManager.isdetailsreglement=NO;
        dataManager.isdetailsimmatriculation=NO;
        dataManager.isdetailscontact=NO;
        dataManager.isdatafacture=NO;
        
        
    }
    
    
    
    detailsBienCommandeViewController_iphone.immobilisation=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"IMMOBILISATION"];
    
    /*     if (!self.showFav)
     listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
     else
     listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
    
    [self.navigationController pushViewController:detailsBienCommandeViewController_iphone animated:YES];
    
}
@end
