//
//  ListCommandesViewController.h
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface ListCommandesViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property(nonatomic, retain)NSString * numcommande;
@property(nonatomic, retain)NSString * contratNumMVT;
@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property(nonatomic, retain)NSString * email;

-(void) initHeaderColor;
- (IBAction)handlFav:(id)sender;


@end
