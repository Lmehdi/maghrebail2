//
//  ListCommandesViewController.m
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "ListCommandesViewController.h"

@interface ListCommandesViewController ()

@end

@implementation ListCommandesViewController

@synthesize contratNumMVT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    [self.response removeAllObjects];
    [_tableView reloadData];
    if(dataManager.isdatacommande)
    {
        self.labelTitle.text=@"BIENS  COMMANDE N°";
        if(dataManager.isDemo)
        {
            //[self getRemoteContent:@"http://www.maghrebail.ma"];
            
            [self getRemoteContentAuth:DETAILS_COMMANDE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.numcommande];
            
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_affaire/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
  
    
    }
     if(dataManager.isdataimmatriculation)
    {
    
        self.labelTitle.text=@"DETAILS AFFAIRE N°";
        
        if(dataManager.isDemo)
        {
           
            
            [self getRemoteContentAuth:DETAILS_IMMATRICULATION_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
            
        }
        else
        {
            self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.numcommande];
            
            
            
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_immat/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
        }
      
    
    }
    
    if(dataManager.isdataengagement)
    {
        
        self.labelTitle.text=@"DETAILS AFFAIRE N°";
        
        self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.numcommande];
	
	
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_affaire/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
    }
    
   
/* //   NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numcommande,@"num", nil];
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
 */
    
    
    
    
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView setDelegate:nil];
    [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
   /*
    if ([[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum])
    {
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:self.contratNum] isEqualToString:@"FACTURE"])
            self.btnFav.selected = YES;
        else
            self.btnFav.selected = NO;
    }
    else
        self.btnFav.selected = NO;
    */
}
-(void) initHeaderColor{
  
    headerView.labelheaderTitre.text=self.labelTitle.text;
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView setDelegate:nil];
    [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
}

- (void)goBack
{
    if (!isLandscape || IS_IPAD)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"ipad-v3-menu.png"] forState:UIControlStateNormal];
    [headerView setDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateView{
    
    [super updateView];
	

    
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    
    
    
    if ([status isEqualToString:@"OK"])
    {
      [_tableView reloadData];
        
    }
 //   [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        return [[self.response objectForKey:@"response"] count];
        
    }
    else
    {
        return 0;
    }

}

#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numcommande,@"num", nil];
   /* if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    */
    if(dataManager.isdatacommande)
    {
        if(dataManager.isDemo)
        {
            //[self getRemoteContent:@"http://www.maghrebail.ma"];
            
             [self getRemoteContentAuth:DETAILS_COMMANDE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
            
        }
        else
        {
         [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_affaire/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }
       

    
    }
    if(dataManager.isdataimmatriculation)
    {
    
        if(dataManager.isDemo)
        {
            //[self getRemoteContent:@"http://www.maghrebail.ma"];
            
            [self getRemoteContentAuth:DETAILS_IMMATRICULATION_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
            
        }
        
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_immat/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        }

    
    }
    
    if(dataManager.isdataengagement)
    {
        
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Details_affaire/%@/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],self.numcommande,[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
    }
    
    
     //  [self postDataToServer:URL_MES_FACTURES_CONTRACT_LIST andParam:params];
}


-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
      
        if(dataManager.isdatacommande)
        {
           
            self.labelTitle.text = [NSString stringWithFormat:@"BIENS COMMANDE N° %@", self.numcommande];
         
        
        }
        if(dataManager.isdataimmatriculation || dataManager.isdataengagement)
        {
        
            self.labelTitle.text = [NSString stringWithFormat:@"DETAILS AFFAIRE N° %@", self.numcommande];

        
        }
        
        [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
        [headerView setDelegate:nil];
        [headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
        
      //  self.labelTitle.text = [NSString stringWithFormat:@"%@ %@", self.labelTitle.text, self.numcommande];
    }
    
    
    
    
    
    
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
}

- (void)dealloc {
    [_labelTitle release];
    [_btnFav release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLabelTitle:nil];
    [self setBtnFav:nil];
    [super viewDidUnload];
}
/*
- (IBAction)handlFav:(id)sender {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (self.btnFav.selected)
    {
        self.btnFav.selected = NO;
        [standardUserDefaults removeObjectForKey:self.contratNum];
    }
    else
    { 
        self.btnFav.selected = YES;
        [standardUserDefaults setObject:@"FACTURE" forKey:self.contratNum];
    }
    [standardUserDefaults synchronize];
    
}
 */


@end
