//
//  ListCommandesViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 14/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "ListCommandesViewController_iphone.h"


#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)
@interface ListCommandesViewController_iphone ()

@end

@implementation ListCommandesViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    
    if(IS_IOS7orHIGHER){
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
    
    if(dataManager.isdetailsimmatriculation){
        [Flurry logEvent:@"DetailsImmatriculation"];
        
    }else{
         [Flurry logEvent:@"DetailsMesCommandes"];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isLandscape) {
    //     float celldesignationheight=[self getCellHeightForTextLandscape:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"]];
        
        
        static NSString *cellIdentifier_l;
        
   /*     if ([DataManager isIphone5])
        {
            cellIdentifier_l = @"ListBiensCell_Commande_iphone5_l";
        }
       
        else
            cellIdentifier_l = @"ListBiensCell_Commande_iphone_l";
    */
        
        cellIdentifier_l = @"ListBiensCell_Commande_iphone_l";

        ListBiensCell_Commande *cell = ( ListBiensCell_Commande *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = ( ListBiensCell_Commande *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        cell.numbien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"IMMOBILISATION"] ;
        
        cell.designation.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"] ;
       
        
        
      cell.typeligne.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TYPE_LIGNE"];
   
        
        cell.montantTTC.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
        cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_TTC"]];
        cell.tva.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TVA"];

        
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }else{
        
        static NSString *cellIdentifier_p = @"ListBiensCell_Commande_iphone_p";
   
        ListBiensCell_Commande *cell = (ListBiensCell_Commande *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (ListBiensCell_Commande *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        cell.alpha = 0.5;
        
        cell.numbien.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"IMMOBILISATION"] ;
        
        cell.designation.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"] ; 
    
         cell.typeligne.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TYPE_LIGNE"];
   
        cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MT_HT"]];
   
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isLandscape)
    {
        /*   if (dataManager.isDemo)
         
         else
         [Flurry logEvent:@"Home/EspaceAbonnés/mesfactures/listefactures"];*/
        self.isFromButton = NO;
          DetailsBienCommandeViewController_iphone *  detailsBienCommandeViewController_iphone  = [[DetailsBienCommandeViewController_iphone alloc]initWithNibName:@"DetailsBienCommandeViewController_iphone" bundle:nil];
        if(dataManager.isdatacommande)
            
        {   dataManager.isdetailscommande=YES;
            dataManager.isdetailsreglement=NO;
            dataManager.isdetailsimmatriculation=NO;
            dataManager.isdetailsengagement=NO;
            dataManager.isdetailscontact=NO;
            dataManager.isdatafacture=NO;
            
        }
       
         if(dataManager.isdataimmatriculation)
         {
        dataManager.isdetailsimmatriculation=YES;
         dataManager.isdetailscommande=NO;
            dataManager.isdetailsreglement=NO;
            dataManager.isdetailsengagement=NO;
             dataManager.isdetailscontact=NO;
             dataManager.isdatafacture=NO;


        }
        if(dataManager.isdataengagement)
        {
            dataManager.isdetailsengagement=YES;
            dataManager.isdetailscommande=NO;
            dataManager.isdetailsreglement=NO;
            dataManager.isdetailsimmatriculation=NO;
            dataManager.isdetailscontact=NO;
            dataManager.isdatafacture=NO;


        }
        
        
      
        detailsBienCommandeViewController_iphone.immobilisation=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"IMMOBILISATION"];

        /*     if (!self.showFav)
         listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
         else
         listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
        
        [self.navigationController pushViewController:detailsBienCommandeViewController_iphone animated:YES];
    }
    
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if(isLandscape)
//    {
//        return [self getCellHeightForTextLandscape:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"]];
//    }
//    else
//        return [self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DÉSIGNATION_BIEN"]];
    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
