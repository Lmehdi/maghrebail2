 //
//  MesCommandesViewController.m
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesCommandesViewController.h"

@interface MesCommandesViewController ()

@end
CGRect  odlFrame;
@implementation MesCommandesViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
     
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    odlFrame=self.view.frame;
   [Flurry logEvent:@"MesCommandes"];


    [self initHeaderColor];
}


- (void)dealloc
{
   
    [_Id_affaire release];
    [_No_commande release];
    [_Date_commande release];
    [_mt_tva_commande release];
    [_Mt_ht_commande release];
 
    
    [_btnaffaireencours release];
    [_btnaffairesemises release];
    [_btnaffairesvalidees release];
    [_interlocuteur release];
    [_statusaffaire release];
    [super dealloc];
}

- (IBAction)encoursPushed:(UIButton *)sender
{
    self.statusaffaire.text=@"Statut Affaire";
    btnSelectedEncours = YES;
    btnSelectedRegles = NO;
    btnSelectedClotures=NO;
    self.buttonEncours.selected = YES;
    self.buttonRegles.selected = NO;
    self.buttonClotures.selected=NO;
   
    if(dataManager.isDemo)
    {
	
         [self getRemoteContentAuth:COMMANDE_ENCOURS_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        
        
    }
    else
    {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_encours/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
        
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;
}

- (IBAction)CloturesPushed:(UIButton *)sender
{
    
    btnSelectedEncours = NO;
    btnSelectedRegles = NO;
    btnSelectedClotures=YES;
    self.buttonEncours.selected = NO;
    self.buttonRegles.selected = NO;
    self.buttonClotures.selected=YES;
   self.statusaffaire.text=@"Date Création Affaire";
    if(dataManager.isDemo)
    {
        
        //[self getRemoteContentAuth:COMMANDE_CLOTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
        [self getRemoteContentAuth:COMMANDE_CLOTURE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
    }
    else
    {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_validees/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
    }
    

    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;


}

- (IBAction)reglesPushed:(UIButton *)sender
{
    btnSelectedEncours = NO;
    btnSelectedRegles = YES;
    btnSelectedClotures=NO;
    self.buttonEncours.selected = NO;
    self.buttonRegles.selected = YES;
    self.buttonClotures.selected=NO;

    self.statusaffaire.text=@"Date Création Affaire";
    if(dataManager.isDemo)
    {
        //[self getRemoteContentAuth:COMMANDE_REGLE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
	
	    [self getRemoteContentAuth:COMMANDE_REGLE_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        
        
    }
    else
    {
        [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_emises/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]]];
    }
    
    self.imgViewScrollMontnat.frame = frameMontImgMinInitial;
    self.imgViewScrollDate.frame=frameDateImgMinInitial;
    
    self.btnMinMontant.frame=frameMontBtnMinInitial;
    self.labelMinMontant.frame=frameMontLblMinInitial;
    self.viewMinMontant.frame=frameViewMontMinInitial;
    
    self.btnMaxMontant.frame=frameMontBtnMaxInitial;
    self.labelMaxMontant.frame=frameMontLblMaxInitial;
    self.viewMaxMontant.frame=frameViewMontMaxInitial;
    
    self.buttonMinDate.frame=frameDateBtnMinInitial;
    self.labelMinDate.frame=frameDateLblMinInitial;
    self.viewMinDate.frame=frameViewDateMinInitial;
    
    self.buttonMaxDate.frame=frameDateBtnMaxInitial;
    self.labelMaxDate.frame=frameDateLblMaxInitial;
    self.viewMaxDate.frame=frameViewDateMaxInitial;

}





-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES COMMANDES";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initHeaderColor];
    
    
 
    
    if (self.isFromButton || dataManager.isLoad)
    {
        [self.response removeAllObjects];
         [_tableView reloadData];
        
        
        btnSelectedEncours = YES;
        btnSelectedRegles = NO;
        btnSelectedClotures=NO;
        self.buttonEncours.selected = YES;
        self.buttonRegles.selected = NO;
        self.buttonClotures.selected=NO;
        isFiltreViewShowed = NO;
        dataManager.isLoad = NO;
        if(dataManager.isDemo)
        {
        
           //[self getRemoteContentAuth:COMMANDE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f"];
		
			[self getRemoteContentAuth:COMMANDE_ENCOURS_DEMO username:@"maghrebail@gmail.com" password:@"bf9d290571cdeff92f73ad2d2aad6c96cd997885"];
        
        }
        else
        {
            NSString *email=[dataManager.authentificationData objectForKey:@"EMAIL"];
            NSString *password=[dataManager.authentificationData objectForKey:@"PASSWORD"];
            NSString * emailencoded = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
            
            NSString *url=[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_encours/%@",emailencoded,password,email];
            [self getRemoteContentfournisseur:url];
        }
        
       
        
      
        
    //    self.btnaffaireencours.selected=YES;
    
    }
        
    
    if (dataManager.authentificationData)
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (headerView)
            [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
    }
}

-(void)updateView{
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];

    [super updateView];
	
//            if ([dataManager isDemo]) {
//                NSString *path ;
//                if (self.buttonClotures.selected) {
//                    path = [[NSBundle mainBundle] pathForResource:@"mesCommandeClotures" ofType:@"json"];
//                }else if(self.buttonRegles.selected){
//                    path = [[NSBundle mainBundle] pathForResource:@"mesCommandeEnRegles" ofType:@"json"];
//                }else {
//                    path = [[NSBundle mainBundle] pathForResource:@"mesCommandeEnCours" ofType:@"json"];
//                }
//
//                NSData *data = [NSData dataWithContentsOfFile:path];
//                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//                self.response = dict;
//            }
	
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
        NSArray *reponse = [self.response objectForKey:@"response"];
        [dataManager.commandes removeAllObjects];
        dataManager.commandes = [NSMutableArray arrayWithArray:reponse];
        dataManager.commandeFiltred = (NSMutableArray *)[dataManager.commandes copy];
        
        if (dataManager.commandes.count != 0)
        {
            //remplir valuesMontant
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"MT_HT_CMDE" ascending:YES comparator:^(id obj1, id obj2) {
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@" " withString:@""];
                obj1 = [obj1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                obj2 = [obj2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
                if ([obj1 doubleValue] > [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1 doubleValue] < [obj2 doubleValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];
            NSArray *sortedData;
            sortedData = [[dataManager.commandes sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]] copy];
            
            NSString *str1 = [[[sortedData lastObject] objectForKey:@"MT_HT_CMDE"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *str2 = [[[sortedData objectAtIndex:0] objectForKey:@"MT_HT_CMDE"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            str1 = [str1 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            str2 = [str2 stringByReplacingOccurrencesOfString:@"," withString:@"."];
            
            maxMontantValue = [str1 doubleValue];
            minMontantValue = [str2  doubleValue];
            self.labelMinMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", minMontantValue]];
            self.labelMaxMontant.text = [DataManager getFormatedNumero2:[NSString stringWithFormat:@"%d", maxMontantValue]];
            int diff = (maxMontantValue - minMontantValue)/100;
            valuesMontant = [[NSMutableArray alloc] init];
            for (int i = minMontantValue; i <= maxMontantValue; i = i + diff)
            {
                [valuesMontant addObject:[NSString stringWithFormat:@"%d", i]];
                if (diff == 0)
                    break;
            }
            
            //[dataManager.ligneCredit removeAllObjects];
            dataManager.commandes.array = sortedData;
            //remplir valuesDates
            NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"DATE_COMMANDE" ascending:YES comparator:^(id obj1, id obj2)
                                                {
                                                    return (NSComparisonResult)[[DataManager stringToDtae:obj1] compare:[DataManager stringToDtae:obj2]];
                                                }];
            NSArray *sortedDataWithDate = [[dataManager.commandes sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptorDate,nil]] copy];
            maxDateValue = [[DataManager stringToDtae:[[sortedDataWithDate lastObject] objectForKey:@"DATE_COMMANDE"]] timeIntervalSince1970];
            minDatetValue = [[DataManager stringToDtae:[[sortedDataWithDate objectAtIndex:0] objectForKey:@"DATE_COMMANDE"]] timeIntervalSince1970];
            self.labelMinDate.text = [DataManager timeIntervaleToString:minDatetValue];
            self.labelMaxDate.text = [DataManager timeIntervaleToString:maxDateValue];
            int diffDate = (maxDateValue - minDatetValue)/100;
            valuesDates = [[NSMutableArray alloc] init];
            for (int i = minDatetValue; i <= maxDateValue; i = i + diffDate)
            {
                [valuesDates addObject:[NSString stringWithFormat:@"%d", i]];
                if (diffDate == 0) {
                    break;
                }
            }
            
            if (dataManager.commandes.count == 1)
            {
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            else
            {
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                
                [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
                [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
            }
            
            self.strMinDate = [NSString stringWithFormat:@"%@", self.labelMinDate.text];
            self.strMinMont = [NSString stringWithFormat:@"%@", self.labelMinMontant.text];
            self.strMaxDate = [NSString stringWithFormat:@"%@", self.labelMaxDate.text];
            self.strMaxMont = [NSString stringWithFormat:@"%@", self.labelMaxMontant.text];
        }
        
        
    }
    /* else
     {
     NSString *message = [header objectForKey:@"message"];
     [BaseViewController showAlert:@"Info" message:message delegate:nil confirmBtn:@"OK" cancelBtn:nil];
     }*/
    
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([[self.response objectForKey:@"response"] isKindOfClass:[NSArray class]])
    {
        return [dataManager.commandeFiltred  count];
    }
    else
    {
        return 0;
    }
}

#pragma mark --
#pragma mark Pull to refresh

-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
    if (dataManager.isDemo)
    {
        params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
    }
    
    if(self.buttonEncours.selected==YES)
    {
        if(dataManager.isDemo)
        {
            [self getRemoteContentAuth:COMMANDE_ENCOURS_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:11];
        
        }
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_encours/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]] andTag:11];
        }

    
    
    }
    if(self.buttonRegles.selected==YES)
    {
        if(dataManager.isDemo)
        {
            [self getRemoteContentAuth:COMMANDE_REGLE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:12];
        
        }
        else
        {
            [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_emises/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]] andTag:12];
        
        }

    
    
    }
    if(self.buttonClotures.selected==YES)
    {
        if(dataManager.isDemo)
        {
            [self getRemoteContentAuth:COMMANDE_CLOTURE_DEMO username:@"mobiblanc@mobiblanc.ma" password:@"eac1cdd60eda56af4300224ccff966dd4b75992f" andTag:13];
        
        }
        else
        {
          [self getRemoteContentfournisseur:[NSString stringWithFormat:@"https://%@:%@@www.maghrebail.ma/leasebox/partenaires/api/rest/Affaires_validees/%@",[[dataManager.authentificationData objectForKey:@"EMAIL"] stringByReplacingOccurrencesOfString:@"@" withString:@"%40"],[dataManager.authentificationData objectForKey:@"PASSWORD"],[dataManager.authentificationData objectForKey:@"EMAIL"]] andTag:13];
        }
    
    
    
    }

    
 
}


-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    if (!isLandscape)
    {
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        if (dataManager.authentificationData)
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
        }
        else
        {
            if (headerView)
                [headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
        }
    }
    
    [_tableView reloadData];
    
    if (dataManager.commandes.count == 1)
    {
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate removeTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate removeTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    else
    {
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMaxMontant addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMaxDate addTarget:self action:@selector(maxScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        
        [self.btnMinMontant addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.btnMinMontant addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [self.buttonMinDate addTarget:self action:@selector(minScrollDraggedEnd:withEvent:) forControlEvents:UIControlEventTouchDragExit|UIControlEventTouchUpInside];
    }
    
    if (!isLandscape)
    {
        [self filtrePushed:nil];
    }
    if (!isLandscape)
    {
        [super filtrePushed:nil];
        if (btnSelectedEncours)
        {
            
            self.statusaffaire.text=@"Statut Affaire";
            self.buttonEncours.selected = YES;
            self.buttonRegles.selected = NO;
            self.buttonClotures.selected=NO;
        }
        else if (btnSelectedRegles)
            {
                self.statusaffaire.text=@"Date Création Affaire";
                self.buttonEncours.selected = NO;
                self.buttonRegles.selected = YES;
                self.buttonClotures.selected=NO;
            }
        else
        {
            if(btnSelectedClotures)
            {
                self.statusaffaire.text=@"Date Création Affaire";
                self.buttonEncours.selected = NO;
                self.buttonRegles.selected = NO;
                self.buttonClotures.selected=YES;
                
            
            }
        
        
        }
        self.labelMinDate.text  = [NSString stringWithFormat:@"%@", self.strMinDate];
        self.labelMinMontant.text = [NSString stringWithFormat:@"%@", self.strMinMont];
        self.labelMaxMontant.text = [NSString stringWithFormat:@"%@", self.strMaxMont];
        self.labelMaxDate.text = [NSString stringWithFormat:@"%@", self.strMaxDate];
    }else{
        
        if (btnSelectedEncours)
        {
            
            self.statusaffaire.text=@"Statut Affaire";
     
        }
        else if (btnSelectedRegles)
        {
            self.statusaffaire.text=@"Date Création Affaire";
         
        }
        else
        {
            if(btnSelectedClotures)
            {
                self.statusaffaire.text=@"Date Création Affaire";
             
                
                
            }
            
            
        }
        
        
    }
    
    
    self.imgViewScrollMontnat.frame = frameMontImgMin;
    self.imgViewScrollDate.frame=frameDateImgMin;
    
    self.btnMinMontant.frame=frameMontBtnMin;
    self.labelMinMontant.frame=frameMontLblMin;
    self.viewMinMontant.frame=frameViewMontMin;
    
    self.btnMaxMontant.frame=frameMontBtnMax;
    self.labelMaxMontant.frame=frameMontLblMax;
    self.viewMaxMontant.frame=frameViewMontMax;
    
    self.buttonMinDate.frame=frameDateBtnMin;
    self.labelMinDate.frame=frameDateLblMin;
    self.viewMinDate.frame=frameViewDateMin;
    
    self.buttonMaxDate.frame=frameDateBtnMax;
    self.labelMaxDate.frame=frameDateLblMax;
    self.viewMaxDate.frame=frameViewDateMax;
    
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
    
    _refreshHeaderView.delegate = self;
    [_tableView addSubview:_refreshHeaderView];
    [_refreshHeaderView refreshLastUpdatedDate];

    
    }
    
- (IBAction)filtrePushed:(UIButton *)sender
{
    [super filtrePushed:sender];
   
    if (isFiltreViewShowed)
    {
        if (btnSelectedEncours)
        {
            self.buttonEncours.selected = YES;
            self.buttonRegles.selected = NO;
            self.buttonClotures.selected=NO;
      
        }
        if (btnSelectedRegles)
        {
            self.buttonEncours.selected = NO;
            self.buttonRegles.selected = YES;
            self.buttonClotures.selected=NO;
            
        }
        else
        {
            if (btnSelectedClotures)
            {
                self.buttonEncours.selected = NO;
                self.buttonRegles.selected = NO;
                self.buttonClotures.selected = YES;

            }
        }
    } 
}




- (void)viewDidUnload {
   
    [super viewDidUnload];
}
@end
