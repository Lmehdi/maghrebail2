//
//  MesCommandesViewController_iphone.m
//  Maghrebail
//
//  Created by Belkheir on 10/11/2016.
//  Copyright © 2016 Mobiblanc. All rights reserved.
//

#import "MesCommandesViewController_iphone.h"


#define IS_IOS7orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.1)

@interface MesCommandesViewController_iphone ()

@end
CGRect oldFrameT;
@implementation MesCommandesViewController_iphone


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
    oldFrameT=self.view.frame;
    
    

    if (IS_IOS7orHIGHER) {
        self.tableView.estimatedRowHeight=58.0;
        self.tableView.rowHeight=UITableViewAutomaticDimension;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    
    [super orientationChanged:note];
   
    if (!isLandscape)
    {
        
        self.view.frame=oldFrameT;
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (IS_IOS7orHIGHER){
        return UITableViewAutomaticDimension;
    }
    return 58.0;
}




#pragma mark --
#pragma mark -- TableView




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    

    
    if (isLandscape) {
        

        static NSString *cellIdentifier_l;
     /*
        if ([DataManager isIphone5LandScape])
        {
            cellIdentifier_l = @"CommandeAffaireCell_iphone5_l";
        }
        else if  ([DataManager isIphone6LandScape])
        {
            cellIdentifier_l = @"CommandeAffaireCell_iphone6_l";
        }
      
        else
            
            cellIdentifier_l = @"CommandeAffaireCell_iphone_l";
      */
        cellIdentifier_l = @"CommandeAffaireCell_iphone_l";

        
        CommandeAffaireCell *cell = (CommandeAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (CommandeAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
        
        NSMutableDictionary* currentItem   =   [[self.response objectForKey:@"response"]objectAtIndex:indexPath.row];
        
        
        /*
        CGRect oldFrame = cell.nomClient.frame;
        [cell.nomClient sizeToFit];
        oldFrame.size.height    =   cell.nomClient.frame.size.height;
        [cell.nomClient setFrame:oldFrame];
        */


          // cell.nocommande.frame=CGRectMake(cell.nocommande.frame.origin.x,cellclientheight/2 - 13, cell.nocommande.frame.size.width, cell.nocommande.frame.size.height);
            cell.nomClient.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
            cell.nocommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey :@"NO_COMMANDE"];
        if(btnSelectedEncours){
            cell.datecommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
        }else{
          cell.datecommande.text =  [DataManager getDateStringFromPresentableDate:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
        }
        
            cell.mt_ttc.text = [DataManager getFormatedNumero:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];
            cell.mt_ht_cmde.text =  [DataManager getFormatedNumero:[[dataManager.commandeFiltred  objectAtIndex:indexPath.row] objectForKey:@"MT_TTC_CMDE"]];
       
            cell.interlocuteur.text=[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"INTERLOCUTEUR"];
        

          //  cell.datecommande.frame=CGRectMake(cell.datecommande.frame.origin.x, cellclientheight/2 -13, cell.datecommande.frame.size.width, cell.datecommande.frame.size.height);
        
           // cell.mt_ht_cmde.frame=CGRectMake(cell.mt_ht_cmde.frame.origin.x, cellclientheight/2 -13 , cell.mt_ht_cmde.frame.size.width, cell.mt_ht_cmde.frame.size.height);
        
        //    cell.mt_ttc.frame=CGRectMake(cell.mt_ttc.frame.origin.x,cellclientheight/2 -13 , cell.mt_ttc.frame.size.width, cell.mt_ttc.frame.size.height);
        
         //   cell.interlocuteur.frame=CGRectMake(cell.interlocuteur.frame.origin.x,cellclientheight/2 - 13 , cell.interlocuteur.frame.size.width, cell.interlocuteur.frame.size.height);
        
        cell.alpha = 0.5;
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:0.5];
                
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:0.5];
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        
        //float cellclientheight=[self getCellHeightForTextPortrait:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"]];

        static NSString *cellIdentifier_p = @"CommandeAffaireCell_iphone_p";
       
        CommandeAffaireCell *cell = (CommandeAffaireCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
       
        if (cell == nil)
        {
            cell = (CommandeAffaireCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
        
        cell.alpha = 0.5;
      
        cell.nomClient.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"TIERS_CLIENT"];
       
        cell.nocommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey :@"NO_COMMANDE"];
        
        if(btnSelectedEncours){
            cell.datecommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
        }else{
            cell.datecommande.text =  [DataManager getDateStringFromPresentableDate:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"DATE_COMMANDE"]];
        }
       // cell.datecommande.text = [[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"STATUT_AFFAIRE"];
        cell.mt_ht_cmde.text = [DataManager getFormatedNumero:[[dataManager.commandeFiltred objectAtIndex:indexPath.row]objectForKey:@"MT_HT_CMDE"]];

        
        

        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:221.0/255.0 blue:224.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (!isLandscape)
    {
        
           
        self.isFromButton = NO;
        dataManager.isdataimmatriculation=NO;
        dataManager.isdatacommande=YES;
        dataManager.isdataengagement=NO;
        
        ListCommandesViewController_iphone * listCommandesViewController_iphone = [[ListCommandesViewController_iphone alloc]initWithNibName:@"ListCommandesViewController_iphone" bundle:nil];
        
  /*     if (!self.showFav)
            listFacturesViewController_iphone.contratNum = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
        else
            listFacturesViewController_iphone.contratNum = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];*/
        listCommandesViewController_iphone.numcommande=[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NO_COMMANDE"];
        [self.navigationController pushViewController:listCommandesViewController_iphone animated:YES];
    }
    
  
}
@end
