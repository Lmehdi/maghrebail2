//
//  EcheanvierViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/28/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "EcheanvierViewController.h"

@interface EcheanvierViewController ()

@end

@implementation EcheanvierViewController
@synthesize arrayFav, showFav;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];


	self.arrayFav = [NSMutableArray array];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MES ECHEANCIERS";
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self initHeaderColor];
	if (self.isFromButton)
	{
		
		NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
		if (dataManager.isDemo)
		{
			params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
		}
		[self postDataToServer:URL_ECHEANCIER_LIST andParam:params];
	}
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (IBAction)showFav:(id)sender {
	
	[arrayFav removeAllObjects];
	if (!showFav)
	{
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/Favoris"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers/Favoris"];
		showFav = YES;
		self.btnFav.selected = YES;
        self.labelFavoris.text=@"LISTE";
		for (NSInteger i = 0 ; i < [[self.response objectForKey:@"response"] count]; i++)
		{
			NSString *str = [[[self.response objectForKey:@"response"] objectAtIndex:i] objectForKey:@"NUM_AFFAIRE"];
			
			if ([[NSUserDefaults standardUserDefaults] stringForKey:str])
			{ 
				if ([[[NSUserDefaults standardUserDefaults] stringForKey:str] isEqualToString:@"ECHEANCIER"])
					[self.arrayFav addObject:[[self.response objectForKey:@"response"] objectAtIndex:i]];
			}
		}
	}
	else
	{ 
		showFav = NO;
		self.btnFav.selected = NO;
        self.labelFavoris.text=@"FAVORIS";
	}
	
	[_tableView reloadData];
}


-(void)updateView{
    [super updateView];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (self.showFav)
	{
		return self.arrayFav.count;
	}
    return [[self.response objectForKey:@"response"] count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email", nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
    [self postDataToServer:URL_ECHEANCIER_LIST andParam:params];
}

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
	
	if (!isLandscape)
	{
		if (dataManager.authentificationData)
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
		}
		else
		{
			if (headerView)
				[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
		}
	}
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
	
	_refreshHeaderView.delegate = self;
	[_tableView addSubview:_refreshHeaderView];
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (void)dealloc {
	[_btnFav release];
    [_labelFavoris release];
	[super dealloc];
}
- (void)viewDidUnload {
	[self setBtnFav:nil];
	[super viewDidUnload];
}
@end
