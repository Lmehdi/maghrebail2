//
//  DetailEcheancierViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailEcheancierViewController_ipad.h"
#import "DetailEcheancierCell_ipad.h"
#import "DetailPDFViewController_ipad.h"

@interface DetailEcheancierViewController_ipad ()

@end

@implementation DetailEcheancierViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	static NSString *cellIdentifier_p = @"DetailEcheancierCell_ipad";
	
	DetailEcheancierCell_ipad *cell = (DetailEcheancierCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
	if (cell == nil)
	{
		cell = (DetailEcheancierCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
	}
	/*
	cell.labelLibelle.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"LIB_GARANTIE"];
	*/
	
	cell.date.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"DATE_ECHEANCE"];
	cell.loyer.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_HT_MENSUEL"];
	cell.tva.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"MTVA"];
	cell.ttc.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_TTC"];
	cell.reste.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_RESTANT_DU_HT_VR"];
	
	switch (indexPath.row % 2)
	{
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
	return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


- (IBAction)showPDF:(id)sender
{
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/description/PDF"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers/description/PDF"];
	 DetailPDFViewController_ipad *detailPDF = [[DetailPDFViewController_ipad alloc] initWithNibName:@"DetailPDFViewController_ipad" bundle:nil];
	 detailPDF.type = @"echeancier"; 
	detailPDF.contratNumMVT = self.numContrat;
	[self.navigationController pushViewController:detailPDF animated:YES];
	 [detailPDF release];
}


@end
