//
//  DetailEcheancierViewController_iphone.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailEcheancierViewController_iphone.h"
#import "DetailEcheancierCell_iphone.h"
#import "DetailPDFViewController_iphone.h"

@interface DetailEcheancierViewController_iphone ()

@end

@implementation DetailEcheancierViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
	static NSString *cellIdentifier_p = @"DetailEcheancierCell_iphone";
	
	DetailEcheancierCell_iphone *cell = (DetailEcheancierCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
	if (cell == nil)
	{
		cell = (DetailEcheancierCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
	}
	/*
	cell.labelLibelle.text =[[[self.response objectForKey:@"response" ]objectAtIndex:indexPath.row ]objectForKey:@"LIB_GARANTIE"];
	*/
	
	
	cell.date.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"DATE_ECHEANCE"];
	cell.loyer.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_HT_MENSUEL"];
	cell.tva.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"MTVA"];
	cell.ttc.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_TTC"];
	cell.reste.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] objectAtIndex:indexPath.row] objectForKey:@"LOYER_RESTANT_DU_HT_VR"];
	
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


- (IBAction)showPDF:(id)sender
{
	
	if (dataManager.isDemo)
		[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/description/PDF"];
	else
		[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers/description/PDF"];
	 DetailPDFViewController_iphone *detailPDF = [[DetailPDFViewController_iphone alloc] initWithNibName:@"DetailPDFViewController_iphone" bundle:nil];
	 detailPDF.type = @"echeancier"; 
	detailPDF.contratNumMVT = self.numContrat;
	[self.navigationController pushViewController:detailPDF animated:YES];
	 [detailPDF release];
}


@end
