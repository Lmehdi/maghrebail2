//
//  DetailEcheancierCell.h
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailEcheancierCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *date;
@property (retain, nonatomic) IBOutlet UILabel *loyer;
@property (retain, nonatomic) IBOutlet UILabel *tva;
@property (retain, nonatomic) IBOutlet UILabel *ttc;
@property (retain, nonatomic) IBOutlet UILabel *reste;

@end
