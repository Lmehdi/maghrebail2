//
//  DetailEcheancierViewController.h
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailEcheancierViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UILabel *codeContrat;
@property (retain, nonatomic) IBOutlet UILabel *montantHT;
@property (retain, nonatomic) IBOutlet UILabel *valeurResid;
@property (retain, nonatomic) IBOutlet UILabel *dureeContrat;
@property (retain, nonatomic) IBOutlet UILabel *dateEffet;
@property (retain, nonatomic) IBOutlet UILabel *tauxVR;
@property (retain, nonatomic) IBOutlet UILabel *terme;
@property (retain, nonatomic) IBOutlet UIView *viewEntete;
@property (retain, nonatomic) IBOutlet UIView *viewTableView;
@property (retain, nonatomic) IBOutlet UIImageView *imgFleche;
@property (retain, nonatomic) IBOutlet UIButton *btnFav;

@property (retain, nonatomic) NSString *numContrat;
- (IBAction)showPDF:(id)sender;
- (IBAction)showEntete:(id)sender;

- (IBAction)handlFav:(id)sender;
-(void) initHeaderColor;
@end
