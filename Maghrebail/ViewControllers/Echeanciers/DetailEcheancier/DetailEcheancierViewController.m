//
//  DetailEcheancierViewController.m
//  Maghrebail
//
//  Created by MAC on 18/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "DetailEcheancierViewController.h"

@interface DetailEcheancierViewController ()

@end

@implementation DetailEcheancierViewController
@synthesize viewTableView;
@synthesize numContrat;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad]; 
    [self initHeaderColor];
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
	
	// Do any additional setup after loading the view.
	NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numContrat,@"num",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"token",@"6b7761d25ded57704372fb7f74e2dc50" , nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
	[self postDataToServer:URL_DETAIL_ECHEANCIER andParam:params];
	  
	// Do any additional setup after loading the view.
	[headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
	[headerView.btnRightHeader addTarget:self action:@selector(showSideOptions) forControlEvents:UIControlEventTouchUpInside];
	[headerView setDelegate:nil];
	[headerView.btnLeftHeader addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	
	if ([[NSUserDefaults standardUserDefaults] stringForKey:self.numContrat])
	{
		if ([[[NSUserDefaults standardUserDefaults] stringForKey:self.numContrat] isEqualToString:@"ECHEANCIER"])
			self.btnFav.selected = YES;
		else 
			self.btnFav.selected = NO;
	}
	else
		self.btnFav.selected = NO;
	
	//[self showEntete:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=[NSString stringWithFormat:@"ÉCHÉANCIER DU CONTRAT N°%@",self.numContrat];
    headerView.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    dataManager.typeColor=@"orange";
}
- (void)goBack
{
	if (!isLandscape || IS_IPAD)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}

-(void)updateView{
    [super updateView];
	
	self.codeContrat.text = [NSString stringWithFormat:@"N° CONTRAT :  %@",[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieA"] objectAtIndex:0] objectForKey:@"NUM_CONTRAT"]];
	
	
	self.montantHT.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieA"] objectAtIndex:0] objectForKey:@"MHT"];
	self.valeurResid.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieA"] objectAtIndex:0] objectForKey:@"VR"]; 
	self.tauxVR.text = [NSString stringWithFormat:@"%@ %%", [[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieA"] objectAtIndex:0] objectForKey:@"TVR"]];
	
	self.dureeContrat.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieB"] objectAtIndex:0] objectForKey:@"DUREE_CONTRAT"];
	self.dateEffet.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieB"] objectAtIndex:0] objectForKey:@"DATE_EFFET"];
	self.terme.text =[[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancieB"] objectAtIndex:0] objectForKey:@"TERME"];
	
	[_tableView reloadData];
}



#pragma mark --
#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	NSLog(@"REDEX %d",[[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] count]);
    return  [[[self.response objectForKey:@"response"] objectForKey:@"detail_echeancie"] count];
}

#pragma mark --
#pragma mark Pull to refresh
-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
	NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.numContrat,@"num",[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"token",@"6b7761d25ded57704372fb7f74e2dc50" , nil];
	if (dataManager.isDemo)
	{
		params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
	}
	[self postDataToServer:URL_DETAIL_ECHEANCIER andParam:params];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_codeContrat release];
    [_montantHT release];
    [_valeurResid release];
    [_dureeContrat release];
    [_dateEffet release];
    [_tauxVR release];
    [_terme release];
	[_viewEntete release];
	[viewTableView release];
	[_imgFleche release];
	[_btnFav release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setCodeContrat:nil];
    [self setMontantHT:nil];
    [self setValeurResid:nil];
    [self setDureeContrat:nil];
    [self setDateEffet:nil];
    [self setTauxVR:nil];
    [self setTerme:nil];
	[self setViewEntete:nil];
	[self setViewTableView:nil];
	[self setImgFleche:nil];
	[self setBtnFav:nil];
    [super viewDidUnload];
}
- (IBAction)showPDF:(id)sender {
}

- (IBAction)showEntete:(id)sender {
	
	CGRect frame = self.viewEntete.frame;
	CGRect frame2 = self.viewTableView.frame;
	if (self.viewEntete.frame.size.height == 33)
	{ 
		frame.size.height = 160;
		[self.imgFleche setImage:[UIImage imageNamed:@"flecheHaut.png"]];
	}
	else
	{ 
		frame.size.height = 33;
		[self.imgFleche setImage:[UIImage imageNamed:@"flecheBas.png"]];
	}
	
	frame2.origin.y = frame.origin.y + frame.size.height + 10;
	frame2.size.height = self.view.frame.size.height - frame2.origin.y - 100;
	
	self.viewEntete.frame = frame;
	self.viewTableView.frame = frame2;
}

- (IBAction)handlFav:(id)sender
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (self.btnFav.selected)
	{
		self.btnFav.selected = NO;
		[standardUserDefaults removeObjectForKey:self.numContrat];
	}
	else
	{
		self.btnFav.selected = YES;
		[standardUserDefaults setObject:@"ECHEANCIER" forKey:self.numContrat];
	}
	[standardUserDefaults synchronize];
}
#pragma mark - Orientation Change

-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note]; 
}


@end
