//
//  EcheancierCell.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/28/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "EcheancierCell_ipad.h"

@implementation EcheancierCell_ipad

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_numContrat release];
    [_miseEnLoyer release];
    [_dateFin release];
    [_duree release];
    [_montant release];
    [super dealloc];
}
@end
