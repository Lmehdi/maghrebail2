//
//  EcheanvierViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 2/28/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "EcheanvierViewController_iphone.h"
#import "DetailPDFViewController_iphone.h"
#import "DetailEcheancierViewController_iphone.h"

@interface EcheanvierViewController_iphone ()

@end

@implementation EcheanvierViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initHeaderColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        [self initHeaderColor];
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isLandscape) {
		
        static NSString *cellIdentifier_l;
		
		if ([DataManager isIphoneLandScape])
		{ 
			cellIdentifier_l = @"EcheancierCell_l";
		}
		else
			cellIdentifier_l = @"EcheancierCell5_l";
		
        EcheancierCell *cell = (EcheancierCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (EcheancierCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        
		if (self.showFav)
		{
			cell.numContrat.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
			cell.miseEnLoyer.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
			cell.dateFin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
			cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"];
			cell.montant.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"TTC"]];
			cell.montantHT.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
		}
		else
		{
			cell.numContrat.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
			cell.miseEnLoyer.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
			cell.dateFin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
			cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"];
			cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"TTC"]];
			cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
		}
		
      
        
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
        
        return cell;
    }else{
        static NSString *cellIdentifier_p = @"EcheancierCell_p";
        
        EcheancierCell *cell = (EcheancierCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (EcheancierCell *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }
		if (self.showFav)
		{
			cell.numContrat.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
			cell.miseEnLoyer.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
			cell.dateFin.text = [DataManager getFormateDate:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
			cell.duree.text = [[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"DURE"];
			cell.montant.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
			cell.montantHT.text = [DataManager getFormatedNumero:[[self.arrayFav objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
		}
		else
		{  
			cell.numContrat.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
			cell.miseEnLoyer.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_EFFET"]];
			cell.dateFin.text = [DataManager getFormateDate:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DATE_FIN"]];
			cell.duree.text = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"DURE"];
			cell.montant.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
			cell.montantHT.text = [DataManager getFormatedNumero:[[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"MFHT"]];
		}
		
        switch (indexPath.row % 2) {
            case 0:
                cell.contentView.backgroundColor = [UIColor whiteColor];
                break;
            case 1:
                 cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
                break;
            default:
                break;
        }
         
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if (!isLandscape)
	{
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/meséchéanciers/description"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/meséchéanciers/description"];
		self.isFromButton = NO;
		DetailEcheancierViewController_iphone *detailPDF = [[DetailEcheancierViewController_iphone alloc] initWithNibName:@"DetailEcheancierViewController_iphone" bundle:nil];
		detailPDF.numContrat = [[[self.response objectForKey:@"response"]objectAtIndex:indexPath.row]objectForKey:@"NUM_AFFAIRE"];
		[self.navigationController pushViewController:detailPDF animated:YES];
		[detailPDF release];
	
		
	}
       
}

@end
