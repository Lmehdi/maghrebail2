//
//  EcheanvierViewController.h
//  Maghrebail
//
//  Created by AHDIDOU on 2/28/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface EcheanvierViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
{
	BOOL showFav; 
}
@property (retain, nonatomic) IBOutlet UIButton *btnFav;
@property (retain, nonatomic) NSMutableArray *arrayFav;
@property (assign, nonatomic) BOOL showFav;

@property (retain, nonatomic) IBOutlet UILabel *labelFavoris;

-(void) initHeaderColor;
- (IBAction)showFav:(id)sender;
@end
