//
//  DocumentVC_ipad.m
//  Maghrebail
//
//  Created by Monassir on 24/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "DocumentVC_ipad.h"
#import "ContratCell_ipad.h"
#import "iCarouselExampleViewController.h"

@interface DocumentVC_ipad ()

@end

@implementation DocumentVC_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAutorate = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --
#pragma mark TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContratCell_ipad *cell = nil;
    
    static NSString *cellIdentifier;
    cellIdentifier = @"ContratCell_ipad";
    cell = (ContratCell_ipad *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = (ContratCell_ipad *)[BaseViewController getCellWithIdentifier:cellIdentifier];
    }
    cell.labelContrat.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
    cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
    cell.labelStatut.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
    cell.labelDateFin.text = [DataManager getFormateDate:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN"]];
    cell.labelDure.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
    cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"TTC"]];
    cell.labelHt.text = [DataManager getFormatedNumero:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
    
	switch (indexPath.row % 2) {
		case 0:
			cell.contentView.backgroundColor = [UIColor whiteColor];
			break;
		case 1:
			cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
			break;
		default:
			break;
	}
    return cell;
}


#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataManager.mesDocumentsFiltred  count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[dataManager.mesDocumentsFiltred objectAtIndex:indexPath.row] objectForKey:@"CHECK"] isEqualToString:@"false"])
    {
        [BaseViewController showAlert:@"Info" message:@"Aucun document PDF trouvé" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
    }
		self.isFromButton = NO;
		
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/documentPDF"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/documentPDF"];
		
		iCarouselExampleViewController *viewC = [[iCarouselExampleViewController alloc] initWithNibName:@"iCarouselExampleViewController_ipad" bundle:nil];
		viewC.numAffaire = [[dataManager.mesDocumentsFiltred objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        
		[self.navigationController pushViewController:viewC animated:YES];
	
}



- (IBAction)filtrePushed:(UIButton *)sender
{
	//[super filtrePushed:sender];
	
	if (self.isFiltreViewShowed)
    {
		self.navigationController.sideMenu.recognizer.enabled = YES;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:nil context:nil];

        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(1024,128,500,640);
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
        //  self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height + self.viewFiltre.frame.size.height);
        
        self.isFiltreViewShowed = NO;
    }
    else
    {
		self.navigationController.sideMenu.recognizer.enabled = NO;
        CGRect filtreFrame = self.viewFiltre.frame;
        [UIView beginAnimations:nil context:nil];

        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDelay:0];
        filtreFrame = CGRectMake(650,128,500,640);
        self.viewFiltre.frame = filtreFrame;
        [UIView commitAnimations];
        
        // self.viewTableView.frame = CGRectMake(self.viewTableView.frame.origin.x, filtreFrame.origin.y + filtreFrame.size.height, self.viewTableView.frame.size.width, self.viewTableView.frame.size.height - self.viewFiltre.frame.size.height);
        self.isFiltreViewShowed = YES;
    }
	
	if (self.isFiltreViewShowed)
	{
		if (btnSelectedEncours)
		{
			self.buttonEncours.selected = YES;
			self.buttonTermine.selected = NO;
			/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"encours",@"type", nil];
			 if (dataManager.isDemo)
			 {
			 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
			 }
			 [self postDataToServer:kCONTRAT_URL andParam:params];*/
		}
		else
		{
			if (btnSelectedTerminee)
			{
				self.buttonEncours.selected = NO;
				self.buttonTermine.selected = YES;
				/*NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[dataManager.authentificationData objectForKey:@"EMAIL"],@"email",@"termine",@"type", nil];
				 if (dataManager.isDemo)
				 {
				 params = [NSMutableDictionary dictionaryWithObject:@"" forKey:@"param"];
				 }
				 [self postDataToServer:kCONTRAT_URL andParam:params];*/
			}
		}
	}
}


@end
