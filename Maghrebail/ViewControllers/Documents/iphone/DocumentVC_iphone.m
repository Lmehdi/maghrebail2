//
//  DocumentVC_iphone.m
//  Maghrebail
//
//  Created by Monassir on 13/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "DocumentVC_iphone.h"
#import "ContratCell_iphone.h"
#import "iCarouselExampleViewController.h"

@interface DocumentVC_iphone ()

@end
CGRect  oldFrameDoc;
@implementation DocumentVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    if(!isLandscape){
        oldFrameDoc=self.viewTableView.frame;
    }
    
    [super viewDidLoad];
     [self initHeaderColor];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self initHeaderColor];
}
-(void)orientationChanged:(NSNotification *)note
{
    [super orientationChanged:note];
    
    if (!isLandscape)
    {
        self.viewTableView.frame=oldFrameDoc;
        [self initHeaderColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContratCell_iphone *cell = nil;
    if (isLandscape)
    {
		static NSString *cellIdentifier_l;
		if ([DataManager isIphoneLandScape])
		{
			cellIdentifier_l = @"ContratCell_iphone_l";
		}
		else
		{
			cellIdentifier_l = @"ContratCell_iphone5_l";
		}
        
        cell = (ContratCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_l];
        if (cell == nil)
        {
            cell = (ContratCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_l];
        }
        cell.labelContrat.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDateFin.text = [DataManager getFormateDate:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_FIN"]];
        cell.labelDure.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"TTC"]];
        cell.labelHt.text = [DataManager getFormatedNumero:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
    }
    else
    {
        static NSString *cellIdentifier_p = @"ContratCell_iphone_p";
        
        cell = (ContratCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
        if (cell == nil)
        {
            cell = (ContratCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
        }

        cell.labelContrat.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];
        cell.labelLoyer.text = [DataManager getFormateDate:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DATE_EFFET"]];
        cell.labelStatut.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"STATUT_AFFAIRE"];
        cell.labelDure.text = [[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"DURE"];
        cell.labelTtc.text = [DataManager getFormatedNumero:[[dataManager.mesDocumentsFiltred  objectAtIndex:indexPath.row] objectForKey:@"MFHT"]];
        
     
        
    }
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:29.0f/255.0f green:31.0f/255.0f blue:42.0f/255.0f alpha:0.1f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}


#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataManager.mesDocumentsFiltred  count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[dataManager.mesDocumentsFiltred objectAtIndex:indexPath.row] objectForKey:@"CHECK"] isEqualToString:@"false"])
    {
        [BaseViewController showAlert:@"Info" message:@"Aucun document PDF trouvé" delegate:nil confirmBtn:@"OK" cancelBtn:nil];
        return;
    }
	if (!isLandscape)
	{
		self.isFromButton = NO;
		
		if (dataManager.isDemo)
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/documentPDF"];
		else
			[Flurry logEvent:@"Home/EspaceAbonnés/Démonstration/documentPDF"];
		
		iCarouselExampleViewController *viewC = [[iCarouselExampleViewController alloc] initWithNibName:@"iCarouselExampleViewController" bundle:nil];
		viewC.numAffaire = [[dataManager.mesDocumentsFiltred objectAtIndex:indexPath.row] objectForKey:@"NUM_AFFAIRE"];

		[self.navigationController pushViewController:viewC animated:YES];
	}
}


@end
