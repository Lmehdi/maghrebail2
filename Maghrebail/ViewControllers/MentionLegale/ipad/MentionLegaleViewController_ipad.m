//
//  MentionLegaleViewController_ipad.m
//  Maghrebail
//
//  Created by MAC on 12/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MentionLegaleViewController_ipad.h"

@interface MentionLegaleViewController_ipad ()

@end

@implementation MentionLegaleViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
