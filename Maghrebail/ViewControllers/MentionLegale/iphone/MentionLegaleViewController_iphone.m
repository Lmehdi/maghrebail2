//
//  MentionLegaleViewController_iphone.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MentionLegaleViewController_iphone.h"

@interface MentionLegaleViewController_iphone ()

@end

@implementation MentionLegaleViewController_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldDisplayLogo=NO;
        shouldDisplayTitre=YES;
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   [self initHeaderColor];
}
-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"MENTIONS LEGALES";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
