//
//  MentionLegaleViewController.m
//  Maghrebail
//
//  Created by AHDIDOU on 3/4/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "MentionLegaleViewController.h"
#import "JSON.h"

@interface MentionLegaleViewController ()

@end

@implementation MentionLegaleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
 
	if (dataManager.authentificationData)
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"deconnexion.png"] forState:UIControlStateNormal];
	}
	else
	{
		if (headerView)
			[headerView.btnRightHeader setImage:[UIImage imageNamed:@"btn_parametre_iphone.png"] forState:UIControlStateNormal];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // [self performSelectorInBackground:@selector(loadMentionsLegales) withObject:nil];
	
	if (dataManager.mentions.count == 0)
	{
		NSData *dataProduits = [DataManager readDataIntoCachWith:@"mentions"];
        if (dataProduits)
        {
            dataManager.mentions = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
        }
		else
		{
			NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mentions" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
			NSDictionary *responseDico = [response JSONValue];
			NSArray *data = [responseDico objectForKey:@"response"];
			dataManager.mentions = [NSMutableArray arrayWithArray:data];
		}
	}
	
    // Do any additional setup after loading the view.
	int yOrigin = 10;
	for (int i = 0 ; i < dataManager.mentions.count ; i++)
	{
		NSDictionary *dic = [dataManager.mentions objectAtIndex:i];
	   
		UILabel *label = [[UILabel alloc] init];
		label.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
		label.backgroundColor = [UIColor clearColor];
		label.numberOfLines = 50;
		label.font = self.labelTitle.font;
	//	CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
		
		label.text = [dic objectForKey:@"titre"];
        [label sizeToFit];
        
        
	//	CGSize expectedLabelSize = [label.text sizeWithFont:self.labelTitle.font constrainedToSize:maximumLabelSize lineBreakMode:self.labelTitle.lineBreakMode];
		
		//adjust the label the new height.
		
		CGRect newFrame = label.frame;
       // newFrame.size.height = label;//expectedLabelSize.height;
		label.frame = newFrame;
		label.textColor =  [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:44.0f/255.0f alpha:1];
		yOrigin += newFrame.size.height+10;
        [self.scrollView addSubview:label];
        
		UILabel *labelDescription = [[UILabel alloc] init];
		labelDescription.frame = CGRectMake(10, yOrigin, self.scrollView.frame.size.width - 20, 20);
		labelDescription.backgroundColor = [UIColor clearColor];
		labelDescription.numberOfLines = 50;
		labelDescription.font = self.labelTitle.font;
	//	CGSize maximumLabelSize2 = CGSizeMake(labelDescription.frame.size.width,9999);
//
		labelDescription.text = [dic objectForKey:@"description"];
        [labelDescription sizeToFit];
        
	//	CGSize expectedLabelSize2 = [labelDescription.text sizeWithFont:self.labelTitle.font constrainedToSize:maximumLabelSize2 lineBreakMode:self.labelTitle.lineBreakMode];
		
		// Adjust the label the the new height.
		
		CGRect newFrame2 = labelDescription.frame;
		//newFrame2.size.height = expectedLabelSize2.height;
		labelDescription.frame = newFrame2;
        
		yOrigin += newFrame2.size.height+10;
		labelDescription.textColor = [UIColor colorWithRed:105.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1];
		[self.scrollView addSubview:labelDescription];

 		[labelDescription release];
 		[label release];
		
	}
    
	[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, yOrigin)];
}
 
-(void)loadMentionsLegales
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	 
	NSString*  response = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mentions" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
	NSDictionary *responseDico = [response JSONValue];
	NSArray *data = [responseDico objectForKey:@"response"];
	dataManager.mentions = [NSMutableArray arrayWithArray:data];
	
    NSURL *url = [NSURL URLWithString:[URL_MENTIONS_LEGALES stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    response = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    response = [DataManager stringByStrippingHTML:response];
    responseDico = [response JSONValue];
    data = [responseDico objectForKey:@"response"];
	
    if (data)
    {
        dataManager.mentions = [NSMutableArray arrayWithArray:data];
        NSData *dataProduits = [NSKeyedArchiver archivedDataWithRootObject:data];
        [DataManager writeDataIntoCachWith:dataProduits andKey:@"mentions"];
    }
    else
    {
        NSData *dataProduits = [DataManager readDataIntoCachWith:@"mentions"];
        if (dataProduits)
        {
            dataManager.mentions = [NSKeyedUnarchiver unarchiveObjectWithData:dataProduits];
        }
    }
    [pool release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_scrollView release];
	[_labelTitle release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
	[self setLabelTitle:nil];
    [super viewDidUnload];
}
@end
