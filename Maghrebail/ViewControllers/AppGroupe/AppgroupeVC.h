//
//  AppgroupeVC.h
//  Maghrebail
//
//  Created by Monassir on 20/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "BaseViewController.h"

@interface AppgroupeVC : BaseViewController
@property (retain, nonatomic) IBOutlet UIView *viewContainer;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)btnBack:(UIButton *)sender;

@end
