//
//  AppgroupeVC_iphone.m
//  Maghrebail
//
//  Created by Monassir on 20/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "AppgroupeVC_iphone.h"
#import "AppgroupeCell_iphone.h"

@interface AppgroupeVC_iphone ()

@end

@implementation AppgroupeVC_iphone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        shouldAddHeader = NO;
//        shouldAutorate = NO;
        shouldDisplayLogo=NO;
//        shouldDisplayTitre=YES;
    }
    return self;
}

-(void) initHeaderColor{
    headerView.labelheaderTitre.text=@"Applications du Groupe";
    headerView.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    dataManager.typeColor=@"gray";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHeaderColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [headerView.btnLeftHeader setImage:[UIImage imageNamed:@"v3-back-top.png"] forState:UIControlStateNormal];
    [headerView setDelegate:nil];
    [headerView.btnRightHeader setHidden:true];
    [self initHeaderColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)removeView:(id)sender
{
    [self.view removeFromSuperview];
}

#pragma mark --
#pragma mark TableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 49;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppgroupeCell_iphone *cell = nil;
	static NSString *cellIdentifier_p = @"AppgroupeCell_iphone";
    
    cell = (AppgroupeCell_iphone *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier_p];
    if (cell == nil)
    {
        cell = (AppgroupeCell_iphone *)[BaseViewController getCellWithIdentifier:cellIdentifier_p];
    }
    
    cell.labelTitle.text = [[dataManager.appgroupe objectAtIndex:indexPath.row] objectForKey:@"titre"];
    [cell.imgViewLogo loadUsingCache:YES];
    [cell.imgViewLogo loadFromURL:[[dataManager.appgroupe objectAtIndex:indexPath.row] objectForKey:@"icone"]];
    
    cell.labelTitle.textColor = [DataManager colorWithHexString:[[dataManager.appgroupe objectAtIndex:indexPath.row] objectForKey:@"titire_color"]];
   
    self.lblTitle.text = [[dataManager.appgroupe objectAtIndex:indexPath.row] objectForKey:@"menu"];
    
    if (indexPath.row % 2)
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:229.0f/255.0f blue:234.0f/255.0f alpha:1.0f]];
    }
    else
    {
        [cell.imgViewBackground setBackgroundColor:[UIColor clearColor]];
    }
	
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/%@?mt=8", [[dataManager.appgroupe objectAtIndex:indexPath.row] objectForKey:@"url_iphone"]]]];
}
- (void)goBack
{

}

@end
