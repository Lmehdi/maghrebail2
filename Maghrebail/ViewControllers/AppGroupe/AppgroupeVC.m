//
//  AppgroupeVC.m
//  Maghrebail
//
//  Created by Monassir on 20/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "AppgroupeVC.h"

@interface AppgroupeVC ()

@end

@implementation AppgroupeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
   // NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"44",@"application_id", nil];
  
    /*
    [self getRemoteContent:URL_APPGROUPE];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadTableViewDataSource
{
    [super reloadTableViewDataSource];
 
    [self getRemoteContent:URL_APPGROUPE];
}

-(void)updateView
{
    [super updateView];
    NSDictionary *header = [self.response objectForKey:@"header"];
    NSString *status = [header objectForKey:@"status"];
    if ([status isEqualToString:@"OK"])
    {
		NSArray *reponse = (NSArray *)[self.response objectForKey:@"response"];
        
        [dataManager.appgroupe removeAllObjects];
         dataManager.appgroupe = [NSMutableArray arrayWithArray:reponse];
        
        [self.tableView reloadData];
    }
}

- (void)dealloc {
    [_viewContainer release];
    [_lblTitle release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setViewContainer:nil];
    [self setLblTitle:nil];
    [super viewDidUnload];
}


#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataManager.appgroupe.count;
}

- (IBAction)btnBack:(UIButton *)sender {
    [self.view removeFromSuperview];
   // [self dismissViewControllerAnimated:NO completion:nil];
}
@end
