//
//  AppgroupeCell.h
//  Maghrebail
//
//  Created by Monassir on 20/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteImageView.h"

@interface AppgroupeCell : UITableViewCell


@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelTaille;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (retain, nonatomic) IBOutlet RemoteImageView *imgViewLogo;

@end
