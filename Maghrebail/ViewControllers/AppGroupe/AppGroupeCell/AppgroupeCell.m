//
//  AppgroupeCell.m
//  Maghrebail
//
//  Created by Monassir on 20/01/2014.
//  Copyright (c) 2014 Mobiblanc. All rights reserved.
//

#import "AppgroupeCell.h"

@implementation AppgroupeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc {
    [_labelTitle release];
    [_labelTaille release];
	[_imgViewBackground release];
	[_imgViewLogo release];
    [super dealloc];
}

@end
