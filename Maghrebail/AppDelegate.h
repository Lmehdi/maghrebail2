//
//  AppDelegate.h
//  Maghrebail
//
// Working On GIT
//  Created by AHDIDOU on 18/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//
//Test Pulll
//For BEndnaiba


#import <UIKit/UIKit.h>
#import "SplashViewController.h"
#import "SplashViewController_iphone.h"
#import "DashboardViewController.h"
#import "DashboardViewController_iphone.h"
#import "PopupAlertViewController_iphone.h"
#import "BaseViewController.h"

#import "CHMFSideMenu.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
	
    DashboardViewController_iphone *dash;
    
    // PushSDK
    
    NSDictionary *lastNotif;
    UIView *pushSDKView;
    NSString *deviceID;
    NSString *modeleDevice;
    NSString * token;
   
}

@property (nonatomic, retain) UINavigationController * mainNavigationController;
@property (nonatomic, retain) MFSideMenu *sideMenu;
@property (nonatomic, retain) MFSideMenuContainerViewController *m_SideMenu;
@property (nonatomic, retain)  PopupAlertViewController_iphone* popupiphone;
@property (nonatomic, retain) HeaderView* headerview;
@property (nonatomic, retain) NSDictionary *lastNotif;
@property (retain, nonatomic) UIView *pushSDKView;
@property (nonatomic, retain) NSString *token;
@property (assign, nonatomic) BOOL isfirsttimeleasebox;
@property (assign, nonatomic) BOOL isfirsttimedash;
@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL isLunched;
@property (assign, nonatomic) BOOL notifstate;
@property (retain, nonatomic) NSTimer *timer;

-(void)sideMenue;
-(void)showNewSideMenu;
- (NSString *)getModel;
@end
