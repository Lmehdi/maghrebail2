//
//  Constants.h
//
//
//  Created by AHDIDOU on 20/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#ifndef CDM_Constants_h
#define CDM_Constants_h


/****************************************************Flurry         ***********************************************/

#define FLURRY_IPHONE_KEY                   @"4WM5JWTQGK7CDY54BRCR"
#define FLURRY_IPAD_KEY                     @"2QS47RPRNYG986XS7KP3"

#define FLURRY_ACTION_AUTH                              @"Athentification"
#define FLURRY_PAGE_HOME                                @"Home"
#define FLURRY_ACTION_HOME_AGENCEPLUSPROCHE             @"Home/Agencelaplusproche"
#define FLURRY_ACTION_HOME_AGENCEPLUSPROCHE_AUTOUMOI    @"Home/Agencelaplusproche/autourdemoi"
#define FLURRY_ACTION_HOME_AGENCEPLUSPROCHE_VILLE       @"Home/Agencelaplusproche/ville"
#define FLURRY_PAGE_HOME_ALAUNE                         @"Home/alaune"
#define FLURRY_PAGE_HOME_ACTUALITES                     @"Home/Actualités"
#define FLURRY_PAGE_HOME_ACTUALITES_CREDITMAROC         @"Home/Actualités/ActualitéscréditduMaroc"
#define FLURRY_PAGE_HOME_ACTUALITES_GENERAL             @"Home/Actualités/Actualitésgénérales"
#define FLURRY_PAGE_HOME_ACTUALITES_SPORTS              @"Home/Actualités/Actualitéssportives"
#define FLURRY_PAGE_HOME_ACTUALITES_CULTURE             @"Home/Actualités/Actualitésculturelles"
#define FLURRY_PAGE_HOME_PRATIQUE                       @"Home/Pratiques"
#define FLURRY_PAGE_HOME_PRATIQUE_VOLS                  @"Home/Pratiques/horairesdevols"
#define FLURRY_PAGE_HOME_PRATIQUE_TRAINS                @"Home/Pratiques/horairesdestrains"
#define FLURRY_PAGE_HOME_PRATIQUE_CINEMA                @"Home/Pratiques/horairesdescinemas"
#define FLURRY_PAGE_HOME_PRATIQUE_METRO                 @"Home/Pratiques/métro"
#define FLURRY_PAGE_HOME_PRATIQUE_METEO                 @"Home/Pratiques/météo"
#define FLURRY_PAGE_HOME_PRATIQUE_PHARMACIES            @"Home/Pratiques/pharmaciesdegardes"
#define FLURRY_PAGE_HOME_PRATIQUE_PHARMACIES_AUTOURMOI  @"Home/Pratiques/pharmaciesdegardes/Autourdemoi"
#define FLURRY_PAGE_HOME_PRATIQUE_PHARMACIES_VILLES     @"Home/Pratiques/pharmaciesdegardes/ville"
#define FLURRY_PAGE_HOME_CONSEILS                       @"Home/conseilsfinanciers"
#define FLURRY_PAGE_HOME_CONSEILS_QUOTIDIENS            @"Home/conseilsfinanciers/conseilsquotidiens"
#define FLURRY_PAGE_HOME_CONSEILS_HEBDO                 @"Home/conseilsfinanciers/conseilshebdomadaires"
#define FLURRY_PAGE_HOME_CONSEILS_ANALYSES              @"Home/conseilsfinanciers/analysesfinanciers"
#define FLURRY_PAGE_HOME_CONSEILS_ETUDES                @"Home/conseilsfinanciers/etudesetanalyses"
#define FLURRY_PAGE_HOME_CONSEILS_NEWSLETTER            @"Home/conseilsfinanciers/newselttersCDM"
#define FLURRY_PAGE_HOME_BONPLANS                       @"Home/Bonsplans"
#define FLURRY_PAGE_HOME_BONPLANS_CREDITMAROC           @"Home/Bonsplans/Créditdumaroc"
#define FLURRY_PAGE_HOME_BONPLANS_PARTENAIRES           @"Home/Bonsplans/partenaires"
#define FLURRY_PAGE_HOME_BONPLANS_PARTICIPE             @"Home/Bonsplans/jeparticipe"
#define FLURRY_PAGE_HOME_PARAMETRES                     @"Home/Paramétres"
#define FLURRY_PAGE_HOME_PARAMETRES_CHOIXVILLE          @"Home/Paramétres/Choixdeville"


/****************************************************URLS         ***********************************************/

#define kAUTHENTIFICATION_URL               @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/auth.php?action=login"
#define kLIST_TIERS_CLIENT                  @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/getalltiers.php?action=getalltiers"
#define kCHANGEMENT_TIERS                   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/auth.php?action=changertiers"
#define SALT                                @"52283025134516455349"
 #define kNEWAUTHENTIFICATION_URL            @"https://www.maghrebail.ma/leasebox/partenaires/api/rest/login"
#define kPROFIL_URL                         @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/profil.php"
#define kCONTRAT_URL                        @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/contrat.php?action=list"
#define kDOCUMENT_URL                       @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/documents.php"
#define kDETAIL_CONTRAT_URL                 @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/contrat.php?action=detailcontrat"
#define kDETAIL_BIEN_URL                    @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/contrat.php?action=detailbien"
#define kLIGNE_CREDIT_DETAIL_URL            @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/lignecredit.php?action=detail"
#define kLIGNE_CREDIT_DETAIL_CONTRAT_URL    @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/lignecredit.php?action=detailcontrat"
#define kLIGNE_CREDIT_DETAIL_BIEN_URL       @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/lignecredit.php?action=detailbien"
#define kPRODUITS_URL                       @"https://www.maghrebail.ma/crm/mobile/produits.php"

#define kINFO_URL							@"https://www.maghrebail.ma/crm/mobile/InfoSecuriteMobile.php"

#define URL_LIGNE_CREDIT                    @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/lignecredit.php?action=list"
#define URL_MESSAGERIE                      @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/message.php?action=list"
#define URL_MESSAGERIE_READ                 @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/message.php?action=read"
#define URL_MESSAGERIE_DELETE               @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/message.php?action=delete"
#define URL_GARANTIES_LIST                  @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/garantie.php?action=list"
#define URL_GARANTIES_DETAILS               @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/garantie.php?action=detailgarantie"
#define URL_ECHEANCIER_LIST                 @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/echeancier.php?action=list"

#define URL_RECLAMATION                     @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/reclamation.php"
#define URL_AGENCES							@"https://www.maghrebail.ma/crm/mobile/agences.php"

#define URL_MES_FACTURES_LIST               @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/facture.php?action=listcontrats" // Email as POST Param
#define URL_MES_FACTURES_CONTRACT_LIST      @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/facture.php?action=listfactures" // numContrat as POST Param
#define URL_FACTURES_PDF                    @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/facture.php?action=facturepdf"  // numContrat && Email as POST Params
#define URL_ECHEANCIER_PDF                  @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/echeancier.php?action=echeanciepdf"

#define URL_MES_CONTACTS                    @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/contacts.php"

#define URL_CHANGE_PASSWORD                 @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/auth.php?action=changepwd"

#define URL_IMPAYE                          @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php?action=impayes"
#define URL_IMPAYE_PDF                      @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php?action=impayesPDF"

#define URL_MENTIONS_LEGALES                @"https://www.maghrebail.ma/crm/mobile/mentionslegales.php"
#define URL_SIMULATEUR						@"https://www.maghrebail.ma/crm/mobile/simulateur.php"

/****************************************************WebServices Demo Fournisseur***********************************************/
#define COMMANDE_ENCOURS_DEMO               @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Commandes_EnCours_Demo"
#define COMMANDE_REGLE_DEMO                 @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Commandes_Regle_Demo"
#define COMMANDE_CLOTURE_DEMO               @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Commandes_Cloture_Demo"
#define DETAILS_COMMANDE_DEMO               @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Details_Commande_Demo"
#define FICHE_COMMANDE_DEMO                 @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Fiche_Bien_Commande_Demo"


#define FACTURE_REGLE_DEMO                  @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Factures_Regles_Demo"
#define FACTURE_CLOTURE_DEMO                @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Factures_Valides_Demo"
#define FACTURE_ENCOURS_DEMO                @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Factures_EnCours_Demo"
#define DETAILS_FACTURE_DEMO                @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Details_Facture_Demo"

#define REGLEMENT_DEMO                      @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Reglements_Demo"
#define DETAILS_REGLEMENT_DEMO              @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Details_Reglement_Demo"
#define IMMATRICULATION_DEMO                @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Immatriculations_Demo"
#define FICHE_IMMATRICULATION_DEMO          @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Detail_Bien_Immatriculation_Demo"
#define DETAILS_IMMATRICULATION_DEMO        @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Details_Immatriculation_Demo"
#define  ENGAGEMENTs_DEMO                   @"http://www.maghrebail.ma/leasebox/partenaires/api/demo/Engagements_Demo"



/*********************************************************************************************************************************/

 //https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php?token=6b7761d25ded57704372fb7f74e2dc50&action=impayesPdfEmail&email=mehdi.akdim@maghrebail.ma

#define URL_FACTURES_MAIL                   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/facture.php?action=facturepdfemail"
#define URL_SHARE_IMPAYES                   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/impayes.php?action=impayesPdfEmail"

#define URL_ECHEANCIER_MAIL                 @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/echeancier.php?action=echeanciepdfemail"
#define URL_DOCUMENT_MAIL                   @"https://www.maghrebail.ma/crm/mobile/dynamique/ws/documents.php?action=sendmail"

#define URL_APPGROUPE                       @"https://www.maghrebail.ma/crm/mobile/ApplicationGroupe.php?token=6b7761d25ded57704372fb7f74e2dc50"

#define URL_FAQ								@"https://www.maghrebail.ma/crm/mobile/faq.php"

#define URL_DETAIL_ECHEANCIER				@"https://www.maghrebail.ma/crm/mobile/dynamique/ws/echeancier.php?action=detailecheanciecontrat"
#define URL_PUBLICATIONS					@"https://www.maghrebail.ma/crm/mobile/publications.php"

#define URL_INFOS							@"https://www.maghrebail.ma/crm/mobile/InfoSecuriteMobile.php"
#define BANNIERE_URL						@"https://www.maghrebail.ma/crm/mobile/Banniere.php?token=6b7761d25ded57704372fb7f74e2dc50&action=iphone"

#define URL_INSCRIPTION                     @"https://www.maghrebail.ma/crm/mobile/inscription.php?token=6b7761d25ded57704372fb7f74e2dc50"

//@"http://slice.mobiblanc.com/adSlice/compagne/bannieresInters/{\"idApp\":\"NQB24DNDHDNCQCVZB93Z\",\"idZones\":\"[1]\"}\/640\/960\/get"
#define BANNIERE_URL_IPAD					@"https://www.maghrebail.ma/crm/mobile/Banniere.php?token=6b7761d25ded57704372fb7f74e2dc50&action=ipad"

/****************************************************Error Messages***********************************************/

#define kCompteDissocie                     @"Votre compte est dissocié de votre Touch ID."
#define kCompteFaceDissocie                     @"Votre compte est dissocié de votre Face ID."
#define kCompteAssocie                      @"Votre compte est maintenant associé avec votre Touch ID."
#define kCompteFAceAssocie                      @"Votre compte est maintenant associé avec votre Face ID."
#define ERROR_LOAGIND                       @"Une erreur est survenue lors de la récupération des données"
#define NO_DATA_LOADED                      @"Aucune donnée disponible"
#define kERROR_EMAIL                        @"Veuillez configurer votre compte messagerie"
#define kERROR_VOL                          @"Aucun vol n'est disponible pour ce trajet"
#define kERROR_CINEMA                       @"Aucune donnée n'est disponible.\nVeuillez réessayer plus tard"
#define kERROR_CINEMA_HORAIRE               @"Veuillez modifier la plage horaire"
#define kERROR_METEO                        @"Aucune donnée n'est disponible.\nVeuillez réessayer plus tard"
#define kERROR_TRAIN                        @"Aucun trajet ne coresspond à votre recherche"
#define kERROR_METRO                        @"Aucune donnée n'est disponible.\nVeuillez réessayer plus tard"
#define kERROR_LIEUX                        @"Aucune donnée n'est disponible.\nVeuillez réessayer plus tard"

/****************************************************    Menu   ***********************************************/

#define kAUTHENTIFICATION_PAGE      2
#define kMON_PROFIL_PAGE            3
#define kLIGNE_CREDIT_PAGE          4
#define kCONTRATS_PAGE              5
#define kPRODUIT_PAGE               6
#define MESSAGERIE_PAGE             100
#define MES_GARANTIES_PAGE          101
#define MES_RECLAMATIONS_PAGE       102
#define MES_ECHEANCIERS_PAGE        103
#define MES_FACTURES_PAGE           104
#define MON_SOLDE_PAGE              105
#define MON_PAIMENT_PAGE            115
#define MENTION_LEGALE_PAGE         106
#define PASSWORD_PAGE               107
#define EXIT_PAGE                   1088

#define MESCOMMANDES_PAGE           200
#define MESFACTURES_PAGE            201
#define MESREGLEMENTS_PAGE          202
#define MESIMMATRICULATIONS_PAGE    203

#define kFAQ_PAGE                   7
#define kSIMULATEUR_PAGE            8
#define kNOS_CONTACTS_PAGE          9
#define kMENTIONS_LEGALES_PAGE      10
#define kAGENCES_PAGE				11
#define kDEMO						200
#define kPAGE_MES_CONTACTS			300
#define kPAGE_PUBLICATIONS			450
#define kPAGE_INSCRIPTION			700
#define kPAGE_DOCUMENTS             1234
#define kPAGE_CHANGERTIERS          780    
#define kPAGE_COMMANDES             500
#define kPAGE_REGLEMENTS            501

#define kPAGE_RMA                   5000
#define kPAGE_BMCE                  5100
#define kPAGE_TWITTER               6000
#define kPAGE_FACEBOOK              6100
#define kPAGE_YOUTUBE               6200
#define kPAGE_FAppGroupe            6101
#define endValue                    1000
#define maxvalue                    20000
/****************************************************   Touch ID Messages PopUp   ************************************/

#define kJailbreak                         @"Votre système n’est pas conforme aux normes de sécurité utilisées par MAGHREBAIL"
#define kTouchID_check                     @"Vérifier votre empreinte"
#define kTouchID_CheckSettings             @"Check your Touch ID Settings."
#define kTouchID_No_touchID                @"No Touch ID fingers enrolled."
#define kTouchID_Not_Available             @"Touch ID not available on your device."
#define kTouchID_NeedPassCode              @"Need a passcode set to use Touch ID."

/****************************************************    Devices   ***********************************************/

#define IPAD                    1001
#define IPHONE_5                1002
#define IPHONE                  1003
#define IPHONE_6                1004
#define IPHONE_6p               1005
#define IPHONE_x               1006

/****************************************************    Picker    ***********************************************/
#define kPICKER_DATE            10
#define kPICKER_DATA            11
#define kDEPART                 12
#define kARRIVEE                13
#define kDATE                   14
#define kHORAIRE                15
#define kVILLE                  18

/****************************************************    Contrat Or LIgne Credit     ***********************************************/
#define kPARENT_LIGNECREDIT     16
#define kPARENT_MENU            17

/****************************************************    Produits type     ***********************************************/
#define kPRODUITS_EQUIPEMENT    21
#define kIMMATRICULATIONS       60
#define kPRODUITS_IMMOBILIER    22
#define KMESENGAGEMENTS         61


/****************************************************    DataManager        ***********************************************/
#define kNOTIFICATION_LOCATED_ME            @"LocatedMe"

/****************************************************    NUMEROS UTILES        ***********************************************/
#define kNUM_TEL1							@"0522203304"
#define kNUM_TEL2							@"0522486500"
#define kNUM_FAX							@"0522274418"
#define kEMAIL								@"maghrebail@maghrebail.com"

/****************************************************    Screen size **********************************************************/

#define mainScreen_width [UIScreen mainScreen].bounds.size.width
#define mainScreen_height [UIScreen mainScreen].bounds.size.height


/****************************************************    TAGS         ***********************************************/
#define MESSAGERIE_MODE_NORMAL   0
#define MESSAGERIE_MODE_DELETE   1
#define MESSAGERIE_MODE_READ     2


///////// Devices  ///////////
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define iPhone6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 667)
#define iPhone6Plus ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 736)
#define isiPhoneX ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 812)


#endif
