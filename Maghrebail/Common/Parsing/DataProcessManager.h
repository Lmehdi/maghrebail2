//
//  DataProcessManager.h
//  Meditel
//
//  Created by mac on 09/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DataProcessManager;
@protocol DataProcessManagerDelegate <NSObject>

@required

-(void)finishedParssing: (DataProcessManager *)dataProcessManager response:(NSDictionary *)response;

@end

@interface DataProcessManager : NSObject{
    NSString * _json;
     id <DataProcessManagerDelegate> delegate;
    
    NSInteger tag;
}

@property (retain) id delegate;
@property (assign) NSInteger tag;

-(id)initWithJson:(NSString *)json;
-(void)processLoginJson;
@end
