//
//  DataProcessManager.m
//  Meditel
//
//  Created by mac on 09/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataProcessManager.h"
#import "JSON.h"
@implementation DataProcessManager
@synthesize delegate;
@synthesize tag;
-(id)initWithJson:(NSString *)json{
    self = [super init];
    if (self) {
        _json = json;
    }
    return self;
}


-(void)processLoginJson{
    
   
    NSDictionary* jsonDict = NULL;
    if (NSClassFromString(@"NSJSONSerialization")) {
        NSData* jsonData=[_json dataUsingEncoding:NSUTF8StringEncoding];
    
            
            //NSLog(@"%@", jsonData);
        NSError* error;
        
        jsonDict = [NSJSONSerialization  JSONObjectWithData:jsonData options:kNilOptions  error:&error];
        
        
    }else{
        jsonDict = [_json JSONValue];
        
    }
    [delegate  finishedParssing:self  response:jsonDict];
    
    
}

@end
