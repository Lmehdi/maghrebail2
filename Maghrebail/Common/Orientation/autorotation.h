//
//  autorotation.h
//  Maghrebail
//
//  Created by bendnaiba mohamed on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UINavigationController (autorotation)

-(BOOL)shouldAutorotate;
-(NSUInteger)supportedInterfaceOrientations;

@end
