//
//  autorotation.m
//  Maghrebail
//
//  Created by bendnaiba mohamed on 01/03/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "autorotation.h"
#import "DataManager.h"

@implementation UINavigationController (autorotation)
 
-(BOOL)shouldAutorotate
{
    
    //UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    [self.topViewController shouldAutorotate];
    DataManager *dataManager = [DataManager sharedManager];
	if (IS_IPAD)
	{
		return YES;
	}
	else
	{
		return dataManager.shouldMainNavigationAutorate ;
	}
}

-(NSUInteger)supportedInterfaceOrientations
{
	//iOS 6
	NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
	
	if (6 <= [[versionCompatibility objectAtIndex:0] intValue])
	{
		if (IS_IPAD)
		{
			return 16 | 8 ;
		}
		return 2 | 16 | 8 | 4;
	}
	else
	{
		if (IS_IPAD) {
			return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight ;
		}
		return UIInterfaceOrientationPortrait | UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationPortraitUpsideDown;
	}
}



@end
