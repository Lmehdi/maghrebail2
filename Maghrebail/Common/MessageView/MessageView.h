//
//  MessageView.h
//  Meditel
//
//  Created by AHDIDOU on 04/01/13.
//
//

#import <UIKit/UIKit.h>

@interface MessageView : UIView{
    
    UILabel * _messageLabel;
}
@property(nonatomic,retain)UILabel * messageLabel;
-(void)adjustLabelHeight:(UILabel * )label string :(NSString *)string;
@end
