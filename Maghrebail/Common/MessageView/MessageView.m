//
//  MessageView.m
//  Meditel
//
//  Created by AHDIDOU on 04/01/13.
//
//

#import "MessageView.h"

@implementation MessageView
@synthesize messageLabel = _messageLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView * alertImage = [[[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width/2 -100, self.frame.size.height/2 - 20, 40, 35)]autorelease];
        UIImage * image = [UIImage imageNamed:@"alerteicon.png"];
        
        alertImage.image = image;
        
        
        [self addSubview: alertImage];
        //[image release];
        _messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(alertImage.frame.origin.x + alertImage.frame.size.width + 10 , self.frame.size.height/2 - 20, self.frame.size.width/2 + 50 , 40)];
        _messageLabel.textAlignment = NSTextAlignmentLeft;
        _messageLabel.numberOfLines = 0;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.font = [UIFont fontWithName:@"Georgia" size:12];
        _messageLabel.textColor = [UIColor colorWithWhite:0.2 alpha:0.7];
        
        [self addSubview:_messageLabel];
       // [self adjustLabelHeight:_messageLabel string:_messageLabel.text];
        
    }
    return self;
}

#pragma mark --
#pragma mark other Stuff

-(void)adjustLabelHeight:(UILabel * )label string :(NSString *)string{
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
    
    CGSize expectedLabelSize = [string sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    
    //adjust the label the the new height.
    
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height;
    label.frame = newFrame;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)dealloc{
    
    [_messageLabel release];
    [super dealloc];
    
}
@end
