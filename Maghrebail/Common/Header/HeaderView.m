//
//  HeaderView.m
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import "HeaderView.h"

@implementation HeaderView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)showSideMenu:(id)sender {
	[[NSNotificationCenter defaultCenter] postNotificationName:@"showMenuPushed" object:nil];
    [delegate showSideMenu];
}

- (IBAction)showSideOptions:(id)sender {
    [delegate showSideOptions];
}

- (void)dealloc {
	[_btnLeftHeader release];
    [_btnRightHeader release];
    [_labelheaderTitre release];
    [_imgHeaderlogo release];
	[super dealloc];
}
@end
