//
//  HeaderView.h
//  Maghrebail
//
//  Created by AHDIDOU on 19/02/13.
//  Copyright (c) 2013 Mobiblanc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HeaderViewSiderMenuDelegate;

@protocol HeaderViewSideMenuDelegate <NSObject>

@required
-(void)showSideMenu;
-(void)showSideOptions;


@end

@interface HeaderView : UIView{
    id<HeaderViewSideMenuDelegate>delegate;
}

// label
@property (retain, nonatomic) IBOutlet UILabel *labelheaderTitre;
@property (retain, nonatomic) IBOutlet UIImageView *imgHeaderlogo;

//

@property (retain, nonatomic) IBOutlet UIButton *btnLeftHeader; 
@property (retain, nonatomic) IBOutlet UIButton *btnRightHeader;


@property(retain)id delegate;
@end
