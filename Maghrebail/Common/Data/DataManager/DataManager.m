//
//  DataManager.m
//  RMA
//
//  Created by Bendnaiba on 04/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataManager.h"
#import <AddressBook/AddressBook.h>
#import "ASIDownloadCache.h"
#import <sys/xattr.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <CommonCrypto/CommonHMAC.h>

static DataManager *sharedMyManager = nil;

@implementation DataManager

@synthesize slideMenu;
@synthesize mArrayUrlImage;
@synthesize mArrayUrlpdf;
@synthesize  FiltredFacture;
@synthesize factures;
@synthesize commandes;
@synthesize isdatafacture;
@synthesize authentificationData;
@synthesize authentificationDatafournisseur;
@synthesize profilData;
@synthesize contrats;
@synthesize reglements;
@synthesize DemoredirectfactureClient;
@synthesize DemoredirectfactureFournisseur;
@synthesize affaireencours;
@synthesize affairesemises;
@synthesize dataimmatriculation;
@synthesize dataengagement;
@synthesize affairescloturees;
@synthesize detailContrat;
@synthesize tablechangementtiers;
@synthesize detailBien;
@synthesize contratFiltred;
@synthesize immatriculationFiltred;
@synthesize reglementsFiltred;
@synthesize commandeFiltred;
@synthesize isdetailscommande;
@synthesize isdatacommande;
@synthesize isdataimmatriculation;
@synthesize isdataengagement;
@synthesize isdetailsreglement;
@synthesize isfirstChangeTiers;
@synthesize ligneCredit;
@synthesize ligneCreditFiltred;
@synthesize produits;
@synthesize agences;
@synthesize isAuthentified;
@synthesize isFournisseurContent;
@synthesize isClientContent;
@synthesize shouldMainNavigationAutorate;
@synthesize firstTime; 
@synthesize isDemo;
@synthesize isFournisseur;
@synthesize isClient;
@synthesize contacts;
@synthesize mentions;
@synthesize locationManager;
@synthesize locationMe;
@synthesize publications;
@synthesize isStayOn;
@synthesize isLandScape;
@synthesize escapePub;
@synthesize isdetailsimmatriculation;
@synthesize isdetailsengagement;
@synthesize mesDocuments;
@synthesize mesDocumentsFiltred;
@synthesize appgroupe;
@synthesize inscription;
@synthesize isdetailscontact;
@synthesize filteredtablechangetiers;
@synthesize  indexselected;
@synthesize indexNewDash;
@synthesize typeColor;
@synthesize isClientTouchID;
@synthesize nameIndexselected;
#pragma mark Singleton Methods

+ (DataManager*)sharedManager
{
    @synchronized(self) {
        if (sharedMyManager == nil) 
        {
            [[self alloc] init];
        }
    }
    return sharedMyManager;
}


+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedMyManager == nil)
        {
            return [super allocWithZone:zone];
        }
    }
    return sharedMyManager;
}

- (id)init
{
    Class myClass = [self class];
    @synchronized(myClass) 
    {
        if (sharedMyManager == nil)
        {
            if ((self = [super init])) 
            {
                sharedMyManager = self;
                
                //cash Folder
                [DataManager creatCachFolder];
                firstTime = YES;
				isDemo = NO;
                _showdemobutton=YES;
                isFournisseur=NO;
                indexselected=-1;
                handleauthentificationconnect=NO;
				if ([[NSUserDefaults standardUserDefaults] stringForKey:@"USER-CONNEXION"])
				{
					if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"USER-CONNEXION"] isEqualToString:@"ON"])
					{
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DataAuthentificationClient"]==YES)
                        {
                        
                            isClientContent=YES;
                            isFournisseurContent=NO;
                        
                        
                        }
                        else if([[NSUserDefaults standardUserDefaults] boolForKey:@"DataAuthentificationFournisseur"]==YES)
                        {
                            isFournisseurContent=YES;
                            isClientContent=NO;
                        
                        }
						isStayOn = YES;
						NSData *dataAuthen = [DataManager readDataIntoCachWith:@"authentificationData"];
						self.isAuthentified = dataAuthen != nil ? YES : NO;
						self.authentificationData = [NSKeyedUnarchiver unarchiveObjectWithData:dataAuthen];
                        
					}
                    
                    
					else
                        
						isStayOn = NO;
				}
                
           
                
                //init
                self.typeColor=@"orange";
                self.mArrayUrlImage = [NSMutableArray array];
                self.mArrayUrlpdf = [NSMutableArray array];
                self.indexNewDash=0;
                self.isfirstChangeTiers=YES;
                
                /*NSData *dataAuthen = [DataManager readDataIntoCachWith:@"authentificationData"];
                self.isAuthentified = dataAuthen != nil ? YES : NO;
                self.authentificationData = [NSKeyedUnarchiver unarchiveObjectWithData:dataAuthen];*/
                self.contrats = [NSMutableArray array];
                self.reglements = [NSMutableArray array];
                self.affaireencours = [NSMutableArray array];
                self.dataimmatriculation=[NSMutableArray array];
                self.dataengagement=[NSMutableArray array];
                self.reglementsFiltred=[NSMutableArray array];
                self.affairesemises = [NSMutableArray array];
                self.commandes = [NSMutableArray array];
                self.affairescloturees = [NSMutableArray array];
                self.filteredtablechangetiers=[NSMutableArray array];
                self.tablechangementtiers=[NSMutableArray array];
                self.agences = [NSMutableArray array];
                self.contacts = [NSMutableArray array];
                self.mentions = [NSMutableArray array];
                self.contratFiltred = [NSMutableArray array];
                self.commandeFiltred = [NSMutableArray array];
                self.FiltredFacture = [NSMutableArray array];
                self.factures = [NSMutableArray array];

                 self.immatriculationFiltred = [NSMutableArray array];
                self.ligneCredit = [NSMutableArray array];
                self.ligneCreditFiltred = [NSMutableArray array];
                self.publications = [NSMutableArray array];
                self.infoSecurite = [NSMutableArray array];
                self.mesDocuments = [NSMutableArray array];
                self.mesDocumentsFiltred = [NSMutableArray array];
                self.appgroupe = [NSMutableArray array];
                self.mesPDF = [NSMutableArray array];
                self.inscription = [NSMutableArray array];
                self.simulateur = @"";
                self.produits = [NSMutableArray array];
                self.locationManager = [[[CLLocationManager alloc] init] autorelease];
                self.locationManager.delegate = self; // send loc updates to myself
                self.locationMe = [[CLLocation alloc] init];
                self.typeDash = 1;
                self.indexselected=-1;
                shouldMainNavigationAutorate = NO;
				
            }
        }
    }
    return sharedMyManager;
}
// KeyChaine Access Methods

static NSString *serviceName = @"www.maghrebail.ma";

+(NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrAccount];
    [searchDictionary setObject:serviceName forKey:(id)kSecAttrService];
    
    return searchDictionary;
}
+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    
    // Add search attributes
    [searchDictionary setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    
    NSData *result = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)searchDictionary,
                                          (CFTypeRef *)&result);
    
    [searchDictionary release];
    return result;
}

+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(id)kSecValueData];
    
    //removing item if it exists
    SecItemDelete((CFDictionaryRef)dictionary);

    OSStatus status = SecItemAdd((CFDictionaryRef)dictionary, NULL);
    [dictionary release];
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (NSString *) hashSHA1:(NSString*)input salt:(NSString*)salt {
    const char *cstr = [[NSString stringWithFormat:@"%@%@",salt,input] cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:strlen(cstr)];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    NSLog(@"%@ : %@", input,output);
    return output;
}

+ (BOOL)updateKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [updateDictionary setObject:passwordData forKey:(id)kSecValueData];
    
    OSStatus status = SecItemUpdate((CFDictionaryRef)searchDictionary,
                                    (CFDictionaryRef)updateDictionary);
    [searchDictionary release];
    [updateDictionary release];
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}
+ (void)deleteKeychainValue:(NSString *)identifier {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    SecItemDelete((CFDictionaryRef)searchDictionary);
    [searchDictionary release];
}

+(BOOL) isJailbroken{
    
    FILE *f = fopen("/bin/bash", "r");
    BOOL isbash = NO;
    if (f != NULL)
    {
        //Device is jailbroken
        isbash = NO;//NO
    }
    fclose(f);
    return isbash;
}
- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (unsigned)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

-(void)dealloc
{
    [mArrayUrlImage release];
    [super dealloc];
}

+ (NSString *)getFormatedNumero:(NSString *)numero
{
    NSRange range = [numero rangeOfString:@" "];
    if(range.location != NSNotFound) {
       
        return numero;
    }
    
    
    numero          =   [numero stringByReplacingOccurrencesOfString:@"," withString:@"."];
    NSNumber* number         =   [NSNumber numberWithFloat:[numero floatValue]];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@","];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    NSString *theString = [numberFormatter stringFromNumber:number];
    
    NSRange rangeI = [theString rangeOfString:@","];
    if(rangeI.location == NSNotFound) {
        theString=[NSString stringWithFormat:@"%@,00",theString];
    }
    
	return theString;
	/*
    NSNumber* number = [NSNumber numberWithDouble:[numero doubleValue]];
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[numberFormatter setGroupingSeparator:@" "];
	NSString* commaString = [numberFormatter stringForObjectValue:number];
	[numberFormatter release];
	if([commaString rangeOfString:@"."].location != NSNotFound) {
		//found it
		
		NSArray *chunks = [commaString componentsSeparatedByString:@"."];
		NSString *str = [chunks objectAtIndex:1];
		if (str.length < 2)
		{ 
			commaString = [commaString stringByAppendingFormat:@"0"];
		}
	}
	else
	commaString = [commaString stringByAppendingFormat:@".00"];
	return commaString;*/
}


+ (NSString *)getFormatedNumero2:(NSString *)numero
{ 
	 NSNumber* number = [NSNumber numberWithDouble:[numero doubleValue]];
	 NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	 [numberFormatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	 [numberFormatter setGroupingSeparator:@" "];
	 NSString* commaString = [numberFormatter stringForObjectValue:number];
	 [numberFormatter release];
	 if([commaString rangeOfString:@"."].location != NSNotFound) {
	 //found it
	 
	 NSArray *chunks = [commaString componentsSeparatedByString:@"."];
	 NSString *str = [chunks objectAtIndex:1];
	 if (str.length < 2)
	 {
	 commaString = [commaString stringByAppendingFormat:@"0"];
	 }
	 }
	 else
	 commaString = [commaString stringByAppendingFormat:@".00"];
	 return commaString; 
}

+ (UIColor *)getColorFromType:(NSString *)type
{
    UIColor* m_color=nil;

    if ([type isEqualToString:@"orange"]) {
        m_color=[UIColor colorWithRed:246.0/255 green:137.0/255 blue:44.0/255 alpha:1];
    }
    if ([type isEqualToString:@"blue"]) {
        m_color=[UIColor colorWithRed:48.0/255 green:116.0/255 blue:184.0/255 alpha:1];
    }
    if ([type isEqualToString:@"gray"]) {
        m_color=[UIColor colorWithRed:102.0/255 green:116.0/255 blue:125.0/255 alpha:1];
    }
    return m_color;
}

- (void)locatedMe
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
   //     [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

#pragma mark -CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
     self.locationMe = newLocation;
	[self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	
}

#pragma mark --
#pragma mark -- Authentification

- (void)VerfiyTouchID
{
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]){
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Veuillez verifier votre emprunt?"
                          reply:^(BOOL success, NSError *error) {
                              if(error){
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
                                                                                  message:@"Problème de verification de l'identité."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                                  return;
                              }
                              if(success){
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                                                  message:@"Identité vérifiée !"
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              } else {
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
                                                                                  message:@"Problème de verification de l'identité."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              }
                          }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
                                                        message:@"Vore appareil ne support pas le TouchID!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
#pragma mark - Initialisation

+(NSString *)getFormateDate:(NSString *)date
{
	return date;
	/*
	NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
	[dateFormatter setDateFormat:@"dd-MMM-yy"];
	NSDate *yourDate                =   [dateFormatter dateFromString:date];
	
	//now format this date to whatever you need…
	[dateFormatter setDateFormat:@"dd-MM-yy"];
	NSString *resultString          =   [dateFormatter stringFromDate:yourDate];
	[dateFormatter release];
	
	return resultString;*/
}


#pragma mark - Data Images

-(void) saveDataImage: (NSData *)data forUrl: (NSString *)url

{
	int index = [self.mArrayUrlImage indexOfObject:url];
	if (index == NSNotFound) 
    {
        [self.mArrayUrlImage addObject:url];
        [DataManager writeDataIntoCachWith:data andKey:url];
        //resave Url
        NSData *dataUrlImages = [NSKeyedArchiver archivedDataWithRootObject:self.mArrayUrlImage];
        [DataManager writeDataIntoCachWith:dataUrlImages andKey:@"mArrayUrlImage"];
    }
	else 
    {
        //ce cas ne devrait pas se presenter
        //[dataImagesUrls replaceObjectAtIndex:index withObject:data];
    }
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:@"NewImage" object:nil];
}



-(NSData *) getDataImageForUrl: (NSString *)url
{
    NSData *data = nil;
    
    int index = [self.mArrayUrlImage indexOfObject:url];
    if (index != NSNotFound)
    {
        data = [DataManager readDataIntoCachWith:url];
    }
    return data;
}




#pragma mark - Resize Images

+ (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    NSLog(@"image = %f   %f", image.size.width, image.size.height);
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSLog(@"size = %f   %f", scaledImage.size.width, scaledImage.size.height);
    return scaledImage;
}

+(UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height
{
	
	CGImageRef imageRef = [image CGImage];
	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
	
	//if (alphaInfo == kCGImageAlphaNone)
    alphaInfo = kCGImageAlphaNoneSkipLast;
	
	CGContextRef bitmap = CGBitmapContextCreate(NULL, width, height, CGImageGetBitsPerComponent(imageRef), 4 * width, CGImageGetColorSpace(imageRef), alphaInfo);
	CGContextDrawImage(bitmap, CGRectMake(0, 0, width, height), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result = [UIImage imageWithCGImage:ref];
	
	CGContextRelease(bitmap);
	CGImageRelease(ref);
	
	return result;	
}



#pragma mark - validateEmail
+(BOOL)validateEmail:(NSString *)mail 
{
    NSLog(@"DataManager validateEmail");
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //  return 0;
    return [emailTest evaluateWithObject:mail];
}

#pragma mark - stringByStrippingHTML
+(NSString *) stringByStrippingHTML:(NSString *)string
{
    NSRange r;
    NSString *s = [[string copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s; 
}

+(NSString *) stringByReplaceHTML:(NSString *)string
{
    NSString *s = [[string copy] autorelease];
    s = [s stringByReplacingOccurrencesOfString:@"\t" withString:@"&nbsp;"];
    s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    return s; 
}

#pragma mark - checkDevice
+ (BOOL) isPad
{
#ifdef UI_USER_INTERFACE_IDIOM
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
#endif
    return NO;
}

#pragma mark - Get Cell
+ (UITableViewCell *) GetCellWith:(NSString *)cellName
{
	NSArray* nibContents = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:NULL]; 
	NSEnumerator *nibEnumerator = [nibContents objectEnumerator]; 
	UITableViewCell *teamCell = NULL;
    //id object = [[NSClassFromString(@"NameofClass") alloc] init];

	NSObject* nibItem = NULL; 
	while ( (nibItem = [nibEnumerator nextObject]) != NULL) 
	{
        NSString *classeName = @"";
        if ([cellName hasSuffix:@"_p"] || [cellName hasSuffix:@"_l"])
        {
            classeName = [cellName substringToIndex:[cellName length]-2];
        }
        else
        {
            classeName = cellName;
        }
		if ( [nibItem isKindOfClass: [NSClassFromString(classeName) class]])
		{
			teamCell = (UITableViewCell *) nibItem;
			if ([teamCell.reuseIdentifier isEqualToString:cellName]) 
				break; 
			else 
				teamCell = NULL; 
		} 
	} 
	return teamCell;	
}

#pragma mark - checkDevice


+ (BOOL)isIphone4
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 480)
    {
        // code for 4-inch screen
        return YES;
    }
    else
    {
        // code for 3.5-inch screen
        return NO;
    }
}

+ (BOOL)isIphone5
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568)
    {
        // code for 4-inch screen
        return YES;
    }
    else
    {
        // code for 3.5-inch screen
        return NO;
    }
}
+ (BOOL)isIphone5LandScape
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.width == 568)
    {
        // code for 4-inch screen
        return YES;
    }
    else
    {
        // code for 3.5-inch screen
        return NO;
    }
}

+ (BOOL)isIphone6LandScape
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.width == 667)
    {
        // code for 4-inch screen
        return YES;
    }
    else
    {
        // code for 3.5-inch screen
        return NO;
    }
}



+ (BOOL)isIphoneLandScape
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.width == 480)
    {
        // code for 4-inch screen
        return YES;
    }
    else
    {
        // code for 3.5-inch screen
        return NO;
    }
}
+ (BOOL)isIphone6
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 667)
    {
        // code for 4.7-inch screen
        return YES;
    }
    
    return NO;

}
+ (BOOL)isIphoneX
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 812)
    {
        // code for 4.7-inch screen
        return YES;
    }
    
    return NO;
    
}
+ (BOOL)isIphone6Plus
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 736)
    {
        // code for 5.5-inch screen
        return YES;
    }

   return NO;
}
+ (int)typeDevice
{
    if (IS_IPAD)
    {
        return IPAD;
    }
    if ([DataManager isIphone6])
    {
        return IPHONE_6;
    }
    if ([DataManager isIphone6Plus])
    {
        return IPHONE_6p;
    }
    if ([DataManager isIphone5])
    {
        return IPHONE_5;
    }
    if ([DataManager isIphoneX])
    {
        return IPHONE_x;
    }
    else
    {
        return IPHONE;
    }
}

+ (int)heightScreen
{
    if ([DataManager typeDevice] == IPHONE_6p)
    {
        return 736;
    }
    if ([DataManager typeDevice] == IPHONE_6)
    {
        return 667;
    }
    if ([DataManager typeDevice] == IPHONE_5)
    {
        return 568;
    }
    if ([DataManager typeDevice] == IPHONE_x)
    {
        return 812;
    }
    else if([DataManager typeDevice] == IPHONE)
    {
        return 480;
    }
    else if([DataManager typeDevice] == IPAD)
    {
        return 768;
    }
    return 568;
}

+ (float)translate
{
    float translateValue = 0.0;
    if ([DataManager typeDevice] == IPHONE_5)
    {
        translateValue = 568.0 - 480.0;
    }
    else if([DataManager typeDevice] == IPHONE)
    {
        translateValue = 0;
    }
    else if([DataManager typeDevice] == IPAD)
    {
        translateValue = 0;
    }
    return translateValue;
}

#pragma mark - Utiles

+ (NSString *)getOccurenceOf:(NSString *)code
{
    if ([code isEqualToString:@"32"] || [code isEqualToString:@"24"])
    {
        return @"32";
    }
    else if ([code isEqualToString:@"30"] || [code isEqualToString:@"28"])
    {
        return @"30";
    }
    else if ([code isEqualToString:@"11"] || [code isEqualToString:@"9"])
    {
        return @"11";
    }
    else if ([code isEqualToString:@"34"] || [code isEqualToString:@"33"])
    {
        return @"34";
    }
    else
    {
        return code;
    }
}

+ (NSDate *)stringToDtae:(NSString *)string
{
    //NSString *dateString = @"10-JAN-13";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:string];
    
    [dateFormatter release];
    return dateFromString;
    
    
}
+ (NSDate *)stringToDateFournisseur:(NSString *)string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"dd-MMM-yy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:string];
    [dateFormatter release];
    return dateFromString;





}

+ (NSString*) getDateStringFromPresentableDate:(NSString*) date {
//    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc] init];
//    dateFormatter.locale            = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    dateFormatter.dateFormat        = @"dd-MMM-yy";
//
//    NSDate * dateObject             = [dateFormatter dateFromString:date];
//
//    dateFormatter.dateFormat        = @"dd/MM/yyyy";
////    NSLog(@"this is the date %@",[dateFormatter stringFromDate:dateObject]);
//    [dateFormatter release];
//    return [dateFormatter stringFromDate:dateObject];
    
    NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
    formatter.locale            = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setDateFormat:@"dd-MM-yy"];
    NSDate * convrtedDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [formatter stringFromDate:convrtedDate];
    
    return dateString;
}


+ (NSString *)dateToString:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    return strDate;
}

+ (NSString *)timeIntervaleToString:(NSTimeInterval)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
    [dateFormatter release];
    return strDate;
}

#pragma mark - should refresh

-(BOOL)shouldRefrech:(NSString *)lien
{
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *lastUpdate = [dateFormat dateFromString:[self lastUpdatedDate:lien]];
    NSDate *now = [[[NSDate alloc] init] autorelease];
    NSTimeInterval interval = [now timeIntervalSinceDate: lastUpdate];
    if ((interval / 3600) > 1)
    {
        return YES;
    }
    return YES;
}

-(NSString *)lastUpdatedDate:(NSString *)lien
{
    NSURL *url = [NSURL URLWithString:[lien stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSDate *lastModifiedLocal = nil;  
    if (url) 
    {
        NSFileManager *fileManager = [NSFileManager defaultManager]; 
        NSString *cachedPath = [[ASIDownloadCache sharedCache] pathToCachedResponseDataForURL:url];
        if ([fileManager fileExistsAtPath:cachedPath]) 
        {  
            NSError *error = nil;  
            NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:cachedPath error:&error];  
            if (error) 
            {  
                NSLog(@"Error reading file attributes for: %@ - %@", cachedPath, [error localizedDescription]);  
            }  
            else
            {
                lastModifiedLocal = [fileAttributes fileModificationDate];
            }
        } 
    }
    
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *lastModifiedFormated = @"";
    if (lastModifiedLocal) 
    {
        lastModifiedFormated = [dateFormat stringFromDate:lastModifiedLocal];
    }
    else
    {
        lastModifiedFormated = @"2010-10-19 00:00:00";
    }
    
    return lastModifiedFormated;
}
#pragma - mark Scan if int or string

+ (BOOL)scanNumero:(NSString *)num
{
    NSString *numeroRegex = @"[0-9]+";
    NSPredicate *numeroTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numeroRegex];
    return [numeroTest evaluateWithObject:num];
}
 


#pragma mark - gestion iCloud && Cash

+ (void)creatCachFolder
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cacheDirectoryName = @"";
    NSString *os5 = @"5.0";
    
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    if ([currSysVer compare:os5 options:NSNumericSearch] == NSOrderedAscending) //lower than 4
    {
        cacheDirectoryName = path;
    }
    else if ([currSysVer compare:os5 options:NSNumericSearch] == NSOrderedDescending) //5.0.1 and above
    {
        cacheDirectoryName = path;
        cacheDirectoryName = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/MAGHREBAIL"];
    }
    else // IOS 5
    {
        cacheDirectoryName = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/MAGHREBAIL"];
    }
    BOOL isDirectory = NO;
    BOOL folderExists = [fileManager fileExistsAtPath:cacheDirectoryName isDirectory:&isDirectory] && isDirectory;
    
    if (!folderExists)
    {
        NSError *error = nil;
        [fileManager createDirectoryAtPath:cacheDirectoryName withIntermediateDirectories:YES attributes:nil error:&error];
        [error release];
    }
    
    //NSURL *storeUrl = [NSURL fileURLWithPath:cacheDirectoryName];
    //[self addSkipBackupAttributeToItemAtURL:storeUrl];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSString *os5 = @"5.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:os5 options:NSNumericSearch] == NSOrderedAscending) //lower than 4
    {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        const char* filePath = [[URL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    }
    else // IOS 5.1 and above // jsut if base sdk is ios 6
    {
        /*assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        NSError *error = nil;
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success)
        {
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
        return success;*/
    }
}

+ (NSString*) savePath
{
    NSString *os5 = @"5.0";
    
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    if ([currSysVer compare:os5 options:NSNumericSearch] == NSOrderedAscending) //lower than 4
    {
        path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    }
    else if ([currSysVer compare:os5 options:NSNumericSearch] == NSOrderedDescending) //5.0.1 and above
    {
        path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/MAGHREBAIL"];
    }
    else // IOS 5
    {
        path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/MAGHREBAIL"];
    }
    
    return path;
}

+ (void)writeDataIntoCachWith:(NSData *)data andKey:(NSString *)key
{
    NSError *error = nil;
    [data writeToFile:[[DataManager savePath] stringByAppendingPathComponent:key] options:NSDataWritingAtomic error:&error];
    NSLog(@"this is the file location %@",[[DataManager savePath] stringByAppendingPathComponent:key]);
    NSLog(@"Write returned error: %@", [error localizedDescription]);
}




+ (NSData *)readDataIntoCachWith:(NSString *)key
{  
    NSString *path = [[DataManager savePath] stringByAppendingPathComponent:key];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return data;
}

+ (void)deleteFileFromCache:(NSString *)key
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsDirectory = [DataManager savePath];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:key];
    [fileManager removeItemAtPath:filePath error:NULL];
}

#pragma mark -- 
#pragma mark Default Ville 
+ (void)setDefaultVille:(NSString *)ville{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:ville forKey:@"DEFAULT_VILLE"];
    [defaults synchronize];
}
+ (NSString *)getDefaultVille
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString *defaultVille = [defaults objectForKey:@"DEFAULT_VILLE"];
    if (defaultVille == nil)
    {
        defaultVille = @"CASABLANCA";
    }
    return defaultVille;
}

#define DEFAULT_VOID_COLOR [UIColor grayColor]

+ (UIColor *) colorWithHexString:(NSString *)stringToConvert
{
    stringToConvert = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6)
        return DEFAULT_VOID_COLOR;
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"＃"])
        cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6)
        return DEFAULT_VOID_COLOR;
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}



@end

