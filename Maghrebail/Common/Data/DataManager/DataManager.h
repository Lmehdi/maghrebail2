//
//  DataManager.h
//  RMA
//
//  Created by Bendnaiba on 04/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <CoreLocation/CoreLocation.h> 

@class SlideViewController;
@interface DataManager : NSObject<CLLocationManagerDelegate>
{
    //image Data
    NSMutableArray *mArrayUrlImage;
    
    //public data
    NSMutableArray *produits;
    
    //Espace Abonne Data
    BOOL isAuthentified;
    BOOL handleauthentificationconnect;
    NSDictionary *authentificationData;
    NSDictionary *authentificationDatafournisseur;
    NSMutableArray *ligneCredit;
    NSMutableArray *ligneCreditFiltred;
    NSMutableArray * tablechangementtiers;
    NSDictionary *profilData;
    NSMutableArray *contrats;
    NSMutableArray *affaireencours;
    NSMutableArray *affairesemises;
    NSMutableArray *dataimmatriculation;
    NSMutableArray *affairescloturees;

    NSMutableArray *contratFiltred;
    NSMutableArray *filteredtablechangetiers;
    NSDictionary *detailContrat;
    NSDictionary *detailBien;

    
    //Orientation
    BOOL shouldMainNavigationAutorate;
	BOOL firstTime;
}
@property (nonatomic, retain) SlideViewController *slideMenu;
@property (nonatomic, retain) NSMutableArray *mArrayUrlImage;
@property (nonatomic, retain) NSMutableArray *mArrayUrlpdf;
@property (nonatomic, retain) NSDictionary *authentificationData;
@property (nonatomic, retain) NSDictionary *authentificationDatafournisseur;
@property (nonatomic, retain) NSDictionary *profilData;
@property (nonatomic, retain) NSMutableArray *contrats;
@property (nonatomic, retain) NSMutableArray *reglements;
@property (nonatomic, retain) NSMutableArray *filteredtablechangetiers;


@property (nonatomic, retain) NSMutableArray *affaireencours;
@property (nonatomic, retain) NSMutableArray *affairesemises;
@property (nonatomic,retain)  NSMutableArray *commandes;
@property (nonatomic, retain)  NSMutableArray* dataimmatriculation;
@property (nonatomic, retain)  NSMutableArray* dataengagement;
@property (nonatomic, retain) NSMutableArray *affairescloturees;
@property (nonatomic, retain) NSDictionary *detailContrat;
@property (nonatomic, retain) NSDictionary *detailBien;
@property (nonatomic, retain) NSMutableArray *contratFiltred;
@property (nonatomic, retain) NSMutableArray *reglementsFiltred;
@property (nonatomic, retain) NSMutableArray *immatriculationFiltred;
@property (nonatomic, retain) NSMutableArray *commandeFiltred;
@property (nonatomic, retain) NSMutableArray *tablechangementtiers;
@property (nonatomic, retain) NSMutableArray *factures;
@property (nonatomic, retain) NSMutableArray *FiltredFacture;
@property (nonatomic, retain) NSMutableArray *ligneCredit;
@property (nonatomic, retain) NSMutableArray *ligneCreditFiltred;
@property (nonatomic, retain) NSMutableArray *produits;
@property (nonatomic, retain) NSMutableArray *agences;
@property (nonatomic, retain) NSMutableArray *contacts;
@property (nonatomic, retain) NSMutableArray *mentions;
@property (nonatomic, retain) NSMutableArray *publications;
@property (nonatomic, retain) NSMutableArray *mesDocuments;
@property (nonatomic, retain) NSMutableArray *mesDocumentsFiltred;
@property (nonatomic, retain) NSMutableArray *mesPDF;
@property (nonatomic, retain) NSMutableArray *infoSecurite;
@property (nonatomic, retain) NSMutableArray *appgroupe;
@property (nonatomic, retain) NSMutableArray *inscription;
@property (assign, nonatomic) BOOL showdemobutton;
@property (assign, nonatomic) BOOL tooglestate;
@property (assign, nonatomic) BOOL  DemoredirectfactureClient;
@property (assign, nonatomic) BOOL  DemoredirectfactureFournisseur;

@property (nonatomic, retain) NSString *simulateur;
@property (assign, nonatomic)  BOOL isshared;
@property (assign, nonatomic)  BOOL isClientTouchID;

@property (assign) int indexNewDash;
@property (assign) BOOL isAuthentified;
@property (assign) BOOL handleauthentificationconnect;
@property (assign) BOOL isClientContent;
@property (assign) BOOL isFournisseurContent;
@property (assign) BOOL isdetailscommande;
@property (assign) BOOL isdetailsimmatriculation;
@property (assign) BOOL isdetailsengagement;
@property (assign) BOOL isdetailscontact;
@property (assign) BOOL isdatafacture;


@property (assign) BOOL isdatacommande;

@property (assign) BOOL isdataengagement;
@property (assign) BOOL isdataimmatriculation;

@property (assign) BOOL isdetailsreglement;
@property (assign) BOOL isfirstChangeTiers;
@property (assign) BOOL isAuthentifiedFournisseur;
@property (assign) BOOL shouldMainNavigationAutorate;
@property (assign) BOOL firstTime;
@property (assign, nonatomic) NSString *typeColor;
@property (assign, nonatomic) BOOL isDemo;
@property (assign, nonatomic) BOOL isFournisseur;
@property (assign, nonatomic) BOOL isClient;

@property (assign, nonatomic) BOOL isLoad;


@property (assign, nonatomic) BOOL isStayOn;
@property (assign, nonatomic) BOOL isLandScape;
@property (assign, nonatomic) BOOL escapePub;

@property (retain, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) CLLocation *locationMe;
@property (assign, nonatomic) NSInteger typeDash;
@property (assign, nonatomic) int indexselected;
@property (assign, nonatomic) NSString *nameIndexselected;



// KeyChaine Access
+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier;
+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier;
+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier;
+ (BOOL)updateKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier;
+ (void)deleteKeychainValue:(NSString *)identifier;
+ (NSString *) hashSHA1:(NSString*)input salt:(NSString*)salt;
//
+(BOOL) isJailbroken;
+ (id)sharedManager;
- (void) saveDataImage: (NSData *)data forUrl: (NSString *)url;
- (void) saveDatapdf: (NSData *)data forname: (NSString *)name forurl:(NSString *) url;
- (NSData *) getDatapdfForUrl: (NSString *)url;
- (NSData *) getDataImageForUrl: (NSString *)url;
+ (NSDate *)stringToDateFournisseur:(NSString *)string;

+ (UIImage *)scale:(UIImage *)image toSize:(CGSize)size;
+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height;
+ (BOOL)validateEmail:(NSString *)mail;
+ (NSString *)stringByStrippingHTML:(NSString *)string;
+ (NSString *)stringByReplaceHTML:(NSString *)string;
+ (BOOL)isPad;
+ (UITableViewCell *) GetCellWith:(NSString *)cellName;
- (BOOL)shouldRefrech:(NSString *)lien;
- (NSString *)lastUpdatedDate:(NSString *)lien;
+ (BOOL)scanNumero:(NSString *)num;
+ (NSString *)getFormatedNumero:(NSString *)dataString;
+ (NSString *)decryptString:(NSString *)stringCrypti;
+ (void)creatCachFolder;
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (NSString*) savePath;
+ (void)writeDataIntoCachWith:(NSData *)data andKey:(NSString *)key;
+ (NSData *)readDataIntoCachWith:(NSString *)key;
+ (void)deleteFileFromCache:(NSString *)key;
+ (UIColor *)getColorFromType:(NSString *)type;
// Dimensions

+ (BOOL)isIphone5;
+ (BOOL)isIphone5LandScape;
+ (BOOL)isIphone6LandScape;
+ (BOOL)isIphoneLandScape;
+ (BOOL)isIphone6;
+ (BOOL)isIphoneX;
+ (BOOL)isIphone6Plus;
+ (BOOL)isIphone4;
+ (int)typeDevice;
+ (int)heightScreen;

+ (NSString *)getOccurenceOf:(NSString *)code;
+ (NSDate *)stringToDtae:(NSString *)string;
+ (NSString *)dateToString:(NSDate *)date;
+ (NSString*) getDateStringFromPresentableDate:(NSString*) date;
+ (NSString *)timeIntervaleToString:(NSTimeInterval)time;

+ (float)translate;
+ (NSString *)getFormatedNumero2:(NSString *)numero;
+(NSString *)getFormateDate:(NSString *)date;
- (void)locatedMe;

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;




@end

