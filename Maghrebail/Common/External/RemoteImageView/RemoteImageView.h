//
//  RemoteImageView.h
//  BNP
//
//
//  Copyright 2010 Clicmobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageCache.h"

@class RemoteImageView;
@protocol RemoteImageViewDelegate
-(void)startedDownloadingImage:(RemoteImageView *)image;
-(void)imageLoaded:(RemoteImageView *)image fromCache:(BOOL)cache;
-(void)errorLoadingImage:(RemoteImageView *)image;

@end


@interface RemoteImageView : UIImageView
{
	NSString		*_url;
	ImageCache		*_imageCache;
	NSObject <RemoteImageViewDelegate> *_delegate;
	NSRunLoop		*_delegateLoop;
	NSURLConnection	*_connection;
	NSMutableData	*_receivedData;
	UIBackgroundTaskIdentifier _bgTask;
	UIActivityIndicatorView	*_loader;
}

-(void)load;
-(void)loadUsingCache:(BOOL)useCache;
-(void)loadFromURL:(NSString *)url;
-(void)setImageCustumer:(UIImage *)img;

@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) ImageCache *imageCache;
@property (nonatomic, assign) id <RemoteImageViewDelegate> delegate;
@property (nonatomic, assign) BOOL thumbnail;
@end
