//
//  UIView+Coords.h
//  Clicmobile Libs
//
//  Created by basile cornet on 17/04/09.
//  Copyright 2009 clicmobile. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
	@author Basile Cornet
 */
@interface UIView (Coords)

/**
	@brief This gives the left Frame coordinate of the receiver.
	This represent the x coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat left;

/**
	@brief This gives the top Frame coordinate of the receiver.
	This represent the y coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat top;

/**
	@brief This gives the right coordinate of the receiver.
	This represent the x + width coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat right;

/**
	@brief This gives the bottom coordinate of the receiver.
	This represent the y + height coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat bottom;

	
/**
	@brief This gives the width coordinate of the receiver.
	This represent the width coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat width;

/**
	@brief This gives the height coordinate of the receiver.
	This represent the height coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat height;

@property (nonatomic) CGSize size;
@property (nonatomic) CGPoint origin;

-(void)centerInParent;
-(void)topLeftInParent;
-(void)topCenterInParent;
-(void)topRightInParent;
-(void)centerLeftInParent;
-(void)centerRightInParent;
-(void)bottomLeftInParent;
-(void)bottomCenterInParent;
-(void)bottomRightInParent;
-(void)leftOfView:(UIView *)v withSpace:(CGFloat)space;
-(void)rightOfView:(UIView *)v withSpace:(CGFloat)space;
-(void)aboveView:(UIView *)v withSpace:(CGFloat)space;
-(void)underView:(UIView *)v withSpace:(CGFloat)space;
-(void)alignLeftWithView:(UIView *)v;
-(void)alignRightWithView:(UIView *)v;
-(void)alignTopWithView:(UIView *)v;
-(void)alignBottomWithView:(UIView *)v;
-(void)centerVerticallyWithView:(UIView *)v;
-(void)centerHorizontallyWithView:(UIView *)v;
-(void)fillSuperview;
-(void)fillSuperviewWithMargin:(CGFloat)margin;
-(void)fillSuperviewWithHorizontalMargin:(CGFloat)hMargin verticalMargin:(CGFloat)vMargin;
-(void)fillSuperviewWithTopMargin:(CGFloat)tMargin rightMargin:(CGFloat)rMargin bottomMargin:(CGFloat)bMargin leftMargin:(CGFloat)lMargin;
- (void)toLandscape:(UIInterfaceOrientation)orientation;
- (void)fadeAnimation:(SEL)selector duration:(CGFloat)duration delay:(NSTimeInterval)delay alpha:(CGFloat)alpha delegate:(id)delegate;

@end
