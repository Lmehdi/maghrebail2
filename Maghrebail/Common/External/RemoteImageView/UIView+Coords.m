//
//  UIView+Coords.m
//  Clicmobile Libs
//
//  Created by basile cornet on 17/04/09.
//  Copyright 2009 clicmobile. All rights reserved.
//

#import "UIView+Coords.h"


@implementation UIView (Coords)


- (CGFloat)left {
	return self.frame.origin.x;
}


- (void)setLeft:(CGFloat)x {
	CGRect frame = self.frame;
	frame.origin.x = x;
	self.frame = frame;
}


- (CGFloat)top {
	return self.frame.origin.y;
}


- (void)setTop:(CGFloat)y {
	CGRect frame = self.frame;
	frame.origin.y = y;
	self.frame = frame;
}


- (CGFloat)right {
	return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight: (CGFloat)right {
	CGRect fr = self.frame;
	fr.origin.x = right - fr.size.width;
	self.frame = fr;
}

- (CGFloat)bottom {
	return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom: (CGFloat)bottom {
	CGRect fr = self.frame;
	fr.origin.y = bottom - fr.size.height;
	self.frame = fr;
}

- (CGFloat)width {
	return self.frame.size.width;
}


- (void)setWidth:(CGFloat)width {
	CGRect frame = self.frame;
	frame.size.width = width;
	self.frame = frame;
}


- (CGFloat)height {
	return self.frame.size.height;
}


- (void)setHeight:(CGFloat)height {
	CGRect frame = self.frame;
	frame.size.height = height;
	self.frame = frame;
}

-(CGSize)size
{
	return self.frame.size;
}

-(void)setSize: (CGSize)size
{
	CGRect fr = self.frame;
	fr.size = size;
	self.frame = fr;
}

-(CGPoint)origin
{
	return self.frame.origin;
}

-(void)setOrigin:(CGPoint) origin
{
	CGRect fr = self.frame;
	fr.origin = origin;
	self.frame = fr;
}	

-(void)centerInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGSize size = sup.frame.size;
	self.center = CGPointMake(size.width / 2.0, size.height / 2.0);
}

-(void)topLeftInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = 0.0;
	fr.origin.y = 0.0;
	self.frame = fr;
}
	
-(void)topCenterInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = (sup.frame.size.width - fr.size.width) / 2.0;
	fr.origin.y = 0.0;
	self.frame = fr;
}

-(void)topRightInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = sup.frame.size.width - fr.size.width;
	fr.origin.y = 0.0;
	self.frame = fr;
}

-(void)centerLeftInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = 0.0;
	fr.origin.y = (sup.frame.size.height - fr.size.height) / 2.0;
	self.frame = fr;
}

-(void)centerRightInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = sup.frame.size.width - fr.size.width;
	fr.origin.y = (sup.frame.size.height - fr.size.height) / 2.0;
	self.frame = fr;
}

-(void)bottomLeftInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = 0.0;
	fr.origin.y = sup.frame.size.height - fr.size.height;
	self.frame = fr;
}

-(void)bottomCenterInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = (sup.frame.size.width - fr.size.width) / 2.0;
	fr.origin.y = sup.frame.size.height - fr.size.height;
	self.frame = fr;
}

-(void)bottomRightInParent
{
	UIView *sup = self.superview;
	if(sup == nil) return;
	CGRect fr = self.frame;
	fr.origin.x = sup.frame.size.width - fr.size.width;
	fr.origin.y = sup.frame.size.height - fr.size.height;
	self.frame = fr;
}

-(void)leftOfView:(UIView *)v withSpace:(CGFloat)space
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.right = localPt.x - space;
}

-(void)rightOfView:(UIView *)v withSpace:(CGFloat)space;
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.left = localPt.x + v.frame.size.width + space;
}

-(void)aboveView:(UIView *)v withSpace:(CGFloat)space;
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.bottom = localPt.y - space;
}

-(void)underView:(UIView *)v withSpace:(CGFloat)space
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}

	self.top = localPt.y + v.frame.size.height + space;
}

-(void)alignLeftWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.left = localPt.x;
}

-(void)alignRightWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.right = localPt.x + v.frame.size.width;
}

-(void)alignTopWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.top = localPt.y;
}
	
-(void)alignBottomWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	self.bottom = localPt.y + v.frame.size.height;
}

-(void)centerVerticallyWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	CGPoint ctr = self.center;
	ctr.y = localPt.y + v.frame.size.height / 2.0;
	self.center = ctr;
}

-(void)centerHorizontallyWithView:(UIView *)v
{
	if((v == nil) || (v.superview == nil) || (self.superview == nil)) return;
	
	CGPoint localPt;
	
	if(v.superview == self.superview)
	{
		localPt = v.frame.origin;
	}
	else
	{
		CGPoint vOrigin = v.frame.origin;
		localPt = [v.superview convertPoint:vOrigin toView:self.superview];
	}
	
	CGPoint ctr = self.center;
	ctr.x = localPt.x + v.frame.size.width / 2.0;
	self.center = ctr;
}

-(void)fillSuperview
{
	[self fillSuperviewWithMargin:0.0];
}

-(void)fillSuperviewWithMargin:(CGFloat)margin
{
	[self fillSuperviewWithHorizontalMargin:margin verticalMargin:margin];
}

-(void)fillSuperviewWithHorizontalMargin:(CGFloat)hMargin verticalMargin:(CGFloat)vMargin
{
	[self fillSuperviewWithTopMargin:vMargin rightMargin:hMargin bottomMargin:vMargin leftMargin:hMargin];
}

-(void)fillSuperviewWithTopMargin:(CGFloat)tMargin rightMargin:(CGFloat)rMargin bottomMargin:(CGFloat)bMargin leftMargin:(CGFloat)lMargin
{
	if(self.superview == nil) return;
	CGRect superFrame = self.superview.frame;
	
	CGRect fr;
	fr.origin.x = lMargin;
	fr.origin.y = tMargin;
	fr.size.width = superFrame.size.width - lMargin - rMargin;
	fr.size.height = superFrame.size.height - tMargin - bMargin;
	self.frame = fr;
}

- (void)toLandscape:(UIInterfaceOrientation)orientation {
	[self setCenter:CGPointMake(160.0, 240.0)];
	
	CGFloat angle = 0.0;
	if (orientation == UIInterfaceOrientationLandscapeRight)
		angle = (90.0 / 180.0) * M_PI;
	if (orientation == UIInterfaceOrientationLandscapeLeft)
		angle = (-90.0 / 180.0) * M_PI;
	CGAffineTransform cgCTM = CGAffineTransformMakeRotation(angle);
	self.transform = cgCTM;
}

- (void)fadeAnimation:(SEL)selector duration:(CGFloat)duration delay:(NSTimeInterval)delay alpha:(CGFloat)alpha delegate:(id)delegate {
	[UIView beginAnimations:nil context:nil];
	if (selector != nil && delegate != nil) {
		[UIView setAnimationDelegate:delegate];
		[UIView setAnimationDidStopSelector:selector];
	}
	[UIView setAnimationDelay:delay];
	[UIView setAnimationDuration:duration];
	
	[self setAlpha:alpha];
	
	[UIView commitAnimations];
}

@end
