//
//  UIImage+Utilities.h
//  BNPTraveler
//
//  Created by badre daha on 28/06/11.
//  Copyright 2011 Mobiware. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIImage (Utilities)

- (UIImage *)scaledImageWithImage:(float)maxImageWidth maxImageHeight:(float)maxHeight;
- (UIImage *)fixImageOrientation;
@end
