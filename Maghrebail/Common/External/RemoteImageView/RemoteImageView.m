//
//  RemoteImageView.m
//  BNP
//
//
//  Copyright 2010 Clicmobile. All rights reserved.
//

#import "RemoteImageView.h"
#import "UIImage+Utilities.h"
#import "UIView+Coords.h"


#define  THUMBNAIL  @"thumbnail_%@"

CGSize sizeImage ;
@interface RemoteImageView (Private)

-(void)_startedDownloadingImage;
-(void)_imageLoaded;
-(void)_imageLoadedFromCache;
-(void)_errorLoadingImage;
-(void)_sendStartedDownloadingImage;
-(void)_sendImageLoadedFromCache:(BOOL)cache;
-(void)_sendErrorLoadingImage;

@end


@implementation RemoteImageView
@synthesize url = _url;
@synthesize imageCache = _imageCache;
@synthesize delegate = _delegate;
@synthesize thumbnail;


-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}
-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    sizeImage = frame.size;
    return self;
}

-(void)load
{
	[self loadUsingCache:YES];
}

-(void)loadUsingCache:(BOOL)useCache
{
	if(!_url) return;
    NSString *fileName = nil;
    if (thumbnail) {
        // ajout de _thumnail
        fileName = [NSString stringWithFormat:THUMBNAIL,[_url lastPathComponent]];
    }else
    {
        fileName = [_url lastPathComponent];
    }
	[_delegateLoop cancelPerformSelectorsWithTarget:self];
    
	if(_connection != nil)
	{
		[_connection cancel];
		[_connection release];
		_connection = nil;
		[_loader stopAnimating];
		[_loader removeFromSuperview];
		_loader = nil;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
    
	if([[UIApplication sharedApplication] respondsToSelector:@selector(endBackgroundTask:)])
		_bgTask = UIBackgroundTaskInvalid;
    
	NSURL *urlObject = [NSURL URLWithString: _url];
	if([urlObject isFileURL])
	{
		self.image = [UIImage imageNamed:fileName];
		[self _sendImageLoadedFromCache:YES];
		return;
	}
	
	if(useCache)
	{
		UIImage *img;
		@synchronized(_imageCache)
		{
			if(_imageCache == nil)
			{
				_imageCache = [ImageCache sharedImageCache];
			}
			img = [_imageCache imageForKey:fileName];
		}
		if(img != nil)
		{
			self.image = img;
			[self _sendImageLoadedFromCache:YES];
			return;
		}
	}
	
	NSURLRequest *theRequest =
	[NSURLRequest	requestWithURL:urlObject
					 cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
				 timeoutInterval:60.0];
	
	// create the connection with the request
	// and start loading the data
	_connection =[[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:NO];
	if (_connection != nil)
	{
		// Create the NSMutableData that will hold
		// the received data
		// receivedData is declared as a method instance elsewhere
		_receivedData = [[NSMutableData data] retain];
		UIApplication* app = [UIApplication sharedApplication];
		if([app respondsToSelector:@selector(beginBackgroundTaskWithExpirationHandler:)] && (_bgTask == UIBackgroundTaskInvalid))
		{
			_bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
				if(_bgTask != UIBackgroundTaskInvalid)
				{
					[_connection cancel];
					[self _sendErrorLoadingImage];
					[app endBackgroundTask:_bgTask];
					_bgTask = UIBackgroundTaskInvalid;
				}
				
			}];
		}
		self.image = nil;		
		[self _sendStartedDownloadingImage];
		_loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[self addSubview:_loader];
		[_loader release];
		_loader.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
		[_loader startAnimating];
		[_connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
		[_connection start];
	}
	else
	{
		// inform the user that the download could not be made
		[self _sendErrorLoadingImage];
	}
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)loadFromURL:(NSString *)url
{
	self.url = url;
	[self load];
}

-(void)setDelegate:(id<RemoteImageViewDelegate>)delegate
{
	if(delegate == _delegate) return;
	[_delegateLoop cancelPerformSelectorsWithTarget:self];
	_delegateLoop = [NSRunLoop currentRunLoop];
	_delegate = (NSObject <RemoteImageViewDelegate> *)delegate;
}

-(void)setImage:(UIImage *)img
{
	[_delegateLoop cancelPerformSelectorsWithTarget:self];
	[super setImage:img];
	if((img != nil) && [_delegate respondsToSelector:@selector(imageLoaded:fromCache:)])
		[_delegateLoop performSelector:@selector(_imageLoaded) target:self argument:nil order:0 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
}

-(void)setImageCustumer:(UIImage *)img
{
	[super setImage:img];
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	if(_loader != nil)
	{
		[_loader centerInParent];
	}
}

- (void)dealloc {
	self.url = nil;
	self.delegate = nil;
    [super dealloc];
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // this method is called when the server has determined that it
    // has enough information to create the NSURLResponse
	
    // it can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    // receivedData is declared as a method instance elsewhere
	NSLog(@"RemoteImageView: DidreceiveResponse");
    [_receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // append the new data to the receivedData
    // receivedData is declared as a method instance elsewhere
    [_receivedData appendData:data];
    //	NSLog(@"RemoteImageView: Got %d bytes", [data length]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // inform the user
    NSLog(@"RemoteImageView: Connection failed! Error - %@ %@",
         [error localizedDescription],
         [[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	
	[_connection release];
	_connection = nil;
    // receivedData is declared as a method instance elsewhere
    [_receivedData release];
	_receivedData = nil;
	
	[_loader stopAnimating];
	[_loader removeFromSuperview];
	_loader = nil;
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	[self _sendErrorLoadingImage];
    
	if([[UIApplication sharedApplication] respondsToSelector:@selector(endBackgroundTask:)])
	{
		[[UIApplication sharedApplication] endBackgroundTask:_bgTask];
		_bgTask = UIBackgroundTaskInvalid;
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"RemoteImageView: Succeeded! Cover received (%d bytes of data)",[_receivedData length]);
	
    // release the connection, and the data object
	[_connection release];
	_connection = nil;
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
	UIImage *img = [UIImage imageWithData:_receivedData];
	[_receivedData release];
	_receivedData = nil;
	
	if(img != nil)
	{
        
        if (thumbnail) 
        {   
            UIImage *imgThumbnail = [img scaledImageWithImage:self.frame.size.width maxImageHeight:self.frame.size.height];
            
            NSString  *url = [NSString stringWithFormat:THUMBNAIL,[_url lastPathComponent]];
            [_imageCache storeImage:imgThumbnail withKey:url];
        }
        
            [_imageCache storeImage:img withKey:[_url lastPathComponent]];
		
		self.image = img;
		[self _sendImageLoadedFromCache:NO];
	}
	else {
		[self _sendErrorLoadingImage];
	}
    
	[_loader stopAnimating];
	[_loader removeFromSuperview];
	_loader = nil;
    
	if([[UIApplication sharedApplication] respondsToSelector:@selector(endBackgroundTask:)])
	{
		[[UIApplication sharedApplication] endBackgroundTask:_bgTask];
		_bgTask = UIBackgroundTaskInvalid;
	}
}

#pragma mark Private methods

-(void)_sendStartedDownloadingImage
{
	if([_delegate respondsToSelector:@selector(startedDownloadingImage:)])
	{
		[_delegateLoop performSelector:@selector(_startedDownloadingImage) target:self argument:nil order:0 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
	}	
}

-(void)_sendImageLoadedFromCache:(BOOL)cache
{
	if([_delegate respondsToSelector:@selector(imageLoaded:fromCache:)])
	{
		if(cache)
		{
			[_delegateLoop performSelector:@selector(_imageLoadedFromCache) target:self argument:nil order:0 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
		}
		else
		{
			[_delegateLoop performSelector:@selector(_imageLoaded) target:self argument:nil order:0 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
		}
	}
    
}

-(void)_sendErrorLoadingImage
{
	if([_delegate respondsToSelector:@selector(errorLoadingImage:)])
	{
		[_delegateLoop performSelector:@selector(_errorLoadingImage) target:self argument:nil order:0 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
	}	
}

-(void)_startedDownloadingImage
{
	[_delegate startedDownloadingImage:self];
}

-(void)_imageLoaded
{
	[_delegate imageLoaded:self fromCache:NO];
}

-(void)_imageLoadedFromCache
{
	[_delegate imageLoaded:self fromCache:YES];
}

-(void)_errorLoadingImage
{
	[_delegate errorLoadingImage:self];
}
@end
