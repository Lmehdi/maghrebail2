//
//  NetworkManager.h
//  
//
//  Created by AHDIDOU on 20/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "DataProcessManager.h"
#import "ASIDownloadCache.h"

@class NetworkManager;
@protocol NetworkManagerDelegate <NSObject>

@required
-(void)finished:(NetworkManager *)networkManager result:(NSMutableDictionary * )result;
@required
-(void)started:(NetworkManager *)networkManager;
@required
-(void)gotError:(NetworkManager *)networkManager;

@end

@interface NetworkManager : NSObject <ASIHTTPRequestDelegate,DataProcessManagerDelegate>{
    
    id<NetworkManagerDelegate>delegate;
    NSString * _url;
    
}

@property (retain) id delegate;
@property (assign) int tag;

-(id) initWithURL:(NSString *)url;
-(void) doGet;
-(void)doGetAuthentification :(NSString *)username  password:(NSString*)pass;
-(void) doPost:(NSMutableDictionary *)params;
@end
