//
//  NetworkManager.m
//  
//
//  Created by AHDIDOU on 20/11/12.
//  Copyright (c) 2012 AHDIDOU. All rights reserved.
//

#import "NetworkManager.h"
#import "DataManager.h"

@implementation NetworkManager
@synthesize delegate,tag;

-(id)initWithURL:(NSString *)url{
    self = [super init];
    if (self) {
        _url = url;
        }
    return self ;
}

#pragma mark --
#pragma mark GET
-(void)doGet
{
    NSURL * urlTemp = [NSURL URLWithString:_url];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    [[ASIDownloadCache sharedCache] setShouldRespectCacheControlHeaders:NO];
    
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
    [request setCachePolicy:ASIFallbackToCacheIfLoadFailsCachePolicy];
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    [request setSecondsToCache:60*60*24*30];
    [request setTimeOutSeconds:20];
    //NSString *pathCach = [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:request];
    //[request setNumberOfTimesToRetryOnTimeout:2];
    request.delegate = self;
    [request startAsynchronous];
}

-(void)doGetAuthentification :(NSString *)username  password:(NSString*)pass
{
    NSURL * urlTemp = [NSURL URLWithString:_url];
    ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:urlTemp];
    
    static NSString * const kHTTPDigestChallengeExample1 = @"Digest realm=\"MAGHREBAIL\", "
    "qop=\"auth\", "
    "nonce=\"\", "
    "opaque=\"e88b637c1044af812b15752beabdc37e\"";
    
    [[ASIDownloadCache sharedCache] setShouldRespectCacheControlHeaders:NO];
    [request setUsername:username];
    [request setPassword:pass];
   // request.authenticationScheme = kHTTPDigestChallengeExample1;
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
    [request setCachePolicy:ASIFallbackToCacheIfLoadFailsCachePolicy];
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    [request setSecondsToCache:60*60*24*30];
    [request setTimeOutSeconds:60];
    
    request.delegate = self;
    
    
    [request startAsynchronous];
}


#pragma mark --
#pragma mark POST
-(void)doPost:(NSMutableDictionary *)params
{
	if(params != nil)
		[params setObject:@"6b7761d25ded57704372fb7f74e2dc50" forKey:@"token"];
    NSURL * urlTemp = [NSURL URLWithString:_url];
    ASIFormDataRequest *postRequest = [ASIFormDataRequest requestWithURL:urlTemp];
    [[ASIDownloadCache sharedCache] setShouldRespectCacheControlHeaders:NO];
    [postRequest setDownloadCache:[ASIDownloadCache sharedCache]];
    [postRequest setCachePolicy:ASIFallbackToCacheIfLoadFailsCachePolicy];
    [postRequest setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    [postRequest setSecondsToCache:60*60*24*30];
    [postRequest setTimeOutSeconds:60];
    //NSString *pathCach =  [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:postRequest];
    //[postRequest setNumberOfTimesToRetryOnTimeout:2];
    for (NSString *key in [params allKeys])
    {
        [postRequest setPostValue:[params objectForKey:key] forKey:key];
    }
    
   // NSLog(@"PARAMS %@", params);
    postRequest.delegate = self;
    [postRequest startAsynchronous];
}

#pragma mark --
#pragma mark ASIHTTPRequest Delegate



-(void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders
{

    
    
}

-(void)requestStarted:(ASIHTTPRequest *)request
{
    [self.delegate started:self];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    
    NSData *responseData = [request responseData];
    NSString * resultString =  [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    DataProcessManager * dataProcessManager = [[DataProcessManager alloc]initWithJson:resultString];
    dataProcessManager.delegate = self;
    
    if (resultString != nil)
        [dataProcessManager processLoginJson];
    else
        [self.delegate gotError:self];
    
   // NSLog(@"Received Header %@",resultString);
	
}
-(void)requestFailed:(ASIHTTPRequest *)request{
	NSLog(@"%@", request.error);
    [self.delegate gotError:self];
	
}

#pragma mark --
#pragma mark DataProcessManager Delegate Methode

-(void)finishedParssing:(DataProcessManager *)dataProcessManager response:(NSDictionary *)response
{
    NSMutableDictionary *responseDict = [[NSMutableDictionary alloc]initWithDictionary:response];
    [self.delegate finished:self result:responseDict];
}


#pragma mark --
#pragma mark Memory Management

-(void)dealloc{
    [delegate release];
    [super dealloc];
}

@end
